mkdir -p  sent-to-pact/config/
mkdir -p  sent-to-pact/vm/
rm sent-to-pact/config/*
rm sent-to-pact/vm/*
curl http://127.0.0.1:9754/mock/pact/upload/configuration/last-sent-operation -o sent-to-pact/config/config.json
cat sent-to-pact/config/config.json |  node  getFileSent.js  | awk '{print "curl http://127.0.0.1:9754/mock/pact/upload/configuration?file=" $1 " -o sent-to-pact/config/" $1  }'   | sh
curl http://127.0.0.1:9754/mock/pact/upload/voting-materials/last-sent-operation -o sent-to-pact/vm/config.json
cat sent-to-pact/vm/config.json |  node  getFileSent.js  | awk '{print "curl http://127.0.0.1:9754/mock/pact/upload/configuration?file=" $1 " -o sent-to-pact/vm/" $1  }'   | sh
