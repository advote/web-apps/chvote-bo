#!/usr/bin/env node

var stdin = process.openStdin();

var data = "";

stdin.on('data', function (chunk) {
    data += chunk;
});

stdin.on('end', function () {
    let request = JSON.parse(data);
    (request.registerFilesCatalog || request.attachments).forEach(attachment => console.log(attachment.zipFileName));
});