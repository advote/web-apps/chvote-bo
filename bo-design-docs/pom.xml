<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ Back Office
  ~ %%
  ~ Copyright (C) 2016 - 2018 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ch.ge.ve</groupId>
    <artifactId>bo-design-docs</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>bo-design-docs</name>
    <description>Back office design documentation</description>

    <distributionManagement>
        <snapshotRepository>
            <id>chvote-snapshots</id>
            <name>bitnami-artifactory-dm-df0d-snapshots</name>
            <url>http://artifactory.chvote-dev.work/artifactory/libs-snapshot</url>
        </snapshotRepository>
        <repository>
            <id>chvote</id>
            <name>bitnami-artifactory-dm-df0d-releases</name>
            <url>http://artifactory.chvote-dev.work/artifactory/libs-release</url>
        </repository>
    </distributionManagement>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <asciidoctor.maven.plugin.version>1.5.7.1</asciidoctor.maven.plugin.version>
        <asciidoctorj.pdf.version>1.5.0-alpha.16</asciidoctorj.pdf.version>
        <asciidoctorj.diagram.version>1.5.10</asciidoctorj.diagram.version>
        <asciidoctorj.version>1.5.8.1</asciidoctorj.version>
        <plantuml-maven-plugin.version>1.2</plantuml-maven-plugin.version>
    </properties>

    <build>
        <defaultGoal>process-resources</defaultGoal>

        <plugins>
            <plugin>
                <groupId>com.github.jeluard</groupId>
                <artifactId>plantuml-maven-plugin</artifactId>
                <version>${plantuml-maven-plugin.version}</version>
                <configuration>
                    <sourceFiles>
                        <directory>${basedir}/src/main/diagrams</directory>
                        <includes>
                            <include>**/*.puml</include>
                        </includes>
                    </sourceFiles>
                    <outputDirectory>${basedir}/src/main/images/generated</outputDirectory>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>net.sourceforge.plantuml</groupId>
                        <artifactId>plantuml</artifactId>
                        <version>8059</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <id>generate-diagrams</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>


            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>copy-asciidoc-resources</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <resources>
                                <resource>
                                    <directory>src/main/images</directory>
                                    <includes>
                                        <include>**/*.jpg</include>
                                        <include>**/*.png</include>
                                        <include>**/*.svg</include>
                                    </includes>
                                </resource>
                            </resources>
                            <outputDirectory>target/generated-docs/images</outputDirectory>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy-asciidoc-icons</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <resources>
                                <resource>
                                    <directory>src/main/icons</directory>
                                    <includes>
                                        <include>**/*.png</include>
                                    </includes>
                                </resource>
                            </resources>
                            <outputDirectory>target/generated-docs/icons</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>


            <plugin>
                <groupId>org.asciidoctor</groupId>
                <artifactId>asciidoctor-maven-plugin</artifactId>
                <version>${asciidoctor.maven.plugin.version}</version>
                <dependencies>
                    <dependency>
                        <groupId>org.asciidoctor</groupId>
                        <artifactId>asciidoctorj-pdf</artifactId>
                        <version>${asciidoctorj.pdf.version}</version>
                    </dependency>
                    <dependency>
                        <groupId>org.jruby</groupId>
                        <artifactId>jruby-complete</artifactId>
                        <version>9.2.4.1</version>

                    </dependency>
                </dependencies>

                <executions>
                    <execution>
                        <id>generate-pdf-doc</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>process-asciidoc</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/generated-docs/pdf</outputDirectory>
                            <preserveDirectories>true</preserveDirectories>
                            <relativeBaseDir>true</relativeBaseDir>
                            <backend>pdf</backend>
                            <sourceHighlighter>prettify</sourceHighlighter>
                            <attributes>
                                <sectanchors>true</sectanchors>
                                <icons>font</icons>
                                <pagenums/>
                                <toc>left</toc>
                                <idprefix/>
                                <idseparator>-</idseparator>
                                <plantUMLDir>resources</plantUMLDir>
                            </attributes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
