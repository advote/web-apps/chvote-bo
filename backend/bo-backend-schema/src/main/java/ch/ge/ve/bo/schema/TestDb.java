/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.schema;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import org.slf4j.LoggerFactory;

/**
 * Class used to verify if the database is up and running
 */
public class TestDb {

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TestDb.class);


  /**
   * Entry point
   */
  public static void main(String[] args) {
    String url = args[0];
    String user = args[1];
    String pwd = args[2];
    boolean databaseIsUp = false;
    try {
      new TestDb().testConnection(url, user, pwd);
      databaseIsUp = true;
    } catch (SQLException e) {
      logger.error(e.getMessage(), e);
    } finally {
      logger.info("{} connection is {}", url, (databaseIsUp ? "up" : "down"));
    }
    System.exit(databaseIsUp ? 0 : 1);
  }

  private void testConnection(String url, String user, String pwd) throws SQLException {
    Connection conn = null;
    boolean acceptsURL = false;
    try {
      Enumeration<Driver> drivers = DriverManager.getDrivers();
      while (!acceptsURL && drivers.hasMoreElements()) {
        Driver driver = drivers.nextElement();
        acceptsURL = driver.acceptsURL(url);
      }
      if (!acceptsURL) {
        throw new SQLException(String.format("No driver accepts url : %s", url));
      }
      DriverManager.getDriver(url);
      conn = DriverManager.getConnection(url, user, pwd);
    } finally {
      if (conn != null) {
        conn.close();
      }
    }
  }
}
