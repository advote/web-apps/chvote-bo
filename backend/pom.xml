<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ chvote-bo
  ~ %%
  ~ Copyright (C) 2016 - 2019 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.1.0.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <groupId>ch.ge.ve</groupId>
    <artifactId>bo-backend-base</artifactId>
    <version>1.0.0</version>
    <packaging>pom</packaging>

    <name>bo-backend-base</name>
    <description>CHVote Backoffice backend project</description>

    <modules>
        <module>bo-backend-schema</module>
        <module>bo-backend-repository</module>
        <module>bo-backend-service</module>
        <module>bo-backend-rest</module>
        <module>bo-backend-mock-server</module>
    </modules>

    <distributionManagement>
        <snapshotRepository>
            <id>chvote-snapshots</id>
            <name>CHVote Snapshots</name>
            <url>${env.MVN_DIST_SNAPSHOTS_URL}/</url>
        </snapshotRepository>
        <repository>
            <id>chvote</id>
            <name>CHVote Releases</name>
            <url>${env.MVN_DIST_RELEASES_URL}/</url>
        </repository>
    </distributionManagement>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>11</java.version>
        <spock.version>1.2-groovy-2.4</spock.version>
        <guava.version>25.1-jre</guava.version>
        <commons-io.version>2.5</commons-io.version>
        <bcpkix-jdk15on.version>1.57</bcpkix-jdk15on.version>
        <commons-fileupload.version>1.3.3</commons-fileupload.version>
        <cglib.version>3.2.9</cglib.version>
        <flyway-spring5-test.version>5.1.0</flyway-spring5-test.version>
        <jaxb-core.version>2.3.0.1</jaxb-core.version>

        <!-- CHVote dependencies -->
        <pact.version>0.0.5</pact.version>
        <file-namer.version>0.1.2</file-namer.version>
        <jackson-serializer.version>1.0.1</jackson-serializer.version>
        <chvote-protocol-model.version>1.0.6</chvote-protocol-model.version>
        <chvote-protocol-core.version>1.0.16</chvote-protocol-core.version>
        <chvote-interface.version>2.0.11</chvote-interface.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.glassfish.jaxb</groupId>
                <artifactId>jaxb-core</artifactId>
                <version>${jaxb-core.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>chvote-protocol-model</artifactId>
                <version>${chvote-protocol-model.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>protocol-algorithms</artifactId>
                <version>${chvote-protocol-core.version}</version>
            </dependency>
            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>${commons-io.version}</version>
            </dependency>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>bo-backend-schema</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>bo-backend-repository</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>bo-backend-repository</artifactId>
                <version>${project.version}</version>
                <type>test-jar</type>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>bo-backend-service</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>bo-backend-service</artifactId>
                <version>${project.version}</version>
                <type>test-jar</type>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>${project.groupId}</groupId>
                <artifactId>bo-backend-rest</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>file-namer</artifactId>
                <version>${file-namer.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve.interfaces</groupId>
                <artifactId>chvote-interfaces-jaxb</artifactId>
                <version>${chvote-interface.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>pact-back-contract</artifactId>
                <version>${pact.version}</version>
            </dependency>
            <dependency>
                <groupId>org.bouncycastle</groupId>
                <artifactId>bcpkix-jdk15on</artifactId>
                <version>${bcpkix-jdk15on.version}</version>
            </dependency>
            <dependency>
                <groupId>ch.ge.ve</groupId>
                <artifactId>jackson-serializer</artifactId>
                <version>${jackson-serializer.version}</version>
            </dependency>
            <dependency>
                <groupId>commons-fileupload</groupId>
                <artifactId>commons-fileupload</artifactId>
                <version>${commons-fileupload.version}</version>
            </dependency>
            <dependency>
                <groupId>xml-resolver</groupId>
                <artifactId>xml-resolver</artifactId>
                <version>1.2</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-web</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.flywaydb.flyway-test-extensions</groupId>
                <artifactId>flyway-spring5-test</artifactId>
                <version>${flyway-spring5-test.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>cglib</groupId>
                <artifactId>cglib-nodep</artifactId>
                <version>${cglib.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-core</artifactId>
                <version>${spock.version}</version>
            </dependency>
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-spring</artifactId>
                <version>${spock.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all</artifactId>
            <version>2.4.15</version>
            <scope>test</scope>
        </dependency>
        <!-- Logging dependencies : all modules should use Logback through slf4j API -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-report-plugin</artifactId>
            <version>2.22.1</version>
        </dependency>
    </dependencies>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                            <configuration>
                                <attach>false</attach>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.jvnet.jaxb2.maven2</groupId>
                    <artifactId>maven-jaxb2-plugin</artifactId>
                    <version>0.13.1</version>
                </plugin>
                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>0.4.13</version>
                    <executions>
                        <execution>
                            <id>build-image</id>
                            <phase>package</phase>
                            <goals>
                                <goal>build</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <!--<skipDocker>true</skipDocker>-->
                        <dockerDirectory>${project.basedir}/src/main/docker</dockerDirectory>
                        <buildArgs>
                            <finalName>${project.build.finalName}.jar</finalName>
                        </buildArgs>
                        <forceTags>true</forceTags>
                        <imageTags>
                            <imageTag>${project.version}</imageTag>
                            <imageTag>latest</imageTag>
                        </imageTags>
                        <resources>
                            <resource>
                                <targetPath>/</targetPath>
                                <directory>${project.build.directory}</directory>
                                <include>${project.build.finalName}.jar</include>
                            </resource>
                        </resources>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>org.codehaus.gmavenplus</groupId>
                <artifactId>gmavenplus-plugin</artifactId>
                <version>1.5</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>addTestSources</goal>
                            <goal>testCompile</goal>
                            <goal>removeTestStubs</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-clean-plugin</artifactId>
                <executions>
                    <execution>
                        <id>clean-groovy-stubs</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>clean</goal>
                        </goals>
                        <configuration>
                            <excludeDefaultDirectories>true</excludeDefaultDirectories>
                            <filesets>
                                <fileset>
                                    <directory>target\generated-sources\groovy-stubs</directory>
                                    <includes>
                                        <include>**</include>
                                    </includes>
                                </fileset>
                            </filesets>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-report-plugin</artifactId>
                <version>2.22.1</version>
            </plugin>
        </plugins>
    </reporting>

</project>
