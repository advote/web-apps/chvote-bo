# CHVote-2.0 BackOffice Backend development project

## Logging

Use SLF4J API. 

Logback-classic is the SLF4J implementation used.

## Development

This is a Maven project, so use usual maven commands.

## How to start the backend

* Define an environment variable to set log directory :

set LOG_DIR=./logs (for example)

* Start backend application with parameters defining datasource :

java -Duser.language=EN  -jar bo-backend-rest-version.jar --spring.profiles.active=development 

## How to start the mock server

Mock server is used for e2e testing or in development mode in order to simulate other server or to interfere with database
 
java -jar bo-backend-mock-server.jar --spring.profiles.active=development

A swagger ui is available at 
http://127.0.0.1:9754/mock/swagger-ui.html

## Integrating FrontEnd and BackEnd project in local development environment

On a developer laptop, FrontEnd and BackEnd are served on 2 differents http ports, which forbid FrontEnd to access BackEnd REST services (same origin policy).

To bypass this impediment, BackEnd must add CORS headers to every HTTP responses. 

This is done by activating the 'development' Spring profile when starting BackEnd application. 

Add `-Dspring.profiles.active=development` to the command-line.

## Error handling strategy

### Business validation errors

In case of a business validation error, a method should :
* throw an exception inheriting from `BusinessException`
* specify a message key, which must be defined in `i18n.ts` of frontend project

### Rest error handling

A Spring REST error handler is defined (see `RestErrorHandled`) to translate any exception into an HTTP response :
* BusinessException are translated to HTTP status code 400 and a ValidationErrorModel payload
* RuntimeException are trnaslated to HTTP status code 500 and a TechnicalErrorVo

## Note

Spring @Value annotations are resolved on post init phase of bean initialization, so they must not be accessed from constructor. Inject Environment parameter in constructor, or use PostInit lifecycle hook