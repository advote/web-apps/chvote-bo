/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest;

import ch.ge.ve.bo.repository.DictionaryRepository;
import ch.ge.ve.bo.repository.entity.Dictionary;
import ch.ge.ve.bo.repository.entity.DictionaryDataType;
import java.io.IOException;
import javax.annotation.PostConstruct;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharEncoding;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class used to create minimum data set. TODO Should be replaced by sql
 */
public class StaticDataLoader {


  private final DictionaryRepository dictionaryRepository;

  /**
   * Default constructor
   */
  public StaticDataLoader(DictionaryRepository dictionaryRepository) {
    this.dictionaryRepository = dictionaryRepository;
  }

  /**
   * Create dictionary data
   */
  @PostConstruct
  @Transactional
  public void create() throws IOException {
    dictionaryRepository.deleteAll();
    create(DictionaryDataType.CANDIDATE_INFORMATION_DISPLAY_MODELS, "Grand Conseil", "GE");
    create(DictionaryDataType.CANDIDATE_INFORMATION_DISPLAY_MODELS, "Conseil National", "GE");
    create(DictionaryDataType.COLUMN_FOR_VERIFICATION_CODE, "candidate-number", null);
    create(DictionaryDataType.COLUMN_FOR_VERIFICATION_CODE, "candidate-identity", null);
    create(DictionaryDataType.COLUMN_FOR_VERIFICATION_CODE, "candidate-position", null);
    create(DictionaryDataType.COLUMN_FOR_VERIFICATION_CODE, "verification-code", null);

    create(DictionaryDataType.VOTE_RECEIVER_DEFAULT_TRANSLATIONS,
           IOUtils.toString(StaticDataLoader.class.getResourceAsStream("/data/vote-receiver.i18n.ge.json"),
                            CharEncoding.UTF_8), "GE");

    create(DictionaryDataType.VOTE_RECEIVER_DEFAULT_TRANSLATIONS,
           IOUtils.toString(StaticDataLoader.class.getResourceAsStream("/data/vote-receiver.i18n.be.json"),
                            CharEncoding.UTF_8), "BE");

    create(DictionaryDataType.VOTE_RECEIVER_DEFAULT_TRANSLATIONS,
           IOUtils.toString(StaticDataLoader.class.getResourceAsStream("/data/vote-receiver.i18n.sg.json"),
                            CharEncoding.UTF_8), "SG");

    create(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, "true", "GE");
    create(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, "false", "AG");
    create(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, "true", "BE");
    create(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, "true", "BS");
    create(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, "true", "LU");
    create(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, "false", "SG");
    create(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, "true", "VD");


    create(DictionaryDataType.DEFAULT_LANGUAGE, "FR", "GE");
    create(DictionaryDataType.DEFAULT_LANGUAGE, "DE", "BE");
    create(DictionaryDataType.DEFAULT_LANGUAGE, "DE", "SG");


  }

  private void create(DictionaryDataType type, String content, String scope) {
    Dictionary dictionary = new Dictionary();
    dictionary.setContent(content);
    dictionary.setScope(scope);
    dictionary.setDataType(type);
    dictionaryRepository.save(dictionary);
  }


}
