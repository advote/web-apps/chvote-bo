/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.parameters;

import ch.ge.ve.bo.rest.controller.FileControllerUtils;
import ch.ge.ve.bo.rest.controller.TransferFileType;
import ch.ge.ve.bo.rest.services.FileUploadService;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import ch.ge.ve.bo.service.operation.parameters.repository.model.RepositoryFileVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.RepositoryImportResult;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller used to handle repository files linked to an operation.
 */
@RestController
@RequestMapping("/operation/repository")
public class OperationRepositoryController {

  private static final Logger logger = LoggerFactory.getLogger(OperationRepositoryController.class);

  private       OperationRepositoryService operationRepositoryService;
  private       FileService                fileService;
  private final FileUploadService          uploadService;

  /**
   * Default constructor.
   *
   * @param operationRepositoryService operation's repository service layer
   * @param fileService                Application File service
   * @param uploadService              service in charge of parsing http request for upload file
   */
  public OperationRepositoryController(OperationRepositoryService operationRepositoryService,
                                       FileService fileService,
                                       FileUploadService uploadService) {
    this.operationRepositoryService = operationRepositoryService;
    this.fileService = fileService;
    this.uploadService = uploadService;
  }

  /**
   * @return list of repository file related to an operation
   */
  @GetMapping(value = "/{operationId}")
  @PreAuthorize("hasRole('USER')")
  public List<RepositoryFileVo> getFiles(@PathVariable long operationId) {
    return operationRepositoryService.getFiles(operationId);
  }

  /**
   * @return list of DOI related to an operation
   */
  @GetMapping(value = "/{operationId}/get-related-dois")
  @PreAuthorize("hasRole('USER')")
  public Set<DomainOfInfluenceVo> getOperationDois(@PathVariable long operationId) {
    return operationRepositoryService.getAllDomainOfInfluenceForOperation(operationId);
  }

  /**
   * @return list of ballots related to an operation
   */
  @GetMapping(value = "/{operationId}/get-related-ballots")
  @PreAuthorize("hasRole('USER')")
  public Set<String> getOperationBallots(@PathVariable long operationId,
                                         @RequestParam("election") boolean election,
                                         @RequestParam("voting") boolean voting) {
    return operationRepositoryService.getAllBallotsForOperation(operationId, election, voting);
  }

  /**
   * add a repository file
   */
  @PostMapping(value = "{operationId}/upload")
  @PreAuthorize("hasRole('UPLOAD_REPOSITORY')")
  public RepositoryImportResult upload(@PathVariable long operationId,
                                       HttpServletRequest request) throws BusinessException {
    logger.debug("import operation repository");

    return uploadService.processFileUpload(
        request, "file",
        (fileName, stream) -> operationRepositoryService.importFile(operationId, fileName, stream)
    );
  }

  /**
   * replace a repository file in limited modification mode
   */
  @PostMapping(value = "{operationId}/replace/{oldId}")
  @PreAuthorize("hasRole('UPLOAD_REPOSITORY')")
  public RepositoryImportResult replace(@PathVariable long operationId, @PathVariable long oldId,
                                        HttpServletRequest request) throws BusinessException {
    logger.info("replace operation repository {}", oldId);
    return uploadService.processFileUpload(
        request, "file",
        (fileName, stream) -> operationRepositoryService.replaceFile(operationId, oldId, fileName, stream)
    );
  }


  /**
   * download a repository file
   */
  @GetMapping(value = "{repositoryFileId}/download")
  @PreAuthorize("hasRole('USER')")
  public void download(@PathVariable long repositoryFileId, HttpServletResponse response) throws IOException {
    logger.debug("downloading repository file ID {}", repositoryFileId);

    RepositoryFileVo file = operationRepositoryService.getFile(repositoryFileId);
    FileControllerUtils.sendFile(response, file.fileName, file.fileContent, TransferFileType.XML);
  }

  /**
   * delete a repository file
   */
  @DeleteMapping(value = "/{repositoryFileId}")
  @PreAuthorize("hasRole('DELETE_REPOSITORY')")
  public void delete(@PathVariable long repositoryFileId) throws BusinessException {
    fileService.deleteFile(repositoryFileId);
  }


}
