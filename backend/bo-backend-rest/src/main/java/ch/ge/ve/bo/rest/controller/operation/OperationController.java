/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.rest.model.SingleStringValue;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.OperationService;
import ch.ge.ve.bo.service.operation.OperationVotingMaterialService;
import ch.ge.ve.bo.service.operation.PrinterOperationService;
import ch.ge.ve.bo.service.operation.exception.PastDateException;
import ch.ge.ve.bo.service.operation.model.BaseConfiguration;
import ch.ge.ve.bo.service.operation.model.MilestoneConfiguration;
import ch.ge.ve.bo.service.operation.model.OperationVo;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller used to access electoral operations.
 */
@RestController
@RequestMapping("/operation")
public class OperationController {

  private final OperationService               operationService;
  private final PrinterOperationService        printerOperationService;
  private final OperationVotingMaterialService votingMaterialService;

  /**
   * default constructor
   */
  public OperationController(OperationService operationService, PrinterOperationService printerOperationService,
                             OperationVotingMaterialService votingMaterialService) {
    this.operationService = operationService;
    this.printerOperationService = printerOperationService;
    this.votingMaterialService = votingMaterialService;
  }


  /**
   * retrieve all operation that a user can access to.
   */
  @GetMapping(value = "/all")
  @PreAuthorize("hasRole('USER')")
  @SuppressWarnings("squid:S1452")
  public List<? extends OperationVo> findAll() {
    return ConnectedUser.get().roles.contains("ROLE_PRINTER") ?
        printerOperationService.findByPrinterManagementEntity() : operationService.findAll();
  }

  /**
   * Create an operation with base parameters
   */
  @PostMapping
  @PreAuthorize("hasRole('CREATE_OPERATION')")
  public long create(@RequestBody BaseConfiguration baseConfig) throws PastDateException {
    return operationService.create(baseConfig);
  }

  /**
   * update operation with base parameters
   */
  @PutMapping("/{id}/base")
  @PreAuthorize("hasAnyRole('UPDATE_BASE_PARAMETER')")
  public OperationVo updateBaseParameter(@PathVariable long id, @RequestBody BaseConfiguration baseConfig)
      throws BusinessException {
    return operationService.updateBaseConfiguration(id, baseConfig);
  }

  /**
   * update operation with milestone
   */
  @PutMapping("/{id}/milestones")
  @PreAuthorize("hasAnyRole('UPDATE_MILESTONE')")
  public OperationVo updateMilestones(@PathVariable long id, @RequestBody MilestoneConfiguration milestoneConfig)
      throws BusinessException {
    return operationService.updateMilestones(id, milestoneConfig);
  }

  /**
   * retrieve one operation configuration
   */
  @GetMapping(value = "/{id}")
  @PreAuthorize("hasRole('USER')")
  public OperationVo findOne(@PathVariable long id) {
    return operationService.findOne(id, false);
  }


  /**
   * update operation to target deployment in simulation
   */
  @PutMapping("/{id}/target-simulation")
  @PreAuthorize("hasRole('DEPLOYMENT_TARGET_SIMULATION')")
  public OperationVo targetSimulation(@PathVariable long id, @RequestBody String simulationName)
      throws BusinessException {
    return operationService.targetSimulation(id, simulationName);
  }

  /**
   * update operation to target deployment in real
   */
  @GetMapping(value = "/{id}/target-real")
  @PreAuthorize("hasRole('DEPLOYMENT_TARGET_REAL')")
  public OperationVo targetReal(@PathVariable long id) throws BusinessException {
    return operationService.targetReal(id);
  }

  /**
   * update operation to set voting card title
   */
  @PostMapping(value = "/{id}/voting-card-title")
  @PreAuthorize("hasRole('UPDATE_VOTING_CARD_TITLE')")
  public void updateVotingCardTitle(@PathVariable long id, @RequestBody SingleStringValue value) {
    votingMaterialService.updateVotingCardTitle(id, value.getValue());
  }

  /**
   * update operation to assign printer template
   */
  @PostMapping(value = "/{id}/printer-template")
  @PreAuthorize("hasRole('SELECT_PRINTER_TEMPLATE')")
  public void updatePrinterTemplate(@PathVariable long id, @RequestBody SingleStringValue value) {
    votingMaterialService.updatePrinterTemplate(id, value.getValue());
  }


}
