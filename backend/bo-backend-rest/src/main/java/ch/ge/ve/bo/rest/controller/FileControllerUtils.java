/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

/**
 * Common methods to handle file upload and download.
 */
public class FileControllerUtils {

  private FileControllerUtils() {
  }

  private static void prepareResponseHeaders(HttpServletResponse response, String filename, TransferFileType type) {
    response.setHeader("Content-Disposition", String.format("attachment; filename=%s", filename));
    response.setHeader("access-control-expose-headers", "content-disposition");
    response.setContentType(type.getContentType());
  }

  /**
   * send a file on the  {@link HttpServletResponse} adding appropriate headers
   */
  public static void sendFile(HttpServletResponse response, String fileName, byte[] fileContent, TransferFileType type)
      throws IOException {
    prepareResponseHeaders(response, fileName, type);
    OutputStream outputStream = response.getOutputStream();
    try {
      outputStream.write(fileContent);
      outputStream.flush();
    } finally {
      IOUtils.closeQuietly(outputStream);
    }
  }

}
