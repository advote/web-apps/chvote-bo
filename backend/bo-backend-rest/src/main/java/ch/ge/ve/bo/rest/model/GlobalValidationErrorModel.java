/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.model;

import java.util.List;

/**
 * DTO sent to frontend in case of global validation error
 */
public class GlobalValidationErrorModel {

  private final String messageKey;

  private final List<String> messageParams;

  /**
   * default constructor
   */
  public GlobalValidationErrorModel(String messageKey, List<String> messageParams) {
    this.messageKey = messageKey;
    this.messageParams = messageParams;
  }

  public String getMessageKey() {
    return messageKey;
  }

  public List<String> getMessageParams() {
    return messageParams;
  }
}
