/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Logging service used by frontend client
 */
@RestController
@RequestMapping("fo/logger")
public class FrontEndLoggerController {

  private static final Logger logger = LoggerFactory.getLogger("frontend");


  /**
   * Send a debug message from frontend
   */
  @PostMapping(value = "/debug")
  @PreAuthorize("hasRole('USER')")
  public void debug(@RequestBody String message) {
    logger.debug(message);
  }

  /**
   * Send am info message from frontend
   */
  @PostMapping(value = "/info")
  @PreAuthorize("hasRole('USER')")
  public void info(@RequestBody String message) {
    logger.info(message);
  }


  /**
   * Send a warning message from frontend
   */
  @PostMapping(value = "/warn")
  @PreAuthorize("hasRole('USER')")
  public void warn(@RequestBody String message) {
    logger.warn(message);
  }


  /**
   * Send an error message from frontend
   */
  @PostMapping(value = "/error")
  @PreAuthorize("hasRole('USER')")
  public void error(@RequestBody String message) {
    logger.error(message);
  }

}
