/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.parameters;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.rest.services.FileUploadService;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.parameters.document.OperationDocumentService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Controller used to handle general documentation files linked to an operation.
 */
@RestController
@RequestMapping("/operation/document")
public class OperationDocumentController {
  private final FileUploadService uploadService;

  private OperationDocumentService operationDocumentService;
  private FileService              fileService;

  /**
   * Default constructor.
   *
   * @param operationDocumentService operation's document service layer
   * @param fileService              application file's service layer
   */
  public OperationDocumentController(OperationDocumentService operationDocumentService,
                                     FileService fileService,
                                     FileUploadService uploadService) {
    this.operationDocumentService = operationDocumentService;
    this.fileService = fileService;
    this.uploadService = uploadService;
  }


  /**
   * list general documentation files
   */
  @GetMapping(value = "/{operationId}")
  @PreAuthorize("hasRole('USER')")
  public List<FileVo> getFiles(@PathVariable long operationId) {
    return operationDocumentService.getFiles(operationId);
  }

  /**
   * add a general documentation files
   */
  @PostMapping(value = "{operationId}/upload/{type}/{language}")
  @PreAuthorize("hasRole('SET_OPERATION_DOCUMENTATION')")
  public FileVo upload(@PathVariable long operationId,
                       @PathVariable ch.ge.ve.bo.FileType type,
                       @PathVariable Language language,
                       HttpServletRequest request) throws BusinessException {
    return uploadService.processFileUpload(request, "file", (fileName, inputStream) ->
        operationDocumentService.importFile(operationId, type, language, fileName, inputStream)
    );
  }

  /**
   * delete general documentation files
   */
  @DeleteMapping(value = "/{fileId}")
  @PreAuthorize("hasRole('DELETE_OPERATION_DOCUMENTATION')")
  public void deleteFile(@PathVariable long fileId) throws BusinessException {
    fileService.deleteFile(fileId);
  }
}
