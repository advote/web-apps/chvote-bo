/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.services;

import ch.ge.ve.bo.service.exception.BusinessException;
import java.io.InputStream;
import javax.servlet.http.HttpServletRequest;

/**
 * Service used to manage uploaded files
 */
public interface FileUploadService {

  /**
   * Process a single file. Can be called only once by request.
   *
   * @param request       the http request
   * @param fieldName     name of the field in the request
   * @param fileProcessor process file with filename and input stream
   * @param <T>           type of returned result
   *
   * @return the result of the processor
   */
  <T> T processFileUpload(HttpServletRequest request, String fieldName,
                          FileFunction<T> fileProcessor) throws BusinessException;

  @FunctionalInterface
  interface FileFunction<T> {
    /**
     * transform a file upload into required type
     */
    T apply(String fileName, InputStream stream) throws BusinessException;
  }
}
