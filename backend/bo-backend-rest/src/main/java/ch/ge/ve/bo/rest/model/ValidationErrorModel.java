/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.model;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO sent to frontend in case of business validation error (field validation error, or business rule validation
 * error)
 */
public class ValidationErrorModel {

  private final String exceptionClassName;

  /**
   * Global errors
   */
  private List<GlobalValidationErrorModel> globalErrors = new ArrayList<>();

  /**
   * Field validation errors
   */
  private List<FieldValidationErrorModel> fieldErrors = new ArrayList<>();

  /**
   * default with no cause exception
   */
  public ValidationErrorModel() {
    this(null);
  }

  /**
   * default constructor
   */
  public ValidationErrorModel(String exceptionClassName) {
    this.exceptionClassName = exceptionClassName;
  }

  /**
   * Add a global error
   */
  public void addGlobalError(GlobalValidationErrorModel globalError) {
    globalErrors.add(globalError);
  }

  /**
   * Add a field error
   */
  public void addFieldError(FieldValidationErrorModel fieldError) {
    fieldErrors.add(fieldError);
  }

  public List<FieldValidationErrorModel> getFieldErrors() {
    return fieldErrors;
  }

  public List<GlobalValidationErrorModel> getGlobalErrors() {
    return globalErrors;
  }

  public String getExceptionClassName() {
    return exceptionClassName;
  }
}
