/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.parameters;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.rest.controller.FileControllerUtils;
import ch.ge.ve.bo.rest.controller.TransferFileType;
import ch.ge.ve.bo.rest.services.FileUploadService;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.ContentAndFileName;
import ch.ge.ve.bo.service.operation.model.BallotDocumentationVo;
import ch.ge.ve.bo.service.operation.parameters.document.BallotDocumentationService;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Ballot documentation controller
 */
@RestController
@RequestMapping("operation/{operationId}/ballot-documentation")
public class BallotDocumentationController {
  private final BallotDocumentationService ballotDocumentationService;
  private final FileUploadService          uploadService;

  /**
   * Default controller
   */
  public BallotDocumentationController(BallotDocumentationService ballotDocumentationService,
                                       FileUploadService uploadService) {
    this.ballotDocumentationService = ballotDocumentationService;
    this.uploadService = uploadService;
  }

  /**
   * @return all {@link BallotDocumentationVo} for an operation
   */
  @GetMapping
  @PreAuthorize("hasRole('USER')")
  public List<BallotDocumentationVo> findByOperation(@PathVariable("operationId") Long operationId) {
    return ballotDocumentationService.findByOperation(operationId);
  }

  /**
   * download a {@link BallotDocumentationVo}
   */
  @GetMapping(value = "/download/{ballotDocumentationId}")
  @PreAuthorize("hasRole('USER')")
  public void download(@PathVariable("ballotDocumentationId") long ballotDocumentationId,
                       HttpServletResponse response) throws IOException {
    ContentAndFileName contentAndFileName = ballotDocumentationService.getFileContent(ballotDocumentationId);
    FileControllerUtils.sendFile(response,
                                 contentAndFileName.getFileName(),
                                 contentAndFileName.getContent(),
                                 TransferFileType.PDF);
  }


  /**
   * Add a {@link BallotDocumentationVo} to an operation
   */
  @PostMapping
  @PreAuthorize("hasRole('ADD_BALLOT_DOCUMENTATION')")
  public BallotDocumentationVo addBallotDocumentation(@PathVariable("operationId") Long operationId,
                                                      @RequestParam("ballotId") String ballotId,
                                                      @RequestParam("language") Language language,
                                                      @RequestParam("localizedLabel") String localizedLabel,
                                                      HttpServletRequest request) throws BusinessException {
    return uploadService.processFileUpload(
        request, "file", (fileName, inputStream) -> {
          try {
            return ballotDocumentationService.addBallotDocumentation(
                operationId, ballotId, language, localizedLabel, fileName, IOUtils.toByteArray(inputStream));
          } catch (IOException e) {
            throw new TechnicalException(e);
          }
        }
    );
  }


  /**
   * delete a {@link BallotDocumentationVo}
   */
  @DeleteMapping(value = "{ballotDocumentationId}")
  @PreAuthorize("hasRole('DELETE_BALLOT_DOCUMENTATION')")
  public void deleteDocumentation(@PathVariable("ballotDocumentationId") Long ballotDocumentationId) {
    ballotDocumentationService.deleteBallotDocumentation(ballotDocumentationId);
  }


}
