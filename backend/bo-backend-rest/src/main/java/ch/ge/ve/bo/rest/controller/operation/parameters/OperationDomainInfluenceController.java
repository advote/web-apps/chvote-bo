/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.parameters;

import ch.ge.ve.bo.rest.controller.FileControllerUtils;
import ch.ge.ve.bo.rest.controller.TransferFileType;
import ch.ge.ve.bo.rest.services.FileUploadService;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceService;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceFileVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceImportResult;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller used to handle domain of influence file linked to an operation.
 */
@RestController
@RequestMapping("/operation/doi")
public class OperationDomainInfluenceController {

  private static final Logger logger = LoggerFactory.getLogger(OperationDomainInfluenceController.class);

  private final OperationDomainInfluenceService operationDomainInfluenceService;
  private final FileService                     fileService;
  private final FileUploadService               uploadService;

  /**
   * Default constructor.
   *
   * @param operationDomainInfluenceService operation's domain of influence service layer
   * @param fileService                     file's service layer
   * @param uploadService                   upload service
   */
  public OperationDomainInfluenceController(
      OperationDomainInfluenceService operationDomainInfluenceService,
      FileService fileService,
      FileUploadService uploadService) {
    this.operationDomainInfluenceService = operationDomainInfluenceService;
    this.fileService = fileService;
    this.uploadService = uploadService;
  }

  /**
   * @return a domain of influence file metadata
   */
  @GetMapping(value = "/{operationId}")
  @PreAuthorize("hasRole('USER')")
  public DomainOfInfluenceFileVo getFile(@PathVariable long operationId) {
    return operationDomainInfluenceService.getFile(operationId, false);
  }

  /**
   * upload a domain of influence file
   */
  @PostMapping(value = "{operationId}/upload")
  @PreAuthorize("hasRole('UPLOAD_DOI')")
  public DomainOfInfluenceImportResult upload(@PathVariable long operationId,
                                              HttpServletRequest request) throws BusinessException {
    logger.debug("import operation domain of influence file");
    return uploadService.processFileUpload(
        request, "file",
        (fileName, stream) -> operationDomainInfluenceService.importFile(operationId, fileName, stream));
  }

  /**
   * download content of a domain of influence file
   */
  @GetMapping(value = "{operationId}/download")
  @PreAuthorize("hasRole('USER')")
  public void download(@PathVariable long operationId, HttpServletResponse response) throws IOException {
    logger.debug("downloading domain of influence file for operation ID {}", operationId);

    DomainOfInfluenceFileVo file = operationDomainInfluenceService.getFile(operationId, true);
    FileControllerUtils.sendFile(response, file.fileName, file.fileContent, TransferFileType.XML);
  }


  /**
   * delete a domain of influence file
   */
  @DeleteMapping(value = "/{fileId}")
  @PreAuthorize("hasRole('DELETE_DOI')")
  public void delete(@PathVariable long fileId) throws BusinessException {
    fileService.deleteFile(fileId);
  }
}
