/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.testing.cards;

import ch.ge.ve.bo.service.model.VoterTestingCardsLotVo;
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for Voter Testing Cards Lot related request
 */
@RestController
@RequestMapping("/voter-testing-cards-lot")
public class VoterTestingCardsLotController {

  private final VoterTestingCardsLotService voterTestingCardsLotService;

  /**
   * Main constructor
   *
   * @param voterTestingCardsLotService Business service
   */
  public VoterTestingCardsLotController(VoterTestingCardsLotService voterTestingCardsLotService) {
    this.voterTestingCardsLotService = voterTestingCardsLotService;
  }

  /**
   * retrieve all voter testing card lot for an operation
   */
  @GetMapping
  @PreAuthorize("hasRole('USER')")
  public List<VoterTestingCardsLotVo> findAll(@RequestParam("operationId") long operationId,
                                              @RequestParam("forConfiguration") boolean forConfiguration) {
    return voterTestingCardsLotService.findAll(operationId, forConfiguration);
  }

  /**
   * Create a voter testing card lot on an operation
   */
  @PostMapping
  @PreAuthorize("hasRole('CREATE_TEST_VOTING_CARD_DEFINITION')")
  public VoterTestingCardsLotVo create(@RequestBody VoterTestingCardsLotVo voterTestingCardsLot,
                                       @RequestParam("operationId") long operationId) {
    return voterTestingCardsLotService.create(operationId, voterTestingCardsLot);
  }

  /**
   * edit an existing voter testing card lot
   */
  @PutMapping
  @PreAuthorize("hasRole('EDIT_TEST_VOTING_CARD_DEFINITION')")
  public VoterTestingCardsLotVo edit(@RequestBody VoterTestingCardsLotVo voterTestingCardsLot) {
    return voterTestingCardsLotService.edit(voterTestingCardsLot);
  }

  /**
   * find one voter testing card lot by id
   */
  @GetMapping(value = "/{id}")
  @PreAuthorize("hasRole('USER')")
  public VoterTestingCardsLotVo findOne(@PathVariable long id) {
    return voterTestingCardsLotService.findOne(id);
  }

  /**
   * delete one voter testing card lot by id
   */
  @DeleteMapping(value = "/{id}")
  @PreAuthorize("hasRole('DELETE_TEST_VOTING_CARD_DEFINITION')")
  public boolean delete(@PathVariable long id) {
    return voterTestingCardsLotService.delete(id);
  }

}
