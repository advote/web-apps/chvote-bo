/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest;

import static java.util.stream.Collectors.toList;

import ch.ge.ve.bo.repository.exception.AccessToProhibitedOperationException;
import ch.ge.ve.bo.rest.exception.StringValidationException;
import ch.ge.ve.bo.rest.model.FieldValidationErrorModel;
import ch.ge.ve.bo.rest.model.GlobalValidationErrorModel;
import ch.ge.ve.bo.rest.model.TechnicalErrorModel;
import ch.ge.ve.bo.rest.model.ValidationErrorModel;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.FieldsValidationException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Intercepts business validation errors, field validation errors and technical errors and translate them to HTTP status
 * code and DTO payload for interpretation by frontend application
 */
@ControllerAdvice
public class RestErrorHandler {

  private static final Logger exceptionLogger                  = LoggerFactory.getLogger("bo-exceptions");
  private static final Logger logger                           = LoggerFactory.getLogger(RestErrorHandler.class);
  private static final String ERROR_STRING_INVALID_MESSAGE_KEY = "global.error.string-invalid";
  private static final String ERROR_UNEXPECTED_MESSAGE_KEY     = "global.errors.unexpected";

  /**
   * Default constructor
   */
  public RestErrorHandler() {
    logger.info("Rest Error Handler");
  }


  /**
   * Handler for {@link MethodArgumentNotValidException}
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationErrorModel processBeanValidationError(MethodArgumentNotValidException ex) {
    exceptionLogger.warn("BeanValidationError", ex);
    ValidationErrorModel validationErrorModel = new ValidationErrorModel();

    BindingResult result = ex.getBindingResult();

    processFieldErrors(result.getFieldErrors(), validationErrorModel);
    processGlobalErrors(result.getGlobalErrors(), validationErrorModel);

    return validationErrorModel;
  }

  private void processFieldErrors(List<FieldError> fieldErrors, ValidationErrorModel validationErrorModel) {
    for (FieldError fieldError : fieldErrors) {
      validationErrorModel.addFieldError(
          new FieldValidationErrorModel(
              fieldError.getField(),
              fieldError.getDefaultMessage(),
              Collections.singletonList(Objects.requireNonNull(fieldError.getRejectedValue()).toString())
          ));
    }
  }

  private void processGlobalErrors(List<ObjectError> objectErrors, ValidationErrorModel validationErrorModel) {
    for (ObjectError objectError : objectErrors) {
      validationErrorModel.addGlobalError(new GlobalValidationErrorModel(objectError.getDefaultMessage(),
                                                                         paramsObjectsToStringList(
                                                                             objectError.getArguments())));
    }
  }

  /**
   * Handler for {@link AccessToProhibitedOperationException}
   */
  @ExceptionHandler(AccessToProhibitedOperationException.class)
  @ResponseStatus(HttpStatus.FORBIDDEN)
  @ResponseBody
  public void processFieldValidationError(AccessToProhibitedOperationException ex) {
    exceptionLogger.error("Access error", ex);
  }

  /**
   * Handler for {@link FieldsValidationException}
   */
  @ExceptionHandler(FieldsValidationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationErrorModel processFieldValidationError(FieldsValidationException ex) {
    exceptionLogger.error("FieldValidationError", ex);
    ValidationErrorModel validationErrorModel = new ValidationErrorModel(ex.getClass().getName());

    for (String field : ex.getFieldsErrorMessages().keySet()) {
      validationErrorModel.addFieldError(
          new FieldValidationErrorModel(
              field,
              ex.getFieldsErrorMessages().get(field),
              ex.getFieldsErrorParameters() != null ? paramsObjectsToStringList(
                  ex.getFieldsErrorParameters().get(field)) : null
          )
      );
    }
    return validationErrorModel;
  }


  /**
   * Handler for {@link BusinessException}
   */
  @ExceptionHandler(BusinessException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationErrorModel processBusinessValidationError(BusinessException ex) {
    exceptionLogger.error("BusinessValidationError", ex);
    ValidationErrorModel validationErrorModel = new ValidationErrorModel(ex.getClass().getName());
    GlobalValidationErrorModel globalError =
        new GlobalValidationErrorModel(ex.getMessageKey(), paramsObjectsToStringList(ex.getParameters()));
    validationErrorModel.addGlobalError(globalError);
    return validationErrorModel;
  }

  private List<String> paramsObjectsToStringList(Object[] parameters) {
    return parameters != null ? Arrays.stream(parameters).map(Object::toString).collect(toList()) : null;
  }

  /**
   * Handler for {@link StringValidationException}
   */
  @ExceptionHandler(StringValidationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationErrorModel processStringValidationError(StringValidationException ex) {
    exceptionLogger.error("StringValidationError", ex);
    ValidationErrorModel validationErrorModel = new ValidationErrorModel();
    validationErrorModel.addFieldError(new FieldValidationErrorModel(ex.getField(), ERROR_STRING_INVALID_MESSAGE_KEY));
    return validationErrorModel;
  }

  /**
   * Handler for all others exception
   */
  @ExceptionHandler(Throwable.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public TechnicalErrorModel processRuntimeError(Throwable t) {
    exceptionLogger.error("RuntimeError", t);
    return new TechnicalErrorModel(HttpStatus.INTERNAL_SERVER_ERROR.value(), t.getClass().getName(),
                                   ERROR_UNEXPECTED_MESSAGE_KEY);
  }

}
