/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation.voting.period;

import ch.ge.ve.bo.service.operation.voting.period.VotingSitePeriodService;
import ch.ge.ve.bo.service.operation.voting.period.model.VotingSitePeriodVO;
import java.time.LocalDateTime;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller used for define {@link VotingSitePeriodVO} in simulation
 */
@RestController
@RequestMapping("/operation/{operationId}/voting-site-period")
public class VotingSitePeriodController {

  private final VotingSitePeriodService votingSitePeriodService;

  /**
   * Default constructor
   */
  public VotingSitePeriodController(VotingSitePeriodService votingSitePeriodService) {
    this.votingSitePeriodService = votingSitePeriodService;
  }

  /**
   * Save or update {@link VotingSitePeriodVO} in simulation
   */
  @PostMapping
  @PreAuthorize("hasRole('SELECT_SIMULATION_PERIOD')")
  public VotingSitePeriodVO saveOrUpdate(@PathVariable("operationId") long operationId,
                                         @RequestBody VotingSitePeriodChange change) {
    return votingSitePeriodService.saveOrUpdate(operationId, change.dateOpen, change.dateClose, change.gracePeriod);
  }

  /**
   * return {@link VotingSitePeriodVO} for an operation
   */
  @GetMapping
  @PreAuthorize("hasRole('USER')")
  public VotingSitePeriodVO findForOperation(@PathVariable("operationId") long operationId) {
    return votingSitePeriodService.findForOperation(operationId).orElse(null);
  }

  public static class VotingSitePeriodChange {

    private LocalDateTime dateOpen;
    private LocalDateTime dateClose;
    private int           gracePeriod;

    public void setDateOpen(LocalDateTime dateOpen) {
      this.dateOpen = dateOpen;
    }

    public void setDateClose(LocalDateTime dateClose) {
      this.dateClose = dateClose;
    }

    public void setGracePeriod(int gracePeriod) {
      this.gracePeriod = gracePeriod;
    }
  }

}
