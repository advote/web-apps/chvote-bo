/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.upload.OperationUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller relative to upload
 */
@RestController
@RequestMapping("/operation/{operationId}")
public class OperationUploadController {


  private OperationUploadService operationUploadService;

  /**
   * default constructor
   */
  @Autowired
  public OperationUploadController(OperationUploadService operationUploadService) {
    this.operationUploadService = operationUploadService;
  }

  /**
   * Upload configuration to pact
   */
  @GetMapping(value = "/configuration/upload")
  @PreAuthorize("hasRole('DEPLOY_TEST_SITE')")
  public void uploadConfigurationToPact(@PathVariable long operationId) throws BusinessException {
    operationUploadService.uploadConfigurationToPact(operationId);
  }

  /**
   * upload voting material configuration to pact
   */
  @GetMapping(value = "/voting-materials/upload")
  @PreAuthorize("hasRole('DEPLOY_VOTING_MATERIAL')")
  public void uploadVotingMaterialToPact(@PathVariable long operationId) throws PactRequestException {
    operationUploadService.uploadVotingMaterialToPact(operationId);
  }


  /**
   * upload voting period configuration to pact
   */
  @GetMapping(value = "/voting-period/upload")
  @PreAuthorize("hasRole('DEPLOY_VOTING_PERIOD')")
  public void uploadVotingPeriodToPact(@PathVariable long operationId) throws PactRequestException {
    operationUploadService.uploadVotingPeriodToPact(operationId);
  }


}
