/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest;

import ch.ge.ve.bo.repository.DictionaryRepository;
import ch.ge.ve.bo.service.conf.ServiceConfiguration;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer;
import ch.ge.ve.jacksonserializer.JSDates;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.IOException;
import java.sql.SQLException;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * BO backend bootstrap.
 */
@SpringBootApplication
@EnableScheduling
@Import({ServiceConfiguration.class, SwaggerConfig.class})
public class BoBackendApplication {

  private static final Logger logger = LoggerFactory.getLogger(BoBackendApplication.class);

  /**
   * Main runner
   */
  public static void main(String[] args) {
    logger.info("BoBackendApplication starting...");
    SpringApplication.run(BoBackendApplication.class, args);
  }

  @Bean
  @Profile({"development"})
  WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        logger.info("Configuring CORS");
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("GET", "POST", "OPTIONS", "PUT", "DELETE")
                .allowedHeaders("*")
                .exposedHeaders("Access-Control-Allow-Origin", "Access-Control-Allow-Credentials")
                .allowCredentials(true)
                .maxAge(3600);
      }
    };
  }

  @Bean
  Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
    return builder -> {
      builder.serializationInclusion(JsonInclude.Include.NON_NULL);
      builder.serializers(JSDates.SERIALIZER, new BigIntegerAsBase64Serializer());
      builder.deserializers(JSDates.DESERIALIZER, new BigIntegerAsBase64Deserializer());
    };
  }

  @Bean
  StaticDataLoader staticDataLoader(DictionaryRepository dictionaryRepository) {
    return new StaticDataLoader(dictionaryRepository);
  }

  @Bean
  ServletFileUpload servletFileUpload() {
    return new ServletFileUpload();
  }

  @Value("${env.timezone}")
  private String timeZone;

  @PostConstruct
  void setDefaultTimezone() {
    TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
  }

  @Bean
  Filter addNoCacheFilter() {
    return new OncePerRequestFilter() {
      @Override
      protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
          throws ServletException, IOException {
        response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.addHeader("pragma", "no-cache");
        filterChain.doFilter(request, response);
      }
    };
  }

  @Bean
  DynamicResourceHttpRequestHandler dynamicResourceHttpRequestHandler() {
    return new DynamicResourceHttpRequestHandler();
  }

  /**
   * @return the created server
   *
   * @throws SQLException if we cannot connect
   */
  @Bean(initMethod = "start", destroyMethod = "stop")
  @Profile({"development", "integration"})
  public Server h2Server() throws SQLException {
    String dbPort = "9757";
    logger.info("Starting H2 database server on port {} ...", dbPort);
    return Server.createTcpServer("-web", "-tcp", "-tcpAllowOthers", "-tcpPort", dbPort);
  }

}
