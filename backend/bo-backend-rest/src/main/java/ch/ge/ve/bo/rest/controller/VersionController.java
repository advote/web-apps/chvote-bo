/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller used to display version number on website
 */
@RestController
@RequestMapping("/version")
public class VersionController {

  private final VersionVo currentVersion;

  /**
   * Default constructor
   */
  @Autowired
  public VersionController(@Value("${env.buildNumber}") String buildNumber,
                           @Value("${env.buildTimestamp}") String buildTimestamp) {
    this.currentVersion = new VersionVo(buildNumber, buildTimestamp);
  }

  /**
   * return current connected user information
   */
  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('USER')")
  public VersionVo getCurrentVersion() {
    return currentVersion;
  }


  @SuppressWarnings("WeakerAccess") /* JSON Serialized */
  public class VersionVo {
    public final String        buildNumber;
    public final LocalDateTime buildTimestamp;

    @SuppressWarnings("squid:S1166")
    private VersionVo(String buildNumber, String buildTimestamp) {
      this.buildNumber = buildNumber.startsWith("@") ? "DEV" : buildNumber;

      if (buildTimestamp.startsWith("@")) {
        this.buildTimestamp = LocalDateTime.now();
      } else {
        LocalDateTime localDateTime;
        try {
          localDateTime = LocalDateTime.parse(buildTimestamp, DateTimeFormatter.ISO_DATE_TIME);
        } catch (Exception e) {
          localDateTime = LocalDateTime.parse(buildTimestamp, DateTimeFormatter.ofPattern("yyyyMMdd-HHmm"));
        }
        this.buildTimestamp = localDateTime;
      }
    }
  }

}
