/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.status.VotingCardDeliveryService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller used to retrieve generated voting materials.
 */
@RestController
@RequestMapping("operation/{operationId}/")
public class GeneratedVotingMaterialController {
  private final VotingCardDeliveryService votingCardDeliveryService;


  /**
   * default constructor
   */
  public GeneratedVotingMaterialController(VotingCardDeliveryService votingCardDeliveryService) {
    this.votingCardDeliveryService = votingCardDeliveryService;
  }

  /**
   * @return testing card for test site
   */
  @GetMapping(path = "voting-cards/test-site")
  @PreAuthorize("hasRole('DOWNLOAD_GENERATED_CARDS_FOR_TEST_SITE')")
  public ResponseEntity<ByteArrayResource> getGeneratedCardsForTestSite(@PathVariable long operationId)
      throws BusinessException {

    FileVo file = votingCardDeliveryService.getGeneratedCardsForTestSite(operationId);
    byte[] bytes = file.fileContent;
    return getByteArrayResourceResponseEntity(file, bytes);
  }

  /**
   * @return not printable testing card for production site
   */
  @GetMapping(path = "voting-cards/not-printable/production")
  @PreAuthorize("hasRole('DOWNLOAD_GENERATED_CARDS_FOR_TEST_SITE')")
  public ResponseEntity<ByteArrayResource> getGeneratedNonPrintableCardsForProduction(@PathVariable long operationId)
      throws BusinessException {

    final FileVo file = votingCardDeliveryService.getGeneratedNonPrintableCardsForProduction(operationId);
    final byte[] bytes = file.fileContent;

    return getByteArrayResourceResponseEntity(file, bytes);
  }


  private ResponseEntity<ByteArrayResource> getByteArrayResourceResponseEntity(FileVo file, byte[] bytes) {
    ByteArrayResource resource = new ByteArrayResource(bytes);
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + file.fileName);
    return ResponseEntity.ok()
                         .headers(headers)
                         .contentLength(bytes.length)
                         .contentType(MediaType.APPLICATION_OCTET_STREAM)
                         .body(resource);
  }

}
