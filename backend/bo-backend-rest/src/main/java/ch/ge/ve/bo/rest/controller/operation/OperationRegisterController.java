/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller.operation;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import ch.ge.ve.bo.rest.controller.FileControllerUtils;
import ch.ge.ve.bo.rest.controller.TransferFileType;
import ch.ge.ve.bo.rest.services.FileUploadService;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.register.RegisterImportService;
import ch.ge.ve.bo.service.operation.register.model.ImportRegisterResult;
import java.io.IOException;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for Operation register related request
 */
@RestController
@RequestMapping("/operation/register/{operationId}")
public class OperationRegisterController {

  private final RegisterImportService registryImportService;
  private final FileUploadService     uploadService;

  /**
   * Main constructor
   *
   * @param registryImportService Business service
   * @param uploadService         service in charge of parsing http request for upload file
   */
  public OperationRegisterController(RegisterImportService registryImportService, FileUploadService uploadService) {
    this.registryImportService = registryImportService;
    this.uploadService = uploadService;
  }

  /**
   * Upload a register
   */
  @ResponseBody
  @PostMapping(value = "/upload")
  @PreAuthorize("hasRole('UPLOAD_REGISTER_FILE')")
  public ImportRegisterResult upload(@PathVariable long operationId, HttpServletRequest request)
      throws BusinessException {
    return uploadService.processFileUpload(
        request,
        "file",
        (fileName, stream) -> registryImportService.importRegister(operationId, fileName, stream));
  }

  /**
   * Validate an uploaded register
   */
  @ResponseBody
  @RequestMapping(value = "/validate-upload", method = GET)
  @PreAuthorize("hasRole('UPLOAD_REGISTER_FILE')")
  public void validate(@PathVariable long operationId, @PathParam("messageUniqueId") String messageUniqueId)
      throws BusinessException {
    registryImportService.validateImport(operationId, messageUniqueId);
  }


  /**
   * Delete a register file
   */
  @ResponseBody
  @DeleteMapping
  @PreAuthorize("hasRole('DELETE_REGISTER_FILE')")
  public void delete(@PathVariable long operationId, @PathParam("messageUniqueId") String messageUniqueId) {
    registryImportService.deleteImport(operationId, messageUniqueId);
  }


  /**
   * Create a report of all imported registers
   */
  @ResponseBody
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @PreAuthorize("hasRole('USER')")
  public String getReports(@PathVariable long operationId) {
    return registryImportService.getReports(operationId).stream()
                                .collect(Collectors.joining(",", "[", "]"));
  }

  /**
   * Create a detailed report on an imported register
   */
  @ResponseBody
  @GetMapping(value = "/detailedReport")
  @PreAuthorize("hasRole('GET_REGISTER_REPORT')")
  public void downloadDetailedReport(@PathVariable long operationId,
                                     @PathParam("messageUniqueId") String messageUniqueId,
                                     HttpServletResponse response) throws IOException {
    FileVo detailedReportPDF = registryImportService.getDetailedReportPDF(operationId, messageUniqueId);
    FileControllerUtils
        .sendFile(response, detailedReportPDF.fileName, detailedReportPDF.fileContent, TransferFileType.PDF);
  }


  /**
   * Create a detailed report of all imported registers
   */
  @ResponseBody
  @GetMapping(value = "/detailedReport/consolidatedDetailedReport")
  @PreAuthorize("hasRole('GET_REGISTER_REPORT')")
  public void downloadDetailedConsolidatedReport(@PathVariable long operationId,
                                                 HttpServletResponse response) throws IOException {
    FileVo detailedReportPDF = registryImportService.getDetailedReportPDF(operationId);
    FileControllerUtils
        .sendFile(response, detailedReportPDF.fileName, detailedReportPDF.fileContent, TransferFileType.PDF);
  }
}
