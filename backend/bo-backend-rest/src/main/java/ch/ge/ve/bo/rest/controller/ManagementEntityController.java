/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller;

import ch.ge.ve.bo.rest.controller.model.ManagementEntityUpdate;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.OperationService;
import ch.ge.ve.bo.service.operation.management.entity.ManagementEntityService;
import ch.ge.ve.bo.service.operation.model.OperationVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to invite and revoke management entity on an operation
 */
@RestController
@RequestMapping("management-entity")
public class ManagementEntityController {

  private final ManagementEntityService managementEntityService;
  private final OperationService        operationService;

  /**
   * Default controller
   */
  public ManagementEntityController(ManagementEntityService managementEntityService,
                                    OperationService operationService) {
    this.managementEntityService = managementEntityService;
    this.operationService = operationService;
  }

  @GetMapping(path = "/all")
  @PreAuthorize("hasRole('USER')")
  public String[] getAllManagementEntities() {
    return managementEntityService.getAllManagementEntities();
  }

  /**
   * Invite one or many management entity
   */
  @PutMapping(path = "/invite-to-operation")
  @PreAuthorize("hasRole('INVITE_MANAGEMENT_ENTITY')")
  public OperationVo invite(@RequestBody ManagementEntityUpdate update) {
    managementEntityService.invite(update.getOperationId(), update.getManagementEntities());
    return operationService.findOne(update.getOperationId(), false);
  }

  /**
   * revoke one or many management entity
   */
  @PutMapping(path = "/revoke-from-operation")
  @PreAuthorize("hasRole('REVOKE_MANAGEMENT_ENTITY')")
  public OperationVo revoke(@RequestBody ManagementEntityUpdate update) throws BusinessException {
    managementEntityService.revoke(update.getOperationId(), update.getManagementEntities());
    return operationService.findOne(update.getOperationId(), false);
  }
}
