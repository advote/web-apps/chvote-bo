/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller;


import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.entity.DictionaryDataType;
import ch.ge.ve.bo.service.dictionnary.DictionaryService;
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller to access dictionary entry
 */
@RestController
public class DictionaryController {

  private final PrinterTemplateService printerTemplateService;
  private final DictionaryService      dictionaryService;


  /**
   * Default constructor
   */
  public DictionaryController(PrinterTemplateService printerTemplateService, DictionaryService dictionaryService) {
    this.printerTemplateService = printerTemplateService;
    this.dictionaryService = dictionaryService;
  }

  /**
   * @return All printer templates for connected user realm
   */
  @GetMapping(path = "printer-template/all")
  @PreAuthorize("hasRole('USER')")
  public PrinterTemplateVo[] getAllPrinterTemplate() {
    return printerTemplateService.getAllPrinterTemplates(ConnectedUser.get().realm);
  }

  /**
   * @return All available display models for election site
   */
  @GetMapping(path = "{operationId}/candidate-information-display-models")
  @PreAuthorize("hasRole('USER')")
  public List<String> getCandidateInformationDisplayModel(@PathVariable long operationId) {
    return this.dictionaryService
        .get(DictionaryDataType.CANDIDATE_INFORMATION_DISPLAY_MODELS, String.class, operationId);
  }

  /**
   * @return all columns to display on verification code table in the election site
   */
  @GetMapping(path = "{operationId}/columns-for-verification-codes")
  @PreAuthorize("hasRole('USER')")
  public List<String> getColumnsForVerificationCodes(@PathVariable long operationId) {
    return this.dictionaryService.get(DictionaryDataType.COLUMN_FOR_VERIFICATION_CODE, String.class, operationId);
  }

  /**
   * @return default language for the connected user realm
   */
  @GetMapping(path = "{operationId}/default-lang")
  @PreAuthorize("hasRole('USER')")
  public String defaultLanguage(@PathVariable long operationId) {
    return this.dictionaryService.get(DictionaryDataType.DEFAULT_LANGUAGE, String.class, operationId)
                                 .stream()
                                 .findFirst()
                                 .orElse(Language.FR.name());
  }


}
