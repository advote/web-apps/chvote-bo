/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest

import ch.ge.ve.bo.rest.exception.StringValidationException
import ch.ge.ve.bo.rest.model.TechnicalErrorModel
import ch.ge.ve.bo.rest.model.ValidationErrorModel
import ch.ge.ve.bo.service.exception.BusinessException
import ch.ge.ve.bo.service.exception.FieldsValidationException
import ch.ge.ve.bo.service.exception.TechnicalException
import ch.ge.ve.bo.service.file.exception.FileAlreadyImportedException
import java.lang.reflect.Method
import org.springframework.core.MethodParameter
import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError
import org.springframework.web.bind.MethodArgumentNotValidException
import spock.lang.Specification

/**
 * Unit tests for the REST error handler.
 */
class RestErrorHandlerTest extends Specification {

    RestErrorHandler errorHandler = new RestErrorHandler()

    def 'TechnicalException should be mapped to a TechnicalModel'() {
        given:
        Throwable throwable = new TechnicalException("An error")

        when:
        TechnicalErrorModel model = errorHandler.processRuntimeError(throwable)

        then:
        model.httpStatus == 500
        model.message == 'global.errors.unexpected'
    }

    def 'StringValidationException should be mapped to a ValidationErrorModel'() {
        given:
        StringValidationException exception = new StringValidationException('', 'field')

        when:
        ValidationErrorModel model = errorHandler.processStringValidationError(exception)

        then:
        model.getExceptionClassName() == null
        model.getGlobalErrors() == []
        model.getFieldErrors() != null
        model.getFieldErrors().size() == 1
        model.getFieldErrors().get(0).getField() == 'field'
        model.getFieldErrors().get(0).getMessageKey() == 'global.error.string-invalid'
    }

    def 'BusinessException should be mapped to a ValidationErrorModel'() {
        given:
        BusinessException exception = new FileAlreadyImportedException('14.06.1978', 'TEST', '201709VP')

        when:
        ValidationErrorModel model = errorHandler.processBusinessValidationError(exception)

        then:
        model.getExceptionClassName() == 'ch.ge.ve.bo.service.file.exception.FileAlreadyImportedException'
        model.getFieldErrors() == []
        model.getGlobalErrors() != null
        model.getGlobalErrors().size() == 1
        model.getGlobalErrors().get(0).getMessageKey() == 'global.errors.file-already-imported'
        model.getGlobalErrors().get(0).getMessageParams() == ['14.06.1978', 'TEST', '201709VP']
    }

    def 'FieldsValidationException should be mapped to a ValidationErrorModel'() {
        given:
        Map<String, String> fieldsMessage = ['field1': 'message1', 'field2': 'message2']
        Map<String, String[]> fieldsParameters = ['field1': ['parameter1', 'parameter2'] as String[]]
        FieldsValidationException exception = new FieldsValidationException(fieldsMessage, fieldsParameters)

        when:
        ValidationErrorModel model = errorHandler.processFieldValidationError(exception)

        then:
        model.getExceptionClassName() == 'ch.ge.ve.bo.service.exception.FieldsValidationException'
        model.getGlobalErrors() == []
        model.getFieldErrors() != null
        model.getFieldErrors().size() == 2
        model.getFieldErrors().get(0).getField() == 'field1'
        model.getFieldErrors().get(0).getMessageKey() == 'message1'
        model.getFieldErrors().get(0).getMessageParams() == ['parameter1', 'parameter2']
        model.getFieldErrors().get(1).getField() == 'field2'
        model.getFieldErrors().get(1).getMessageKey() == 'message2'
        model.getFieldErrors().get(1).getMessageParams() == []
    }

    def 'MethodArgumentNotValidException should be mapped to a ValidationErrorModel'() {
        given:
        BindingResult bindingResult = new BeanPropertyBindingResult(null, null)

        bindingResult.addError(new ObjectError('objectName', null, ['globalParam1', 'globalParam2'] as Object[], 'global message'))
        bindingResult.addError(new FieldError('objectName', 'field', 'rejected value', true, null, null, 'field message'))

        Method method = Object.class.getDeclaredMethod('toString')
        MethodArgumentNotValidException exception = new MethodArgumentNotValidException(new MethodParameter(method, -1), bindingResult)

        when:
        ValidationErrorModel model = errorHandler.processBeanValidationError(exception)

        then:
        model.getExceptionClassName() == null
        model.getGlobalErrors() != null
        model.getGlobalErrors().size() == 1
        model.getGlobalErrors().get(0).getMessageKey() == 'global message'
        model.getGlobalErrors().get(0).getMessageParams() == ['globalParam1', 'globalParam2']
        model.getFieldErrors() != null
        model.getFieldErrors().size() == 1
        model.getFieldErrors().get(0).getField() == 'field'
        model.getFieldErrors().get(0).getMessageKey() == 'field message'
        model.getFieldErrors().get(0).getMessageParams() == ['rejected value']
    }
}
