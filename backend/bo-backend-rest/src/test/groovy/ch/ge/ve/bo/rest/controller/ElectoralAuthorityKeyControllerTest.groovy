/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.rest.controller
import ch.ge.ve.bo.rest.services.FileUploadService
import ch.ge.ve.bo.service.admin.ElectoralAuthorityKeyService
import ch.ge.ve.bo.service.exception.BusinessException
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import java.nio.file.Path
import java.nio.file.Paths
import spock.lang.Shared
import spock.lang.Specification

class ElectoralAuthorityKeyControllerTest extends Specification {

  static Path RESOURCES_ROOT = Paths.get("src", "test", "resources")
  static Path ELECTION_OFFICER_PUBLIC_KEY_FOLDER = RESOURCES_ROOT.resolve("ea-public-key")
  @Shared
  ElectoralAuthorityKeyController authorityKeyController

  def setupSpec() {
    authorityKeyController = new ElectoralAuthorityKeyController(
            Mock(ElectoralAuthorityKeyService),
            createObjectMapper(),
            Mock(FileUploadService))

  }

  def 'get public key from u-Count json file'() {
    given:
    Path eaPublicKey = ELECTION_OFFICER_PUBLIC_KEY_FOLDER.resolve("election-officer-public-key.json")
    def inputStream = new FileInputStream(eaPublicKey.toFile())

    when:
    def publicKey = authorityKeyController.getPublicKey(inputStream)

    then:
    publicKey != null
  }

  def 'get an error if public key from u-Count json file is invalid'() {
    given:
    Path eaPublicKey = ELECTION_OFFICER_PUBLIC_KEY_FOLDER.resolve("wrong-election-officer-public-key.json")
    def inputStream = new FileInputStream(eaPublicKey.toFile())

    when:
    authorityKeyController.getPublicKey(inputStream)

    then:
    BusinessException exception = thrown()
    exception.messageKey == "admin.electoral-authority-key.error.invalid-format"
  }


  def 'get error if inptstream of public key from u-Count json file is null'() {
    when:
    authorityKeyController.getPublicKey(null)

    then:
    BusinessException exception = thrown()
    exception.messageKey == "admin.electoral-authority-key.error.invalid-format"
  }


  static ObjectMapper createObjectMapper() {
    SimpleModule module = new SimpleModule()
    module.addSerializer(BigInteger.class, new BigIntegerAsBase64Serializer())
    module.addDeserializer(BigInteger.class, new BigIntegerAsBase64Deserializer())
    return new ObjectMapper().registerModule(module)
  }

}