/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Deserializer;
import ch.ge.ve.jacksonserializer.BigIntegerAsBase64Serializer;
import ch.ge.ve.jacksonserializer.JSDates;
import java.io.IOException;
import java.util.Arrays;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@SpringBootApplication
@Import({RepositoryConfigurationForMockServer.class, SwaggerConfig.class})
@EnableTransactionManagement
@EnableAspectJAutoProxy
public class MockServer implements WebMvcConfigurer {
  public static final String MOCK_SERVER_USER = "mock-server";

  public static void main(String[] args) {
    SpringApplication.run(MockServer.class, args);
  }

  @Bean
  public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer() {
    return jacksonObjectMapperBuilder -> {
      jacksonObjectMapperBuilder.serializers(JSDates.SERIALIZER, new BigIntegerAsBase64Serializer());
      jacksonObjectMapperBuilder.deserializers(JSDates.DESERIALIZER, new BigIntegerAsBase64Deserializer());
    };
  }

  @Value("${env.timezone}")
  private String timeZone;

  @PostConstruct
  public void setDefaultTimezone() {
    TimeZone.setDefault(TimeZone.getTimeZone(timeZone));
  }

  @Bean
  public CommonsRequestLoggingFilter logFilter() {
    CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
    filter.setIncludeQueryString(false);
    filter.setIncludePayload(false);
    filter.setIncludeHeaders(false);
    filter.setIncludeClientInfo(false);
    filter.setAfterMessagePrefix("REQUEST DATA : ");
    return filter;
  }

  @SuppressWarnings("Duplicates")
  @Bean
  public Filter addNoCacheFilter() {
    return new OncePerRequestFilter() {
      @Override
      protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
          throws ServletException, IOException {
        response.addHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.addHeader("pragma", "no-cache");
        filterChain.doFilter(request, response);
      }
    };
  }


  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(new WebSecurityConnectedInterceptor());
  }

  private class WebSecurityConnectedInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
      ConnectedUser.set(MOCK_SERVER_USER, Arrays.asList(ConnectedUser.BELONGS_TO_REALM + "GE",
                                                        ConnectedUser.BELONGS_TO_MANAGEMENT_ENTITY + "Canton de Genève",
                                                        "ROLE_USER"));
      return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
      ConnectedUser.unset();
    }
  }

}