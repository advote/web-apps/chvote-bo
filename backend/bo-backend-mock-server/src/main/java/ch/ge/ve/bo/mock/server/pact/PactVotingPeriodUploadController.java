/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.pact;

import ch.ge.ve.bo.mock.server.services.PactSimulator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile({"development", "integration"})
public class PactVotingPeriodUploadController extends AbstractPactUploadController {


  @Autowired
  public PactVotingPeriodUploadController(PactSimulator pactSimulator, ObjectMapper objectMapper) {
    super(pactSimulator, objectMapper);
  }

  @RequestMapping(path = "pact/operation/{operationId}/voting-period/configuration", method = RequestMethod.POST,
                  consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
                  produces = MediaType.APPLICATION_JSON_VALUE)
  public synchronized void uploadVotingPeriod(
      @PathVariable("operationId") long operationId,
      @RequestPart("configuration") String votingPeriodConfigurationAsJson) {
    handleInvalid();
    lastRequestJson = votingPeriodConfigurationAsJson;
    pactSimulator.markVotingPeriodAsUploaded(operationId);
  }


  @ResponseBody
  @RequestMapping(path = "pact/upload/voting-period/last-sent-operation", method = RequestMethod.GET)
  public synchronized String getLastRequestJson() {
    return lastRequestJson;
  }


  @RequestMapping(path = "pact/upload/voting-period/reset", method = RequestMethod.GET)
  public void reset() {
    super.reset();
    pactSimulator.resetVotingPeriod();
  }

}
