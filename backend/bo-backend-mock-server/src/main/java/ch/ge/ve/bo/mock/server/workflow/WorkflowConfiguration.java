/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.workflow;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "workflow")
public class WorkflowConfiguration {
  private String boBaseUrl;
  private String boSgUser;
  private String boSgPass;
  private String boGeUser;
  private String boGePass;
  private String boBeUser;
  private String boBePass;
  private String pactBaseUrl;
  private String pactUser1;
  private String pactPass1;
  private String pactUser2;
  private String pactPass2;

  public String getBoBaseUrl() {
    return boBaseUrl;
  }

  public void setBoBaseUrl(String boBaseUrl) {
    this.boBaseUrl = boBaseUrl;
  }

  public String getBoSgUser() {
    return boSgUser;
  }

  public void setBoSgUser(String boSgUser) {
    this.boSgUser = boSgUser;
  }

  public String getBoSgPass() {
    return boSgPass;
  }

  public void setBoSgPass(String boSgPass) {
    this.boSgPass = boSgPass;
  }

  public String getBoGeUser() {
    return boGeUser;
  }

  public void setBoGeUser(String boGeUser) {
    this.boGeUser = boGeUser;
  }

  public String getBoGePass() {
    return boGePass;
  }

  public void setBoGePass(String boGePass) {
    this.boGePass = boGePass;
  }

  public String getBoBeUser() {
    return boBeUser;
  }

  public void setBoBeUser(String boBeUser) {
    this.boBeUser = boBeUser;
  }

  public String getBoBePass() {
    return boBePass;
  }

  public void setBoBePass(String boBePass) {
    this.boBePass = boBePass;
  }

  public String getPactBaseUrl() {
    return pactBaseUrl;
  }

  public void setPactBaseUrl(String pactBaseUrl) {
    this.pactBaseUrl = pactBaseUrl;
  }

  public String getPactUser1() {
    return pactUser1;
  }

  public void setPactUser1(String pactUser1) {
    this.pactUser1 = pactUser1;
  }

  public String getPactPass1() {
    return pactPass1;
  }

  public void setPactPass1(String pactPass1) {
    this.pactPass1 = pactPass1;
  }

  public String getPactUser2() {
    return pactUser2;
  }

  public void setPactUser2(String pactUser2) {
    this.pactUser2 = pactUser2;
  }

  public String getPactPass2() {
    return pactPass2;
  }

  public void setPactPass2(String pactPass2) {
    this.pactPass2 = pactPass2;
  }
}
