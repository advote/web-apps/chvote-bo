/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.pact;

import ch.ge.ve.bo.mock.server.services.PactSimulator;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Profile({"development", "integration"})
public class PactVotingMaterialUploadController extends AbstractPactUploadController {
  private Map<String, byte[]> lastSentFiles;

  private List<String> lastZipsSent;

  @Autowired
  public PactVotingMaterialUploadController(PactSimulator pactSimulator, ObjectMapper objectMapper) {
    super(pactSimulator, objectMapper);
  }

  @RequestMapping(path = "pact/operation/{operationId}/voting-materials/configuration",
                  method = RequestMethod.POST,
                  consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public synchronized void uploadVotingMaterials(
      @RequestPart("data") MultipartFile[] files,
      @PathVariable("operationId") long operationId,
      @RequestParam("configuration") String votingMaterialsConfigurationAsJson) {
    handleInvalid();
    lastSentFiles = new HashMap<>();
    lastZipsSent = Arrays.stream(files)
                         .peek(this::storeInMemory)
                         .map(MultipartFile::getOriginalFilename).collect(Collectors.toList());
    lastRequestJson = votingMaterialsConfigurationAsJson;
    pactSimulator.markVotingMaterialAsUploaded(operationId);
  }

  private void storeInMemory(MultipartFile multipartFile) {
    try {
      lastSentFiles.put(multipartFile.getOriginalFilename(), multipartFile.getBytes());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @ResponseBody
  @RequestMapping(path = "pact/upload/voting-materials", method = RequestMethod.GET, produces = "application/zip")
  public synchronized ResponseEntity<byte[]> getLastRequestJson(@RequestParam("file") String fileName) {
    HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
    return ResponseEntity.ok()
                         .headers(headers)
                         .contentType(MediaType.parseMediaType("application/octet-stream"))
                         .body(lastSentFiles.get(fileName));
  }


  @ResponseBody
  @RequestMapping(path = "pact/upload/voting-materials/last-sent-operation", method = RequestMethod.GET)
  public synchronized String getLastRequestJson() {
    return lastRequestJson;
  }

  @ResponseBody
  @RequestMapping(path = "pact/upload/voting-materials/last-sent-zip-list", method = RequestMethod.GET)
  public synchronized List<String> getLastZipsSent() {
    return lastZipsSent;
  }

  @RequestMapping(path = "pact/upload/voting-materials/reset", method = RequestMethod.GET)
  public void reset() {
    super.reset();
    pactSimulator.resetVotingMaterial();
    lastZipsSent = null;
  }

}
