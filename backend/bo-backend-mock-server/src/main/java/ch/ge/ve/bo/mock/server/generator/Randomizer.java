/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.generator;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.io.IOUtils;

/**
 * Randomizer used to generate "natural" names and addresses.
 */
class Randomizer {
  private static final long START_DATE = LocalDate.of(1920, 1, 1).toEpochDay();
  private static final long END_DATE   = LocalDate.of(2000, 12, 31).toEpochDay();

  private List<String> firstnames;
  private List<String> lastnames;
  private List<String> streets;
  private List<String> countries;
  private List<String> swissTowns;

  String randomFirstname() throws IOException {
    return randomInList(getFirstNameList());
  }

  String randomLastname() throws IOException {
    return randomInList(getLastNameList());
  }

  String randomAddressLine() throws IOException {
    return ThreadLocalRandom.current().nextBoolean() ?
        String.format("%d, %s", ThreadLocalRandom.current().nextInt(1, 450), randomInList(getStreetList())) :
        null;
  }

  String randomStreet() throws IOException {
    return randomInList(getStreetList());
  }

  String randomCountry() throws IOException {
    return randomInList(getCountryList());
  }

  String randomSwissTown() throws IOException {
    return randomInList(getSwissTownList());
  }

  long randomZip() {
    return ThreadLocalRandom.current().nextLong(1000, 9999);
  }

  LocalDate randomDateOfBirth() {
    long randomEpochDay = ThreadLocalRandom.current().nextLong(START_DATE, END_DATE);
    return LocalDate.ofEpochDay(randomEpochDay);
  }

  private synchronized List<String> getFirstNameList() throws IOException {
    if (firstnames == null) {
      firstnames = getListOfRowsFromFileName("/generator/firstnames.csv");
    }
    return firstnames;
  }

  private synchronized List<String> getLastNameList() throws IOException {
    if (lastnames == null) {
      lastnames = getListOfRowsFromFileName("/generator/lastnames.csv");
    }
    return lastnames;
  }

  private synchronized List<String> getStreetList() throws IOException {
    if (streets == null) {
      streets = getListOfRowsFromFileName("/generator/streets.csv");
    }
    return streets;
  }

  private synchronized List<String> getCountryList() throws IOException {
    if (countries == null) {
      countries = getListOfRowsFromFileName("/generator/countries.csv");
    }
    return countries;
  }

  private synchronized List<String> getSwissTownList() throws IOException {
    if (swissTowns == null) {
      swissTowns = getListOfRowsFromFileName("/generator/swissTowns.csv");
    }
    return swissTowns;
  }

  private List<String> getListOfRowsFromFileName(String fileName) throws IOException {
    try (InputStream inputStream = Randomizer.class.getResourceAsStream(fileName)) {
      return IOUtils.readLines(inputStream, "UTF-8");
    }
  }

  private <T> T randomInList(List<? extends T> list) {
    int id = ThreadLocalRandom.current().nextInt(list.size());
    return list.get(id);
  }
}
