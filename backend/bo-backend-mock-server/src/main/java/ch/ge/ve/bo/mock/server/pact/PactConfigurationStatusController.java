/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.pact;


import static ch.ge.ve.bo.mock.server.MockServer.MOCK_SERVER_USER;

import ch.ge.ve.bo.mock.server.NotFound;
import ch.ge.ve.bo.mock.server.services.PactSimulator;
import ch.ge.ve.chvote.pactback.contract.operation.status.InTestConfigurationStatusVo.State;
import ch.ge.ve.chvote.pactback.contract.operation.status.OperationConfigurationStatusVo;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile({"development", "integration"})
public class PactConfigurationStatusController {

  private final PactSimulator pactSimulator;

  public PactConfigurationStatusController(PactSimulator pactSimulator) {
    this.pactSimulator = pactSimulator;
  }


  @RequestMapping(path = "pact/operation/{operationId}/configuration/status", method = RequestMethod.GET)
  @ResponseBody
  public OperationConfigurationStatusVo getConfigurationStatus(@PathVariable("operationId") long operationId) {
    OperationConfigurationStatusVo status = pactSimulator.getConfigurationStatus(operationId);
    if (status == null) {
      throw new NotFound();
    }

    return status;
  }

  @RequestMapping(path = "pact/operation/{operationId}/configuration/invalidate", method = RequestMethod.POST)
  @ResponseBody
  public void invalidateTestSite(@PathVariable("operationId") long operationId, @RequestParam("user") String userName) {
    pactSimulator.setNewTestStatus(operationId, State.INVALIDATED, null, userName);
  }

  @RequestMapping(path = "pact/operation/{operationId}/configuration/validate", method = RequestMethod.POST)
  @ResponseBody
  public void validateTestSite(@PathVariable("operationId") long operationId, @RequestParam("user") String userName) {
    pactSimulator.setNewTestStatus(operationId, State.VALIDATED, null, userName);
  }


  @RequestMapping(path = "pact/operation/{operationId}/configuration/status/test", method = RequestMethod.PUT)
  @ResponseBody
  public void updateTestStatus(@PathVariable(
      "operationId") long operationId, @RequestBody StatusWithComment newTestStatus) {
    pactSimulator
        .setNewTestStatus(operationId, newTestStatus.getNewTestStatus(), newTestStatus.getComment(), MOCK_SERVER_USER);
  }


  @RequestMapping(path = "pact/operation/{operationId}/configuration/status/finish-test-deployment-in-pact",
                  method = RequestMethod.GET)
  @ResponseBody
  public void finishTestDeploymentInPact(@PathVariable("operationId") long operationId) {
    updateTestStatus(operationId, new StatusWithComment(State.IN_VALIDATION, null));
  }


  @RequestMapping(path = "pact/operation/{operationId}/configuration/status/request-deployment-in-pact",
                  method = RequestMethod.GET)
  @ResponseBody
  public void requestDeploymentInPact(@PathVariable("operationId") long operationId) {
    updateTestStatus(operationId, new StatusWithComment(State.DEPLOYMENT_REQUESTED, null));
  }


  @RequestMapping(path = "pact/operation/{operationId}/configuration/status/put-deployment-in-error-in-pact",
                  method = RequestMethod.GET)
  @ResponseBody
  public void putDeploymentInErrorInPact(@PathVariable("operationId") long operationId) {
    updateTestStatus(operationId, new StatusWithComment(State.IN_ERROR, null));
  }

  @RequestMapping(path = "pact/operation/{operationId}/configuration/status/validate-deployment-request-in-pact",
                  method = RequestMethod.GET)
  @ResponseBody
  public void validateDeploymentRequestInPact(@PathVariable("operationId") long operationId) {
    pactSimulator.deployConfiguration(operationId);
  }


  @RequestMapping(path = "pact/operation/{operationId}/configuration/status/refuse-deployment-request-in-pact",
                  method = RequestMethod.GET)
  @ResponseBody
  public void refuseDeploymentRequestInPact(@PathVariable("operationId") long operationId) {
    updateTestStatus(operationId, new StatusWithComment(
        State.DEPLOYMENT_REFUSED,
        "Mock server has been asked to refuse this deployment"));
  }


  public static class StatusWithComment {

    private State  newTestStatus;
    private String comment;

    public StatusWithComment() {
    }

    public StatusWithComment(State newTestStatus, String comment) {
      this.newTestStatus = newTestStatus;
      this.comment = comment;
    }

    public void setNewTestStatus(State newTestStatus) {
      this.newTestStatus = newTestStatus;
    }

    public void setComment(String comment) {
      this.comment = comment;
    }

    public State getNewTestStatus() {
      return newTestStatus;
    }

    public String getComment() {
      return comment;
    }
  }

}
