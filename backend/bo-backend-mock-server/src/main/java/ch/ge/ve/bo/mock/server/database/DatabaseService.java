/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.database;

import static ch.ge.ve.bo.MilestoneType.BALLOT_BOX_DECRYPT;
import static ch.ge.ve.bo.MilestoneType.BALLOT_BOX_INIT;
import static ch.ge.ve.bo.MilestoneType.CERTIFICATION;
import static ch.ge.ve.bo.MilestoneType.DATA_DESTRUCTION;
import static ch.ge.ve.bo.MilestoneType.PRINTER_FILES;
import static ch.ge.ve.bo.MilestoneType.RESULT_VALIDATION;
import static ch.ge.ve.bo.MilestoneType.SITE_CLOSE;
import static ch.ge.ve.bo.MilestoneType.SITE_OPEN;
import static ch.ge.ve.bo.MilestoneType.SITE_VALIDATION;
import static ch.ge.ve.bo.mock.server.MockServer.MOCK_SERVER_USER;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.MilestoneType;
import ch.ge.ve.bo.mock.server.workflow.ManagementEntity;
import ch.ge.ve.bo.repository.BallotDocumentationRepository;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.DownloadTokenRepository;
import ch.ge.ve.bo.repository.ElectionPagePropertiesModelRepository;
import ch.ge.ve.bo.repository.ElectoralAuthorityKeyConfigurationRepository;
import ch.ge.ve.bo.repository.ElectoralAuthorityKeyRepository;
import ch.ge.ve.bo.repository.FileRepository;
import ch.ge.ve.bo.repository.HighlightedQuestionRepository;
import ch.ge.ve.bo.repository.RegisterMetadataRepository;
import ch.ge.ve.bo.repository.SimulationPeriodConfigurationRepository;
import ch.ge.ve.bo.repository.UnsecuredOperationRepository;
import ch.ge.ve.bo.repository.VoterTestingCardsLotRepository;
import ch.ge.ve.bo.repository.VotingSiteConfigurationRepository;
import ch.ge.ve.bo.repository.entity.BallotDocumentation;
import ch.ge.ve.bo.repository.entity.CardType;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.ElectionPageProperties;
import ch.ge.ve.bo.repository.entity.ElectionPagePropertiesModel;
import ch.ge.ve.bo.repository.entity.ElectoralAuthorityKey;
import ch.ge.ve.bo.repository.entity.ElectoralAuthorityKeyConfiguration;
import ch.ge.ve.bo.repository.entity.File;
import ch.ge.ve.bo.repository.entity.Milestone;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.Signature;
import ch.ge.ve.bo.repository.entity.SimulationPeriodConfiguration;
import ch.ge.ve.bo.repository.entity.TestingCardLanguage;
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot;
import ch.ge.ve.bo.repository.entity.VotingSiteConfiguration;
import ch.ge.ve.bo.repository.entity.WorkflowStep;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.register.RegisterImportService;
import ch.ge.ve.bo.service.operation.register.model.ImportRegisterResult;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import org.apache.commons.io.IOUtils;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DatabaseService {
  private static final LocalDateTime                                PAST               =
      LocalDateTime.of(2015, 1, 1, 10, 0);
  private static final String                                       USER               = "user";
  private static final Language                                     UNDEFINED_LANGUAGE = null;
  private final        UnsecuredOperationRepository                 operationRepository;
  private final        FileRepository                               fileRepository;
  private final        VoterTestingCardsLotRepository               voterTestingCardLotRepository;
  private final        RegisterMetadataRepository                   metadataRepository;
  private final        HighlightedQuestionRepository                highlightedQuestionRepository;
  private final        BallotDocumentationRepository                ballotDocumentationRepository;
  private final        ElectionPagePropertiesModelRepository        electionPagePropertiesModelRepository;
  private final        ElectoralAuthorityKeyRepository              electoralAuthorityKeyRepository;
  private final        ElectoralAuthorityKeyConfigurationRepository electoralAuthorityKeyConfigurationRepository;
  private final        VotingSiteConfigurationRepository            votingSiteConfigurationRepository;
  private final        SimulationPeriodConfigurationRepository      simulationPeriodConfigurationRepository;
  private final        RegisterImportService                        registerImportService;
  private final        EntityManager                                entityManager;
  private final        DownloadTokenRepository                      downloadTokenRepository;


  public DatabaseService(UnsecuredOperationRepository operationRepository, FileRepository fileRepository,
                         VoterTestingCardsLotRepository voterTestingCardLotRepository,
                         RegisterMetadataRepository metadataRepository,
                         HighlightedQuestionRepository highlightedQuestionRepository,
                         BallotDocumentationRepository ballotDocumentationRepository,
                         ElectionPagePropertiesModelRepository electionPagePropertiesModelRepository,
                         ElectoralAuthorityKeyRepository electoralAuthorityKeyRepository,
                         ElectoralAuthorityKeyConfigurationRepository electoralAuthorityKeyConfigurationRepository,
                         VotingSiteConfigurationRepository votingSiteConfigurationRepository,
                         SimulationPeriodConfigurationRepository simulationPeriodConfigurationRepository,
                         RegisterImportService registerImportService, EntityManager entityManager,
                         DownloadTokenRepository downloadTokenRepository) {
    this.operationRepository = operationRepository;
    this.fileRepository = fileRepository;
    this.voterTestingCardLotRepository = voterTestingCardLotRepository;
    this.metadataRepository = metadataRepository;
    this.highlightedQuestionRepository = highlightedQuestionRepository;
    this.ballotDocumentationRepository = ballotDocumentationRepository;
    this.electionPagePropertiesModelRepository = electionPagePropertiesModelRepository;
    this.electoralAuthorityKeyRepository = electoralAuthorityKeyRepository;
    this.electoralAuthorityKeyConfigurationRepository = electoralAuthorityKeyConfigurationRepository;
    this.votingSiteConfigurationRepository = votingSiteConfigurationRepository;
    this.simulationPeriodConfigurationRepository = simulationPeriodConfigurationRepository;
    this.registerImportService = registerImportService;
    this.entityManager = entityManager;
    this.downloadTokenRepository = downloadTokenRepository;
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void reset() {
    deleteAll(votingSiteConfigurationRepository,
              electoralAuthorityKeyConfigurationRepository,
              electoralAuthorityKeyRepository,
              simulationPeriodConfigurationRepository,
              downloadTokenRepository,
              electionPagePropertiesModelRepository,
              ballotDocumentationRepository,
              highlightedQuestionRepository,
              metadataRepository,
              voterTestingCardLotRepository,
              fileRepository,
              operationRepository);

    entityManager.createNativeQuery("DROP SEQUENCE BO_S_OPERATION").executeUpdate();
    long newId = System.currentTimeMillis() % (30 * 24 * 60 * 60);
    entityManager.createNativeQuery(String.format("CREATE SEQUENCE BO_S_OPERATION START WITH %d INCREMENT BY 1", newId))
                 .executeUpdate();
  }

  private void deleteAll(CrudRepository... repositories) {
    Stream.of(repositories).forEach(CrudRepository::deleteAll);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addElectoralAuthorityKeys() throws IOException {
    Set<String> keyAlreadyImported =
        electoralAuthorityKeyRepository.findAllByManagementEntity(ConnectedUser.get().managementEntity).stream()
                                       .map(ElectoralAuthorityKey::getKeyName).collect(Collectors.toSet());

    createKey(keyAlreadyImported, "key1",
              IOUtils.toByteArray(DatabaseController.class.getResourceAsStream("/aePublicKey-level1.pub")));
    createKey(keyAlreadyImported, "key2",
              IOUtils.toByteArray(DatabaseController.class.getResourceAsStream("/aePublicKey-level2.pub")));
  }

  private void createKey(Set<String> keyAlreadyImported, String key, byte[] content) {
    if (!keyAlreadyImported.contains(key)) {
      ElectoralAuthorityKey key1 = new ElectoralAuthorityKey();
      key1.setKeyName(key);
      key1.setImportDate(LocalDateTime.parse("2057-06-02T10:00:00"));
      key1.setManagementEntity(ConnectedUser.get().managementEntity);
      key1.setKeyContent(content);
      electoralAuthorityKeyRepository.save(key1);
    }
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addVotingMaterial(long operationId) throws BusinessException {
    Operation operation = operationRepository
        .findById(operationId)
        .orElseThrow(() -> new EntityNotFoundException("operation " + operationId));
    operation.setVotingCardTitle("Test label");
    operation.setPrinterTemplate(operation.getCanton().equals("GE") ? "template 3" : "BOH Printer");
    ImportRegisterResult importResult = registerImportService.importRegister(
        operationId, "register1.xml", this.getClass().getClassLoader().getResourceAsStream("register1.xml"));
    registerImportService.validateImport(operationId, importResult.getMessageUniqueId());

    createVoterTestingCardsLot(operationId, CardType.CONTROLLER_TESTING_CARD, "Card for controller");
    createVoterTestingCardsLot(operationId, CardType.PRINTER_TESTING_CARD, "Card for printer");

    operation.setLastVotingMaterialUpdateDate(PAST);
    operation.setLastConfigurationUpdateDate(PAST);
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addGuestManagementEntity(long operationId, String guestManagementEntity) {
    Operation operation = operationRepository
        .findById(operationId)
        .orElseThrow(() -> new EntityNotFoundException("operation " + operationId));

    operation.getGuestManagementEntities().add(guestManagementEntity);
    operation.setLastConfigurationUpdateDate(PAST);
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void deleteGuestManagementEntity(long operationId, String guestManagementEntity) {
    Operation operation = operationRepository
        .findById(operationId)
        .orElseThrow(() -> new EntityNotFoundException("operation " + operationId));
    operation.getGuestManagementEntities().remove(guestManagementEntity);
    operation.setLastConfigurationUpdateDate(PAST);
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Operation createOperation(String shortName, String longName, ManagementEntity managementEntity) {


    String canton = managementEntity.getDefaultUser().name();
    ConnectedUser
        .set(MOCK_SERVER_USER, Arrays.asList(ConnectedUser.BELONGS_TO_REALM + canton,
                                             ConnectedUser.BELONGS_TO_MANAGEMENT_ENTITY +
                                             managementEntity.getManagementEntity(),
                                             "ROLE_USER"));


    Operation operation = new Operation();
    operation.setLogin(USER);
    operation.setShortLabel(shortName);
    operation.setLongLabel(longName);
    operation.setDate(LocalDateTime.of(2057, 6, 12, 0, 0));
    operation.setManagementEntity(managementEntity.getManagementEntity());
    operation.setLastVotingMaterialUpdateDate(PAST);
    operation.setLastConfigurationUpdateDate(PAST);
    operation.setInModification(false);

    return operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addMilestones(long operationId) {
    Operation operation = operationRepository.getOne(operationId);
    operation.getMilestones().clear();
    operation.getMilestones().addAll(List.of(
        milestone(operationId, SITE_VALIDATION, LocalDateTime.of(2057, 6, 1, 0, 0)),
        milestone(operationId, CERTIFICATION, LocalDateTime.of(2057, 6, 2, 0, 0)),
        milestone(operationId, PRINTER_FILES, LocalDateTime.of(2057, 6, 3, 0, 0)),
        milestone(operationId, BALLOT_BOX_INIT, LocalDateTime.of(2057, 6, 4, 0, 0)),
        milestone(operationId, SITE_OPEN, LocalDateTime.of(2057, 6, 5, 12, 1)),
        milestone(operationId, SITE_CLOSE, LocalDateTime.of(2057, 6, 6, 9, 0)),
        milestone(operationId, BALLOT_BOX_DECRYPT, LocalDateTime.of(2057, 6, 8, 0, 0)),
        milestone(operationId, RESULT_VALIDATION, LocalDateTime.of(2057, 7, 1, 0, 0)),
        milestone(operationId, DATA_DESTRUCTION, LocalDateTime.of(2057, 7, 2, 0, 0))
    ));
    operation.setGracePeriod(9);
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void removeElectionPageProperties() {
    electionPagePropertiesModelRepository.deleteAll();
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void removeRepositoryElection(long operationId) {
    List<File> repositories =
        fileRepository.findByOperationIdAndType(operationId, FileType.ELECTION_REPOSITORY);
    fileRepository.deleteAll(repositories);

  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addDomainOfInfluence(long operationId, String managementEntity) throws IOException {
    fileRepository.save(file(operationId, FileType.DOMAIN_OF_INFLUENCE, "doi.xml", managementEntity));
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addRepositoryVotation(long operationId, String managementEntity) throws IOException {
    fileRepository.save(file(operationId, FileType.VOTATION_REPOSITORY, "votrep.xml", managementEntity));
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addRepositoryVotationIncpqs(long operationId, String managementEntity) throws IOException {
    fileRepository.save(file(operationId, FileType.VOTATION_REPOSITORY, "votrep-incpqs.xml", managementEntity));
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addRepositoryElection(long operationId, String managementEntity) throws IOException {
    fileRepository.save(file(operationId, FileType.ELECTION_REPOSITORY, "elecrep.xml", managementEntity));
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addDocuments(long operationId, String managementEntity) throws IOException {
    fileRepository.save(file(operationId, FileType.DOCUMENT_FAQ, "op_doc_faq.fr.pdf", managementEntity, Language.FR));
    fileRepository
        .save(file(operationId, FileType.DOCUMENT_CERTIFICATE, "op_doc_cert.fr.pdf", managementEntity, Language.FR));
    fileRepository
        .save(file(operationId, FileType.DOCUMENT_TERMS, "op_doc_terms.fr.pdf", managementEntity, Language.FR));
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addTestingCards(long operationId) {
    createVoterTestingCardsLot(operationId, CardType.TEST_SITE_TESTING_CARD, "Card for testing");
    createVoterTestingCardsLot(operationId, CardType.PRODUCTION_TESTING_CARD, "Card for testing");
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addElectionPageProperties(long operationId) {
    ElectionPagePropertiesModel model = new ElectionPagePropertiesModel();
    model.setModelName("model");
    model.setCandidateInformationDisplayModel("Grand Conseil");
    model.setOperation(operationRepository.getOne(operationId));
    model.setDisplayedColumnsOnVerificationTable("candidate-identity|verification-code");
    model.setColumnsOrderOnVerificationTable("");
    model.setMappings(Arrays.asList(
        mapping("Conseil d'Etat 2018"),
        mapping("Grand Conseil 2018"),
        mapping("Test majoritaire sans liste 2018")
    ));

    electionPagePropertiesModelRepository.save(model);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public ImportRegisterResult addRegister(long operationId, String canton, String fileName)
      throws BusinessException {
    return registerImportService.importRegister(
        operationId, fileName,
        this.getClass().getClassLoader().getResourceAsStream(canton.toLowerCase() + "/" + fileName)
    );
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void validateRegister(long operationId, ImportRegisterResult importResult) throws BusinessException {
    registerImportService.validateImport(operationId, importResult.getMessageUniqueId());
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addVotingCardTitle(long operationId, String votingCardTitle) {
    Operation operation = operationRepository.getOne(operationId);
    operation.setVotingCardTitle(votingCardTitle);
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addPrinterTemplace(long operationId, String canton) {
    Operation operation = operationRepository.getOne(operationId);
    operation.setPrinterTemplate(canton.equals("GE") ? "template 3" : "BOH Printer");
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addDeploymentTarget(long operationId, DeploymentTarget deploymentTarget) {
    Operation operation = operationRepository.getOne(operationId);
    operation.setDeploymentTarget(deploymentTarget);
    if (deploymentTarget == DeploymentTarget.SIMULATION) {
      operation.setSimulationName("Test Simulation");
    }
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void removeBallotDocumentation(long operationId) {
    List<BallotDocumentation> ballotDocumentations =
        ballotDocumentationRepository.findByOperationId(operationId);
    ballotDocumentationRepository.deleteAll(ballotDocumentations);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addSimulationSitePeriod(long operationId) {
    SimulationPeriodConfiguration simulationPeriodConfiguration = new SimulationPeriodConfiguration();
    simulationPeriodConfiguration.setGracePeriod(10);
    simulationPeriodConfiguration.setDateOpen(LocalDateTime.of(2010, 6, 4, 10, 10));
    simulationPeriodConfiguration.setDateClose(LocalDateTime.of(2057, 6, 4, 11, 20));
    simulationPeriodConfiguration.setOperation(operationRepository.getOne(operationId));
    simulationPeriodConfigurationRepository.save(simulationPeriodConfiguration);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addElectoralAuthorityKeyConfig(long operationId) {
    ElectoralAuthorityKeyConfiguration config = new ElectoralAuthorityKeyConfiguration();
    config.setOperation(operationRepository.getOne(operationId));
    config.setElectoralAuthorityKey(
        electoralAuthorityKeyRepository
            .findByManagementEntityAndKeyName(ConnectedUser.get().managementEntity, "key1"));
    electoralAuthorityKeyConfigurationRepository.save(config);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void addVotingSiteConfiguration(long operationId) {
    Operation operation = operationRepository.getOne(operationId);
    VotingSiteConfiguration votingSiteConfiguration = new VotingSiteConfiguration();
    votingSiteConfiguration.setDefaultLanguage(Language.FR);
    votingSiteConfiguration.setOperation(operation);
    votingSiteConfiguration.setAllLanguages(EnumSet.of(Language.FR, Language.DE));
    votingSiteConfigurationRepository.save(votingSiteConfiguration);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void ensureConsistencyCheck(long operationId) {
    // Update operation date to ensure that consistency check will be scheduled
    Operation operation = operationRepository.getOne(operationId);
    operation.setLastVotingMaterialUpdateDate(PAST);
    operation.setLastConfigurationUpdateDate(PAST);
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void setWorkflowStep(WorkflowStep workflowStep, Long operationId) {
    Operation operation = operationRepository.getOne(operationId);
    operation.setWorkflowStep(workflowStep);
    operationRepository.save(operation);
  }

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void setWorkflowStep(WorkflowStep workflowStep) {
    operationRepository.findAll().forEach(
        operation -> {
          operation.setWorkflowStep(workflowStep);
          operationRepository.save(operation);
        }
    );
  }

  private ElectionPageProperties mapping(String ballot) {
    ElectionPageProperties mapping = new ElectionPageProperties();
    mapping.setBallot(ballot);
    return mapping;
  }

  private void createVoterTestingCardsLot(long operationId, CardType cardType, String lotName) {
    VoterTestingCardsLot lot = new VoterTestingCardsLot();
    lot.setOperation(operationRepository.getOne(operationId));
    lot.setAddress1("Address");
    lot.setBirthday(LocalDateTime.of(1980, 8, 25, 0, 0));
    lot.setCardType(cardType);
    lot.setCity("Geneve");
    lot.setPostalCode("1201");
    lot.setLotName(lotName);
    lot.setCount(10);
    lot.setCountry("Swiss");
    lot.setDoi(Collections.singletonList("CH"));
    lot.setSignature(Signature.Ms);
    lot.setFirstName("testing");
    lot.setLastName("user");
    lot.setStreet("Street");
    lot.setLanguage(TestingCardLanguage.FR);
    voterTestingCardLotRepository.save(lot);
  }

  private File file(long operationId, FileType type, String fileName, String managementEntity)
      throws IOException {
    return file(operationId, type, fileName, managementEntity, UNDEFINED_LANGUAGE);
  }

  private File file(long operationId, FileType type, String fileName, String managementEntity, Language language)
      throws IOException {
    return file(operationId, type, fileName, managementEntity, fileName, language,
                Stream.of(ConnectedUser.get().realm, operationId, type, language, fileName)
                      .filter(Objects::nonNull).map(Object::toString)
                      .collect(Collectors.joining("|")));
  }

  private File file(long operationId, FileType type, String fileName, String managementEntity,
                    String resourceName, Language language, String businessKey) throws IOException {
    File file = new File();
    file.setLogin(USER);
    file.setType(type);
    file.setOperation(operationRepository.getOne(operationId));
    file.setLanguage(language);
    file.setManagementEntity(managementEntity);
    file.setFileName(fileName);
    file.setDate(LocalDateTime.of(2017, 1, 1, 1, 1));
    file.setBusinessKey(businessKey);
    file.setFileContent(IOUtils.toByteArray(
        Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream(resourceName))));
    return file;
  }

  private Milestone milestone(long operationId, MilestoneType type, LocalDateTime date) {
    Milestone milestone = new Milestone();
    milestone.setDate(date);
    milestone.setType(type);
    milestone.setOperation(operationRepository.getOne(operationId));
    milestone.setLogin(USER);
    return milestone;
  }


}
