/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.workflow;

import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.response.ValidatableResponse;
import org.springframework.stereotype.Service;

@Service
public final class BackOffice {
  private final Connection connection;
private BackOfficeUser user = BackOfficeUser.GE;
  public BackOffice(Connection connection) {
    this.connection = connection;
  }

  public void switchUser(BackOfficeUser user) {
    this.user = user;
  }

  public ValidatableResponse operationStatus(Long operationId) {
    return connection.boServer(user)
                     .get("/operation/" + operationId + "/status")
                     .then()
                     .statusCode(200);
  }

  public ValidatableResponse uploadConfiguration(Long operationId) {
    return connection.boServer(user)
                     .get("/operation/" + operationId + "/configuration/upload")
                     .then()
                     .statusCode(200);
  }

  public ValidatableResponse invalidateTestSite(long operationId) {
    return connection.boServer(user)
                     .get("/operation/" + operationId + "/invalidate-test-site")
                     .then();
  }

  public ValidatableResponse validateTestSite(long operationId) {
    return connection.boServer(user)
                     .get("/operation/" + operationId + "/validate-test-site")
                     .then()
                     .statusCode(200);
  }

  public void targetSimulation(long operationId) {
    connection.boServer(user)
              .body("test")
              .put("/operation/" + operationId + "/target-simulation")
              .then().statusCode(200);

  }

  public void targetReal(long operationId) {
    connection.boServer(user)
              .body("test")
              .put("/operation/" + operationId + "/target-real")
              .then().statusCode(200);

  }

  public void uploadVotingMaterial(long operationId) {
    connection.boServer(user)
              .get("/operation/" + operationId + "/voting-materials/upload")
              .then().statusCode(200);

  }


  public void uploadVotingPeriod(long operationId) {
    connection.boServer(user)
              .get("/operation/" + operationId + "/voting-period/upload")
              .then().statusCode(200);

  }

  public String pactIdFromConfigStatus(ResponseBodyExtractionOptions response) {
    return response.jsonPath().getString("configurationStatus.pactUrl").replaceAll("^.*/(\\d+)$", "$1");
  }

  public String pactIdFromVmStatus(ResponseBodyExtractionOptions response) {
    return response.jsonPath().getString("votingMaterialStatus.pactUrl").replaceAll("^.*/(\\d+)$", "$1");
  }


  public long pactIdFromVpStatus(ResponseBodyExtractionOptions response) {
    return Long
        .parseLong(response.jsonPath().getString("votingPeriodStatus.pactUrl").replaceAll("^.*/(\\d+)$", "$1"));
  }

  public ValidatableResponse validateVotingMaterial(long operationId) {
    return connection.boServer(user)
                     .get("/operation/" + operationId + "/validate-voting-material")
                     .then()
                     .statusCode(200);
  }

}
