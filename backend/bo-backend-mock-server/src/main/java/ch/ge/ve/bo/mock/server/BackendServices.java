/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server;

import ch.ge.ve.bo.repository.ConsistencyCheckCallback;
import ch.ge.ve.bo.repository.FileRepository;
import ch.ge.ve.bo.repository.OperationConsistencyRepository;
import ch.ge.ve.bo.repository.RegisterMetadataRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepositoryImpl;
import ch.ge.ve.bo.repository.UnsecuredOperationRepository;
import ch.ge.ve.bo.repository.security.OperationHolder;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.FileServiceImpl;
import ch.ge.ve.bo.service.operation.OperationService;
import ch.ge.ve.bo.service.operation.OperationServiceImpl;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService;
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceService;
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryServiceElectionHandler;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryServiceVotationHandler;
import ch.ge.ve.bo.service.operation.register.RegisterImportService;
import ch.ge.ve.bo.service.operation.register.RegisterImportServiceImpl;
import ch.ge.ve.bo.service.operation.register.RegisterMetadataService;
import ch.ge.ve.bo.service.operation.register.RegisterMetadataServiceImpl;
import ch.ge.ve.bo.service.operation.register.RegisterReportBuilder;
import ch.ge.ve.bo.service.operation.status.OperationStatusService;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService;
import ch.ge.ve.bo.service.utils.xml.XSDValidator;
import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery;
import ch.ge.ve.interfaces.ech.service.EchCodec;
import ch.ge.ve.interfaces.ech.service.EchDeserializationRuntimeException;
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import net.sf.jasperreports.engine.JRException;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BackendServices {
  private static final String CATALOG_FILE = "catalog.cat";

  @Bean
  protected RegisterImportService registryImportService(
      ObjectMapper jacksonObjectMapper,
      RegisterMetadataService metadataService,
      OperationRepositoryService operationRepositoryService,
      OperationService operationService) throws JRException {

    return new RegisterImportServiceImpl(ech45Validator(),
                                         jacksonObjectMapper,
                                         metadataService,
                                         operationRepositoryService,
                                         registerReportBuilder(),
                                         operationService);
  }

  @Bean
  OperationRepositoryService operationRepositoryService(SecuredOperationRepository operationRepository,
                                                        OperationDomainInfluenceService operationDomainInfluenceService,
                                                        FileService fileService,
                                                        OperationRepositoryServiceVotationHandler votationHandler,
                                                        OperationRepositoryServiceElectionHandler electionHandler) {
    return new OperationRepositoryServiceImpl(operationRepository, operationDomainInfluenceService, fileService,
            votationHandler, electionHandler);
  }


  @Bean
  OperationRepositoryServiceVotationHandler votationHandler(FileService fileService) {
    EchCodec<Delivery> codec =
            new JAXBEchCodecImpl<>(ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery.class);
    return new OperationRepositoryServiceVotationHandler(
            votationReferentialValidator(),
            is -> {
              try {
                return codec.deserialize(is, false);
              } catch (EchDeserializationRuntimeException e) {
                throw new TechnicalException(e);
              }
            }, fileService);
  }

    @Bean
  XSDValidator votationReferentialValidator() {
    return XSDValidator.of("xsd/eCH-0159-4-0.xsd", CATALOG_FILE);
  }


  @Bean
  OperationRepositoryServiceElectionHandler electionHandler(FileService fileService) {
    EchCodec<ch.ge.ve.interfaces.ech.eCH0157.v4.Delivery> codec =
            new JAXBEchCodecImpl<>(ch.ge.ve.interfaces.ech.eCH0157.v4.Delivery.class);
    return new OperationRepositoryServiceElectionHandler(
            electionReferentialValidator(),
            is -> {
              try {
                return codec.deserialize(is, false);
              } catch (EchDeserializationRuntimeException e) {
                throw new TechnicalException(e);
              }
            }, fileService);
  }


  @Bean
  XSDValidator electionReferentialValidator() {
    return XSDValidator.of("xsd/eCH-0157-4-0.xsd", CATALOG_FILE);
  }
  @Bean
  protected XSDValidator ech45Validator() {
    return XSDValidator.of("xsd/eCH-0045-4-0.xsd", "catalog.cat");
  }

  @Bean
  RegisterReportBuilder registerReportBuilder() throws JRException {
    return new RegisterReportBuilder();
  }

  @Bean
  SecuredOperationRepository securedOperationRepository(UnsecuredOperationRepository operationRepository) {
    return new SecuredOperationRepositoryImpl(operationRepository);
  }

  @Bean
  protected RegisterMetadataService registerMetadataService(
      RegisterMetadataRepository registerMetadataRepository,
      SecuredOperationRepository operationRepository,
      SecuredOperationHolderService securedOperationHolderService,
      @Value("${operation.register.temporaryFile.ttl}") long ttl) {
    return new RegisterMetadataServiceImpl(registerMetadataRepository,
                                           operationRepository,
                                           securedOperationHolderService,
                                           ttl);
  }

  @Bean
  OperationService operationService(SecuredOperationRepository operationRepository,
                                    OperationConsistencyRepository operationConsistencyRepository,
                                    FileService fileService,
                                    VoterTestingCardsLotService voterTestingCardsLotService,
                                    OperationStatusService operationStatusService,
                                    ObjectMapper jacksonObjectMapper) {
    return new OperationServiceImpl(operationRepository, operationConsistencyRepository, voterTestingCardsLotService,
                                    fileService, operationStatusService, consistencyScheduleService(),
                                    jacksonObjectMapper);
  }


  @Bean
  FileService fileService(FileRepository fileRepository,
                          SecuredOperationRepository operationRepository,
                          SecuredOperationHolderService securedOperationHolderService) {
    return new FileServiceImpl(fileRepository, operationRepository, securedOperationHolderService);
  }

  @Bean
  public ConsistencyScheduleService consistencyScheduleService() {
    return new ConsistencyScheduleService() {
      @Override
      public void scheduleCheck(long operationId) {
      }

      @Override
      public ConsistencyCheckCallback asCallback(long operationId) {
        return () -> {
        };
      }
    };

  }


  @Bean
  public VoterTestingCardsLotService voterTestingCardsLotService() {
    return Mockito.mock(VoterTestingCardsLotService.class);
  }


  @Bean
  public OperationStatusService operationStatusService() {
    return Mockito.mock(OperationStatusService.class);
  }


  @Bean
  public PrinterTemplateService printerTemplateService() {
    return Mockito.mock(PrinterTemplateService.class);
  }


  @Bean
  OperationDomainInfluenceService operationDomainInfluenceService(FileService fileService) {
    return new OperationDomainInfluenceServiceImpl(fileService, logisticDeliveryValidator());
  }

  @Bean
  protected XSDValidator logisticDeliveryValidator() {
    return XSDValidator.of("xsd/logistic-delivery.xsd", "catalog.cat");
  }


  @Bean
  public SecuredOperationHolderService securedOperationHolderService() {
    return new SecuredOperationHolderService() {
      @Override
      public <T extends OperationHolder> Optional<T> safeRead(Supplier<Optional<T>> operationHolderSupplier) {
        return operationHolderSupplier.get();
      }

      @Override
      public <U extends OperationHolder> void doUpdateAndForget(Supplier<Optional<U>> operationHolderSupplier,
                                                                Consumer<U> updater) {
        operationHolderSupplier.get().ifPresent(updater);
      }

      @Override
      public <U extends OperationHolder> void doUpdateOperationHolderAndForget(U operationHolder, Consumer<U> updater) {
        updater.accept(operationHolder);
      }

      @Override
      public <U extends OperationHolder, T extends Collection<U>> T safeReadAll(Supplier<T> operationHolderSupplier) {
        return operationHolderSupplier.get();
      }

      @Override
      public <U extends OperationHolder> void doNotOptionalUpdateAndForget
          (Supplier<U> operationHolderSupplier, Consumer<U> updater) {
        updater.accept(operationHolderSupplier.get());
      }

      @Override
      public <U extends
          OperationHolder, T> Optional<T> doTechnicalUpdate(Supplier<Optional<U>> operationHolderSupplier,
                                                            Function<U, T> updater) {
        return operationHolderSupplier.get().map(updater);
      }

      @Override
      public <U extends OperationHolder, T> T
      doTechnicalNotOptionalUpdate(Supplier<U> operationHolderSupplier, Function<U, T> updater) {
        return updater.apply(operationHolderSupplier.get());
      }

      @Override
      public <U extends OperationHolder> void doTechnicalUpdateAndForget
          (Supplier<Optional<U>> operationHolderSupplier, Consumer<U> updater) {
        operationHolderSupplier.get().ifPresent(updater);
      }

      @Override
      public <U extends OperationHolder> void doTechnicalNotOptionalUpdateAndForget
          (Supplier<U> operationHolderSupplier, Consumer<U> updater) {
        updater.accept(operationHolderSupplier.get());
      }

      @Override
      public <U extends
          OperationHolder, T> Optional<T> doUpdate(Supplier<Optional<U>> operationHolderSupplier,
                                                   Function<U, T> updater) {
        return operationHolderSupplier.get().map(updater);
      }

      @Override
      public <U extends OperationHolder, T> T doUpdateOperationHolder(U operationHolder, Function<U, T> updater) {
        return updater.apply(operationHolder);
      }

      @Override
      public <U extends OperationHolder, T> T
      doNotOptionalUpdate(Supplier<U> operationHolderSupplier, Function<U, T> updater) {
        return updater.apply(operationHolderSupplier.get());
      }

    };
  }

}
