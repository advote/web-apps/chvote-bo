/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.workflow;

public enum ManagementEntity {

  BE_BERN("Bern", BackOfficeUser.BE),
  GE_CANTON_DE_GENEVE("Canton de Genève", BackOfficeUser.GE),
  SG_SAINT_GALL("Saint-Gall", BackOfficeUser.SG);

  private final String managementEntity;
  private final BackOfficeUser defaultUser;

  ManagementEntity(String managementEntity, BackOfficeUser defaultUser) {
    this.managementEntity = managementEntity;
    this.defaultUser = defaultUser;
  }

  public String getManagementEntity() {
    return managementEntity;
  }

  public BackOfficeUser getDefaultUser() {
    return defaultUser;
  }
}
