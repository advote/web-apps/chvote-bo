/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.workflow;

import static org.awaitility.Awaitility.await;
import static org.hamcrest.CoreMatchers.equalTo;

import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.service.conf.Canton;
import java.io.File;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletResponse;
import org.awaitility.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "workflow")
public class WorkflowController {
  private static final Logger   logger                                =
      LoggerFactory.getLogger(WorkflowController.class);
  private static final Duration MAX_VOTING_MATERIAL_UPLOAD_TIME       =
      new Duration(2, TimeUnit.MINUTES);
  private static final Duration MAX_VOTING_PERIOD_UPLOAD_TIME         =
      new Duration(2, TimeUnit.MINUTES);
  private static final Duration MAX_VOTING_PERIOD_INITIALIZATION_TIME =
      new Duration(5, TimeUnit.MINUTES);
  private static final Duration MAX_CONSISTENCY_TIME                  =
      new Duration(2, TimeUnit.MINUTES);
  private static final Duration MAX_TEST_SITE_CREATION_TIME           =
      new Duration(5, TimeUnit.MINUTES);
  private static final Duration MAX_VOTING_MATERIAL_CREATION_TIME     =
      new Duration(10, TimeUnit.MINUTES);
  private static final Duration MAX_TEST_SITE_DEPLOYMENT_TIME         =
      new Duration(3, TimeUnit.MINUTES);

  private final BackOffice bo;
  private final Pact       pact;
  private final String     outputDir;


  public WorkflowController(BackOffice bo, Pact pact, @Value("${pact.output-dir}") String outputDir) {
    this.bo = bo;
    this.pact = pact;
    this.outputDir = outputDir;

  }


  @RequestMapping(path = "fast-forward/{operationId}/{canton}",
                  method = RequestMethod.GET)
  public void fastForward(@PathVariable("operationId") long operationId, @PathVariable("canton") Canton canton,
                          @RequestParam("DeploymentTarget") DeploymentTarget deploymentTarget) {
    moveFromConfiguration(operationId, canton, deploymentTarget);
    moveFromVotingMaterial(operationId, canton);
    moveFromVotingPeriod(operationId, canton);
  }

  @RequestMapping(path = "testing-card/{operationId}",
                  produces = MediaType.APPLICATION_OCTET_STREAM_VALUE,
                  method = RequestMethod.GET)
  @ResponseBody
  public FileSystemResource testingCard(@PathVariable("operationId") long operationId, HttpServletResponse response) {
    String testCardLocation = bo.operationStatus(operationId).extract().jsonPath()
                                .getString("votingMaterialStatus.votingCardLocations.boh-vprt");
    testCardLocation = outputDir + File.separator + testCardLocation.replace('/', File.separator.charAt(0));
    response.setHeader("Content-Disposition",
                       String.format("attachment; filename=\"%s\"",
                                     testCardLocation.substring(testCardLocation.lastIndexOf(File.separator) + 1)));
    return new FileSystemResource(testCardLocation);
  }


  /**
   * move an operation to voting material state
   */
  @RequestMapping(path = "to-voting-material/{operationId}/{canton}",
                  method = RequestMethod.GET)
  public void moveFromConfiguration(@PathVariable("operationId") long operationId,
                                    @PathVariable("canton") Canton canton,
                                    @RequestParam("DeploymentTarget") DeploymentTarget deploymentTarget) {
    logger.info("Processing operation " + operationId);
    bo.switchUser(BackOfficeUser.valueOf(canton.name()));

    await().atMost(MAX_CONSISTENCY_TIME).untilAsserted(
        () -> bo.operationStatus(operationId).body("consistencyComputationInProgress", equalTo(false)));
    bo.uploadConfiguration(operationId);

    await().atMost(MAX_TEST_SITE_CREATION_TIME).untilAsserted(
        () -> bo.operationStatus(operationId).body("configurationStatus.state", equalTo("IN_VALIDATION")));
    logger.info("Configuration uploaded for operation " + operationId);

    bo.validateTestSite(operationId);
    Long confBusinessId =
        Long.valueOf(bo.pactIdFromConfigStatus(bo.operationStatus(operationId).extract().body()));

    logger.info("Test Site validated for operation " + operationId);

    pact.switchToUser(PactUser.REQUESTER);
    pact.requestConfigurationDeployment(confBusinessId);

    logger.info("Deployment requested for operation " + operationId);

    long configurationDeploymentId = pact.getConfigurationDeploymentIdByBusinessKey(confBusinessId);
    pact.switchToUser(PactUser.VALIDATOR);
    pact.validateConfigurationDeployment(configurationDeploymentId);


    await().atMost(MAX_TEST_SITE_DEPLOYMENT_TIME).untilAsserted(
        () -> bo.operationStatus(operationId).body("configurationStatus.state", equalTo("DEPLOYED")));

    logger.info("deployment validated for operation " + operationId);

    if (deploymentTarget.equals(DeploymentTarget.REAL)) {
      bo.targetReal(operationId);
    } else {
      bo.targetSimulation(operationId);
    }
    logger.info("target selected " + deploymentTarget + " for operation " + operationId);
  }


  /**
   * move an operation to voting period state
   */
  @RequestMapping(path = "to-voting-period/{operationId}/{canton}",
                  method = RequestMethod.GET)
  public void moveFromVotingMaterial(@PathVariable("operationId") long operationId,
                                     @PathVariable("canton") Canton canton) {
    bo.switchUser(BackOfficeUser.valueOf(canton.name()));

    bo.uploadVotingMaterial(operationId);

    await().atMost(MAX_VOTING_MATERIAL_UPLOAD_TIME).untilAsserted(
        () -> bo.operationStatus(operationId).body("votingMaterialStatus.state", equalTo("AVAILABLE_FOR_CREATION")));

    logger.info("Voting material ccnfiguration uploaded for operation " + operationId);

    pact.switchToUser(PactUser.REQUESTER);
    long businessId = Long.valueOf(bo.pactIdFromVmStatus(bo.operationStatus(operationId).extract().body()));
    pact.requestVotingMaterialCreation(businessId);

    logger.info("Voting material creation requested for operation " + operationId);


    pact.switchToUser(PactUser.VALIDATOR);

    long vmDeploymentId = pact.getVotingMaterialIdByBusinessKey(businessId);
    pact.switchToUser(PactUser.VALIDATOR);
    pact.validateVotingMaterialCreation(vmDeploymentId);


    await().atMost(MAX_VOTING_MATERIAL_CREATION_TIME).untilAsserted(
        () -> bo.operationStatus(operationId).body("votingMaterialStatus.state", equalTo("CREATED")));

    logger.info("Voting material creation validated for operation " + operationId);

    bo.validateVotingMaterial(operationId);

    logger.info("Voting material validated for operation " + operationId);
  }


  /**
   * move an operation to voting period state
   */
  @RequestMapping(path = "initialize-voting-period/{operationId}/{canton}",
                  method = RequestMethod.GET)
  @Transactional
  public void moveFromVotingPeriod(@PathVariable("operationId") long operationId,
                                   @PathVariable("canton") Canton canton) {
    bo.switchUser(BackOfficeUser.valueOf(canton.name()));

    bo.uploadVotingPeriod(operationId);
    await().atMost(MAX_VOTING_PERIOD_UPLOAD_TIME).untilAsserted(
        () -> bo.operationStatus(operationId)
                .body("votingPeriodStatus.state", equalTo("AVAILABLE_FOR_INITIALIZATION")));
    logger.info("Voting period uploaded for operation " + operationId);


    pact.switchToUser(PactUser.REQUESTER);
    long businessId = bo.pactIdFromVpStatus(bo.operationStatus(operationId).extract().body());
    pact.requestVotingPeriodInitialization(businessId);
    logger.info("Voting period initialization requested for operation " + operationId);


    pact.switchToUser(PactUser.VALIDATOR);

    long vmDeploymentId = pact.getVotingPeriodIdByBusinessKey(businessId);
    pact.switchToUser(PactUser.VALIDATOR);
    pact.validateVotingPeriodInitialization(vmDeploymentId);

    await().atMost(MAX_VOTING_PERIOD_INITIALIZATION_TIME).untilAsserted(
        () -> bo.operationStatus(operationId).body("votingPeriodStatus.state", equalTo("INITIALIZED")));

    logger.info("Voting period initialized for operation " + operationId);

  }

}
