/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.mock.server.pact;

import ch.ge.ve.bo.mock.server.services.PactSimulator;
import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile({"development", "integration"})
@RequestMapping(path = "pact/operation/{operationId}")
public class TallyArchiveStatusController {

  private final PactSimulator pactSimulator;

  public TallyArchiveStatusController(PactSimulator pactSimulator) {
    this.pactSimulator = pactSimulator;
  }


  @GetMapping(path = "/tally-archive/status")
  @ResponseBody
  public TallyArchiveStatusVo getTallyStatus(@PathVariable("operationId") long operationId) {
    return pactSimulator.getTallyArchiveStatus(operationId);
  }


  @PutMapping(path = "/tally-archive/early-demand")
  @ResponseBody
  public void triggerTallyArchiveGeneration(@PathVariable("operationId") long operationId) {
    updateTallyArchiveStatus(operationId, TallyArchiveStatusVo.State.CREATION_IN_PROGRESS);
  }

  @GetMapping(path = "/update-tally-archive-status")
  @ResponseBody
  public void updateTallyArchiveStatus(@PathVariable("operationId") long operationId,
                                       @RequestParam("state") TallyArchiveStatusVo.State state) {
    switch (state) {
      case NOT_REQUESTED:
        pactSimulator.setVotingPeriodStatus(operationId, VotingPeriodStatusVo.State.INITIALIZED, null);
        break;
      case NOT_ENOUGH_VOTES_CAST:
        break;
      case CREATION_FAILED:
      case CREATED:
      case CREATION_IN_PROGRESS:
        pactSimulator.setVotingPeriodStatus(operationId, VotingPeriodStatusVo.State.CLOSED, null);
        break;
      default:
        throw new IllegalArgumentException("state " + state + "is not expected");
    }

    pactSimulator.setTallyArchiveStatus(operationId, state);
  }

}
