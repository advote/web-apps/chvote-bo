/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.dataset

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

/**
 * Set of tools used for testing.
 */
class DatasetTools {

    /** Date object format. */
    static DateTimeFormatter DATE_TIME_PARSER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss").withZone(ZoneId.systemDefault())
    static DateTimeFormatter DATE_PARSER = DateTimeFormatter.ofPattern("dd.MM.yyyy").withZone(ZoneId.systemDefault())

    /** Object graph builder for BO's entities. */
    static ObjectGraphBuilder entityBuilder = null

    /**
     * @return Object graph builder for BO's entities
     */
    static ObjectGraphBuilder entity() {
        if (entityBuilder == null) {
            entityBuilder = new ObjectGraphBuilder()
            entityBuilder.setIdentifierResolver(new ObjectGraphBuilder.IdentifierResolver() {
                @Override
                String getIdentifierFor(String s) {
                    return "uid"
                }
            })
            entityBuilder.classLoader = DatasetTools.classLoader
            entityBuilder.classNameResolver = "ch.ge.ve.bo.repository.entity"
        }
        return entityBuilder
    }

    /**
     * Create a new LocalDate instance from a formatted string "dd.MM.yyyy".
     *
     * @param date the string formatted date
     * @return the corresponding LocalDate object
     */
    static LocalDate localDate(final String date) {
        LocalDate.parse(date, DATE_PARSER)
    }

    /**
     * Create a new LocalDateTime instance from a formatted string "dd.MM.yyyy HH:mm:ss".
     *
     * @param dateTime the string formatted date time
     * @return the corresponding LocalDateTime object
     */
    static LocalDateTime localDateTime(final String dateTime) {
        LocalDateTime.parse(dateTime, DATE_TIME_PARSER)
    }
}
