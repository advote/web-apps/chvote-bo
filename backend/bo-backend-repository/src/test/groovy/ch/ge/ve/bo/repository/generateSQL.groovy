/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import javax.persistence.Persistence
import org.hibernate.jpa.AvailableSettings

final Properties persistenceProperties = new Properties()
def destination = "test.sql"

// XXX force persistence properties : remove database target
persistenceProperties.setProperty(org.hibernate.cfg.AvailableSettings.HBM2DDL_AUTO, "")
persistenceProperties.setProperty(AvailableSettings.SCHEMA_GEN_DATABASE_ACTION, "none")

// XXX force persistence properties : define create script target from metadata to destination
// persistenceProperties.setProperty(AvailableSettings.SCHEMA_GEN_CREATE_SCHEMAS, "true");
persistenceProperties.setProperty(AvailableSettings.SCHEMA_GEN_SCRIPTS_ACTION, "create")
persistenceProperties.setProperty(AvailableSettings.SCHEMA_GEN_CREATE_SOURCE, "metadata")
persistenceProperties.setProperty(AvailableSettings.SCHEMA_GEN_SCRIPTS_CREATE_TARGET, destination)

persistenceProperties.setProperty(AvailableSettings.JDBC_DRIVER, "org.h2.Driver")
persistenceProperties.setProperty(AvailableSettings.JDBC_URL, "jdbc:h2:mem:export")
persistenceProperties.setProperty(AvailableSettings.JDBC_USER, "sa")
persistenceProperties.setProperty(AvailableSettings.JDBC_PASSWORD, "")

persistenceProperties.setProperty(org.hibernate.cfg.AvailableSettings.DIALECT, "org.hibernate.dialect.Oracle10gDialect")

Persistence.generateSchema("bo", persistenceProperties)