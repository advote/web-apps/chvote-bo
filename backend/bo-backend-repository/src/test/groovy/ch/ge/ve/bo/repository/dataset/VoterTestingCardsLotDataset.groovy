/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.dataset

import static ch.ge.ve.bo.repository.dataset.DatasetTools.entity

import ch.ge.ve.bo.repository.entity.CardType
import ch.ge.ve.bo.repository.entity.Signature
import ch.ge.ve.bo.repository.entity.TestingCardLanguage
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot
import java.time.LocalDateTime

class VoterTestingCardsLotDataset {
  static VoterTestingCardsLot voterTestingCardsLot(CardType type) {
    entity().voterTestingCardsLot(
            id: 1,
            lotName: "lotName",
            cardType: type,
            count: 2,
            signature: Signature.M,
            firstName: "firstName",
            lastName: "lastName",
            birthday: LocalDateTime.of(2000, 1, 1, 10, 10),
            saveDate: LocalDateTime.of(2000, 1, 2, 12, 30),
            address1: "address1",
            address2: null,
            street: "street",
            postalCode: "1234",
            city: "city",
            country: "country",
            operation: OperationDataset.votingOperation(new OperationDataset.Options().with(OperationDataset.Options.id, 1)),
            language: TestingCardLanguage.FR,
            shouldPrint: true,
            doi: ["doi"]
    )
  }
}
