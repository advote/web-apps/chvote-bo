/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.dataset

import static ch.ge.ve.bo.repository.dataset.DatasetTools.entity
import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.repository.entity.File
import java.nio.charset.StandardCharsets

class FileDataset {

  static File file(Options options = new Options()) {
    entity().file(
            id: options.get(Options.id, 1),
            managementEntity: options.get(Options.managementEntity, "Canton de Genève"),
            fileName: "repositoryVotationTest.xml",
            businessKey: options.get(Options.withoutBusinessKey, false) ? null : '202012SGAvota3://all6c6f4b14b6d349ef97703f2af84ffbc6',
            operation: OperationDataset.votingOperation(new OperationDataset.Options().with(OperationDataset.Options.id, options.get(Options.operationId, 1))),
            type: options.get(Options.fileType, FileType.VOTATION_REPOSITORY),
            date: localDateTime('19.04.2015 00:00:00'),
            login: 'admin',
            saveDate: localDateTime('13.12.2016 00:00:00'),
            fileContent: options.get(Options.withoutContent, false) ? null : """<?xml version="1.0" encoding="UTF-8"?>
<ns:delivery xmlns:ns="http://www.ech.ch/xmlns/eCH-0159/4">
    <ns:deliveryHeader>
        <ns1:senderId xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">vota3://all</ns1:senderId>
        <ns1:messageId xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">6c6f4b14b6d349ef97703f2af84ffbc6</ns1:messageId>
        <ns1:messageType xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">vota3://referentielVotation</ns1:messageType>
        <ns1:sendingApplication xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">
            <ns1:manufacturer>DGSI - SIDP</ns1:manufacturer>
            <ns1:product>VOTA3</ns1:product>
            <ns1:productVersion>1.3.1.1-SN</ns1:productVersion>
        </ns1:sendingApplication>
        <ns1:messageDate xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">2017-06-16T14:08:33.596Z</ns1:messageDate>
        <ns1:action xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">1</ns1:action>
        <ns1:testDeliveryFlag xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">false</ns1:testDeliveryFlag>
    </ns:deliveryHeader>
    <ns:initialDelivery>
        <ns:contest>
            <ns1:contestIdentification xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">202012SGA</ns1:contestIdentification>
            <ns1:contestDate xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">2020-12-01+01:00</ns1:contestDate>
            <ns1:contestDescription xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">
                <ns1:contestDescriptionInfo>
                    <ns1:language>fr</ns1:language>
                    <ns1:contestDescription>202012SGA</ns1:contestDescription>
                </ns1:contestDescriptionInfo>
            </ns1:contestDescription>
        </ns:contest>
        <ns:voteInformation>
            <ns:vote>
                <ns1:voteIdentification xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">202012SGA-COM-6624</ns1:voteIdentification>
                <ns1:domainOfInfluenceIdentification xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">6624</ns1:domainOfInfluenceIdentification>
            </ns:vote>
            <ns:ballot>
                <ns1:ballotIdentification xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">COM_6624_Q1</ns1:ballotIdentification>
                <ns1:ballotPosition xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">1</ns1:ballotPosition>
                <ns1:standardBallot xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">
                    <ns1:questionIdentification>COM_6624_Q1</ns1:questionIdentification>
                    <ns1:answerType>
                        <ns1:answerType>2</ns1:answerType>
                    </ns1:answerType>
                    <ns1:ballotQuestion>
                        <ns1:ballotQuestionInfo>
                            <ns1:language>fr</ns1:language>
                            <ns1:ballotQuestion><![CDATA[<b>Sujet </b>01 de <u>Gy</u>]]></ns1:ballotQuestion>
                        </ns1:ballotQuestionInfo>
                    </ns1:ballotQuestion>
                </ns1:standardBallot>
                <ns1:extension xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">
                    <questionNumberLabel>1</questionNumberLabel>
                </ns1:extension>
            </ns:ballot>
        </ns:voteInformation>
    </ns:initialDelivery>
</ns:delivery>""".getBytes(StandardCharsets.UTF_8)
    )
  }

  static File electionOperationRepository() {
    entity().file(
            id: 2,
            fileContent: ''.getBytes(),
            fileName: "repositoryElectionTest.xml",
            businessKey: '202015SGAvota3://all6c6f4b14b6d349ef97703f2af84fabcd',
            operation: OperationDataset.electoralOperation(),
            type: FileType.VOTATION_REPOSITORY,
            date: localDateTime('19.04.2015 00:00:00'),
            login: 'admin',
            saveDate: localDateTime('13.12.2016 00:00:00')
    )
  }


  static class Options {
    static def id = new PossibleOption<Long>() {}
    static def managementEntity = new PossibleOption<String>() {}
    static def fileType = new PossibleOption<FileType>() {}
    static def withoutBusinessKey = new PossibleOption<Boolean>() {}
    static def withoutContent = new PossibleOption<Boolean>() {}
    static def operationId = new PossibleOption<Long>() {}

    final Map<PossibleOption, Object> values = new HashMap<>()

    def <T> T get(PossibleOption<T> option, T defaultValue) {
      return (T) values.getOrDefault(option, defaultValue)
    }

    def <T> Options with(PossibleOption<T> option, T value) {
      values.put(option, value)
      return this
    }


    static interface PossibleOption<T> {
    }
  }

}