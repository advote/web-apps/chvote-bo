/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository

import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.exception.AccessToProhibitedOperationException
import spock.lang.Specification

class SecuredOperationRepositoryImplTest extends Specification {
  def unsecured = Mock(UnsecuredOperationRepository)
  def secured = new SecuredOperationRepositoryImpl(unsecured)

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def "FindOne should successfully retrieve operation if user belongs to this management entity"() {
    given:
    unsecured.findById(2) >> Optional.of(OperationDataset.votingOperation())
    ConnectedUserTestUtils.connectUser("user1", "GE", OperationDataset.DEFAULT_MANAGEMENT_ENTITY)

    when:
    def operation = secured.findOne(2, true)

    then:
    2 == operation.id
  }

  def "FindOne should successfully retrieve operation if user's management entity is invited and there is no restriction"() {
    given:
    def operation = OperationDataset.votingOperation()
    operation.guestManagementEntities = ["Invited"].toSet()
    unsecured.findById(2) >> Optional.of(operation)
    ConnectedUserTestUtils.connectUser("user1", "GE", "Invited")

    when:
    def retriedOperation = secured.findOne(2, false)

    then:
    2 == retriedOperation.id
  }

  def "Technical user can always retrieve operation"() {
    given:
    unsecured.findById(2) >> Optional.of(OperationDataset.votingOperation())
    ConnectedUser.technical()

    when:
    def operation = secured.findOne(2, true)

    then:
    2 == operation.id

  }


  def "Printer can always retrieve operation"() {
    given:
    unsecured.findById(2) >> Optional.of(OperationDataset.votingOperation())
    ConnectedUserTestUtils.connectPrinter()

    when:
    def operation = secured.findOne(2, false)

    then:
    2 == operation.id
  }


  def "FindOne should not retrieve operation if user's management entity is invited but there is restriction"() {
    given:
    def operation = OperationDataset.votingOperation()
    operation.guestManagementEntities = ["Invited"].toSet()
    unsecured.findById(2) >> Optional.of(operation)
    ConnectedUserTestUtils.connectUser("user1", "GE", "Invited")

    when:
    def retriedOperation = secured.findOne(2, true)

    then:
    AccessToProhibitedOperationException exception = thrown()
    "Try to access operation 2 by user user1 from management entity Invited" == exception.message
  }


  def "FindOne should not retrieve operation if user belongs to another management entity"() {
    given:
    unsecured.findById(2) >> Optional.of(OperationDataset.votingOperation())
    ConnectedUserTestUtils.connectUser("user1", "GE", "dunkerke")

    when:
    secured.findOne(2, false)

    then:
    AccessToProhibitedOperationException exception = thrown()
    "Try to access operation 2 by user user1 from management entity dunkerke" == exception.message
  }


  def "FindOne should not retrieve operation if there is no user"() {
    given:
    unsecured.findById(2) >> Optional.of(OperationDataset.votingOperation())

    when:
    secured.findOne(2, false)

    then:
    AccessToProhibitedOperationException exception = thrown()
    "Try to access operation 2 without being connected" == exception.message
  }

  def "save should not block if it is a new entity"() {
    given:
    def operation = OperationDataset.votingOperation()
    operation.setManagementEntity(null)

    when:
    secured.save(operation)

    then:
    1 * unsecured.save(_) >> operation
  }

  def "save should block if it is not a new entity"() {
    given:
    def operation = OperationDataset.votingOperation()
    operation.setManagementEntity("an entity")
    ConnectedUserTestUtils.connectUser("user1", "GE", "dunkerke")

    when:
    secured.save(operation)

    then:
    AccessToProhibitedOperationException exception = thrown()
    "Try to access operation 2 by user user1 from management entity dunkerke" == exception.message
  }

}
