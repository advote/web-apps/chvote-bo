/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.security.OperationHolder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Parameters used to define a lot of testing cards
 */
@Entity
@Table(name = "BO_T_TESTING_CARDS_LOT")
@SequenceGenerator(
    name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_TESTING_CARDS_LOT")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "TCL_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN, column = @Column(name = "TCL_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "TCL_D_SAVE"))

public class VoterTestingCardsLot extends BaseEntity implements OperationHolder {

  @Column(name = "TCL_C_LOT_NAME")
  private String lotName;

  @Column(name = "TCL_C_CARD_TYPE")
  @Enumerated(EnumType.STRING)
  private CardType cardType;

  @Column(name = "TCL_N_COUNT")
  private Integer count;

  @Column(name = "TCL_C_SIGNATURE")
  @Enumerated(EnumType.STRING)
  private Signature signature;

  @Column(name = "TCL_C_FIRST_NAME")
  private String firstName;

  @Column(name = "TCL_C_LAST_NAME")
  private String lastName;

  @Column(name = "TCL_D_BIRTHDAY")
  private LocalDateTime birthday;

  @Column(name = "TCL_C_ADDRESS_1")
  private String address1;

  @Column(name = "TCL_C_ADDRESS_2")
  private String address2;

  @Column(name = "TCL_C_STREET")
  private String street;

  @Column(name = "TCL_C_POSTAL_CODE")
  private String postalCode;

  @Column(name = "TCL_C_CITY")
  private String city;

  @Column(name = "TCL_C_COUNTRY")
  private String country;

  @ManyToOne
  @JoinColumn(name = "TCL_OPE_N_ID", referencedColumnName = "OPE_N_ID")
  private Operation operation;

  @Column(name = "TCL_C_LANGUAGE")
  @Enumerated(EnumType.STRING)
  private TestingCardLanguage language;

  @Column(name = "TCL_N_SHOULD_PRINT")
  private boolean shouldPrint;

  @ElementCollection
  @CollectionTable(name = "BO_T_TESTING_CARDS_LOT_DOI", joinColumns = {@JoinColumn(name = "TCLD_N_LOT_ID")})
  @Column(name = "TCLD_C_DOI")
  private List<String> doi = new ArrayList<>();

  public String getLotName() {
    return lotName;
  }

  public void setLotName(String lotName) {
    this.lotName = lotName;
  }

  public CardType getCardType() {
    return cardType;
  }

  public void setCardType(CardType cardType) {
    this.cardType = cardType;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Signature getSignature() {
    return signature;
  }

  public void setSignature(Signature signature) {
    this.signature = signature;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public LocalDateTime getBirthday() {
    return birthday;
  }

  public void setBirthday(LocalDateTime birthday) {
    this.birthday = birthday;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public TestingCardLanguage getLanguage() {
    return language;
  }

  public void setLanguage(TestingCardLanguage language) {
    this.language = language;
  }

  public boolean isShouldPrint() {
    return shouldPrint;
  }

  public void setShouldPrint(boolean shouldPrint) {
    this.shouldPrint = shouldPrint;
  }

  public List<String> getDoi() {
    return doi;
  }

  public void setDoi(List<String> doi) {
    this.doi = doi;
  }

  public Operation getOperation() {
    return operation;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return true;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return cardType.isForConfiguration() ?
        OperationParameterType.CONFIGURATION : OperationParameterType.VOTING_MATERIAL;
  }
}