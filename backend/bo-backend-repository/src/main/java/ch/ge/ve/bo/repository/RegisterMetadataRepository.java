/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository;

import ch.ge.ve.bo.repository.entity.RegisterMetadata;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface managing entities {@link ch.ge.ve.bo.repository.entity.RegisterMetadata}.
 */
public interface RegisterMetadataRepository extends JpaRepository<RegisterMetadata, Long> {

  /**
   * find all {@link RegisterMetadata} for no operation id validated or not
   */
  List<RegisterMetadata> findAllByFileOperationIdAndValidated(Long fileOperationId, boolean validated);

  /**
   * find a {@link RegisterMetadata} for a given business key
   */
  Optional<RegisterMetadata> findByFileBusinessKey(String businessKey);

  /**
   * find a {@link RegisterMetadata} for a given business key validated or not
   */
  Optional<RegisterMetadata> findByFileBusinessKeyAndValidated(String businessKey, boolean validated);

  /**
   * delete a {@link RegisterMetadata} for a given business key
   */
  void deleteByFileBusinessKey(String businessKey);

  /**
   * Find all {@link RegisterMetadata} that has been validated or not
   */
  List<RegisterMetadata> findAllByValidated(boolean validated);


}
