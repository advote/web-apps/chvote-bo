/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository;

import ch.ge.ve.bo.repository.entity.HighlightedQuestion;
import ch.ge.ve.bo.repository.entity.Operation;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for highlighted question
 */
public interface HighlightedQuestionRepository extends JpaRepository<HighlightedQuestion, Long> {

  /**
   * find all highlighted questions for a given operation
   */
  List<HighlightedQuestion> findByOperation(Operation operation);
}
