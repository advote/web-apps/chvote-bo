/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository;

import ch.ge.ve.bo.repository.exception.ConnectedUserException;
import ch.ge.ve.bo.repository.exception.InvalidRealmTypeException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Entity representing a user connected
 */
public class ConnectedUser {
  static final         String            TECHNICAL_USER               = "technical";
  public static final  String            BELONGS_TO_REALM             = "BELONGS_TO_REALM:";
  public static final  String            REALM_PRINTER                = "REALM_PRINTER";
  private static final Predicate<String> IS_CANTON_ROLE               =
      role -> role.startsWith(BELONGS_TO_REALM);
  public static final  String            BELONGS_TO_MANAGEMENT_ENTITY = "BELONGS_TO_MANAGEMENT_ENTITY:";
  private static final Predicate<String> IS_MANAGEMENT_ENTITY_ROLE    =
      role -> role.startsWith(BELONGS_TO_MANAGEMENT_ENTITY);


  private static final ThreadLocal<ConnectedUser> currentConnectedUser = new ThreadLocal<>();
  public final         String                     login;
  public final         Set<String>                roles;
  public final         String                     realm;
  public final         String                     managementEntity;

  /**
   * Set a connected user should only be used in WebSecurityConnectedInterceptor or tests
   */
  public static void set(String login, Collection<String> roles) {
    currentConnectedUser.set(new ConnectedUser(login, roles));
  }

  /**
   * @return The current connected user associated with request thread
   */
  public static ConnectedUser get() {
    return currentConnectedUser.get();
  }

  /**
   * Checks the current user belongs to {@literal REALM_PRINTER} and returns the printerId it belongs to
   *
   * @return the printerId the user belongs to
   */
  public static String getPrinterId() {
    checkIsInPrinterRealm();
    return get().managementEntity;
  }

  /**
   * Checks the current user belongs to {@literal REALM_PRINTER}
   */
  public static void checkIsInPrinterRealm() {
    if (!get().isPrinter()) {
      throw new InvalidRealmTypeException(String.format("Operation not allowed for user %s belonging to realm %s",
                                                        get().login, get().realm));
    }
  }

  private ConnectedUser() {
    this.login = TECHNICAL_USER;
    this.realm = null;
    this.managementEntity = TECHNICAL_USER;
    this.roles = Collections.singleton("ROLE_TECHNICAL");
  }


  private ConnectedUser(String login, Collection<String> authorities) {
    this.login = login;
    realm = authorities.stream()
                       .filter(IS_CANTON_ROLE)
                       .findFirst()
                       .orElseThrow(() -> new ConnectedUserException("Cannot find canton from user authorities"))
                       .substring(BELONGS_TO_REALM.length()).trim();

    managementEntity = authorities.stream()
                                  .filter(IS_MANAGEMENT_ENTITY_ROLE)
                                  .findFirst()
                                  .orElseThrow(
                                      () -> new ConnectedUserException("Cannot find canton from user authorities"))
                                  .substring(BELONGS_TO_MANAGEMENT_ENTITY.length()).trim();

    this.roles = new HashSet<>(authorities);
    this.roles.removeIf(IS_CANTON_ROLE);
    this.roles.removeIf(IS_MANAGEMENT_ENTITY_ROLE);
  }

  /**
   * Cleanup connected user
   */
  public static void unset() {
    currentConnectedUser.remove();
  }

  /**
   * Insure that current user is technical for not user relative operation
   */
  public static void technical() {
    currentConnectedUser.set(new ConnectedUser());
  }

  /**
   * Define current connected user
   */
  public static void set(ConnectedUser connectedUser) {
    currentConnectedUser.set(connectedUser);
  }

  public boolean isPrinter() {
    return realm.equals(REALM_PRINTER);
  }

}
