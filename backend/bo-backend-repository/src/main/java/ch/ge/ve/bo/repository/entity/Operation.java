/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.MilestoneType;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.security.NoCheckForOperationHolder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity representing an electoral operation.
 */
@Entity
@Table(name = "BO_T_OPERATION")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_OPERATION")
@AttributeOverrides({
                        @AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "OPE_N_ID")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_LOGIN,
                                           column = @Column(name = "OPE_C_LOGIN")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "OPE_D_SAVE")),
                    })
@NoCheckForOperationHolder
public class Operation extends BaseEntity {

  @Column(name = "OPE_C_SHORT_LABEL")
  private String shortLabel;

  @Column(name = "OPE_C_LONG_LABEL")
  private String longLabel;

  @Column(name = "OPE_D_DATE")
  @SuppressWarnings("squid:S3437") // suppress Sonar check on serialized value-based objects
  private LocalDateTime date;

  @Column(name = "OPE_D_LAST_CONFIG_DATE")
  @SuppressWarnings("squid:S3437") // suppress Sonar check on serialized value-based objects
  private LocalDateTime lastConfigurationUpdateDate;


  @Column(name = "OPE_N_GRACE_PERIOD")
  private Integer gracePeriod;

  @Column(name = "OPE_C_REG_HASH")
  private String registerHash;

  @Column(name = "OPE_C_SIMULATION_NAME")
  private String simulationName;

  @Column(name = "OPE_C_DEPLOYMENT_TARGET")
  @Enumerated(EnumType.STRING)
  private DeploymentTarget deploymentTarget = DeploymentTarget.NOT_DEFINED;

  @Column(name = "OPE_D_LAST_VOT_MAT_UPD_DATE")
  @SuppressWarnings("squid:S3437")
  private LocalDateTime lastVotingMaterialUpdateDate;

  @Column(name = "OPE_D_LAST_VOT_PER_UPD_DATE")
  @SuppressWarnings("squid:S3437")
  private LocalDateTime lastVotingPeriodConfigUpdateDate;

  @Column(name = "OPE_C_VOTING_CARD_TITLE")
  private String votingCardTitle;

  @Column(name = "OPE_C_PRINTER_TEMPLATE")
  private String printerTemplate;

  @Column(name = "OPE_C_MANAGEMENT_ENTITY")
  private String managementEntity;

  @Column(name = "OPE_C_CANTON")
  private String canton;

  @Column(name = "OPE_N_IN_MODIFICATION")
  private boolean inModification;

  @ElementCollection
  @CollectionTable(name = "BO_T_GUEST_MGNT_ENTITY", joinColumns = {@JoinColumn(name = "GME_N_OPE_ID")})
  @Column(name = "GME_C_GUEST_MGNT_ENTITY")
  private Set<String> guestManagementEntities = new HashSet<>();


  @Column(name = "OPE_C_WORKFLOW_STEP", nullable = false)
  @Enumerated(EnumType.STRING)
  private WorkflowStep workflowStep = WorkflowStep.CONFIGURATION_NOT_SENT;


  public String getManagementEntity() {
    return managementEntity;
  }

  public void setManagementEntity(String managementEntity) {
    this.managementEntity = managementEntity;
  }

  @PrePersist
  void setup() {
    if (managementEntity == null) {
      setManagementEntity(ConnectedUser.get().managementEntity);
    }
    if (lastVotingMaterialUpdateDate == null) {
      lastVotingMaterialUpdateDate = LocalDateTime.now();
    }

    if (lastVotingPeriodConfigUpdateDate == null) {
      lastVotingPeriodConfigUpdateDate = LocalDateTime.now();
    }

    if (canton == null) {
      setCanton(ConnectedUser.get().realm);
    }
  }

  @OneToMany(mappedBy = "operation", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Milestone> milestones = new ArrayList<>();


  /**
   * constructor with minimal base configuration
   */
  public static Operation operation(String shortLabel, String longLabel, LocalDateTime date) {
    Operation operation = new Operation();
    operation.setShortLabel(shortLabel);
    operation.setLongLabel(longLabel);
    operation.date = date;
    operation.lastConfigurationUpdateDate = LocalDateTime.now();
    return operation;
  }


  public LocalDateTime getLastConfigurationUpdateDate() {
    return lastConfigurationUpdateDate;
  }

  public void setLastConfigurationUpdateDate(LocalDateTime lastConfigurationUpdateDate) {
    this.lastConfigurationUpdateDate = lastConfigurationUpdateDate;
  }

  public String getShortLabel() {
    return shortLabel;
  }

  public void setShortLabel(String shortLabel) {
    this.shortLabel = shortLabel;
  }

  public String getLongLabel() {
    return longLabel;
  }

  public void setLongLabel(String longLabel) {
    this.longLabel = longLabel;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  public Integer getGracePeriod() {
    return gracePeriod;
  }

  public void setGracePeriod(Integer gracePeriod) {
    this.gracePeriod = gracePeriod;
  }

  public List<Milestone> getMilestones() {
    return milestones;
  }


  /**
   * @return a milestone date for a given {@link MilestoneType}
   */
  public Optional<LocalDateTime> getMilestone(MilestoneType type) {
    if (milestones != null) {
      return milestones.stream().filter(ms -> ms.getType() == type)
                       .map(Milestone::getDate)
                       .findFirst();
    }
    return Optional.empty();
  }


  public void setMilestones(List<Milestone> milestones) {
    this.milestones.clear();
    if (milestones != null) {
      this.milestones.addAll(milestones);
    }
  }

  public String getRegisterHash() {
    return registerHash;
  }

  public void setRegisterHash(String registerHash) {
    this.registerHash = registerHash;
  }

  public String getSimulationName() {
    return simulationName;
  }

  public void setSimulationName(String simulationName) {
    this.simulationName = simulationName;
  }

  public DeploymentTarget getDeploymentTarget() {
    return deploymentTarget;
  }

  public void setDeploymentTarget(DeploymentTarget deploymentTarget) {
    this.deploymentTarget = deploymentTarget;
  }


  public String getVotingCardTitle() {
    return votingCardTitle;
  }

  public void setVotingCardTitle(String votingCardTitle) {
    this.votingCardTitle = votingCardTitle;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Operation)) {
      return false;
    }
    Operation operation = (Operation) o;
    return Objects.equals(shortLabel, operation.shortLabel) &&
           Objects.equals(date, operation.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(shortLabel, date);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Operation{");
    sb.append("shortLabel='").append(shortLabel).append('\'');
    sb.append(", longLabel='").append(longLabel).append('\'');
    sb.append(", date=").append(date != null ? date.format(DATE_FORMATTER) : null);
    sb.append(", gracePeriod=").append(gracePeriod);
    sb.append(", milestones=").append(milestones);
    sb.append('}');
    return sb.toString();
  }

  public LocalDateTime getLastVotingMaterialUpdateDate() {
    return lastVotingMaterialUpdateDate;
  }

  public void setLastVotingMaterialUpdateDate(LocalDateTime lastVotingMaterialUpdateDate) {
    this.lastVotingMaterialUpdateDate = lastVotingMaterialUpdateDate;
  }

  public void setPrinterTemplate(String printerTemplate) {
    this.printerTemplate = printerTemplate;
  }

  public String getPrinterTemplate() {
    return printerTemplate;
  }

  public Set<String> getGuestManagementEntities() {
    return guestManagementEntities;
  }

  public void setGuestManagementEntities(Set<String> guestManagementEntities) {
    this.guestManagementEntities = guestManagementEntities;
  }

  public LocalDateTime getLastVotingPeriodConfigUpdateDate() {
    return lastVotingPeriodConfigUpdateDate;
  }

  public void setLastVotingPeriodConfigUpdateDate(LocalDateTime lastVotingPeriodUpdateDate) {
    this.lastVotingPeriodConfigUpdateDate = lastVotingPeriodUpdateDate;
  }

  public boolean isInModification() {
    return inModification;
  }

  public void setInModification(boolean inModication) {
    this.inModification = inModication;
  }

  public String getCanton() {
    return canton;
  }

  public void setCanton(String canton) {
    this.canton = canton;
  }

  public WorkflowStep getWorkflowStep() {
    return workflowStep;
  }

  public void setWorkflowStep(WorkflowStep workflowStep) {
    this.workflowStep = workflowStep;
  }
}
