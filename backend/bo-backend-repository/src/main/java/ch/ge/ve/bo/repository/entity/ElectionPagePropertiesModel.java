/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.security.OperationHolder;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Election page properties model represents a template of configuration for an election site
 */
@Entity
@Table(name = "BO_T_ELECTION_PAGE_PROP_MODEL")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1,
                   sequenceName = "BO_S_ELECTION_PAGE_PROP_MODEL")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "EPPM_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN,column = @Column(name = "EPPM_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE,column = @Column(name = "EPPM_D_SAVE"))

public class ElectionPagePropertiesModel extends BaseEntity implements OperationHolder {

  @ManyToOne
  @JoinColumn(name = "EPPM_OPE_N_ID", referencedColumnName = "OPE_N_ID", nullable = false)
  private Operation operation;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "EPP_EPPM_N_ID", nullable = false)
  private List<ElectionPageProperties> mappings = new ArrayList<>();

  @Column(name = "EPPM_C_MODEL_NAME", nullable = false)
  private String modelName;

  @Column(name = "EPPM_N_CANDIDATE_SEARCH_FORM", nullable = false)
  private boolean displayCandidateSearchForm;

  @Column(name = "EPPM_N_CHANGE_ELECTORAL_LIST", nullable = false)
  private boolean allowChangeOfElectoralList;

  @Column(name = "EPPM_N_DISPLAY_EMPTY_POS", nullable = false)
  private boolean displayEmptyPosition;

  @Column(name = "EPPM_N_DISPLAY_POS_ON_COMPACT", nullable = false)
  private boolean displayCandidatePositionOnACompactBallotPaper;

  @Column(name = "EPPM_N_DISPLAY_POS_ON_MOD", nullable = false)
  private boolean displayCandidatePositionOnAModifiedBallotPaper;

  @Column(name = "EPPM_N_DISPLAY_SUFFRAGE_COUNT", nullable = false)
  private boolean displaySuffrageCount;

  @Column(name = "EPPM_N_DISP_LIST_VERIF_CODE", nullable = false)
  private boolean displayListVerificationCode;

  @Column(name = "EPPM_C_CAND_INFO_DISP_MODEL", nullable = false)
  private String candidateInformationDisplayModel;

  @Column(name = "EPPM_N_OPEN_CANDIDATURE", nullable = false)
  private boolean allowOpenCandidature;

  @Column(name = "EPPM_N_MULTIPLE_MANDATES", nullable = false)
  private boolean allowMultipleMandates;

  @Column(name = "EPPM_N_DISP_VOID_ON_EMPTY_BP", nullable = false)
  private boolean displayVoidOnEmptyBallotPaper;

  @Column(name = "EPPM_C_COLUMNS_VERIF_TABLE", nullable = false)
  private String displayedColumnsOnVerificationTable;

  @Column(name = "EPPM_C_COL_ORDER_VERIF_TABLE")
  private String columnsOrderOnVerificationTable;

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public String getModelName() {
    return modelName;
  }

  public void setModelName(String modelName) {
    this.modelName = modelName;
  }

  public boolean isDisplayCandidateSearchForm() {
    return displayCandidateSearchForm;
  }

  public void setDisplayCandidateSearchForm(boolean displayCandidateSearchForm) {
    this.displayCandidateSearchForm = displayCandidateSearchForm;
  }

  public boolean isAllowChangeOfElectoralList() {
    return allowChangeOfElectoralList;
  }

  public void setAllowChangeOfElectoralList(boolean allowChangeOfElectoralList) {
    this.allowChangeOfElectoralList = allowChangeOfElectoralList;
  }

  public boolean isDisplayEmptyPosition() {
    return displayEmptyPosition;
  }

  public void setDisplayEmptyPosition(boolean displayEmptyPosition) {
    this.displayEmptyPosition = displayEmptyPosition;
  }

  public boolean isDisplayCandidatePositionOnACompactBallotPaper() {
    return displayCandidatePositionOnACompactBallotPaper;
  }

  public void setDisplayCandidatePositionOnACompactBallotPaper(boolean displayCandidatePositionOnACompactBallotPaper) {
    this.displayCandidatePositionOnACompactBallotPaper = displayCandidatePositionOnACompactBallotPaper;
  }

  public boolean isDisplayCandidatePositionOnAModifiedBallotPaper() {
    return displayCandidatePositionOnAModifiedBallotPaper;
  }

  public void setDisplayCandidatePositionOnAModifiedBallotPaper(
      boolean displayCandidatePositionOnAModifiedBallotPaper) {
    this.displayCandidatePositionOnAModifiedBallotPaper = displayCandidatePositionOnAModifiedBallotPaper;
  }

  public boolean isDisplaySuffrageCount() {
    return displaySuffrageCount;
  }

  public void setDisplaySuffrageCount(boolean displaySuffrageCount) {
    this.displaySuffrageCount = displaySuffrageCount;
  }

  public boolean isDisplayListVerificationCode() {
    return displayListVerificationCode;
  }

  public void setDisplayListVerificationCode(boolean displayListVerificationCode) {
    this.displayListVerificationCode = displayListVerificationCode;
  }

  public String getCandidateInformationDisplayModel() {
    return candidateInformationDisplayModel;
  }

  public void setCandidateInformationDisplayModel(String candidateInformationDisplayModel) {
    this.candidateInformationDisplayModel = candidateInformationDisplayModel;
  }

  public boolean isAllowOpenCandidature() {
    return allowOpenCandidature;
  }

  public void setAllowOpenCandidature(boolean allowOpenCandidature) {
    this.allowOpenCandidature = allowOpenCandidature;
  }

  public boolean isAllowMultipleMandates() {
    return allowMultipleMandates;
  }

  public void setAllowMultipleMandates(boolean allowMultipleMandates) {
    this.allowMultipleMandates = allowMultipleMandates;
  }

  public boolean isDisplayVoidOnEmptyBallotPaper() {
    return displayVoidOnEmptyBallotPaper;
  }

  public void setDisplayVoidOnEmptyBallotPaper(boolean displayVoidOnEmptyBallotPaper) {
    this.displayVoidOnEmptyBallotPaper = displayVoidOnEmptyBallotPaper;
  }

  @Override
  public Operation getOperation() {
    return operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return true;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.CONFIGURATION;
  }

  public String getDisplayedColumnsOnVerificationTable() {
    return displayedColumnsOnVerificationTable;
  }

  public void setDisplayedColumnsOnVerificationTable(String displayedColumnsOnVerificationTable) {
    this.displayedColumnsOnVerificationTable = displayedColumnsOnVerificationTable;
  }

  public String getColumnsOrderOnVerificationTable() {
    return columnsOrderOnVerificationTable;
  }

  public void setColumnsOrderOnVerificationTable(String columnsOrderOnVerificationTable) {
    this.columnsOrderOnVerificationTable = columnsOrderOnVerificationTable;
  }

  public List<ElectionPageProperties> getMappings() {
    return mappings;
  }

  public void setMappings(List<ElectionPageProperties> electionPageProperties) {
    this.mappings = electionPageProperties;
  }
}
