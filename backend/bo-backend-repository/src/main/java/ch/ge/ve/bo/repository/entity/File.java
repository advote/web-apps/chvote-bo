/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.security.ManagementEntityHolder;
import ch.ge.ve.bo.repository.security.OperationHolder;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity representing an application file
 */
@Entity
@Table(name = "BO_T_FILES")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_FILES")
@AttributeOverrides({
                        @AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "FIL_N_ID")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_LOGIN,
                                           column = @Column(name = "FIL_C_LOGIN")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "FIL_D_SAVE"))
                    })
public class File extends BaseEntity implements OperationHolder, ManagementEntityHolder {

  @Column(name = "FIL_X_CONTENT")
  @Lob
  @Basic(fetch = FetchType.LAZY)
  private byte[] fileContent;

  @Column(name = "FIL_C_BUSINESS_KEY")
  private String businessKey;

  @Column(name = "FIL_C_NAME")
  private String fileName;

  @ManyToOne
  @JoinColumn(name = "FIL_OPE_N_ID", referencedColumnName = "OPE_N_ID")
  private Operation operation;

  @Column(name = "FIL_C_TYPE")
  @Enumerated(EnumType.STRING)
  private FileType type;

  @Column(name = "FIL_C_LANGUAGE")
  @Enumerated(EnumType.STRING)
  private Language language;

  @Column(name = "FIL_C_MANAGEMENT_ENTITY")
  private String managementEntity;

  @Column(name = "FIL_D_DATE")
  @SuppressWarnings("squid:S3437") // suppress Sonar check on serialized value-based objects
  private LocalDateTime date;

  public byte[] getFileContent() {
    return fileContent;
  }

  public void setFileContent(byte[] fileContent) {
    this.fileContent = fileContent;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getBusinessKey() {
    return businessKey;
  }

  public void setBusinessKey(String businessKey) {
    this.businessKey = businessKey;
  }

  @Override
  public Operation getOperation() {
    return operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return type.isRestrictedToInChargeManagementEntity();
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return type.getOperationParameterType();
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public FileType getType() {
    return type;
  }

  public void setType(FileType fileType) {
    this.type = fileType;
  }

  @Override
  public String getManagementEntity() {
    return managementEntity;
  }

  public void setManagementEntity(String managementEntity) {
    this.managementEntity = managementEntity;
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public void setDate(LocalDateTime date) {
    this.date = date;
  }

  @PrePersist
  void setup() {
    if (managementEntity == null) {
      setManagementEntity(ConnectedUser.get().managementEntity);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof File)) {
      return false;
    }
    File that = (File) o;
    return Objects.equals(businessKey, that.businessKey) &&
           Objects.equals(fileName, that.fileName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(businessKey, fileName);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("ApplicationFile{");
    sb.append("fileContent length=").append(fileContent != null ? fileContent.length : 0);
    sb.append(", businessKey='").append(businessKey).append('\'');
    sb.append(", fileName='").append(fileName).append('\'');
    sb.append(", type=").append(type);
    sb.append(", managementEntity=").append(managementEntity);
    sb.append(", date=").append(date != null ? date.format(DATE_FORMATTER) : null);
    sb.append('}');
    return sb.toString();
  }
}
