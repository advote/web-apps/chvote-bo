/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.security.NoCheckForOperationHolder;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * A download token is a privileged access to a file in the filesystem for a not connected frontend user.
 */
@Entity
@Table(name = "BO_T_DOWNLOAD_TOKEN")
@NoCheckForOperationHolder
public class DownloadToken {
  @Id
  @Column(name = "DT_C_ID")
  private String id;

  @Column(name = "DT_C_PATH")
  private String path;

  @ManyToOne
  @JoinColumn(name = "DT_OPE_N_ID", referencedColumnName = "OPE_N_ID")
  private Operation operation;

  /**
   * Login of user who create the token.
   */
  @Column(name = "DT_C_LOGIN")
  private String login;

  @Version
  @SuppressWarnings("squid:S3437") // suppress Sonar check on serialized value-based objects
  @Column(name = "DT_D_CREATION_DATE")
  private LocalDateTime creationDate;

  @PreUpdate
  @PrePersist
  void updateLogin() {
    setLogin(ConnectedUser.get().login);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public LocalDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public Operation getOperation() {
    return operation;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }
}
