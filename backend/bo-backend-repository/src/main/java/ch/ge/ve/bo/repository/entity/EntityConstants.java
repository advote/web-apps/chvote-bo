/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

/**
 * Constants used by entities.
 */
final class EntityConstants {

  private EntityConstants() {
  }

  /**
   * Entity's sequence generator key.
   */
  static final String SEQUENCE_GENERATOR = "sequenceGenerator";

  /**
   * Name of the "id" property.
   */
  static final String PROPERTY_ID = "id";

  /**
   * Name of the "login" property.
   */
  static final String PROPERTY_LOGIN = "login";

  /**
   * Name of the "saveDate" property.
   */
  static final String PROPERTY_SAVE = "saveDate";
}
