/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.security;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.exception.EntityShouldImplementOperationHolderException;
import java.io.Serializable;
import java.lang.reflect.TypeVariable;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.support.DefaultRepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

/**
 * Upgrade repository for enabling security and consistency check on OperationHolder repositories method
 */
public class OperationSecurityRepositoryFactoryBean<R extends JpaRepository<T, I>, T,
    I extends Serializable> extends JpaRepositoryFactoryBean<R, T, I> {


  private SecuredContext securedContext;


  /**
   * Default Constructor
   */
  public OperationSecurityRepositoryFactoryBean(Class<? extends R> repositoryInterface) {
    super(repositoryInterface);
  }


  @Override
  protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
    Class<?> domainType = new DefaultRepositoryMetadata(getObjectType()).getDomainType();
    if (OperationHolder.class.isAssignableFrom(domainType)) {
      return new OperationSecurityRepositoryFactory(entityManager, securedContext);
    }


    if (domainType.getAnnotation(NoCheckForOperationHolder.class) == null) {
      Stream.of(domainType.getMethods())
            .filter(m -> Operation.class.isAssignableFrom(m.getReturnType()))
            .findFirst()
            .ifPresent(method -> {
              throw new EntityShouldImplementOperationHolderException(method);
            });
    }

    return super.createRepositoryFactory(entityManager);
  }

  // Spring doesn't support it on the constructor for any reason
  @Autowired
  public void setSecuredContext(SecuredContext securedContext) {
    this.securedContext = securedContext;
  }

  private static class OperationSecurityRepositoryFactory
      extends JpaRepositoryFactory {


    private OperationSecurityRepositoryFactory(EntityManager em, SecuredContext securedContext) {
      super(em);
      this.addRepositoryProxyPostProcessor(
          (factory, repositoryInformation) ->
              factory.addAdvice((MethodBeforeAdvice) (method, objects, o) -> {
                if (!securedContext.isInSecureContext()) {
                  throw new IllegalAccessException(
                      String.format(
                          "method %s.%s%s has been called from an a unsecured context. " +
                          "Wrap method call with SecuredOperationHolderService utility methods",
                          repositoryInformation.getRepositoryInterface(), method.getName(),
                          Stream.of(method.getTypeParameters())
                                .map(TypeVariable::getName)
                                .collect(Collectors.joining(", ", "(", ")"))));
                }
              })
      );
    }


  }


}
