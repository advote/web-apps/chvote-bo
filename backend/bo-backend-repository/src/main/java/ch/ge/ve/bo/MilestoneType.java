/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo;

/**
 * Milestone's type for an electoral operation.
 */
public enum MilestoneType {
  SITE_VALIDATION,
  CERTIFICATION,
  PRINTER_FILES,
  BALLOT_BOX_INIT,
  SITE_OPEN,
  SITE_CLOSE,
  BALLOT_BOX_DECRYPT,
  RESULT_VALIDATION,
  DATA_DESTRUCTION
}
