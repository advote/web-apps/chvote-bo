/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.conf;

import ch.ge.ve.bo.repository.security.OperationSecurityRepositoryFactoryBean;
import ch.ge.ve.bo.repository.security.SecuredContext;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;

/**
 * Repository layer Spring configuration.
 */
@Configuration
@EnableJpaRepositories(basePackages = "ch.ge.ve.bo.repository",
                       repositoryFactoryBeanClass = OperationSecurityRepositoryFactoryBean.class)
public class RepositoryConfiguration {

  /**
   * @return a secured context to update operation holder without checks
   */
  @Bean
  public SecuredContext securedContext() {
    return new SecuredContext();
  }

  /**
   * Persistence unit name for bo.
   */
  private static final String PERSISTENCE_UNIT_NAME = "bo";


  /**
   * Create an entity manager factory
   */
  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(
      DataSource dataSource,
      JpaVendorAdapter jpaVendorAdapter) {
    LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
    entityManagerFactory.setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
    entityManagerFactory.setDataSource(dataSource);
    entityManagerFactory.setJpaDialect(new HibernateJpaDialect());
    entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter);
    return entityManagerFactory;
  }
}
