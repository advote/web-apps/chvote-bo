/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.entity.File;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface managing entities {@link File}.
 */
public interface FileRepository extends JpaRepository<File, Long> {

  /**
   * Retrieve the number of files of the given type for a given operation.
   *
   * @param operationId unique ID of the concerned operation
   * @param type        the seek file type
   *
   * @return the corresponding files count
   */
  int countByOperationIdAndType(long operationId, FileType type);

  /**
   * Retrieve all the files in database associated to a given operation.
   *
   * @param operationId unique ID of the concerned operation
   *
   * @return the corresponding list of files
   */
  List<File> findByOperationId(long operationId);

  /**
   * Retrieve all the files of the given type for a given operation.
   *
   * @param operationId unique ID of the concerned operation
   * @param type        the seek file type
   *
   * @return the corresponding list of files
   */
  List<File> findByOperationIdAndType(long operationId, FileType type);

  /**
   * Retrieve all the files of the given type and language for a given operation.
   *
   * @param operationId unique ID of the concerned operation
   * @param type        the seek file type
   * @param language    language code of the seek file
   *
   * @return the corresponding list of files
   */
  List<File> findByOperationIdAndTypeAndLanguage(long operationId, FileType type, Language language);

  /**
   * Retrieve all the files of the given types for a given operation.
   *
   * @param operationId unique ID of the concerned operation
   * @param types       the seek file types
   *
   * @return the corresponding list of files
   */
  List<File> findByOperationIdAndTypeIn(long operationId, Iterable<FileType> types);

  /**
   * Find an application file by its business key.
   *
   * @param businessKey the seek business key
   *
   * @return the corresponding application file or null if none match
   */
  Optional<File> findByBusinessKey(String businessKey);

  /**
   * Retrieve all applications files by its type.
   *
   * @param types the seek file types
   *
   * @return files
   */
  List<File> findByTypeIn(Set<FileType> types);
}
