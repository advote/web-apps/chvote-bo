/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository;

import ch.ge.ve.bo.repository.entity.Operation;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface managing entities {@link Operation}.
 */
public interface UnsecuredOperationRepository extends JpaRepository<Operation, Long> {

  /**
   * Method used to update register
   *
   * @return 0 if no update in register has been done since user validated it.
   */
  @Modifying(clearAutomatically = true)
  @Query("update Operation o set o.registerHash = :postRegisterHash " +
         "where o.id = :operationid " +
         "and (o.registerHash is null or o.registerHash=:preRegisterHash)")
  int getRegisterFilesLock(@Param("operationid") Long operationId,
                           @Param("preRegisterHash") String preRegisterHash,
                           @Param("postRegisterHash") String postRegisterHash);

  /**
   * Register hash is used to create a lock to avoid concurent acces to repository import
   */
  @Modifying()
  @Query("update Operation o set o.registerHash = :newHash where o.id = :operationid ")
  void updateRegisterHash(@Param("operationid") Long operationId, @Param("newHash") String newHash);


  /**
   * Update last configuration update date with to current moment
   */
  @Modifying()
  @Query("update Operation o set o.lastConfigurationUpdateDate = current_timestamp where o.id = :operationid ")
  void markConfigurationAsUpdated(@Param("operationid") long operationId);


  /**
   * Update last voting material update date with to current moment
   */
  @Modifying()
  @Query("update Operation o set o.lastVotingMaterialUpdateDate = current_timestamp where o.id = :operationid ")
  void markVotingMaterialAsUpdated(@Param("operationid") long operationId);


  /**
   * List operations that are visible to a given management entity
   */
  List<Operation> findDistinctByManagementEntityOrGuestManagementEntitiesEquals(
      String managementEntity, String guestManagementEntity);

  /**
   * List operations that are related to a given printer template
   */
  List<Operation> findByPrinterTemplateIn(Collection<String> printerTemplateIds);

  /**
   * Update last voting period update date with the current moment
   */
  @Modifying()
  @Query("update Operation o set o.lastVotingPeriodConfigUpdateDate = current_timestamp where o.id = :operationid ")
  void markVotingPeriodConfigAsUpdated(@Param("operationid") long operationId);
}
