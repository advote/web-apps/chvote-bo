/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.ConnectedUser;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 * Base abstract class identifying an entity.
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

  /**
   * Date object format.
   */
  public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

  /**
   * Entity's id.
   */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = EntityConstants.SEQUENCE_GENERATOR)
  private Long id;


  /**
   * Login of user who last modified the entity.
   */
  private String login;

  /**
   * Date and time of the last modification.
   */
  @SuppressWarnings("squid:S3437") // suppress Sonar check on serialized value-based objects
  private LocalDateTime saveDate;

  @PreUpdate
  @PrePersist
  void updateLogin() {
    saveDate = LocalDateTime.now();
    setLogin(ConnectedUser.get().login);
  }



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public LocalDateTime getSaveDate() {
    return saveDate;
  }

  public void setSaveDate(LocalDateTime saveDate) {
    this.saveDate = saveDate;
  }
}
