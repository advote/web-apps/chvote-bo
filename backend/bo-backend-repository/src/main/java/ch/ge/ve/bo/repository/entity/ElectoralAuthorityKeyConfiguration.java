/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.security.OperationHolder;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * link between and {@link Operation} and {@link ElectoralAuthorityKey}
 */
@Entity
@Table(name = "BO_T_ELECT_AUTH_KEY_CONFIG")
@SequenceGenerator(
    name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_ELECT_AUTH_KEY_CONFIG")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "EAKC_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN, column = @Column(name = "EAKC_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "EAKC_D_SAVE"))
public class ElectoralAuthorityKeyConfiguration extends BaseEntity implements OperationHolder {

  @OneToOne
  @JoinColumn(name = "EAKC_N_OPE_ID", nullable = false)
  private Operation operation;


  @ManyToOne
  @JoinColumn(name = "EAKC_N_ELEC_AUTH_KEY_ID", referencedColumnName = "EAK_N_ID", nullable = false)
  private ElectoralAuthorityKey electoralAuthorityKey;

  @Override
  public Operation getOperation() {
    return operation;
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return true;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.VOTING_PERIOD;
  }

  public void setOperation(Operation operation) {
    this.operation = operation;
  }

  public ElectoralAuthorityKey getElectoralAuthorityKey() {
    return electoralAuthorityKey;
  }

  public void setElectoralAuthorityKey(ElectoralAuthorityKey electoralAuthorityKey) {
    this.electoralAuthorityKey = electoralAuthorityKey;
  }
}
