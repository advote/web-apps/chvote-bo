/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Election page properties represent a link between a {@link ElectionPagePropertiesModel} and a ballot
 */
@Entity
@Table(name = "BO_T_ELECTION_PAGE_PROP")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1,
                   sequenceName = "BO_S_ELECTION_PAGE_PROP")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "EPP_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN,
                   column = @Column(name = "EPP_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "EPP_D_SAVE"))

public class ElectionPageProperties extends BaseEntity {

  @Column(name = "EPP_C_BALLOT", nullable = false)
  private String ballot;

  public String getBallot() {
    return ballot;
  }

  public void setBallot(String ballot) {
    this.ballot = ballot;
  }

}
