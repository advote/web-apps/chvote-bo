/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * enum representing voter testing card types
 */
public enum CardType {
  TEST_SITE_TESTING_CARD(true),
  PRODUCTION_TESTING_CARD(false),
  CONTROLLER_TESTING_CARD(false),
  PRINTER_TESTING_CARD(false),;

  private final boolean forConfiguration;

  CardType(boolean forConfiguration) {
    this.forConfiguration = forConfiguration;
  }

  public boolean isForConfiguration() {
    return forConfiguration;
  }

  /**
   * @return card type usable for voting material
   */
  public static Set<CardType> forVotingMaterial(){
    return Stream.of(CardType.values()).filter(v ->  !v.forConfiguration).collect(Collectors.toSet());
  }

  /**
   * @return card type usable for configuration
   */
  public static Set<CardType> forConfiguration(){
    return Stream.of(CardType.values()).filter(v ->  v.forConfiguration).collect(Collectors.toSet());
  }

}
