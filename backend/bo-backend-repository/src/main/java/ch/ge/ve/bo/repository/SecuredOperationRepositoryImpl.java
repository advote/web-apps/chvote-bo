/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.exception.AccessToProhibitedOperationException;
import java.util.List;
import javax.persistence.EntityNotFoundException;


/**
 * This class is a facade to UnsecuredOperationRepository bringing security to method call It controls : - User belongs
 * to the same management entity than the operation - User's management entity has been invited by the operation's
 * management entity Warning it cannot verify more specific rights
 */
public class SecuredOperationRepositoryImpl implements SecuredOperationRepository {

  private final UnsecuredOperationRepository operationRepository;

  /**
   * Default constructor
   */
  public SecuredOperationRepositoryImpl(UnsecuredOperationRepository operationRepository) {
    this.operationRepository = operationRepository;
  }

  @Override
  public void updateRegisterHash(Long operationId, String newHash) {
    findOne(operationId, false);
    operationRepository.updateRegisterHash(operationId, newHash);
  }


  private Operation assertUserCanRead(Operation operation) {

    if (ConnectedUser.get() == null) {
      throw new AccessToProhibitedOperationException(operation.getId());
    }
    if (ConnectedUser.get().login.equals(ConnectedUser.TECHNICAL_USER) || ConnectedUser.get().isPrinter()) {
      return operation;
    }

    if (!ConnectedUser.get().managementEntity.equals(operation.getManagementEntity()) &&
        !operation.getGuestManagementEntities().contains(ConnectedUser.get().managementEntity)) {
      throw new AccessToProhibitedOperationException(ConnectedUser.get(), operation.getId());
    }
    return operation;
  }


  @Override
  public void markConfigurationAsUpdated(long operationId, ConsistencyCheckCallback consistencyCheckCallback) {
    findOne(operationId, false);
    operationRepository.markConfigurationAsUpdated(operationId);
    consistencyCheckCallback.scheduleCheck();
  }

  @Override
  public void markVotingMaterialAsUpdated(long operationId, ConsistencyCheckCallback consistencyCheckCallback) {
    findOne(operationId, false);
    operationRepository.markVotingMaterialAsUpdated(operationId);
    consistencyCheckCallback.scheduleCheck();
  }

  /**
   * This function should be by default secured since we pass management entity as parameter
   */
  @Override
  public List<Operation> findAllByManagementEntity(String managementEntity) {
    return operationRepository
        .findDistinctByManagementEntityOrGuestManagementEntitiesEquals(managementEntity, managementEntity);
  }

  @Override
  public Operation save(Operation operation) {
    if (operation.getManagementEntity() != null) {
      assertUserCanRead(operation);
    }
    return operationRepository.save(operation);
  }

  @Override
  public Operation findOne(Long operationId, boolean restrictedToOperationManagementEntity) {
    Operation operation = operationRepository
        .findById(operationId)
        .orElseThrow(() -> new EntityNotFoundException("operation " + operationId));
    if (restrictedToOperationManagementEntity) {
      assertUserBelongsToOperationManagementEntity(operation);
    }

    return assertUserCanRead(operation);
  }

  private void assertUserBelongsToOperationManagementEntity(Operation operation) {
    if (!ConnectedUser.get().login.equals(ConnectedUser.TECHNICAL_USER) &&
        !ConnectedUser.get().managementEntity.equals(operation.getManagementEntity())) {
      throw new AccessToProhibitedOperationException(ConnectedUser.get(), operation.getId());
    }
  }

  @Override
  public int getRegisterFilesLock(Long operationId, String preRegisterHash, String postRegisterHash) {
    findOne(operationId, false);
    return operationRepository.getRegisterFilesLock(operationId, preRegisterHash, postRegisterHash);
  }

  @Override
  public void markVotingPeriodConfigAsUpdated(Long operationId) {
    findOne(operationId, false);
    operationRepository.markVotingPeriodConfigAsUpdated(operationId);
  }


}
