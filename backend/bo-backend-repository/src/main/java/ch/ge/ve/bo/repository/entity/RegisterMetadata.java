/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;

import ch.ge.ve.bo.repository.security.ManagementEntityHolder;
import ch.ge.ve.bo.repository.security.OperationHolder;
import java.util.Objects;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Metadata for {@link File} representing a register
 */
@Entity
@Table(name = "BO_T_REGISTER_METADATA")
@SequenceGenerator(
    name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1, sequenceName = "BO_S_REGISTER_METADATA")
@AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "RMD_N_ID"))
@AttributeOverride(name = EntityConstants.PROPERTY_LOGIN, column = @Column(name = "RMD_C_LOGIN"))
@AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "RMD_D_SAVE"))

public class RegisterMetadata extends BaseEntity implements OperationHolder, ManagementEntityHolder {

  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "RMD_FIL_N_ID")
  private File file;

  @Column(name = "RMD_N_VALIDATED")
  private Boolean validated;

  @Lob
  @Column(name = "RMD_X_REPORT")
  private String report;

  @Lob
  @Column(name = "RMD_X_HASH_TABLE")
  private String hashTable;

  @Column(name = "RMD_C_MESSAGE_UID")
  private String messageUniqueId;

  @Column(name = "OPE_C_PRE_REG_HASH")
  private String preRegisterHash;

  @Column(name = "OPE_C_POST_REG_HASH")
  private String postRegisterHash;

  public String getPreRegisterHash() {
    return preRegisterHash;
  }

  public void setPreRegisterHash(String preRegisterHash) {
    this.preRegisterHash = preRegisterHash;
  }

  public String getPostRegisterHash() {
    return postRegisterHash;
  }

  public void setPostRegisterHash(String postRegisterHash) {
    this.postRegisterHash = postRegisterHash;
  }

  public File getFile() {
    return file;
  }

  public void setFile(File file) {
    this.file = file;
  }

  public Boolean getValidated() {
    return validated;
  }

  public void setValidated(Boolean validated) {
    this.validated = validated;
  }

  public String getReport() {
    return report;
  }

  public void setReport(String report) {
    this.report = report;
  }

  public String getHashTable() {
    return hashTable;
  }

  public void setHashTable(String hashTable) {
    this.hashTable = hashTable;
  }

  public String getMessageUniqueId() {
    return messageUniqueId;
  }

  public void setMessageUniqueId(String messageUniqueId) {
    this.messageUniqueId = messageUniqueId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    RegisterMetadata that = (RegisterMetadata) o;

    return Objects.equals(file, that.file);
  }

  @Override
  public int hashCode() {
    return file != null ? file.hashCode() : 0;
  }

  @Override
  public Operation getOperation() {
    return file.getOperation();
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return false;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.VOTING_MATERIAL;
  }

  @Override
  public String getManagementEntity() {
    return file.getManagementEntity();
  }
}
