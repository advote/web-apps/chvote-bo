/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.repository.entity;


import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.security.OperationHolder;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity representing a localized question it is part of an {@link HighlightedQuestion}
 */
@Entity
@Table(name = "BO_T_LOCALIZED_HLIGH_QUESTION")
@SequenceGenerator(name = EntityConstants.SEQUENCE_GENERATOR, allocationSize = 1,
                   sequenceName = "BO_S_LOCALIZED_HLIGH_QUESTION")
@AttributeOverrides({
                        @AttributeOverride(name = EntityConstants.PROPERTY_ID, column = @Column(name = "LHQ_N_ID")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_LOGIN,
                                           column = @Column(name = "LHQ_C_LOGIN")),
                        @AttributeOverride(name = EntityConstants.PROPERTY_SAVE, column = @Column(name = "LHQ_D_SAVE"))
                    })
public class LocalizedHighlightedQuestion extends BaseEntity implements OperationHolder {

  @Lob
  @Column(name = "LHQ_X_CONTENT")
  @Basic(fetch = FetchType.LAZY)
  private byte[] fileContent;

  @Column(name = "LHQ_C_FILENAME")
  private String fileName;

  @Column(name = "LHQ_C_LANGUAGE")
  @Enumerated(EnumType.STRING)
  private Language language;

  @Column(name = "LHQ_C_LOCALIZED_QUESTION")
  private String localizedQuestion;

  @ManyToOne
  @JoinColumn(name = "LHQ_HQU_N_ID", referencedColumnName = "HQU_N_ID")
  private HighlightedQuestion question;

  public byte[] getFileContent() {
    return fileContent;
  }

  public void setFileContent(byte[] fileContent) {
    this.fileContent = fileContent;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public Language getLanguage() {
    return language;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

  public String getLocalizedQuestion() {
    return localizedQuestion;
  }

  public void setLocalizedQuestion(String localizedQuestion) {
    this.localizedQuestion = localizedQuestion;
  }

  public HighlightedQuestion getQuestion() {
    return question;
  }

  public void setQuestion(HighlightedQuestion question) {
    this.question = question;
  }

  @Override
  public Operation getOperation() {
    return question.getOperation();
  }

  @Override
  public boolean onlyManagementEntityInChargeCanCreate() {
    return true;
  }

  @Override
  public OperationParameterType getOperationParameterType() {
    return OperationParameterType.CONFIGURATION;
  }
}
