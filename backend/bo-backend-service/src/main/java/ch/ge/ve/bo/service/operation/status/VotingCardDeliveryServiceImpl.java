/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo;
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.EnumSet;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang.StringUtils;

/**
 * Default implementation of VotingCardDeliveryService
 */
public class VotingCardDeliveryServiceImpl implements VotingCardDeliveryService {
  private final OperationStatusService operationStatusService;
  private final PactConfiguration      pactConfiguration;
  private final PrinterTemplateService printerTemplateService;


  /**
   * Default constructor
   */
  public VotingCardDeliveryServiceImpl(OperationStatusService operationStatusService,
                                       PactConfiguration pactConfiguration,
                                       PrinterTemplateService printerTemplateService) {
    this.operationStatusService = operationStatusService;
    this.pactConfiguration = pactConfiguration;
    this.printerTemplateService = printerTemplateService;
  }


  @Override
  public FileVo getGeneratedCardsForTestSite(long operationId) throws BusinessException {
    ConfigurationStatusVo configurationStatus = operationStatusService.getStatus(operationId).getConfigurationStatus();

    if (EnumSet.of(ConfigurationStatusVo.State.INCOMPLETE, ConfigurationStatusVo.State.COMPLETE)
               .contains(configurationStatus.getState())) {
      throw new BusinessException("operationSummary.configuration.errors.noTestSiteAvailable");
    }

    if (StringUtils.isBlank(configurationStatus.getGeneratedVotingCardLocation())) {
      throw new BusinessException("operationSummary.configuration.errors.noVotingMaterialAvailable");
    }

    return getFileVoByLocation(configurationStatus.getGeneratedVotingCardLocation());
  }

  @Override
  public FileVo getGeneratedNonPrintableCardsForProduction(long operationId) throws BusinessException {
    try {
      VotingMaterialStatusVo votingMaterialStatus =
          operationStatusService.getStatus(operationId).getVotingMaterialStatus();

      if (votingMaterialStatus.getState() != VotingMaterialStatusVo.State.CREATED &&
          votingMaterialStatus.getState() != VotingMaterialStatusVo.State.VALIDATED) {
        throw new BusinessException("operationSummary.votingMaterial.errors.notCreated");
      }

      PrinterConfiguration printer = getPrinterConfiguration();

      final Map<String, String> votingCardLocations = votingMaterialStatus.getVotingCardLocations();
      if (votingCardLocations == null || votingCardLocations.get(printer.id) == null) {
        throw new BusinessException("operationSummary.votingMaterial.errors.noPrinterFileForThisPrinter");
      }

      return getFileVoByLocation(votingCardLocations.get(printer.id));
    } catch (PactRequestException e) {
      throw new TechnicalException(e);
    }
  }


  private PrinterConfiguration getPrinterConfiguration() throws BusinessException {
    return Optional.of(printerTemplateService.getVirtualPrinterForTestCard())
                   .orElseThrow(() -> new BusinessException(
                       "operationSummary.votingMaterial.errors.noVirtualPrinterConfigurationFound"));
  }



  /**
   * Call PACT to get File by it's location
   *
   * @param location of the file
   *
   * @return FileVo
   */
  private FileVo getFileVoByLocation(String location) {
    Path path = Paths.get(pactConfiguration.getVotingMaterialBaseLocation(), location);
    if (!path.toFile().exists()) {
      throw new TechnicalException(String.format("File %s does not exist", path));
    }

    try {
      return new FileVo(path.getFileName().toString(), Files.readAllBytes(path));
    } catch (Exception e) {
      throw new TechnicalException(e);
    }
  }
}
