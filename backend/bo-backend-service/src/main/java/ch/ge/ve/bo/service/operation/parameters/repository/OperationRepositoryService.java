/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.RepositoryFileVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.RepositoryImportResult;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

/**
 * Operation's repository services.
 */
public interface OperationRepositoryService {

  /**
   * Import voting repositories in eCH-0157 format (version 2)
   *
   * @param operationId concerned operation's unique ID
   * @param fileName    name's file
   * @param inputStream content's file
   *
   * @return the imported repository file
   *
   * @throws BusinessException if an error occured during the import
   */
  RepositoryImportResult importFile(long operationId, String fileName, InputStream inputStream)
      throws BusinessException;

  /**
   * Retrieves the repository files information (without the file's content) linked to the given operation ID.
   *
   * @param operationId the concerned operation's ID
   *
   * @return the liste of repository files associated to this operation or an empty list if no repository file was
   * imported for this operation
   */
  List<RepositoryFileVo> getFiles(long operationId);

  /**
   * Retrieve the full votation repository file (with the file's content) identified by its ID.
   *
   * @param fileId the repository file's ID
   *
   * @return the corresponding repository file
   */
  RepositoryFileVo getFile(long fileId);


  /**
   * Retrieving all DOIs for a specific operation
   * @param operationId id for Retrieve DOIs
   * @return a Set<DomainOfInfluenceVo> with all DOIs
   */
  Set<DomainOfInfluenceVo> getAllDomainOfInfluenceForOperation(long operationId);

  /**
   * Get all ballots defined in register files of an operation
   */
  Set<String> getAllBallotsForOperation(long operationId, boolean includeElection, boolean includeVoting);


  /**
   * Replace repository
   *
   * @param operationId concerned operation's unique ID
   * @param oldId identifier of file to replace
   * @param fileName    name's file
   * @param inputStream content's file
   *
   * @return the imported repository file
   *
   * @throws BusinessException if an error occured during the import
   */
  RepositoryImportResult replaceFile(long operationId, long oldId, String fileName, InputStream inputStream)
      throws BusinessException;
}
