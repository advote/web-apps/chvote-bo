/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.doi.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;

/**
 * Value object representing a domain of influence for an operation
 */
public class DomainOfInfluenceVo {

  public final String type;
  public final String id;
  public final String name;
  public final String shortName;

  /**
   * Default constructor
   */
  @JsonCreator
  public DomainOfInfluenceVo(@JsonProperty("type") String type,
                             @JsonProperty("id") String id,
                             @JsonProperty("name") String name,
                             @JsonProperty("shortName") String shortName) {
    this.type = type;
    this.id = id;
    this.name = name;
    this.shortName = shortName;
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, id);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    final DomainOfInfluenceVo other = (DomainOfInfluenceVo) obj;
    return Objects.equals(this.type, other.type)
           && Objects.equals(this.id, other.id);
  }
}
