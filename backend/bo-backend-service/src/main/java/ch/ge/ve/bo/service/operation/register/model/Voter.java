/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

import ch.ge.ve.bo.service.model.CountingCircleVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a voter in an operation register.
 */
public class Voter {

  public enum VoterType {
    SWISS, SWISS_ABROAD, FOREIGNER
  }

  private       String           sex;
  private       String           title;
  private       String           firstName;
  private       String           lastName;
  private       String           addressLine1;
  private       String           addressLine2;
  private       String           street;
  private       String           town;
  private       String           zipCode;
  private       LocalDate        dateOfBirth;
  private       String           identifier;
  private       VoterType        voterType;
  private       String           municipality;
  private       String           lang;
  private       Integer          postageCode;
  private       String           municipalityId;
  private       String           canton;
  private final int              lineNumber;
  private final int              columnNumber;
  private       CountingCircleVo countingCircle;

  /**
   * Default constructor
   */
  public Voter(int lineNumber, int columnNumber) {
    this.lineNumber = lineNumber;
    this.columnNumber = columnNumber;
  }

  private       String                    hash;
  private       boolean                   knownDoi          = false;
  private final List<DomainOfInfluenceVo> domainOfInfluence = new ArrayList<>();

  public int getLineNumber() {
    return lineNumber;
  }

  public int getColumnNumber() {
    return columnNumber;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }

  /**
   * Add a domain of influance for a voter
   */
  public void addDoi(DomainOfInfluenceVo doi) {
    this.domainOfInfluence.add(doi);
  }

  public boolean isKnownDoi() {
    return knownDoi;
  }

  public void setKnownDoi(boolean knownDoi) {
    this.knownDoi = knownDoi;
  }

  public List<DomainOfInfluenceVo> getDomainOfInfluence() {
    return domainOfInfluence;
  }


  public VoterType getVoterType() {
    return voterType;
  }

  public void setVoterType(VoterType voterType) {
    this.voterType = voterType;
  }

  public String getLang() {
    return lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public int getPostageCode() {
    return postageCode;
  }

  public void setPostageCode(int postageCode) {
    this.postageCode = postageCode;
  }

  public String getMunicipality() {
    return municipality;
  }

  public void setMunicipality(String municipality) {
    this.municipality = municipality;
  }

  public void setCanton(String canton) {
    this.canton = canton;
  }

  public String getCanton() {
    return canton;
  }

  public String getMunicipalityId() {
    return municipalityId;
  }

  public void setMunicipalityId(String municipalityId) {
    this.municipalityId = municipalityId;
  }

  public void setCountingCircle(CountingCircleVo countingCircle) {
    this.countingCircle = countingCircle;
  }

  public CountingCircleVo getCountingCircle() {
    return countingCircle;
  }
}

