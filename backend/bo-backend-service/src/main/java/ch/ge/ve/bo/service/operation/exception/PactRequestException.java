/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.exception;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;

import ch.ge.ve.bo.service.exception.BusinessException;
import org.apache.http.client.HttpResponseException;

/**
 * Exception throned when a request for pact has failed
 */
public class PactRequestException extends BusinessException {


  private PactRequestException(String messageKey, Throwable cause, String... parameters) {
    super(messageKey, cause, parameters);
  }


  /**
   * Constructor method
   */
  public static PactRequestException of(HttpResponseException cause, String url) {
    switch (cause.getStatusCode()) {
      case SC_BAD_REQUEST:
        return new PactRequestException("global.errors.pact.request.bad_request", cause);
      case SC_NOT_FOUND:
        return new PactRequestException("global.errors.pact.request.not_found", cause, url);
      default:
        return new PactRequestException("global.errors.pact.request.unknown",
                                        cause,
                                        url,
                                        String.valueOf(cause.getStatusCode()),
                                        cause.getMessage());
    }
  }
}
