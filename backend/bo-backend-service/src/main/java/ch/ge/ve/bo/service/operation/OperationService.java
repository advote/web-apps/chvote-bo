/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.exception.CannotTargetProduction;
import ch.ge.ve.bo.service.operation.exception.CannotTargetSimulation;
import ch.ge.ve.bo.service.operation.exception.PastDateException;
import ch.ge.ve.bo.service.operation.model.BaseConfiguration;
import ch.ge.ve.bo.service.operation.model.MilestoneConfiguration;
import ch.ge.ve.bo.service.operation.model.OperationVo;
import java.util.List;

/**
 * Electoral operations services.
 */
public interface OperationService {

  /**
   * @return all the available electoral operations.
   */
  List<OperationVo> findAll();

  /**
   * Create a new operation.
   *
   * @param baseConfig Value object representing base information of an operation
   *
   * @return the saved operation's ID
   *
   * @throws PastDateException if the operation's date is in the past
   */
  long create(BaseConfiguration baseConfig) throws PastDateException;

  /**
   * Update base information of a given operation
   */
  OperationVo updateBaseConfiguration(Long operationId, BaseConfiguration baseConfig) throws BusinessException;

  /**
   *
   * Update milestones of an operation.
   *
   */
  OperationVo updateMilestones(long operationId, MilestoneConfiguration milestoneConfig) throws BusinessException;

  /**
   * Retrieve an operation identified by its unique ID.
   *
   * @param operationId the operation's ID
   *
   * @return the corresponding operation object
   */
  OperationVo findOne(long operationId, boolean restrictedToOperationManagementEntity);

  /**
   * update operation to target deployment in simulation
   */
  OperationVo targetSimulation(long id, String simulationName) throws CannotTargetSimulation;

  /**
   * update operation to target deployment in real
   */
  OperationVo targetReal(long id) throws CannotTargetProduction;

}
