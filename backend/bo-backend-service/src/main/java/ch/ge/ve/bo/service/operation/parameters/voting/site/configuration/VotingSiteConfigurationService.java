/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.voting.site.configuration;

import ch.ge.ve.bo.service.operation.model.VotingSiteConfigurationVo;
import java.util.Optional;
import javax.transaction.Transactional;

/**
 * Service used to create and retrieve voting site specific configuration
 *
 * @see ch.ge.ve.bo.repository.entity.VotingSiteConfiguration
 */
public interface VotingSiteConfigurationService {

  /**
   * Save or update existing {@link ch.ge.ve.bo.repository.entity.VotingSiteConfiguration} for a given operation
   */
  @Transactional
  void saveOrUpdate(VotingSiteConfigurationVo votingSiteConfigurationVo, Long operationId);

  /**
   * retrieve existing {@link ch.ge.ve.bo.repository.entity.VotingSiteConfiguration} for a given operation
   */
  Optional<VotingSiteConfigurationVo> findForOperation(long operationId);
}
