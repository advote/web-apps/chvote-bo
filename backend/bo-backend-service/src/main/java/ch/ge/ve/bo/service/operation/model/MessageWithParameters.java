/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import java.util.HashMap;
import java.util.Map;

/**
 * message key with parameter to replace in frontend
 */
public class MessageWithParameters {
  public final String              message;
  public final Map<String, String> parameters = new HashMap<>();

  /**
   * Default constructor
   */
  public MessageWithParameters(String message) {
    this.message = message;
  }


  /**
   * Add a parameter to existing one
   */
  public MessageWithParameters addParams(String key, String value) {
    if (value != null) {
      parameters.put(key, value);
    }
    return this;
  }


}
