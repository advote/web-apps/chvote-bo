/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import static ch.ge.ve.bo.service.utils.PactServiceUtils.attachCredentialsToRequest;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.WorkflowStep;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.exception.CloseVotingPeriodException;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.ModificationMode;
import ch.ge.ve.bo.service.operation.model.OperationStatusVo;
import ch.ge.ve.bo.service.operation.model.TallyArchiveStatusVo;
import ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.EnumSet;
import javax.transaction.Transactional;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;

/**
 * Default implementation of {@link OperationWorkflowService}
 */
public class OperationWorkflowServiceImpl implements OperationWorkflowService {
  private final OperationStatusService     operationStatusService;
  private final SecuredOperationRepository securedOperationRepository;
  private final HttpClient                 httpClient;
  private final PactConfiguration          pactConfiguration;

  /**
   * Default constructor
   */
  public OperationWorkflowServiceImpl(OperationStatusService operationStatusService,
                                      SecuredOperationRepository securedOperationRepository, HttpClient httpClient,
                                      PactConfiguration pactConfiguration) {
    this.operationStatusService = operationStatusService;
    this.securedOperationRepository = securedOperationRepository;
    this.httpClient = httpClient;
    this.pactConfiguration = pactConfiguration;
  }


  @Override
  @Transactional
  public void suspendModificationMode(long operationId) {
    Operation operation = securedOperationRepository.findOne(operationId, true);
    operation.setInModification(false);
    securedOperationRepository.save(operation);
  }

  @Override
  @Transactional
  public void setWorkflowStep(long operationId, WorkflowStep step) {
    Operation operation = securedOperationRepository.findOne(operationId, true);
    if (!operation.isInModification()) {
      operation.setWorkflowStep(step);
      securedOperationRepository.save(operation);
    }
  }

  @Override
  @Transactional
  public void switchToModificationMode(Long operationId) throws BusinessException {
    Operation operation = securedOperationRepository.findOne(operationId, true);
    ModificationMode modificationMode = operationStatusService.getModificationMode(operation);
    if (EnumSet.of(ModificationMode.PARTIALLY_MODIFIABLE, ModificationMode.FULLY_MODIFIABLE)
               .contains(modificationMode)) {
      operation.setInModification(true);
      securedOperationRepository.save(operation);
    } else {
      throw new CannotSwitchToModificationMode(operationId);
    }
  }


  @Override
  public void validateTestSite(long operationId) throws BusinessException {
    doPostOnPactInTheNameOf(operationId, pactConfiguration.getUrl().getValidateTestSite());
  }


  @Override
  public void invalidateTestSite(long operationId) throws BusinessException {
    doPostOnPactInTheNameOf(operationId, pactConfiguration.getUrl().getInvalidateTestSite());
  }

  @Override
  public void validateVotingMaterial(long operationId) throws BusinessException {
    doPostOnPactInTheNameOf(operationId, pactConfiguration.getUrl().getValidateVotingMaterial());
  }

  @Override
  public void closeVotingPeriod(long operationId) throws BusinessException {
    OperationStatusVo operationStatus = operationStatusService.getStatus(operationId);

    // Defensive check. Front end should prevent such errors
    if (operationStatus.getDeploymentTarget() != DeploymentTarget.SIMULATION) {
      throw CloseVotingPeriodException.operationIsNotInSimulation();
    }
    if (operationStatus.getTallyArchiveStatus().getState() == TallyArchiveStatusVo.State.CREATION_IN_PROGRESS) {
      throw CloseVotingPeriodException.tallyArchiveGenerationAlreadyRequested();
    }
    if (operationStatus.getTallyArchiveStatus().getState() == TallyArchiveStatusVo.State.CREATED) {
      throw CloseVotingPeriodException.tallyArchiveGenerationAlreadyDone();
    }
    if (operationStatus.getVotingPeriodStatus().state != VotingPeriodStatusVo.State.INITIALIZED &&
        operationStatus.getTallyArchiveStatus().getState() != TallyArchiveStatusVo.State.CREATION_FAILED) {
      throw CloseVotingPeriodException.votingPeriodNotInitialized();
    }

    doPutOnPactInTheNameOf(operationId, pactConfiguration.getUrl().getTriggerTallyArchive());
  }


  private void doPostOnPactInTheNameOf(long operationId, String action) throws PactRequestException {
    executePactRequest(new HttpPost(MessageFormat.format(action, operationId)));
  }

  private void doPutOnPactInTheNameOf(long operationId, String action) throws PactRequestException {
    executePactRequest(new HttpPut(MessageFormat.format(action, operationId)));
  }

  private void executePactRequest(HttpEntityEnclosingRequestBase request) throws PactRequestException {
    request.setEntity(
        new UrlEncodedFormEntity(Collections.singleton(new BasicNameValuePair("user", ConnectedUser.get().login))));
    attachCredentialsToRequest(request, pactConfiguration.getUsername(), pactConfiguration.getPassword());
    try {
      httpClient.execute(request, new BasicResponseHandler());
    } catch (HttpResponseException responseException) {
      throw PactRequestException.of(responseException, request.getURI().toString());
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }

}
