/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Map;

/**
 * Global operation status aggregating
 * {@link ConfigurationStatusVo}
 * {@link VotingMaterialStatusVo}
 * {@link VotingPeriodStatusVo}
 * {@link TallyArchiveStatusVo}
 */
public class OperationStatusVo {
  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
  private static final String            PARAM_KEY_USER      = "user";
  private static final String            PARAM_KEY_DATE      = "date";
  private static final String            PARAM_KEY_COMMENT   = "comment";
  private static final String            PARAM_KEY_REASON    = "reason";

  private final ConfigurationStatusVo  configurationStatus;
  private final VotingMaterialStatusVo votingMaterialStatus;
  private final VotingPeriodStatusVo   votingPeriodStatus;
  private final TallyArchiveStatusVo   tallyArchiveStatus;
  private final DeploymentTarget       deploymentTarget;
  private final MessageWithParameters  inTestStatusMessage;
  private final MessageWithParameters  inProductionStatusMessage;
  private final MessageWithParameters  deploymentStatusMessage;

  @JsonIgnore
  private final String simulationName;

  /**
   * Default constructor
   */
  public OperationStatusVo(ConfigurationStatusVo configurationStatus,
                           VotingMaterialStatusVo votingMaterialStatus,
                           VotingPeriodStatusVo votingPeriodStatus,
                           TallyArchiveStatusVo tallyArchiveStatus,
                           String simulationName,
                           DeploymentTarget deploymentTarget) {
    this.configurationStatus = configurationStatus;
    this.votingMaterialStatus = votingMaterialStatus;
    this.tallyArchiveStatus = tallyArchiveStatus;
    this.deploymentTarget = deploymentTarget;
    this.votingPeriodStatus = votingPeriodStatus;
    this.simulationName = simulationName;
    this.inTestStatusMessage = computeInTestStatusMessage();
    this.deploymentStatusMessage = computeDeploymentStatusMessage();
    this.inProductionStatusMessage = computeInProductionStatusMessage();
  }

  private MessageWithParameters computeDeploymentStatusMessage() {
    if (deploymentTarget == DeploymentTarget.SIMULATION) {
      return new MessageWithParameters("deployment-summary.production.target.simulation.status.in-preparation")
          .addParams("simulationName", simulationName);
    }

    if (deploymentTarget == DeploymentTarget.REAL) {
      return new MessageWithParameters("deployment-summary.production.target.real.status.in-preparation");
    }

    if (configurationStatus.getState() == ConfigurationStatusVo.State.DEPLOYED) {
      return new MessageWithParameters(
          "deployment-summary.configuration.status.deployed")
          .addParams(PARAM_KEY_USER, configurationStatus.getStateUser())
          .addParams(PARAM_KEY_DATE, configurationStatus.getStateDate().format(DATE_TIME_FORMATTER));
    }
    return null;
  }


  private MessageWithParameters computeInProductionStatusMessage() {
    if (deploymentTarget == DeploymentTarget.NOT_DEFINED) {
      return null;
    }

    if (tallyArchiveStatus.getState() != TallyArchiveStatusVo.State.NOT_REQUESTED) {
      return new MessageWithParameters(
          "deployment-summary.tally-archive.status." + tallyArchiveStatus.getState().name().toLowerCase(Locale.ENGLISH))
          .addParams(PARAM_KEY_USER, votingPeriodStatus.stateUser)
          .addParams(PARAM_KEY_DATE, votingPeriodStatus.stateDate.format(DATE_TIME_FORMATTER));
    }

    if (votingMaterialStatus.getState() == VotingMaterialStatusVo.State.VALIDATED &&
        votingPeriodStatus.state != VotingPeriodStatusVo.State.INCOMPLETE) {
      return new MessageWithParameters(
          "deployment-summary.voting-period.status." + votingPeriodStatus.state.name().toLowerCase(Locale.ENGLISH))
          .addParams(PARAM_KEY_USER, votingPeriodStatus.stateUser)
          .addParams(PARAM_KEY_DATE, votingPeriodStatus.stateDate.format(DATE_TIME_FORMATTER))
          .addParams(PARAM_KEY_COMMENT, votingPeriodStatus.comment);
    }

    return new MessageWithParameters(
        "deployment-summary.voting-material.status." + votingMaterialStatus.getState().name().toLowerCase(Locale.ENGLISH))
        .addParams(PARAM_KEY_USER, votingMaterialStatus.getStateUser())
        .addParams(PARAM_KEY_DATE, votingMaterialStatus.getStateDate().format(DATE_TIME_FORMATTER))
        .addParams(PARAM_KEY_COMMENT, votingMaterialStatus.getComment());
  }

  /*

   */

  /**
   * return complete status based based on completed sections status
   */
  public static boolean isComplete(Map<String, Boolean> completedSection) {
    return completedSection.values().stream().allMatch(isSectionComplete -> isSectionComplete);
  }

  private MessageWithParameters computeInTestStatusMessage() {
    if (configurationStatus.getState() != ConfigurationStatusVo.State.DEPLOYED) {

      final MessageWithParameters messageWithParameters = new MessageWithParameters(
          "deployment-summary.configuration.status." + configurationStatus.getState().name().toLowerCase(Locale.ENGLISH))
          .addParams(PARAM_KEY_USER, configurationStatus.getStateUser())
          .addParams(PARAM_KEY_DATE, configurationStatus.getStateDate().format(DATE_TIME_FORMATTER));

      if (configurationStatus.getState() == ConfigurationStatusVo.State.DEPLOYMENT_REFUSED) {
        messageWithParameters.addParams(PARAM_KEY_REASON, configurationStatus.getComment());
      }
      return messageWithParameters;
    }

    return null;
  }

  public ConfigurationStatusVo getConfigurationStatus() {
    return configurationStatus;
  }

  public VotingMaterialStatusVo getVotingMaterialStatus() {
    return votingMaterialStatus;
  }

  public VotingPeriodStatusVo getVotingPeriodStatus() {
    return votingPeriodStatus;
  }

  public TallyArchiveStatusVo getTallyArchiveStatus() {
    return tallyArchiveStatus;
  }

  public DeploymentTarget getDeploymentTarget() {
    return deploymentTarget;
  }

  public MessageWithParameters getInTestStatusMessage() {
    return inTestStatusMessage;
  }

  public MessageWithParameters getInProductionStatusMessage() {
    return inProductionStatusMessage;
  }

  public MessageWithParameters getDeploymentStatusMessage() {
    return deploymentStatusMessage;
  }

  public String getSimulationName() {
    return simulationName;
  }
}

