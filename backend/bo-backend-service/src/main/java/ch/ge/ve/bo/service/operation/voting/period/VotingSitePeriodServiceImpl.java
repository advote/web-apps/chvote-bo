/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.voting.period;

import ch.ge.ve.bo.MilestoneType;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.SimulationPeriodConfigurationRepository;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.SimulationPeriodConfiguration;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.voting.period.model.VotingSitePeriodVO;
import java.time.LocalDateTime;
import java.util.Optional;
import javax.transaction.Transactional;

/**
 * Default implementation of VotingSitePeriodService
 */
public class VotingSitePeriodServiceImpl implements VotingSitePeriodService {
  private final SimulationPeriodConfigurationRepository repository;
  private final SecuredOperationRepository              operationRepository;
  private final SecuredOperationHolderService           securedOperationHolderService;

  /**
   * Default constructor
   */
  public VotingSitePeriodServiceImpl(SimulationPeriodConfigurationRepository repository,
                                     SecuredOperationRepository operationRepository,
                                     SecuredOperationHolderService securedOperationHolderService) {
    this.repository = repository;
    this.operationRepository = operationRepository;
    this.securedOperationHolderService = securedOperationHolderService;
  }


  @Override
  @Transactional
  public VotingSitePeriodVO saveOrUpdate(long operationId, LocalDateTime dateOpen, LocalDateTime dateClose,
                                         int gracePeriod) {
    return securedOperationHolderService.doNotOptionalUpdate(
        () -> {
          SimulationPeriodConfiguration config = repository.findByOperationId(operationId).orElseGet(
              () -> {
                SimulationPeriodConfiguration conf = new SimulationPeriodConfiguration();
                Operation operation = operationRepository.findOne(operationId, true);
                conf.setOperation(operation);
                if (operation.getDeploymentTarget() == DeploymentTarget.REAL) {
                  throw new TechnicalException("Could not change vote site period if the operation is in production");
                }
                return conf;
              }
          );
          config.setDateOpen(dateOpen);
          config.setDateClose(dateClose);
          config.setGracePeriod(gracePeriod);
          return config;
        }, config -> new VotingSitePeriodVO(repository.save(config))
    );
  }

  @Override
  public Optional<VotingSitePeriodVO> findForOperation(long operationId) {
    Operation operation = operationRepository.findOne(operationId, false);
    if (operation.getDeploymentTarget() == DeploymentTarget.REAL) {
      if (operation.getMilestones() != null && !operation.getMilestones().isEmpty()) {
        return Optional.of(new VotingSitePeriodVO(
            operation.getMilestone(MilestoneType.SITE_OPEN).orElse(null),
            operation.getMilestone(MilestoneType.SITE_CLOSE).orElse(null),
            operation.getGracePeriod()));
      }
      return Optional.empty();
    }

    return securedOperationHolderService.safeRead(
        () -> repository.findByOperationId(operationId)).map(VotingSitePeriodVO::new);
  }

}
