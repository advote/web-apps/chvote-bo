/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.download.token;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.DownloadTokenRepository;
import ch.ge.ve.bo.repository.entity.DownloadToken;
import ch.ge.ve.bo.repository.entity.Operation;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of DownloadTokenService
 */
@Service
public class DownloadTokenServiceImpl implements DownloadTokenService {
  private final DownloadTokenRepository downloadTokenRepository;

  /**
   * Default constructor
   */
  public DownloadTokenServiceImpl(DownloadTokenRepository downloadTokenRepository) {
    this.downloadTokenRepository = downloadTokenRepository;
  }

  @Override
  @Transactional
  public DownloadToken getOrCreateDownloadToken(Operation operation, String path) {
    DownloadToken downloadToken =
        downloadTokenRepository.findByPathAndLogin(path, ConnectedUser.get().login);
    if (downloadToken == null) {
      downloadToken = new DownloadToken();
      downloadToken.setLogin(ConnectedUser.get().login);
      downloadToken.setOperation(operation);
      downloadToken.setPath(path);
      downloadToken.setId(RandomStringUtils.random(200, true, true));
      downloadTokenRepository.save(downloadToken);
    }
    return downloadToken;
  }
}
