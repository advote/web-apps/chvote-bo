/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import java.time.LocalDateTime;

/**
 * Value object representing an operation view for a printer.
 */
public class PrinterOperationVo extends OperationVo {
  public final String printerFileDownloadToken;
  public final LocalDateTime printerFileGenerationDate;
  /**
   * Default constructor
   */
  public PrinterOperationVo(OperationVo operation, String printerFileDownloadToken, LocalDateTime printerFileGenerationDate) {
    super(operation.id, operation.login, operation.saveDate, operation.shortLabel, operation.longLabel, operation.date,
          operation.gracePeriod, operation.milestones, operation.lastModificationDate, operation.simulationName,
          operation.deploymentTarget, operation.lastConfigurationUpdateDate, operation.votingCardTitle,
          operation.printerTemplate, operation.managementEntity, operation.guestManagementEntities);
    this.printerFileDownloadToken = printerFileDownloadToken;
    this.printerFileGenerationDate = printerFileGenerationDate;
  }

}
