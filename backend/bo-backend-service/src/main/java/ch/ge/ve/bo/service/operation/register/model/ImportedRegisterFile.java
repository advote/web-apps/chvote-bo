/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

/**
 * Data concerning a register file
 */
public class ImportedRegisterFile {
  public final String         uploader;
  public final String         managementEntity;
  public final LocalDateTime  uploadDate;
  public final String         emitter;
  public final LocalDateTime  emissionDate;
  public final int            voterCount;
  public final int            doiCount;
  public final String         messageUniqueId;
  public final int            countingCircleCount;
  public final String         fileName;
  public final DetailedReport detailedReport;

  /**
   * Default constructor
   */
  @SuppressWarnings("squid:S00107") // suppress Sonar check on parameters count
  @JsonCreator
  public ImportedRegisterFile(
      @JsonProperty("uploader") String uploader,
      @JsonProperty("managementEntity") String managementEntity,
      @JsonProperty("uploadDate") LocalDateTime uploadDate,
      @JsonProperty("emitter") String emitter,
      @JsonProperty("emissionDate") LocalDateTime emissionDate,
      @JsonProperty("voterCount") int voterCount,
      @JsonProperty("doiCount") int doiCount,
      @JsonProperty("messageUniqueId") String messageUniqueId,
      @JsonProperty("countingCircleCount") int countingCircleCount,
      @JsonProperty("fileName") String fileName,
      @JsonProperty("detailedReport") DetailedReport detailedReport
      ) {
    this.uploader = uploader;
    this.uploadDate = uploadDate;
    this.emitter = emitter;
    this.emissionDate = emissionDate;
    this.voterCount = voterCount;
    this.doiCount = doiCount;
    this.messageUniqueId = messageUniqueId;
    this.countingCircleCount = countingCircleCount;
    this.fileName = fileName;
    this.detailedReport = detailedReport;
    this.managementEntity = managementEntity;
  }
}
