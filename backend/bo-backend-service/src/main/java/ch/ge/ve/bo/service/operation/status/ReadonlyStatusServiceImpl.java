/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo;
import ch.ge.ve.bo.service.operation.model.ModificationMode;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service that identify which cards are in readonly
 */
public class ReadonlyStatusServiceImpl implements ReadonlyStatusService {

  public enum ConfigurationSection {
    BASE_PARAMETER("base-parameter", false, false, "ROLE_UPDATE_BASE_PARAMETER"),
    MILESTONE("milestone", false, false, "ROLE_UPDATE_MILESTONE"),
    DOCUMENT("document", false, true, "ROLE_UPDATE_MILESTONE", "DELETE_OPERATION_DOCUMENTATION",
             "ROLE_SET_OPERATION_DOCUMENTATION",
             "ROLE_ADD_HIGHLIGHTED_QUESTION", "ROLE_ADD_LOCALIZED_HIGHLIGHTED_QUESTION",
             "ROLE_DELETE_LOCALIZED_HIGHLIGHTED_QUESTION",
             "ROLE_DELETE_HIGHLIGHTED_QUESTION"),
    DOI("doi", false, false, "ROLE_DELETE_DOI", "ROLE_UPLOAD_DOI"),
    MANAGEMENT_ENTITY("management-entity", false, false, "ROLE_REVOKE_MANAGEMENT_ENTITY",
                      "ROLE_INVITE_MANAGEMENT_ENTITY"),
    REPOSITORY("repository", true, true, "ROLE_DELETE_REPOSITORY", "ROLE_UPLOAD_REPOSITORY"),
    TESTING_CARD("testing-card", false, false, "ROLE_EDIT_TEST_VOTING_CARD_DEFINITION",
                 "ROLE_DELETE_TEST_VOTING_CARD_DEFINITION",
                 "ROLE_CREATE_TEST_VOTING_CARD_DEFINITION"),
    BALLOT_DOCUMENTATION("ballot_documentation", true, true, "ROLE_ADD_BALLOT_DOCUMENTATION",
                         "ROLE_DELETE_BALLOT_DOCUMENTATION"),
    ELECTION_PAGE_PROPERTIES("election_page_properties", false, true, "ROLE_CREATE_ELECTION_PAGE_PROPERTIES_MODEL",
                             "ROLE_DELETE_ELECTION_PAGE_PROPERTIES_MODEL",
                             "ROLE_MAP_ELECTION_PAGE_PROPERTIES_MODEL_TO_BALLOT"),
    VOTING_SITE_CONFIGURATION("voting_site_configuration", false, true, "ROLE_UPDATE_VOTING_SITE_CONFIG");

    final String  sectionName;
    final boolean editableByGuestManagementEntity;
    final boolean editableInPartialModification;
    List<String> supportedRoles;


    ConfigurationSection(String sectionName, boolean editableByGuestManagementEntity,
                         boolean editableInPartialModification, String... supportedRoles) {
      this.sectionName = sectionName;
      this.editableByGuestManagementEntity = editableByGuestManagementEntity;
      this.editableInPartialModification = editableInPartialModification;
      this.supportedRoles = Arrays.asList(supportedRoles);
    }
  }


  @Override
  public List<String> getReadOnlySectionsForConfiguration(
       Operation operation, ModificationMode modificationMode,
      ConfigurationStatusVo.State state) {
    boolean isGuestManagementEntity = !ConnectedUser.get().managementEntity.equals(operation.getManagementEntity());
    return Stream.of(ConfigurationSection.values())
                 .filter(section -> isSectionInReadonly(section, isGuestManagementEntity, modificationMode, state))
                 .map(section -> section.sectionName)
                 .collect(Collectors.toList());
  }


  private boolean isSectionInReadonly(
      ConfigurationSection section, boolean isGuestManagementEntity, ModificationMode modificationMode,
      ConfigurationStatusVo.State configurationState) {

    if (isGuestManagementEntity && !section.editableByGuestManagementEntity) {
      return true;
    }
    if (configurationState.isReadOnly()) {
      return true;
    }
    if (modificationMode == ModificationMode.IN_PARTIAL_MODIFICATION &&
        !section.editableInPartialModification) {
      return true;
    }

    return section.supportedRoles.stream().noneMatch(role -> ConnectedUser.get().roles.contains(role));
  }


}
