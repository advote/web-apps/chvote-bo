/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.utils.xml;

import ch.ge.ve.bo.service.utils.exception.SchemaValidationException;
import ch.ge.ve.interfaces.ech.parser.EchInput;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.xml.resolver.Catalog;
import org.apache.xml.resolver.readers.TR9401CatalogReader;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 * Validate an xml file against a xsd
 */
public class XSDValidator {
  public static final ErrorHandler RETHROW_ERROR_HANDLER = new ErrorHandler() {
    @Override
    public void warning(SAXParseException exception) throws SAXParseException {
      throw exception;
    }

    @Override
    public void error(SAXParseException exception) throws SAXParseException {
      throw exception;
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXParseException {
      throw exception;
    }
  };

  private final Schema schema;

  /**
   * Create a xml validator
   *
   * @param baseSchema  Base schema to use
   * @param catalogFile Catalog file
   *
   * @return the created XSD validator
   */
  public static XSDValidator of(String baseSchema, String catalogFile) {
    try {
      InputStream catalogStream = getClasspathResourceAsStream(catalogFile);
      TR9401CatalogReader catalogReader = new TR9401CatalogReader();
      Catalog catalog = new Catalog();
      catalogReader.readCatalog(catalog, catalogStream);
      return new XSDValidator(getClasspathResourceAsStream(baseSchema), new ClassPathResourceResolver(catalog));
    } catch (SAXException | IOException e) {
      throw new XmlTechnicalException(e);
    }
  }

  private static InputStream getClasspathResourceAsStream(String resource) {
    return XSDValidator.class.getClassLoader().getResourceAsStream(resource);
  }

  /**
   * create a validator
   *
   * @param xsdInputstreams  input stream of xsd
   * @param resourceResolver resources resolver for imported xsd
   */
  private XSDValidator(InputStream xsdInputstreams, LSResourceResolver resourceResolver) throws SAXException {
    SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
    if (resourceResolver != null) {
      factory.setResourceResolver(resourceResolver);
    }
    schema = factory.newSchema(new StreamSource(xsdInputstreams));
  }


  /**
   * Validate an xml document
   *
   * @param xmlInputstreams source input stream
   * @param errorHandler    ErrorHandler to use uring the process
   */
  public void validate(InputStream xmlInputstreams, ErrorHandler errorHandler) throws SchemaValidationException {
    try {
      Validator validator = schema.newValidator();
      validator.setErrorHandler(errorHandler);
      validator.validate(new StreamSource(xmlInputstreams));
    } catch (SAXException exception) {
      throw new SchemaValidationException(exception);
    } catch (IOException e) {
      throw new XmlTechnicalException(e);
    }
  }

  private static class ClassPathResourceResolver implements LSResourceResolver {
    final Catalog catalog;

    private ClassPathResourceResolver(Catalog catalog) {
      this.catalog = catalog;
    }

    public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
      try {
        String file = catalog.resolveEntity(type, publicId, systemId);
        file = file.replaceFirst(".*!/", "");
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(file);
        if (resourceAsStream == null) {
          throw new FileNotFoundException("Cannot find schema in file " + file);
        }
        return new EchInput(publicId, systemId, resourceAsStream);
      } catch (IOException e) {
        throw new XmlTechnicalException(e);
      }
    }
  }
}
