/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.processor;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.service.model.CountingCircleVo;
import ch.ge.ve.bo.service.model.SchemaValidationError;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import ch.ge.ve.bo.service.operation.register.model.DetailedReport;
import ch.ge.ve.bo.service.operation.register.model.DuplicateVo;
import ch.ge.ve.bo.service.operation.register.model.FileInfo;
import ch.ge.ve.bo.service.operation.register.model.InvalidDateOfBirthVo;
import ch.ge.ve.bo.service.operation.register.model.OperationVoterRef;
import ch.ge.ve.bo.service.operation.register.model.UndefinedDoiVo;
import ch.ge.ve.bo.service.operation.register.model.UniqueMessageIdContext;
import ch.ge.ve.bo.service.operation.register.model.Voter;
import ch.ge.ve.bo.service.operation.register.model.VoterRefTable;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.commons.lang.StringUtils;

/**
 * Execution context used by {@link BusinessProcessor}
 */
public class BusinessProcessorContext {
  private       int                         expectedNumberOfVoter;
  private       int                         actualNumberOfVoter;
  private final DetailedReport              detailedReport;
  private final FileInfo                    fileInfo;
  private final Set<String>                 operationDomainOfInfluence;
  private final VoterRefTable               voterRefTable;
  private final UniqueMessageIdContext      uniqueMessageIdContext;
  private       Set<DomainOfInfluenceVo>    domainOfInfluences          = new HashSet<>();
  private       Set<CountingCircleVo>       countingCircles             = new HashSet<>();
  private final Deque<String>               currentPath                 = new LinkedList<>();
  private final List<DuplicateVo>           duplicates                  = new ArrayList<>();
  private final List<UndefinedDoiVo>        undefinedDois               = new ArrayList<>();
  private final List<InvalidDateOfBirthVo>  invalidDateOfBirths         = new ArrayList<>();
  private final List<SchemaValidationError> otherSchemaValidationErrors = new ArrayList<>();


  BusinessProcessorContext(VoterRefTable voterRefTable, FileInfo fileInfo, Set<String> alreadyImported,
                           Set<String> operationDomainOfInfluence) {
    this.voterRefTable = voterRefTable;
    this.fileInfo = fileInfo;
    this.uniqueMessageIdContext = new UniqueMessageIdContext(alreadyImported);
    this.operationDomainOfInfluence = operationDomainOfInfluence;
    this.detailedReport = new DetailedReport();
  }

  Set<String> getOperationDomainOfInfluence() {
    return operationDomainOfInfluence;
  }

  int getExpectedNumberOfVoter() {
    return expectedNumberOfVoter;
  }

  void setExpectedNumberOfVoter(int expectedNumberOfVoter) {
    this.expectedNumberOfVoter = expectedNumberOfVoter;
  }

  public int getActualNumberOfVoter() {
    return actualNumberOfVoter;
  }

  public List<DuplicateVo> getDuplicates() {
    return duplicates;
  }

  public List<UndefinedDoiVo> getUndefinedDois() {
    return undefinedDois;
  }

  String getCurrentPath() {
    return String.join(".", this.currentPath);
  }

  VoterRefTable getVoterRefTable() {
    return voterRefTable;
  }

  String getText(XMLStreamReader xml) throws XMLStreamException {
    currentPath.pollLast();
    return xml.getElementText();
  }

  public UniqueMessageIdContext getUniqueMessageIdContext() {
    return uniqueMessageIdContext;
  }

  void addDoi(DomainOfInfluenceVo doi) {
    this.domainOfInfluences.add(doi);
    detailedReport.addDoi(doi.shortName);
  }

  public FileInfo getFileInfo() {
    return fileInfo;
  }

  void addDuplicate(OperationVoterRef voterRef, Voter voter) {
    duplicates.add(new DuplicateVo(voterRef, voter));
  }

  void setVoterAsDuplicate(Voter voter) {
    addDuplicate(new OperationVoterRef(fileInfo, voter), voter);
  }

  void setVoterHasOnlyUndefinedDoi(Voter voter) {
    undefinedDois.add(new UndefinedDoiVo(new OperationVoterRef(fileInfo, voter), voter));
  }

  void setVoterHasInvalidDateOfBirth(Voter voter) {
    invalidDateOfBirths.add(new InvalidDateOfBirthVo(new OperationVoterRef(fileInfo, voter), voter));
  }

  public List<InvalidDateOfBirthVo> getInvalidDateOfBirths() {
    return invalidDateOfBirths;
  }

  void navigateTo(String relativePath) {
    currentPath.addLast(relativePath);
  }

  void navigateBack() {
    this.currentPath.pollLast();
  }

  public int getCountingCircleCount() {
    return countingCircles.size();
  }

  public int getDoiCount() {
    return domainOfInfluences.size();
  }


  public DetailedReport getDetailedReport() {
    return detailedReport;
  }


  public List<SchemaValidationError> getOtherSchemaValidationErrors() {
    return otherSchemaValidationErrors;
  }

  void addMissingMunicipalityId(String identifier, Voter voter) {
    if (voter.getVoterType() == Voter.VoterType.SWISS_ABROAD) {
      otherSchemaValidationErrors.add(new SchemaValidationError(voter.getLineNumber(),
                                                                voter.getColumnNumber(),
                                                                "Cannot find municipality id or canton for voter " +
                                                                "identified by " + identifier));
    } else {
      otherSchemaValidationErrors
          .add(new SchemaValidationError(voter.getLineNumber(), voter.getColumnNumber(),
                                         "Cannot find municipality id for voter identified by " + identifier));
    }
  }

  void addInvalidCanton(String identifier, Voter voter) {
    otherSchemaValidationErrors.add(new SchemaValidationError(
        voter.getLineNumber(), voter.getColumnNumber(),
        "Canton for voter identified by " + identifier + " should be " + ConnectedUser.get().realm));
  }


  void addVoter(Voter voter) {
    actualNumberOfVoter++;

    if (voter.getCountingCircle() != null) {
      this.countingCircles.add(voter.getCountingCircle());
    }

    if (voter.getVoterType() == Voter.VoterType.SWISS) {
      detailedReport.addSwiss(Optional.ofNullable(voter.getMunicipality()).orElse("Unknown"));
    } else if (voter.getVoterType() == Voter.VoterType.FOREIGNER) {
      detailedReport.addForeigner(voter.getMunicipality());
    } else {
      detailedReport.addSwissAbroad(voter.getLang(), voter.getPostageCode());
    }
    if (voter.getMunicipalityId() != null) {
      detailedReport.addMunicipality(Integer.parseInt(voter.getMunicipalityId()), voter.getMunicipality());
    }

  }

  void addDuplicateCountingCirlce(Voter voter) {
    otherSchemaValidationErrors.add(new SchemaValidationError(
        voter.getLineNumber(), voter.getColumnNumber(),
        "There is multiple different counting circles for voter identified by " + voter.getIdentifier()));
  }

  void addMissingCountingCircle(Voter voter) {
    otherSchemaValidationErrors.add(new SchemaValidationError(
        voter.getLineNumber(), voter.getColumnNumber(),
        "There is no counting circle for voter identified by " + voter.getIdentifier()));
  }

  void addNotAnEVoter(Voter voter) {
    otherSchemaValidationErrors.add(new SchemaValidationError(
        voter.getLineNumber(), voter.getColumnNumber(),
        "Voter identified by " + voter.getIdentifier() + " is not an eVoter"));
  }

  void addInvalidDateOfBirth(Voter voter) {
    otherSchemaValidationErrors.add(new SchemaValidationError(
        voter.getLineNumber(), voter.getColumnNumber(),
        "Voter identified by " + voter.getIdentifier() + " has an invalid date of birth"));
  }

  void addDuplicateVoterId(Voter voter) {
    otherSchemaValidationErrors.add(new SchemaValidationError(
        voter.getLineNumber(), voter.getColumnNumber(),
        "Duplicate voter identifier: " + voter.getIdentifier()));
  }

  // FIXME: this is a temporary solution... The extension must be validated when the whole XML is validated (this
  // should be done in chvote-interfaces)
  void addMissingExtensionElement(Voter voter, String extensionElement) {
    otherSchemaValidationErrors.add(new SchemaValidationError(
        voter.getLineNumber(), voter.getColumnNumber(),
        "Missing extension element: " + extensionElement));
  }

  void addInvalidVoter(Location location, String message) {
    actualNumberOfVoter++;
    String errorToReport = "Voter cannot be parsed";
    if (StringUtils.isNotBlank(message)) {
      errorToReport += ": " + message;
    }

    otherSchemaValidationErrors.add(
        new SchemaValidationError(location.getLineNumber(), location.getColumnNumber(), errorToReport));
  }
}
