/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

import ch.ge.ve.bo.repository.entity.File;
import java.time.LocalDateTime;

/**
 * Base information concerning a register file
 */
public class FileInfo {
  private String        fileName;
  private String        importedBy;
  private LocalDateTime importDate;
  private String        emitter;
  private LocalDateTime emissionDate;

  /**
   * Default constructor for import
   */
  public FileInfo(String fileName, String importedBy, LocalDateTime importDate) {
    this.fileName = fileName;
    this.importedBy = importedBy;
    this.importDate = importDate;
  }

  /**
   * default constructor for already validated register
   */
  public FileInfo(File file, ImportedRegisterFile registerFileInfo) {
    this(file.getFileName(), file.getLogin(), file.getSaveDate());
    setEmitter(registerFileInfo.emitter);
    setEmissionDate(registerFileInfo.emissionDate);
  }

  public String getFileName() {
    return fileName;
  }

  public String getImportedBy() {
    return importedBy;
  }

  public LocalDateTime getImportDate() {
    return importDate;
  }

  public String getEmitter() {
    return emitter;
  }

  public void setEmitter(String emitter) {
    this.emitter = emitter;
  }

  public LocalDateTime getEmissionDate() {
    return emissionDate;
  }

  public void setEmissionDate(LocalDateTime emissionDate) {
    this.emissionDate = emissionDate;
  }
}
