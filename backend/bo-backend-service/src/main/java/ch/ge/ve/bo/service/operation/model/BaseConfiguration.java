/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

/**
 * Base operation configuration used for creation or edition
 */
public class BaseConfiguration {
  private final String        shortLabel;
  private final String        longLabel;
  private final LocalDateTime date;

  /**
   * Default constructor
   */
  @JsonCreator
  public BaseConfiguration(
      @JsonProperty("shortLabel") String shortLabel,
      @JsonProperty("longLabel") String longLabel,
      @JsonProperty("date") LocalDateTime date) {
    this.shortLabel = shortLabel;
    this.longLabel = longLabel;
    this.date = date;
  }

  public String getShortLabel() {
    return shortLabel;
  }

  public String getLongLabel() {
    return longLabel;
  }

  public LocalDateTime getDate() {
    return date;
  }
}
