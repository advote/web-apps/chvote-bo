/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.entity.BallotDocumentation;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

/**
 * Value object for {@link BallotDocumentation}
 */
public class BallotDocumentationVo {
  private final String        ballotId;
  private final String        fileName;
  private final String        localizedLabel;
  private final String        managementEntity;
  private final Long          id;
  private final Language      language;
  private final LocalDateTime saveDate;

  /**
   * Constructor using entity
   */
  public BallotDocumentationVo(BallotDocumentation ballotDocumentation) {
    this(
        ballotDocumentation.getBallotId(),
        ballotDocumentation.getFileName(),
        ballotDocumentation.getLocalizedLabel(),
        ballotDocumentation.getManagementEntity(),
        ballotDocumentation.getId(),
        ballotDocumentation.getLanguage(),
        ballotDocumentation.getSaveDate()

    );
  }

  /**
   * Default constructor
   */
  @JsonCreator
  @SuppressWarnings("WeakerAccess")
  public BallotDocumentationVo(
      @JsonProperty("ballotId") String ballotId,
      @JsonProperty("fileName") String fileName,
      @JsonProperty("localizedLabel") String localizedLabel,
      @JsonProperty("managementEntity") String managementEntity,
      @JsonProperty("id") Long id,
      @JsonProperty("language") Language language,
      @JsonProperty("saveDate") LocalDateTime saveDate
  ) {
    this.ballotId = ballotId;
    this.fileName = fileName;
    this.localizedLabel = localizedLabel;
    this.managementEntity = managementEntity;
    this.id = id;
    this.language = language;
    this.saveDate = saveDate;
  }

  public Language getLanguage() {
    return language;
  }

  public String getBallotId() {
    return ballotId;
  }

  public String getFileName() {
    return fileName;
  }

  public String getLocalizedLabel() {
    return localizedLabel;
  }

  public String getManagementEntity() {
    return managementEntity;
  }

  public Long getId() {
    return id;
  }

  public LocalDateTime getSaveDate() {
    return saveDate;
  }
}
