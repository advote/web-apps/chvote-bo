/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.bo.MilestoneType;
import ch.ge.ve.bo.repository.entity.BaseEntity;
import ch.ge.ve.bo.service.model.BaseVo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Value object representing an operation's milestone.
 */
public class MilestoneVo extends BaseVo {

  public final MilestoneType type;
  public final LocalDateTime date;

  /**
   * Default constructor
   */
  @JsonCreator
  public MilestoneVo(@JsonProperty("id") long id,
                     @JsonProperty("login") String login,
                     @JsonProperty("saveDate") LocalDateTime saveDate,
                     @JsonProperty("type") MilestoneType type,
                     @JsonProperty("date") LocalDateTime date) {
    super(id, login, saveDate);
    this.type = type;
    this.date = date;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof MilestoneVo)) {
      return false;
    }
    MilestoneVo that = (MilestoneVo) o;
    return type == that.type &&
           Objects.equals(date, that.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, date);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("MilestoneVo{");
    sb.append("id=").append(id);
    sb.append(", type=").append(type);
    sb.append(", date=").append(date != null ? date.format(BaseEntity.DATE_FORMATTER) : null);
    sb.append(", login='").append(login).append('\'');
    sb.append(", saveDate=").append(saveDate != null ? saveDate.format(BaseEntity.DATE_FORMATTER) : null);
    sb.append('}');
    return sb.toString();
  }
}
