/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.processor;

import ch.ge.ve.bo.service.operation.register.exception.StreamStoppedException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Imput Stream that we can stop.
 */
public class RandomStopInputStream extends InputStream {
  private static final String      STREAM_ALREADY_STOPPED = "Stream already stopped. Cannot read more";
  private final        InputStream stream;
  private              boolean     stopped;

  RandomStopInputStream(InputStream stream) {
    this.stream = stream;
  }

  @Override
  public int read() throws IOException {
    if (!stopped) {
      return stream.read();
    } else {
      throw new StreamStoppedException(STREAM_ALREADY_STOPPED);
    }
  }

  @Override
  public int read(byte[] b) throws IOException {
    if (!stopped) {
      return stream.read(b);
    } else {
      throw new StreamStoppedException(STREAM_ALREADY_STOPPED);
    }
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    if (!stopped) {
      return stream.read(b, off, len);
    } else {
      throw new StreamStoppedException(STREAM_ALREADY_STOPPED);
    }
  }

  /**
   * Stop further processing for each streams
   */
  public void stop() {
    stopped = true;
  }
}
