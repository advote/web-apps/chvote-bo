/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.file.model.FileVo;
import java.io.InputStream;
import java.util.List;

/**
 * Operation's document services.
 */
public interface OperationDocumentService {

  /**
   * Import a document for the given operation.
   *
   * @param operationId concerned operation's unique ID
   * @param type        file's type
   * @param fileName    file's name
   * @param inputStream file's content
   *
   * @return the imported file information
   *
   * @throws BusinessException if an error occured during the import
   */
  FileVo importFile(long operationId, FileType type, Language language, String fileName, InputStream inputStream) throws BusinessException;

  /**
   * Retrieves the document file
   *
   * @param fileId the file ID
   *
   * @return the document file, or null if none found
   */
  FileVo getFile(long fileId);

  /**
   * Retrieves the documents linked to the given operation ID.
   *
   * @param operationId the file ID
   *
   * @return the list of document files
   */
  List<FileVo> getFiles(long operationId);
}
