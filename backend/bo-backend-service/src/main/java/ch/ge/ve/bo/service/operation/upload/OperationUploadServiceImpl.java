/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload;

import ch.ge.ve.bo.repository.entity.WorkflowStep;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.status.OperationWorkflowService;
import org.springframework.transaction.annotation.Transactional;

/**
 * Operation Uploader for pact
 */
public class OperationUploadServiceImpl implements OperationUploadService {

  private final OperationUploadServiceConfigurationHandler  configurationHandler;
  private final OperationUploadServiceVotingMaterialHandler votingMaterialHandler;
  private final OperationUploadServiceVotingPeriodHandler   votingPeriodHandler;
  private final OperationWorkflowService                    operationWorkflowService;

  /**
   * Default constructor
   */
  public OperationUploadServiceImpl(OperationUploadServiceConfigurationHandler configurationHandler,
                                    OperationUploadServiceVotingMaterialHandler votingMaterialHandler,
                                    OperationUploadServiceVotingPeriodHandler votingPeriodHandler,
                                    OperationWorkflowService operationWorkflowService) {
    this.configurationHandler = configurationHandler;
    this.votingMaterialHandler = votingMaterialHandler;
    this.votingPeriodHandler = votingPeriodHandler;
    this.operationWorkflowService = operationWorkflowService;
  }



  @Override
  @Transactional
  public void uploadConfigurationToPact(long operationId) throws BusinessException {
    configurationHandler.upload(operationId);
    operationWorkflowService.suspendModificationMode(operationId);
    operationWorkflowService.setWorkflowStep(operationId, WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT);
  }

  @Override
  @Transactional
  public void uploadVotingMaterialToPact(long operationId) throws PactRequestException {
    votingMaterialHandler.upload(operationId);
    operationWorkflowService.setWorkflowStep(operationId, WorkflowStep.VOTING_PERIOD_CONF_NOT_SENT);
  }

  @Override
  @Transactional
  public void uploadVotingPeriodToPact(long operationId) throws PactRequestException {
    votingPeriodHandler.upload(operationId);
    operationWorkflowService.setWorkflowStep(operationId, WorkflowStep.VOTING_PERIOD_CONF_SENT);
  }

}
