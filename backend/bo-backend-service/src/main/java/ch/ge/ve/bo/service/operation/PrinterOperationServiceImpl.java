/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.UnsecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.DownloadToken;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.mapper.BeanMapper;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo;
import ch.ge.ve.bo.service.operation.download.token.DownloadTokenService;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.OperationStatusVo;
import ch.ge.ve.bo.service.operation.model.PrinterOperationVo;
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo;
import ch.ge.ve.bo.service.operation.status.OperationStatusService;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the {@link OperationService} interface.
 */
public class PrinterOperationServiceImpl implements PrinterOperationService {

  private final UnsecuredOperationRepository unsecuredOperationRepository;
  private final OperationStatusService       statusService;
  private final PrinterTemplateService       printerTemplateService;
  private final DownloadTokenService         downloadTokenService;
  private final PactConfiguration            pactConfiguration;

  /**
   * Default constructor.
   */
  public PrinterOperationServiceImpl(UnsecuredOperationRepository unsecuredOperationRepository,
                                     OperationStatusService statusService,
                                     PrinterTemplateService printerTemplateService,
                                     DownloadTokenService downloadTokenService,
                                     PactConfiguration pactConfiguration) {
    this.unsecuredOperationRepository = unsecuredOperationRepository;
    this.statusService = statusService;
    this.printerTemplateService = printerTemplateService;
    this.downloadTokenService = downloadTokenService;
    this.pactConfiguration = pactConfiguration;
  }

  @Override
  @Transactional
  public List<PrinterOperationVo> findByPrinterManagementEntity() {
    ConnectedUser.checkIsInPrinterRealm();

    final PrinterTemplateVo[] printerTemplateVos =
        printerTemplateService.getPrinterTemplatesByPrinterId(ConnectedUser.getPrinterId());

    final List<Operation> operations = unsecuredOperationRepository.findByPrinterTemplateIn(
        Stream.of(printerTemplateVos)
              .map(PrinterTemplateVo::getTemplateName)
              .collect(Collectors.toList()));


    return operations.stream()
                     .map(this::addPrinterFileDownloadToken)
                     .filter(Optional::isPresent)
                     .map(Optional::get)
                     .map(OperationAndPrinterFileDownloadToken::convertToPrinterOperationVo)
                     .collect(Collectors.toList());
  }

  private Optional<OperationAndPrinterFileDownloadToken> addPrinterFileDownloadToken(Operation operation) {

    try {
      OperationStatusVo status = statusService.getStatus(operation.getId());
      VotingMaterialStatusVo votingMaterialStatus = status.getVotingMaterialStatus();
      boolean printerFileAvailable =
          votingMaterialStatus != null &&
          (VotingMaterialStatusVo.State.CREATED == votingMaterialStatus.getState() ||
           VotingMaterialStatusVo.State.VALIDATED == votingMaterialStatus.getState() ||
           VotingMaterialStatusVo.State.INVALIDATION_REQUESTED == votingMaterialStatus.getState() ||
           VotingMaterialStatusVo.State.INVALIDATION_REJECTED == votingMaterialStatus.getState()) &&
          votingMaterialStatus.getVotingCardLocations() != null &&
          votingMaterialStatus.getVotingCardLocations().containsKey(ConnectedUser.getPrinterId());
      if (!printerFileAvailable) {
        return Optional.empty();
      }
      Path path = Paths.get(
          pactConfiguration.getVotingMaterialBaseLocation(),
          votingMaterialStatus.getVotingCardLocations().get(ConnectedUser.getPrinterId()));
      return Optional.of(new OperationAndPrinterFileDownloadToken(
          operation, downloadTokenService.getOrCreateDownloadToken(operation, path.toString()), path));

    } catch (PactRequestException e) {
      throw new TechnicalException(e);
    }

  }

  private class OperationAndPrinterFileDownloadToken {
    private final Operation     operation;
    private final DownloadToken printerFileDownloadToken;
    private final LocalDateTime printerFileGenerationDate;

    private OperationAndPrinterFileDownloadToken(Operation operation, DownloadToken printerFileDownloadToken,
                                                 Path path) {
      this.operation = operation;
      this.printerFileDownloadToken = printerFileDownloadToken;
      try {
        BasicFileAttributes attr = Files.readAttributes(path, BasicFileAttributes.class);
        this.printerFileGenerationDate =
            LocalDateTime.ofInstant(attr.creationTime().toInstant(), ZoneId.systemDefault());
      } catch (IOException e) {
        throw new TechnicalException(e);
      }
    }

    PrinterOperationVo convertToPrinterOperationVo() {
      return new PrinterOperationVo(BeanMapper.map(operation), printerFileDownloadToken.getId(),
                                    printerFileGenerationDate);
    }
  }
}
