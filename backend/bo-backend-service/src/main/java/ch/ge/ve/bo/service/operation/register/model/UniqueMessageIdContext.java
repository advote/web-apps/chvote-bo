/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

import ch.ge.ve.bo.service.operation.register.exception.MessageUniqueIdAlreadyImportedException;
import java.util.Set;

/**
 * Object to identify uniqueness of register file
 */
public class UniqueMessageIdContext {
  private final Set<String> alreadyImported;
  private       String      senderId;
  private       String      manufacturer;
  private       String      product;
  private       String      messageDate;
  private       String      messageId;

  /**
   * Default constructor
   */
  public UniqueMessageIdContext(Set<String> alreadyImported) {
    this.alreadyImported = alreadyImported;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }

  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }

  public void setManufacturer(String manufacturer) {
    this.manufacturer = manufacturer;
  }

  public void setProduct(String product) {
    this.product = product;
  }

  public void setMessageDate(String messageDate) {
    this.messageDate = messageDate;
  }

  public String getMessageUniqueId() {
    return String.format("%s|%s|%s|%s|%s", senderId, manufacturer, product, messageDate, messageId);
  }


  /**
   * Verify that message is unique
   */
  public void checkMessageUniqueness() throws MessageUniqueIdAlreadyImportedException {
    if (alreadyImported.contains(getMessageUniqueId())) {
      throw new MessageUniqueIdAlreadyImportedException(getMessageUniqueId());
    }
  }

}
