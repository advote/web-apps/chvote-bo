/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import static ch.ge.ve.bo.service.utils.PactServiceUtils.attachCredentialsToRequest;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.OperationParameterType;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.status.complete.ConfigurationIsCompleteSupplier;
import ch.ge.ve.bo.service.operation.status.complete.SectionIsCompleteSupplier;
import ch.ge.ve.bo.service.operation.status.complete.VotingMaterialIsCompleteSupplier;
import ch.ge.ve.bo.service.operation.status.complete.VotingPeriodIsCompleteSupplier;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;

/**
 * Helper methods for operation status computation
 */
public class OperationStatusHandlerHelper {

  private final    Set<ConfigurationIsCompleteSupplier>  configurationIsCompleteSuppliers;
  private final    Set<VotingMaterialIsCompleteSupplier> votingMaterialIsCompleteSuppliers;
  private final    Set<VotingPeriodIsCompleteSupplier>   votingPeriodIsCompleteSuppliers;
  private    final ObjectMapper                          objectMapper;
  private final    PactConfiguration                     pactConfiguration;
  private    final HttpClient                            httpClient;

  /**
   * Default constructor
   */
  public OperationStatusHandlerHelper(Set<ConfigurationIsCompleteSupplier> configurationIsCompleteSuppliers,
                                      Set<VotingMaterialIsCompleteSupplier> votingMaterialIsCompleteSuppliers,
                                      Set<VotingPeriodIsCompleteSupplier> votingPeriodIsCompleteSuppliers,
                                      ObjectMapper objectMapper,
                                      PactConfiguration pactConfiguration,
                                      HttpClient httpClient) {
    this.configurationIsCompleteSuppliers = configurationIsCompleteSuppliers;
    this.votingMaterialIsCompleteSuppliers = votingMaterialIsCompleteSuppliers;
    this.votingPeriodIsCompleteSuppliers = votingPeriodIsCompleteSuppliers;
    this.objectMapper = objectMapper;
    this.pactConfiguration = pactConfiguration;
    this.httpClient = httpClient;
  }

  /**
   * Retrieve completeness
   */
  public Map<String, Boolean> computeIsCompleteBySection(Operation operation, OperationParameterType parameterType) {
    return getIsCompleteSupplier(parameterType)
        .stream()
        .collect(Collectors.toMap(
            SectionIsCompleteSupplier::getSectionName,
            supplier -> supplier.isSectionComplete(operation)));
  }

  private Set<? extends SectionIsCompleteSupplier> getIsCompleteSupplier(OperationParameterType parameterType) {
    if (parameterType == OperationParameterType.CONFIGURATION) {
      return configurationIsCompleteSuppliers;
    }

    if (parameterType == OperationParameterType.VOTING_MATERIAL) {
      return votingMaterialIsCompleteSuppliers;
    }

    return votingPeriodIsCompleteSuppliers;
  }


  /**
   * Do a status request on PACT
   */
  public <T> T doStatusRequest(Class<T> statusClass, long operationId, String urlTemplate)
      throws PactRequestException {
    HttpGet request = new HttpGet(MessageFormat.format(urlTemplate, operationId));
    attachCredentialsToRequest(request, pactConfiguration.getUsername(), pactConfiguration.getPassword());
    try {
      return objectMapper.readValue(httpClient.execute(request, new BasicResponseHandler()), statusClass);
    } catch (HttpResponseException responseException) {
      if (responseException.getStatusCode() == SC_NOT_FOUND) {
        return null;
      }
      throw PactRequestException.of(responseException, request.getURI().toString());
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }
}
