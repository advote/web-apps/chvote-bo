/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency;

import ch.ge.ve.bo.repository.ConsistencyCheckCallback;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;


/**
 * Service used to schedule consistency check
 */
public class ConsistencyScheduleServiceImpl implements ConsistencyScheduleService {

  /**
   * This is the delay between a modification and the time to take this modification for concurrency check.
   * Since the check is not done in the same transaction than the update a non zero value is required
   */
  public static final int                                      TIME_BEFORE_TAKE_IN_MILLIS = 500;
  private final       ScheduledExecutorService                 consistencyCheckExecutor;
  private final       Function<Long, ConsistencyServiceRunner> consistencyServiceRunnerSupplier;

  /**
   * Default constructor
   */
  public ConsistencyScheduleServiceImpl(Function<Long, ConsistencyServiceRunner> consistencyServiceRunnerSupplier,
                                        ScheduledExecutorService singleThreadExecutor) {
    this.consistencyServiceRunnerSupplier = consistencyServiceRunnerSupplier;
    this.consistencyCheckExecutor = singleThreadExecutor;
  }


  @Override
  public void scheduleCheck(long operationId) {
    consistencyCheckExecutor.schedule(consistencyServiceRunnerSupplier.apply(operationId), TIME_BEFORE_TAKE_IN_MILLIS,
                                      TimeUnit.MILLISECONDS);
  }

  @Override
  public ConsistencyCheckCallback asCallback(long operationId) {
    return () -> scheduleCheck(operationId);
  }
}
