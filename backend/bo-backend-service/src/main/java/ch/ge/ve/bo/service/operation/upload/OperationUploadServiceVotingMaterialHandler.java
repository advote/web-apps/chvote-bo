/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload;

import static org.apache.http.entity.ContentType.APPLICATION_JSON;
import static org.apache.http.entity.ContentType.APPLICATION_OCTET_STREAM;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration;
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.status.OperationStatusAndConsistencyService;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import ch.ge.ve.bo.service.testing.card.TestingCardRegisterGeneratorService;
import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityToPrinterLinkVo;
import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityVo;
import ch.ge.ve.chvote.pactback.contract.operation.PrinterConfigurationVo;
import ch.ge.ve.chvote.pactback.contract.operation.RegisterFileEntryVo;
import ch.ge.ve.chvote.pactback.contract.operation.VotingMaterialsConfigurationSubmissionVo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.Clock;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

/**
 * {@link MultipartOperationUploadServiceHandler for voting material part}
 */
public class OperationUploadServiceVotingMaterialHandler extends MultipartOperationUploadServiceHandler {

  private final OperationStatusAndConsistencyService operationStatusAndConsistencyService;


  private       PactConfiguration          pactConfiguration;
  private final SecuredOperationRepository operationRepository;
  private final FileService                fileService;
  private final PrinterTemplateService     printerTemplateService;
  private final ObjectMapper               objectMapper;

  private final TestingCardRegisterGeneratorService testingCardRegisterGeneratorService;


  /**
   * Default constructor.
   */
  @SuppressWarnings("squid:S00107")
  public OperationUploadServiceVotingMaterialHandler(
      PactConfiguration pactConfiguration,
      SecuredOperationRepository operationRepository,
      FileService fileService,
      PrinterTemplateService printerTemplateService,
      ObjectMapper objectMapper,
      HttpClient httpClient,
      TestingCardRegisterGeneratorService testingCardRegisterGeneratorService,
      OperationStatusAndConsistencyService operationStatusAndConsistencyService,
      Clock clock
  ) {
    super(clock, pactConfiguration, httpClient);
    this.operationStatusAndConsistencyService = operationStatusAndConsistencyService;
    this.pactConfiguration = pactConfiguration;
    this.operationRepository = operationRepository;
    this.fileService = fileService;
    this.printerTemplateService = printerTemplateService;
    this.objectMapper = objectMapper;
    this.testingCardRegisterGeneratorService = testingCardRegisterGeneratorService;
  }

  @Override
  public void upload(long operationId) throws PactRequestException {
    if (!operationStatusAndConsistencyService.getOperationStatusAndConsistency(operationId)
                                             .isVotingMaterialCompleteAndConsistent()) {
      throw new TryToUploadUncompleteOrInconsitentOperation();
    }

    try {
      MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
      multipartEntityBuilder.setMode(HttpMultipartMode.RFC6532);
      Operation operation = operationRepository.findOne(operationId, true);
      List<FileVo> registerFiles = fileService.getFiles(operationId, true, EnumSet.of(FileType.REGISTER));

      // Create object to submit
      VotingMaterialsConfigurationSubmissionVo submission = new VotingMaterialsConfigurationSubmissionVo();
      submission.setUser(ConnectedUser.get().login);
      submission.setSimulationName(operation.getSimulationName());
      submission.setTarget(operation.getDeploymentTarget() == DeploymentTarget.REAL ?
                               ch.ge.ve.chvote.pactback.contract.operation.DeploymentTarget.REAL :
                               ch.ge.ve.chvote.pactback.contract.operation.DeploymentTarget.SIMULATION);
      submission.setVotingCardLabel(operation.getVotingCardTitle());
      populatePrinterConfig(operation, submission);

      Set<RegisterFileEntryVo> fileEntries = new LinkedHashSet<>();
      fileEntries.addAll(populateRegisterFileMetaData(registerFiles, multipartEntityBuilder));
      fileEntries.addAll(getRegisterForTestingCards(operationId, operation.getShortLabel(), multipartEntityBuilder));
      submission.setRegisterFilesCatalog(fileEntries);

      multipartEntityBuilder
          .addPart("configuration", new StringBody(objectMapper.writeValueAsString(submission), APPLICATION_JSON));

      HttpEntity reqEntity = multipartEntityBuilder.build();
      doMultipartPost(pactConfiguration.getUrl().getVotingMaterialUpload(), operationId, reqEntity);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }

  }


  private void populatePrinterConfig(Operation operation, VotingMaterialsConfigurationSubmissionVo submission) {
    PrinterTemplateVo printerConfig = printerTemplateService.getPrinterTemplate(
        operation.getCanton(), operation.getPrinterTemplate());
    submission.setMunicipalities(getMunicipalitiesFromTemplate(printerConfig));
    submission.setVirtualMunicipalities(
        Stream.concat(Stream.of(printerConfig.printerConfigurations),
                      Stream.of(printerTemplateService.getVirtualPrinterForTestCard()))
              .map(this::createVirtualMunicipality)
              .collect(Collectors.toCollection(LinkedHashSet::new)));

    submission.setPrinters(getAllPrinters(printerConfig));
    submission.setMunicipalitiesToPrintersLinks(
        getMunicipalitiesToPrintersLinks(printerConfig, printerTemplateService.getVirtualPrinterForTestCard()));

    submission.setSwissAbroadWithoutMunicipalityPrinter(
        submission.getPrinters().stream()
                  .filter(pc -> pc.getId().equals(printerConfig.printerSwissAbroadMapping.printerId))
                  .findFirst()
                  .orElseThrow(() -> new TechnicalException("Swiss abroad printer is not in the printer list"))
    );
  }


  private Set<PrinterConfigurationVo> getAllPrinters(PrinterTemplateVo printerConfig) {
    Function<PrinterConfiguration, PrinterConfigurationVo> printerVoBuilder = p -> {
      PrinterConfigurationVo printer = new PrinterConfigurationVo();
      printer.setId(p.id);
      printer.setName(p.name);
      printer.setPublicKey(p.publicKey);
      return printer;
    };
    LinkedHashSet<PrinterConfigurationVo> printers = Stream.of(printerConfig.printerConfigurations)
                                                           .map(printerVoBuilder)
                                                           .collect(Collectors.toCollection(LinkedHashSet::new));

    printers.add(printerVoBuilder.apply(printerTemplateService.getVirtualPrinterForTestCard()));
    return printers;
  }

  private Set<MunicipalityToPrinterLinkVo> getMunicipalitiesToPrintersLinks(
      PrinterTemplateVo printerConfig, PrinterConfiguration virtualPrinterForTestCard) {

    LinkedHashSet<MunicipalityToPrinterLinkVo> links =
        Arrays.stream(printerConfig.printerMunicipalityMappings)
              .map(mapping -> createLink(mapping.municipalityOfsId, mapping.printerId))
              .collect(Collectors.toCollection(LinkedHashSet::new));
    links.add(createLink(virtualPrinterForTestCard.municipalityForTestingCard, virtualPrinterForTestCard.id));
    Stream.of(printerConfig.printerConfigurations)
          .forEach(pc -> links.add(createLink(pc.municipalityForTestingCard, pc.id)));
    return new LinkedHashSet<>(links);
  }

  private List<RegisterFileEntryVo> getRegisterForTestingCards(
      long operationId, String operationLabel, MultipartEntityBuilder multipartEntityBuilder) throws IOException {

    Map<VoterTestingCardsLot, Map<String, String>> testingCardXmls =
        testingCardRegisterGeneratorService.generateForOperation(operationId, false);
    return attachRegisterForTestingCards(operationLabel, testingCardXmls, multipartEntityBuilder)
        .stream()
        .map(attachmentFileEntryVo -> {
               RegisterFileEntryVo registerFileEntryVo = new RegisterFileEntryVo();
               registerFileEntryVo.setZipFileName(attachmentFileEntryVo.getZipFileName());
               registerFileEntryVo.setImportDateTime(attachmentFileEntryVo.getImportDateTime());
               return registerFileEntryVo;
             }
        ).collect(Collectors.toList());
  }

  private List<RegisterFileEntryVo> populateRegisterFileMetaData(
      List<FileVo> registerFiles,
      MultipartEntityBuilder multipartEntityBuilder) {
    return
        registerFiles.stream().map(f -> {
          RegisterFileEntryVo entry = new RegisterFileEntryVo();
          entry.setImportDateTime(f.saveDate);
          String zipFileName = getZipFileName(f);
          entry.setZipFileName(zipFileName);
          multipartEntityBuilder
              .addPart("data", new ByteArrayBody(f.fileContent, APPLICATION_OCTET_STREAM, zipFileName));
          return entry;
        }).collect(Collectors.toList());
  }

  private String getZipFileName(FileVo file) {
    return String.format("%s.%s.zip", file.type, file.id);
  }

  private Set<MunicipalityVo> getMunicipalitiesFromTemplate
      (PrinterTemplateVo printerConfig) {
    return Stream.of(printerConfig.printerMunicipalityMappings)
                 .map(mapping -> {
                   MunicipalityVo municipalityVo = new MunicipalityVo();
                   municipalityVo.setName(mapping.municipalityName);
                   municipalityVo.setOfsId(mapping.municipalityOfsId);
                   return municipalityVo;
                 })
                 .collect(Collectors.toCollection(LinkedHashSet::new));
  }

  private MunicipalityToPrinterLinkVo createLink(int ofsId, String printerId) {
    MunicipalityToPrinterLinkVo vo = new MunicipalityToPrinterLinkVo();
    vo.setMunicipalityOfsId(ofsId);
    vo.setPrinterId(printerId);
    return vo;
  }

}
