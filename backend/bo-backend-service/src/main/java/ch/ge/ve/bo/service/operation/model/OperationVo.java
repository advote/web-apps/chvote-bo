/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.bo.repository.entity.BaseEntity;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.service.model.BaseVo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Value object representing an electoral operation.
 */
public class OperationVo extends BaseVo {

  public final String            shortLabel;
  public final String            longLabel;
  public final LocalDateTime     date;
  public final Integer           gracePeriod;
  public final List<MilestoneVo> milestones;
  public final LocalDateTime     lastModificationDate;
  public final String            simulationName;
  public final DeploymentTarget  deploymentTarget;
  public final LocalDateTime     lastConfigurationUpdateDate;
  public final String            votingCardTitle;
  public final String            printerTemplate;
  public final String            managementEntity;
  public final Set<String>       guestManagementEntities;

  /**
   * Default constructor
   */
  @JsonCreator
  public OperationVo(@JsonProperty("id") long id,
                     @JsonProperty("login") String login,
                     @JsonProperty("saveDate") LocalDateTime saveDate,
                     @JsonProperty("shortLabel") String shortLabel,
                     @JsonProperty("longLabel") String longLabel,
                     @JsonProperty("date") LocalDateTime date,
                     @JsonProperty("gracePeriod") Integer gracePeriod,
                     @JsonProperty("milestones") List<MilestoneVo> milestones,
                     @JsonProperty("lastModificationDate") LocalDateTime lastModificationDate,
                     @JsonProperty("simulationName") String simulationName,
                     @JsonProperty("deploymentTarget") DeploymentTarget deploymentTarget,
                     @JsonProperty("lastConfigurationUpdateDate") LocalDateTime lastConfigurationUpdateDate,
                     @JsonProperty("votingCardTitle") String votingCardTitle,
                     @JsonProperty("printerTemplate") String printerTemplate,
                     @JsonProperty("managementEntity") String managementEntity,
                     @JsonProperty("guestManagementEntities") Set<String> guestManagementEntities
  ) {
    super(id, login, saveDate);
    this.shortLabel = shortLabel;
    this.longLabel = longLabel;
    this.date = date.withHour(0).withMinute(0).withSecond(0);
    this.gracePeriod = gracePeriod;
    this.milestones = milestones;
    this.lastModificationDate = lastModificationDate;
    this.simulationName = simulationName;
    this.deploymentTarget = deploymentTarget;
    this.lastConfigurationUpdateDate = lastConfigurationUpdateDate;
    this.votingCardTitle = votingCardTitle;
    this.printerTemplate = printerTemplate;
    this.managementEntity = managementEntity;
    this.guestManagementEntities = new TreeSet<>(guestManagementEntities);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("OperationVo{");
    sb.append("id=").append(id);
    sb.append(", shortLabel='").append(shortLabel).append('\'');
    sb.append(", longLabel='").append(longLabel).append('\'');
    sb.append(", date=").append(date != null ? date.format(BaseEntity.DATE_FORMATTER) : null);
    sb.append(", gracePeriod=").append(gracePeriod);
    sb.append(", milestones=").append(milestones);
    sb.append(", login=").append(login);
    sb.append(", saveDate=").append(saveDate != null ? saveDate.format(BaseEntity.DATE_FORMATTER) : null);
    sb.append('}');
    return sb.toString();
  }
}
