/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorScope.CONFIGURATION;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorScope.VOTING_MATERIAL;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * {@link OperationStatusVo} with  consistency status ({@link ConsistencyResultVo})
 */
public class OperationStatusAndConsistencyVo extends OperationStatusVo {
  private final boolean                  consistencyComputationInProgress;
  private final List<ConsistencyErrorVo> configurationConsistencyErrors;
  private final List<ConsistencyErrorVo> votingMaterialConsistencyErrors;

  /**
   * Default constructor
   */
  public OperationStatusAndConsistencyVo(OperationStatusVo status, ConsistencyResultVo consistencyResult) {
    super(status.getConfigurationStatus(), status.getVotingMaterialStatus(), status.getVotingPeriodStatus(),
          status.getTallyArchiveStatus(),
          status.getSimulationName(), status.getDeploymentTarget());
    consistencyComputationInProgress = consistencyResult == null;
    if (consistencyComputationInProgress) {
      configurationConsistencyErrors = Collections.emptyList();
      votingMaterialConsistencyErrors = Collections.emptyList();
    } else {
      configurationConsistencyErrors = filterByScope(consistencyResult, CONFIGURATION);
      votingMaterialConsistencyErrors = filterByScope(consistencyResult, VOTING_MATERIAL);


      // mark sections in error
      configurationConsistencyErrors.forEach(error -> error.errorType.getLinkedSections().forEach(
          section -> getConfigurationStatus().markSectionInError(section.getSectionName())));
      votingMaterialConsistencyErrors.forEach(error -> error.errorType.getLinkedSections().forEach(
          section -> getVotingMaterialStatus().markSectionInError(section.getSectionName())));
    }
  }

  private List<ConsistencyErrorVo> filterByScope(ConsistencyResultVo consistencyResult,
                                                 ConsistencyErrorVo.ConsistencyErrorScope scope) {
    return consistencyResult.consistencyErrors
        .stream()
        .filter(error -> error.errorType.getScope() == scope)
        .collect(Collectors.toList());
  }

  public boolean isConsistencyComputationInProgress() {
    return consistencyComputationInProgress;
  }

  public List<ConsistencyErrorVo> getConfigurationConsistencyErrors() {
    return configurationConsistencyErrors;
  }

  public List<ConsistencyErrorVo> getVotingMaterialConsistencyErrors() {
    return votingMaterialConsistencyErrors;
  }

  public boolean isConfigurationCompleteAndConsistent() {
    return !consistencyComputationInProgress &&
           configurationConsistencyErrors.isEmpty() &&
           this.getConfigurationStatus().getState() != ConfigurationStatusVo.State.INCOMPLETE;
  }

  public boolean isVotingMaterialCompleteAndConsistent() {
    return !consistencyComputationInProgress &&
           votingMaterialConsistencyErrors.isEmpty() &&
           this.getVotingMaterialStatus() != null &&
           this.getVotingMaterialStatus().getState() != VotingMaterialStatusVo.State.INCOMPLETE;
  }
}
