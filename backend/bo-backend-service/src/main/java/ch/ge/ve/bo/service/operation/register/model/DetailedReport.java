/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang.StringUtils;

/**
 * Detailed report of a register files
 */
public class DetailedReport {
  @JsonProperty
  public final  Set<Municipality> municipalities;
  @JsonIgnore
  private final Set<String>       internalDoi;
  @JsonProperty
  private final Set<Resident>     swiss;
  @JsonProperty
  private final Set<Resident>     foreigner;
  @JsonProperty
  private final Set<SwissAbroad>  swissAbroad;

  /**
   * Default json constructor
   */
  @JsonCreator
  public DetailedReport(
      @JsonProperty("domainOfInfluence") String domainOfInfluence,
      @JsonProperty("swiss") Set<Resident> swiss,
      @JsonProperty("foreigner") Set<Resident> foreigner,
      @JsonProperty("swissAbroad") Set<SwissAbroad> swissAbroad,
      @JsonProperty("municipalities") Set<Municipality> municipalities
  ) {
    this.internalDoi =
        Stream.of(domainOfInfluence.split(",\\s*")).collect(Collectors.toCollection(TreeSet::new));
    this.swiss = new TreeSet<>(swiss);
    this.foreigner = new TreeSet<>(foreigner);
    this.swissAbroad = new TreeSet<>(swissAbroad);
    this.municipalities = municipalities;
  }

  /**
   * Constructor for empty report
   */
  public DetailedReport() {
    this.internalDoi = new TreeSet<>();
    this.swiss = new TreeSet<>();
    this.foreigner = new TreeSet<>();
    this.swissAbroad = new TreeSet<>();
    this.municipalities = new TreeSet<>();
  }


  /**
   * Merge detailed report to support aggregated one
   */
  public static DetailedReport merge(DetailedReport... reports) {
    DetailedReport detailedReport = new DetailedReport();
    Stream.of(reports).forEach(report -> {
      detailedReport.internalDoi.addAll(report.internalDoi);
      add(detailedReport.swiss, report.swiss);
      add(detailedReport.foreigner, report.foreigner);
      add(detailedReport.swissAbroad, report.swissAbroad);
      detailedReport.municipalities.addAll(report.municipalities);
    });
    return detailedReport;
  }

  private static <T extends Counted> void add(Set<T> dest, Set<T> origin) {
    origin.forEach(newValue -> {
      if (!dest.contains(newValue)) {
        dest.add(newValue);
      } else {
        dest.stream()
            .filter(oldValue -> oldValue.equals(newValue))
            .findFirst()
            // Always present by construction
            .ifPresent(oldValue -> oldValue.increment(newValue.getCount()));
      }
    });
  }

  @JsonProperty
  public String getDomainOfInfluence() {
    return String.join(", ", internalDoi);
  }


  /**
   * Add a swiss person
   */
  public void addSwiss(String municipality) {
    add(this.swiss, Collections.singleton(new Resident(municipality)));
  }

  /**
   * Add a foreigner person
   */
  public void addForeigner(String municipality) {
    add(this.foreigner, Collections.singleton(new Resident(municipality)));
  }

  /**
   * Add a swiss abroad person
   */
  public void addSwissAbroad(String lang, Integer postageCode) {
    add(this.swissAbroad, Collections.singleton(new SwissAbroad(lang, postageCode)));
  }

  /**
   * Add a doi
   */
  public void addDoi(String doi) {
    if (StringUtils.isNotBlank(doi)) {
      this.internalDoi.add(doi);
    }
  }

  /**
   * Add a municipality
   */
  public void addMunicipality(Integer id, String name) {
    municipalities.add(new Municipality(id, name));
  }


  interface Counted {

    void increment(int n);

    int getCount();
  }

  private static class Resident implements Counted, Comparable<Resident> {

    private static final Comparator<String>   nullSafeStringComparator = Comparator
        .nullsFirst(String::compareToIgnoreCase);
    private static final Comparator<Resident> metadataComparator       = Comparator
        .comparing(Resident::getMunicipality, nullSafeStringComparator);

    @JsonProperty
    private final String municipality;
    @JsonProperty
    private       int    count;

    @JsonCreator
    private Resident(@JsonProperty("municipality") String municipality, @JsonProperty("count") int count) {
      this(municipality);
      this.count = count;
    }

    private Resident(String municipality) {
      this.municipality = municipality;
      count = 1;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Resident resident = (Resident) o;
      return Objects.equals(municipality, resident.municipality);
    }

    @Override
    public int hashCode() {
      return Objects.hash(municipality);
    }

    @Override
    public void increment(int n) {
      count += n;
    }

    @Override
    public int getCount() {
      return count;
    }

    String getMunicipality() {
      return municipality;
    }

    @Override
    public int compareTo(@NotNull Resident resident) {
      // safe comparaison
      // note : org.apache.commons.lang.compare does not give the same result
      return metadataComparator.compare(this, resident);
    }
  }

  private static class SwissAbroad implements Counted, Comparable<SwissAbroad> {

    @JsonProperty
    private final String                  lang;
    @JsonProperty("postage-code")
    private final Integer                 postageCode;
    private       int                     count;
    private final Comparator<SwissAbroad> comparator = Comparator.comparing(SwissAbroad::getLang)
                                                                 .thenComparing(SwissAbroad::getPostageCode);

    @JsonCreator
    private SwissAbroad(@JsonProperty("lang") String lang,
                        @JsonProperty("postage-code") Integer postageCode,
                        @JsonProperty("count") int count) {
      this(lang, postageCode);
      this.count = count;
    }

    private SwissAbroad(String lang, Integer postageCode) {
      this.lang = lang;
      this.postageCode = postageCode;
      count = 1;
    }

    String getLang() {
      return lang;
    }

    Integer getPostageCode() {
      return postageCode;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      SwissAbroad that = (SwissAbroad) o;
      return Objects.equals(lang, that.lang) &&
             Objects.equals(postageCode, that.postageCode);
    }

    @Override
    public int hashCode() {
      return Objects.hash(lang, postageCode);
    }

    @Override
    public void increment(int n) {
      count += n;
    }

    @Override
    public int getCount() {
      return count;
    }

    @Override
    public int compareTo(@NotNull SwissAbroad other) {
      return comparator.compare(this, other);
    }
  }

  public static class Municipality implements Comparable<Municipality> {
    public final Integer ofsId;
    public final String  name;

    @JsonCreator
    Municipality(@JsonProperty("ofsId") Integer ofsId, @JsonProperty("name") String name) {
      this.ofsId = ofsId;
      this.name = name;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      Municipality that = (Municipality) o;
      return Objects.equals(ofsId, that.ofsId);
    }

    @Override
    public int hashCode() {
      return Objects.hash(ofsId);
    }

    @Override
    public int compareTo(@NotNull Municipality other) {
      return this.ofsId.compareTo(other.ofsId);
    }
  }
}
