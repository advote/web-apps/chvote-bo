/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.doi.model;

/**
 * Builder for {@link DomainOfInfluenceVo} immutable objects.
 */
public class DomainOfInfluenceVoBuilder {
  private String type;
  private String id;
  private String name;
  private String shortName;

  /**
   * Set type of DOI
   */
  public DomainOfInfluenceVoBuilder type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Set id of DOI
   */
  public DomainOfInfluenceVoBuilder id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Set name of DOI
   */
  public DomainOfInfluenceVoBuilder name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Set shortName of DOI
   */
  public DomainOfInfluenceVoBuilder shortName(String shortName) {
    this.shortName = shortName;
    return this;
  }

  /**
   * @return a {@link DomainOfInfluenceVo} corresponding to builder configuration
   */
  public DomainOfInfluenceVo build() {
    return new DomainOfInfluenceVo(type, id, name, shortName);
  }
}