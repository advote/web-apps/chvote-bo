/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;


import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.entity.LocalizedHighlightedQuestion;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

/**
 * Value object for  {@link LocalizedHighlightedQuestion}
 */
public class LocalizedHighlightedQuestionVo {

  private final Long          id;
  private final String        fileName;
  private final LocalDateTime date;
  private final String        localizedQuestion;
  private final Language      language;

  /**
   * Default constructor
   */
  @JsonCreator
  public LocalizedHighlightedQuestionVo(
      @JsonProperty("id") Long id,
      @JsonProperty("fileName") String fileName,
      @JsonProperty("date") LocalDateTime date,
      @JsonProperty("localizedQuestion") String localizedQuestion,
      @JsonProperty("language") Language language) {
    this.id = id;
    this.fileName = fileName;
    this.date = date;
    this.localizedQuestion = localizedQuestion;
    this.language = language;
  }


  /**
   * Constructor using entity
   */
  public LocalizedHighlightedQuestionVo(LocalizedHighlightedQuestion question) {
    this(question.getId(),
         question.getFileName(),
         question.getSaveDate(),
         question.getLocalizedQuestion(),
         question.getLanguage());
  }

  public Long getId() {
    return id;
  }

  public String getFileName() {
    return fileName;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public String getLocalizedQuestion() {
    return localizedQuestion;
  }

  public Language getLanguage() {
    return language;
  }

}
