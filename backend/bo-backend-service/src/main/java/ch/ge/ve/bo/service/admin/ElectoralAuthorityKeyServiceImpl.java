/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.admin;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.ElectoralAuthorityKeyRepository;
import ch.ge.ve.bo.repository.entity.ElectoralAuthorityKey;
import ch.ge.ve.bo.service.electoralAuthorityKey.model.ElectoralAuthorityKeyVo;
import ch.ge.ve.bo.service.exception.SecurityException;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link ElectoralAuthorityKeyService}
 */
public class ElectoralAuthorityKeyServiceImpl implements ElectoralAuthorityKeyService {

  private final ElectoralAuthorityKeyRepository repository;
  private final Clock                           clock;


  /**
   * Default constructor
   */
  public ElectoralAuthorityKeyServiceImpl(ElectoralAuthorityKeyRepository repository, Clock clock) {
    this.repository = repository;
    this.clock = clock;
  }

  @Override
  public ElectoralAuthorityKeyVo updateLabel(long id, String newLabel) {
    ElectoralAuthorityKey key = repository.getOne(id);
    if (!key.getManagementEntity().equals(ConnectedUser.get().managementEntity)) {
      throw new SecurityException(
          String.format("Could not modify key belonging to %s by user %s belonging to %s",
                        key.getManagementEntity(), ConnectedUser.get().login, ConnectedUser.get().managementEntity));
    }
    key.setKeyName(newLabel);

    return new ElectoralAuthorityKeyVo(repository.save(key));
  }

  @Override
  public ElectoralAuthorityKeyVo create(String label, byte[] content) {
    ElectoralAuthorityKey key = new ElectoralAuthorityKey();
    key.setKeyName(label);
    key.setKeyContent(content);
    key.setManagementEntity(ConnectedUser.get().managementEntity);
    key.setLogin(ConnectedUser.get().login);
    key.setImportDate(LocalDateTime.now(clock));
    return new ElectoralAuthorityKeyVo(repository.save(key));
  }

  @Override
  public List<ElectoralAuthorityKeyVo> findAll() {
    return repository.findAllByManagementEntity(ConnectedUser.get().managementEntity)
                     .stream()
                     .map(ElectoralAuthorityKeyVo::new)
                     .collect(Collectors.toList());
  }
}
