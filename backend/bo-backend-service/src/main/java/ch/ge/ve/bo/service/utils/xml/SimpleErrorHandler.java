/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.utils.xml;

import ch.ge.ve.bo.service.model.SchemaValidationError;
import java.util.LinkedList;
import java.util.List;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

/**
 * Aggregator of SchemaValidationError when parsing xml
 */
public class SimpleErrorHandler implements ErrorHandler {
  private final List<SchemaValidationError> errors = new LinkedList<>();

  @Override
  public void warning(SAXParseException exception) {
    errors.add(new SchemaValidationError(exception));
  }

  @Override
  public void error(SAXParseException exception) {
    errors.add(new SchemaValidationError(exception));
  }

  @Override
  public void fatalError(SAXParseException exception) {
    errors.add(new SchemaValidationError(exception));
  }

  /**
   * @return list of errors
   */
  public List<SchemaValidationError> getErrors() {
    return errors;
  }
}
