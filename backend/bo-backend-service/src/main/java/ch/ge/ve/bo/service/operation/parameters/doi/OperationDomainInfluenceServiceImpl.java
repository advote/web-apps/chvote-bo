/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.doi;


import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.exception.InvalidFileException;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceFileVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceImportResult;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVoBuilder;
import ch.ge.ve.bo.service.utils.xml.SimpleErrorHandler;
import ch.ge.ve.bo.service.utils.xml.XSDValidator;
import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceType;
import ch.ge.ve.interfaces.ech.service.EchCodec;
import ch.ge.ve.interfaces.ech.service.EchDeserializationRuntimeException;
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl;
import ch.ge.ve.interfaces.logistic.Delivery;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import org.apache.commons.io.IOUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the {@link OperationDomainInfluenceService} interface.
 */
public class OperationDomainInfluenceServiceImpl implements OperationDomainInfluenceService {

  private FileService        fileService;
  private EchCodec<Delivery> logisticFactory;
  private XSDValidator       logisticDeliveryValidator;

  /**
   * Default constructor
   */
  public OperationDomainInfluenceServiceImpl(FileService fileService, XSDValidator logisticDeliveryValidator) {
    this.fileService = fileService;
    this.logisticDeliveryValidator = logisticDeliveryValidator;
    logisticFactory = new JAXBEchCodecImpl<>(Delivery.class);
  }

  /**
   * Setter used for testing purposes only.
   *
   * @param logisticFactory the new LogisticFactory instance
   */
  protected void setLogisticFactory(EchCodec<Delivery> logisticFactory) {
    this.logisticFactory = logisticFactory;
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public DomainOfInfluenceImportResult importFile(long operationId, String fileName, InputStream inputStream)
      throws BusinessException {

    // check that the file is an XML file
    if (!fileName.toLowerCase(Locale.ENGLISH).endsWith(FileVo.XML_EXTENSION)) {
      throw new InvalidFileException("global.errors.not-xml-file");
    }

    try {
      byte[] fileContent = IOUtils.toByteArray(inputStream);

      SimpleErrorHandler errorHandler = new SimpleErrorHandler();
      DomainOfInfluenceFileVo file = null;

      // validate stream
      logisticDeliveryValidator.validate(new ByteArrayInputStream(fileContent), errorHandler);

      if (errorHandler.getErrors().isEmpty()) {
        Delivery logisticDelivery = logisticFactory.deserialize(new ByteArrayInputStream(fileContent), false);

        Optional<FileVo> alreadyPresentFile =
            fileService.getFiles(operationId, false, Collections.singleton(FileType.DOMAIN_OF_INFLUENCE)).stream()
                       .findFirst();
        alreadyPresentFile.ifPresent(fileVo -> fileService.deleteFile(fileVo.id));

        FileVo applicationFileVo = fileService.importFile(
            new FileVo(
                getBusinessKey(logisticDelivery),
                operationId,
                FileType.DOMAIN_OF_INFLUENCE,
                null,
                fileName,
                ConnectedUser.get().managementEntity,
                fileContent
            ), false);

        file = buildFrom(applicationFileVo, false);

      }

      return new DomainOfInfluenceImportResult(file, errorHandler.getErrors());

    } catch (IOException | EchDeserializationRuntimeException e) {
      throw new TechnicalException(e);
    }
  }

  private String getBusinessKey(Delivery logisticDelivery) throws InvalidFileException {
    return fileService.buildApplicationBusinessKey(
        logisticDelivery.getDeliveryHeader().getSenderId(),
        logisticDelivery.getDeliveryHeader().getMessageId());
  }

  @Override
  @Transactional(readOnly = true)
  public DomainOfInfluenceFileVo getFile(long operationId, boolean withContent) {
    Optional<FileVo> applicationFile =
        fileService.getFiles(operationId, FileType.DOMAIN_OF_INFLUENCE, null)
                   .stream()
                   .findFirst();
    return applicationFile.map(applicationFileVo -> buildFrom(applicationFileVo, withContent)).orElse(null);
  }

  /**
   * Build votation file information based on the given application file.
   *
   * @param applicationFile the concerned application file
   * @param withContent     if true, include the file's content
   *
   * @return the corresponding votation repository file information
   */
  private DomainOfInfluenceFileVo buildFrom(FileVo applicationFile, boolean withContent) {
    // parse file content to extract domain of influence information
    Delivery delivery;
    try {
      delivery = logisticFactory.deserialize(new ByteArrayInputStream(applicationFile.fileContent), false);
    } catch (EchDeserializationRuntimeException e) {
      throw new TechnicalException(e);
    }

    List<DomainOfInfluenceVo> details = new ArrayList<>();
    for (DomainOfInfluenceType doi : delivery.getDomainOfInfluence()) {
      details.add(new DomainOfInfluenceVoBuilder().type(doi.getDomainOfInfluenceType().value())
                                                  .id(doi.getLocalDomainOfInfluenceIdentification())
                                                  .name(doi.getDomainOfInfluenceName())
                                                  .shortName(doi.getDomainOfInfluenceShortname())
                                                  .build());
    }

    return new DomainOfInfluenceFileVo(
        applicationFile.id,
        applicationFile.login,
        applicationFile.saveDate,
        applicationFile.businessKey,
        applicationFile.operationId,
        applicationFile.type,
        applicationFile.language,
        applicationFile.fileName,
        applicationFile.managementEntity,
        withContent ? applicationFile.fileContent : null,
        details
    );
  }
}
