/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.chvote.pactback.contract.operation.status.ProtocolStageProgressVo;
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

/**
 * information about voting material creation progress
 */
public class VotingMaterialCreationProgress {
  private static Clock clock = Clock.systemDefaultZone();

  private static final int        PROGRESS_TIMEOUT = 120;
  private final        List<Step> steps;

  /**
   * Default constructor
   */
  public VotingMaterialCreationProgress(List<ProtocolStageProgressVo<VotingMaterialCreationStage>> protocolProgress) {
    Map<VotingMaterialCreationStage, Step> convertedStep = new TreeMap<>();

    Stream.of(VotingMaterialCreationStage.values())
          .forEach(protocolStage -> convertedStep.put(protocolStage, new Step(protocolStage.name())));

    protocolProgress
        .forEach(progress -> convertedStep.get(progress.getStage())
            .componentControlStepProgressions.add(new ComponentControlStepProgression(progress)));

    steps = new ArrayList<>(convertedStep.values());
  }

  public List<Step> getSteps() {
    return steps;
  }

  public boolean isStuck() {
    return steps.stream().anyMatch(Step::isStuck);
  }

  /**
   * Default constructor
   */
  public static class Step {
    private String                                stepId;
    private List<ComponentControlStepProgression> componentControlStepProgressions;

    /**
     * Default constructor
     */
    public Step(String stepId) {
      this.stepId = stepId;
      this.componentControlStepProgressions = new LinkedList<>();
    }

    public String getStepId() {
      return stepId;
    }

    public List<ComponentControlStepProgression> getComponentControlStepProgressions() {
      return componentControlStepProgressions;
    }

    public boolean isStuck() {
      return componentControlStepProgressions.stream().anyMatch(ComponentControlStepProgression::isStuck);
    }

    public boolean isStepStarted() {
      return !componentControlStepProgressions.isEmpty();
    }

    public int getPercent() {
      return componentControlStepProgressions.stream()
                                             .map(ComponentControlStepProgression::getPercent)
                                             .min(Comparator.comparingInt(o -> o)).orElse(0);
    }

    /**
     * @deprecated Used only for test
     * @param clock to set
     */
    @Deprecated
    protected static void setClock(Clock clock) {
      VotingMaterialCreationProgress.clock = clock;
    }

  }


  public static class ComponentControlStepProgression {

    private final int     percent;
    private final boolean stuck;
    private final Integer index;

    /**
     * A Step in voting material creation process
     */
    public ComponentControlStepProgression(ProtocolStageProgressVo progress) {
      this.percent = (int) (progress.getRatio() * 100);
      this.stuck = isStuck(progress);
      this.index = progress.getCcIndex();
    }

    private boolean isStuck(ProtocolStageProgressVo progress) {
      if (progress.getLastModificationDate() == null) {
        return false;
      }
      return progress.getRatio() < 1 && (Duration.between(progress.getLastModificationDate(), LocalDateTime.now(clock))
                                                 .compareTo(Duration.ofSeconds(PROGRESS_TIMEOUT)) > 0);
    }

    public int getPercent() {
      return percent;
    }

    public boolean isStuck() {
      return stuck;
    }

    public Integer getIndex() {
      return index;
    }
  }
}
