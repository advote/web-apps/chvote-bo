/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository.model;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.service.file.model.FileVo;
import java.time.LocalDateTime;

/**
 * Value object representing  Repository {@link ch.ge.ve.bo.repository.entity.File}
 */
public abstract class RepositoryFileVo extends FileVo {

  private final String        contestIdentification;
  private final LocalDateTime contestDate;
  private final String        contestDescription;

  /**
   * Default constructor
   */
  @SuppressWarnings("squid:S00107") // suppress Sonar check on parameters count
  public RepositoryFileVo(
      long id, String login, LocalDateTime saveDate, String businessKey, long operationId, FileType type,
      Language language, String fileName, String managementEntity, byte[] fileContent, String contestIdentification,
      LocalDateTime contestDate, String contestDescription) {
    super(id, login, saveDate, businessKey, operationId, type, language, fileName, managementEntity, fileContent);
    this.contestIdentification = contestIdentification;
    this.contestDate = contestDate;
    this.contestDescription = contestDescription;
  }

  public String getContestIdentification() {
    return contestIdentification;
  }

  public LocalDateTime getContestDate() {
    return contestDate;
  }

  public String getContestDescription() {
    return contestDescription;
  }




}
