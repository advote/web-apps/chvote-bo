/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload;

import static ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo.State.COMPLETE;
import static ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo.State.INCOMPLETE;
import static ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo.State.INITIALIZATION_FAILED;
import static ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo.State.INITIALIZATION_REJECTED;
import static ch.ge.ve.bo.service.utils.PactServiceUtils.attachCredentialsToRequest;
import static org.apache.http.entity.ContentType.APPLICATION_OCTET_STREAM;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.electoralAuthorityKey.model.ElectoralAuthorityKeyVo;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo;
import ch.ge.ve.bo.service.operation.status.OperationStatusService;
import ch.ge.ve.bo.service.operation.voting.period.ElectoralAuthorityKeyConfigurationService;
import ch.ge.ve.bo.service.operation.voting.period.VotingSitePeriodService;
import ch.ge.ve.bo.service.operation.voting.period.model.VotingSitePeriodVO;
import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo;
import ch.ge.ve.chvote.pactback.contract.operation.VotingPeriodConfigurationSubmissionVo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.EnumSet;
import javax.persistence.EntityNotFoundException;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.BasicResponseHandler;

/**
 * {@link OperationUploadServiceHandler for voting period part}
 */
public class OperationUploadServiceVotingPeriodHandler implements OperationUploadServiceHandler {

  private final HttpClient                                httpClient;
  private final OperationStatusService                    operationStatusService;
  private final PactConfiguration                         pactConfiguration;
  private final ObjectMapper                              objectMapper;
  private final ElectoralAuthorityKeyConfigurationService authorityKeyConfigurationService;
  private final VotingSitePeriodService                   votingSitePeriodService;
  private final SecuredOperationRepository                operationRepository;

  /**
   * Default constructor.
   */
  public OperationUploadServiceVotingPeriodHandler(
      PactConfiguration pactConfiguration,
      ObjectMapper objectMapper,
      HttpClient httpClient,
      OperationStatusService operationStatusService,
      ElectoralAuthorityKeyConfigurationService authorityKeyConfigurationService,
      VotingSitePeriodService votingSitePeriodService,
      SecuredOperationRepository operationRepository
  ) {
    this.httpClient = httpClient;

    this.operationStatusService = operationStatusService;
    this.pactConfiguration = pactConfiguration;
    this.objectMapper = objectMapper;
    this.authorityKeyConfigurationService = authorityKeyConfigurationService;
    this.votingSitePeriodService = votingSitePeriodService;
    this.operationRepository = operationRepository;
  }

  @Override
  public void upload(long operationId) throws PactRequestException {
    verifyStatus(operationId);
    postSubmission(operationId, createSubmission(operationId));
  }

  private void verifyStatus(long operationId) throws PactRequestException {
    VotingPeriodStatusVo votingPeriodStatus = operationStatusService.getStatus(operationId).getVotingPeriodStatus();

    if (votingPeriodStatus == null ||
        votingPeriodStatus.state == INCOMPLETE) {
      throw new TryToUploadUncompleteOrInconsitentOperation();
    }
    if (!EnumSet.of(COMPLETE, INITIALIZATION_FAILED, INITIALIZATION_REJECTED)
                .contains(votingPeriodStatus.state)) {
      throw new TryToUploadAnOperationWithIllegalState(votingPeriodStatus.state);
    }
  }

  private VotingPeriodConfigurationSubmissionVo createSubmission(long operationId) {
    VotingPeriodConfigurationSubmissionVo submission = new VotingPeriodConfigurationSubmissionVo();

    Operation operation = operationRepository.findOne(operationId, true);

    ElectoralAuthorityKeyVo electoralAuthorityKeyVo = authorityKeyConfigurationService
        .findForOperation(operationId)
        .orElseThrow(() -> new EntityNotFoundException("authorityKeyConfiguration for operation " + operationId));
    AttachmentFileEntryVo publicKeyInfo = new AttachmentFileEntryVo();
    publicKeyInfo.setAttachmentType(AttachmentFileEntryVo.AttachmentType.ELECTION_OFFICER_KEY);
    publicKeyInfo.setImportDateTime(electoralAuthorityKeyVo.importDate);
    publicKeyInfo.setZipFileName(electoralAuthorityKeyVo.label);
    publicKeyInfo.setExternalIdentifier(electoralAuthorityKeyVo.id);
    submission.setElectoralAuthorityKey(publicKeyInfo);

    VotingSitePeriodVO votingSitePeriod = votingSitePeriodService
        .findForOperation(operationId)
        .orElseThrow(() -> new TechnicalException(
            "Inconsistent state should contains a voting site period in order to be completed"));
    submission.setGracePeriod(votingSitePeriod.getGracePeriod());
    submission.setSiteOpeningDate(votingSitePeriod.getDateOpen());
    submission.setSiteClosingDate(votingSitePeriod.getDateClose());

    submission.setTarget(operation.getDeploymentTarget() == DeploymentTarget.SIMULATION ?
                             ch.ge.ve.chvote.pactback.contract.operation.DeploymentTarget.SIMULATION :
                             ch.ge.ve.chvote.pactback.contract.operation.DeploymentTarget.REAL);
    submission.setSimulationName(operation.getSimulationName());
    submission.setUser(ConnectedUser.get().login);
    return submission;
  }

  private void postSubmission(long operationId, VotingPeriodConfigurationSubmissionVo submission)
      throws PactRequestException {
    String url = MessageFormat.format(pactConfiguration.getUrl().getVotingPeriodUpload(), operationId);
    try {
      HttpPost post = new HttpPost(url);
      String submissionJson = objectMapper.writeValueAsString(submission);

      HttpEntity entity = MultipartEntityBuilder
          .create()
          .addTextBody("configuration", submissionJson)
          .addPart("data", new ByteArrayBody(
              authorityKeyConfigurationService.getContentForOperation(operationId),
              APPLICATION_OCTET_STREAM,
              submission.getElectoralAuthorityKey().getZipFileName()))
          .build();
      post.setEntity(entity);
      post.setHeader("Accept", "application/json");
      attachCredentialsToRequest(post, pactConfiguration.getUsername(), pactConfiguration.getPassword());
      httpClient.execute(post, new BasicResponseHandler());
    } catch (HttpResponseException responseException) {
      throw PactRequestException.of(responseException, url);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }
}
