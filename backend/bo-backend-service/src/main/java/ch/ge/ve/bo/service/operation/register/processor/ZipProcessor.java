/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.processor;

import ch.ge.ve.bo.service.operation.register.RegisterImportProcessor;
import ch.ge.ve.bo.service.operation.register.exception.ZipProcessorException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processor responsible of creating a zip file
 */
public class ZipProcessor extends RegisterImportProcessor {
  private static final Logger logger = LoggerFactory.getLogger(ZipProcessor.class);

  private static final int INITIAL_SIZE     = 1024 * 1024;
  private static final int READ_BUFFER_SIZE = 4096;
  private final String fileName;

  private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream(INITIAL_SIZE);

  /**
   * Default constructor
   */
  public ZipProcessor(InputStream streamToProcess, Executor executor, String fileName) {
    super(streamToProcess, executor);
    this.fileName = fileName;
  }


  @Override
  protected void process() {
    byte[] buffer = new byte[READ_BUFFER_SIZE];
    try (ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream)) {
      zipOutputStream.putNextEntry(new ZipEntry(fileName));
      int bytesRead;
      while ((bytesRead = streamToProcess.read(buffer)) != -1 && !isCancelled()) {
        zipOutputStream.write(buffer, 0, bytesRead);
      }
      zipOutputStream.closeEntry();
      zipOutputStream.close();
      logger.info("Zip has been generated and has a size of {} bytes", outputStream.size());
    } catch (IOException e) {
      throw new ZipProcessorException(e);
    }
  }

  public byte[] getZipAsByteArray() {
    return outputStream.toByteArray();
  }

}
