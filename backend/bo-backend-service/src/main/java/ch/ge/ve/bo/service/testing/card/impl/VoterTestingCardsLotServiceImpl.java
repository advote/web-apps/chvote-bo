/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.testing.card.impl;

import static java.util.Collections.singleton;

import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.VoterTestingCardsLotRepository;
import ch.ge.ve.bo.repository.entity.CardType;
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot;
import ch.ge.ve.bo.service.mapper.BeanMapper;
import ch.ge.ve.bo.service.model.VoterTestingCardsLotVo;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of {@link VoterTestingCardsLotService}
 */
public class VoterTestingCardsLotServiceImpl implements VoterTestingCardsLotService {

  private final VoterTestingCardsLotRepository repository;
  private final SecuredOperationRepository     operationRepository;
  private final SecuredOperationHolderService  securedOperationHolderService;

  /**
   * Default constructor
   */
  public VoterTestingCardsLotServiceImpl(VoterTestingCardsLotRepository repository,
                                         SecuredOperationRepository operationRepository,
                                         SecuredOperationHolderService securedOperationHolderService) {
    this.repository = repository;
    this.operationRepository = operationRepository;
    this.securedOperationHolderService = securedOperationHolderService;
  }


  @Override
  @Transactional(readOnly = true)
  public List<VoterTestingCardsLotVo> findAll(Long operationId, boolean forConfiguration) {
    return securedOperationHolderService.safeReadAll(
        () -> repository.findAllByOperationIdAndCardTypeInOrderById(operationId, getCardTypes(forConfiguration))
    ).stream().map(BeanMapper::map).collect(Collectors.toList());
  }

  private Collection<CardType> getCardTypes(boolean forConfiguration) {
    return Stream.of(CardType.values())
                 .filter(cardType -> cardType.isForConfiguration() == forConfiguration)
                 .collect(Collectors.toSet());
  }

  @Override
  @Transactional
  public VoterTestingCardsLotVo create(Long operationId, VoterTestingCardsLotVo lotVo) {
    return securedOperationHolderService.doNotOptionalUpdate(
        () -> {
          VoterTestingCardsLot lot = new VoterTestingCardsLot();
          updateData(lotVo, lot);
          lot.setOperation(operationRepository.findOne(operationId, true));
          return lot;
        },
        lot -> BeanMapper.map(repository.save(lot)));
  }

  @Override
  @Transactional
  public void copyTestingCardFromTestSiteToProduction(Long operationId) {

    List<VoterTestingCardsLot> toDuplicate = securedOperationHolderService.safeReadAll(() -> repository
        .findAllByOperationIdAndCardTypeInOrderById(operationId, singleton(CardType.TEST_SITE_TESTING_CARD)));

    toDuplicate.forEach(
        lot -> securedOperationHolderService.doNotOptionalUpdate(
            () -> {
              VoterTestingCardsLot newLot = new VoterTestingCardsLot();
              newLot.setAddress1(lot.getAddress1());
              newLot.setAddress2(lot.getAddress2());
              newLot.setBirthday(lot.getBirthday());
              newLot.setCity(lot.getCity());
              newLot.setCount(lot.getCount());
              newLot.setCountry(lot.getCountry());
              newLot.setFirstName(lot.getFirstName());
              newLot.setLanguage(lot.getLanguage());
              newLot.setLastName(lot.getLastName());
              newLot.setOperation(lot.getOperation());
              newLot.setLotName(lot.getLotName());
              newLot.setStreet(lot.getStreet());
              newLot.setPostalCode(lot.getPostalCode());
              newLot.setSignature(lot.getSignature());
              newLot.setShouldPrint(false);
              newLot.setDoi(new ArrayList<>(lot.getDoi()));
              newLot.setCardType(CardType.PRODUCTION_TESTING_CARD);
              return newLot;

            },
            repository::save
        )
    );
  }


  @Override
  @Transactional
  public VoterTestingCardsLotVo edit(VoterTestingCardsLotVo lotVo) {
    return securedOperationHolderService
        .doUpdate(
            () -> repository.findById(lotVo.id).map(
                lot -> {
                  updateData(lotVo, lot);
                  return lot;
                }
            ), repository::save)
        .map(BeanMapper::map)
        .orElseThrow(() -> new EntityNotFoundException("VoterTestingCardsLot " + lotVo.id
        )

    );
  }

  private void updateData(VoterTestingCardsLotVo lotVo, VoterTestingCardsLot lot) {
    lot.setLotName(lotVo.lotName);
    lot.setCardType(lotVo.cardType);
    lot.setCount(lotVo.count);
    lot.setSignature(lotVo.signature);
    lot.setFirstName(lotVo.firstName);
    lot.setLastName(lotVo.lastName);
    lot.setBirthday(lotVo.birthday);
    lot.setAddress1(lotVo.address1);
    lot.setAddress2(lotVo.address2);
    lot.setStreet(lotVo.street);
    lot.setPostalCode(lotVo.postalCode);
    lot.setCity(lotVo.city);
    lot.setCountry(lotVo.country);
    lot.setLanguage(lotVo.language);
    lot.setShouldPrint(lotVo.shouldPrint);
    lot.setDoi(lotVo.doi);
  }

  @Override
  @Transactional(readOnly = true)
  public VoterTestingCardsLotVo findOne(long id) {
    return securedOperationHolderService.safeRead(() -> repository.findById(id))
                                        .map(BeanMapper::map)
                                        .orElseThrow(() -> new EntityNotFoundException("VoterTestingCardsLot" + id));
  }

  @Override
  @Transactional
  public boolean delete(long id) {
    securedOperationHolderService.doNotOptionalUpdateAndForget(
        () -> repository.getOne(id),
        repository::delete);
    return true;
  }


}
