/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorScope.CONFIGURATION;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorScope.VOTING_MATERIAL;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyLinkedSection.BALLOT_DOCUMENTATION;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyLinkedSection.DOMAIN_OF_INFLUENCE;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyLinkedSection.ELECTION_DISPLAY;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyLinkedSection.PRINTER;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyLinkedSection.REGISTER;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyLinkedSection.REPOSITORY;
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyLinkedSection.TESTING_CARDS;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Error related to consistency provided on status request
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsistencyErrorVo {
  private static final String CONFIGURATION_ERROR_KEY_PREFIX = "consistency.configuration.error.";
  private static final String VOTING_MATERIAL_KEY_PREFIX     = "consistency.votingMaterial.error.";

  public enum ConsistencyLinkedSection {
    BASE("base-parameter"),
    MILESTONE("milestone"),
    DOMAIN_OF_INFLUENCE("domain-of-influence"),
    REPOSITORY("repository"),
    TESTING_CARDS("testing-card-lot"),
    ELECTION_DISPLAY("election-page-properties"),
    DOCUMENTATION("document"),
    MANAGEMENT_ENTITIES("management-entity"),
    BALLOT_DOCUMENTATION("ballot-document"),
    REGISTER("register"),
    VOTING_CARD_TITLE("voting-card-title"),
    PRINTER("printer-template");

    private final String sectionName;

    ConsistencyLinkedSection(String sectionName) {
      this.sectionName = sectionName;
    }

    public String getSectionName() {
      return sectionName;
    }
  }

  public enum ConsistencyErrorScope {
    CONFIGURATION, VOTING_MATERIAL
  }

  public enum ConsistencyErrorType {
    VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY("noPrinterAssociatedToMunicipality", VOTING_MATERIAL,
                                             Arrays.asList(PRINTER, REGISTER)),
    CONF_UNKNOWN_OPERATION_REPOSITORY_DOI("unknownOperationRepositoryDoi", CONFIGURATION,
                                          Arrays.asList(DOMAIN_OF_INFLUENCE, REPOSITORY)),
    CONF_UNKNOWN_DOI_FOR_TESTING_CARD_LOT("unknownDoiForTestingCardLot", CONFIGURATION,
                                          Arrays.asList(DOMAIN_OF_INFLUENCE, TESTING_CARDS)),
    VM_UNKNOWN_DOI_FOR_TESTING_CARD_LOT("unknownDoiForTestingCardLot", VOTING_MATERIAL,
                                        Arrays.asList(DOMAIN_OF_INFLUENCE, TESTING_CARDS)),
    BALLOT_IN_DOCUMENTATION_NOT_IN_OPERATION_REPOSITORY("ballotInConfigurationNotInOperationRepository", CONFIGURATION,
                                                        Arrays.asList(BALLOT_DOCUMENTATION, REPOSITORY)),
    BALLOT_IN_ELECTION_PAGE_PROPERTIES_NOT_IN_OPERATION_REPOSITORY(
        "ballotInElectionPagePropertiesNotInOperationRepository", CONFIGURATION,
        Arrays.asList(ELECTION_DISPLAY, REPOSITORY));

    private       String                         errorKey;
    private final ConsistencyErrorScope          scope;
    private final List<ConsistencyLinkedSection> linkedSections;

    ConsistencyErrorType(String errorKey, ConsistencyErrorScope scope, List<ConsistencyLinkedSection> linkedSections) {
      this.errorKey = (scope == CONFIGURATION ? CONFIGURATION_ERROR_KEY_PREFIX : VOTING_MATERIAL_KEY_PREFIX) + errorKey;
      this.scope = scope;
      this.linkedSections = linkedSections;
    }

    public ConsistencyErrorScope getScope() {
      return scope;
    }

    public List<ConsistencyLinkedSection> getLinkedSections() {
      return linkedSections;
    }
  }


  public final ConsistencyErrorType errorType;

  public final Map<String, String> errorsParameters;

  /**
   * Default constructor
   */
  @JsonCreator
  public ConsistencyErrorVo(@JsonProperty("errorType") ConsistencyErrorType errorType,
                            @JsonProperty("errorsParameters") Map<String, String> errorsParameters) {
    this.errorType = errorType;
    this.errorsParameters = errorsParameters;
  }

  public String getErrorKey() {
    return errorType.errorKey;
  }

  public ConsistencyErrorScope getScope() {
    return errorType.scope;
  }


}
