/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload;

import static ch.ge.ve.bo.FileType.DOCUMENT_CERTIFICATE;
import static ch.ge.ve.bo.FileType.DOCUMENT_FAQ;
import static ch.ge.ve.bo.FileType.DOCUMENT_TERMS;
import static ch.ge.ve.bo.FileType.DOMAIN_OF_INFLUENCE;
import static ch.ge.ve.bo.FileType.ELECTION_REPOSITORY;
import static ch.ge.ve.bo.FileType.VOTATION_REPOSITORY;
import static org.apache.http.entity.ContentType.APPLICATION_JSON;
import static org.apache.http.entity.ContentType.APPLICATION_OCTET_STREAM;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.DictionaryDataType;
import ch.ge.ve.bo.repository.entity.Milestone;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot;
import ch.ge.ve.bo.service.dictionnary.DictionaryService;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.parameters.document.BallotDocumentationService;
import ch.ge.ve.bo.service.operation.parameters.document.HighlightedQuestionService;
import ch.ge.ve.bo.service.operation.parameters.election.ElectionPagePropertiesService;
import ch.ge.ve.bo.service.operation.status.OperationStatusAndConsistencyService;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import ch.ge.ve.bo.service.testing.card.TestingCardRegisterGeneratorService;
import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo;
import ch.ge.ve.chvote.pactback.contract.operation.BallotDocumentationVo;
import ch.ge.ve.chvote.pactback.contract.operation.ElectionSiteConfigurationVo;
import ch.ge.ve.chvote.pactback.contract.operation.HighlightedQuestionsVo;
import ch.ge.ve.chvote.pactback.contract.operation.OperationConfigurationSubmissionVo;
import ch.ge.ve.chvote.pactback.contract.operation.PrinterConfigurationVo;
import ch.ge.ve.chvote.pactback.contract.operation.TestSitePrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;

/**
 * {@link MultipartOperationUploadServiceHandler for configuration part}
 */
public class OperationUploadServiceConfigurationHandler extends MultipartOperationUploadServiceHandler {


  private final OperationStatusAndConsistencyService operationStatusAndConsistencyService;
  private       PactConfiguration                    pactConfiguration;
  private final SecuredOperationRepository           operationRepository;
  private final FileService                          fileService;
  private final PrinterTemplateService               printerTemplateService;
  private final ObjectMapper                         objectMapper;
  private final TestingCardRegisterGeneratorService  testingCardRegisterGeneratorService;
  private final BallotDocumentationService           ballotDocumentationService;
  private final ElectionPagePropertiesService        electionPagePropertiesService;
  private final HighlightedQuestionService           highlightedQuestionService;
  private final DictionaryService                    dictionaryService;


  /**
   * Default constructor.
   */
  @SuppressWarnings("squid:S00107")
  public OperationUploadServiceConfigurationHandler(
      PactConfiguration pactConfiguration,
      SecuredOperationRepository operationRepository,
      FileService fileService,
      PrinterTemplateService printerTemplateService,
      ObjectMapper objectMapper,
      HttpClient httpClient,
      TestingCardRegisterGeneratorService testingCardRegisterGeneratorService,
      OperationStatusAndConsistencyService operationStatusAndConsistencyService,
      Clock clock,
      BallotDocumentationService ballotDocumentationService,
      ElectionPagePropertiesService electionPagePropertiesService,
      HighlightedQuestionService highlightedQuestionService,
      DictionaryService dictionaryService
  ) {
    super(clock, pactConfiguration, httpClient);
    this.operationStatusAndConsistencyService = operationStatusAndConsistencyService;
    this.pactConfiguration = pactConfiguration;
    this.operationRepository = operationRepository;
    this.fileService = fileService;
    this.printerTemplateService = printerTemplateService;
    this.objectMapper = objectMapper;
    this.testingCardRegisterGeneratorService = testingCardRegisterGeneratorService;
    this.ballotDocumentationService = ballotDocumentationService;
    this.electionPagePropertiesService = electionPagePropertiesService;
    this.highlightedQuestionService = highlightedQuestionService;

    this.dictionaryService = dictionaryService;
  }

  @Override
  public void upload(long operationId) throws PactRequestException {
    if (!operationStatusAndConsistencyService.getOperationStatusAndConsistency(operationId)
                                             .isConfigurationCompleteAndConsistent()) {
      throw new TryToUploadUncompleteOrInconsitentOperation();
    }

    MultipartEntityBuilder reqEntityBuilder = MultipartEntityBuilder.create();
    OperationConfigurationSubmissionVo submission = getOperationConfiguration(operationId);

    List<AttachmentFileEntryVo> attachments = new LinkedList<>();
    attachments.addAll(attachHighlightedQuestions(submission, operationId, reqEntityBuilder));
    attachments.addAll(attachBallotDocumentations(submission, operationId, reqEntityBuilder));
    attachElectionSiteConfiguration(submission, operationId);

    try {

      Map<VoterTestingCardsLot, Map<String, String>> testingCardXmls =
          testingCardRegisterGeneratorService.generateForOperation(operationId, true);
      attachments
          .addAll(attachRegisterForTestingCards(submission.getOperationLabel(), testingCardXmls, reqEntityBuilder));
      attachments.addAll(attachConfigurationFiles(reqEntityBuilder, operationId));
      attachments.add(attachTranslations(reqEntityBuilder, operationId));
      submission.setAttachments(attachments);

      StringBody operation = new StringBody(objectMapper.writeValueAsString(submission), APPLICATION_JSON);
      reqEntityBuilder.addPart("operation", operation);

    } catch (IOException e) {
      throw new TechnicalException(e);
    }

    doMultipartPost(pactConfiguration.getUrl().getOperationConfigurationUpload(), operationId,
                    reqEntityBuilder.build());
  }

  private AttachmentFileEntryVo attachTranslations(MultipartEntityBuilder reqEntityBuilder, Long operationId)
      throws IOException {
    List<String> translationsAsString =
        dictionaryService.get(DictionaryDataType.VOTE_RECEIVER_DEFAULT_TRANSLATIONS, String.class, operationId);

    if (translationsAsString.size() != 1) {
      throw new TechnicalException("There should be one and only one default translation configuration by realm");
    }

    return attachFile(reqEntityBuilder, "translations.zip", "translation.txt",
                      translationsAsString.get(0).getBytes(StandardCharsets.UTF_8),
                      null, LocalDateTime.now(clock), AttachmentFileEntryVo.AttachmentType.TRANSLATIONS,
                      null);
  }

  private List<AttachmentFileEntryVo> attachConfigurationFiles(MultipartEntityBuilder reqEntityBuilder, long
      operationId) throws IOException {
    List<AttachmentFileEntryVo> attachments = new ArrayList<>();
    List<FileVo> files = fileService.getFiles(operationId, true, EnumSet.of(
        DOMAIN_OF_INFLUENCE, DOCUMENT_FAQ, VOTATION_REPOSITORY, ELECTION_REPOSITORY, DOCUMENT_CERTIFICATE,
        DOCUMENT_TERMS));
    files = files.stream().sorted(Comparator.comparingLong(value -> value.id)).collect(Collectors.toList());

    for (int i = 0; i < files.size(); i++) {
      FileVo fileVo = files.get(i);
      attachments.add(attachFile(reqEntityBuilder, "file-" + i + ".zip", fileVo.fileName,
                                 fileVo.fileContent, fileVo.language, fileVo.saveDate,
                                 getAttachmentType(fileVo.type), fileVo.id));
    }
    return attachments;
  }

  private AttachmentFileEntryVo attachFile(
      MultipartEntityBuilder reqEntityBuilder, String zipFilename, String fileName, byte[] fileContent,
      Language language, LocalDateTime saveDate, AttachmentFileEntryVo.AttachmentType attachmentType,
      Long externalIdentifier)
      throws IOException {
    try (
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(bos)) {
      zipOutputStream.putNextEntry(new ZipEntry(fileName));
      zipOutputStream.write(fileContent);
      zipOutputStream.closeEntry();
      AttachmentFileEntryVo attachment = new AttachmentFileEntryVo();
      attachment.setAttachmentType(attachmentType);
      if (language != null) {
        attachment.setLanguage(language.name());
      }
      attachment.setImportDateTime(saveDate);
      attachment.setZipFileName(zipFilename);
      attachment.setExternalIdentifier(externalIdentifier);
      reqEntityBuilder.addPart("data", new ByteArrayBody(bos.toByteArray(), APPLICATION_OCTET_STREAM, zipFilename));
      return attachment;
    }
  }

  private AttachmentFileEntryVo.AttachmentType getAttachmentType(FileType type) {
    switch (type) {
      case VOTATION_REPOSITORY:
        return AttachmentFileEntryVo.AttachmentType.REPOSITORY_VOTATION;
      case ELECTION_REPOSITORY:
        return AttachmentFileEntryVo.AttachmentType.REPOSITORY_ELECTION;
      case DOCUMENT_CERTIFICATE:
        return AttachmentFileEntryVo.AttachmentType.DOCUMENT_CERTIFICATE;
      case DOCUMENT_FAQ:
        return AttachmentFileEntryVo.AttachmentType.DOCUMENT_FAQ;
      case DOCUMENT_TERMS:
        return AttachmentFileEntryVo.AttachmentType.DOCUMENT_TERMS;
      case DOMAIN_OF_INFLUENCE:
        return AttachmentFileEntryVo.AttachmentType.DOMAIN_OF_INFLUENCE;
      case REGISTER:
        return AttachmentFileEntryVo.AttachmentType.REGISTER;
    }
    throw new TechnicalException("cannot convert a file of type " + type + " to Pact types");
  }


  private TestSitePrinter getTestSitePrinter() {
    TestSitePrinter testSitePrinter = new TestSitePrinter();
    testSitePrinter.setMunicipality(createVirtualMunicipality(printerTemplateService.getVirtualPrinterForTestCard()));
    testSitePrinter.setPrinter(getVirtualPrinterForTestCard());
    return testSitePrinter;
  }


  private PrinterConfigurationVo getVirtualPrinterForTestCard() {
    PrinterConfigurationVo printerConfigurationVo = new PrinterConfigurationVo();
    printerConfigurationVo.setPublicKey(printerTemplateService.getVirtualPrinterForTestCard().publicKey);
    printerConfigurationVo.setName(printerTemplateService.getVirtualPrinterForTestCard().name);
    printerConfigurationVo.setId(printerTemplateService.getVirtualPrinterForTestCard().id);
    return printerConfigurationVo;
  }

  private OperationConfigurationSubmissionVo getOperationConfiguration(long operationId) {
    Operation operation = operationRepository.findOne(operationId, true);
    OperationConfigurationSubmissionVo submission = new OperationConfigurationSubmissionVo();
    submission.setUser(ConnectedUser.get().login);
    submission.setCanton(ConnectedUser.get().realm);
    submission.setGroupVotation(dictionaryService.getOne(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, Boolean.class, operationId));
    submission.setClientId(String.valueOf(operation.getId()));
    submission.setGracePeriod(operation.getGracePeriod());
    submission.setOperationName(operation.getLongLabel());
    submission.setOperationLabel(operation.getShortLabel());
    submission.setOperationDate(operation.getDate());

    Map<OperationConfigurationSubmissionVo.Milestone, LocalDateTime> milestones = operation
        .getMilestones()
        .stream()
        .filter(milestone -> milestone.getDate() != null)
        .collect(Collectors.toMap(
            m -> OperationConfigurationSubmissionVo.Milestone.valueOf(m.getType().name()),
            Milestone::getDate));
    submission.setMilestones(new TreeMap<>(milestones));
    submission.setTestSitePrinter(getTestSitePrinter());
    return submission;
  }

  private void attachElectionSiteConfiguration(OperationConfigurationSubmissionVo submission, Long operationId) {
    submission.setElectionSiteConfigurations(
        electionPagePropertiesService
            .getBallotToModelMapping(operationId).entrySet()
            .stream()
            .map(m -> {
              ElectionSiteConfigurationVo conf = new ElectionSiteConfigurationVo();
              conf.setBallot(m.getKey());
              conf.setAllowChangeOfElectoralList(m.getValue().isAllowChangeOfElectoralList());
              conf.setAllowMultipleMandates(m.getValue().isAllowMultipleMandates());
              conf.setCandidateInformationDisplayModel(m.getValue().getCandidateInformationDisplayModel());
              conf.setAllowOpenCandidature(m.getValue().isAllowOpenCandidature());
              conf.setColumnsOrderOnVerificationTable(Arrays.asList(m.getValue().getColumnsOrderOnVerificationTable()));
              conf.setDisplayedColumnsOnVerificationTable(
                  Arrays.asList(m.getValue().getDisplayedColumnsOnVerificationTable()));
              conf.setDisplayCandidatePositionOnAModifiedBallotPaper(
                  m.getValue().isDisplayCandidatePositionOnAModifiedBallotPaper());
              conf.setDisplayCandidateSearchForm(m.getValue().isDisplayCandidateSearchForm());
              conf.setDisplaySuffrageCount(m.getValue().isDisplaySuffrageCount());
              conf.setDisplayVoidOnEmptyBallotPaper(m.getValue().isDisplayVoidOnEmptyBallotPaper());
              conf.setDisplayListVerificationCode(m.getValue().isDisplayListVerificationCode());
              conf.setDisplayEmptyPosition(m.getValue().isDisplayEmptyPosition());
              conf.setDisplayCandidatePositionOnACompactBallotPaper(
                  m.getValue().isDisplayCandidatePositionOnACompactBallotPaper());
              return conf;
            }).collect(Collectors.toCollection(LinkedHashSet::new)));
  }

  private List<AttachmentFileEntryVo> attachHighlightedQuestions(OperationConfigurationSubmissionVo submission,
                                                                 Long operationId,
                                                                 MultipartEntityBuilder reqEntityBuilder) {
    Set<HighlightedQuestionsVo> highlightedQuestions = new LinkedHashSet<>();
    submission.setHighlightedQuestions(highlightedQuestions);
    return highlightedQuestionService
        .findByOperation(operationId)
        .stream()
        .flatMap(hq -> hq.getLocalizedQuestions().stream())
        .map(question -> {
          try {
            AttachmentFileEntryVo attachment =
                attachFile(reqEntityBuilder, "highlighted-questions-" + question.getId() + ".zip",
                           question.getFileName(),
                           highlightedQuestionService.getFileContent(question.getId()).getContent(),
                           question.getLanguage(), question.getDate(),
                           AttachmentFileEntryVo.AttachmentType.HIGHLIGHTED_QUESTION,
                           question.getId());
            HighlightedQuestionsVo highlightedQuestionsVo = new HighlightedQuestionsVo();
            highlightedQuestionsVo.setAnswerFile(attachment);
            highlightedQuestionsVo.setQuestion(question.getLocalizedQuestion());
            highlightedQuestions.add(highlightedQuestionsVo);
            return attachment;
          } catch (IOException e) {
            throw new TechnicalException(e);
          }
        })
        .collect(Collectors.toList());

  }

  private List<AttachmentFileEntryVo> attachBallotDocumentations(OperationConfigurationSubmissionVo submission,
                                                                 Long operationId,
                                                                 MultipartEntityBuilder reqEntityBuilder) {
    Set<BallotDocumentationVo> ballotDocumentations = new LinkedHashSet<>();
    submission.setBallotDocumentations(ballotDocumentations);
    return ballotDocumentationService
        .findByOperation(operationId)
        .stream()
        .map(bd -> {
          try {
            AttachmentFileEntryVo attachment =
                attachFile(reqEntityBuilder, "ballot-documentation-" + bd.getId() + ".zip", bd.getFileName(),
                           ballotDocumentationService.getFileContent(bd.getId()).getContent(), bd.getLanguage(),
                           bd.getSaveDate(),
                           AttachmentFileEntryVo.AttachmentType.BALLOT_DOCUMENTATION,
                           bd.getId());
            BallotDocumentationVo ballotDocumentationVo = new BallotDocumentationVo();
            ballotDocumentationVo.setBallot(bd.getBallotId());
            ballotDocumentationVo.setDocumentation(attachment);
            ballotDocumentationVo.setLabel(bd.getLocalizedLabel());
            ballotDocumentations.add(ballotDocumentationVo);
            return attachment;
          } catch (IOException e) {
            throw new TechnicalException(e);
          }
        })
        .collect(Collectors.toList());
  }

}
