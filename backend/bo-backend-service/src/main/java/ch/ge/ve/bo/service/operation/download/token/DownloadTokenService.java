/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.download.token;

import ch.ge.ve.bo.repository.entity.DownloadToken;
import ch.ge.ve.bo.repository.entity.Operation;

/**
 * create a download token for the connected user ans operation for a given path.
 * This token permits to bypass security filters and create genuine html link to download a file.
 * Download in such context is also stoppable and support error recovery
 */
public interface DownloadTokenService {

  /**
   * Create a token to download a file without being connected
   */
  DownloadToken getOrCreateDownloadToken(Operation operation, String path);


}
