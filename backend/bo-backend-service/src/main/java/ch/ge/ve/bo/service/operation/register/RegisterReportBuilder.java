/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.query.JsonQueryExecuterFactory;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

/**
 * Service responsible of creating report.
 */
public class RegisterReportBuilder {
  private final JasperReport mainReport;
  private final JasperReport residentReport;
  private final JasperReport swissAbroadReport;


  private interface ReportFormatter {
    byte[] format(JasperPrint print) throws JRException;
  }

  public enum SupportedFormat implements ReportFormatter {
    CSV {
      @Override
      public byte[] format(JasperPrint print) throws JRException {
        JRCsvExporter exporter = new JRCsvExporter();
        exporter.setExporterInput(new SimpleExporterInput(print));
        StringBuilder toReturn = new StringBuilder();
        exporter.setExporterOutput(new SimpleWriterExporterOutput(toReturn));
        exporter.exportReport();
        return toReturn.toString().getBytes(Charset.forName("UTF-8"));
      }
    },
    PDF {
      @Override
      public byte[] format(JasperPrint print) throws JRException {
        return JasperExportManager.exportReportToPdf(print);
      }
    }
  }


  /**
   * Default constructor
   */
  public RegisterReportBuilder() throws JRException {
    mainReport =
        (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream("/jasper/RegisterReport.jasper"));
    residentReport = (JasperReport) JRLoader
        .loadObject(this.getClass().getResourceAsStream("/jasper/RegisterReportResident.jasper"));
    swissAbroadReport = (JasperReport) JRLoader
        .loadObject(this.getClass().getResourceAsStream("/jasper/RegisterReportSwissAbroad.jasper"));

  }


  /**
   * generate the given report
   */
  public byte[] buildReport(String report, String operationName, SupportedFormat format) throws JRException {
    Map<String, Object> params = new HashMap<>();
    params.put(JsonQueryExecuterFactory.JSON_DATE_PATTERN, "'JSDate['dd.MM.yyyy hh:mm:ss.SSS']'");
    params.put(JRParameter.REPORT_RESOURCE_BUNDLE, resourceBundle(Locale.FRENCH)); // TODO enable other lang
    params.put("SubReportResident", residentReport);
    params.put("SubReportSwissAbroad", swissAbroadReport);
    params.put("operation", operationName);
    return format.format(JasperFillManager.fillReport(mainReport, params, new JsonDataSource(
        new ByteArrayInputStream(report.getBytes(Charset.forName("UTF-8")))

    )));
  }

  private ResourceBundle resourceBundle(Locale locale) {
    return ResourceBundle.getBundle("i18n.jasper.register", locale, this.getClass().getClassLoader());
  }


}
