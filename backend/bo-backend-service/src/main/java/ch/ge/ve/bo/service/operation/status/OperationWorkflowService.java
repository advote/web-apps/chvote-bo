/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import ch.ge.ve.bo.repository.entity.WorkflowStep;
import ch.ge.ve.bo.service.exception.BusinessException;

/**
 * Class responsible to affect workflow status of an operation
 */
public interface OperationWorkflowService {

  /**
   * stop modification mode
   */
  void suspendModificationMode(long operationId);

  /**
   * change {@link WorkflowStep} after upload to pact
   */
  void setWorkflowStep(long operationId, WorkflowStep step);

  /**
   * Start modification mode
   */
  void switchToModificationMode(Long operationId) throws BusinessException;

  /**
   * Mark test site as validated
   */
  void validateTestSite(long operationId) throws BusinessException;

  /**
   * Mark test site as invalidated
   */
  void invalidateTestSite(long operationId) throws BusinessException;

  /**
   * Mark created voting material as validated
   */
  void validateVotingMaterial(long operationId) throws BusinessException;

  /**
   * Force a close on voting period in simulation
   */
  void closeVotingPeriod(long operationId) throws BusinessException;
}
