/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.OperationParameterType;
import ch.ge.ve.bo.repository.security.ManagementEntityHolder;
import ch.ge.ve.bo.repository.security.OperationHolder;
import ch.ge.ve.bo.repository.security.SecuredContext;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.exception.TryToModifyReadonlyPropertyException;
import ch.ge.ve.bo.service.operation.model.OperationStatusVo;
import ch.ge.ve.bo.service.operation.status.OperationStatusService;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * This class permit to ensure that
 * - Updates are safe in regards of user rights including user's management entity
 * - Configuration and voting material date is updated correctly
 * - Consistency check is done
 */
public class SecuredOperationHolderServiceImpl implements SecuredOperationHolderService {

  private static final Map<OperationParameterType, Function<OperationStatusVo, Boolean>> readOnlyByStatus;

  static {
    readOnlyByStatus = new EnumMap<>(OperationParameterType.class);
    readOnlyByStatus.put(OperationParameterType.CONFIGURATION, status -> status.getConfigurationStatus().getState().isReadOnly());
    readOnlyByStatus.put(OperationParameterType.VOTING_MATERIAL,
                         status -> status.getVotingMaterialStatus() == null || status.getVotingMaterialStatus().isReadOnly());
    readOnlyByStatus.put(OperationParameterType.VOTING_PERIOD,
                         status -> status.getVotingPeriodStatus() == null || status.getVotingPeriodStatus().isReadOnly());
  }


  private final ConsistencyScheduleService consistencyScheduleService;
  private final SecuredOperationRepository operationRepository;
  private final SecuredContext             securedContext;
  private final OperationStatusService     statusService;


  /**
   * Default constructor
   */
  public SecuredOperationHolderServiceImpl(ConsistencyScheduleService consistencyScheduleService,
                                           SecuredOperationRepository operationRepository,
                                           SecuredContext securedContext,
                                           OperationStatusService operationStatusService) {
    this.consistencyScheduleService = consistencyScheduleService;
    this.operationRepository = operationRepository;
    this.securedContext = securedContext;
    this.statusService = operationStatusService;
  }


  @Override
  public <U extends OperationHolder, T extends Collection<U>> T safeReadAll(Supplier<T> operationHolderSupplier) {
    if (securedContext.isInSecureContext()) {
      return operationHolderSupplier.get();
    }

    return securedContext.doInSecureContext(
        () -> {
          T operationHolders = operationHolderSupplier.get();
          operationHolders.forEach(
              operationHolder -> operationRepository.findOne(operationHolder.getOperation().getId(), false)
          );
          return operationHolders;
        }
    );

  }


  @Override
  public <T extends OperationHolder> Optional<T> safeRead(Supplier<Optional<T>> operationHolderSupplier) {
    if (securedContext.isInSecureContext()) {
      return operationHolderSupplier.get();
    }

    return securedContext.doInSecureContext(() -> {
      Optional<T> optionalOperationHolder = operationHolderSupplier.get();

      // Search costs nothing since it is already in hibernate's L2 cache
      optionalOperationHolder.ifPresent(
          operationHolder -> operationRepository.findOne(operationHolder.getOperation().getId(), false));
      return optionalOperationHolder;
    });
  }

  @Override
  public <U extends OperationHolder> void doUpdateAndForget(
      Supplier<Optional<U>> operationHolderSupplier, Consumer<U> updater) {
    securedContext.doInSecureContext(() -> {
      doUpdate(operationHolderSupplier, value -> {
        updater.accept(value);
        return Optional.empty();
      });
      return Optional.empty();
    });
  }

  @Override
  public <U extends OperationHolder> void doUpdateOperationHolderAndForget(U operationHolder, Consumer<U> updater) {
    doNotOptionalUpdateAndForget(() -> operationHolder, updater);
  }


  @Override
  public <U extends OperationHolder> void doNotOptionalUpdateAndForget(Supplier<U> operationHolder, Consumer<U> updater) {
    doUpdateAndForget(() -> Optional.ofNullable(operationHolder.get()), updater);
  }

  @Override
  public <U extends OperationHolder, T> Optional<T> doTechnicalUpdate(Supplier<Optional<U>> operationHolderSupplier,
                                                                      Function<U, T> updater) {
    return securedContext.doInSecureContext(() -> safeRead(operationHolderSupplier).map(updater));
  }

  @Override
  public <U extends OperationHolder, T> T doTechnicalNotOptionalUpdate(Supplier<U> operationHolderSupplier,
                                                                       Function<U, T> updater) {
    return doTechnicalUpdate(() -> Optional.ofNullable(operationHolderSupplier.get()), updater)
        .orElse(null);
  }

  @Override
  public <U extends OperationHolder> void doTechnicalUpdateAndForget(Supplier<Optional<U>> operationHolderSupplier,
                                                                     Consumer<U> updater) {
    doTechnicalUpdate(operationHolderSupplier, operationHolder -> {
      updater.accept(operationHolder);
      return Optional.empty();
    });
  }

  @Override
  public <U extends OperationHolder> void doTechnicalNotOptionalUpdateAndForget(Supplier<U> operationHolderSupplier,
                                                                                Consumer<U> updater) {
    doTechnicalUpdateAndForget(() -> Optional.ofNullable(operationHolderSupplier.get()), updater);
  }

  @Override
  public <U extends OperationHolder, T> Optional<T> doUpdate(Supplier<Optional<U>> operationHolderSupplier,
                                                             Function<U, T> updater) {
    return securedContext.doInSecureContext(
        () -> operationHolderSupplier
            .get()
            .map(operationHolder -> {
                   Long operationId = operationHolder.getOperation().getId();
                   checkForManagementEntity(operationHolder, operationId);
                   insureStatusAllowModification(operationHolder);
                   T result = updater.apply(operationHolder);
                   markForConsistencyChecks(operationHolder, operationId);
                   return result;
                 }
            ));
  }

  @Override
  public <U extends OperationHolder, T> T doUpdateOperationHolder(U operationHolder, Function<U, T> updater) {
    return doNotOptionalUpdate(() -> operationHolder, updater);
  }

  private <U extends OperationHolder> void markForConsistencyChecks(U operationHolder, Long operationId) {
    if (operationHolder.getOperationParameterType() == OperationParameterType.CONFIGURATION) {
      operationRepository
          .markConfigurationAsUpdated(operationId, consistencyScheduleService.asCallback(operationId));
    }

    if (operationHolder.getOperationParameterType() == OperationParameterType.VOTING_MATERIAL) {
      operationRepository
          .markVotingMaterialAsUpdated(operationId, consistencyScheduleService.asCallback(operationId));
    }

    if (operationHolder.getOperationParameterType() == OperationParameterType.VOTING_PERIOD) {
      operationRepository.markVotingPeriodConfigAsUpdated(operationId);
    }
  }

  private <U extends OperationHolder> void checkForManagementEntity(U operationHolder, Long operationId) {
    if (operationHolder instanceof ManagementEntityHolder && ((ManagementEntityHolder) operationHolder)
        .getManagementEntity().equals(ConnectedUser.get().managementEntity)) {
      operationRepository.findOne(operationId, false);
    } else {
      operationRepository.findOne(operationId, operationHolder.onlyManagementEntityInChargeCanCreate());
    }
  }

  @Override
  public <U extends OperationHolder, T> T doNotOptionalUpdate(Supplier<U> operationHolder, Function<U, T> updater) {
    return doUpdate(() -> Optional.ofNullable(operationHolder.get()), updater).orElse(null);
  }

  private <U extends OperationHolder> void insureStatusAllowModification(U operationHolder) {
    try {
      Long operationId = operationHolder.getOperation().getId();
      if (readOnlyByStatus.getOrDefault(operationHolder.getOperationParameterType(), statusVo -> {
        throw new TechnicalException("Need implementation for this type of parameters");
      }).apply(statusService.getStatus(operationId))) {
        throw new TryToModifyReadonlyPropertyException();
      }
    } catch (PactRequestException e) {
      throw new TechnicalException(e);
    }
  }

}
