/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency;

import ch.ge.ve.bo.repository.OperationConsistencyRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.OperationConsistency;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

/**
 * Default implementation of {@link ConsistencyService}
 */
public class ConsistencyServiceImpl implements ConsistencyService {

  private final SecuredOperationRepository     operationRepository;
  private final OperationConsistencyRepository operationConsistencyRepository;
  private final ObjectMapper                   objectMapper;
  /**
   * After this timeout (in millisecond) we consider that for any reason computation of consistency should already have
   * been done.
   * If it is not the case this process will do it by itself.
   * <p>
   * Even if it is not a blocker, it's preferable to set a different value for each instance.
   * Hence A random value is a reasonable choice
   */
  private final long                           shouldHaveAlreadyBeenComputeTimeout;
  private final ConsistencyScheduleService     consistencyScheduleService;
  private final Clock                          clock;


  /**
   * Default constructor
   */
  public ConsistencyServiceImpl(SecuredOperationRepository operationRepository,
                                OperationConsistencyRepository operationConsistencyRepository,
                                ObjectMapper objectMapper,
                                long shouldHaveAlreadyBeenComputeTimeout,
                                ConsistencyScheduleService consistencyScheduleService,
                                Clock clock) {
    this.operationRepository = operationRepository;
    this.operationConsistencyRepository = operationConsistencyRepository;
    this.objectMapper = objectMapper;
    this.shouldHaveAlreadyBeenComputeTimeout = shouldHaveAlreadyBeenComputeTimeout;
    this.consistencyScheduleService = consistencyScheduleService;
    this.clock = clock;
  }

  @Override
  public ConsistencyResultVo getConsistencyResultIfAvailable(long operationId) {
    OperationConsistency operationConsistency = operationConsistencyRepository
        .findByOperationId(operationId)
        .orElse(null);
    Operation operation = operationRepository.findOne(operationId, false);
    if (noConsistencyAvailableOrOutdated(operationConsistency, operation)) {
      if (shouldTakeInCharge(operation)) {
        consistencyScheduleService.scheduleCheck(operationId);
      }
      return null;
    }
    try {
      return objectMapper.readValue(operationConsistency.getReport(), ConsistencyResultVo.class);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }

  private boolean shouldTakeInCharge(Operation operation) {
    return Duration.between(getOperationLastUpdateDate(operation), LocalDateTime.now(clock))
                   .toMillis() > shouldHaveAlreadyBeenComputeTimeout;
  }

  private boolean noConsistencyAvailableOrOutdated(OperationConsistency operationConsistency, Operation operation) {
    return operationConsistency == null ||
           operationConsistency.getComputationDate()
                               .isBefore(getOperationLastUpdateDate(operation));
  }

  /**
   * Return last update date of an operation in the context of consistency check
   * It's actually the last date between last configuration change and las voting material change
   */
  static LocalDateTime getOperationLastUpdateDate(Operation operation) {
    return Collections.max(
        Arrays.asList(operation.getLastConfigurationUpdateDate(),
                      operation.getLastVotingMaterialUpdateDate()));
  }


}
