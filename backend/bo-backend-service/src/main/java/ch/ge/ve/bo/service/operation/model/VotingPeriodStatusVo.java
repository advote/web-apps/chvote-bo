/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * Status concerning voting period initialization workflow
 */
public class VotingPeriodStatusVo {

  public enum State {
    INCOMPLETE(false),
    COMPLETE(false),
    AVAILABLE_FOR_INITIALIZATION(true),
    INITIALIZATION_REQUESTED(true),
    INITIALIZATION_REJECTED(false),
    INITIALIZATION_IN_PROGRESS(true),
    INITIALIZATION_FAILED(false),
    INITIALIZED(true),
    CLOSED(true);

    private final boolean readOnly;

    State(boolean readOnly) {
      this.readOnly = readOnly;
    }
  }

  public final VotingPeriodStatusVo.State state;

  @JsonIgnore
  public final String        comment;
  @JsonIgnore
  public final LocalDateTime stateDate;
  @JsonIgnore
  public final String        stateUser;

  public final String pactUrl;

  public final Map<String, Boolean> completedSections;

  /**
   * Default constuctor
   */
  public VotingPeriodStatusVo(State state, Map<String, Boolean> completedSections, LocalDateTime stateDate,
                              String stateUser, String pactUrl, String comment) {
    this.state = state;
    this.comment = comment;
    this.stateDate = stateDate;
    this.stateUser = stateUser;
    this.pactUrl = pactUrl;
    this.completedSections = completedSections;
  }


  /**
   * Used to create a VotingPeriodStatusVo when the voting period configuration hasn't been submitted to PACT
   */
  public static VotingPeriodStatusVo notSent(Map<String, Boolean> completedSections, LocalDateTime saveDate, String
      login) {
    boolean isComplete = completedSections.values().stream().allMatch(completed -> completed);
    return new VotingPeriodStatusVo(isComplete ? State.COMPLETE : State.INCOMPLETE, completedSections, saveDate, login,
                                    null, null);
  }


  public boolean isReadOnly() {
    return state.readOnly;
  }

}
