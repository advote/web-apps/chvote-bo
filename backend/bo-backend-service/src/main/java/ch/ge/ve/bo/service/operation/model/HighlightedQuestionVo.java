/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.bo.repository.entity.HighlightedQuestion;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Value object for {@link HighlightedQuestion}
 */
public class HighlightedQuestionVo {

  private final Long                                 id;
  private final List<LocalizedHighlightedQuestionVo> localizedQuestions;

  /**
   * Default constructor
   */
  @JsonCreator
  public HighlightedQuestionVo(
      @JsonProperty("id") Long id,
      @JsonProperty("localizedQuestions") List<LocalizedHighlightedQuestionVo> localizedQuestions) {
    this.id = id;
    this.localizedQuestions = localizedQuestions;
  }

  /**
   * Default constructor using entity
   */
  public HighlightedQuestionVo(HighlightedQuestion highlightedQuestion) {
    this(highlightedQuestion.getId(),
         highlightedQuestion.getLocalizedQuestions()
                            .stream()
                            .map(LocalizedHighlightedQuestionVo::new)
                            .collect(Collectors.toList()));
  }


  public Long getId() {
    return id;
  }

  public List<LocalizedHighlightedQuestionVo> getLocalizedQuestions() {
    return localizedQuestions;
  }
}
