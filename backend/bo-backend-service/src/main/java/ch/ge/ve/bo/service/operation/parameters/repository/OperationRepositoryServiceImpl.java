/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository;


import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.exception.InvalidFileException;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceService;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceFileVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.RepositoryFileVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.RepositoryImportResult;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the {@link OperationRepositoryService} interface.
 */
public class OperationRepositoryServiceImpl implements OperationRepositoryService {

  public static final String UNKNOWN_DOI_TYPE = "Unknown";

  private final SecuredOperationRepository      operationRepository;
  private final OperationDomainInfluenceService operationDomainInfluenceService;
  private final FileService                     fileService;

  private final OperationRepositoryServiceVotationHandler votationHandler;
  private final OperationRepositoryServiceElectionHandler electionHandler;

  /**
   * Default constructor
   */
  public OperationRepositoryServiceImpl(SecuredOperationRepository operationRepository,
                                        OperationDomainInfluenceService operationDomainInfluenceService,
                                        FileService fileService,
                                        OperationRepositoryServiceVotationHandler votationHandler,
                                        OperationRepositoryServiceElectionHandler electionHandler) {
    this.operationRepository = operationRepository;
    this.operationDomainInfluenceService = operationDomainInfluenceService;
    this.fileService = fileService;
    this.votationHandler = votationHandler;
    this.electionHandler = electionHandler;

  }


  @Override
  @Transactional(rollbackFor = Exception.class)
  public RepositoryImportResult importFile(long operationId, String fileName, InputStream inputStream)
      throws BusinessException {
    // retrieve operation
    Operation operation = operationRepository.findOne(operationId, false);
    byte[] fileContent = getFileContent(inputStream);
    return getHandler(getFileType(fileName, fileContent)).importFile(fileName, fileContent, operation);
  }

  private OperationRepositoryServiceHandler getHandler(FileType fileType) {
    return (fileType == FileType.VOTATION_REPOSITORY) ? votationHandler : electionHandler;
  }


  private FileType getFileType(String fileName, byte[] fileContent)
      throws InvalidFileException {
    // check that the file is an XML file
    if (!fileName.toLowerCase(Locale.ENGLISH).endsWith(FileVo.XML_EXTENSION)) {
      throw new InvalidFileException("global.errors.not-xml-file");
    }


    String contentAsString = new String(fileContent, Charset.forName("UTF-8"));

    if (contentAsString.contains("http://www.ech.ch/xmlns/eCH-0159/4")) {
      return FileType.VOTATION_REPOSITORY;
    }

    if (contentAsString.contains("http://www.ech.ch/xmlns/eCH-0157/4")) {
      return FileType.ELECTION_REPOSITORY;
    }

    throw new InvalidFileException("global.errors.not-expected-xml-file", "[eCH-0159, eCH-0157]");
  }

  private byte[] getFileContent(InputStream inputStream) {
    byte[] fileContent;
    try {
      fileContent = IOUtils.toByteArray(inputStream);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
    return fileContent;
  }

  @Override
  @Transactional(readOnly = true)
  public List<RepositoryFileVo> getFiles(long operationId) {
    return fileService
        .getFiles(operationId, true, EnumSet.of(FileType.ELECTION_REPOSITORY, FileType.VOTATION_REPOSITORY))
        .stream()
        .map(applicationFileVo -> buildFrom(applicationFileVo, false))
        .collect(Collectors.toList());
  }

  @Override
  @Transactional(readOnly = true)
  public RepositoryFileVo getFile(long fileId) {
    return buildFrom(fileService.getFile(fileId), true);
  }

  private RepositoryFileVo buildFrom(FileVo file, boolean withContent) {
    return getHandler(file.type).buildFrom(file, withContent);
  }

  @Override
  @Transactional(readOnly = true)
  @SuppressWarnings("unchecked")
  public Set<DomainOfInfluenceVo> getAllDomainOfInfluenceForOperation(long operationId) {

    DomainOfInfluenceFileVo doiFile = operationDomainInfluenceService.getFile(operationId, false);
    Map<String, DomainOfInfluenceVo> knownDois = doiFile == null ? new HashMap<>() :
        doiFile.details.stream().collect(Collectors.toMap(doi -> doi.id, doi -> doi, (doi1, doi2) -> doi1));

    return getFiles(operationId)
        .stream()
        .flatMap(file -> ((Set<String>) getHandler(file.type).getAllDoiIdentification(file)).stream())
        .map(doiId -> knownDois.computeIfAbsent(doiId, this::createUnknownDoi))
        .collect(Collectors.toSet());
  }


  @Override
  @Transactional(readOnly = true)
  @SuppressWarnings("unchecked")
  public Set<String> getAllBallotsForOperation(long operationId, boolean includeElection, boolean includeVotation) {
    return getFiles(operationId)
        .stream()
        .filter(file -> includeElection && file.type == FileType.ELECTION_REPOSITORY ||
                        includeVotation && file.type == FileType.VOTATION_REPOSITORY)
        .flatMap(file -> ((Set<String>) getHandler(file.type).getAllBallots(file)).stream())
        .collect(Collectors.toCollection(TreeSet::new));
  }

  @Override
  public RepositoryImportResult replaceFile(long operationId, long oldId, String fileName, InputStream inputStream)
      throws BusinessException {
    Operation operation = operationRepository.findOne(operationId, false);
    byte[] fileContent = getFileContent(inputStream);
    return getHandler(getFileType(fileName, fileContent)).replaceFile(fileName, oldId, fileContent, operation);
  }

  private DomainOfInfluenceVo createUnknownDoi(String id) {
    return new DomainOfInfluenceVo(UNKNOWN_DOI_TYPE, id, "Unknown id=" + id, id);
  }


}
