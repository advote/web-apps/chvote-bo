/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.access;

import ch.ge.ve.bo.repository.ConnectedUser;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.util.StopWatch;

/**
 * Aspect logging all access to the service layer.
 */
@Order(1)
@Aspect
public class ServiceAccessLoggerAspect {
  private static final Logger logger       = LoggerFactory.getLogger(ServiceAccessLoggerAspect.class);
  private static final Logger accessLogger = LoggerFactory.getLogger("bo-event");

  /**
   * Default constructor
   */
  public ServiceAccessLoggerAspect() {
    logger.info("Logging all access to the service layer...");
  }

  /**
   * Pointcut definition for all method of service implementation
   */
  @Pointcut("execution(public * ch.ge.ve.bo.service..*ServiceImpl.*(..))")
  public void serviceLayer() {
    // empty implementation, the important part is handled by the annotation
  }

  /**
   * default logger for service access
   */
  @Around("serviceLayer()")
  public Object logAccessService(ProceedingJoinPoint joinPoint)
      throws Throwable { // NO SONAR ('throws Throwable' is required in the signature
    Signature signature = joinPoint.getSignature();
    StopWatch stopwatch = new StopWatch();
    stopwatch.start();

    Object result;
    try {
      result = joinPoint.proceed();
    } finally {
      stopwatch.stop();

      String serviceName = signature.getDeclaringType().getSimpleName();
      String methodName = signature.getName();
      String userName = ConnectedUser.get() != null ? ConnectedUser.get().login : "UNKNOWN";
      accessLogger.info("{} - {}.{} {}ms", userName, serviceName, methodName, stopwatch.getTotalTimeMillis());
    }

    return result;
  }
}
