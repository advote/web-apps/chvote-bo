/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.ElectionInformationVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.ElectionRepositoryFileVo;
import ch.ge.ve.bo.service.utils.xml.XSDValidator;
import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ContestType;
import ch.ge.ve.interfaces.ech.eCH0157.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0157.v4.EventInitialDelivery;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * OperationRepositoryServiceHandler specific to an election
 */
public class OperationRepositoryServiceElectionHandler extends OperationRepositoryServiceHandler<Delivery,
    ElectionRepositoryFileVo> {

  /**
   * Default constructor
   */
  public OperationRepositoryServiceElectionHandler(XSDValidator xsdValidator, Function<InputStream, Delivery>
      deliveryBuilder, FileService fileService) {
    super(xsdValidator, deliveryBuilder, fileService);
  }

  @Override
  HeaderType getHeader(Delivery delivery) {
    return delivery.getDeliveryHeader();
  }

  @Override
  ContestType getContest(Delivery delivery) {
    return delivery.getInitialDelivery().getContest();
  }

  @Override
  ElectionRepositoryFileVo buildFrom(Delivery delivery, FileVo file, boolean withContent) {
    return new ElectionRepositoryFileVo(
        file.id, file.login, file.saveDate, file.businessKey, file.operationId, file.type, file.language, file.fileName,
        file.managementEntity, withContent ? file.fileContent : null,
        getContest(delivery).getContestIdentification(),
        getContest(delivery).getContestDate().atStartOfDay(),
        getContest(delivery).getContestDescription().getContestDescriptionInfo().get(0).getContestDescription(),
        getElectionInfo(delivery)
    );
  }

  private List<ElectionInformationVo> getElectionInfo(Delivery delivery) {
    return delivery
        .getInitialDelivery()
        .getElectionGroupBallot()
        .stream()
        .flatMap(electionGroupBallot -> {
          String ballot = electionGroupBallot.getElectionGroupDescription()
                                             .getElectionDescriptionInfo().get(0)
                                             .getElectionDescription();


          return electionGroupBallot
              .getElectionInformation()
              .stream()
              .map(electionInformation -> createElectionInformationVo(
                  ballot, electionGroupBallot.getDomainOfInfluenceIdentification(), electionInformation));
        }).collect(Collectors.toList());
  }

  private ElectionInformationVo createElectionInformationVo(String ballot,
                                                            String doiId, EventInitialDelivery
                                                                .ElectionGroupBallot.ElectionInformation
                                                                electionInformation) {
    boolean isProportional = electionInformation.getElection().getTypeOfElection().equals(BigInteger.ONE);
    int numberOfMandates = electionInformation.getElection().getNumberOfMandates().intValue();
    String mandate = electionInformation.getElection()
                                        .getElectionDescription()
                                        .getElectionDescriptionInfo().get(0)
                                        .getElectionDescription();


    List<ElectionInformationVo.ElectionInformationListVo> lists = getElectionLists(electionInformation);
    if (isProportional) {
      return ElectionInformationVo.forProportional(doiId, ballot, numberOfMandates, mandate, lists);
    }
    if (lists.isEmpty()) {
      return ElectionInformationVo.forMajority(doiId, ballot, numberOfMandates, mandate,
                                               electionInformation.getCandidate().size());
    }
    return ElectionInformationVo.forMajorityWithList(doiId, ballot, numberOfMandates, mandate, lists);
  }

  private List<ElectionInformationVo.ElectionInformationListVo> getElectionLists(
      EventInitialDelivery.ElectionGroupBallot.ElectionInformation electionInformation) {
    Set<String> listsInAnUnion = electionInformation
        .getListUnion()
        .stream()
        .flatMap(unions -> unions.getReferencedList().stream())
        .collect(Collectors.toSet());
    return electionInformation
        .getList()
        .stream()
        .map(list -> new ElectionInformationVo.ElectionInformationListVo(list, listsInAnUnion
            .contains(list.getListIdentification())))
        .collect(Collectors.toList());
  }

  @Override
  ElectionRepositoryFileVo buildFrom(FileVo file, boolean withContent) {
    return buildFrom(fromXml(new ByteArrayInputStream(file.fileContent)), file, withContent);
  }

  @Override
  FileType getFileType() {
    return FileType.ELECTION_REPOSITORY;
  }

  @Override
  Set<String> getAllDoiIdentificationInternal(ElectionRepositoryFileVo file) {
    return file.getElectionInformationList()
               .stream()
               .map(ElectionInformationVo::getDoiId)
               .collect(Collectors.toSet());
  }

  @Override
  Set<String> getAllBallotsInternal(ElectionRepositoryFileVo file) {
    return file.getElectionInformationList()
               .stream()
               .map(ElectionInformationVo::getBallot)
               .collect(Collectors.toSet());
  }

}
