/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.dictionnary;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.DictionaryRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.Dictionary;
import ch.ge.ve.bo.repository.entity.DictionaryDataType;
import ch.ge.ve.bo.service.exception.TechnicalException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
/**
 * Default implementation of {@link DictionaryService}
 */
public class DictionaryServiceImpl implements DictionaryService {
  private final ObjectMapper               objectMapper;
  private final SecuredOperationRepository operationRepository;
  private final DictionaryRepository       repository;
  /**
   * Default constructor
   */
  public DictionaryServiceImpl(ObjectMapper objectMapper,
                               DictionaryRepository repository,
                               SecuredOperationRepository operationRepository) {
    this.objectMapper = objectMapper;
    this.operationRepository = operationRepository;
    this.repository = repository;
  }


  @Override
  public <T> List<T> get(DictionaryDataType dataType, Class<T> objectClass, String realm, String managementEntity) {
    return findAllByScope(dataType, realm, managementEntity)
        .stream()
        .map(dictionary -> mapTo(objectClass, dictionary.getContent()))
        .collect(Collectors.toList());
  }

  @Override
  public <T> List<T> get(DictionaryDataType dataType, Class<T> objectClass, Long operationId) {
    return get(dataType,
               objectClass,
               ConnectedUser.get().realm,
               operationRepository.findOne(operationId, false).getManagementEntity());
  }

  @Override
  public <T> T getOne(DictionaryDataType dataType, Class<T> objectClass, Long operationId) {
    return get(dataType,
               objectClass,
               ConnectedUser.get().realm,
               operationRepository.findOne(operationId, false).getManagementEntity())
        .get(0);
  }

  private List<Dictionary> findAllByScope(DictionaryDataType dataType, String realm, String managementEntity) {
    switch (dataType.getScopeType()) {
      case REALM:
        return repository.findAllByDataTypeAndScope(dataType, realm);
      case MANAGEMENT_ENTITY:
        return repository.findAllByDataTypeAndScope(dataType, managementEntity);
      default:
        return repository.findAllByDataType(dataType);
    }
  }

  private <T> T mapTo(Class<T> objectClass, String content) {
    if (objectClass == String.class) {
      //noinspection unchecked
      return (T) content;
    }


    try {
      return objectMapper.readValue(content, objectClass);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }
}
