/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.management.entity;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService;
import ch.ge.ve.bo.service.operation.exception.CannotRevokeManagementEntityException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link ManagementEntityService}
 */
public class ManagementEntityServiceImpl implements ManagementEntityService {

  private final Map<String, String[]> managementEntitiesByCanton;

  private final SecuredOperationRepository operationRepository;
  private final ConsistencyScheduleService consistencyScheduleService;
  private final FileService                fileService;

  /**
   * Default constructor
   */
  public ManagementEntityServiceImpl(ObjectMapper objectMapper,
                                     SecuredOperationRepository operationRepository,
                                     ConsistencyScheduleService consistencyScheduleService,
                                     FileService fileService) throws IOException {

    managementEntitiesByCanton =
        Stream.of(objectMapper.readValue(getClass().getResourceAsStream("/management-entities.json"),
                                         ManagementEntitiesByCanton[].class))
              .collect(Collectors.toMap(m -> m.canton, m -> {
                Arrays.sort(m.managementEntities);
                return m.managementEntities;
              }));

    this.operationRepository = operationRepository;
    this.consistencyScheduleService = consistencyScheduleService;
    this.fileService = fileService;
  }

  @Override
  public String[] getAllManagementEntities() {
    return managementEntitiesByCanton.get(ConnectedUser.get().realm);
  }

  @Override
  @Transactional
  public void invite(long operationId, Collection<String> managementEntities) {
    if (!Arrays.asList(managementEntitiesByCanton.get(ConnectedUser.get().realm)).containsAll(managementEntities)) {
      throw new TechnicalException(
          String.format("Some management entities from %s don't belongs to user's canton %s",
                        managementEntities,
                        ConnectedUser.get().realm));
    }

    Operation operation = operationRepository.findOne(operationId, true);
    checkUserManagementEntity(operation);

    operation.getGuestManagementEntities().addAll(managementEntities);
    operationRepository.save(operation);
    operationRepository.markConfigurationAsUpdated(operationId, consistencyScheduleService.asCallback(operationId));
  }

  @Override
  @Transactional
  public void revoke(long operationId, Collection<String> managementEntities)
      throws CannotRevokeManagementEntityException {
    Operation operation = operationRepository.findOne(operationId, true);
    checkUserManagementEntity(operation);
    checkOperationFiles(operationId, managementEntities);

    operation.getGuestManagementEntities().removeAll(managementEntities);
    operationRepository.save(operation);
    operationRepository.markConfigurationAsUpdated(operationId, consistencyScheduleService.asCallback(operationId));
  }

  private void checkUserManagementEntity(Operation operation) {
    if (!operation.getManagementEntity().equals(ConnectedUser.get().managementEntity)) {
      throw new TechnicalException(
          String.format(
              "Try to change guest management entity of an operation created by management entities %s by a user " +
              "belonging to %s",
              operation.getManagementEntity(),
              ConnectedUser.get().managementEntity));
    }
  }

  private void checkOperationFiles(long operationId, Collection<String> managementEntities)
      throws CannotRevokeManagementEntityException {
    List<String> managementEntitiesInError = fileService.getFiles(operationId, false,
                                                                  EnumSet.of(FileType.VOTATION_REPOSITORY,
                                                                             FileType.ELECTION_REPOSITORY,
                                                                             FileType.REGISTER))
                                                        .stream()
                                                        .map(file -> file.managementEntity)
                                                        .filter(managementEntities::contains)
                                                        .collect(Collectors.toList());
    if (!managementEntitiesInError.isEmpty()) {
      throw new CannotRevokeManagementEntityException(managementEntitiesInError);
    }
  }
}
