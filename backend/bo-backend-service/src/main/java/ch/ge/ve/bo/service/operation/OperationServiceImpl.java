/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation;

import static ch.ge.ve.bo.repository.entity.Operation.operation;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.MilestoneType;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.OperationConsistencyRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.Milestone;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.OperationConsistency;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.mapper.BeanMapper;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService;
import ch.ge.ve.bo.service.operation.exception.CannotTargetProduction;
import ch.ge.ve.bo.service.operation.exception.CannotTargetSimulation;
import ch.ge.ve.bo.service.operation.exception.InvalidMilestoneDatesException;
import ch.ge.ve.bo.service.operation.exception.LockedDateException;
import ch.ge.ve.bo.service.operation.exception.PastDateException;
import ch.ge.ve.bo.service.operation.exception.TryToModifyReadonlyPropertyException;
import ch.ge.ve.bo.service.operation.model.BaseConfiguration;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import ch.ge.ve.bo.service.operation.model.MilestoneConfiguration;
import ch.ge.ve.bo.service.operation.model.MilestoneVo;
import ch.ge.ve.bo.service.operation.model.OperationVo;
import ch.ge.ve.bo.service.operation.status.OperationStatusService;
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the {@link OperationService} interface.
 */
public class OperationServiceImpl implements OperationService {

  private       SecuredOperationRepository     operationRepository;
  private       OperationConsistencyRepository operationConsistencyRepository;
  private final VoterTestingCardsLotService    voterTestingCardsLotService;
  private       FileService                    fileService;
  private       OperationStatusService         statusService;
  private final ConsistencyScheduleService     consistencyScheduleService;
  private final ObjectMapper                   objectMapper;

  /**
   * Default constructor.
   *
   * @param operationRepository operation's repository service
   * @param fileService         the application file service
   */
  public OperationServiceImpl(
      SecuredOperationRepository operationRepository,
      OperationConsistencyRepository operationConsistencyRepository,
      VoterTestingCardsLotService voterTestingCardsLotService,
      FileService fileService,
      OperationStatusService statusService,
      ConsistencyScheduleService consistencyScheduleService,
      ObjectMapper objectMapper) {

    this.operationRepository = operationRepository;
    this.operationConsistencyRepository = operationConsistencyRepository;
    this.voterTestingCardsLotService = voterTestingCardsLotService;
    this.fileService = fileService;
    this.statusService = statusService;
    this.consistencyScheduleService = consistencyScheduleService;

    this.objectMapper = objectMapper;
  }

  @Override
  @Transactional(readOnly = true)
  public List<OperationVo> findAll() {
    return mapToVos(operationRepository.findAllByManagementEntity(ConnectedUser.get().managementEntity));
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public long create(BaseConfiguration baseConfig) throws PastDateException {
    try {
      checkOperationDateIsInFuture(baseConfig.getDate());
      final Operation operation = operationRepository.save(
          operation(
              baseConfig.getShortLabel(),
              baseConfig.getLongLabel(),
              baseConfig.getDate()
          )
      );

      // create empty consistency check
      OperationConsistency operationConsistency = new OperationConsistency();
      operationConsistency.setOperation(operation);
      operationConsistency.setReport(objectMapper.writeValueAsString(new ConsistencyResultVo()));
      operationConsistency.setComputationDate(LocalDateTime.now());
      operationConsistencyRepository.save(operationConsistency);

      return operation.getId();
    } catch (JsonProcessingException jpe) {
      throw new TechnicalException(jpe);
    }
  }

  /**
   * Used to update configuration only information
   */
  @Override
  @Transactional(rollbackFor = Exception.class)
  public OperationVo updateBaseConfiguration(Long operationId, BaseConfiguration baseConfig) throws BusinessException {
    return updateConfiguration(operationId, operation -> {
      checkOperationDateIsInFuture(baseConfig.getDate());
      checkOperationDateIsNotLocked(baseConfig.getDate(), operation);
      operation.setShortLabel(baseConfig.getShortLabel());
      operation.setLongLabel(baseConfig.getLongLabel());
      operation.setDate(baseConfig.getDate());
      operation.setLastConfigurationUpdateDate(LocalDateTime.now());
    });
  }

  @Override
  @Transactional(rollbackFor = Exception.class)
  public OperationVo updateMilestones(long operationId, MilestoneConfiguration milestoneConfig)
      throws BusinessException {
    return updateConfiguration(operationId, operation -> {
      operation.setGracePeriod(milestoneConfig.getGracePeriod());

      List<MilestoneVo> milestoneVos = milestoneConfig.milestones.stream().map(this::alignMilestoneTime)
                                                                 .collect(Collectors.toList());
      // check milestone's validity

      Map<MilestoneType, Milestone> milestones =
          operation.getMilestones().stream().collect(toMap(Milestone::getType, identity()));
      operation.getMilestones().clear();


      if (!milestoneVos.isEmpty()) {
        checkMilestoneDateValidity(operation.getDate(), milestoneConfig.getGracePeriod(), milestoneVos);
        milestoneVos.forEach(
            milestoneVo -> {
              var milestone = milestones.computeIfAbsent(
                  milestoneVo.type,
                  (key) -> {
                    var newMilestone = new Milestone();
                    newMilestone.setType(milestoneVo.type);
                    newMilestone.setOperation(operation);
                    return newMilestone;
                  });
              milestone.setDate(milestoneVo.date);
              operation.getMilestones().add(milestone);
            });
      }
    });
  }

  private OperationVo updateConfiguration(Long operationId, BusinessException.ConsumerWithException<Operation> consumer)
      throws BusinessException {

    Operation operation = operationRepository.findOne(operationId, true);
    if (statusService.isConfigurationInReadonly(operationId)) {
      throw new TryToModifyReadonlyPropertyException();
    }

    consumer.accept(operation);

    consistencyScheduleService.scheduleCheck(operation.getId());
    return BeanMapper.map(operationRepository.save(operation));

  }


  @Override
  @Transactional(readOnly = true)
  public OperationVo findOne(long operationId, boolean restrictedToOperationManagementEntity) {
    return BeanMapper.map(operationRepository.findOne(operationId, restrictedToOperationManagementEntity));
  }

  @Override
  @Transactional
  public OperationVo targetSimulation(long id, String simulationName) throws CannotTargetSimulation {
    Operation operation = operationRepository.findOne(id, true);
    if (operation.getDeploymentTarget() != DeploymentTarget.NOT_DEFINED) {
      throw new CannotTargetSimulation();
    }

    return setTarget(operation, DeploymentTarget.SIMULATION, simulationName);
  }

  @Override
  public OperationVo targetReal(long id) throws CannotTargetProduction {
    Operation operation = operationRepository.findOne(id, true);
    if (operation.getDeploymentTarget() == DeploymentTarget.REAL) {
      throw new CannotTargetProduction();
    }

    return setTarget(operation, DeploymentTarget.REAL, null);
  }

  private List<OperationVo> mapToVos(List<Operation> operations) {
    return operations.stream()
                     .map(BeanMapper::map)
                     .collect(Collectors.toList());
  }

  private OperationVo setTarget(Operation operation, DeploymentTarget target, String simulationName) {
    if (operation.getDeploymentTarget() == DeploymentTarget.NOT_DEFINED) {
      voterTestingCardsLotService.copyTestingCardFromTestSiteToProduction(operation.getId());
    }

    operation.setDeploymentTarget(target);
    operation.setSimulationName(simulationName);
    return BeanMapper.map(operationRepository.save(operation));
  }

  protected MilestoneVo alignMilestoneTime(MilestoneVo milestone) {
    LocalDateTime milestoneDate = null;
    if (milestone.date != null) {
      if (MilestoneType.SITE_OPEN.equals(milestone.type) ||
          MilestoneType.SITE_CLOSE.equals(milestone.type) ||
          MilestoneType.BALLOT_BOX_DECRYPT.equals(milestone.type)) {
        milestoneDate = milestone.date.withSecond(0);
      } else {
        milestoneDate = milestone.date.withHour(0).withMinute(0).withSecond(0);
      }
    }
    return new MilestoneVo(
        milestone.id,
        milestone.login,
        milestone.saveDate,
        milestone.type,
        milestoneDate
    );
  }

  protected String checkDateValidity(LocalDateTime operationDate, int gracePeriod,
                                     Map<MilestoneType, LocalDateTime> milestoneDates, MilestoneVo milestone) {
    if (milestone.date == null) {
      return null;
    }
    if (milestone.date.isBefore(LocalDateTime.now())) {
      return InvalidMilestoneDatesException.IN_FUTURE;
    }
    switch (milestone.type) {
      case SITE_VALIDATION:
        return checkSiteValidationMilestone(operationDate, milestone);
      case CERTIFICATION:
        return checkCertificationMilestone(operationDate, milestoneDates, milestone);
      case PRINTER_FILES:
        return checkPrinterFilesMilestone(operationDate, milestoneDates, milestone);
      case BALLOT_BOX_INIT:
        return checkBallotBoxInitMilestone(operationDate, milestoneDates, milestone);
      case SITE_OPEN:
        return checkSiteOpenMilestone(operationDate, milestoneDates, milestone);
      case SITE_CLOSE:
        return checkSiteCloseMilestone(operationDate, milestoneDates, milestone);
      case BALLOT_BOX_DECRYPT:
        return checkBallotBoxDecryptMilestone(operationDate, gracePeriod, milestoneDates, milestone);
      case RESULT_VALIDATION:
        return checkResultValidationMilestone(operationDate, milestoneDates, milestone);
      case DATA_DESTRUCTION:
        return checkDataDestructionMilestone(operationDate, milestoneDates, milestone);
      default:
        return null;
    }
  }

  private void checkOperationDateIsInFuture(LocalDateTime date) throws PastDateException {
    if (date.isBefore(LocalDateTime.now())) {
      throw new PastDateException();
    }
  }

  private void checkOperationDateIsNotLocked(LocalDateTime newDate, Operation operation) throws LockedDateException {
    if (!newDate.isEqual(operation.getDate()) &&
        (!operation.getMilestones().isEmpty() ||
         !fileService.getFiles(operation.getId(), false,
                               EnumSet.of(FileType.ELECTION_REPOSITORY, FileType.VOTATION_REPOSITORY)).isEmpty())) {
      throw new LockedDateException();
    }
  }

  private void checkMilestoneDateValidity(LocalDateTime operationDate, int gracePeriod, List<MilestoneVo> milestoneVos)
      throws InvalidMilestoneDatesException {
    Map<MilestoneType, LocalDateTime> milestoneDates =
        milestoneVos.stream().filter(milestone -> milestone.date != null).collect(toMap(milestone -> milestone.type,
                                                                                        milestone -> milestone.date));
    Map<String, String> dateInError = new HashMap<>();

    for (MilestoneVo milestoneVo : milestoneVos) {
      String error = checkDateValidity(operationDate, gracePeriod, milestoneDates, milestoneVo);
      if (error != null) {
        dateInError.put(milestoneVo.type.name(), error);
      }
    }

    if (!dateInError.isEmpty()) {
      throw new InvalidMilestoneDatesException(dateInError);
    }
  }

  private String checkSiteValidationMilestone(LocalDateTime operationDate, MilestoneVo milestone) {
    return (!milestone.date.isBefore(operationDate)) ? InvalidMilestoneDatesException.BEFORE_OPERATION : null;
  }

  private String checkCertificationMilestone(LocalDateTime operationDate,
                                             Map<MilestoneType, LocalDateTime> milestoneDates, MilestoneVo milestone) {
    if (!milestone.date.isBefore(operationDate)) {
      return InvalidMilestoneDatesException.BEFORE_OPERATION;
    }
    if (milestoneDates.get(MilestoneType.SITE_VALIDATION) != null && milestone.date
        .isBefore(milestoneDates.get(MilestoneType.SITE_VALIDATION))) {
      return InvalidMilestoneDatesException.CERTIFICATION_DATE;
    }
    return null;
  }

  private String checkPrinterFilesMilestone(LocalDateTime operationDate,
                                            Map<MilestoneType, LocalDateTime> milestoneDates, MilestoneVo milestone) {
    if (!milestone.date.isBefore(operationDate)) {
      return InvalidMilestoneDatesException.BEFORE_OPERATION;
    }
    if (milestoneDates.get(MilestoneType.CERTIFICATION) != null && milestone.date
        .isBefore(milestoneDates.get(MilestoneType.CERTIFICATION))) {
      return InvalidMilestoneDatesException.PRINTER_FILE_DATE;
    }
    return null;
  }

  private String checkBallotBoxInitMilestone(LocalDateTime operationDate,
                                             Map<MilestoneType, LocalDateTime> milestoneDates, MilestoneVo milestone) {
    if (!milestone.date.isBefore(operationDate)) {
      return InvalidMilestoneDatesException.BEFORE_OPERATION;
    }
    if (milestoneDates.get(MilestoneType.PRINTER_FILES) != null && milestone.date
        .isBefore(milestoneDates.get(MilestoneType.PRINTER_FILES))) {
      return InvalidMilestoneDatesException.BALLOT_BOX_INIT_DATE;
    }
    return null;
  }

  private String checkSiteOpenMilestone(LocalDateTime operationDate, Map<MilestoneType, LocalDateTime>
      milestoneDates, MilestoneVo milestone) {
    if (!milestone.date.isBefore(operationDate)) {
      return InvalidMilestoneDatesException.BEFORE_OPERATION;
    } else if (milestoneDates.get(MilestoneType.BALLOT_BOX_INIT) != null && !milestone.date
        .isAfter(milestoneDates.get(MilestoneType.BALLOT_BOX_INIT))) {
      return InvalidMilestoneDatesException.SITE_OPEN_DATE;
    }
    return null;
  }

  private String checkSiteCloseMilestone(LocalDateTime operationDate,
                                         Map<MilestoneType, LocalDateTime> milestoneDates, MilestoneVo milestone) {
    if (milestone.date.withHour(0).withMinute(0).isAfter(operationDate)) {
      return InvalidMilestoneDatesException.BEFORE_OR_EQUAL_OPERATION;
    }
    if (!milestone.date.isAfter(milestoneDates.get(MilestoneType.SITE_OPEN))) {
      return InvalidMilestoneDatesException.SITE_CLOSE_DATE;
    }
    return null;
  }

  private String checkBallotBoxDecryptMilestone(LocalDateTime operationDate, int gracePeriod,
                                                Map<MilestoneType, LocalDateTime> milestoneDates,
                                                MilestoneVo milestone) {
    if (milestone.date.isAfter(operationDate)) {
      return InvalidMilestoneDatesException.BEFORE_OR_EQUAL_OPERATION;
    }
    if (milestone.date.isBefore(milestoneDates.get(MilestoneType.SITE_CLOSE).plusMinutes(gracePeriod))) {
      return InvalidMilestoneDatesException.BALLOT_BOX_DECRYPT_DATE;
    }
    return null;
  }

  private String checkResultValidationMilestone(LocalDateTime operationDate,
                                                Map<MilestoneType, LocalDateTime> milestoneDates,
                                                MilestoneVo milestone) {
    if (milestone.date.isBefore(operationDate)) {
      return InvalidMilestoneDatesException.AFTER_OR_EQUAL_OPERATION;
    }
    if (milestone.date.isBefore(milestoneDates.get(MilestoneType.BALLOT_BOX_DECRYPT))) {
      return InvalidMilestoneDatesException.RESULT_VALIDATION_DATE;
    }
    return null;
  }

  private String checkDataDestructionMilestone(LocalDateTime operationDate,
                                               Map<MilestoneType, LocalDateTime> milestoneDates,
                                               MilestoneVo milestone) {
    if (!milestone.date.isAfter(operationDate)) {
      return InvalidMilestoneDatesException.AFTER_OPERATION;
    }
    if (milestoneDates.get(MilestoneType.RESULT_VALIDATION) != null && !milestone.date.isAfter(milestoneDates.get
        (MilestoneType.RESULT_VALIDATION))) {
      return InvalidMilestoneDatesException.DATA_DESTRUCTION_DATE;
    }
    return null;
  }
}
