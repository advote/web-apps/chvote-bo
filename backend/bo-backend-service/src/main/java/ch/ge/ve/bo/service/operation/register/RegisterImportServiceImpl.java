/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register;

import static ch.ge.ve.bo.service.utils.HashUtils.computeFilesHash;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.entity.RegisterMetadata;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.exception.InvalidFileException;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.OperationService;
import ch.ge.ve.bo.service.operation.model.OperationVo;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import ch.ge.ve.bo.service.operation.register.exception.ConcurrentImportException;
import ch.ge.ve.bo.service.operation.register.exception.NoDomainInfluenceException;
import ch.ge.ve.bo.service.operation.register.model.DetailedReport;
import ch.ge.ve.bo.service.operation.register.model.FileInfo;
import ch.ge.ve.bo.service.operation.register.model.ImportRegisterResult;
import ch.ge.ve.bo.service.operation.register.model.ImportedRegisterFile;
import ch.ge.ve.bo.service.operation.register.model.RegisterVoterRef;
import ch.ge.ve.bo.service.operation.register.model.VoterRefTable;
import ch.ge.ve.bo.service.operation.register.processor.BusinessProcessor;
import ch.ge.ve.bo.service.operation.register.processor.SchemaValidationProcessor;
import ch.ge.ve.bo.service.operation.register.processor.ZipProcessor;
import ch.ge.ve.bo.service.utils.BufferedTeeStream;
import ch.ge.ve.bo.service.utils.xml.XSDValidator;
import ch.ge.ve.filenamer.FileNamer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import net.sf.jasperreports.engine.JRException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of RegisterImportService
 */
public class RegisterImportServiceImpl implements RegisterImportService {
  private static final int    BUFFER_SIZE                   = 16 * 1024;
  private static final String DETAILED_REPORT_PROPERTY_NAME = "detailedReport";

  private final RegisterMetadataService    metadataService;
  private final XSDValidator               xsdValidator;
  private final RegisterReportBuilder      reportBuilder;
  private final OperationRepositoryService operationRepositoryService;
  private final OperationService           operationService;
  /**
   * It is important that thread runs in parallel to allow buffered Tee Stream to work without dead locks. Therefor a
   * cached thread pool while enabling Thread to be reused permit to have all threads running in parallel
   */
  private final Executor                   executor = Executors.newCachedThreadPool();

  private final ObjectMapper objectMapper;

  /**
   * Default constructor
   */
  public RegisterImportServiceImpl(XSDValidator xsdValidator,
                                   ObjectMapper objectMapper,
                                   RegisterMetadataService metadataService,
                                   OperationRepositoryService operationRepositoryService,
                                   RegisterReportBuilder reportBuilder,
                                   OperationService operationService) {
    this.xsdValidator = xsdValidator;
    this.operationRepositoryService = operationRepositoryService;
    this.objectMapper = objectMapper;
    this.metadataService = metadataService;
    this.reportBuilder = reportBuilder;
    this.operationService = operationService;
  }


  @Override
  // Never open a transaction here
  // Stream can take a while to proceed
  public ImportRegisterResult importRegister(Long operationId, String fileName, InputStream inputStream)
      throws BusinessException {

    ImportRegisterResult registryResult;
    BufferedTeeStream bufferedTeeStream = new BufferedTeeStream(inputStream, BUFFER_SIZE);
    try (
        InputStream inputStreamBusinessProcessor = bufferedTeeStream.createInputStream();
        InputStream inputStreamSchemaValidationProcessor = bufferedTeeStream.createInputStream();
        InputStream inputStreamZipProcessor = bufferedTeeStream.createInputStream()
    ) {
      OperationVo operation = operationService.findOne(operationId, false);

      // check that the file is an XML file
      if (!fileName.toLowerCase(Locale.ENGLISH).endsWith(FileVo.XML_EXTENSION)) {
        throw new InvalidFileException("global.errors.not-xml-file");
      }

      List<RegisterMetadata> alreadyImported = metadataService.getAllByFileOperationIdAndValidated(operationId, true);
      VoterRefTable voterRefTable = loadVoterRef(alreadyImported);

      final Set<DomainOfInfluenceVo> doiFiles =
          operationRepositoryService.getAllDomainOfInfluenceForOperation(operationId);
      if (isEmpty(doiFiles)) {
        throw new NoDomainInfluenceException();
      }
      Set<String> domainOfInfluences =
          doiFiles.stream().map(doi -> String.format("%s.%s", doi.type, doi.id))
                  .collect(Collectors.toSet());

      FileInfo fileInfo = new FileInfo(fileName, ConnectedUser.get().login, LocalDateTime.now());

      RegisterImportHypervisor hypervisor = new RegisterImportHypervisor(
          new BusinessProcessor(inputStreamBusinessProcessor, executor, fileInfo, voterRefTable,
                                getMessageUIds(alreadyImported), domainOfInfluences),
          new SchemaValidationProcessor(inputStreamSchemaValidationProcessor, xsdValidator, executor),
          new ZipProcessor(inputStreamZipProcessor, executor, fileInfo.getFileName())
      );

      hypervisor.run();
      hypervisor.waitEndResult();

      registryResult = new ImportRegisterResult(
          hypervisor.getBusinessProcessor().getContext(),
          hypervisor.getSchemaValidationProcessor().getErrors()
      );

      if (shouldSave(registryResult)) {
        final DetailedReport detailedReport = hypervisor.getBusinessProcessor().getContext()
                                                        .getDetailedReport();
        String voterHash = getHashes(voterRefTable.getHashTableForFile(fileName));

        String preHash = computeFilesHash(alreadyImported.stream().map(RegisterMetadata::getHashTable));
        String postHash = computeFilesHash(
            Stream.concat(alreadyImported.stream().map(RegisterMetadata::getHashTable), Stream.of(voterHash)));

        metadataService.store(
            operationId,
            hypervisor.getBusinessProcessor().getMessageUniqueId(),
            fileName,
            hypervisor.getZipProcessor().getZipAsByteArray(),
            voterHash,
            generateReport(registryResult, detailedReport), preHash, postHash);

        registryResult.addDetailedReport(generateDetailedReport(operation.longLabel, detailedReport));
      }

    } catch (IOException e) {
      throw new TechnicalException(e);
    }
    return registryResult;
  }

  private byte[] generateDetailedReport(String longLabel, DetailedReport detailedReport) {
    try {
      String detailedReportJson = objectMapper.writeValueAsString(detailedReport);
      return reportBuilder.buildReport(detailedReportJson, longLabel, RegisterReportBuilder.SupportedFormat.PDF);
    } catch (JRException | IOException e) {
      throw new TechnicalException(e);
    }
  }

  private Set<String> getMessageUIds(List<RegisterMetadata> alreadyImported) {
    return alreadyImported.stream().map(RegisterMetadata::getMessageUniqueId).collect(Collectors.toSet());
  }

  private VoterRefTable loadVoterRef(List<RegisterMetadata> alreadyImported) {
    VoterRefTable voterRefTable = new VoterRefTable();

    for (RegisterMetadata meta : alreadyImported) {

      ImportedRegisterFile registryFileInfo;
      try {
        registryFileInfo = objectMapper.readValue(meta.getReport(), ImportedRegisterFile.class);
      } catch (IOException e) {
        throw new TechnicalException(e);
      }

      FileInfo fileInfo = new FileInfo(meta.getFile(), registryFileInfo);

      for (String line : meta.getHashTable().split("\n")) {
        String[] cells = line.split(";");
        voterRefTable.add(fileInfo, new RegisterVoterRef(cells[1], cells[0]));
      }
    }
    return voterRefTable;
  }

  private String generateReport(ImportRegisterResult result, DetailedReport detailedReport) {
    ImportedRegisterFile importedRegistryFile = new ImportedRegisterFile(
        ConnectedUser.get().login,
        ConnectedUser.get().managementEntity,
        LocalDateTime.now(),
        result.getEmitter(),
        result.getEmissionDate(),
        result.getVoterCount(),
        result.getDoiCount(),
        result.getMessageUniqueId(),
        result.getCountingCircleCount(),
        result.getFileName(),
        detailedReport
    );

    try {
      return objectMapper.writeValueAsString(importedRegistryFile);
    } catch (JsonProcessingException e) {
      throw new TechnicalException(e);
    }
  }

  private boolean shouldSave(ImportRegisterResult registryResult) {
    return registryResult.getValidationErrors().isEmpty() &&
           registryResult.getDuplicates().isEmpty() &&
           registryResult.getInvalidDateOfBirths().isEmpty() &&
           registryResult.getUndefinedDois().isEmpty();
  }

  @Override
  public void validateImport(Long operationId, String messageUniqueId) throws ConcurrentImportException {
    metadataService.validate(operationId, messageUniqueId);
  }


  private String getHashes(List<RegisterVoterRef> registryVoters) {
    return registryVoters.stream().map(ref -> ref.getHash() + ";" + ref.getIdentifier())
                         .collect(Collectors.joining("\n"));
  }


  @Override
  public List<String> getReports(long operationId) {
    return metadataService.getAllByFileOperationIdAndValidated(operationId, true)
                          .stream()
                          .map(RegisterMetadata::getReport)
                          .collect(Collectors.toList());
  }

  @Override
  public void deleteImport(long operationId, String messageUniqueId) {
    metadataService.delete(operationId, messageUniqueId);
  }

  @Override
  @Transactional(readOnly = true)
  public FileVo getDetailedReportPDF(long operationId, String messageUniqueId) {
    final RegisterMetadata registerMetadata =
        metadataService.getByOperationIdAndFileMessageUniqueIdAndValidated(operationId, messageUniqueId, true);
    try {
      final JsonNode detailedReportNode = getDetailedReportJsonNode(registerMetadata.getReport());
      return new FileVo(FileNamer.votersReport(registerMetadata.getOperation().getShortLabel(),
                                               registerMetadata.getFile().getFileName(),
                                               registerMetadata.getFile().getDate()),
                        FileType.REGISTER, registerMetadata.getFile().getManagementEntity(),
                        reportBuilder.buildReport(detailedReportNode.toString(),
                                                  operationService.findOne(operationId, false).longLabel,
                                                  RegisterReportBuilder.SupportedFormat.PDF));
    } catch (JRException e) {
      throw new TechnicalException(e);
    }
  }

  private DetailedReport aNewDetailedReport(JsonNode detailedReportNode) {
    try {
      return objectMapper.readValue(detailedReportNode.traverse(), DetailedReport.class);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }

  private JsonNode getDetailedReportJsonNode(String reportJson) {
    try {
      return objectMapper.readTree(reportJson).get(DETAILED_REPORT_PROPERTY_NAME);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }

  @Override
  @Transactional(readOnly = true)
  public FileVo getDetailedReportPDF(long operationId) {
    final List<RegisterMetadata> registerMetadataList =
        metadataService.getAllByFileOperationIdAndValidated(operationId, true);
    DetailedReport consolidatedReport =
        DetailedReport.merge(registerMetadataList.stream().map(
            registerMetadata -> aNewDetailedReport(getDetailedReportJsonNode(registerMetadata.getReport())))
                                                 .toArray(DetailedReport[]::new));

    try {
      final String reportJson = objectMapper.writer().writeValueAsString(consolidatedReport);
      OperationVo operationVo = operationService.findOne(operationId, false);

      return new FileVo(FileNamer.consolidatedVotersReport(operationVo.shortLabel, operationVo.date),
                        FileType.REGISTER,
                        operationVo.managementEntity,
                        reportBuilder.buildReport(
                            reportJson, operationVo.longLabel, RegisterReportBuilder.SupportedFormat.PDF));

    } catch (JsonProcessingException | JRException e) {
      throw new TechnicalException(e);
    }
  }

}
