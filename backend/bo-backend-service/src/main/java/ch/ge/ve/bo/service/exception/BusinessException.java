/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.exception;

/**
 * Root of Business exception hierarchy
 */
public class BusinessException extends Exception {

  private final String   messageKey;
  private final String[] parameters;

  /**
   * empty constructor
   */
  public BusinessException() {
    this(null);
  }

  /**
   * default constructor
   */
  public BusinessException(String messageKey, Throwable cause, String... parameters) {
    super(cause);
    this.messageKey = messageKey;
    this.parameters = parameters;
  }

  /**
   * default constructor with no cause
   */
  public BusinessException(String messageKey, String... parameters) {
    this(messageKey, null, parameters);
  }

  /**
   * default constructor with no cause or parameters
   */
  public BusinessException(String messageKey) {
    this(messageKey, null);
  }

  public String getMessageKey() {
    return messageKey;
  }

  public String[] getParameters() {
    return parameters;
  }


  @FunctionalInterface
  public interface ConsumerWithException<T> {

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    void accept(T t) throws BusinessException;
  }

}
