/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Value object representing an entry with an incorrect date of birth in a register file.
 */
public class InvalidDateOfBirthVo {
  public final String        fileName;
  public final String        importedBy;
  public final LocalDateTime importDate;
  public final String        emitter;
  public final LocalDateTime emissionDate;
  public final String        identifier;
  public final String        title;
  public final String        lastName;
  public final String        firstName;
  public final String        address;
  public final String        municipality;

  /**
   * Default constructor
   */
  public InvalidDateOfBirthVo(OperationVoterRef operationVoterRef, Voter voter) {
    this.fileName = operationVoterRef.fileInfo.getFileName();
    this.importedBy = operationVoterRef.fileInfo.getImportedBy();
    this.importDate = operationVoterRef.fileInfo.getImportDate();
    this.emitter = operationVoterRef.fileInfo.getEmitter();
    this.emissionDate = operationVoterRef.fileInfo.getEmissionDate();
    this.identifier = operationVoterRef.identifier;

    this.title = voter.getTitle();
    this.lastName = voter.getLastName();
    this.firstName = voter.getFirstName();

    this.address = Stream.of(voter.getAddressLine1(), voter.getAddressLine2(), voter.getStreet())
                         .filter(Objects::nonNull)
                         .collect(Collectors.joining(", "));

    this.municipality = Stream.of(voter.getZipCode(), voter.getTown())
                              .filter(Objects::nonNull)
                              .collect(Collectors.joining(" "));
  }
}
