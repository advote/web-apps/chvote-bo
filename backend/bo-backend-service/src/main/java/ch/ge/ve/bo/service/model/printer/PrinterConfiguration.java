/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.model.printer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigInteger;

/**
 * Configuration of a printer
 */
public class PrinterConfiguration {
  public final String     id;
  public final String     name;
  public final BigInteger publicKey;
  public final int        municipalityForTestingCard;


  /**
   * Default constructor
   */
  @JsonCreator
  public PrinterConfiguration(@JsonProperty("id") String id,
                              @JsonProperty("name") String name,
                              @JsonProperty("publicKey") BigInteger publicKey,
                              @JsonProperty("municipalityForTestingCard") int municipalityForTestingCard) {
    this.id = id;
    this.name = name;
    this.publicKey = publicKey;
    this.municipalityForTestingCard = municipalityForTestingCard;
  }
}
