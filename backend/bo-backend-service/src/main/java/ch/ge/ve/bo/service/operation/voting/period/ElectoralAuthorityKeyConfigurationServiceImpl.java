/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.voting.period;

import ch.ge.ve.bo.repository.ElectoralAuthorityKeyConfigurationRepository;
import ch.ge.ve.bo.repository.ElectoralAuthorityKeyRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.ElectoralAuthorityKeyConfiguration;
import ch.ge.ve.bo.service.electoralAuthorityKey.model.ElectoralAuthorityKeyVo;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import java.util.Optional;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

/**
 * Default implementation of {@link ElectoralAuthorityKeyConfigurationService}
 */
public class ElectoralAuthorityKeyConfigurationServiceImpl implements ElectoralAuthorityKeyConfigurationService {
  private final ElectoralAuthorityKeyConfigurationRepository repository;
  private final SecuredOperationRepository                   operationRepository;
  private final ElectoralAuthorityKeyRepository              electoralAuthorityKeyRepository;
  private final SecuredOperationHolderService                securedOperationHolderService;

  /**
   * Default constructor
   */
  public ElectoralAuthorityKeyConfigurationServiceImpl(ElectoralAuthorityKeyConfigurationRepository repository,
                                                       SecuredOperationRepository operationRepository,
                                                       ElectoralAuthorityKeyRepository electoralAuthorityKeyRepository,
                                                       SecuredOperationHolderService securedOperationHolderService) {
    this.repository = repository;
    this.operationRepository = operationRepository;
    this.electoralAuthorityKeyRepository = electoralAuthorityKeyRepository;
    this.securedOperationHolderService = securedOperationHolderService;
  }


  @Override
  @Transactional
  public ElectoralAuthorityKeyVo saveOrUpdate(long operationId, long keyId) {
    return securedOperationHolderService.doNotOptionalUpdate(
        () -> {
          ElectoralAuthorityKeyConfiguration config = repository.findByOperationId(operationId).orElseGet(() -> {
            ElectoralAuthorityKeyConfiguration c = new ElectoralAuthorityKeyConfiguration();
            c.setOperation(operationRepository.findOne(operationId, true));
            return c;
          });

          config.setElectoralAuthorityKey(
              electoralAuthorityKeyRepository
                  .findById(keyId)
                  .orElseThrow(() -> new EntityNotFoundException("electoralAuthorityKey " + keyId))
          );
          return config;

        }, config -> new ElectoralAuthorityKeyVo(repository.save(config).getElectoralAuthorityKey())
    );
  }

  @Override
  public Optional<ElectoralAuthorityKeyVo> findForOperation(long operationId) {
    return securedOperationHolderService.safeRead(() -> repository.findByOperationId(operationId))
                                        .map(config -> new ElectoralAuthorityKeyVo(config.getElectoralAuthorityKey()));
  }

  @Override
  public byte[] getContentForOperation(long operationId) {
    return securedOperationHolderService
        .safeRead(() -> repository.findByOperationId(operationId))
        .map(config -> config.getElectoralAuthorityKey().getKeyContent())
        .orElse(new byte[0]);
  }
}
