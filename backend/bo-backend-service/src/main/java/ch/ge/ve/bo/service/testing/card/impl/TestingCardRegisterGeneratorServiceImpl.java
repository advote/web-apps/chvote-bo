/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.testing.card.impl;


import static ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle.CONTROLLER_TESTING_CARDS;
import static ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle.NOT_PRINTABLE_TESTING_CARDS;
import static ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle.PRINTABLE_TESTING_CARDS;
import static ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle.PRINTER_TESTING_CARDS;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.VoterTestingCardsLotRepository;
import ch.ge.ve.bo.repository.entity.CardType;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.Signature;
import ch.ge.ve.bo.repository.entity.TestingCardLanguage;
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot;
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration;
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import ch.ge.ve.bo.service.testing.card.CannotGenerateTestingCardXmlException;
import ch.ge.ve.bo.service.testing.card.TestingCardRegisterGeneratorService;
import ch.ge.ve.chvote.pactback.contract.operation.VirtualCountingCircle;
import ch.ge.ve.interfaces.ech.eCH0007.v6.CantonAbbreviationType;
import ch.ge.ve.interfaces.ech.eCH0007.v6.SwissMunicipalityType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.AddressInformationType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.CountryType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.OrganisationMailAddressInfoType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.OrganisationMailAddressType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.PersonMailAddressInfoType;
import ch.ge.ve.interfaces.ech.eCH0010.v6.PersonMailAddressType;
import ch.ge.ve.interfaces.ech.eCH0011.v8.PlaceOfOriginType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.DatePartiallyKnownType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.NamedPersonIdType;
import ch.ge.ve.interfaces.ech.eCH0044.v4.PersonIdentificationType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.AuthorityType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.CantonalRegisterType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.LanguageType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissDomesticType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.SwissPersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VoterDelivery;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VoterListType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.VotingPersonType;
import ch.ge.ve.interfaces.ech.eCH0045.v4.extension.PersonTypeExtensionType;
import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0058.v5.SendingApplicationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.CountingCircleType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.DomainOfInfluenceTypeType;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * Default implementation of {@link TestingCardRegisterGeneratorService}
 */
public class TestingCardRegisterGeneratorServiceImpl implements TestingCardRegisterGeneratorService {

  private static final String DATA_LOCK     = "0";
  private static final String ALLIANCE_NAME = "SINGLE";

  private final Clock clock;

  private final OperationRepositoryService    repositoryService;
  private final PrinterTemplateService        printerTemplateService;
  private final SecuredOperationHolderService securedOperationHolderService;

  private final VoterTestingCardsLotRepository voterTestingCardsLotRepository;
  private final SecuredOperationRepository     operationRepository;

  /**
   * Default constructor
   */
  public TestingCardRegisterGeneratorServiceImpl(OperationRepositoryService repositoryService,
                                                 VoterTestingCardsLotRepository voterTestingCardsLotRepository,
                                                 SecuredOperationRepository operationRepository,
                                                 SecuredOperationHolderService securedOperationHolderService,
                                                 PrinterTemplateService printerTemplateService,
                                                 Clock clock) {
    this.repositoryService = repositoryService;
    this.voterTestingCardsLotRepository = voterTestingCardsLotRepository;
    this.operationRepository = operationRepository;
    this.securedOperationHolderService = securedOperationHolderService;
    this.printerTemplateService = printerTemplateService;
    this.clock = clock;
  }

  @Override
  public Map<VoterTestingCardsLot, Map<String, String>> generateForOperation(long operationId, boolean
      forConfiguration) {
    Operation operation = operationRepository.findOne(operationId, true);
    Map<String, DomainOfInfluenceVo> doiById = repositoryService.getAllDomainOfInfluenceForOperation(
        operation.getId()).stream().collect(Collectors.toMap(doi -> doi.id, doi -> doi));

    final List<VoterTestingCardsLot> lots = securedOperationHolderService.safeReadAll(
        () -> voterTestingCardsLotRepository.findAllByOperationIdAndCardTypeInOrderById(
            operationId, forConfiguration ? CardType.forConfiguration() : CardType.forVotingMaterial())
    );

    Map<VoterTestingCardsLot, Map<String, String>> files = new HashMap<>();

    for (VoterTestingCardsLot lot : lots) {
      Map<String, String> printers = new HashMap<>();
      for (PrinterAndMunicipality printerAndMunicipality : getMunicipalitiesToCreateTestingCardFor(lot)) {
        printers.put(printerAndMunicipality.printer,
                     new String(generate(lot, printerAndMunicipality.municipality, doiById), Charset.forName("UTF-8")));
      }
      files.put(lot, printers);
    }

    return files;
  }

  private List<PrinterAndMunicipality> getMunicipalitiesToCreateTestingCardFor(VoterTestingCardsLot lot) {
    if (!lot.isShouldPrint()) {
      return Collections
          .singletonList(createVirtualMunicipalityForPrinter(printerTemplateService.getVirtualPrinterForTestCard()));
    }
    PrinterTemplateVo printerTemplate =
        printerTemplateService
            .getPrinterTemplate(lot.getOperation().getCanton(), lot.getOperation().getPrinterTemplate());
    if (lot.getCardType() == CardType.PRINTER_TESTING_CARD) {
      return Arrays.stream(printerTemplate.printerConfigurations)
                   .map(TestingCardRegisterGeneratorServiceImpl::createVirtualMunicipalityForPrinter)
                   .collect(Collectors.toList());
    }

    return Arrays.stream(printerTemplate.printerConfigurations)
                 .filter(pc -> pc.id.equals(printerTemplate.printerIdForPrintableTestingCard))
                 .map(TestingCardRegisterGeneratorServiceImpl::createVirtualMunicipalityForPrinter)
                 .collect(Collectors.toList());
  }


  private byte[] generate(VoterTestingCardsLot lot, SwissMunicipalityType municipality,
                          Map<String, DomainOfInfluenceVo> doiById) {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(VoterDelivery.class);

      Marshaller marshaller = jaxbContext.createMarshaller();

      VoterDelivery voterDelivery = new VoterDelivery();
      voterDelivery.setDeliveryHeader(deliveryHeader(lot));
      voterDelivery.setVoterList(createVoterList(lot, municipality, doiById));
      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
      marshaller.marshal(voterDelivery, outputStream);
      return outputStream.toByteArray();
    } catch (JAXBException e) {
      throw new CannotGenerateTestingCardXmlException(e);
    }
  }

  private VoterListType createVoterList(VoterTestingCardsLot lot, SwissMunicipalityType municipality,
                                        Map<String, DomainOfInfluenceVo> doiById) {
    VoterListType voterList = new VoterListType();
    voterList.setReportingAuthority(authority(lot));
    voterList.setNumberOfVoters(BigInteger.valueOf(lot.getCount()));

    IntStream.range(0, lot.getCount())
             .forEach(i -> voterList.getVoter().add(votingPersonType(i, lot, municipality, doiById)));
    return voterList;
  }


  private AuthorityType authority(VoterTestingCardsLot lot) {
    AuthorityType authority = new AuthorityType();
    CantonalRegisterType cantonalRegister = new CantonalRegisterType();
    cantonalRegister.setCantonAbbreviation(CantonAbbreviationType.fromValue(ConnectedUser.get().realm));
    cantonalRegister.setRegisterIdentification("testing-card-" + lot.getId());
    cantonalRegister.setRegisterName(lot.getLotName());
    authority.setCantonalRegister(cantonalRegister);
    return authority;
  }

  private HeaderType deliveryHeader(VoterTestingCardsLot lot) {
    HeaderType header = new HeaderType();
    header.setSenderId("BOH");
    header.setMessageId(lot.getId() + "_" + lot.getSaveDate().format(DateTimeFormatter.ISO_DATE_TIME));
    header.setMessageType("Registry");
    header.setAction("1");
    header.setMessageDate(LocalDateTime.now(clock));
    SendingApplicationType application = new SendingApplicationType();
    application.setManufacturer("ch.ge.ve");
    application.setProduct("BOH");
    application.setProductVersion("1");
    header.setSendingApplication(application);
    return header;
  }

  private VotingPersonType votingPersonType(int voterIdx, VoterTestingCardsLot lot, SwissMunicipalityType
      municipality, Map<String, DomainOfInfluenceVo> doiById) {
    VotingPersonType votingPersonType = new VotingPersonType();
    votingPersonType.setDataLock(DATA_LOCK);
    final PersonMailAddressType address = personMailAddressType(lot);
    votingPersonType.setDeliveryAddress(address);
    votingPersonType.setElectoralAddress(address);
    votingPersonType.setPerson(person(voterIdx, lot, municipality));
    votingPersonType.setIsEvoter(true);
    List<VotingPersonType.DomainOfInfluenceInfo> domainOfInfluenceInfoList =
        votingPersonType.getDomainOfInfluenceInfo();
    domainOfInfluenceInfoList.forEach(VotingPersonType.DomainOfInfluenceInfo::getDomainOfInfluence);

    lot.getDoi().stream()
       .map(doiById::get)
       .forEach(doi -> votingPersonType.getDomainOfInfluenceInfo().add(domainOfInfluenceInfo(doi, lot)));

    return votingPersonType;
  }

  private VotingPersonType.DomainOfInfluenceInfo domainOfInfluenceInfo(DomainOfInfluenceVo doi,
                                                                       VoterTestingCardsLot lot) {
    VotingPersonType.DomainOfInfluenceInfo domainOfInfluenceInfo = new VotingPersonType.DomainOfInfluenceInfo();
    domainOfInfluenceInfo.setDomainOfInfluence(domainOfInfluence(doi));
    domainOfInfluenceInfo.setCountingCircle(virtualCountingCircles(lot));
    return domainOfInfluenceInfo;
  }

  private DomainOfInfluenceType domainOfInfluence(DomainOfInfluenceVo doi) {
    DomainOfInfluenceType domainOfInfluenceType = new DomainOfInfluenceType();
    domainOfInfluenceType.setDomainOfInfluenceType(DomainOfInfluenceTypeType.fromValue(doi.type));
    domainOfInfluenceType.setLocalDomainOfInfluenceIdentification(doi.id);
    domainOfInfluenceType.setDomainOfInfluenceName(doi.name);
    domainOfInfluenceType.setDomainOfInfluenceShortname(doi.shortName);
    return domainOfInfluenceType;
  }

  private PersonMailAddressType personMailAddressType(VoterTestingCardsLot lot) {
    PersonMailAddressType personMailAddressType = new PersonMailAddressType();
    personMailAddressType.setAddressInformation(addressInformation(lot));
    PersonMailAddressInfoType addressInfoType = new PersonMailAddressInfoType();
    addressInfoType.setFirstName(lot.getFirstName());
    addressInfoType.setLastName(lot.getLastName());
    addressInfoType.setMrMrs(lot.getSignature() == Signature.M ? "2" : "1");
    personMailAddressType.setPerson(addressInfoType);
    return personMailAddressType;
  }

  private AddressInformationType addressInformation(VoterTestingCardsLot lot) {
    AddressInformationType addressInformation = new AddressInformationType();
    addressInformation.setAddressLine1(lot.getAddress1());
    addressInformation.setAddressLine2(lot.getAddress2());
    CountryType countryType = new CountryType();
    countryType.setCountryNameShort("CH");
    addressInformation.setCountry(countryType);
    addressInformation.setSwissZipCode(Long.parseLong(lot.getPostalCode()));
    addressInformation.setTown(lot.getCity());
    addressInformation.setStreet(lot.getStreet());
    return addressInformation;
  }

  private VotingPersonType.Person person(int voterIdx, VoterTestingCardsLot lot, SwissMunicipalityType municipality) {
    SwissDomesticType swissDomestic = new SwissDomesticType();
    swissDomestic.setSwissDomesticPerson(swissPersonType(voterIdx, lot));
    swissDomestic.setMunicipality(municipality);

    VotingPersonType.Person person = new VotingPersonType.Person();
    person.setSwiss(swissDomestic);
    return person;
  }


  private SwissPersonType swissPersonType(int voterIdx, VoterTestingCardsLot lot) {
    SwissPersonType swissPersonType = new SwissPersonType();
    swissPersonType.setAllianceName(ALLIANCE_NAME);
    swissPersonType.getPlaceOfOrigin().add(placeOfOrigin(lot));
    swissPersonType
        .setLanguageOfCorrespondance(lot.getLanguage() == TestingCardLanguage.FR ? LanguageType.FR : LanguageType.DE);
    swissPersonType.setPersonIdentification(personIdentification(voterIdx, lot));


    OrganisationMailAddressInfoType organisationMailAddressInfoType = new OrganisationMailAddressInfoType();
    organisationMailAddressInfoType.setOrganisationName("ASE");
    organisationMailAddressInfoType.setOrganisationNameAddOn1("votingPlace Code to display on voting card");

    OrganisationMailAddressType votingPlace = new OrganisationMailAddressType();
    votingPlace.setOrganisation(organisationMailAddressInfoType);
    votingPlace.setAddressInformation(addressInformationType("avenue du grand pré 55", 1100));

    PersonTypeExtensionType personTypeExtension = new PersonTypeExtensionType();
    personTypeExtension.setPostageCode(1 + voterIdx % 3);
    personTypeExtension.setVotingPlace(votingPlace);

    OrganisationMailAddressType votingCardReturnAddress = new OrganisationMailAddressType();
    OrganisationMailAddressInfoType organisationForVotingCardReturnAddress = new OrganisationMailAddressInfoType();
    organisationForVotingCardReturnAddress.setOrganisationName("SVE");
    votingCardReturnAddress.setOrganisation(organisationForVotingCardReturnAddress);


    votingCardReturnAddress.setAddressInformation(addressInformationType("Route des acacias 25", 1211));
    personTypeExtension.setVotingCardReturnAddress(votingCardReturnAddress);
    swissPersonType.setExtension(personTypeExtension);
    return swissPersonType;
  }

  private AddressInformationType addressInformationType(String addressLine1, long swissZipCode) {
    AddressInformationType addressInformation = new AddressInformationType();
    CountryType country1 = new CountryType();
    country1.setCountryNameShort("Switzerland");
    addressInformation.setCountry(country1);
    addressInformation.setTown("Genève");
    addressInformation.setAddressLine1(addressLine1);
    addressInformation.setSwissZipCode(swissZipCode);

    return addressInformation;
  }

  private PlaceOfOriginType placeOfOrigin(VoterTestingCardsLot lot) {
    PlaceOfOriginType placeOfOriginType = new PlaceOfOriginType();
    placeOfOriginType.setOriginName(lot.getCity());

    placeOfOriginType
        .setCanton(ch.ge.ve.interfaces.ech.eCH0007.v5.CantonAbbreviationType.valueOf(ConnectedUser.get().realm));
    return placeOfOriginType;
  }

  private CountingCircleType virtualCountingCircles(VoterTestingCardsLot lot) {
    switch (lot.getCardType()) {
      case PRINTER_TESTING_CARD:
        return virtualCountingCircle(PRINTER_TESTING_CARDS);
      case CONTROLLER_TESTING_CARD:
        return virtualCountingCircle(CONTROLLER_TESTING_CARDS);
      default:
        return virtualCountingCircle(lot.isShouldPrint() ? PRINTABLE_TESTING_CARDS : NOT_PRINTABLE_TESTING_CARDS);
    }
  }

  private CountingCircleType virtualCountingCircle(VirtualCountingCircle virtualCountingCircle) {
    CountingCircleType countingCircleType = new CountingCircleType();
    countingCircleType.setCountingCircleId(String.valueOf(virtualCountingCircle.id));
    countingCircleType.setCountingCircleName(virtualCountingCircle.countingCircleName);
    return countingCircleType;
  }

  private PersonIdentificationType personIdentification(int voterIdx, VoterTestingCardsLot lot) {
    PersonIdentificationType personIdentification = new PersonIdentificationType();
    NamedPersonIdType namedPersonId = new NamedPersonIdType();
    namedPersonId.setPersonId("test-card-" + lot.getId() + "-" + voterIdx);
    namedPersonId.setPersonIdCategory("id");
    personIdentification.setLocalPersonId(namedPersonId);
    personIdentification.setDateOfBirth(datePartiallyKnown(lot.getBirthday()));
    personIdentification.setFirstName(lot.getFirstName());
    personIdentification.setOfficialName(lot.getLastName());
    personIdentification.setSex(lot.getSignature() == Signature.M ? "1" : "0");
    return personIdentification;
  }

  private static PrinterAndMunicipality createVirtualMunicipalityForPrinter(PrinterConfiguration printerConfiguration) {
    SwissMunicipalityType swissMunicipalityType = new SwissMunicipalityType();
    swissMunicipalityType.setMunicipalityId(printerConfiguration.municipalityForTestingCard);
    swissMunicipalityType.setMunicipalityName("Virtual municipality for printer " + printerConfiguration.id);
    return new PrinterAndMunicipality(printerConfiguration.id, swissMunicipalityType);
  }

  private DatePartiallyKnownType datePartiallyKnown(LocalDateTime birthday) {
    DatePartiallyKnownType datePartiallyKnown = new DatePartiallyKnownType();
    datePartiallyKnown.setYearMonthDay(birthday.toLocalDate());
    return datePartiallyKnown;
  }

  private static final class PrinterAndMunicipality {
    public final String                printer;
    public final SwissMunicipalityType municipality;

    public PrinterAndMunicipality(String printer, SwissMunicipalityType municipality) {
      this.printer = printer;
      this.municipality = municipality;
    }
  }
}


