/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation;

import ch.ge.ve.bo.repository.security.OperationHolder;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * service used to secure read and write operation on operation holders entities
 */
public interface SecuredOperationHolderService {

  /**
   * Do a read on an {@link OperationHolder} and ensure that user as the right
   * to read the linked {@link ch.ge.ve.bo.repository.entity.Operation}
   */
  <T extends OperationHolder> Optional<T> safeRead(Supplier<Optional<T>> operationHolderSupplier);

  /**
   * same as {@link #safeRead(Supplier)} for a collection of {@link OperationHolder}
   */
  <U extends OperationHolder, T extends Collection<U>> T safeReadAll(Supplier<T> operationHolderSupplier);

  /**
   * Same as {@link #doUpdate(Supplier, Function)} but returns a void
   */
  <U extends OperationHolder> void doUpdateAndForget(Supplier<Optional<U>> operationHolder, Consumer<U>
      updater);

  /**
   * Same as {@link #doUpdateOperationHolder(U, Function)} but returns a void
   */
  <U extends OperationHolder> void doUpdateOperationHolderAndForget(U operationHolder, Consumer<U> updater);


  /**
   * Same as {@link #doUpdateAndForget(Supplier, Consumer)}
   */
  <U extends OperationHolder> void doNotOptionalUpdateAndForget(Supplier<U> operationHolder, Consumer<U>
      updater);

  /**
   * Warning : this method should be used carefully
   * This method perform an update with read only controls.
   * In particular :
   * - It doesn't check status of the {@link ch.ge.ve.bo.repository.entity.Operation}.
   * - It doesn't update configuration or voting material modification dates of the {@link
   * ch.ge.ve.bo.repository.entity.Operation}
   * - It doesn't schedule a consistency check
   */
  <U extends OperationHolder, T> Optional<T> doTechnicalUpdate(Supplier<Optional<U>> operationHolderSupplier,
                                                               Function<U, T> updater);


  /**
   * Same as {@link #doTechnicalUpdate(Supplier, Function)}
   */
  <U extends OperationHolder, T> T doTechnicalNotOptionalUpdate(Supplier<U> operationHolderSupplier,
                                                                Function<U, T> updater);


  /**
   * Same as {@link #doTechnicalUpdate(Supplier, Function)} but returns a void
   */
  <U extends OperationHolder> void doTechnicalUpdateAndForget(Supplier<Optional<U>> operationHolderSupplier,
                                                              Consumer<U> updater);


  /**
   * Same as {@link #doTechnicalUpdate(Supplier, Function)}
   */
  <U extends OperationHolder> void doTechnicalNotOptionalUpdateAndForget(Supplier<U> operationHolderSupplier,
                                                                         Consumer<U> updater);

  /**
   * Do an update of an {@link OperationHolder} and ensure that :
   * - User as the right to read the {@link ch.ge.ve.bo.repository.entity.Operation}
   * - The status of the operation doesn't prevent this modification
   * - It updates configuration or voting material modification dates of the {@link
   * ch.ge.ve.bo.repository.entity.Operation}
   * - A schedule on consistency is done
   */
  <U extends OperationHolder, T> Optional<T> doUpdate(Supplier<Optional<U>> operationHolder,
                                                      Function<U, T> updater);

  /**
   * Same as {@link #doUpdate(Supplier, Function)}
   */
  <U extends OperationHolder, T> T doUpdateOperationHolder(U operationHolder, Function<U, T> updater);

  /**
   * Same as {@link #doUpdate(Supplier, Function)}
   */
  <U extends OperationHolder, T> T doNotOptionalUpdate(Supplier<U> operationHolder, Function<U, T> updater);
}
