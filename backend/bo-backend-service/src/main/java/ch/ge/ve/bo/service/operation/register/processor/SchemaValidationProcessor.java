/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.processor;

import ch.ge.ve.bo.service.model.SchemaValidationError;
import ch.ge.ve.bo.service.operation.register.RegisterImportProcessor;
import ch.ge.ve.bo.service.utils.exception.SchemaValidationException;
import ch.ge.ve.bo.service.utils.xml.SimpleErrorHandler;
import ch.ge.ve.bo.service.utils.xml.XSDValidator;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.Executor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Processor responsible of validating xsd schema
 */
public class SchemaValidationProcessor extends RegisterImportProcessor {
  private static final Logger logger = LoggerFactory.getLogger(SchemaValidationProcessor.class);

  private final RandomStopInputStream randomStopInputStream;
  private final XSDValidator          xsdValidator;
  private final SimpleErrorHandler errorHandler = new SimpleErrorHandler();


  /**
   * Default constructor
   */
  public SchemaValidationProcessor(InputStream streamToProcess, XSDValidator xsdValidator, Executor executor) {
    super(streamToProcess, executor);
    randomStopInputStream = new RandomStopInputStream(streamToProcess);
    this.xsdValidator = xsdValidator;
  }

  @Override
  protected void stopProcessing() {
    super.stopProcessing();
    logger.info("Schema validation has been stopped.");
    randomStopInputStream.stop();
  }

  @Override
  protected void process() throws SchemaValidationException {
    xsdValidator.validate(randomStopInputStream, errorHandler);
  }

  public List<SchemaValidationError> getErrors() {
    return errorHandler.getErrors();
  }
}
