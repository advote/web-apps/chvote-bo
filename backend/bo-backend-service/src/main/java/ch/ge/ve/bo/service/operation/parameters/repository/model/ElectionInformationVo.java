/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository.model;

import ch.ge.ve.interfaces.ech.eCH0155.v4.CandidatePositionInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ListType;
import java.util.List;

/**
 * Election information extracted from a registry file
 */
public class ElectionInformationVo {
  private static final Integer                         NO_CANDIDATE   = null;
  private static final Integer                         NO_CANDIDATURE = null;
  private static final List<ElectionInformationListVo> NO_LIST        = null;

  private final List<ElectionInformationListVo> lists;
  private final TypeOfElection                  typeOfElection;
  private final String                          doiId;
  private final String                          ballot;
  private final Integer                         numberOfMandates;
  private final String                          mandate;
  private final Integer                         candidateCount;
  private final Integer                         candidatureCount;
  private final Integer                         listCount;

  public enum TypeOfElection {
    MAJORITY_WITH_LIST, MAJORITY, PROPORTIONAL
  }

  @SuppressWarnings("squid:S00107")
  private ElectionInformationVo(
      String doiId, TypeOfElection typeOfElection, String ballot, Integer numberOfMandates,
      String mandate,
      Integer candidateCount, Integer candidatureCount, Integer listCount, List<ElectionInformationListVo> lists) {
    this.doiId = doiId;
    this.typeOfElection = typeOfElection;
    this.ballot = ballot;
    this.numberOfMandates = numberOfMandates;
    this.mandate = mandate;
    this.candidateCount = candidateCount;
    this.candidatureCount = candidatureCount;
    this.listCount = listCount;
    this.lists = lists;
  }

  /**
   * Constructor for a proportional election
   */
  public static ElectionInformationVo forProportional(String doiId, String ballot, Integer
      numberOfMandates, String mandate, List<ElectionInformationListVo> lists) {
    return new ElectionInformationVo(
        doiId, TypeOfElection.PROPORTIONAL, ballot, numberOfMandates, mandate, NO_CANDIDATE,
        lists.stream().mapToInt(ElectionInformationListVo::getCandidateCount).sum(), lists.size(), lists);
  }

  /**
   * Constructor for a majority election
   */
  public static ElectionInformationVo forMajority(
      String doiId, String ballot, Integer numberOfMandates, String mandate, int candidateCount) {
    return new ElectionInformationVo(
        doiId, TypeOfElection.MAJORITY, ballot, numberOfMandates, mandate, candidateCount,
        NO_CANDIDATURE, 0, NO_LIST);
  }


  /**
   * Constructor for a majority election with list
   */
  public static ElectionInformationVo forMajorityWithList(
      String doiId, String ballot, Integer numberOfMandates, String mandate,
      List<ElectionInformationListVo> lists) {
    return new ElectionInformationVo(
        doiId, TypeOfElection.MAJORITY_WITH_LIST, ballot, numberOfMandates, mandate, NO_CANDIDATE,
        lists.stream().mapToInt(ElectionInformationListVo::getCandidateCount).sum(), lists.size(), lists);
  }

  public TypeOfElection getTypeOfElection() {
    return typeOfElection;
  }

  public String getBallot() {
    return ballot;
  }

  public Integer getNumberOfMandates() {
    return numberOfMandates;
  }

  public String getMandate() {
    return mandate;
  }

  public Integer getCandidateCount() {
    return candidateCount;
  }

  public String getDoiId() {
    return doiId;
  }

  public Integer getCandidatureCount() {
    return candidatureCount;
  }

  public Integer getListCount() {
    return listCount;
  }

  public List<ElectionInformationListVo> getLists() {
    return lists;
  }

  public static class ElectionInformationListVo {
    private final String  indentureNumber;
    private final String  name;
    private final int     candidateCount;
    private final boolean inAnUnion;
    private final boolean isEmptyList;

    private ElectionInformationListVo() {
      indentureNumber = null;
      name = null;
      candidateCount = 0;
      inAnUnion = false;
      isEmptyList = true;
    }

    /**
     * An election list
     */
    public ElectionInformationListVo(ListType list, boolean inAnUnion) {
      indentureNumber = list.getListIndentureNumber();
      name = list.getListDescription().getListDescriptionInfo().get(0).getListDescriptionShort();
      candidateCount = (int) list.getCandidatePosition()
                                 .stream()
                                 .map(CandidatePositionInformationType::getCandidateIdentification)
                                 .distinct()
                                 .count();
      this.inAnUnion = inAnUnion;
      this.isEmptyList = false;
    }

    public String getIndentureNumber() {
      return indentureNumber;
    }

    public String getName() {
      return name;
    }

    public boolean isInAnUnion() {
      return inAnUnion;
    }

    public Integer getCandidateCount() {
      return candidateCount;
    }

    public boolean isEmptyList() {
      return isEmptyList;
    }
  }
}
