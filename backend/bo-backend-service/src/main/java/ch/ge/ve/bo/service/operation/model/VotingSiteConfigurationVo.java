/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.entity.VotingSiteConfiguration;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;

/**
 * Value object representing a voting site specific configuration.
 */
public class VotingSiteConfigurationVo {
  private final Language      defaultLanguage;
  private final Set<Language> allLanguages;

  /**
   * default constructor
   */
  @JsonCreator
  public VotingSiteConfigurationVo(
      @JsonProperty("defaultLanguage") Language defaultLanguage,
      @JsonProperty("allLanguages") Set<Language> allLanguages) {
    this.defaultLanguage = defaultLanguage;
    this.allLanguages = allLanguages;
  }

  /**
   * Constructor using entity
   */
  public VotingSiteConfigurationVo(VotingSiteConfiguration configuration) {
    this.defaultLanguage = configuration.getDefaultLanguage();
    this.allLanguages = configuration.getAllLanguages();
  }

  public Language getDefaultLanguage() {
    return defaultLanguage;
  }

  public Set<Language> getAllLanguages() {
    return allLanguages;
  }
}
