/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.model.VoterTestingCardsLotVo;
import ch.ge.ve.bo.service.operation.consistency.AbstractConsistencyChecker;
import ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorType;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
/**
 * check to verify all doi declared in voting cards are in operation repository
 */
public class AllDoiDeclaredInVotingCardsShouldBeInOperationRepository extends AbstractConsistencyChecker {

  private final CheckType                   checkType;
  private final OperationRepositoryService  repositoryService;
  private final VoterTestingCardsLotService voterTestingCardsLotService;
  /**
   * Default constructor
   */
  public AllDoiDeclaredInVotingCardsShouldBeInOperationRepository(
      CheckType checkType,
      OperationRepositoryService repositoryService,
      VoterTestingCardsLotService voterTestingCardsLotService) {
    this.checkType = checkType;
    this.repositoryService = repositoryService;
    this.voterTestingCardsLotService = voterTestingCardsLotService;
  }

  @Override
  public CheckType getCheckType() {
    return checkType;
  }


  @Override
  public ConsistencyResultVo checkForOperation(Operation operation) {
    Set<String> knownDoi = repositoryService.getAllDomainOfInfluenceForOperation(operation.getId()).stream()
                                            .map(domainOfInfluenceVo -> domainOfInfluenceVo.id).collect(
            Collectors.toSet());


    List<VoterTestingCardsLotVo> testingCards =
        voterTestingCardsLotService.findAll(operation.getId(), CheckType.CONFIGURATION == getCheckType());
    ConsistencyResultVo consistencyResultVo = new ConsistencyResultVo();
    testingCards.forEach(
        testingCard ->
            testingCard.doi
                .stream()
                .filter(doi -> !knownDoi.contains(doi))
                .forEach(unknownDoi -> consistencyResultVo.addError(
                    getError(),
                    ConsistencyResultVo.param("lotName", testingCard.lotName),
                    ConsistencyResultVo.param("unknownDoi", unknownDoi))));
    return consistencyResultVo;
  }


  private ConsistencyErrorType getError() {
    return getCheckType() == CheckType.VOTING_MATERIAL ?
        ConsistencyErrorType.VM_UNKNOWN_DOI_FOR_TESTING_CARD_LOT :
        ConsistencyErrorType.CONF_UNKNOWN_DOI_FOR_TESTING_CARD_LOT;
  }

}
