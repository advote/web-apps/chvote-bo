/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.model.FileVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.VotationRepositoryFileVo;
import ch.ge.ve.bo.service.operation.parameters.repository.model.VoteInformationVo;
import ch.ge.ve.bo.service.utils.xml.XSDValidator;
import ch.ge.ve.interfaces.ech.eCH0058.v5.HeaderType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.AnswerInformationType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.BallotType;
import ch.ge.ve.interfaces.ech.eCH0155.v4.ContestType;
import ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery;
import ch.ge.ve.interfaces.ech.eCH0159.v4.EventInitialDelivery;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * {@link OperationRepositoryServiceHandler} for votation
 */
public class OperationRepositoryServiceVotationHandler extends OperationRepositoryServiceHandler<Delivery,
    VotationRepositoryFileVo> {

  /**
   * Default constructor
   */
  public OperationRepositoryServiceVotationHandler(XSDValidator xsdValidator, Function<InputStream, Delivery>
      deliveryBuilder, FileService fileService) {
    super(xsdValidator, deliveryBuilder, fileService);
  }

  @Override
  HeaderType getHeader(Delivery delivery) {
    return delivery.getDeliveryHeader();
  }

  @Override
  ContestType getContest(Delivery delivery) {
    return delivery.getInitialDelivery().getContest();
  }

  @Override
  VotationRepositoryFileVo buildFrom(Delivery delivery, FileVo file, boolean withContent) {
    List<VoteInformationVo> voteInformations = new ArrayList<>();
    for (EventInitialDelivery.VoteInformation voteInformation : delivery.getInitialDelivery().getVoteInformation()) {
      for (BallotType ballotType : voteInformation.getBallot()) {
        if (ballotType.getStandardBallot() != null) {
          voteInformations.add(createVoteInformation(voteInformation,
                                                     ballotType.getBallotIdentification(),
                                                     ballotType.getStandardBallot().getAnswerInformation(),
                                                     ballotType.getBallotPosition().intValue()));
        } else {

          ballotType
              .getVariantBallot()
              .getQuestionInformation()
              .stream()
              .map(questionInfo -> createVoteInformation(voteInformation,
                                                         questionInfo.getQuestionIdentification(),
                                                         questionInfo.getAnswerInformation(),
                                                         questionInfo.getQuestionPosition().intValue()))
              .forEach(voteInformations::add);
          ballotType.getVariantBallot()
                    .getTieBreakInformation()
                    .stream()
                    .map(questionInfo -> createVoteInformation(voteInformation,
                                                               questionInfo.getQuestionIdentification(),
                                                               questionInfo.getAnswerInformation(),
                                                               questionInfo.getQuestionPosition().intValue()))
                    .forEach(voteInformations::add);
        }
      }
    }

    return new VotationRepositoryFileVo(
        file.id, file.login, file.saveDate, file.businessKey, file.operationId, file.type, file.language, file.fileName,
        file.managementEntity, withContent ? file.fileContent : null,
        getContest(delivery).getContestIdentification(),
        getContest(delivery).getContestDate().atStartOfDay(),
        getContest(delivery).getContestDescription().getContestDescriptionInfo().get(0).getContestDescription(),
        voteInformations
    );
  }

  private VoteInformationVo createVoteInformation(EventInitialDelivery.VoteInformation voteInformation,
                                                  String questionIdentification,
                                                  AnswerInformationType answerType,
                                                  int ballotPosition) {
    return new VoteInformationVo(
        voteInformation.getVote().getVoteIdentification(),
        voteInformation.getVote().getDomainOfInfluenceIdentification(),
        ballotPosition,
        questionIdentification,
        String.format("parameters.repository.edit.detail-grid.answer-type-string.%d",
                      answerType.getAnswerType().intValue()));
  }

  @Override
  VotationRepositoryFileVo buildFrom(FileVo file, boolean withContent) {
    return buildFrom(fromXml(new ByteArrayInputStream(file.fileContent)), file, withContent);
  }

  @Override
  FileType getFileType() {
    return FileType.VOTATION_REPOSITORY;
  }

  @Override
  Set<String> getAllDoiIdentificationInternal(VotationRepositoryFileVo file) {
    return file.getDetails()
               .stream()
               .map(voteInformationVo -> voteInformationVo.domainOfInfluence)
               .collect(Collectors.toSet());
  }

  @Override
  Set<String> getAllBallotsInternal(VotationRepositoryFileVo file) {
    return file.getDetails()
               .stream()
               .map(voteInformationVo -> voteInformationVo.voteIdentification)
               .collect(Collectors.toSet());
  }

}
