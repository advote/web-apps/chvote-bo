/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.model;

import ch.ge.ve.bo.repository.entity.CardType;
import ch.ge.ve.bo.repository.entity.Signature;
import ch.ge.ve.bo.repository.entity.TestingCardLanguage;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Value object for {@link ch.ge.ve.bo.repository.entity.VoterTestingCardsLot}
 */
public class VoterTestingCardsLotVo {

  public final Long      id;
  public final String    lotName;
  public final CardType  cardType;
  public final Integer   count;
  public final Signature signature;

  @NotNull
  @Size(max = 30)
  public final String firstName;

  @NotNull
  @Size(max = 30)
  public final String lastName;

  @NotNull
  public final LocalDateTime birthday;

  @Size(max = 200)
  public final String address1;

  @Size(max = 200)
  public final String address2;

  @NotNull
  @Size(max = 100)
  public final String street;


  @NotNull
  @Size(max = 6)
  public final String postalCode;

  @NotNull
  @Size(max = 50)
  public final String city;

  @NotNull
  @Size(max = 50)
  public final String country;


  @NotNull
  public final TestingCardLanguage language;

  @NotNull
  public final boolean shouldPrint;

  @NotNull
  public final List<String> doi;


  /**
   * Default constructor
   */
  @JsonCreator
  public VoterTestingCardsLotVo(
      @JsonProperty("id") Long id,
      @JsonProperty("lotName") String lotName,
      @JsonProperty("cardType") CardType cardType,
      @JsonProperty("count") Integer count,
      @JsonProperty("signature") Signature signature,
      @JsonProperty("firstName") String firstName,
      @JsonProperty("lastName") String lastName,
      @JsonProperty("birthday") LocalDateTime birthday,
      @JsonProperty("address1") String address1,
      @JsonProperty("address2") String address2,
      @JsonProperty("street") String street,
      @JsonProperty("postalCode") String postalCode,
      @JsonProperty("city") String city,
      @JsonProperty("country") String country,
      @JsonProperty("language") TestingCardLanguage language,
      @JsonProperty("shouldPrint") boolean shouldPrint,
      @JsonProperty("doi") List<String> doi) {
    this.id = id;
    this.lotName = lotName;
    this.cardType = cardType;
    this.count = count;
    this.signature = signature;
    this.firstName = firstName;
    this.lastName = lastName;
    this.birthday = birthday;
    this.address1 = address1;
    this.address2 = address2;
    this.street = street;
    this.postalCode = postalCode;
    this.city = city;
    this.country = country;
    this.language = language;
    this.shouldPrint = shouldPrint;
    this.doi = doi;
  }
}