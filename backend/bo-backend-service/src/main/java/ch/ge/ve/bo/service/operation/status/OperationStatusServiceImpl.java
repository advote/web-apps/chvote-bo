/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status;

import static ch.ge.ve.bo.service.operation.model.ModificationMode.FULLY_MODIFIABLE;
import static ch.ge.ve.bo.service.operation.model.ModificationMode.IN_FULL_MODIFICATION;
import static ch.ge.ve.bo.service.operation.model.ModificationMode.IN_PARTIAL_MODIFICATION;
import static ch.ge.ve.bo.service.operation.model.ModificationMode.PARTIALLY_MODIFIABLE;

import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.DeploymentTarget;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo;
import ch.ge.ve.bo.service.operation.model.ModificationMode;
import ch.ge.ve.bo.service.operation.model.OperationStatusVo;
import ch.ge.ve.bo.service.operation.model.TallyArchiveStatusVo;
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo;
import ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo;

/**
 * Service responsible of aggregating  Configuration, Voting Material, Voting Period and tally status
 */
public class OperationStatusServiceImpl implements OperationStatusService {

  private final SecuredOperationRepository           securedOperationRepository;
  private final ReadonlyStatusService                readonlyStatusService;
  private final OperationStatusTallyHandler          operationStatusTallyHandler;
  private final OperationStatusConfigurationHandler  configurationHandler;
  private final OperationStatusVotingMaterialHandler votingMaterialHandler;
  private final OperationStatusVotingPeriodHandler   votingPeriodHandler;

  /**
   * Default constructor
   */
  @SuppressWarnings("squid:S00107")
  public OperationStatusServiceImpl(
      SecuredOperationRepository securedOperationRepository,
      ReadonlyStatusService readonlyStatusService,
      OperationStatusTallyHandler operationStatusTallyHandler,
      OperationStatusConfigurationHandler configurationHandler,
      OperationStatusVotingMaterialHandler votingMaterialHandler,
      OperationStatusVotingPeriodHandler votingPeriodHandler) {
    this.securedOperationRepository = securedOperationRepository;
    this.operationStatusTallyHandler = operationStatusTallyHandler;
    this.readonlyStatusService = readonlyStatusService;
    this.configurationHandler = configurationHandler;
    this.votingMaterialHandler = votingMaterialHandler;
    this.votingPeriodHandler = votingPeriodHandler;
  }

  @Override
  public OperationStatusVo getStatus(long operationId) throws PactRequestException {
    return getStatus(securedOperationRepository.findOne(operationId, false));
  }

  private OperationStatusVo getStatus(Operation operation) throws PactRequestException {
    ConfigurationStatusVo.Builder configurationStatusBuilder = configurationHandler.getConfigurationStatusVo(operation);


    VotingMaterialStatusVo votingMaterialStatus = votingMaterialHandler.getVotingMaterialStatusVo(operation);
    VotingPeriodStatusVo votingPeriodStatus = votingPeriodHandler.getVotingPeriodStatusVo(operation);

    TallyArchiveStatusVo tallyArchiveStatus = operationStatusTallyHandler
        .getTallyArchiveStatusVo(votingPeriodStatus.state, operation);


    return new OperationStatusVo(
        addModificationModeToConfigurationStatus(
            operation, configurationStatusBuilder, votingMaterialStatus, votingPeriodStatus),
        votingMaterialStatus,
        votingPeriodStatus,
        tallyArchiveStatus,
        operation.getSimulationName(),
        operation.getDeploymentTarget());
  }

  private ConfigurationStatusVo addModificationModeToConfigurationStatus(
      Operation operation, ConfigurationStatusVo.Builder builder,
      VotingMaterialStatusVo votingMaterialStatus, VotingPeriodStatusVo votingPeriodStatus) {
    ConfigurationStatusVo oldStatus = builder.build();
    var newState = oldStatus.getState();

    var modificationMode = getModificationMode(operation, votingMaterialStatus, oldStatus, votingPeriodStatus);

    if (isShouldOverrideStatus(oldStatus, modificationMode)) {
      newState = OperationStatusVo.isComplete(oldStatus.getCompletedSections()) ?
          ConfigurationStatusVo.State.COMPLETE : ConfigurationStatusVo.State.INCOMPLETE;
      builder.setState(newState,
                       operation.getLastConfigurationUpdateDate(),
                       operation.getLogin()
      );
    }

    builder.setModificationMode(modificationMode);
    builder.setReadOnlySections(
        readonlyStatusService.getReadOnlySectionsForConfiguration(operation, modificationMode, newState));
    return builder.build();

  }

  private boolean isShouldOverrideStatus(ConfigurationStatusVo configurationStatus, ModificationMode modificationMode) {
    return configurationStatus.getState() == ConfigurationStatusVo.State.DEPLOYED &&
           (modificationMode == ModificationMode.IN_FULL_MODIFICATION ||
            modificationMode == ModificationMode.IN_PARTIAL_MODIFICATION);
  }

  @Override
  public boolean isConfigurationInReadonly(long id) throws PactRequestException {
    return getStatus(id).getConfigurationStatus().getState().isReadOnly();
  }

  @Override
  public ModificationMode getModificationMode(Operation operation) throws PactRequestException {
    return getModificationMode(operation,
                               votingMaterialHandler.getVotingMaterialStatusVo(operation),
                               configurationHandler.getConfigurationStatusVo(operation).build(),
                               votingPeriodHandler.getVotingPeriodStatusVo(operation));
  }

  private ModificationMode getModificationMode(Operation operation,
                                               VotingMaterialStatusVo vmStatus,
                                               ConfigurationStatusVo confStatus,
                                               VotingPeriodStatusVo vpStatus) {

    if (vpStatus.state == VotingPeriodStatusVo.State.CLOSED) {
      return ModificationMode.NOT_MODIFIABLE;
    }

    if (operation.getDeploymentTarget() == DeploymentTarget.NOT_DEFINED) {
      return ModificationMode.NOT_MODIFIABLE;
    }

    boolean configurationInModification = operation.isInModification() ||
                                          confStatus.getState() != ConfigurationStatusVo.State.DEPLOYED;
    switch (vmStatus.getState()) {
      case INCOMPLETE:
      case COMPLETE:
      case AVAILABLE_FOR_CREATION:
      case CREATION_REJECTED:
      case CREATION_FAILED:
      case INVALIDATED:
        return configurationInModification ? IN_FULL_MODIFICATION : FULLY_MODIFIABLE;
      case CREATION_REQUESTED:
      case CREATION_IN_PROGRESS:
      case CREATED:
      case INVALIDATION_REJECTED:
      case VALIDATED:
        return configurationInModification ? IN_PARTIAL_MODIFICATION : PARTIALLY_MODIFIABLE;
      default:
        return ModificationMode.NOT_MODIFIABLE;
    }
  }

}
