/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload;

import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to upload operation part to pact
 */
public interface OperationUploadService {

  /**
   * Upload a operation's configuration identified by its unique ID to PACT .
   *
   * @param operationId the operation's ID
   */
  void uploadConfigurationToPact(long operationId) throws BusinessException;


  /**
   * Upload a operation's votingMaterial identified by its unique ID to PACT .
   *
   * @param operationId the operation's ID
   */
  void uploadVotingMaterialToPact(long operationId) throws PactRequestException;


  /**
   * Upload a operation's votingPeriod identified by its unique ID to PACT .
   *
   * @param operationId the operation's ID
   */
  @Transactional(readOnly = true)
  void uploadVotingPeriodToPact(long operationId) throws PactRequestException;
}
