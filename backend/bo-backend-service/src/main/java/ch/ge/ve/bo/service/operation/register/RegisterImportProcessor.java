/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.service.exception.BusinessException;
import ch.ge.ve.bo.service.exception.TechnicalException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Executor;

/**
 * Abstract class for input stream processor
 */
public abstract class RegisterImportProcessor {

  private         boolean                  isCancelled;
  private         RegisterImportHypervisor hypervisor;
  private final   Executor                 executor;
  protected final InputStream              streamToProcess;

  protected RegisterImportProcessor(InputStream streamToProcess, Executor executor) {
    this.streamToProcess = streamToProcess;
    this.executor = executor;
  }


  public void setHypervisor(RegisterImportHypervisor registryImportHypervisor) {
    this.hypervisor = registryImportHypervisor;
  }

  /**
   * Start processor
   */
  public void run() {
    ConnectedUser connectedUser = ConnectedUser.get();
    executor.execute(() -> {
      try {
        Thread.currentThread().setName("Processor :" + this.getClass().getSimpleName());
        ConnectedUser.set(connectedUser);
        process();
      } catch (Exception t) {
        hypervisor.stopOthersFrom(this, t);
      } finally {
        hypervisor.processFinish();
        closeStream();
      }
    });
  }

  private void closeStream() {
    try {
      streamToProcess.close();
    } catch (IOException e) {
      throw new TechnicalException(e);
    }

  }

  protected abstract void process() throws BusinessException;

  protected boolean isCancelled() {
    return isCancelled;
  }

  protected void stopProcessing() {
    isCancelled = true;
  }

}
