/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency;

import ch.ge.ve.bo.repository.ConsistencyCheckCallback;

/**
 * Service used to schedule consistency check
 */
public interface ConsistencyScheduleService {

  /**
   * This method will schedule a consistency check on an operation.
   * It should be called whenever an update has been done that may
   * impact operation
   * @param operationId operation id
   */
  void scheduleCheck(long operationId);


  /**
   * create a future for a consistency check
   */
  ConsistencyCheckCallback asCallback(long operationId);
}
