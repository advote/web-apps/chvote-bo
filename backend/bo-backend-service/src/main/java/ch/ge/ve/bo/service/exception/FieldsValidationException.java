/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.exception;

import java.util.Map;

/**
 * Root of fields validation error exceptions
 */
public class FieldsValidationException extends BusinessException {

  private final Map<String, String>   fieldsErrorMessages;
  private final Map<String, String[]> fieldsErrorParameters;

  /**
   * default constructor with no parameters on fields messages
   */
  public FieldsValidationException(Map<String, String> fieldsErrorMessages) {
    super();
    this.fieldsErrorMessages = fieldsErrorMessages;
    this.fieldsErrorParameters = null;
  }

  /**
   * default constructor
   */
  public FieldsValidationException(Map<String, String> fieldsErrorMessages, Map<String, String[]>
      fieldsErrorParameters) {
    super();
    this.fieldsErrorMessages = fieldsErrorMessages;
    this.fieldsErrorParameters = fieldsErrorParameters;
  }

  public Map<String, String> getFieldsErrorMessages() {
    return fieldsErrorMessages;
  }

  public Map<String, String[]> getFieldsErrorParameters() {
    return fieldsErrorParameters;
  }
}
