/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks;

import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorType.BALLOT_IN_DOCUMENTATION_NOT_IN_OPERATION_REPOSITORY;
import static ch.ge.ve.bo.service.operation.model.ConsistencyResultVo.param;

import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.operation.consistency.AbstractConsistencyChecker;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import ch.ge.ve.bo.service.operation.parameters.document.BallotDocumentationService;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import java.util.Set;

/**
 * Check to verify that all ballots in documentation ard defined in operation repository
 */
public class AllBallotsInDocumentationShouldBeInOperationRepository extends AbstractConsistencyChecker {
  private final OperationRepositoryService repositoryService;
  private final BallotDocumentationService ballotDocumentationService;

  /**
   * Default constructor
   */
  public AllBallotsInDocumentationShouldBeInOperationRepository(OperationRepositoryService repositoryService,
                                                                BallotDocumentationService ballotDocumentationService) {
    this.repositoryService = repositoryService;
    this.ballotDocumentationService = ballotDocumentationService;
  }

  @Override
  public CheckType getCheckType() {
    return CheckType.CONFIGURATION;
  }

  @Override
  public ConsistencyResultVo checkForOperation(Operation operation) {
    Set<String> operationBallots = repositoryService.getAllBallotsForOperation(operation.getId(), true, true);
    ConsistencyResultVo results = new ConsistencyResultVo();
    ballotDocumentationService.findByOperation(operation.getId())
                              .stream()
                              .filter(bd -> !operationBallots.contains(bd.getBallotId()))
                              .forEach(bd -> results.addError(BALLOT_IN_DOCUMENTATION_NOT_IN_OPERATION_REPOSITORY,
                                                              param("ballot", bd.getBallotId())));
    return results;
  }
}
