/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Input stream generator designed to be processed in parallel
 */
public class BufferedTeeStream {
  private final InputStream                  inputStream;
  private       byte[]                       buffer;
  private       int                          actualBufferLength;
  private       AtomicInteger                nbToWait      = new AtomicInteger(0);
  private final List<MultiStreamInputStream> listeners     = new LinkedList<>();
  private       ReadWriteLock                readWriteLock = new ReentrantReadWriteLock();

  /**
   * Default constructor
   */
  public BufferedTeeStream(InputStream inputStream, int bufferSize) {
    this.inputStream = inputStream;
    buffer = new byte[bufferSize];
  }

  /**
   * Create a new input stream from original one
   */
  public InputStream createInputStream() {
    MultiStreamInputStream listener = new MultiStreamInputStream();
    listeners.add(listener);
    return listener;
  }

  private class MultiStreamInputStream extends InputStream {
    private int                        positionInBuffer = 0;
    private CompletableFuture<Integer> waitingRead;

    @Override
    public int read() throws IOException {
      readWriteLock.readLock().lock();
      try {
        if (canReadFromBuffer()) {
          return readFromBuffer();
        }
      } finally {
        readWriteLock.readLock().unlock();
      }

      return readAfterBufferGetUpdate();
    }

    private void readFromOrigin() {
      try {
        actualBufferLength = inputStream.read(buffer);
      } catch (Exception e) {
        nbToWait.set(0);
        listeners.forEach(waitingStream -> waitingStream.releaseOnError(e));
      }
      nbToWait.set(0);
      listeners.forEach(MultiStreamInputStream::release);
    }

    private int readAfterBufferGetUpdate() throws IOException {
      readWriteLock.writeLock().lock();
      try {
        this.waitingRead = new CompletableFuture<>();
        if (nbToWait.incrementAndGet() == listeners.size()) {
          readFromOrigin();
        }
      } finally {
        readWriteLock.writeLock().unlock();
      }

      try {
        return waitingRead.get();
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
        throw new IOException(e);
      } catch (ExecutionException e) {
        throw new IOException(e);
      }
    }

    private void releaseOnError(Exception e) {
      waitingRead.completeExceptionally(e);
    }


    private boolean canReadFromBuffer() {
      return positionInBuffer < actualBufferLength || actualBufferLength == -1;
    }

    private int readFromBuffer() {
      if (actualBufferLength == -1) {
        return -1;
      }

      return buffer[positionInBuffer++];
    }

    private void release() {
      positionInBuffer = 0;
      waitingRead.complete(readFromBuffer());
    }

    @Override
    public void close() throws IOException {
      readWriteLock.writeLock().lock();
      listeners.remove(this);
      if (listeners.isEmpty()) {
        inputStream.close();
      } else if (nbToWait.get() == listeners.size()) {
        readFromOrigin();
      }
      readWriteLock.writeLock().unlock();
    }
  }
}
