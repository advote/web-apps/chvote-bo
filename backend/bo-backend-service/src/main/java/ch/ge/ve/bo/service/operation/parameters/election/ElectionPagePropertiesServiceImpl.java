/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.election;

import ch.ge.ve.bo.repository.ElectionPagePropertiesModelRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.ElectionPageProperties;
import ch.ge.ve.bo.repository.entity.ElectionPagePropertiesModel;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.model.ElectionPagePropertiesModelVo;
import com.google.common.collect.ImmutableSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

/**
 * Default implementation of {@link ElectionPagePropertiesService}
 */
public class ElectionPagePropertiesServiceImpl implements ElectionPagePropertiesService {

  public static final String MODEL = "model ";
  private final ElectionPagePropertiesModelRepository modelRepository;
  private final SecuredOperationRepository            operationRepository;
  private final SecuredOperationHolderService         securedService;

  /**
   * Default constructor
   */
  public ElectionPagePropertiesServiceImpl(ElectionPagePropertiesModelRepository modelRepository,
                                           SecuredOperationRepository operationRepository,
                                           SecuredOperationHolderService securedService) {
    this.modelRepository = modelRepository;
    this.operationRepository = operationRepository;
    this.securedService = securedService;
  }


  @Override
  public List<ElectionPagePropertiesModelVo> getModelsForOperation(long operationId) {
    return securedService
        .safeReadAll(() -> modelRepository.findAllByOperationId(operationId))
        .stream()
        .map(ElectionPagePropertiesModelVo::new)
        .collect(Collectors.toList());
  }

  @Override
  @Transactional
  public ElectionPagePropertiesModelVo saveOrUpdateModel(long operationId, ElectionPagePropertiesModelVo model) {
    return securedService
        .doNotOptionalUpdate(() -> {
                    ElectionPagePropertiesModel modelToSave;
                    if (model.getId() == 0L) {
                      modelToSave = new ElectionPagePropertiesModel();
                      modelToSave.setOperation(operationRepository.findOne(operationId, true));
                    } else {
                      modelToSave = modelRepository
                          .findById(model.getId())
                          .orElseThrow(() -> new EntityNotFoundException(MODEL + model.getId()));
                    }
                    model.updateEntity(modelToSave);
                    return modelToSave;
                  }, modelToSave -> new ElectionPagePropertiesModelVo(modelRepository.save(modelToSave))
        );
  }

  @Override
  @Transactional
  public void deleteModel(long modelId) {
    securedService.doNotOptionalUpdateAndForget(
        () -> {
          ElectionPagePropertiesModel model = modelRepository
              .findById(modelId)
              .orElseThrow(() -> new EntityNotFoundException("model " + modelId));
          if (!model.getMappings().isEmpty()) {
            throw new TechnicalException(
                "Cannot remove model identified by " + modelId + " since it is used by a mapping");
          }
          return model;
        },
        modelRepository::delete);
  }

  @Override
  @Transactional
  public void associateModelToBallots(Long operationId, long modelId, String[] ballots) {
    Set<String> ballotSet = ImmutableSet.copyOf(ballots);

    // Clean all previous associations for those ballots
    securedService
        .safeReadAll(() -> modelRepository.findAllByOperationId(operationId))
        .forEach(
            modelToClean -> securedService
                .doUpdateOperationHolderAndForget(
                    modelToClean, u -> {
                      u.getMappings().removeIf(e -> ballotSet.contains(e.getBallot()));
                      modelRepository.save(u);
                    }));


    // Add association to this ballots
    securedService.doUpdate(
        () -> modelRepository.findById(modelId),
        model -> {
          model.getMappings().addAll(Stream.of(ballots).map(ballot -> {
            ElectionPageProperties props = new ElectionPageProperties();
            props.setBallot(ballot);
            return props;
          }).collect(Collectors.toList()));
          return modelRepository.save(model);
        }).orElseThrow(() -> new EntityNotFoundException("model " + modelId));
  }

  @Override
  @Transactional
  public Map<String, Long> getBallotToModelIdMapping(Long operationId) {
    return getBallotToModelMapping(operationId)
        .entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, o -> o.getValue().getId()));
  }

  @Override
  @Transactional
  public Map<String, ElectionPagePropertiesModelVo> getBallotToModelMapping(Long operationId) {
    Map<String, ElectionPagePropertiesModelVo> map = new HashMap<>();
    securedService
        .safeReadAll(() -> modelRepository.findAllByOperationId(operationId))
        .forEach(model -> model
            .getMappings()
            .forEach(mapping -> map.put(mapping.getBallot(), new ElectionPagePropertiesModelVo(model))));
    return map;
  }


  @Override
  @Transactional
  public void removeMapping(long operationId, String ballot) {
    securedService
        .safeReadAll(() -> modelRepository.findAllByOperationId(operationId))
        .forEach(
            model -> securedService.doNotOptionalUpdateAndForget(
                () -> model,
                m -> {
                  if (model.getMappings().removeIf(mapping -> mapping.getBallot().equals(ballot))) {
                    modelRepository.save(model);
                  }
                }
            )
    );
  }

  @Override
  public ElectionPagePropertiesModelVo getModel(Long modelId) {
    return securedService.safeRead(() -> modelRepository.findById(modelId))
                         .map(ElectionPagePropertiesModelVo::new)
                         .orElseThrow(() -> new EntityNotFoundException("model " + modelId));
  }
}
