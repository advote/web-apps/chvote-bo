/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.repository.BallotDocumentationRepository;
import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.BallotDocumentation;
import ch.ge.ve.bo.service.model.ContentAndFileName;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.model.BallotDocumentationVo;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

/**
 * Default implementation of {@link BallotDocumentationService}
 */
@Transactional
public class BallotDocumentationServiceImpl implements BallotDocumentationService {


  private final SecuredOperationHolderService securedOperationHolderService;
  private final SecuredOperationRepository    securedOperationRepository;
  private final BallotDocumentationRepository ballotDocumentationRepository;

  /**
   * Default constructor
   */
  public BallotDocumentationServiceImpl(SecuredOperationHolderService securedOperationHolderService,
                                        SecuredOperationRepository securedOperationRepository,
                                        BallotDocumentationRepository ballotDocumentationRepository) {
    this.securedOperationHolderService = securedOperationHolderService;
    this.securedOperationRepository = securedOperationRepository;
    this.ballotDocumentationRepository = ballotDocumentationRepository;
  }

  @Override
  public BallotDocumentationVo addBallotDocumentation(Long operationId, String ballotId, Language language, String
      localizedLabel, String filename, byte[] fileContent) {


    BallotDocumentation ballotDocumentation = new BallotDocumentation();
    ballotDocumentation.setBallotId(ballotId);
    ballotDocumentation.setFileContent(fileContent);
    ballotDocumentation.setFileName(filename);
    ballotDocumentation.setLanguage(language);
    ballotDocumentation.setLocalizedLabel(localizedLabel);
    ballotDocumentation.setManagementEntity(ConnectedUser.get().managementEntity);
    ballotDocumentation.setOperation(securedOperationRepository.findOne(operationId, false));

    return securedOperationHolderService.doNotOptionalUpdate(
        () -> ballotDocumentation,
        bd -> new BallotDocumentationVo(ballotDocumentationRepository.save(bd))
    );
  }

  @Override
  public void deleteBallotDocumentation(Long ballotDocumentationId) {
    securedOperationHolderService.doNotOptionalUpdateAndForget(
        () -> ballotDocumentationRepository
            .findById(ballotDocumentationId)
            .orElseThrow(() -> new EntityNotFoundException("BallotDocumentation " + ballotDocumentationId)),
        ballotDocumentationRepository::delete);
  }

  @Override
  public List<BallotDocumentationVo> findByOperation(Long operationId) {
    List<BallotDocumentation> bd = securedOperationHolderService.safeReadAll(
        () -> ballotDocumentationRepository.findByOperationId(operationId));
    return bd.stream().map(BallotDocumentationVo::new).collect(Collectors.toList());
  }

  @Override
  public ContentAndFileName getFileContent(Long ballotDocumentationId) {
    BallotDocumentation bd =
        securedOperationHolderService
            .safeRead(() -> ballotDocumentationRepository.findById(ballotDocumentationId))
            .orElseThrow(() -> new EntityNotFoundException("BallotDocumentation " + ballotDocumentationId));
    return new ContentAndFileName(bd.getFileName(), bd.getFileContent());
  }


}
