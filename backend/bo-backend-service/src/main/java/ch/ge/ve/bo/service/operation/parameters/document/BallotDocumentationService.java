/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document;

import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.service.model.ContentAndFileName;
import ch.ge.ve.bo.service.operation.model.BallotDocumentationVo;
import java.util.List;

/**
 * Service related to ballot documentation
 */
public interface BallotDocumentationService {
  /**
   * Add a {@link BallotDocumentationVo} to an operation
   */
  BallotDocumentationVo addBallotDocumentation(
      Long operationId,
      String ballotId,
      Language language,
      String localizedLabel,
      String filename,
      byte[] fileContent);

  /**
   * delete a {@link BallotDocumentationVo}
   */
  void deleteBallotDocumentation(Long ballotDocumentationId);

  /**
   * @return all {@link BallotDocumentationVo} for an operation
   */
  List<BallotDocumentationVo> findByOperation(Long operationId);

  /**
   * return download information concerning a {@link BallotDocumentationVo}
   */
  ContentAndFileName getFileContent(Long ballotDocumentationId);


}
