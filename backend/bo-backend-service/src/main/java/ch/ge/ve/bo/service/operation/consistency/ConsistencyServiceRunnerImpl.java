/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency;

import ch.ge.ve.bo.repository.ConnectedUser;
import ch.ge.ve.bo.repository.OperationConsistencyRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.OperationConsistency;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo;
import ch.ge.ve.bo.service.operation.model.OperationStatusVo;
import ch.ge.ve.bo.service.operation.status.OperationStatusService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDateTime;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of ConsistencyServiceRunner
 */
public class ConsistencyServiceRunnerImpl implements ConsistencyServiceRunner {
  private static final Logger logger = LoggerFactory.getLogger(ConsistencyServiceRunnerImpl.class);


  private final long                           operationId;
  private final SecuredOperationRepository     operationRepository;
  private final OperationConsistencyRepository operationConsistencyRepository;
  private final ObjectMapper                   objectMapper;
  private final ConsistencyResultVo            consistencyResultVo;
  private final OperationStatusService         operationStatusService;
  private final Collection<ConsistencyChecker> checkerList;

  /**
   * Default constructor
   */
  public ConsistencyServiceRunnerImpl(long operationId,
                                      SecuredOperationRepository operationRepository,
                                      OperationConsistencyRepository operationConsistencyRepository,
                                      ObjectMapper objectMapper,
                                      OperationStatusService operationStatusService,
                                      Collection<ConsistencyChecker> checkerList) {
    this.operationId = operationId;
    this.operationRepository = operationRepository;
    this.operationConsistencyRepository = operationConsistencyRepository;
    this.objectMapper = objectMapper;
    this.operationStatusService = operationStatusService;
    this.checkerList = checkerList;
    this.consistencyResultVo = new ConsistencyResultVo();
  }


  @Override
  @Transactional
  public void run() {
    ConnectedUser.technical();
    try {
      Operation operation = this.operationRepository.findOne(operationId, false);
      LocalDateTime lastUpdateDate = ConsistencyServiceImpl.getOperationLastUpdateDate(operation);
      if (needComputation(lastUpdateDate)) {
        OperationStatusVo status = operationStatusService.getStatus(operationId);
        checkerList.forEach(checker -> consistencyResultVo.concat(checker.checkForOperation(status, operation)));
        storeResults(operation, lastUpdateDate);
      }
    } catch (Exception e) {
      logger
          .error("Could not compute consistency for operation " + operationId + " due to unexpected technical error",
                 e);
      throw new TechnicalException(e);
    } finally {
      ConnectedUser.unset();
    }
  }

  private void storeResults(Operation operation, LocalDateTime computationDate) throws JsonProcessingException {
    operationConsistencyRepository.findByOperationId(operationId).ifPresent(operationConsistency -> {
      operationConsistencyRepository.deleteById(operationConsistency.getId());
      if (operationConsistency.getComputationDate().isAfter(computationDate)) {
        logger.warn("Computation of consistency for operation {} has not been stored " +
                    "since a more recent version exists in the database", operationId);
      }
    });

    OperationConsistency operationConsistency = new OperationConsistency();
    operationConsistency.setOperation(operation);
    operationConsistency.setComputationDate(computationDate);
    operationConsistency.setReport(objectMapper.writeValueAsString(consistencyResultVo));
    operationConsistencyRepository.save(operationConsistency);
  }

  private boolean needComputation(LocalDateTime lastUpdateDate) {
    return operationConsistencyRepository.findByOperationId(operationId)
                                         .map(consistency -> consistency.getComputationDate().isBefore(lastUpdateDate))
                                         .orElse(true);
  }
}
