/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.conf;

import ch.ge.ve.bo.repository.BallotDocumentationRepository;
import ch.ge.ve.bo.repository.DictionaryRepository;
import ch.ge.ve.bo.repository.DownloadTokenRepository;
import ch.ge.ve.bo.repository.ElectionPagePropertiesModelRepository;
import ch.ge.ve.bo.repository.ElectoralAuthorityKeyConfigurationRepository;
import ch.ge.ve.bo.repository.ElectoralAuthorityKeyRepository;
import ch.ge.ve.bo.repository.FileRepository;
import ch.ge.ve.bo.repository.HighlightedQuestionRepository;
import ch.ge.ve.bo.repository.LocalizedHighlightedQuestionRepository;
import ch.ge.ve.bo.repository.OperationConsistencyRepository;
import ch.ge.ve.bo.repository.RegisterMetadataRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.SecuredOperationRepositoryImpl;
import ch.ge.ve.bo.repository.SimulationPeriodConfigurationRepository;
import ch.ge.ve.bo.repository.UnsecuredOperationRepository;
import ch.ge.ve.bo.repository.VoterTestingCardsLotRepository;
import ch.ge.ve.bo.repository.VotingSiteConfigurationRepository;
import ch.ge.ve.bo.repository.conf.RepositoryConfiguration;
import ch.ge.ve.bo.repository.security.SecuredContext;
import ch.ge.ve.bo.service.access.ServiceAccessLoggerAspect;
import ch.ge.ve.bo.service.admin.ElectoralAuthorityKeyService;
import ch.ge.ve.bo.service.admin.ElectoralAuthorityKeyServiceImpl;
import ch.ge.ve.bo.service.dictionnary.DictionaryService;
import ch.ge.ve.bo.service.dictionnary.DictionaryServiceImpl;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.file.FileService;
import ch.ge.ve.bo.service.file.FileServiceImpl;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.operation.OperationService;
import ch.ge.ve.bo.service.operation.OperationServiceImpl;
import ch.ge.ve.bo.service.operation.OperationVotingMaterialService;
import ch.ge.ve.bo.service.operation.OperationVotingMaterialServiceImpl;
import ch.ge.ve.bo.service.operation.PrinterOperationService;
import ch.ge.ve.bo.service.operation.PrinterOperationServiceImpl;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderServiceImpl;
import ch.ge.ve.bo.service.operation.consistency.AbstractConsistencyChecker;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyChecker;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleServiceImpl;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyService;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyServiceImpl;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyServiceRunner;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyServiceRunnerImpl;
import ch.ge.ve.bo.service.operation.consistency.checks.AllBallotsInDocumentationShouldBeInOperationRepository;
import ch.ge.ve.bo.service.operation.consistency.checks.AllBallotsInElectionPagePropertiesShouldBeInOperationRepository;
import ch.ge.ve.bo.service.operation.consistency.checks.AllDoiDeclaredInRepositoryShouldBeInDoiFile;
import ch.ge.ve.bo.service.operation.consistency.checks.AllDoiDeclaredInVotingCardsShouldBeInOperationRepository;
import ch.ge.ve.bo.service.operation.consistency.checks.AllMunicipalitiesShouldHaveAPrinter;
import ch.ge.ve.bo.service.operation.download.token.DownloadTokenService;
import ch.ge.ve.bo.service.operation.download.token.DownloadTokenServiceImpl;
import ch.ge.ve.bo.service.operation.management.entity.ManagementEntityService;
import ch.ge.ve.bo.service.operation.management.entity.ManagementEntityServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.document.BallotDocumentationService;
import ch.ge.ve.bo.service.operation.parameters.document.BallotDocumentationServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.document.HighlightedQuestionService;
import ch.ge.ve.bo.service.operation.parameters.document.HighlightedQuestionServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.document.OperationDocumentService;
import ch.ge.ve.bo.service.operation.parameters.document.OperationDocumentServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceService;
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.election.ElectionPagePropertiesService;
import ch.ge.ve.bo.service.operation.parameters.election.ElectionPagePropertiesServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryServiceElectionHandler;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryServiceImpl;
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryServiceVotationHandler;
import ch.ge.ve.bo.service.operation.parameters.voting.site.configuration.VotingSiteConfigurationServiceImpl;
import ch.ge.ve.bo.service.operation.register.RegisterImportService;
import ch.ge.ve.bo.service.operation.register.RegisterImportServiceImpl;
import ch.ge.ve.bo.service.operation.register.RegisterMetadataService;
import ch.ge.ve.bo.service.operation.register.RegisterMetadataServiceImpl;
import ch.ge.ve.bo.service.operation.register.RegisterReportBuilder;
import ch.ge.ve.bo.service.operation.status.OperationStatusAndConsistencyService;
import ch.ge.ve.bo.service.operation.status.OperationStatusAndConsistencyServiceImpl;
import ch.ge.ve.bo.service.operation.status.OperationStatusConfigurationHandler;
import ch.ge.ve.bo.service.operation.status.OperationStatusHandlerHelper;
import ch.ge.ve.bo.service.operation.status.OperationStatusService;
import ch.ge.ve.bo.service.operation.status.OperationStatusServiceImpl;
import ch.ge.ve.bo.service.operation.status.OperationStatusTallyHandler;
import ch.ge.ve.bo.service.operation.status.OperationStatusVotingMaterialHandler;
import ch.ge.ve.bo.service.operation.status.OperationStatusVotingPeriodHandler;
import ch.ge.ve.bo.service.operation.status.OperationWorkflowService;
import ch.ge.ve.bo.service.operation.status.OperationWorkflowServiceImpl;
import ch.ge.ve.bo.service.operation.status.ReadonlyStatusService;
import ch.ge.ve.bo.service.operation.status.ReadonlyStatusServiceImpl;
import ch.ge.ve.bo.service.operation.status.VotingCardDeliveryService;
import ch.ge.ve.bo.service.operation.status.VotingCardDeliveryServiceImpl;
import ch.ge.ve.bo.service.operation.status.complete.ConfigurationIsCompleteSupplier;
import ch.ge.ve.bo.service.operation.status.complete.VotingMaterialIsCompleteSupplier;
import ch.ge.ve.bo.service.operation.status.complete.VotingPeriodIsCompleteSupplier;
import ch.ge.ve.bo.service.operation.upload.OperationUploadService;
import ch.ge.ve.bo.service.operation.upload.OperationUploadServiceConfigurationHandler;
import ch.ge.ve.bo.service.operation.upload.OperationUploadServiceImpl;
import ch.ge.ve.bo.service.operation.upload.OperationUploadServiceVotingMaterialHandler;
import ch.ge.ve.bo.service.operation.upload.OperationUploadServiceVotingPeriodHandler;
import ch.ge.ve.bo.service.operation.voting.period.ElectoralAuthorityKeyConfigurationService;
import ch.ge.ve.bo.service.operation.voting.period.ElectoralAuthorityKeyConfigurationServiceImpl;
import ch.ge.ve.bo.service.operation.voting.period.VotingSitePeriodService;
import ch.ge.ve.bo.service.operation.voting.period.VotingSitePeriodServiceImpl;
import ch.ge.ve.bo.service.printer.PrinterTemplateService;
import ch.ge.ve.bo.service.printer.PrinterTemplateServiceImpl;
import ch.ge.ve.bo.service.testing.card.TestingCardRegisterGeneratorService;
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService;
import ch.ge.ve.bo.service.testing.card.impl.TestingCardRegisterGeneratorServiceImpl;
import ch.ge.ve.bo.service.testing.card.impl.VoterTestingCardsLotServiceImpl;
import ch.ge.ve.bo.service.utils.xml.XSDValidator;
import ch.ge.ve.interfaces.ech.service.EchCodec;
import ch.ge.ve.interfaces.ech.service.EchDeserializationRuntimeException;
import ch.ge.ve.interfaces.ech.service.JAXBEchCodecImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.security.SecureRandom;
import java.time.Clock;
import java.util.Set;
import java.util.concurrent.Executors;
import net.sf.jasperreports.engine.JRException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Service layer Spring configuration.
 */
@Configuration
@Import({RepositoryConfiguration.class, PactConfiguration.class})
@EnableTransactionManagement
@EnableAspectJAutoProxy
@ComponentScan(basePackages = "ch.ge.ve.bo.service.operation.status.complete")
public class ServiceConfiguration {

  private static final String CATALOG_FILE = "catalog.cat";
  @Value("${operation.register.temporaryFile.ttl}")
  private              long   tempFileTTL;

  @Bean
  DownloadTokenService downloadTokenService(DownloadTokenRepository downloadTokenRepository) {
    return new DownloadTokenServiceImpl(downloadTokenRepository);
  }

  @Bean
  ElectoralAuthorityKeyService electoralAuthorityKeyService(ElectoralAuthorityKeyRepository repository) {
    return new ElectoralAuthorityKeyServiceImpl(repository, Clock.systemDefaultZone());
  }

  @Bean
  HighlightedQuestionService highlightedQuestionService(
      SecuredOperationRepository operationRepository,
      HighlightedQuestionRepository highlightedQuestionRepository,
      LocalizedHighlightedQuestionRepository localizedHighlightedQuestionRepository,
      SecuredOperationHolderService securedOperationHolderService) {
    return new HighlightedQuestionServiceImpl(operationRepository,
                                              highlightedQuestionRepository,
                                              localizedHighlightedQuestionRepository,
                                              securedOperationHolderService);
  }

  @Bean
  OperationUploadServiceVotingPeriodHandler operationUploadServiceVotingPeriodHandler(
      PactConfiguration pactConfiguration, ObjectMapper objectMapper, HttpClient httpClient,
      OperationStatusService operationStatusService, VotingSitePeriodService votingSitePeriodService,
      ElectoralAuthorityKeyConfigurationService authorityKeyConfigurationService,
      SecuredOperationRepository operationRepository) {
    return new OperationUploadServiceVotingPeriodHandler(
        pactConfiguration, objectMapper, httpClient, operationStatusService, authorityKeyConfigurationService,
        votingSitePeriodService, operationRepository);
  }

  @Bean
  VotingSitePeriodService votingSitePeriodService(SimulationPeriodConfigurationRepository repository,
                                                  SecuredOperationRepository operationRepository,
                                                  SecuredOperationHolderService securedOperationHolderService) {
    return new VotingSitePeriodServiceImpl(repository, operationRepository, securedOperationHolderService);
  }

  @Bean
  ElectoralAuthorityKeyConfigurationService electoralAuthorityKeyConfigurationServiceImpl(
      ElectoralAuthorityKeyConfigurationRepository repository,
      SecuredOperationRepository operationRepository,
      ElectoralAuthorityKeyRepository electoralAuthorityKeyRepository,
      SecuredOperationHolderService securedOperationHolderService) {
    return new ElectoralAuthorityKeyConfigurationServiceImpl(
        repository, operationRepository, electoralAuthorityKeyRepository, securedOperationHolderService);
  }


  @Bean
  DictionaryService dictionaryService(ObjectMapper objectMapper, DictionaryRepository dictionaryRepository,
                                      SecuredOperationRepository operationRepository) {
    return new DictionaryServiceImpl(objectMapper, dictionaryRepository, operationRepository);
  }

  @Bean
  BallotDocumentationService ballotDocumentationService(
      SecuredOperationHolderService securedOperationHolderService,
      SecuredOperationRepository securedOperationRepository,
      BallotDocumentationRepository ballotDocumentationRepository) {
    return new BallotDocumentationServiceImpl(securedOperationHolderService,
                                              securedOperationRepository,
                                              ballotDocumentationRepository);
  }


  @Bean
  SecuredOperationHolderService securedOperationHolderService(
      SecuredOperationRepository operationRepository, ConsistencyScheduleService consistencyScheduleService,
      SecuredContext securedContext, OperationStatusService operationStatusService) {
    return new SecuredOperationHolderServiceImpl(consistencyScheduleService, operationRepository, securedContext,
                                                 operationStatusService);
  }


  @Bean
  SecuredOperationRepository securedOperationRepository(
      UnsecuredOperationRepository unsecuredOperationRepository) {
    return new SecuredOperationRepositoryImpl(unsecuredOperationRepository);
  }

  @Bean
  AllBallotsInElectionPagePropertiesShouldBeInOperationRepository ballotsInElectionPageConsistency(
      OperationRepositoryService operationRepositoryService, ElectionPagePropertiesService
      electionPagePropertiesService) {
    return new AllBallotsInElectionPagePropertiesShouldBeInOperationRepository(operationRepositoryService,
                                                                               electionPagePropertiesService);
  }

  @Bean
  ConsistencyChecker allMunicipalitiesShouldHaveAPrinter(
      RegisterMetadataService registerMetadataService,
      ObjectMapper objectMapper,
      PrinterTemplateService printerTemplateService) {
    return new AllMunicipalitiesShouldHaveAPrinter(registerMetadataService, objectMapper, printerTemplateService);
  }

  @Bean
  ConsistencyChecker allDoiDeclaredInVotingCardsShouldBeInOperationRepositoryForConfiguration(
      VoterTestingCardsLotService voterTestingCardLotService, OperationRepositoryService repositoryService) {
    return new AllDoiDeclaredInVotingCardsShouldBeInOperationRepository(
        AbstractConsistencyChecker.CheckType.CONFIGURATION,
        repositoryService,
        voterTestingCardLotService
    );
  }

  @Bean
  ConsistencyChecker allDoiDeclaredInVotingCardsShouldBeInOperationRepositoryForVotingMaterial(
      VoterTestingCardsLotService voterTestingCardLotService, OperationRepositoryService repositoryService) {
    return new AllDoiDeclaredInVotingCardsShouldBeInOperationRepository(
        AbstractConsistencyChecker.CheckType.VOTING_MATERIAL,
        repositoryService,
        voterTestingCardLotService
    );
  }

  @Bean
  AllBallotsInDocumentationShouldBeInOperationRepository allBallotsInDocumentationShouldBeInOperationRepository(
      OperationRepositoryService repositoryService, BallotDocumentationService ballotDocumentationService) {
    return new AllBallotsInDocumentationShouldBeInOperationRepository(repositoryService, ballotDocumentationService);
  }


  @Bean
  ConsistencyChecker allDoiDeclaredInRepositoryShouldBeInDoiFile(
      OperationRepositoryService operationRepositoryService) {
    return new AllDoiDeclaredInRepositoryShouldBeInDoiFile(operationRepositoryService);
  }


  @Bean
  ConsistencyService consistencyService(
      SecuredOperationRepository operationRepository,
      OperationConsistencyRepository consistencyRepository,
      ObjectMapper objectMapper,
      ConsistencyScheduleService consistencyScheduleService) {

    // between 30 and 120 minutes
    long computeTimeout = 30 * 60 * 1000L + new SecureRandom().nextInt(90 * 60 * 1000);

    return new ConsistencyServiceImpl(
        operationRepository,
        consistencyRepository,
        objectMapper,
        computeTimeout,
        consistencyScheduleService,
        Clock.systemDefaultZone());
  }


  @Bean
  ConsistencyScheduleService consistencyScheduleService(
      SecuredOperationRepository operationRepository,
      OperationConsistencyRepository consistencyRepository,
      ObjectMapper objectMapper,
      ApplicationContext applicationContext) {

    return new ConsistencyScheduleServiceImpl(
        operationId -> consistencyServiceRunner(operationId,
                                                operationRepository,
                                                consistencyRepository,
                                                objectMapper,
                                                applicationContext),
        // Never put a non thread safe executor unless strongly change consistencyService
        Executors.newSingleThreadScheduledExecutor()
    );
  }


  @Bean()
  @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
  ConsistencyServiceRunner consistencyServiceRunner(
      @SuppressWarnings(
          {"SpringJavaAutowiringInspection", "SpringJavaInjectionPointsAutowiringInspection"}) Long operationId,
      SecuredOperationRepository operationRepository,
      OperationConsistencyRepository consistencyRepository,
      ObjectMapper objectMapper,
      ApplicationContext applicationContext
  ) {


    return new ConsistencyServiceRunnerImpl(operationId,
                                            operationRepository,
                                            consistencyRepository,
                                            objectMapper,
                                            applicationContext.getBean(OperationStatusService.class),
                                            applicationContext.getBeansOfType(ConsistencyChecker.class).values());
  }


  @Bean
  ManagementEntityService managementEntityService(
      SecuredOperationRepository operationRepository,
      ObjectMapper objectMapper,
      ConsistencyScheduleService consistencyScheduleService,
      FileService fileService) throws IOException {
    return new ManagementEntityServiceImpl(objectMapper, operationRepository, consistencyScheduleService, fileService);
  }

  @Bean
  VoterTestingCardsLotService voterTestingCardsLotService(
      VoterTestingCardsLotRepository repository,
      SecuredOperationRepository operationRepository,
      SecuredOperationHolderService securedOperationHolderService) {
    return new VoterTestingCardsLotServiceImpl(
        repository, operationRepository, securedOperationHolderService);
  }

  @Bean
  OperationVotingMaterialService operationVotingMaterialService(
      SecuredOperationRepository operationRepository,
      ConsistencyScheduleService consistencyScheduleService) {
    return new OperationVotingMaterialServiceImpl(operationRepository, consistencyScheduleService);
  }

  @SuppressWarnings("squid:S00107")
  @Bean
  ReadonlyStatusService readonlyStatusService() {
    return new ReadonlyStatusServiceImpl();
  }


  @Bean
  OperationStatusService operationStatusService(
      SecuredOperationRepository securedOperationRepository,
      ReadonlyStatusService readonlyStatusService,
      OperationStatusTallyHandler statusTallyHandler,
      OperationStatusConfigurationHandler statusConfigurationHandler,
      OperationStatusVotingMaterialHandler statusVotingMaterialHandler,
      OperationStatusVotingPeriodHandler statusVotingPeriodHandler) {
    return new OperationStatusServiceImpl(
        securedOperationRepository,
        readonlyStatusService,
        statusTallyHandler,
        statusConfigurationHandler,
        statusVotingMaterialHandler,
        statusVotingPeriodHandler);
  }

  @Bean
  OperationStatusHandlerHelper helper(@Lazy Set<ConfigurationIsCompleteSupplier> configurationIsCompleteSupplier,
                                      @Lazy Set<VotingMaterialIsCompleteSupplier> votingMaterialIsCompleteSuppliers,
                                      @Lazy Set<VotingPeriodIsCompleteSupplier> votingPeriodIsCompleteSuppliers,
                                      ObjectMapper objectMapper,
                                      PactConfiguration pactConfiguration) {
    return new OperationStatusHandlerHelper(configurationIsCompleteSupplier, votingMaterialIsCompleteSuppliers,
                                            votingPeriodIsCompleteSuppliers, objectMapper, pactConfiguration,
                                            httpClient());
  }


  @Bean
  OperationStatusVotingMaterialHandler statusVotingMaterialHandler(OperationStatusHandlerHelper helper,
                                                                   PactConfiguration pactConfiguration,
                                                                   PrinterTemplateService templateService) {
    return new OperationStatusVotingMaterialHandler(helper, pactConfiguration, templateService);
  }

  @Bean
  OperationStatusTallyHandler statusTallyHandler(OperationStatusHandlerHelper helper,
                                                 PactConfiguration pactConfiguration,
                                                 DownloadTokenService downloadTokenService) {
    return new OperationStatusTallyHandler(downloadTokenService, helper, pactConfiguration);
  }

  @Bean
  OperationStatusConfigurationHandler statusConfigurationHandler(OperationStatusHandlerHelper helper,
                                                                 PactConfiguration pactConfiguration) {
    return new OperationStatusConfigurationHandler(helper, pactConfiguration);
  }

  @Bean
  OperationStatusVotingPeriodHandler statusVotingPeriodHandler(OperationStatusHandlerHelper helper,
                                                               PactConfiguration pactConfiguration) {
    return new OperationStatusVotingPeriodHandler(helper, pactConfiguration);
  }

  @Bean
  PrinterTemplateService printerTemplateService(ObjectMapper objectMapper) throws IOException {
    return new PrinterTemplateServiceImpl(objectMapper);
  }


  @Bean
  ServiceAccessLoggerAspect serviceAccessLoggerAspect() {
    return new ServiceAccessLoggerAspect();
  }


  @Bean
  RegisterMetadataService registerMetadataService(
      RegisterMetadataRepository registerMetadataRepository,
      SecuredOperationRepository operationRepository,
      SecuredOperationHolderService securedOperationHolderService) {
    return new RegisterMetadataServiceImpl(registerMetadataRepository,
                                           operationRepository,
                                           securedOperationHolderService,
                                           tempFileTTL);
  }


  @Bean
  protected RegisterImportService registryImportService(
      ObjectMapper jacksonObjectMapper,
      RegisterMetadataService metadataService,
      OperationRepositoryService operationRepositoryService,
      OperationService operationService) throws JRException {

    return new RegisterImportServiceImpl(ech45Validator(),
                                         jacksonObjectMapper,
                                         metadataService,
                                         operationRepositoryService,
                                         registerReportBuilder(),
                                         operationService);
  }

  @Bean
  RegisterReportBuilder registerReportBuilder() throws JRException {
    return new RegisterReportBuilder();
  }

  @Bean
  protected XSDValidator ech45Validator() {
    return XSDValidator.of("xsd/eCH-0045-4-0.xsd", CATALOG_FILE);
  }

  @Bean
  protected XSDValidator logisticDeliveryValidator() {
    return XSDValidator.of("xsd/logistic-delivery.xsd", CATALOG_FILE);
  }


  @Bean
  OperationService operationService(SecuredOperationRepository operationRepository,
                                    OperationConsistencyRepository operationConsistencyRepository,
                                    VoterTestingCardsLotService voterTestingCardsLotService,
                                    FileService fileService,
                                    OperationStatusService operationStatusService,
                                    ConsistencyScheduleService consistencyScheduleService,
                                    ObjectMapper objectMapper) {
    return new OperationServiceImpl(operationRepository, operationConsistencyRepository, voterTestingCardsLotService,
                                    fileService, operationStatusService, consistencyScheduleService,
                                    objectMapper);
  }

  @Bean
  PrinterOperationService printerOperationService(UnsecuredOperationRepository unsecuredOperationRepository,
                                                  OperationStatusService operationStatusService,
                                                  PrinterTemplateService printerTemplateService,
                                                  DownloadTokenService downloadTokenService,
                                                  PactConfiguration pactConfiguration) {
    return new PrinterOperationServiceImpl(unsecuredOperationRepository,
                                           operationStatusService,
                                           printerTemplateService,
                                           downloadTokenService,
                                           pactConfiguration);
  }

  @Bean
  TestingCardRegisterGeneratorService testingCardRegisterGeneratorService(
      OperationRepositoryService repositoryService,
      SecuredOperationHolderService securedOperationHolderService,
      SecuredOperationRepository securedOperationRepository,
      VoterTestingCardsLotRepository voterTestingCardsLotRepository,
      PrinterTemplateService printerTemplateService) {
    return new TestingCardRegisterGeneratorServiceImpl(
        repositoryService, voterTestingCardsLotRepository, securedOperationRepository, securedOperationHolderService,
        printerTemplateService, Clock.systemDefaultZone());
  }

  @Bean
  OperationStatusAndConsistencyService operationStatusAndConsistencyService(
      OperationStatusService operationStatusService, ConsistencyService consistencyService) {
    return new OperationStatusAndConsistencyServiceImpl(operationStatusService, consistencyService);
  }

  @Bean
  OperationUploadService operationUploadService(
      OperationUploadServiceConfigurationHandler operationUploadServiceConfigurationHandler,
      OperationUploadServiceVotingMaterialHandler operationUploadServiceVotingMaterialHandler,
      OperationUploadServiceVotingPeriodHandler operationUploadServiceVotingPeriodHandler,
      OperationWorkflowService operationWorkflowService) {
    return new OperationUploadServiceImpl(operationUploadServiceConfigurationHandler,
                                          operationUploadServiceVotingMaterialHandler,
                                          operationUploadServiceVotingPeriodHandler,
                                          operationWorkflowService);
  }

  @Bean
  OperationWorkflowService operationWorkflowService(OperationStatusService operationStatusService,
                                                    SecuredOperationRepository securedOperationRepository,
                                                    PactConfiguration pactConfiguration) {
    return new OperationWorkflowServiceImpl(operationStatusService, securedOperationRepository, httpClient(),
                                            pactConfiguration);
  }


  @Bean
  @SuppressWarnings("squid:S00107")
  OperationUploadServiceConfigurationHandler operationUploadServiceConfigurationHandler(
      SecuredOperationRepository operationRepository,
      FileService fileService,
      ObjectMapper objectMapper,
      PrinterTemplateService printerTemplateService,
      PactConfiguration pactConfiguration,
      OperationStatusAndConsistencyService operationStatusAndConsistencyService,
      TestingCardRegisterGeneratorService testingCardRegisterGeneratorService,
      BallotDocumentationService ballotDocumentationService,
      ElectionPagePropertiesService electionPagePropertiesService,
      HighlightedQuestionService highlightedQuestionService,
      DictionaryService dictionaryService
  ) {
    return new OperationUploadServiceConfigurationHandler(
        pactConfiguration, operationRepository, fileService, printerTemplateService, objectMapper, httpClient(),
        testingCardRegisterGeneratorService, operationStatusAndConsistencyService, Clock.systemDefaultZone(),
        ballotDocumentationService, electionPagePropertiesService, highlightedQuestionService, dictionaryService);
  }

  @Bean
  OperationUploadServiceVotingMaterialHandler operationUploadServiceVotingMaterialHandler(
      SecuredOperationRepository operationRepository,
      FileService fileService,
      ObjectMapper objectMapper,
      PrinterTemplateService printerTemplateService,
      PactConfiguration pactConfiguration,
      OperationStatusAndConsistencyService operationStatusAndConsistencyService,
      TestingCardRegisterGeneratorService testingCardRegisterGeneratorService) {
    return new OperationUploadServiceVotingMaterialHandler(
        pactConfiguration, operationRepository, fileService, printerTemplateService, objectMapper, httpClient(),
        testingCardRegisterGeneratorService, operationStatusAndConsistencyService, Clock.systemDefaultZone());
  }


  @Bean
  HttpClient httpClient() {
    return HttpClients.createDefault();
  }

  @Bean
  OperationDomainInfluenceService operationDomainInfluenceService(FileService fileService) {
    return new OperationDomainInfluenceServiceImpl(fileService, logisticDeliveryValidator());
  }

  @Bean
  OperationDocumentService operationDocumentService(FileService fileService) {
    return new OperationDocumentServiceImpl(fileService);
  }

  @Bean
  OperationRepositoryService operationRepositoryService(SecuredOperationRepository operationRepository,
                                                        OperationDomainInfluenceService operationDomainInfluenceService,
                                                        FileService fileService,
                                                        OperationRepositoryServiceVotationHandler votationHandler,
                                                        OperationRepositoryServiceElectionHandler electionHandler) {
    return new OperationRepositoryServiceImpl(operationRepository, operationDomainInfluenceService, fileService,
                                              votationHandler, electionHandler);
  }

  @Bean
  OperationRepositoryServiceVotationHandler votationHandler(FileService fileService) {
    EchCodec<ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery> codec =
        new JAXBEchCodecImpl<>(ch.ge.ve.interfaces.ech.eCH0159.v4.Delivery.class);
    return new OperationRepositoryServiceVotationHandler(
        votationReferentialValidator(),
        is -> {
          try {
            return codec.deserialize(is, false);
          } catch (EchDeserializationRuntimeException e) {
            throw new TechnicalException(e);
          }
        }, fileService);
  }

  @Bean
  OperationRepositoryServiceElectionHandler electionHandler(FileService fileService) {
    EchCodec<ch.ge.ve.interfaces.ech.eCH0157.v4.Delivery> codec =
        new JAXBEchCodecImpl<>(ch.ge.ve.interfaces.ech.eCH0157.v4.Delivery.class);
    return new OperationRepositoryServiceElectionHandler(
        electionReferentialValidator(),
        is -> {
          try {
            return codec.deserialize(is, false);
          } catch (EchDeserializationRuntimeException e) {
            throw new TechnicalException(e);
          }
        }, fileService);
  }

  @Bean
  VotingCardDeliveryService votingCardDeliveryService(OperationStatusService operationStatusService,
                                                      PactConfiguration pactConfiguration,
                                                      PrinterTemplateService printerTemplateService) {
    return new VotingCardDeliveryServiceImpl(operationStatusService,
                                             pactConfiguration,
                                             printerTemplateService);
  }


  @Bean
  XSDValidator votationReferentialValidator() {
    return XSDValidator.of("xsd/eCH-0159-4-0.xsd", CATALOG_FILE);
  }

  @Bean
  XSDValidator electionReferentialValidator() {
    return XSDValidator.of("xsd/eCH-0157-4-0.xsd", CATALOG_FILE);
  }

  @Bean
  FileService fileService(FileRepository fileRepository,
                          SecuredOperationRepository operationRepository,
                          SecuredOperationHolderService securedOperationHolderService) {
    return new FileServiceImpl(fileRepository, operationRepository, securedOperationHolderService);
  }

  @Bean
  ElectionPagePropertiesService electionPagePropertiesService(ElectionPagePropertiesModelRepository modelRepository,
                                                              SecuredOperationRepository operationRepository,
                                                              SecuredOperationHolderService securedService) {
    return new ElectionPagePropertiesServiceImpl(modelRepository, operationRepository,
                                                 securedService);
  }

  @Bean
  VotingSiteConfigurationServiceImpl votingSiteConfigurationServiceImpl(
      VotingSiteConfigurationRepository repository,
      SecuredOperationHolderService securedOperationHolderService,
      SecuredOperationRepository operationRepository) {
    return new VotingSiteConfigurationServiceImpl(repository, securedOperationHolderService, operationRepository);
  }


}
