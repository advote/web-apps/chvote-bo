/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.doi.model;

import ch.ge.ve.bo.FileType;
import ch.ge.ve.bo.Language;
import ch.ge.ve.bo.service.file.model.FileVo;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Value object representing a domain of influence file for an operation
 */
public class DomainOfInfluenceFileVo extends FileVo {

  public final List<DomainOfInfluenceVo> details;

  /**
   * Default constructor
   */
  @SuppressWarnings("squid:S00107") // suppress Sonar check on parameters count
  @JsonCreator
  public DomainOfInfluenceFileVo(@JsonProperty("id") long id,
                                 @JsonProperty("login") String login,
                                 @JsonProperty("saveDate") LocalDateTime saveDate,
                                 @JsonProperty("businessKey") String businessKey,
                                 @JsonProperty("operationId") long operationId,
                                 @JsonProperty("type") FileType type,
                                 @JsonProperty("language") Language language,
                                 @JsonProperty("fileName") String fileName,
                                 @JsonProperty("managementEntity") String managementEntity,
                                 @JsonProperty("fileContent") byte[] fileContent,
                                 @JsonProperty("details") List<DomainOfInfluenceVo> details) {
    super(id, login, saveDate, businessKey, operationId, type, language, fileName, managementEntity, fileContent);
    this.details = details;
  }
}
