/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.repository.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Value object representing a vote information in a votation repository file.
 */
public class VoteInformationVo {

  public final String voteIdentification;
  public final String domainOfInfluence;
  public final int    ballotPosition;
  public final String ballotIdentification;
  public final String acceptedAnswers;

  /**
   * Default constructor
   */
  @JsonCreator
  public VoteInformationVo(@JsonProperty("voteIdentification") String voteIdentification,
                           @JsonProperty("domainOfInfluence") String domainOfInfluence,
                           @JsonProperty("ballotPosition") int ballotPosition,
                           @JsonProperty("ballotIdentification") String ballotIdentification,
                           @JsonProperty("acceptedAnswers") String acceptedAnswers) {
    this.voteIdentification = voteIdentification;
    this.domainOfInfluence = domainOfInfluence;
    this.ballotPosition = ballotPosition;
    this.ballotIdentification = ballotIdentification;
    this.acceptedAnswers = acceptedAnswers;
  }

}
