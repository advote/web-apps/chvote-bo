/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation;

import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService;
import java.time.LocalDateTime;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of {@link OperationVotingMaterialService}
 */
public class OperationVotingMaterialServiceImpl implements OperationVotingMaterialService {
  private final SecuredOperationRepository operationRepository;
  private final ConsistencyScheduleService consistencyScheduleService;


  /**
   * Default constructor
   */
  public OperationVotingMaterialServiceImpl(SecuredOperationRepository operationRepository, ConsistencyScheduleService consistencyScheduleService) {
    this.operationRepository = operationRepository;
    this.consistencyScheduleService = consistencyScheduleService;
  }


  @Override
  @Transactional
  public void updateVotingCardTitle(Long operationId, String newTitle) {
    Operation operation = operationRepository.findOne(operationId, true);
    operation.setVotingCardTitle(newTitle);
    operation.setLastVotingMaterialUpdateDate(LocalDateTime.now());
    consistencyScheduleService.scheduleCheck(operationId);
    operationRepository.save(operation);
  }

  @Override
  public void updatePrinterTemplate(long operationId, String printerTemplate) {
    Operation operation = operationRepository.findOne(operationId, true);
    operation.setPrinterTemplate(printerTemplate);
    operation.setLastVotingMaterialUpdateDate(LocalDateTime.now());
    consistencyScheduleService.scheduleCheck(operationId);
    operationRepository.save(operation);
  }
}
