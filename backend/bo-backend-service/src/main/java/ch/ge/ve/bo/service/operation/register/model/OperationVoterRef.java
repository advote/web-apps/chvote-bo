/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

/**
 * Represent a reference to a voter in an operation
 */
public class OperationVoterRef {
  public final FileInfo fileInfo;
  public final String identifier;
  public final String hash;

  /**
   * Default constructor
   */
  public OperationVoterRef(FileInfo fileInfo, Voter voter) {
    this.fileInfo = fileInfo;
    this.identifier = voter.getIdentifier();
    this.hash = voter.getHash();
  }

  /**
   * Default constructor
   */
  public OperationVoterRef(FileInfo fileInfo, RegisterVoterRef voter) {
    this.fileInfo = fileInfo;
    this.identifier = voter.getIdentifier();
    this.hash = voter.getHash();
  }

  RegisterVoterRef toRegistryVoterRef() {
    return new RegisterVoterRef(identifier, hash);
  }
}
