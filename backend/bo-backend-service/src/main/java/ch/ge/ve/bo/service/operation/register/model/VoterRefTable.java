/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Memory table used to store all the voter references of an operation.
 */
public class VoterRefTable {
  private final Map<String, Set<OperationVoterRef>> voterRefByHash = new HashMap<>();

  /**
   * Add a {@link Voter}
   */
  public void add(FileInfo fileInfo, Voter voter) {
    voterRefByHash
        .computeIfAbsent(voter.getHash(), hash -> new HashSet<>())
        .add(new OperationVoterRef(fileInfo, voter));
  }

  /**
   * Add a {@link RegisterVoterRef}
   */
  public void add(FileInfo fileInfo, RegisterVoterRef voter) {
    voterRefByHash
        .computeIfAbsent(voter.getHash(), hash -> new HashSet<>())
        .add(new OperationVoterRef(fileInfo, voter));
  }

  /**
   * Get Hash table of voters for a given file
   */
  public List<RegisterVoterRef> getHashTableForFile(String fileName) {
    return voterRefByHash
        .values()
        .stream()
        .flatMap(Collection::stream)
        .filter(voterRef -> voterRef.fileInfo.getFileName().equals(fileName))
        .map(OperationVoterRef::toRegistryVoterRef)
        .collect(Collectors.toList());
  }

  /**
   * get others voters to find duplicates
   */
  public Set<OperationVoterRef> getOthers(Voter currentVoter) {
    return voterRefByHash.getOrDefault(currentVoter.getHash(), Collections.emptySet());
  }
}
