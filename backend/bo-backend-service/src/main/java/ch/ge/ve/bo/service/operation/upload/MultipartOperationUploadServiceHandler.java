/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload;

import static ch.ge.ve.bo.service.utils.PactServiceUtils.attachCredentialsToRequest;
import static org.apache.http.entity.ContentType.APPLICATION_OCTET_STREAM;

import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot;
import ch.ge.ve.bo.service.exception.TechnicalException;
import ch.ge.ve.bo.service.model.PactConfiguration;
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration;
import ch.ge.ve.bo.service.operation.exception.PactRequestException;
import ch.ge.ve.chvote.pactback.contract.operation.AttachmentFileEntryVo;
import ch.ge.ve.chvote.pactback.contract.operation.MunicipalityVo;
import ch.ge.ve.filenamer.FileNamer;
import ch.ge.ve.filenamer.PrintingLotType;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.BasicResponseHandler;

/**
 * Class used to support multipart upload to pact application
 */
public abstract class MultipartOperationUploadServiceHandler implements OperationUploadServiceHandler {
  protected final Clock             clock;
  private final   PactConfiguration pactConfiguration;
  private final   HttpClient        httpClient;

  /**
   * Default constructor
   */
  public MultipartOperationUploadServiceHandler(Clock clock,
                                                PactConfiguration pactConfiguration,
                                                HttpClient httpClient) {
    this.clock = clock;
    this.pactConfiguration = pactConfiguration;
    this.httpClient = httpClient;
  }

  List<AttachmentFileEntryVo> attachRegisterForTestingCards(
      String operationLabel,
      Map<VoterTestingCardsLot, Map<String, String>> testingCardXMLs,
      MultipartEntityBuilder reqEntityBuilder
  ) throws IOException {

    List<AttachmentFileEntryVo> attachments = new ArrayList<>();

    LocalDateTime creationDate = LocalDateTime.now(clock);

    for (Map.Entry<VoterTestingCardsLot, Map<String, String>> lot : testingCardXMLs.entrySet()) {
      for (Map.Entry<String, String> printer : lot.getValue().entrySet()) {
        String filename = FileNamer.lotPrinterFile(
            mapToPrintingLotType(lot.getKey()),
            operationLabel,
            lot.getKey().getLotName(),
            printer.getKey(),
            creationDate
        );

        try (
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(bos)
        ) {
          zipOutputStream.putNextEntry(new ZipEntry(String.format("%s.xml", filename)));
          zipOutputStream.write(printer.getValue().getBytes(StandardCharsets.UTF_8));
          zipOutputStream.closeEntry();
          AttachmentFileEntryVo attachment = new AttachmentFileEntryVo();
          attachment.setAttachmentType(AttachmentFileEntryVo.AttachmentType.REGISTER);
          attachment.setImportDateTime(creationDate);
          final String zipFileName = String.format("%s.zip", filename);
          attachment.setZipFileName(zipFileName);
          attachments.add(attachment);
          reqEntityBuilder.addPart("data", new ByteArrayBody(bos.toByteArray(), APPLICATION_OCTET_STREAM, zipFileName));
        }

      }
    }
    return attachments;
  }

  void doMultipartPost(String urlTemplate, long operationId, HttpEntity reqEntity) throws PactRequestException {
    String url = MessageFormat.format(urlTemplate, operationId);
    try {
      HttpPost post = new HttpPost(url);
      post.setEntity(reqEntity);
      attachCredentialsToRequest(post, pactConfiguration.getUsername(), pactConfiguration.getPassword());
      httpClient.execute(post, new BasicResponseHandler());
    } catch (HttpResponseException responseException) {
      throw PactRequestException.of(responseException, url);
    } catch (IOException e) {
      throw new TechnicalException(e);
    }
  }

  MunicipalityVo createVirtualMunicipality(PrinterConfiguration printerConfiguration) {
    MunicipalityVo municipalityVo = new MunicipalityVo();
    municipalityVo.setName("Virtual municipality for printer " + printerConfiguration.id);
    municipalityVo.setOfsId(printerConfiguration.municipalityForTestingCard);
    return municipalityVo;
  }

  private PrintingLotType mapToPrintingLotType(VoterTestingCardsLot lot) {
    switch (lot.getCardType()) {
      case PRINTER_TESTING_CARD:
        return PrintingLotType.PRINTER;
      case CONTROLLER_TESTING_CARD:
        return PrintingLotType.CONTROL;
      default:
        if (lot.isShouldPrint()) {
          return PrintingLotType.TEST_PRINTABLE;
        } else {
          return PrintingLotType.TEST_NOT_PRINTABLE;
        }
    }
  }

}
