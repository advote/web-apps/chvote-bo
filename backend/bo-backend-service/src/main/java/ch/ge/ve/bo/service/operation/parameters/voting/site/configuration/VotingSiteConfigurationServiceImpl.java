/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.voting.site.configuration;

import ch.ge.ve.bo.repository.SecuredOperationRepository;
import ch.ge.ve.bo.repository.VotingSiteConfigurationRepository;
import ch.ge.ve.bo.repository.entity.Operation;
import ch.ge.ve.bo.repository.entity.VotingSiteConfiguration;
import ch.ge.ve.bo.service.operation.SecuredOperationHolderService;
import ch.ge.ve.bo.service.operation.model.VotingSiteConfigurationVo;
import java.util.Optional;
import javax.transaction.Transactional;

/**
 * Default implementation of {@link VotingSiteConfigurationService}
 */
public class VotingSiteConfigurationServiceImpl implements VotingSiteConfigurationService {

  private final VotingSiteConfigurationRepository votingSiteConfigurationRepository;
  private final SecuredOperationHolderService     securedOperationHolderService;
  private final SecuredOperationRepository        securedOperationRepository;

  /**
   * Default constructor
   */
  public VotingSiteConfigurationServiceImpl(VotingSiteConfigurationRepository votingSiteConfigurationRepository,
                                            SecuredOperationHolderService securedOperationHolderService,
                                            SecuredOperationRepository securedOperationRepository) {
    this.votingSiteConfigurationRepository = votingSiteConfigurationRepository;
    this.securedOperationHolderService = securedOperationHolderService;
    this.securedOperationRepository = securedOperationRepository;
  }

  @Override
  @Transactional
  public void saveOrUpdate(VotingSiteConfigurationVo votingSiteConfigurationVo, Long operationId) {
    securedOperationHolderService.doNotOptionalUpdateAndForget(
        () -> {
          VotingSiteConfiguration configuration = votingSiteConfigurationRepository
              .findByOperationId(operationId)
              .orElseGet(() -> {
                Operation operation = securedOperationRepository.findOne(operationId, true);
                VotingSiteConfiguration newConfiguration = new VotingSiteConfiguration();
                newConfiguration.setOperation(operation);
                return newConfiguration;
              });
          configuration.setDefaultLanguage(votingSiteConfigurationVo.getDefaultLanguage());
          configuration.setAllLanguages(votingSiteConfigurationVo.getAllLanguages());
          return configuration;
        }, votingSiteConfigurationRepository::save);
  }

  @Override
  public Optional<VotingSiteConfigurationVo> findForOperation(long operationId) {
    return securedOperationHolderService
        .safeRead(() -> votingSiteConfigurationRepository.findByOperationId(operationId))
        .map(VotingSiteConfigurationVo::new);
  }
}
