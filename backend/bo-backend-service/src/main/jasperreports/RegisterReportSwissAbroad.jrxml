<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ #%L
  ~ chvote-bo
  ~ %%
  ~ Copyright (C) 2016 - 2019 République et Canton de Genève
  ~ %%
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program. If not, see <http://www.gnu.org/licenses/>.
  ~ #L%
  -->

<!-- Created with Jaspersoft Studio version 6.4.3.final using JasperReports Library version 6.4.3  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd"
              name="Blank_A4" pageWidth="595" pageHeight="842" columnWidth="555" leftMargin="20" rightMargin="20"
              topMargin="0" bottomMargin="0" isFloatColumnFooter="true" resourceBundle="reports/reports">
    <style name="default" isDefault="true" fontSize="9"/>
    <field name="lang" class="java.lang.String">
        <property name="net.sf.jasperreports.json.field.expression" value="lang"/>
        <fieldDescription><![CDATA[lang]]></fieldDescription>
    </field>
    <field name="postage-code" class="java.lang.Integer">
        <property name="net.sf.jasperreports.json.field.expression" value="postage-code"/>
        <fieldDescription><![CDATA[postage-code]]></fieldDescription>
    </field>
    <field name="count" class="java.lang.Integer">
        <property name="net.sf.jasperreports.json.field.expression" value="count"/>
        <fieldDescription><![CDATA[count]]></fieldDescription>
    </field>
    <variable name="count by lang" class="java.lang.Integer" resetType="Group" resetGroup="byLang" calculation="Sum">
        <variableExpression><![CDATA[$F{count}]]></variableExpression>
        <initialValueExpression><![CDATA[0]]></initialValueExpression>
    </variable>
    <variable name="total count" class="java.lang.Integer" calculation="Sum">
        <variableExpression><![CDATA[$F{count}]]></variableExpression>
        <initialValueExpression><![CDATA[0]]></initialValueExpression>
    </variable>
    <group name="byLang" keepTogether="true">
        <groupExpression><![CDATA[$F{lang}]]></groupExpression>
        <groupFooter>
            <band height="20">
                <property name="com.jaspersoft.studio.unit.height" value="px"/>
                <rectangle>
                    <reportElement stretchType="ElementGroupHeight" x="230" y="0" width="320" height="20"
                                   forecolor="#FFFFFF" backcolor="#87ACD4"/>
                    <graphicElement>
                        <pen lineWidth="0.0"/>
                    </graphicElement>
                </rectangle>
                <textField>
                    <reportElement positionType="Float" stretchType="ElementGroupHeight" x="300" y="0" width="250"
                                   height="20">
                        <property name="com.jaspersoft.studio.unit.y" value="px"/>
                        <property name="com.jaspersoft.studio.unit.height" value="px"/>
                    </reportElement>
                    <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                    <textElement textAlignment="Right"/>
                    <textFieldExpression><![CDATA[$R{subTotal} + " " + $V{count by lang}]]></textFieldExpression>
                </textField>
            </band>
        </groupFooter>
    </group>
    <columnHeader>
        <band height="20" splitType="Stretch">
            <rectangle>
                <reportElement x="0" y="0" width="550" height="20" backcolor="#6580BF">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <graphicElement>
                    <pen lineWidth="0.0"/>
                </graphicElement>
            </rectangle>
            <textField>
                <reportElement x="230" y="0" width="70" height="20" forecolor="#FFFFFF"/>
                <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                <textElement>
                    <font isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA[$R{header.lang}]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="300" y="0" width="140" height="20" forecolor="#FFFFFF"/>
                <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                <textElement>
                    <font isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA[$R{header.posting_code}]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="440" y="0" width="110" height="20" forecolor="#FFFFFF"/>
                <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                <textElement textAlignment="Right">
                    <font isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA[$R{header.count}]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="0" y="0" width="230" height="20" forecolor="#FFFFFF"/>
                <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                <textElement>
                    <font isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA[$R{header.voter_type}]]></textFieldExpression>
            </textField>
        </band>
    </columnHeader>
    <detail>
        <band height="20" splitType="Stretch">
            <property name="com.jaspersoft.studio.unit.height" value="px"/>
            <rectangle>
                <reportElement mode="Opaque" x="300" y="0" width="250" height="20" backcolor="#A5CAE8">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <graphicElement>
                    <pen lineWidth="0.0"/>
                </graphicElement>
            </rectangle>

            <rectangle>
                <reportElement x="230" y="0" width="70" height="20" backcolor="#D6D6D6">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <graphicElement>
                    <pen lineWidth="0.0"/>
                </graphicElement>
            </rectangle>
            <textField>
                <reportElement isPrintRepeatedValues="false" x="230" y="0" width="70" height="20">
                    <property name="com.jaspersoft.studio.spreadsheet.connectionID"
                              value="d82d1872-dc40-4b70-96b5-a0ebbdb68221"/>
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                <textFieldExpression><![CDATA[$F{lang}]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="300" y="0" width="140" height="20">
                    <property name="com.jaspersoft.studio.spreadsheet.connectionID"
                              value="54c3da12-709d-41f8-aba2-9778f559500b"/>
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                <textElement textAlignment="Right"/>
                <textFieldExpression><![CDATA[$F{postage-code}]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement x="440" y="0" width="110" height="20">
                    <property name="com.jaspersoft.studio.spreadsheet.connectionID"
                              value="e616b6c0-bdc8-4372-a529-fc27ccc7dc8b"/>
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                <textElement textAlignment="Right"/>
                <textFieldExpression><![CDATA[$F{count}]]></textFieldExpression>
            </textField>
            <textField>
                <reportElement isPrintRepeatedValues="false" x="0" y="0" width="230" height="20">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <box topPadding="3" leftPadding="8" bottomPadding="3" rightPadding="3"/>
                <textElement>
                    <font isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA[$R{swissAbroad}]]></textFieldExpression>
            </textField>
        </band>
    </detail>
    <columnFooter>
        <band height="20" splitType="Stretch">
            <property name="com.jaspersoft.studio.unit.height" value="px"/>
            <rectangle>
                <reportElement mode="Opaque" x="0" y="0" width="550" height="20" backcolor="#EDEDED">
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                </reportElement>
                <graphicElement>
                    <pen lineWidth="0.0"/>
                </graphicElement>
            </rectangle>
            <textField>
                <reportElement x="300" y="0" width="250" height="20">
                    <property name="com.jaspersoft.studio.unit.height" value="px"/>
                    <property name="com.jaspersoft.studio.unit.y" value="px"/>
                </reportElement>
                <box topPadding="3" bottomPadding="3" rightPadding="3"/>
                <textElement textAlignment="Right">
                    <font isBold="true"/>
                </textElement>
                <textFieldExpression><![CDATA[$R{total.foreigners} + " "  + $V{total count}]]></textFieldExpression>
            </textField>
        </band>
    </columnFooter>
</jasperReport>
