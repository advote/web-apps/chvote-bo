/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status.complete

import ch.ge.ve.bo.Language
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.service.operation.model.VotingSiteConfigurationVo
import ch.ge.ve.bo.service.operation.parameters.voting.site.configuration.VotingSiteConfigurationService
import spock.lang.Specification

class VotingSiteConfigurationCompleteTest extends Specification {


  def configurationService = Mock(VotingSiteConfigurationService)
  def service = new VotingSiteConfigurationComplete(configurationService)


  def "isSectionComplete is true when configuration  has been set"() {
    given:
    def operation = OperationDataset.electoralOperation()
    configurationService.findForOperation(operation.id) >>
            Optional.of(new VotingSiteConfigurationVo(Language.FR, [Language.FR] as Set))

    expect:
    service.isSectionComplete(operation)
  }

  def "isSectionComplete is false when configuration has not been set"() {
    given:
    def operation = OperationDataset.electoralOperation()
    configurationService.findForOperation(operation.id) >> Optional.empty()

    expect:
    !service.isSectionComplete(operation)
  }

  def "sectionName is voting-card-title"() {
    expect:
    service.sectionName == "voting-site-configuration"

  }

}
