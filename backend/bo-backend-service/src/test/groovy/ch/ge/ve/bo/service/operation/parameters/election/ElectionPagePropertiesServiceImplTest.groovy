/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.election

import ch.ge.ve.bo.repository.ElectionPagePropertiesModelRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.ElectionPageProperties
import ch.ge.ve.bo.repository.entity.ElectionPagePropertiesModel
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.exception.TechnicalException
import ch.ge.ve.bo.service.operation.model.ElectionPagePropertiesModelVo
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import spock.lang.Specification

class ElectionPagePropertiesServiceImplTest extends Specification {
  def modelRepository = Mock(ElectionPagePropertiesModelRepository.class)
  def operationRepository = Mock(SecuredOperationRepository.class)
  def service = new ElectionPagePropertiesServiceImpl(modelRepository, operationRepository, TestHelpers.passThroughSecuredOperationHolderService())


  def "GetModelsForOperation"() {
    given:
    def operation = OperationDataset.electoralOperation()
    def model = new ElectionPagePropertiesModel()
    model.id = 1
    model.setAllowOpenCandidature(true)
    model.modelName = "model name"
    model.candidateInformationDisplayModel = "candidate display model"
    model.displayedColumnsOnVerificationTable = "c1|c2"
    model.columnsOrderOnVerificationTable = "c3"
    modelRepository.findAllByOperationId(operation.id) >> [model]

    when:
    def models = service.getModelsForOperation(operation.id)

    then:
    1 == models.size()
    JSONAssert.assertEquals(""" 
    {
      "id":1,
      "modelName":"model name",
      "displayCandidateSearchForm":false,
      "allowChangeOfElectoralList":false,
      "displayEmptyPosition":false,
      "displayCandidatePositionOnACompactBallotPaper":false,
      "displayCandidatePositionOnAModifiedBallotPaper":false,
      "displaySuffrageCount":false,
      "displayListVerificationCode":false,
      "displayedColumnsOnVerificationTable" : ["c1","c2"],
      "columnsOrderOnVerificationTable" : ["c3"],
      "candidateInformationDisplayModel":"candidate display model",
      "allowOpenCandidature":true,
      "allowMultipleMandates":false,
      "displayVoidOnEmptyBallotPaper":false
    }""", TestHelpers.getObjectMapper().writeValueAsString(models[0]), JSONCompareMode.NON_EXTENSIBLE)

  }

  def "SaveOrUpdateModel"() {
    given:
    def operation = OperationDataset.electoralOperation()

    def model = new ElectionPagePropertiesModel()
    model.setId(1)
    model.displayedColumnsOnVerificationTable = "c1|c2"
    model.columnsOrderOnVerificationTable = "c3"
    ElectionPagePropertiesModelVo modelVo = new ElectionPagePropertiesModelVo(
            0L, "modelName", true, true, true, true, true, true, true, "model",
            true, true, true, [] as String[], [] as String[])


    when:
    service.saveOrUpdateModel(operation.id, modelVo)

    then:
    1 * operationRepository.findOne(operation.id, true) >> operation
    1 * modelRepository.save(_) >> model
  }

  def "DeleteModel should work if there is no mapping"() {
    given:
    def model = new ElectionPagePropertiesModel()
    model.setId(1)
    model.setMappings([])
    modelRepository.findById(model.id) >> Optional.of(model)

    when:
    service.deleteModel(model.id)

    then:
    1 * modelRepository.delete(model)
  }

  def "DeleteModel should throw Exception if there is mapping"() {
    given:
    def model = new ElectionPagePropertiesModel()
    model.setId(1)
    model.setMappings([new ElectionPageProperties()])
    modelRepository.findById(model.id) >> Optional.of(model)

    when:
    service.deleteModel(model.id)

    then:
    thrown TechnicalException
    0 * modelRepository.delete(model)
  }


  def "associateModelToBallots"() {
    def model1 = new ElectionPagePropertiesModel()
    model1.id = 1
    model1.mappings = [electionPageProperties("ballot2")]

    def model2 = new ElectionPagePropertiesModel()
    model2.id = 2
    model2.mappings = [electionPageProperties("ballot"), electionPageProperties("ballot3")]
    modelRepository.findById(model1.id) >> Optional.of(model1)
    modelRepository.findAllByOperationId(1) >> [model1, model2]

    when:
    service.associateModelToBallots(1L, model1.id, ["ballot"] as String[])

    then:
    3 * modelRepository.save(_ as ElectionPagePropertiesModel) >> { it[0] }

    model1.mappings.ballot == ["ballot2", "ballot"]
    model2.mappings.ballot == ["ballot3"]
  }

  def electionPageProperties(String ballot) {
    def mapping = new ElectionPageProperties()
    mapping.ballot = ballot
    mapping
  }

  def "GetBallotToModelMapping"() {
    given:
    def operation = OperationDataset.electoralOperation()
    def model1 = new ElectionPagePropertiesModel()
    model1.setId(1)
    model1.displayedColumnsOnVerificationTable = ""
    model1.mappings = [electionPageProperties("ballot")]
    def model2 = new ElectionPagePropertiesModel()
    model2.setId(2)
    model2.displayedColumnsOnVerificationTable = ""
    model2.mappings = [electionPageProperties("ballot2")]

    modelRepository.findAllByOperationId(operation.id) >> [model1, model2]

    when:
    def map = service.getBallotToModelIdMapping(operation.id)

    then:
    ["ballot": 1L, "ballot2": 2L] == map

  }

  def "RemoveMapping"() {
    given:
    def model1 = new ElectionPagePropertiesModel()
    model1.id = 1
    model1.mappings = [electionPageProperties("ballot2")]

    def model2 = new ElectionPagePropertiesModel()
    model2.id = 2
    model2.mappings = [electionPageProperties("ballot"), electionPageProperties("ballot3")]
    modelRepository.findAllByOperationId(1) >> [model1, model2]


    when:
    service.removeMapping(1, "ballot")

    then:
    1 * modelRepository.save(model2) >> model2
    model1.mappings.ballot == ["ballot2"]
    model2.mappings.ballot == ["ballot3"]
  }

}
