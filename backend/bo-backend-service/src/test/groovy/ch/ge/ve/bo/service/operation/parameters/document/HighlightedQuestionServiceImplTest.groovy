/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document

import ch.ge.ve.bo.Language
import ch.ge.ve.bo.repository.HighlightedQuestionRepository
import ch.ge.ve.bo.repository.LocalizedHighlightedQuestionRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.HighlightedQuestion
import ch.ge.ve.bo.repository.entity.LocalizedHighlightedQuestion
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.service.TestHelpers
import spock.lang.Specification

class HighlightedQuestionServiceImplTest extends Specification {

    def operationRepository = Mock(SecuredOperationRepository.class)
    def highlightedQuestionRepository = Mock(HighlightedQuestionRepository)
    def localizedHighlightedQuestionRepository = Mock(LocalizedHighlightedQuestionRepository)
    def service = new HighlightedQuestionServiceImpl(operationRepository,
            highlightedQuestionRepository, localizedHighlightedQuestionRepository,
            TestHelpers.passThroughSecuredOperationHolderService())

    def "getFileContent"() {
        given:
        Operation operation = OperationDataset.electoralOperation()
        def question = new HighlightedQuestion()
        question.setOperation(operation)

        def localizedQuestion = new LocalizedHighlightedQuestion()
        localizedQuestion.id = 457
        localizedQuestion.fileName = "file.pdf"
        localizedQuestion.fileContent = "content".bytes
        localizedQuestion.question = question
        localizedHighlightedQuestionRepository.findById(localizedQuestion.id) >> Optional.of(localizedQuestion)

        when:
        def contentAndFileName = service.getFileContent(localizedQuestion.id)

        then:
        "file.pdf" == contentAndFileName.getFileName()
        localizedQuestion.fileContent == contentAndFileName.content
    }

    def "findByOperation"() {
        given:
        Operation operation = OperationDataset.electoralOperation()
        def question = new HighlightedQuestion()
        question.setLocalizedQuestions([])
        question.setId(1)
        question.setOperation(operation)
        highlightedQuestionRepository.findByOperation(operation) >> [question]

        when:
        def result = service.findByOperation(operation.id)

        then:
        1 * operationRepository.findOne(operation.id, false) >> operation
        1 == result.size()
        1 == result.get(0).id
    }

    def "AddQuestion"() {
        given:
        Operation operation = OperationDataset.electoralOperation()

        when:
        def question = service.addQuestion(operation.id)

        then:
        (1.._) * operationRepository.findOne(operation.id, true) >> operation
        1 * highlightedQuestionRepository.save(_) >> { args -> def q = args[0]; q.id = 1; return q }
        0 == question.localizedQuestions.size()
        1l == question.id
    }

    def "AddLocalizedQuestion"() {
        given:
        def questionId = 124
        Operation operation = OperationDataset.electoralOperation()
        def question = new HighlightedQuestion()
        question.setOperation(operation)
        question.setLocalizedQuestions([])
        highlightedQuestionRepository.findById(questionId) >> Optional.of(question)
        when:
        def savedQuestion = service.addLocalizedQuestion(questionId, "filename", "question", Language.FR, null)

        then:
        1 * highlightedQuestionRepository.save(question) >> { args -> args[0] }
        1 == savedQuestion.localizedQuestions.size()
        "filename" == savedQuestion.localizedQuestions[0].fileName
        Language.FR == savedQuestion.localizedQuestions[0].language
        "question" == savedQuestion.localizedQuestions[0].localizedQuestion
    }

    def "DeleteLocalizedQuestion"() {
        given:
        def questionId = 124
        Operation operation = OperationDataset.electoralOperation()
        def question = new HighlightedQuestion()
        question.setOperation(operation)

        def localizedQuestion = new LocalizedHighlightedQuestion()
        localizedQuestion.id = 457
        question.setLocalizedQuestions([localizedQuestion])

        highlightedQuestionRepository.findById(questionId) >> Optional.of(question)

        when:
        def savedQuestion = service.deleteLocalizedQuestion(questionId, localizedQuestion.id)

        then:
        1 * highlightedQuestionRepository.save(question) >> { args -> args[0] }
        0 == savedQuestion.localizedQuestions.size()
    }

    def "DeleteQuestion"() {
        given:
        def questionId = 124
        Operation operation = OperationDataset.electoralOperation()
        def question = new HighlightedQuestion()
        question.setOperation(operation)
        question.setLocalizedQuestions([])
        highlightedQuestionRepository.save(question) >> { args -> args[0] }
        highlightedQuestionRepository.findById(questionId) >> Optional.of(question)

        when:
        service.deleteQuestion(questionId)

        then:
        1 * highlightedQuestionRepository.delete(question)
    }
}
