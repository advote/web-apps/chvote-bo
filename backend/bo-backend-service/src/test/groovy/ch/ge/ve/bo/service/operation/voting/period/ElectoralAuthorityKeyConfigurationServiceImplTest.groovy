/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.voting.period

import ch.ge.ve.bo.repository.ElectoralAuthorityKeyConfigurationRepository
import ch.ge.ve.bo.repository.ElectoralAuthorityKeyRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.ElectoralAuthorityKey
import ch.ge.ve.bo.repository.entity.ElectoralAuthorityKeyConfiguration
import ch.ge.ve.bo.service.TestHelpers
import java.time.LocalDateTime
import spock.lang.Specification

class ElectoralAuthorityKeyConfigurationServiceImplTest extends Specification {

  def repository = Mock(ElectoralAuthorityKeyConfigurationRepository)
  def operationRepository = Mock(SecuredOperationRepository)
  def electoralAuthorityKeyRepository = Mock(ElectoralAuthorityKeyRepository)
  def service = new ElectoralAuthorityKeyConfigurationServiceImpl(
          repository,
          operationRepository,
          electoralAuthorityKeyRepository,
          TestHelpers.passThroughSecuredOperationHolderService())

  def "saveOrUpdate"() {
    given:
    def key = key()

    electoralAuthorityKeyRepository.findById(3) >> Optional.of(key)
    def operation = OperationDataset.electoralOperation()
    operationRepository.findOne(operation.id, true) >> operation
    repository.findByOperationId(operation.id) >> Optional.empty()
    when:
    def result = service.saveOrUpdate(operation.id, key.id)

    then:
    1 * repository.save(_) >> { it[0] }
    "label" == result.label
  }

  def "findForOperation with data"() {
    given:
    def keyConfig = new ElectoralAuthorityKeyConfiguration()
    keyConfig.setElectoralAuthorityKey(key())
    repository.findByOperationId(1) >> Optional.of(keyConfig)

    when:
    def result = service.findForOperation(1)

    then:
    "label" == result.get().label
  }

  def "findForOperation without data should return null"() {
    given:
    repository.findByOperationId(1) >> Optional.empty()

    when:
    def result = service.findForOperation(1)

    then:
    !result.isPresent()
  }


  ElectoralAuthorityKey key() {
    ElectoralAuthorityKey key = new ElectoralAuthorityKey()
    key.setId(3)
    key.setManagementEntity("cdg")
    key.setSaveDate(LocalDateTime.of(2010, 1, 1, 11, 00))
    key.setImportDate(LocalDateTime.of(2010, 1, 1, 10, 00))
    key.setKeyContent("content".bytes)
    key.setKeyName("label")
    return key
  }


}
