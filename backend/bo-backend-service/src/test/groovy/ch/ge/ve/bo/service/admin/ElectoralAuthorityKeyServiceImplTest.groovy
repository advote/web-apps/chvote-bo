/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.admin

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.ElectoralAuthorityKeyRepository
import ch.ge.ve.bo.repository.entity.ElectoralAuthorityKey
import ch.ge.ve.bo.service.exception.SecurityException
import java.time.Clock
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import spock.lang.Specification

class ElectoralAuthorityKeyServiceImplTest extends Specification {

  def repository = Mock(ElectoralAuthorityKeyRepository)
  def service = new ElectoralAuthorityKeyServiceImpl(repository, Clock.fixed(Instant.EPOCH, ZoneId.systemDefault()))

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def "UpdateLabel with good management entity"() {
    given:
    ConnectedUserTestUtils.connectUser("test", "GE", "cdg")
    repository.getOne(1) >> key()

    when:
    service.updateLabel(1, "new Label")

    then:
    1 * repository.save({ it.keyName == "new Label" }) >> { it[0] }

  }

  def "UpdateLabel with bad management entity"() {
    given:
    ConnectedUserTestUtils.connectUser("test", "GE", "cdg 2")
    repository.getOne(1) >> key()

    when:
    service.updateLabel(1, "new Label")

    then:
    thrown SecurityException

  }

  def "Create"() {
    given:
    ConnectedUserTestUtils.connectUser("test", "GE", "cdg")
    repository.save(_) >> { it[0].id = 1; return it[0] }

    when:
    def key = service.create("label", "content".bytes)

    then:
    1l == key.id
    "cdg" == key.managementEntity
  }

  def "FindAll"() {
    given:
    ConnectedUserTestUtils.connectUser("test", "GE", "cdg")
    repository.findAllByManagementEntity("cdg") >> [key()]

    when:
    def keys = service.findAll()

    then:
    keys.size() == 1
    ["label"] == keys.label


  }

  ElectoralAuthorityKey key() {
    ElectoralAuthorityKey key = new ElectoralAuthorityKey()
    key.setId(1)
    key.setManagementEntity("cdg")
    key.setSaveDate(LocalDateTime.of(2010, 1, 1, 11, 00))
    key.setImportDate(LocalDateTime.of(2010, 1, 1, 10, 00))
    key.setKeyContent("content".bytes)
    key.setKeyName("label")
    return key
  }
}
