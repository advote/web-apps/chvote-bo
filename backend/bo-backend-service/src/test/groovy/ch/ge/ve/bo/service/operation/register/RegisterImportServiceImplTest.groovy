/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register

import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime
import static ch.ge.ve.bo.service.TestHelpers.getResourceStream
import static ch.ge.ve.bo.service.dataset.DomainOfInfluenceFileVoDataset.fedCanCom
import static ch.ge.ve.bo.service.dataset.DomainOfInfluenceFileVoDataset.genevaDoi

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.File
import ch.ge.ve.bo.repository.entity.RegisterMetadata
import ch.ge.ve.bo.service.dataset.OperationVoDataset
import ch.ge.ve.bo.service.file.exception.InvalidFileException
import ch.ge.ve.bo.service.operation.OperationService
import ch.ge.ve.bo.service.operation.parameters.doi.OperationDomainInfluenceService
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService
import ch.ge.ve.bo.service.operation.register.exception.InvalidNumberOfVoterException
import ch.ge.ve.bo.service.operation.register.exception.MessageUniqueIdAlreadyImportedException
import ch.ge.ve.bo.service.operation.register.exception.NoDomainInfluenceException
import ch.ge.ve.bo.service.utils.xml.XSDValidator
import ch.ge.ve.jacksonserializer.JSDates
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import spock.lang.Specification

class RegisterImportServiceImplTest extends Specification {

  def validator = XSDValidator.of("xsd/eCH-0045-4-0.xsd", "catalog.cat")
  def operationDomainInfluenceService = Mock(OperationDomainInfluenceService)
  def operationRepositoryService = Mock(OperationRepositoryService)
  def mapper = new ObjectMapper()
  def metadataService = Mock(RegisterMetadataService)
  def reportBuilder = Mock(RegisterReportBuilder.class)
  def operationService = Mock(OperationService.class)

  def registryImportService = new RegisterImportServiceImpl(validator,
          mapper, metadataService, operationRepositoryService, reportBuilder, operationService)

  def cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def setup() {
    ConnectedUserTestUtils.connectUser()
    SimpleModule module = new SimpleModule()
    module.addSerializer(JSDates.SERIALIZER)
    module.addDeserializer(LocalDateTime.class, JSDates.DESERIALIZER)

    mapper.registerModule(module)

    RegisterMetadata metadata = new RegisterMetadata()
    File file = new File()
    file.setFileName("old.xml")
    file.setOperation(OperationDataset.votingOperation())
    file.setDate(localDateTime('21.05.2055 00:00:00'))
    metadata.setFile(file)
    metadata.setHashTable("tlHBgt3jdopvSx0TTV7M3gcTTnTq59ChIMXymhzlAfU=;0")
    metadata.setMessageUniqueId("test|manufacturer|product|2017-10-20T15:21:57.481|alreadyImported")
    metadata.setReport("""
            {
                "uploader":"test",
                "uploadDate":"JSDate[03.11.2017 11:56:51.000]",
                "emitter":"test",
                "emissionDate":"JSDate[03.11.2017 10:21:01.000]",
                "voterCount":3,
                "doiCount":3,
                "messageUniqueId":"messageID",
                "countingCircleCount":3,
                "fileName":"old.xml",
                "detailedReport":{
                    "domainOfInfluence":"CH, ZH, Ville",
                    "swiss":[{"municipality":"Ville de Zürich","count":1}],
                    "foreigner":[{"municipality":"Ville de Zürich","count":1}],
                    "swissAbroad":[{"lang":"fr","postage-code":1,"count":1}],
                    "municipalities": [{"ofsId": 1234, "name": "Ville de Zürich"}]
                }
            }""")

    metadataService.getAllByFileOperationIdAndValidated(_, _) >> [metadata]
    metadataService.getByOperationIdAndFileMessageUniqueIdAndValidated(_, _, _) >> metadata

    operationDomainInfluenceService.getFile(1l, false) >> genevaDoi()
    operationRepositoryService.getAllDomainOfInfluenceForOperation(1l) >> fedCanCom()

    operationService.findOne(_, false) >> OperationVoDataset.votingOperationVo()
    reportBuilder.buildReport(_, _) >> "test".bytes
  }


  def "should throw an exception if input file is not an XML file"() {
    when: "import is started"
    registryImportService.importRegister(1, "file.doc", null)

    then:
    thrown(InvalidFileException)
  }

  def "should throw an exception if no domain of influence is defined for the operation"() {
    given:
    operationDomainInfluenceService.getFile(2l, false) >> null
    def is = getResourceStream(this.class, "valid.xml")

    when: "import is started"
    registryImportService.importRegister(2l, "file.xml", is)

    then:
    thrown(NoDomainInfluenceException)
  }

  def "A registry cannot be uploaded if the schema is not valid"() {
    given: "a register with invalid XML structure"
    def is = getResourceStream(this.class, "invalid.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.size() == 1
    result.validationErrors.get(0).message.contains("Invalid content was found starting with element 'ech-0045:voterList'")
    result.getFileName() == "file.xml"
  }


  def "A registry cannot be uploaded if there is no municipality id for swiss domestic"() {
    given:
    def is = getResourceStream(this.class, "NoMunicipalityId.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    1 == result.getValidationErrors().size()
    "Cannot find municipality id for voter identified by 0" == result.getValidationErrors()[0].message
  }

  def "An old version registry file cannot be uploaded"() {
    given:
    def is = getResourceStream(this.class, "wrong-ech-version.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    9 == result.getValidationErrors().size()
    ["cvc-elt.1: Cannot find the declaration of element 'voterDelivery'.",
     "cvc-elt.4.2: Cannot resolve 'ech-0045:PersonTypeExtensionType' to a type definition for element 'ech-0045:extension'.",
     "Voter cannot be parsed"] as Set == new HashSet(result.getValidationErrors().message)
  }

  def "A registry cannot be uploaded if the message has already been uploaded"() {
    given: "a register file already uploaded"
    def is = getResourceStream(this.class, "alreadyImported.xml")


    when: "Import is started"
    registryImportService.importRegister(1l, "file.xml", is)

    then:
    thrown(MessageUniqueIdAlreadyImportedException)
  }

  def "A registry cannot be uploaded if the business validation is invalid"() {
    given: "a register file with wrong number of voters "
    def is = getResourceStream(this.class, "wrong_number_of_voters.xml")

    when: "import is started"
    registryImportService.importRegister(1, "file.xml", is)

    then:
    def ex = thrown(InvalidNumberOfVoterException.class)
    ex.getParameters() == ["4", "5"]
  }

  def "A registry can be uploaded but not imported if there are inner and external duplicates"() {
    given: "a register file with duplicates"
    def is = getResourceStream(this.class, "internal_duplicates.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.messageUniqueId == "test|manufacturer|product|2018-05-04T12:16:27.784|messageID"
    result.duplicates.size() == 3
    result.duplicates.get(0).firstName == "Ike"
  }

  def "A registry can be uploaded but not imported if there are external duplicates"() {
    given: "a register file with duplicates"
    def is = getResourceStream(this.class, "external_duplicates.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.messageUniqueId == "test|manufacturer|product|2018-05-04T12:16:27.784|messageID"
    result.duplicates.size() == 2
    result.duplicates.collect { it.fileName }.toSet() == ["old.xml", "file.xml"].toSet()

    result.duplicates.get(0).firstName == "Ike"
  }

  def "A registry cannot be uploaded if there is a duplicate voter id"() {
    given:
    def is = getResourceStream(this.class, "DuplicatePersonId.xml")

    when:
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.size() == 1
    result.validationErrors[0].message.startsWith("Duplicate voter identifier")
  }

  def "A registry cannot be uploaded if a required extension element is missing"() {
    given:
    def is = getResourceStream(this.class, "NoVotingPlace.xml")

    when:
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.size() == 1
    result.validationErrors[0].message.startsWith("Missing extension element")
  }

  def "A registry can be uploaded but not imported if there are voter with undefined DOI"() {
    given: "a register file with one voter with an undefined DOI and one voter with both undefined DOI and defined DOI"
    def is = getResourceStream(this.class, "undefined-doi.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.messageUniqueId == "test|manufacturer|product|2018-05-04T12:16:27.784|messageID"
    result.undefinedDois.size() == 1
    result.undefinedDois.get(0).firstName == "Ike"
    result.undefinedDois.get(0).dois == ["MU.666"]
  }

  def "A registry cannot be imported if there are non e-vote voter"() {
    given:
    def is = getResourceStream(this.class, "not-evote.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.message == ["Voter identified by 0 is not an eVoter"]
  }


  def "A registry cannot be imported if there is voter with multiple counting circles"() {
    given:
    def is = getResourceStream(this.class, "multiple-counting_circle.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.message == ["There is multiple different counting circles for voter identified by 0"]
  }

  def "A registry cannot be imported if there is voter with no counting circle"() {
    given:
    def is = getResourceStream(this.class, "no-counting_circle.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.message == ["There is no counting circle for voter identified by 0"]
  }

  def "A registry cannot be imported if there is voter with an invalid date of birth"() {
    given:
    def is = getResourceStream(this.class, "invalid-date-of-birth.xml")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.message.contains("Voter identified by badYearMonthId has an invalid date of birth")
    result.validationErrors.message.contains("Voter identified by badYear0000Id has an invalid date of birth")
    result.validationErrors.message.contains("Voter identified by badYearMonth00Id has an invalid date of birth")
    result.validationErrors.message.contains("Voter identified by badYearMonthDay00Id has an invalid date of birth")
  }

  def "A valid registry with no duplicate and no missing DOI can be imported"() {
    given: "a valid register file with no duplicate"
    def is = getResourceStream(this.class, "valid.xml")
    def report = ""

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.size() == 0
    result.duplicates.size() == 0
    result.undefinedDois.size() == 0
    result.getFileName() == "file.xml"
    result.getCountingCircleCount() == 3
    result.getEmissionDate().format(DateTimeFormatter.ISO_DATE) == "2018-05-04"
    result.getDoiCount() == 3
    result.getEmitter() == "id.register name"

    1 * reportBuilder.buildReport({
      report = it
    }, _, RegisterReportBuilder.SupportedFormat.PDF)

    JSONAssert.assertEquals(report, '''{
"domainOfInfluence":"CH, GE, VdG",
"swiss":[{"municipality":"GE","count":1}],
"foreigner":[{"municipality":"GE","count":1}],
"swissAbroad":[{"lang":"fr","postage-code":1,"count":1},{"lang":"fr","postage-code":2,"count":1}],
"municipalities":[{"ofsId":6621,"name":"GE"}]}
''', JSONCompareMode.NON_EXTENSIBLE)


    1 * metadataService.store(1, "test|manufacturer|product|2018-05-04T12:16:27.784|messageID", "file.xml", _, _, _, _, _)
  }

  def "A valid registry cannot be imported if user belong to another canton"() {
    given: "a valid register file with no duplicate"
    def is = getResourceStream(this.class, "valid.xml")

    and:
    ConnectedUserTestUtils.connectUser("test", "BE", "Canton de berne")

    when: "import is started"
    def result = registryImportService.importRegister(1, "file.xml", is)

    then:
    result.validationErrors.size() == 1
    "Canton for voter identified by 4 should be BE" == result.validationErrors.get(0).message
    0 * metadataService.store(_, _, _, _, _, _, _, _)
  }


  def "getDetailedReportPDF(id) should return the consolidated register report"() {
    when:
    def fileVo = registryImportService.getDetailedReportPDF(5L)

    then:
    1 * reportBuilder.buildReport(_, _, _) >> "a PDF file".bytes
    fileVo.fileName == "consolidated-voters-report_VP052055_2055-05-21-00h00m00s.pdf"
    fileVo.fileContent == "a PDF file".bytes
  }

  def "getDetailedReportPDF(id, messageId) should return the consolidated register report"() {
    when:
    def fileVo = registryImportService.getDetailedReportPDF(5L, "messageId")

    then:
    1 * reportBuilder.buildReport(_, _, _) >> "a PDF file".bytes
    fileVo.fileName == "voters-report_VP052055_old.xml_2055-05-21-00h00m00s.pdf"
    fileVo.fileContent == "a PDF file".bytes
  }

}
