/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.dataset

import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.service.operation.parameters.repository.model.VotationRepositoryFileVo
import ch.ge.ve.bo.service.operation.parameters.repository.model.VoteInformationVo
import java.nio.charset.StandardCharsets

/**
 * Dataset used for testing {@link VotationRepositoryFileVo} model and service.
 */
class VotationRepositoryFileVoDataset {

  static VotationRepositoryFileVo votationRepositoryFileVo(withContent = true) {
    new VotationRepositoryFileVo(
            1,
            'admin',
            localDateTime('13.12.2016 00:00:00'),
            '202012SGA_Key',
            1,
            FileType.VOTATION_REPOSITORY,
            null,
            'repositoryVotationTest.xml',
            'Canton de Genève',
            withContent ? """<?xml version="1.0" encoding="UTF-8"?>
<ns:delivery xmlns:ns="http://www.ech.ch/xmlns/eCH-0159/4">
    <ns:deliveryHeader>
        <ns1:senderId xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">vota3://all</ns1:senderId>
        <ns1:messageId xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">6c6f4b14b6d349ef97703f2af84ffbc6</ns1:messageId>
        <ns1:messageType xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">vota3://referentielVotation</ns1:messageType>
        <ns1:sendingApplication xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">
            <ns1:manufacturer>DGSI - SIDP</ns1:manufacturer>
            <ns1:product>VOTA3</ns1:product>
            <ns1:productVersion>1.3.1.1-SN</ns1:productVersion>
        </ns1:sendingApplication>
        <ns1:messageDate xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">2017-06-16T14:08:33.596Z</ns1:messageDate>
        <ns1:action xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/4">1</ns1:action>
        <ns1:testDeliveryFlag xmlns:ns1="http://www.ech.ch/xmlns/eCH-0058/5">false</ns1:testDeliveryFlag>
    </ns:deliveryHeader>
    <ns:initialDelivery>
        <ns:contest>
            <ns1:contestIdentification xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">202012SGA</ns1:contestIdentification>
            <ns1:contestDate xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">2020-12-01+01:00</ns1:contestDate>
            <ns1:contestDescription xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">
                <ns1:contestDescriptionInfo>
                    <ns1:language>fr</ns1:language>
                    <ns1:contestDescription>202012SGA</ns1:contestDescription>
                </ns1:contestDescriptionInfo>
            </ns1:contestDescription>
        </ns:contest>
        <ns:voteInformation>
            <ns:vote>
                <ns1:voteIdentification xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">202012SGA-COM-6624</ns1:voteIdentification>
                <ns1:domainOfInfluenceIdentification xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">6624</ns1:domainOfInfluenceIdentification>
            </ns:vote>
            <ns:ballot>
                <ns1:ballotIdentification xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">COM_6624_Q1</ns1:ballotIdentification>
                <ns1:ballotPosition xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">1</ns1:ballotPosition>
                <ns1:standardBallot xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">
                    <ns1:questionIdentification>COM_6624_Q1</ns1:questionIdentification>
                    <ns1:answerType>
                        <ns1:answerType>2</ns1:answerType>
                    </ns1:answerType>
                    <ns1:ballotQuestion>
                        <ns1:ballotQuestionInfo>
                            <ns1:language>fr</ns1:language>
                            <ns1:ballotQuestion><![CDATA[<b>Sujet </b>01 de <u>Gy</u>]]></ns1:ballotQuestion>
                        </ns1:ballotQuestionInfo>
                    </ns1:ballotQuestion>
                </ns1:standardBallot>
                <ns1:extension xmlns:ns1="http://www.ech.ch/xmlns/eCH-0155/4">
                    <questionNumberLabel>1</questionNumberLabel>
                </ns1:extension>
            </ns:ballot>
        </ns:voteInformation>
    </ns:initialDelivery>
</ns:delivery>""".getBytes(StandardCharsets.UTF_8) : null,
            '202012SGA',
            localDateTime('01.12.2020 00:00:00'),
            '202012SGA',
            [
                    new VoteInformationVo(
                            '202012SGA-COM-6624',
                            '6624',
                            1,
                            'COM_6624_Q1',
                            'repository.edit.detail-grid.answer-type-string.2'
                    )
            ],
    )
  }
}
