/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.utils

import java.util.stream.Stream
import spock.lang.Specification

class HashUtilsTest extends Specification {
    def "ComputeFilesHash should always return same hash whatever the order of files are"() {
        given: "2 set of files with different orders"
        def files1 = Stream.of("1", "2", "3")
        def files2 = Stream.of("2", "1", "3")

        when: "computing hashes"
        def hash1 = HashUtils.computeFilesHash(files1)
        def hash2 = HashUtils.computeFilesHash(files2)

        then: "Hashes are equal"
        hash1 == hash2
    }

    def "Hash is always less than 128 char long"() {
        when:
        def hash = HashUtils.toHash(randomString())

        then: "Hashes are equal"
        hash.length() <= 128

    }


    private String randomString() {
        Random r = new Random()
        StringBuilder buf = new StringBuilder()
        for (int i = 0; i < r.nextInt(); i++) {
            buf.append((char) (r.nextInt((int) (Character.MAX_VALUE))))
        }
        return buf.toString()
    }


    private String

}
