/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.doi

import static ch.ge.ve.bo.service.dataset.DomainOfInfluenceFileVoDataset.validDomainOfInfluenceFileVo
import static ch.ge.ve.bo.service.dataset.FileVoDataset.doiApplicationFileVo

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.repository.ConnectedUser
import ch.ge.ve.bo.repository.dataset.FileDataset
import ch.ge.ve.bo.service.exception.TechnicalException
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.utils.xml.XSDValidator
import ch.ge.ve.interfaces.ech.service.EchCodec
import ch.ge.ve.interfaces.ech.service.EchDeserializationRuntimeException
import spock.lang.Specification

class OperationDomainInfluenceServiceTest extends Specification {

  def logisticFactoryMock = Mock(EchCodec)
  def fileServiceMock = Mock(FileService)

  def operationDomainInfluenceService = new OperationDomainInfluenceServiceImpl(fileServiceMock, XSDValidator.of("xsd/logistic-delivery.xsd", "catalog.cat"))

  def setupSpec() {
    Locale.setDefault(Locale.ENGLISH)
    ConnectedUser.technical()
  }

  def "import a domain of influence should save the file in database"() {
    given: 'a valid DOI input stream and file name'
    def fileName = "valid-doi.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)
    fileServiceMock.getFiles(1, false, Collections.singleton(FileType.DOMAIN_OF_INFLUENCE)) >> []

    when: 'calling import service'
    def importResult = operationDomainInfluenceService.importFile(1, fileName, xmlFileStream)

    then: 'corresponding services are called'
    1 * fileServiceMock.importFile(_, false) >> doiApplicationFileVo()
    0 * fileServiceMock.deleteFile(_)


    and: 'there was no validation error'
    importResult.validationErrors.isEmpty()

    and: 'the imported file information is returned'
    importResult.domainOfInfluenceFile.businessKey == validDomainOfInfluenceFileVo(false).businessKey
    importResult.domainOfInfluenceFile.fileContent == null
  }

  def "import a domain of influence should override the file in database if present"() {
    given: 'a valid DOI input stream and file name'
    def fileName = "valid-doi.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)
    fileServiceMock.getFiles(1, false, Collections.singleton(FileType.DOMAIN_OF_INFLUENCE)) >> [doiApplicationFileVo(new FileDataset.Options().with(FileDataset.Options.operationId, 1))]

    when: 'calling import service'
    def importResult = operationDomainInfluenceService.importFile(1, fileName, xmlFileStream)

    then: 'corresponding services are called'
    1 * fileServiceMock.importFile(_, false) >> doiApplicationFileVo()
    1 * fileServiceMock.deleteFile(_)

    and: 'there was no validation error'
    importResult.validationErrors.isEmpty()

    and: 'the imported file information is returned'
    importResult.domainOfInfluenceFile.businessKey == validDomainOfInfluenceFileVo(false).businessKey
    importResult.domainOfInfluenceFile.fileContent == null
  }

  def 'a EchDeserializationRuntimeException should be wrapped in a TechnicalException'() {
    given:
    operationDomainInfluenceService.setLogisticFactory(logisticFactoryMock)
    def fileName = "valid-doi.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)
    logisticFactoryMock.deserialize(_, false) >> { throw new EchDeserializationRuntimeException('JAXB error') }
    fileServiceMock.getFiles(1, FileType.DOMAIN_OF_INFLUENCE, null) >> []

    when:
    operationDomainInfluenceService.importFile(1, fileName, xmlFileStream)

    then:
    thrown(TechnicalException)
  }

  def "invalid DOI type should report an error"() {
    given: 'an invalid XML file'
    def fileName = "invalid-doi-type.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    when: 'calling import service'
    def importResult = operationDomainInfluenceService.importFile(1, fileName, xmlFileStream)

    then: 'an error should be reported'
    importResult.domainOfInfluenceFile == null
    importResult.validationErrors.size() == 2
    importResult.validationErrors[0].message == 'cvc-enumeration-valid: Value \'XX\' is not facet-valid with respect to enumeration \'[CH, CT, BZ, MU, SC, KI, OG, KO, SK, AN]\'. It must be a value from the enumeration.'
    importResult.validationErrors[1].message == 'cvc-type.3.1.3: The value \'XX\' of element \'ns3:domainOfInfluenceType\' is not valid.'
  }

  def "invalid DOI ID should report an error"() {
    given: 'an invalid XML file'
    def fileName = "invalid-doi-id.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    when: 'calling import service'
    def importResult = operationDomainInfluenceService.importFile(1, fileName, xmlFileStream)

    then: 'an error should be reported'
    importResult.domainOfInfluenceFile == null
    importResult.validationErrors.size() == 2
    importResult.validationErrors[0].message == 'cvc-minLength-valid: Value \'\' with length = \'0\' is not facet-valid with respect to minLength \'1\' for type \'domainOfInfluenceIdentificationType\'.'
    importResult.validationErrors[1].message == 'cvc-type.3.1.3: The value \'\' of element \'ns3:localDomainOfInfluenceIdentification\' is not valid.'
  }

  def "invalid DOI short name should report an error"() {
    given: 'an invalid XML file'
    def fileName = "invalid-doi-shortname.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    when: 'calling import service'
    def importResult = operationDomainInfluenceService.importFile(1, fileName, xmlFileStream)

    then: 'an error should be reported'
    importResult.domainOfInfluenceFile == null
    importResult.validationErrors.size() == 2
    importResult.validationErrors[0].message == 'cvc-maxLength-valid: Value \'Lorem Ipsum\' with length = \'11\' is not facet-valid with respect to maxLength \'5\' for type \'domainOfInfluenceShortnameType\'.'
    importResult.validationErrors[1].message == 'cvc-type.3.1.3: The value \'Lorem Ipsum\' of element \'ns3:domainOfInfluenceShortname\' is not valid.'
  }

  def "definition without any DOI should report an error"() {
    given: 'an invalid XML file'
    def fileName = "invalid-doi-missing-doi.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    when: 'calling import service'
    def importResult = operationDomainInfluenceService.importFile(1, fileName, xmlFileStream)

    then: 'an error should be reported'
    importResult.domainOfInfluenceFile == null
    importResult.validationErrors.size() == 1
    importResult.validationErrors[0].message == 'cvc-complex-type.2.4.b: The content of element \'ns2:delivery\' is not complete. One of \'{"http://www.ech.ch/xmlns/logistic/1":domainOfInfluence}\' is expected.'
  }

  def "definition without a delivery header should raise an error"() {
    given: 'an invalid XML file'
    def fileName = "invalid-doi-missing-delivery-header.xml"
    InputStream xmlFileStream = this.getClass().getResourceAsStream(fileName)

    when: 'calling import service'
    def importResult = operationDomainInfluenceService.importFile(1, fileName, xmlFileStream)

    then: 'an error should be reported'
    importResult.domainOfInfluenceFile == null
    importResult.validationErrors.size() == 3
    importResult.validationErrors[0].message == 'cvc-complex-type.2.4.a: Invalid content was found starting with element \'ns2:domainOfInfluence\'. One of \'{"http://www.ech.ch/xmlns/logistic/1":deliveryHeader}\' is expected.'
    importResult.validationErrors[1].message == 'cvc-minLength-valid: Value \'\' with length = \'0\' is not facet-valid with respect to minLength \'1\' for type \'domainOfInfluenceIdentificationType\'.'
    importResult.validationErrors[2].message == 'cvc-type.3.1.3: The value \'\' of element \'ns3:localDomainOfInfluenceIdentification\' is not valid.'

  }

  def "getFile(id, true) should return the DOI file with its content if present"() {
    when:
    def doiFile = operationDomainInfluenceService.getFile(5L, true)

    then:
    1 * fileServiceMock.getFiles(5L, FileType.DOMAIN_OF_INFLUENCE, null) >> [doiApplicationFileVo()]
    doiFile.businessKey == validDomainOfInfluenceFileVo().businessKey
  }

  def "getFile(id, false) should return the DOI file without its content if present"() {
    when:
    def doiFile = operationDomainInfluenceService.getFile(5L, false)

    then:
    1 * fileServiceMock.getFiles(5L, FileType.DOMAIN_OF_INFLUENCE, null) >> [doiApplicationFileVo()]
    doiFile.businessKey == validDomainOfInfluenceFileVo(false).businessKey
  }

  def "getFile should return null if no file is present"() {
    when:
    def doiFile = operationDomainInfluenceService.getFile(5L, true)

    then:
    1 * fileServiceMock.getFiles(5L, FileType.DOMAIN_OF_INFLUENCE, null) >> []
    doiFile == null
  }

  def "EchDeserializationRuntimeException during getFile should be wrapped in a TechnicalException"() {
    given:
    operationDomainInfluenceService.setLogisticFactory(logisticFactoryMock)
    logisticFactoryMock.deserialize(_, _) >> { throw new EchDeserializationRuntimeException('parsing error') }

    when:
    operationDomainInfluenceService.getFile(5L, true)

    then:
    1 * fileServiceMock.getFiles(5L, FileType.DOMAIN_OF_INFLUENCE, null) >> [doiApplicationFileVo()]
    thrown(TechnicalException)
  }
}
