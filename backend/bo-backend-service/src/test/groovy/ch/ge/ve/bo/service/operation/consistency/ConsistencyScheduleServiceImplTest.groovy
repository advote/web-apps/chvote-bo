/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency


import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import spock.lang.Specification

class ConsistencyScheduleServiceImplTest extends Specification {


    def scheduledExecutorService = Mock(ScheduledExecutorService)

    def runner = Mock(ConsistencyServiceRunnerImpl)
    def consistencyScheduleService = new ConsistencyScheduleServiceImpl(
            { operationId -> runner },
            scheduledExecutorService)


    def "ScheduleCheck run the runner created"() {
        given:
        scheduledExecutorService.schedule( _ as Runnable, _ as Long, _ as TimeUnit) >> {args -> args[0].run() }

        when:
        consistencyScheduleService.scheduleCheck(1l)

        then:
        1 * runner.run()
    }

}
