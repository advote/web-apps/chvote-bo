/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.management.entity

import static ch.ge.ve.bo.repository.dataset.FileDataset.Options
import static ch.ge.ve.bo.repository.dataset.FileDataset.Options.getFileType
import static ch.ge.ve.bo.repository.dataset.FileDataset.Options.managementEntity

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.dataset.FileVoDataset
import ch.ge.ve.bo.service.exception.TechnicalException
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.operation.consistency.ConsistencyScheduleService
import ch.ge.ve.bo.service.operation.exception.CannotRevokeManagementEntityException
import spock.lang.Specification

class ManagementEntityServiceImplTest extends Specification {

  def operationRepository = Mock(SecuredOperationRepository)

  def fileService = Mock(FileService)
  def service = new ManagementEntityServiceImpl(TestHelpers.objectMapper, operationRepository, Mock(ConsistencyScheduleService), fileService)

  void setup() {
    ConnectedUserTestUtils.connectUser()
  }

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def "Invite should add management entities if they belongs to the canton"() {
    given:
    Operation ope = OperationDataset.votingOperation()
    ope.setGuestManagementEntities(["Bardonnex"].toSet())
    ope.setManagementEntity("Canton de Genève")
    operationRepository.findOne(1, true) >> ope
    Operation savedOperation = null

    when:
    service.invite(1, ["Gy"])

    then:

    1 * operationRepository.markConfigurationAsUpdated(1, _)
    1 * operationRepository.save { savedOperation = it }
    ["Bardonnex", "Gy"].toSet() == savedOperation.guestManagementEntities
  }

  def "Invite should fail if user don't belongs to same management entity than the operation"() {
    given:
    Operation ope = OperationDataset.votingOperation()
    ope.setGuestManagementEntities(["Bardonnex"].toSet())
    ope.setManagementEntity("Another one")
    operationRepository.findOne(1, true) >> ope

    when:
    service.invite(1, ["Gy"])

    then:
    TechnicalException exception = thrown()
    exception.message == "Try to change guest management entity of an operation created by management entities Another one by a user belonging to Canton de Genève"
    0 * operationRepository.markConfigurationAsUpdated(1, _)
    0 * operationRepository.save(_)
  }

  def "Invite should fail if management entities don't belongs to the canton"() {
    given:
    Operation ope = OperationDataset.votingOperation()
    ope.setGuestManagementEntities(["Bardonnex"].toSet())
    operationRepository.findOne(1, true) >> ope

    when:
    service.invite(1, ["Dunkerke"])

    then:
    TechnicalException exception = thrown()
    exception.message == "Some management entities from [Dunkerke] don't belongs to user's canton GE"
    0 * operationRepository.markConfigurationAsUpdated(1, _)
    0 * operationRepository.save(_)
  }

  def "Revoke should remove management entities if they belongs to the canton"() {
    given:
    Operation ope = OperationDataset.votingOperation()
    ope.setGuestManagementEntities(["Bardonnex", "Gy"].toSet())
    ope.setManagementEntity("Canton de Genève")
    operationRepository.findOne(1, true) >> ope
    Operation savedOperation = null

    when:
    service.revoke(1, ["Gy"])

    then:
    1 * fileService.getFiles(1, false, EnumSet.of(FileType.VOTATION_REPOSITORY, FileType.ELECTION_REPOSITORY, FileType.REGISTER)) >> []
    1 * operationRepository.markConfigurationAsUpdated(1, _)
    1 * operationRepository.save { savedOperation = it }
    ["Bardonnex"].toSet() == savedOperation.guestManagementEntities
  }

  def "Revoke should fail if user don't belongs to same management entity than the operation"() {
    given:
    Operation ope = OperationDataset.votingOperation()
    ope.setGuestManagementEntities(["Bardonnex"].toSet())
    ope.setManagementEntity("Another one")
    operationRepository.findOne(1, true) >> ope

    when:
    service.revoke(1, ["Gy"])

    then:
    TechnicalException exception = thrown()
    exception.message == "Try to change guest management entity of an operation created by management entities Another one by a user belonging to Canton de Genève"
    0 * operationRepository.markConfigurationAsUpdated(1, _)
    0 * operationRepository.save(_)
  }

  def "Revoke should fail if one revoked entity has already imported repository files"() {
    given:
    Operation ope = OperationDataset.votingOperation()
    ope.setGuestManagementEntities(["Bardonnex", "Gy"].toSet())
    ope.setManagementEntity("Canton de Genève")
    operationRepository.findOne(1, true) >> ope

    when:
    service.revoke(1, ["Bardonnex", "Gy"])

    then:
    1 * fileService.getFiles(1, false, EnumSet.of(FileType.VOTATION_REPOSITORY, FileType.ELECTION_REPOSITORY, FileType.REGISTER)) >> [
            FileVoDataset.someFileVo(new Options().with(fileType, FileType.VOTATION_REPOSITORY).with(managementEntity, "Gy"))
    ]
    CannotRevokeManagementEntityException exception = thrown()
    exception.messageKey == "parameters.management-entity.dialog.error.cannot-revoke.one"
    exception.parameters == ["Gy"]
    0 * operationRepository.markConfigurationAsUpdated(1)
    0 * operationRepository.save(_)
  }

  def "Revoke should fail if one revoked entity has already imported register files"() {
    given:
    Operation ope = OperationDataset.votingOperation()
    ope.setGuestManagementEntities(["Bardonnex", "Gy"].toSet())
    ope.setManagementEntity("Canton de Genève")
    operationRepository.findOne(1, true) >> ope

    when:
    service.revoke(1, ["Bardonnex", "Gy"])

    then:
    1 * fileService.getFiles(1, false, EnumSet.of(FileType.ELECTION_REPOSITORY, FileType.VOTATION_REPOSITORY, FileType.REGISTER)) >> [
            FileVoDataset.someFileVo(new Options().with(fileType, FileType.REGISTER).with(managementEntity, "Gy"))
    ]
    CannotRevokeManagementEntityException exception = thrown()
    exception.messageKey == "parameters.management-entity.dialog.error.cannot-revoke.one"
    exception.parameters == ["Gy"]
    0 * operationRepository.markConfigurationAsUpdated(1, _)
    0 * operationRepository.save(_)
  }

  def "Revoke should fail if several revoked entities have already imported files"() {
    given:
    Operation ope = OperationDataset.votingOperation()
    ope.setGuestManagementEntities(["Bardonnex", "Gy"].toSet())
    ope.setManagementEntity("Canton de Genève")
    operationRepository.findOne(1, true) >> ope

    when:
    service.revoke(1, ["Bardonnex", "Gy"])

    then:
    1 * fileService.getFiles(1, false, EnumSet.of(FileType.ELECTION_REPOSITORY, FileType.VOTATION_REPOSITORY, FileType.REGISTER)) >> [
            FileVoDataset.someFileVo(new Options().with(fileType, FileType.ELECTION_REPOSITORY).with(managementEntity, "Gy")),
            FileVoDataset.someFileVo(new Options().with(fileType, FileType.REGISTER).with(managementEntity, "Bardonnex"))
    ]
    CannotRevokeManagementEntityException exception = thrown()
    exception.messageKey == "parameters.management-entity.dialog.error.cannot-revoke.several"
    exception.parameters == ["Gy, Bardonnex"]
    0 * operationRepository.markConfigurationAsUpdated(1)
    0 * operationRepository.save(_)
  }

  def "getAllManagementEntities should return the list of management entities"() {
    when:
    def values = service.allManagementEntities

    then:
    45 == values.length
  }
}
