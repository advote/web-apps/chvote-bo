/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model

import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorType.CONF_UNKNOWN_DOI_FOR_TESTING_CARD_LOT
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorType.VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY

import ch.ge.ve.bo.service.utils.OperationStatusBuilder
import spock.lang.Specification

class OperationStatusAndConsistencyVoTest extends Specification {
    def "IsXXXCompleteAndConsistent should return true if complete and the operation passes all consistency checks"() {
        when:
        def operationStatusAndConsistency = new OperationStatusAndConsistencyVo(
                new OperationStatusBuilder()
                        .withConfigurationComplete()
                        .withVotingMaterialComplete()
                        .build(),
                new ConsistencyResultVo())

        then:
        assert operationStatusAndConsistency.configurationCompleteAndConsistent
        assert operationStatusAndConsistency.votingMaterialCompleteAndConsistent
    }


    def "IsXXXCompleteAndConsistent should return false if incomplete even if the operation passes all consistency checks"() {
        when:
        def operationStatusAndConsistency = new OperationStatusAndConsistencyVo(
                new OperationStatusBuilder()
                        .build(),
                new ConsistencyResultVo())

        then:
        assert !operationStatusAndConsistency.configurationCompleteAndConsistent
        assert !operationStatusAndConsistency.votingMaterialCompleteAndConsistent
    }

    def "IsXXXCompleteAndConsistent should return false if complete but there is failing consistency checks"() {
        when:
        def operationStatusAndConsistency = new OperationStatusAndConsistencyVo(
                new OperationStatusBuilder()
                        .withConfigurationComplete()
                        .withVotingMaterialComplete()
                        .build(),
                new ConsistencyResultVo()
                        .addError(CONF_UNKNOWN_DOI_FOR_TESTING_CARD_LOT)
                        .addError(VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY))

        then:
        assert !operationStatusAndConsistency.configurationCompleteAndConsistent
        assert !operationStatusAndConsistency.votingMaterialCompleteAndConsistent
    }


    def "IsXXXCompleteAndConsistent should return false if computation of consistency has not been done"() {
        when:
        def operationStatusAndConsistency = new OperationStatusAndConsistencyVo(
                new OperationStatusBuilder()
                        .withConfigurationComplete()
                        .withVotingMaterialComplete()
                        .build(),
                null)

        then:
        assert !operationStatusAndConsistency.configurationCompleteAndConsistent
        assert !operationStatusAndConsistency.votingMaterialCompleteAndConsistent
    }


    def "getXXXConsistencyErrors should return only errors relative to there scope"() {
        when:
        def operationStatusAndConsistency = new OperationStatusAndConsistencyVo(
                new OperationStatusBuilder()
                        .withConfigurationComplete()
                        .withVotingMaterialComplete()
                        .build(),
                new ConsistencyResultVo()
                        .addError(CONF_UNKNOWN_DOI_FOR_TESTING_CARD_LOT)
                        .addError(VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY))

        then:
        1 == operationStatusAndConsistency.getConfigurationConsistencyErrors().size()
        CONF_UNKNOWN_DOI_FOR_TESTING_CARD_LOT == operationStatusAndConsistency.getConfigurationConsistencyErrors().get(0).errorType
        ['domain-of-influence': true, 'testing-card-lot': true] == operationStatusAndConsistency.configurationStatus.sectionsInError

        1 == operationStatusAndConsistency.getVotingMaterialConsistencyErrors().size()
        VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY == operationStatusAndConsistency.getVotingMaterialConsistencyErrors().get(0).errorType
        ['printer-template': true, 'register': true] == operationStatusAndConsistency.votingMaterialStatus.sectionsInError
    }

    def "isConsistencyComputationInProgress should return true if consistency has not been done"() {
        when:
        def operationStatusAndConsistency = new OperationStatusAndConsistencyVo(
                new OperationStatusBuilder()
                        .withConfigurationComplete()
                        .withVotingMaterialComplete()
                        .build(),
                null)

        then:
        assert operationStatusAndConsistency.isConsistencyComputationInProgress()
    }


}
