/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status.complete

import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.RegisterMetadata
import ch.ge.ve.bo.service.operation.register.RegisterMetadataService
import spock.lang.Specification

class RegisterCompleteTest extends Specification {
  def metadataService = Mock(RegisterMetadataService)
  def service = new RegisterComplete(metadataService)

  def "IsSectionComplete is true when there is a register file"() {
    given:
    def operation = OperationDataset.electoralOperation()
    metadataService.getAllByFileOperationIdAndValidated(operation.id, true) >> [
            Mock(RegisterMetadata)
    ]

    expect:
    service.isSectionComplete(operation)
  }


  def "IsSectionComplete is false when there is no register file"() {
    given:
    def operation = OperationDataset.electoralOperation()
    metadataService.getAllByFileOperationIdAndValidated(operation.id, true) >> []
    expect:
    !service.isSectionComplete(operation)
  }

  def "sectionName is register"() {
    expect:
    service.sectionName == "register"
  }

}
