/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.model

import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage.INITIALIZE_PARAMETERS
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialCreationStage.SEND_VOTERS

import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.chvote.pactback.contract.operation.status.ProtocolStageProgressVo
import java.time.Clock
import java.time.LocalDateTime
import java.time.ZoneId
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import spock.lang.Specification

class VotingMaterialCreationProgressTest extends Specification {


  def "create an object and transform to JSON"() {

    given:
    VotingMaterialCreationProgress.clock = Clock.fixed(LocalDateTime.of(2018, 1, 1, 10, 26).atZone(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault())
    List<ProtocolStageProgressVo> protocolProgress = new ArrayList<>()
    protocolProgress.add(new ProtocolStageProgressVo(INITIALIZE_PARAMETERS, -1, 1, LocalDateTime.of(2018, 1, 1, 10, 00), LocalDateTime.of(2018, 1, 1, 10, 05)))
    protocolProgress.add(new ProtocolStageProgressVo(SEND_VOTERS, 0, 0.5d, LocalDateTime.of(2018, 1, 1, 10, 05), LocalDateTime.of(2018, 1, 1, 10, 15)))
    protocolProgress.add(new ProtocolStageProgressVo(SEND_VOTERS, 1, 0.3d, LocalDateTime.of(2018, 1, 1, 10, 05), LocalDateTime.of(2018, 1, 1, 10, 25)))
    def creationProgress = new VotingMaterialCreationProgress(protocolProgress)


    when:
    def json = TestHelpers.getObjectMapper().writeValueAsString(creationProgress)


    then:
    JSONAssert.assertEquals(json, TestHelpers.getResourceString(VotingMaterialCreationProgressTest, "VotingMaterialCreationProgressTest.json"), JSONCompareMode.NON_EXTENSIBLE)


  }

}
