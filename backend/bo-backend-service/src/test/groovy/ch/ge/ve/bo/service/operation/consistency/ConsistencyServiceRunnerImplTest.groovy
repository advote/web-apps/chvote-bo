/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency

import ch.ge.ve.bo.repository.OperationConsistencyRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.repository.entity.OperationConsistency
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo
import ch.ge.ve.bo.service.operation.model.ConsistencyResultVo
import ch.ge.ve.bo.service.operation.model.OperationStatusVo
import ch.ge.ve.bo.service.operation.status.OperationStatusService
import java.time.LocalDateTime
import java.time.Period
import org.skyscreamer.jsonassert.JSONAssert
import spock.lang.Specification

class ConsistencyServiceRunnerImplTest extends Specification {
    long operationId = 1
    SecuredOperationRepository operationRepository = Mock(SecuredOperationRepository)
    OperationConsistencyRepository operationConsistencyRepository = Mock(OperationConsistencyRepository)
    OperationStatusService operationStatusService = Mock(OperationStatusService)
    List<ConsistencyChecker> checkerList = [new ConsistencyChecker() {
        @Override
        ConsistencyResultVo checkForOperation(OperationStatusVo operationStatus, Operation operation) {
            return new ConsistencyResultVo().addError(ConsistencyErrorVo.ConsistencyErrorType.VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY, ConsistencyResultVo.param("key", "test"))
        }
    }]
    def runner = new ConsistencyServiceRunnerImpl(operationId, operationRepository, operationConsistencyRepository, TestHelpers.objectMapper, operationStatusService, checkerList)

    def "run() WHEN there is a previous outdated consistency result it should store and compute a new one"() {
        String actualJson = null

        given: "An operation"
        def operationLastUpdate = LocalDateTime.now()
        def operation = OperationDataset.electoralOperation()
        operation.lastVotingMaterialUpdateDate = operationLastUpdate - Period.ofDays(10)
        operation.lastConfigurationUpdateDate = operationLastUpdate
        operationRepository.findOne(1, false) >> operation

        and: "A previously computed but outdated consistency result"
        def operationConsistency = new OperationConsistency()
        operationConsistency.setOperation(operation)
        operationConsistency.setReport(TestHelpers.getResourceString(ConsistencyServiceImplTest.class, "ConsistencyResult.json"))
        operationConsistency.setComputationDate(operationLastUpdate.minusDays(1))
        operationConsistencyRepository.findByOperationId(1) >> Optional.of(operationConsistency)

        when: "Compute result"
        runner.run()

        then:
        1 * operationConsistencyRepository.save { OperationConsistency result -> actualJson = result.getReport() }
        JSONAssert.assertEquals(TestHelpers.getResourceString(ConsistencyServiceRunnerImplTest, "runnerTest.json"), actualJson, true)
    }

    def "run() WHEN there is not a previous consistency result it should store and compute a new one"() {
        String actualJson = null

        given: "An operation"
        def operationLastUpdate = LocalDateTime.now()
        def operation = OperationDataset.electoralOperation()
        operation.lastVotingMaterialUpdateDate = operationLastUpdate - Period.ofDays(10)
        operation.lastConfigurationUpdateDate = operationLastUpdate
        operationRepository.findOne(1, false) >> operation
        operationConsistencyRepository.findByOperationId(1) >> Optional.empty()
        when: "Compute result"
        runner.run()

        then:
        1 * operationConsistencyRepository.save { OperationConsistency result -> actualJson = result.getReport() }
        JSONAssert.assertEquals(TestHelpers.getResourceString(ConsistencyServiceRunnerImplTest, "runnerTest.json"), actualJson, true)
    }

    def "run() WHEN there is a previous consistency result that is not outdated it should not compute a new one"() {
        given: "An operation"
        def operationLastUpdate = LocalDateTime.now()
        def operation = OperationDataset.electoralOperation()
        operation.lastVotingMaterialUpdateDate = operationLastUpdate - Period.ofDays(10)
        operation.lastConfigurationUpdateDate = operationLastUpdate
        operationRepository.findOne(1, false) >> operation

        and: "A previously computed but not outdated consistency result"
        def operationConsistency = new OperationConsistency()
        operationConsistency.setOperation(operation)
        operationConsistency.setReport(TestHelpers.getResourceString(ConsistencyServiceImplTest.class, "ConsistencyResult.json"))
        operationConsistency.setComputationDate(operationLastUpdate)
        operationConsistencyRepository.findByOperationId(1) >> Optional.of(operationConsistency)

        when: "Compute result"
        runner.run()

        then:
        0 * operationConsistencyRepository.save(_)
    }

}
