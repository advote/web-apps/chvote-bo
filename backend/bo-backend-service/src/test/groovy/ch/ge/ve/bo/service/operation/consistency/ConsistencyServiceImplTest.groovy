/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency

import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorType.VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY

import ch.ge.ve.bo.repository.OperationConsistencyRepository
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.OperationConsistency
import ch.ge.ve.bo.service.TestHelpers
import java.time.Clock
import java.time.Instant
import java.time.LocalDateTime
import java.time.Period
import java.time.ZoneId
import spock.lang.Specification
import spock.lang.Unroll

class ConsistencyServiceImplTest extends Specification {


    def operationRepository = Mock(SecuredOperationRepository)
    def operationConsistencyRepository = Mock(OperationConsistencyRepository)
    def consistencyScheduleService = Mock(ConsistencyScheduleService)
    def clock = Clock.fixed(Instant.EPOCH + Period.ofDays(10), ZoneId.systemDefault())
    def consistencyService = new ConsistencyServiceImpl(
            operationRepository,
            operationConsistencyRepository,
            TestHelpers.objectMapper,
            2 * 24 * 60 * 60 * 1000,
            consistencyScheduleService,
            clock)


    @Unroll
    def "GetConsistencyResultIfAvailable WHEN report is outdated : #reportOutDated and timeout Expired: #timeoutExpired THEN should return report : #shouldReturnReport and should schedule a check : #shouldScheduleACheck"() {
        given: "An operation"

        def now = LocalDateTime.now(clock)
        def operationLastUpdate = timeoutExpired ? now.minusDays(5) : now.minusDays(1)
        def reportDate = reportOutDated ? operationLastUpdate.minusDays(1) : operationLastUpdate

        def operation = OperationDataset.electoralOperation()
        operation.lastVotingMaterialUpdateDate = operationLastUpdate
        operation.lastConfigurationUpdateDate = LocalDateTime.now(clock) - Period.ofDays(10)

        operationRepository.findOne(1, false) >> operation


        and: "A previously computed consistency result exists for this version of the operation"
        def operationConsistency = new OperationConsistency()
        operationConsistency.setOperation(operation)
        operationConsistency.setReport(TestHelpers.getResourceString(ConsistencyServiceImplTest.class, "ConsistencyResult.json"))
        operationConsistency.setComputationDate(reportDate)
        operationConsistencyRepository.findByOperationId(1) >> Optional.of(operationConsistency)

        when: "Compute result"
        def result = consistencyService.getConsistencyResultIfAvailable(1)

        then:
        (shouldReturnReport ? 1 : null) == result?.consistencyErrors?.size()
        (shouldReturnReport ? VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY : null) == result?.consistencyErrors?.get(0)?.errorType
        (shouldScheduleACheck ? 1 : 0) * consistencyScheduleService.scheduleCheck(1)

        where:
        reportOutDated | timeoutExpired | shouldReturnReport | shouldScheduleACheck
        true           | true           | false              | true
        true           | false          | false              | false
        false          | true           | true               | false
        false          | false          | true               | false

    }

    @Unroll
    def "GetConsistencyResultIfAvailable WHEN there is no report and timeout Expired: #timeoutExpired THEN should return null and schedule a check : #shouldScheduleACheck"() {
        given: "An operation"
        def now = LocalDateTime.now(clock)
        def operationLastUpdate = timeoutExpired ? now.minusDays(5) : now.minusDays(1)
        operationConsistencyRepository.findByOperationId(1) >> Optional.empty()
        def operation = OperationDataset.electoralOperation()
        operation.lastVotingMaterialUpdateDate = operationLastUpdate
        operation.lastConfigurationUpdateDate = LocalDateTime.now(clock) - Period.ofDays(10)
        operationRepository.findOne(1, false) >> operation

        when: "Compute result"
        def result = consistencyService.getConsistencyResultIfAvailable(1)

        then:
        null == result
        (shouldScheduleACheck ? 1 : 0) * consistencyScheduleService.scheduleCheck(1)

        where:
        timeoutExpired | shouldScheduleACheck
        true           | true
        false          | false

    }


}
