/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status.complete

import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.service.model.VoterTestingCardsLotVo
import ch.ge.ve.bo.service.testing.card.VoterTestingCardsLotService
import spock.lang.Specification

class ConfigurationTestingCardLotCompleteTest extends Specification {
  def voterTestingCardsLotService = Mock(VoterTestingCardsLotService)
  def service = new ConfigurationTestingCardLotComplete(voterTestingCardsLotService)

  def "IsSectionComplete is true when there is a least one lot"() {
    given:
    def operation = OperationDataset.electoralOperation()
    voterTestingCardsLotService.findAll(operation.id, true) >> [
            Mock(VoterTestingCardsLotVo)
    ]

    expect:
    service.isSectionComplete(operation)
  }


  def "IsSectionComplete is false when there is no lot"() {
    given:
    def operation = OperationDataset.electoralOperation()
    voterTestingCardsLotService.findAll(operation.id, true) >> []

    expect:
    !service.isSectionComplete(operation)
  }

  def "sectionName is repository"() {
    expect:
    service.sectionName == "testing-card-lot"
  }


}
