/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status.complete

import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.service.operation.parameters.election.ElectionPagePropertiesService
import ch.ge.ve.bo.service.operation.parameters.repository.OperationRepositoryService
import spock.lang.Specification

class ElectionPagePropertiesCompleteTest extends Specification {
  OperationRepositoryService repositoryService = Mock(OperationRepositoryService)
  ElectionPagePropertiesService electionPagePropertiesService = Mock(ElectionPagePropertiesService)
  def service = new ElectionPagePropertiesComplete(repositoryService, electionPagePropertiesService)

  def "IsSectionComplete is true when all mappings have been correctly defined"() {
    given:
    def operation = OperationDataset.electoralOperation()
    repositoryService.getAllBallotsForOperation(operation.id, true, false) >> ["b1", "b2"]
    electionPagePropertiesService.getBallotToModelIdMapping(operation.id) >> ["b1": 1, "b2": 1]

    expect:
    service.isSectionComplete(operation)
  }


  def "IsSectionComplete is false when some mappings doesn't match expected"() {
    given:
    def operation = OperationDataset.electoralOperation()
    repositoryService.getAllBallotsForOperation(operation.id, true, false) >> ["b1", "b2"]
    electionPagePropertiesService.getBallotToModelIdMapping(operation.id) >> ["b3": 1, "b2": 1]

    expect:
    !service.isSectionComplete(operation)
  }


  def "IsSectionComplete is false when not all mappings has been defined"() {
    given:
    def operation = OperationDataset.electoralOperation()
    repositoryService.getAllBallotsForOperation(operation.id, true, false) >> ["b1", "b2"]
    electionPagePropertiesService.getBallotToModelIdMapping(operation.id) >> ["b2": 1]

    expect:
    !service.isSectionComplete(operation)
  }


  def "sectionName is election-page-properties"() {
    expect:
    service.sectionName == "election-page-properties"
  }

}
