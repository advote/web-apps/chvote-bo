/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status

import static ch.ge.ve.bo.repository.dataset.OperationDataset.LAST_VOTING_PERIOD_CONFIG_UPDATE_DATE
import static ch.ge.ve.bo.repository.dataset.OperationDataset.Options
import static ch.ge.ve.bo.repository.dataset.OperationDataset.electoralOperation
import static ch.ge.ve.bo.repository.dataset.OperationDataset.getLAST_VOTING_MATERIAL_UPDATE_DATE
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.AVAILABLE_FOR_CREATION
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_FAILED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_IN_PROGRESS
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_REJECTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.CREATION_REQUESTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATION_REJECTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.INVALIDATION_REQUESTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo.State.VALIDATED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.AVAILABLE_FOR_INITIALIZATION
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.CLOSED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZATION_FAILED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZATION_IN_PROGRESS
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZATION_REJECTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZATION_REQUESTED
import static ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo.State.INITIALIZED

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.DownloadToken
import ch.ge.ve.bo.repository.entity.WorkflowStep
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.model.PactConfiguration
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration
import ch.ge.ve.bo.service.operation.download.token.DownloadTokenService
import ch.ge.ve.bo.service.operation.status.complete.ConfigurationIsCompleteSupplier
import ch.ge.ve.bo.service.operation.status.complete.VotingMaterialIsCompleteSupplier
import ch.ge.ve.bo.service.operation.status.complete.VotingPeriodIsCompleteSupplier
import ch.ge.ve.bo.service.printer.PrinterTemplateService
import ch.ge.ve.chvote.pactback.contract.operation.status.TallyArchiveStatusVo
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingPeriodStatusVo
import java.nio.charset.Charset
import java.nio.file.Files
import org.apache.http.HttpStatus
import org.apache.http.client.HttpClient
import org.apache.http.client.HttpResponseException
import org.apache.http.client.methods.HttpGet
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import spock.lang.Specification
import spock.lang.Unroll

class OperationStatusServiceImplTest extends Specification {
  static afterVMUpd = LAST_VOTING_MATERIAL_UPDATE_DATE.plusHours(1)
  static beforeVMUpd = LAST_VOTING_MATERIAL_UPDATE_DATE.minusHours(1)

  static afterVPUpd = LAST_VOTING_PERIOD_CONFIG_UPDATE_DATE.plusHours(1)
  static beforeVPUpd = LAST_VOTING_PERIOD_CONFIG_UPDATE_DATE.minusHours(1)


  def PRINTER_FOR_TEST = new PrinterConfiguration("boh-vprt", "printer for test card", null, 9999)
  def operationRepository = Mock(SecuredOperationRepository)
  def printerTemplateServiceMock = Mock(PrinterTemplateService)
  def objectMapper = TestHelpers.getObjectMapper()
  def httpClient = Mock(HttpClient)
  def downloadTokenService = Mock(DownloadTokenService)
  static def pactConfig = new PactConfiguration()


  def configurationIsCompleteSupplier = Mock(ConfigurationIsCompleteSupplier)
  def votingMaterialIsCompleteSupplier = Mock(VotingMaterialIsCompleteSupplier)
  def votingPeriodIsCompleteSupplier = Mock(VotingPeriodIsCompleteSupplier)
  def readonlyStatusService = new ReadonlyStatusServiceImpl()
  def helper = new OperationStatusHandlerHelper([configurationIsCompleteSupplier] as Set,
          [votingMaterialIsCompleteSupplier] as Set,
          [votingPeriodIsCompleteSupplier] as Set, TestHelpers.objectMapper, pactConfig, httpClient)
  def operationStatusTallyHandler = new OperationStatusTallyHandler(downloadTokenService, helper, pactConfig)
  def configurationHandler = new OperationStatusConfigurationHandler(helper, pactConfig)
  def votingMaterialHandler = new OperationStatusVotingMaterialHandler(helper, pactConfig, printerTemplateServiceMock)
  def votingPeriodHandler = new OperationStatusVotingPeriodHandler(helper, pactConfig)
  def serviceUnderTest = new OperationStatusServiceImpl(operationRepository, readonlyStatusService,
          operationStatusTallyHandler, configurationHandler, votingMaterialHandler, votingPeriodHandler)


  def setupSpec() {
    pactConfig.url.operationConfigurationStatus = "ocStatus"
    pactConfig.url.votingMaterialStatus = "vmStatus"
    pactConfig.url.votingPeriodStatus = "vpStatus"
    pactConfig.url.tallyArchiveStatus = "taStatus"
    pactConfig.votingMaterialBaseLocation = "/temp"
  }

  void setup() {
    configurationIsCompleteSupplier.getSectionName() >> "config-section"
    votingMaterialIsCompleteSupplier.getSectionName() >> "voting-material-section"
    votingPeriodIsCompleteSupplier.getSectionName() >> "voting-period-section"
    printerTemplateServiceMock.getVirtualPrinterForTestCard() >> PRINTER_FOR_TEST


    ConnectedUserTestUtils.connectUserWithAllRoles()
  }

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  @Unroll
  def "verify configuration status for #useCase "() {
    given: 'an operation'

    def ope = electoralOperation(new Options()
            .with(Options.deploymentTarget, deploymentTarget)
            .with(Options.simulationName, DeploymentTarget.SIMULATION ? "testSimulation" : null)
    )
    ope.setWorkflowStep(WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT)

    operationRepository.findOne(1L, false) >> ope
    configurationIsCompleteSupplier.isSectionComplete(ope) >> true
    votingMaterialIsCompleteSupplier.isSectionComplete(ope) >> false


    httpClient.execute(_, _) >> TestHelpers.getResourceString(OperationStatusServiceImplTest, pactResponse)

    when: "getting the status"
    def response = serviceUnderTest.getStatus(ope.id)

    then: "service should return #canUpload"
    JSONAssert.assertEquals(
            TestHelpers.getResourceString(OperationStatusServiceImplTest, serviceResponse, true),
            objectMapper.writeValueAsString(response), JSONCompareMode.NON_EXTENSIBLE)


    where:
    useCase                                  | pactResponse                    | serviceResponse                          | deploymentTarget
    "test site in deployment"                | "test_site_in_deployment.json"  | "test_site_in_deployment.response.json"  | DeploymentTarget.NOT_DEFINED
    "in validation"                          | "in_validation.json"            | "in_validation.response.json"            | DeploymentTarget.NOT_DEFINED
    "validated"                              | "validated.json"                | "validated.response.json"                | DeploymentTarget.NOT_DEFINED
    "invalidated"                            | "invalidated.json"              | "invalidated.response.json"              | DeploymentTarget.NOT_DEFINED
    "deployment_requested"                   | "deployment_requested.json"     | "deployment_requested.response.json"     | DeploymentTarget.NOT_DEFINED
    "deployment_refused"                     | "deployment_refused.json"       | "deployment_refused.response.json"       | DeploymentTarget.NOT_DEFINED
    "deployed but no target defined"         | "deployed.json"                 | "deployed-no-target.response.json"       | DeploymentTarget.NOT_DEFINED
    "deployed target defined for production" | "deployed.json"                 | "deployed.response.json"                 | DeploymentTarget.REAL
    "deployed target defined for simulation" | "deployed.json"                 | "deployed-in-simulation.response.json"   | DeploymentTarget.SIMULATION
    "invalidated but modified"               | "invalidated_but_modified.json" | "invalidated_but_modified.response.json" | DeploymentTarget.NOT_DEFINED
  }


  @Unroll
  def "verify in modification mode for #useCase"() {
    given: 'an operation'

    def ope = electoralOperation(new Options()
            .with(Options.deploymentTarget, DeploymentTarget.REAL)
            .with(Options.simulationName, null)
            .with(Options.inModification, operationInModificationFlag)
    )
    ope.setWorkflowStep(WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT)

    operationRepository.findOne(1L, false) >> ope
    configurationIsCompleteSupplier.isSectionComplete(ope) >> true
    votingMaterialIsCompleteSupplier.isSectionComplete(ope) >> true
    votingPeriodIsCompleteSupplier.isSectionComplete(ope) >> false

    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.operationConfigurationStatus }, _) >>
            TestHelpers.getResourceString(OperationStatusServiceImplTest, confStatusFile)
    if (vmState != null) {
      ope.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_NOT_SENT)
      httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.votingMaterialStatus }, _) >>
              objectMapper.writeValueAsString(new VotingMaterialsStatusVo("user", afterVMUpd, vmState,
                      null, "pactUrl", "pactInvalidateUrl", [:], Collections.emptyList()))

    } else {
      httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.votingMaterialStatus }, _) >> {
        throw new HttpResponseException(404, "not found")
      }
    }



    when: "getting the status"
    def response = serviceUnderTest.getStatus(ope.id)

    then: "service should return #canUpload"
    JSONAssert.assertEquals(
            TestHelpers.getResourceString(OperationStatusServiceImplTest, serviceResponse, true),
            objectMapper.writeValueAsString(response), JSONCompareMode.NON_EXTENSIBLE)

    where:
    useCase                        | operationInModificationFlag | confStatusFile                                | vmState                | serviceResponse
    "no vm sent/no conf sent"      | true                        | "deployed.json"                               | null                   | "modif_no_vm_sent_no_conf_sent.response.json"
    "no vm sent/conf sent"         | false                       | "deployed_in_prod_in_deployemnt_in_test.json" | null                   | "modif_no_vm_sent_conf_sent.response.json"
    "vm sent no/conf sent"         | true                        | "deployed.json"                               | AVAILABLE_FOR_CREATION | "modif_vm_sent_no_conf_sent.response.json"
    "vm sent no/conf sent partial" | true                        | "deployed.json"                               | CREATED                | "modif_vm_sent_no_conf_sent-partial.response.json"
  }


  @Unroll
  def "verify voting material status for #useCase "() {
    given:
    def ope = electoralOperation(
            new Options().with(Options.deploymentTarget, DeploymentTarget.REAL)
                    .with(Options.printerTemplate, "template")
                    .with(Options.votingCardTitle, "voting card")
    )

    operationRepository.findOne(1L, false) >> ope
    configurationIsCompleteSupplier.isSectionComplete(ope) >> true
    votingMaterialIsCompleteSupplier.isSectionComplete(ope) >> true
    votingPeriodIsCompleteSupplier.isSectionComplete(ope) >> false

    ope.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_NOT_SENT)
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.operationConfigurationStatus }, _) >>
            TestHelpers.getResourceString(OperationStatusServiceImplTest, "deployed.json")
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.votingMaterialStatus }, _) >>
            objectMapper.writeValueAsString(new VotingMaterialsStatusVo("user", stateDate, state,
                    rejectionReason, "pactUrl", "pactInvalidateUrl", [:], Collections.emptyList()))



    when: "getting the status"
    def response = serviceUnderTest.getStatus(ope.id)

    then: "service should return #canUpload"
    JSONAssert.assertEquals(
            TestHelpers.getResourceString(OperationStatusServiceImplTest, serviceResponse, true),
            objectMapper.writeValueAsString(response), JSONCompareMode.NON_EXTENSIBLE)


    where:
    useCase                             | stateDate   | state                  | rejectionReason   | serviceResponse
    "vm available for creation"         | afterVMUpd  | AVAILABLE_FOR_CREATION | null              | "vot-mat_available_for_creation.json"
    "vm creation requested"             | afterVMUpd  | CREATION_REQUESTED     | null              | "vot-mat_creation_requested.json"
    "vm creation rejected"              | afterVMUpd  | CREATION_REJECTED      | "not good enough" | "vot-mat_creation_rejected.json"
    "vm creation rejected but modified" | beforeVMUpd | CREATION_REJECTED      | "not good enough" | "vot-mat_modified.json"
    "vm creation in progress"           | afterVMUpd  | CREATION_IN_PROGRESS   | null              | "vot-mat_creation_in_progress.json"
    "vm creation failed"                | afterVMUpd  | CREATION_FAILED        | null              | "vot-mat_creation_failed.json"
    "vm creation failed but modified"   | beforeVMUpd | CREATION_FAILED        | null              | "vot-mat_modified.json"
    "vm created"                        | afterVMUpd  | CREATED                | null              | "vot-mat_created.json"
    "vm validate"                       | afterVMUpd  | VALIDATED              | null              | "vot-mat_validated.json"
    "vm invalidation requested"         | afterVMUpd  | INVALIDATION_REQUESTED | null              | "vot-mat_invalidation_requested.json"
    "vm invalidation rejected"          | afterVMUpd  | INVALIDATION_REJECTED  | null              | "vot-mat_invalidation_rejected.json"
    "vm invalidated"                    | afterVMUpd  | INVALIDATED            | null              | "vot-mat_invalidated.json"
    "vm invalidated but modifier"       | beforeVMUpd | INVALIDATED            | null              | "vot-mat_modified.json"
  }


  @Unroll
  def "verify voting period status for #useCase "() {
    given:
    def ope = electoralOperation(
            new Options().with(Options.deploymentTarget, DeploymentTarget.REAL)
                    .with(Options.printerTemplate, "template")
                    .with(Options.votingCardTitle, "voting card")
    )

    operationRepository.findOne(1L, false) >> ope
    configurationIsCompleteSupplier.isSectionComplete(ope) >> true
    votingMaterialIsCompleteSupplier.isSectionComplete(ope) >> true
    votingPeriodIsCompleteSupplier.isSectionComplete(ope) >> true

    ope.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_SENT)
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.operationConfigurationStatus }, _) >>
            TestHelpers.getResourceString(OperationStatusServiceImplTest, "deployed.json")
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.votingMaterialStatus }, _) >>
            objectMapper.writeValueAsString(new VotingMaterialsStatusVo("user", afterVMUpd, VALIDATED,
                    null, "pactUrl", "invalidationURL", [:], Collections.emptyList()))
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.votingPeriodStatus }, _) >>
            objectMapper.writeValueAsString(
                    new VotingPeriodStatusVo("user", stateDate, state, rejectionReason, "pactUrl"))

    when: "getting the status"
    def response = serviceUnderTest.getStatus(ope.id)

    then: "service should return #canUpload"
    JSONAssert.assertEquals(
            TestHelpers.getResourceString(OperationStatusServiceImplTest, serviceResponse, true),
            objectMapper.writeValueAsString(response), JSONCompareMode.NON_EXTENSIBLE)


    where:
    useCase                                   | stateDate   | state                        | rejectionReason   | serviceResponse
    "vp available for initialization"         | afterVPUpd  | AVAILABLE_FOR_INITIALIZATION | null              | "vot-period_available_for_initialization.json"
    "vp initialization requested"             | afterVPUpd  | INITIALIZATION_REQUESTED     | null              | "vot-period_initialization_requested.json"
    "vp initialization rejected"              | afterVPUpd  | INITIALIZATION_REJECTED      | "not good enough" | "vot-period_initialization_rejected.json"
    "vp initialization rejected but modified" | beforeVPUpd | INITIALIZATION_REJECTED      | "not good enough" | "vot-period_modified.json"
    "vp initialization in progress"           | afterVPUpd  | INITIALIZATION_IN_PROGRESS   | null              | "vot-period_initialization_in_progress.json"
    "vp initialization failed"                | afterVPUpd  | INITIALIZATION_FAILED        | null              | "vot-period_initialization_failed.json"
    "vp initialization failed but modified"   | beforeVPUpd | INITIALIZATION_FAILED        | null              | "vot-period_modified.json"
    "vp initialized"                          | afterVPUpd  | INITIALIZED                  | null              | "vot-period_initialized.json"

  }


  @Unroll
  def "verify tally archive status for #useCase "() {
    given:
    def ope = electoralOperation(
            new Options().with(Options.deploymentTarget, DeploymentTarget.REAL)
                    .with(Options.printerTemplate, "template")
                    .with(Options.votingCardTitle, "voting card")
    )

    operationRepository.findOne(1L, false) >> ope
    configurationIsCompleteSupplier.isSectionComplete(ope) >> true
    votingMaterialIsCompleteSupplier.isSectionComplete(ope) >> true
    votingPeriodIsCompleteSupplier.isSectionComplete(ope) >> true

    DownloadToken downloadToken = new DownloadToken()
    downloadToken.id = "token"
    downloadTokenService.getOrCreateDownloadToken(ope, _ as String) >> downloadToken
    ConnectedUserTestUtils.connectUserWithRole("ROLE_DOWNLOAD_TALLY_ARCHIVE")
    ope.setWorkflowStep(WorkflowStep.VOTING_PERIOD_CONF_SENT)
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.operationConfigurationStatus }, _) >>
            TestHelpers.getResourceString(OperationStatusServiceImplTest, "deployed.json")
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.votingMaterialStatus }, _) >>
            objectMapper.writeValueAsString(new VotingMaterialsStatusVo("user", afterVMUpd, VALIDATED,
                    null, "pactUrl", "invalidationURL", [:], Collections.emptyList()))
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.votingPeriodStatus }, _) >>
            objectMapper.writeValueAsString(
                    new VotingPeriodStatusVo("user", afterVPUpd, CLOSED, null, null))
    httpClient.execute({ (it as HttpGet).getURI().toString() == pactConfig.url.tallyArchiveStatus }, _) >>
            objectMapper.writeValueAsString(
                    new TallyArchiveStatusVo(state, state == TallyArchiveStatusVo.State.CREATED ? "tallyLocation" : null))


    when: "getting the status"
    def response = serviceUnderTest.getStatus(ope.id)

    then: "service should return #canUpload"
    JSONAssert.assertEquals(
            TestHelpers.getResourceString(OperationStatusServiceImplTest, serviceResponse, true),
            objectMapper.writeValueAsString(response), JSONCompareMode.NON_EXTENSIBLE)


    where:
    useCase                | state                                           | serviceResponse
    "not requested"        | TallyArchiveStatusVo.State.NOT_REQUESTED        | "tally-archive_not_requested.json"
    "creation in progress" | TallyArchiveStatusVo.State.CREATION_IN_PROGRESS | "tally-archive_creation_in_progress.json"
    "creation Failed"      | TallyArchiveStatusVo.State.CREATION_FAILED      | "tally-archive_creation_failed.json"
    "created"              | TallyArchiveStatusVo.State.CREATED              | "tally-archive_created.json"

  }


  def "Configuration is incomplete"() {
    given:
    def ope = electoralOperation()
    operationRepository.findOne(1L, false) >> ope
    httpClient.execute(_, _) >> { throw new HttpResponseException(HttpStatus.SC_NOT_FOUND, "not found") }
    configurationIsCompleteSupplier.isSectionComplete(ope) >> complete

    when: "getting the status"
    def response = serviceUnderTest.getStatus(ope.id)

    then: "service should return #canUpload"



    JSONAssert.assertEquals(TestHelpers.getResourceString(OperationStatusServiceImplTest, serviceResponse, true),
            objectMapper.writeValueAsString(response), JSONCompareMode.NON_EXTENSIBLE)

    where:
    _ | complete | serviceResponse
    _ | true     | "complete_configuration.json"
    _ | false    | "incomplete_configuration.json"


  }




}
