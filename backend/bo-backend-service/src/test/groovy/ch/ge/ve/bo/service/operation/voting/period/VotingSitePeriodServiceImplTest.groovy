/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.voting.period

import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.SimulationPeriodConfigurationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.SimulationPeriodConfiguration
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.exception.TechnicalException
import java.time.LocalDateTime
import spock.lang.Specification

class VotingSitePeriodServiceImplTest extends Specification {
  def repository = Mock(SimulationPeriodConfigurationRepository)
  def operationRepository = Mock(SecuredOperationRepository)

  def service = new VotingSitePeriodServiceImpl(
          repository,
          operationRepository,
          TestHelpers.passThroughSecuredOperationHolderService())


  def "SaveOrUpdate for simulation should save date in database"() {
    given:
    def operation = OperationDataset.electoralOperation()
    operation.setDeploymentTarget(DeploymentTarget.SIMULATION)
    operationRepository.findOne(operation.id, true) >> operation
    repository.findByOperationId(operation.id) >> Optional.empty()

    when:
    def result = service.saveOrUpdate(operation.id, LocalDateTime.parse("2007-12-03T09:15:30"), LocalDateTime.parse("2007-12-04T11:00:30"), 10)

    then:
    1 * repository.save(_) >> { it[0] }
    9 == result.dateOpen.getHour()
    11 == result.dateClose.getHour()
    10 == result.gracePeriod
  }

  def "SaveOrUpdate for real should throw an exception"() {
    given:
    def operation = OperationDataset.electoralOperation()
    operation.setDeploymentTarget(DeploymentTarget.REAL)
    operationRepository.findOne(operation.id, true) >> operation
    repository.findByOperationId(operation.id) >> Optional.empty()

    when:
    service.saveOrUpdate(operation.id, LocalDateTime.parse("2007-12-03T09:15:30"), LocalDateTime.parse("2007-12-04T11:00:30"), 10)

    then:
    def exception = thrown(TechnicalException)
    "Could not change vote site period if the operation is in production" == exception.message
  }


  def "FindForOperation for simulation with existing data"() {
    given:
    def operation = OperationDataset.electoralOperation()
    operation.setDeploymentTarget(DeploymentTarget.SIMULATION)
    operationRepository.findOne(operation.id, false) >> operation

    def configuration = new SimulationPeriodConfiguration()
    configuration.setDateOpen(LocalDateTime.parse("2007-12-03T09:15:30"))
    configuration.setDateClose(LocalDateTime.parse("2007-12-03T11:15:30"))
    configuration.setGracePeriod(10)
    repository.findByOperationId(operation.id) >> Optional.of(configuration)

    when:
    def result = service.findForOperation(operation.id).get()

    then:
    "2007-12-03T09:15:30" == result.dateOpen.toString()
    "2007-12-03T11:15:30" == result.dateClose.toString()
    10 == result.gracePeriod
  }


  def "FindForOperation for production should rely on milestone data"() {
    given:
    def operation = OperationDataset.electoralOperation()
    operation.setDeploymentTarget(DeploymentTarget.REAL)
    operationRepository.findOne(operation.id, false) >> operation

    when:
    def result = service.findForOperation(operation.id).get()

    then:
    "2050-03-23T12:00" == result.dateOpen.toString()
    "2050-04-18T12:00" == result.dateClose.toString()
    9 == result.gracePeriod
  }

  def "FindForOperation for simulation without data should not be present"() {
    given:
    def operation = OperationDataset.electoralOperation()
    operation.setDeploymentTarget(DeploymentTarget.SIMULATION)
    operationRepository.findOne(operation.id, false) >> operation
    repository.findByOperationId(operation.id) >> Optional.empty()

    when:
    def result = service.findForOperation(operation.id)

    then:
    !result.isPresent()
  }

}
