/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.testing.card.impl

import static ch.ge.ve.bo.repository.dataset.OperationDataset.votingOperation
import static ch.ge.ve.bo.repository.entity.CardType.PRODUCTION_TESTING_CARD
import static ch.ge.ve.bo.repository.entity.CardType.TEST_SITE_TESTING_CARD
import static ch.ge.ve.bo.service.dataset.VoterTestingCardsLotVoDataset.voterTestingCardsLotVo
import static java.util.Collections.singleton

import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.VoterTestingCardsLotRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.dataset.VoterTestingCardsLotDataset
import ch.ge.ve.bo.repository.entity.CardType
import ch.ge.ve.bo.repository.entity.TestingCardLanguage
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot
import ch.ge.ve.bo.service.TestHelpers
import spock.lang.Specification

class VoterTestingCardsLotServiceImplTest extends Specification {
    def lotRepository = Mock(VoterTestingCardsLotRepository)
    def operationRepository = Mock(SecuredOperationRepository)
    def service = new VoterTestingCardsLotServiceImpl(lotRepository, operationRepository, TestHelpers.passThroughSecuredOperationHolderService())


    def "FindAll should retrieve appropriate dataset depending on configuration or voting material"() {
        when:
        service.findAll(1, forConfiguration)

        then:
        1 * lotRepository.findAllByOperationIdAndCardTypeInOrderById(1, expectedLotTypeForSearchingDatabase) >> []

        where:
        forConfiguration | expectedLotTypeForSearchingDatabase
        true             | CardType.forConfiguration()
        false            | CardType.forVotingMaterial()

    }

    def "Create should call appropriate repository method"() {
        given:
        operationRepository.findOne(1, true) >> votingOperation()

        when:
        service.create(1, voterTestingCardsLotVo(TEST_SITE_TESTING_CARD))

        then:
        1 * lotRepository.save(_) >> new VoterTestingCardsLot()
    }

    def "Edit"() {
        given:
        def previousLot = new VoterTestingCardsLot()
        previousLot.setOperation(votingOperation(new OperationDataset.Options().with(OperationDataset.Options.id, 1)))
        lotRepository.findById(1) >> Optional.of(previousLot)

        when:
        service.edit(voterTestingCardsLotVo(TEST_SITE_TESTING_CARD))

        then:
        1 * lotRepository.save(_) >> VoterTestingCardsLotDataset.voterTestingCardsLot(TEST_SITE_TESTING_CARD)
    }

    def "Delete"() {
        given:
        def previousLot = new VoterTestingCardsLot()
        previousLot.setCardType(TEST_SITE_TESTING_CARD)
        previousLot.setOperation(votingOperation(new OperationDataset.Options().with(OperationDataset.Options.id, 1)))
        lotRepository.getOne(1) >> previousLot

        when:
        service.delete(1)

        then:
        1 * lotRepository.delete(_)
    }


    def "CopyTestingCardFromTestSiteToProduction"() {
        given:
        def previousLot = new VoterTestingCardsLot()
        previousLot.cardType = TEST_SITE_TESTING_CARD
        previousLot.language = TestingCardLanguage.FR
        previousLot.id = 1
        lotRepository.findAllByOperationIdAndCardTypeInOrderById(1, singleton(TEST_SITE_TESTING_CARD)) >> [previousLot]

        when:
        service.copyTestingCardFromTestSiteToProduction(1)

        then:
        1 * lotRepository.save({
            it.cardType == PRODUCTION_TESTING_CARD
            it.language == TestingCardLanguage.FR
        })
    }

}
