/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status

import static ch.ge.ve.bo.repository.dataset.OperationDataset.electoralOperation

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.WorkflowStep
import ch.ge.ve.bo.service.model.PactConfiguration
import ch.ge.ve.bo.service.operation.exception.CloseVotingPeriodException
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo
import ch.ge.ve.bo.service.operation.model.ModificationMode
import ch.ge.ve.bo.service.operation.model.OperationStatusVo
import ch.ge.ve.bo.service.operation.model.TallyArchiveStatusVo
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo
import ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo
import java.time.LocalDateTime
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpPut
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.impl.client.BasicResponseHandler
import spock.lang.Specification

class OperationWorkflowServiceImplTest extends Specification {
  def operationStatusService = Mock(OperationStatusService)
  def operationRepository = Mock(SecuredOperationRepository)
  def httpClient = Mock(HttpClient)
  def pactConfiguration = new PactConfiguration()
  def service = new OperationWorkflowServiceImpl(
          operationStatusService, operationRepository, httpClient, pactConfiguration)

  void setup() {
    pactConfiguration.url.invalidateTestSite = "invalidateTestSite/{0}"
    pactConfiguration.url.validateTestSite = "validateTestSite/{0}"
    pactConfiguration.url.validateVotingMaterial = "validateVotingMaterial/{0}"
    pactConfiguration.url.triggerTallyArchive = "triggerTallyArchive/{0}"
    pactConfiguration.username = "user"
    pactConfiguration.password = "pass"
    ConnectedUserTestUtils.connectUser()
  }

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def "SuspendModificationMode"() {
    given:
    def ope = electoralOperation()
    ope.inModification = true
    operationRepository.findOne(1, true) >> ope

    when:
    service.suspendModificationMode(1)

    then:
    1 * operationRepository.save(ope)
    !ope.inModification
  }


  def "setWorkflowStep should switch step if operation is not in modification"() {
    given:
    def ope = electoralOperation(new OperationDataset.Options())
    ope.inModification = false
    operationRepository.findOne(1L, true) >> ope

    when:
    service.setWorkflowStep(1, WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT)

    then:
    1 * operationRepository.save(ope) >> ope
    ope.getWorkflowStep() == WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT
  }


  def "setWorkflowStep should not switch step if operation is in modification"() {
    given:
    def ope = electoralOperation(new OperationDataset.Options())
    ope.inModification = true
    operationRepository.findOne(1L, true) >> ope

    when:
    service.setWorkflowStep(1, WorkflowStep.VOTING_MATERIAL_CONF_NOT_SENT)

    then:
    0 * operationRepository.save(ope)
    ope.getWorkflowStep() == WorkflowStep.CONFIGURATION_NOT_SENT
  }

  def "SwitchToModificationMode should failed if operation is not modifiable"() {
    given:
    def ope = electoralOperation(new OperationDataset.Options())
    operationRepository.findOne(1L, true) >> ope
    operationStatusService.getModificationMode(ope) >> modificationMode

    when:
    service.switchToModificationMode(1)

    then:
    def exception = thrown(CannotSwitchToModificationMode)
    exception.messageKey == "deployment-summary.voting-material.modification.errors.cannot-switch-to-modification-mode"
    0 * operationRepository.save(ope)
    !ope.inModification

    where:
    _ | modificationMode
    _ | ModificationMode.IN_FULL_MODIFICATION
    _ | ModificationMode.IN_PARTIAL_MODIFICATION
    _ | ModificationMode.NOT_MODIFIABLE
  }

  def "SwitchToModificationMode should succeed if operation is modifiable"() {
    given:
    def ope = electoralOperation(new OperationDataset.Options())
    operationRepository.findOne(1L, true) >> ope
    operationStatusService.getModificationMode(ope) >> modificationMode

    when:
    service.switchToModificationMode(1)

    then:
    1 * operationRepository.save(ope)
    ope.inModification

    where:
    _ | modificationMode
    _ | ModificationMode.FULLY_MODIFIABLE
    _ | ModificationMode.PARTIALLY_MODIFIABLE
  }

  def "ValidateTestSite"() {
    given:
    HttpPost request = null

    when:
    service.validateTestSite(1)

    then:
    1 * httpClient.execute({ request = it } as HttpUriRequest, _ as BasicResponseHandler)
    verifyRequest(request, "POST", "validateTestSite/1")
  }

  def "InvalidateTestSite"() {
    given:
    HttpPost request = null

    when:
    service.invalidateTestSite(1)

    then:
    1 * httpClient.execute({ request = it } as HttpUriRequest, _ as BasicResponseHandler)
    verifyRequest(request, "POST", "invalidateTestSite/1")

  }

  def "ValidateVotingMaterial"() {
    given:
    HttpPost request = null

    when:
    service.validateVotingMaterial(1)

    then:
    1 * httpClient.execute({ request = it } as HttpUriRequest, _ as BasicResponseHandler)
    verifyRequest(request, "POST", "validateVotingMaterial/1")
  }

  def "CloseVotingPeriod should be forbidden for an operation in Real"() {
    def operationStatus = status(DeploymentTarget.REAL,
            TallyArchiveStatusVo.State.NOT_REQUESTED,
            VotingPeriodStatusVo.State.INITIALIZED)
    operationStatusService.getStatus(1) >> operationStatus

    when:
    service.closeVotingPeriod(1)

    then:
    def exception = thrown(CloseVotingPeriodException)
    exception.messageKey == "tally.archive.errors.operation-is-not-in-simulation"
  }

  def "CloseVotingPeriod should be forbidden if tally archive is already in creation"() {
    def operationStatus = status(DeploymentTarget.SIMULATION,
            TallyArchiveStatusVo.State.CREATION_IN_PROGRESS,
            VotingPeriodStatusVo.State.INITIALIZED)
    operationStatusService.getStatus(1) >> operationStatus

    when:
    service.closeVotingPeriod(1)

    then:
    def exception = thrown(CloseVotingPeriodException)
    exception.messageKey == "tally.archive.errors.generation-already-requested"
  }

  def "CloseVotingPeriod should be forbidden if tally archive is already created"() {
    def operationStatus = status(DeploymentTarget.SIMULATION,
            TallyArchiveStatusVo.State.CREATED,
            VotingPeriodStatusVo.State.INITIALIZED)
    operationStatusService.getStatus(1) >> operationStatus

    when:
    service.closeVotingPeriod(1)

    then:
    def exception = thrown(CloseVotingPeriodException)
    exception.messageKey == "tally.archive.errors.generation-already-done"
  }


  def "CloseVotingPeriod should be forbidden if voting period is not initialized"() {
    def operationStatus = status(DeploymentTarget.SIMULATION,
            TallyArchiveStatusVo.State.NOT_REQUESTED,
            VotingPeriodStatusVo.State.AVAILABLE_FOR_INITIALIZATION)
    operationStatusService.getStatus(1) >> operationStatus

    when:
    service.closeVotingPeriod(1)

    then:
    def exception = thrown(CloseVotingPeriodException)
    exception.messageKey == "tally.archive.errors.voting-period-not-initialized"
  }

  def "CloseVotingPeriod should be accepted if it's a new request"() {
    given:
    HttpPut request = null
    def operationStatus = status(DeploymentTarget.SIMULATION,
            TallyArchiveStatusVo.State.NOT_REQUESTED,
            VotingPeriodStatusVo.State.INITIALIZED)
    operationStatusService.getStatus(1) >> operationStatus

    when:
    service.closeVotingPeriod(1)

    then:
    1 * httpClient.execute({ request = it } as HttpUriRequest, _ as BasicResponseHandler)
    verifyRequest(request, "PUT", "triggerTallyArchive/1")
  }


  def "CloseVotingPeriod should be accepted if previous attempt failed"() {
    given:
    def operationStatus = status(DeploymentTarget.SIMULATION,
            TallyArchiveStatusVo.State.CREATION_FAILED,
            VotingPeriodStatusVo.State.CLOSED)
    operationStatusService.getStatus(1) >> operationStatus
    HttpPut request = null

    when:
    service.closeVotingPeriod(1)

    then:
    1 * httpClient.execute({ request = it } as HttpUriRequest, _ as BasicResponseHandler)
    verifyRequest(request, "PUT", "triggerTallyArchive/1")
  }

  private static void verifyRequest(HttpEntityEnclosingRequestBase request, String method, String expectedUrl) {
    assert request.method == method
    assert request.URI.path == expectedUrl
    assert new String(request.entity.content.bytes) == "user=test"
    assert request.getLastHeader("Authorization").value == "Basic dXNlcjpwYXNz"
    assert request.getLastHeader("X-Requested-With").value == "XMLHttpRequest"
  }

  private OperationStatusVo status(DeploymentTarget deploymentTarget,
                                   TallyArchiveStatusVo.State tallyArchiveStatus,
                                   VotingPeriodStatusVo.State votingPeriodStatus) {

    def vmStatus = new VotingMaterialStatusVo(VotingMaterialStatusVo.State.CREATED, LocalDateTime.now(), [:], "user")
    def vpStatus = new VotingPeriodStatusVo(votingPeriodStatus, [:], LocalDateTime.now(), "user", "pactUrl", null)
    def tallyStatus = new TallyArchiveStatusVo(tallyArchiveStatus, "token")
    def confStatus = new ConfigurationStatusVo.Builder()
            .setState(ConfigurationStatusVo.State.DEPLOYED, LocalDateTime.now(), 'USER')
            .build()

    new OperationStatusVo(confStatus, vmStatus, vpStatus, tallyStatus, "simulationName", deploymentTarget)
  }
}
