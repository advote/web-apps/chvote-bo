/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.dataset

import static ch.ge.ve.bo.repository.dataset.DatasetTools.localDateTime

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceFileVo
import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo
import java.nio.charset.StandardCharsets

/**
 * Dataset used for testing {@link DomainOfInfluenceFileVo} model and service.
 */
class DomainOfInfluenceFileVoDataset {

  static DomainOfInfluenceFileVo validDomainOfInfluenceFileVo(withContent = true) {
    new DomainOfInfluenceFileVo(
            1,
            'test',
            localDateTime('13.12.2016 14:30:00'),
            '201709VPbo://TEST70dc0e5d947d44bdbbef2092ee473f99',
            1,
            FileType.DOMAIN_OF_INFLUENCE,
            null,
            'valid-doi.xml',
            'Canton de Genève',
            withContent ? """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<ns2:delivery xmlns="http://www.ech.ch/xmlns/eCH-0058/5"
              xmlns:ns2="http://www.ech.ch/xmlns/logistic/1"
              xmlns:ns3="http://www.ech.ch/xmlns/eCH-0155/4">
    <ns2:deliveryHeader>
        <senderId>bo://TEST</senderId>
        <messageId>70dc0e5d947d44bdbbef2092ee473f99</messageId>
        <messageType>bo://domainOfInfluence</messageType>
        <sendingApplication>
            <manufacturer>DGSI</manufacturer>
            <product>CH-Vote - Back Office</product>
            <productVersion>1.0.0</productVersion>
        </sendingApplication>
        <messageDate>2017-07-25T14:00:00.0Z</messageDate>
        <action>1</action>
        <testDeliveryFlag>false</testDeliveryFlag>
    </ns2:deliveryHeader>
    <ns2:domainOfInfluence>
        <ns3:domainOfInfluenceType>CH</ns3:domainOfInfluenceType>
        <ns3:localDomainOfInfluenceIdentification>1</ns3:localDomainOfInfluenceIdentification>
        <ns3:domainOfInfluenceName>Confédération</ns3:domainOfInfluenceName>
        <ns3:domainOfInfluenceShortname>CH</ns3:domainOfInfluenceShortname>
    </ns2:domainOfInfluence>
    <ns2:domainOfInfluence>
        <ns3:domainOfInfluenceType>CT</ns3:domainOfInfluenceType>
        <ns3:localDomainOfInfluenceIdentification>1</ns3:localDomainOfInfluenceIdentification>
        <ns3:domainOfInfluenceName>Canton de Zürich</ns3:domainOfInfluenceName>
        <ns3:domainOfInfluenceShortname>ZH</ns3:domainOfInfluenceShortname>
    </ns2:domainOfInfluence>
    <ns2:domainOfInfluence>
        <ns3:domainOfInfluenceType>MU</ns3:domainOfInfluenceType>
        <ns3:localDomainOfInfluenceIdentification>261</ns3:localDomainOfInfluenceIdentification>
        <ns3:domainOfInfluenceName>Ville de Zürich</ns3:domainOfInfluenceName>
        <ns3:domainOfInfluenceShortname>Züri</ns3:domainOfInfluenceShortname>
    </ns2:domainOfInfluence>
</ns2:delivery>""".getBytes(StandardCharsets.UTF_8) : null,
            [
                    new DomainOfInfluenceVo('CH', '1', 'Confédération', 'CH'),
                    new DomainOfInfluenceVo('CT', '1', 'Canton de Zürich', 'ZH'),
                    new DomainOfInfluenceVo('MU', '261', 'Ville de Zürich', 'Züri')
            ],
    )
  }

  static DomainOfInfluenceFileVo genevaDoi() {
    new DomainOfInfluenceFileVo(
            1,
            'test',
            localDateTime('13.12.2016 14:30:00'),
            '201709VPbo://TEST70dc0e5d947d44bdbbef2092ee473f99',
            1,
            FileType.DOMAIN_OF_INFLUENCE,
            null,
            'valid-doi.xml',
            'Canton de Genève',
            null,
            fedCanCom(),
    )
  }

    private static List<DomainOfInfluenceVo> fedCanCom() {
        [
                new DomainOfInfluenceVo('CH', 'CH', 'Confédération', 'CH'),
                new DomainOfInfluenceVo('CT', 'GE', 'Canton de Genève', 'GE'),
                new DomainOfInfluenceVo('MU', '6621', 'Ville de Genève', 'Genève')
        ]
    }

}
