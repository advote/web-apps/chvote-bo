/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.consistency.checks

import static ch.ge.ve.bo.repository.dataset.OperationDataset.PRINTER_TEMPLATE
import static ch.ge.ve.bo.service.operation.model.ConsistencyErrorVo.ConsistencyErrorType.VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY

import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.RegisterMetadata
import ch.ge.ve.bo.service.TestHelpers
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration
import ch.ge.ve.bo.service.model.printer.PrinterMunicipalityMapping
import ch.ge.ve.bo.service.model.printer.PrinterSwissAbroadMapping
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo
import ch.ge.ve.bo.service.operation.register.RegisterMetadataService
import ch.ge.ve.bo.service.printer.PrinterTemplateService
import ch.ge.ve.bo.service.utils.OperationStatusBuilder
import spock.lang.Specification

class AllMunicipalitiesShouldHaveAPrinterTest extends Specification {
    def registerMetadataService = Mock(RegisterMetadataService)
    def printerTemplateService = Mock(PrinterTemplateService)
    private allMunicipalitiesShouldHaveAPrinter = new AllMunicipalitiesShouldHaveAPrinter(registerMetadataService, TestHelpers.objectMapper, printerTemplateService)


    def "CheckForOperation() should not check if voting material is in readonly"() {
        given:
        def operation = OperationDataset.electoralOperation()

        when:
        def result = allMunicipalitiesShouldHaveAPrinter.checkForOperation(
                new OperationStatusBuilder().withVotingMaterialInReadonly().build(), operation)

        then:
        0 == result.consistencyErrors.size()
    }


    def "CheckForOperation() should returns municipality for which there is no printer config"() {
        given: "an operation"
        def operation = OperationDataset.electoralOperation()

        and: "a printer template"
        def printer = "printer"
        def printerTemplate = new PrinterTemplateVo(PRINTER_TEMPLATE,
                [new PrinterConfiguration(printer, printer, null, 1)] as PrinterConfiguration[],
                [new PrinterMunicipalityMapping(1, "mapped_municipality", printer)] as PrinterMunicipalityMapping[],
                printer,
                new PrinterSwissAbroadMapping(printer))
        printerTemplateService.getPrinterTemplate("GE", PRINTER_TEMPLATE) >> printerTemplate


        and: "a register metadata"
        RegisterMetadata metadata = new RegisterMetadata()
        metadata.setReport(TestHelpers.getResourceString(AllMunicipalitiesShouldHaveAPrinterTest, "register_import_report_with_unmapped_municipality.json"))
        registerMetadataService.getAllByFileOperationIdAndValidated(operation.id, true) >> [metadata]

        when:
        def result = allMunicipalitiesShouldHaveAPrinter.checkForOperation(
                new OperationStatusBuilder().build(), operation)

        then:
        1 == result.consistencyErrors.size()
        VM_NO_PRINTER_ASSOCIATED_TO_MUNICIPALITY == result.consistencyErrors.get(0).errorType
        "[filename:old.xml, municipality:unmapped_municipality, ofsId:2]"  == result.consistencyErrors.get(0).errorsParameters.toString()
    }

    def "CheckForOperation() should returns no error if printer template is not yet defined"() {
        given: "an operation with no printer template defined"
        def operation = OperationDataset.electoralOperation()
        operation.setPrinterTemplate(null)

        and: "a register metadata"
        RegisterMetadata metadata = new RegisterMetadata()
        metadata.setReport(TestHelpers.getResourceString(AllMunicipalitiesShouldHaveAPrinterTest, "register_import_report_with_unmapped_municipality.json"))
        registerMetadataService.getAllByFileOperationIdAndValidated(operation.id, true) >> [metadata]

        when:
        def result = allMunicipalitiesShouldHaveAPrinter.checkForOperation(
                new OperationStatusBuilder().build(), operation)

        then:
        0 == result.consistencyErrors.size()
    }


}
