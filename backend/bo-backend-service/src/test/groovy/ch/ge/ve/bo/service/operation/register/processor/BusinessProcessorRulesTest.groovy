/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.processor

import ch.ge.ve.bo.service.operation.parameters.doi.model.DomainOfInfluenceVo
import ch.ge.ve.bo.service.operation.register.model.Voter
import spock.lang.Specification

class BusinessProcessorRulesTest extends Specification {

    def "doi's test : thrown NullPointerException if Voter is null"() {
        given:
        def nullVoter=null
        def nullOperationDomainOfInfluence=null

        when: "voter is null"
        BusinessProcessorRules.isVoterWithoutDoiOfOperation(nullOperationDomainOfInfluence, nullVoter)

        then:
        thrown(NullPointerException)
    }


    def "test if none of the voter's doi matches doi operation"() {
        given: "a domain of influence list for current operation and a voter with his domain of influence"
        Set<String> operationDois = new HashSet<>()
        operationDois.add(getMockTypeId(mockDoiCt()))
        operationDois.add(getMockTypeId(mockDoiCh()))

        def voter = new Voter(0,0)
        voter.domainOfInfluence.add(mockDoiMu())

        when: "is voter without doi of operation"
        def result = BusinessProcessorRules.isVoterWithoutDoiOfOperation(operationDois, voter)

        then:
        result == true
    }


    def "doi voter 'MU' matches with doi operation"() {
        given: "a domain of influence list for current operation and a voter with his domain of influence"
        Set<String> operationDois = new HashSet<>()
        operationDois.add(getMockTypeId(mockDoiMu()))
        operationDois.add(getMockTypeId(mockDoiCh()))

        def voter = new Voter(0,0)
        voter.domainOfInfluence.add(mockDoiMu())

        when: "is voter without doi of operation"
        def result = BusinessProcessorRules.isVoterWithoutDoiOfOperation(operationDois, voter)

        then:
        result == false
    }

    def "doi voter empty does not matches with doi operation"() {
        given: "a domain of influence list for current operation and a voter with his domain of influence"
        Set<String> operationDois = new HashSet<>()
        operationDois.add(getMockTypeId(mockDoiMu()))
        operationDois.add(getMockTypeId(mockDoiCh()))

        def voter = new Voter(0,0)

        when: "is voter without doi of operation"
        def result = BusinessProcessorRules.isVoterWithoutDoiOfOperation(operationDois, voter)

        then:
        result == true
    }

    private DomainOfInfluenceVo mockDoiCh() {
        new DomainOfInfluenceVo("CH", "CH", "Opération fédérale", "fédérale")
    }

    private DomainOfInfluenceVo mockDoiCt() {
        new DomainOfInfluenceVo("CT", "GE", "Opération cantonale", "cantonale")
    }

    private DomainOfInfluenceVo mockDoiMu() {
        new DomainOfInfluenceVo("MU", "6621", "Opération municipale", "municipale")
    }

    private String getMockTypeId(DomainOfInfluenceVo doi){
        return doi.type+"."+doi.id
    }
}

