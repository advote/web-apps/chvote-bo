/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.model.printer

import static ch.ge.ve.bo.service.TestHelpers.getResourceString
import static ch.ge.ve.bo.service.TestHelpers.readPublicKeyFromClasspath

import ch.ge.ve.bo.service.TestHelpers
import spock.lang.Specification

class PrinterTemplateVoTest extends Specification {

  Map<Integer, String> municipalitiesToCreate = getResourceString(PrinterTemplateVoTest, "ge-municipalities.txt")
          .split("\\n")
          .collectEntries() {
    def split = it.split(":")
    [(Integer.parseInt(split[0])): split[1]]
  }


  def availablePrinters = [
          new PrinterConfiguration("BELONGS_TO_MANAGEMENT_ENTITY:Printer1", "printer 1", readPublicKeyFromClasspath("public-key-pa0.pub"), 10001),
          new PrinterConfiguration("BELONGS_TO_MANAGEMENT_ENTITY:Printer2", "printer 2", readPublicKeyFromClasspath("public-key-pa1.pub"), 10002),
          new PrinterConfiguration("BELONGS_TO_MANAGEMENT_ENTITY:Printer3", "printer 3", readPublicKeyFromClasspath("public-key-pa2.pub"), 10003),
          new PrinterConfiguration("BELONGS_TO_MANAGEMENT_ENTITY:Printer4", "printer 4", readPublicKeyFromClasspath("public-key-pa3.pub"), 10004)
  ]


  def nbTemplateToCreate = 5


  def "(actually not a test) it should create some templates"() {
    given:
    def rnd = new Random()

    def templates = (1..nbTemplateToCreate).collect { createTemplate(rnd, "template $it") }


    when:

    File file = File.createTempFile("PrinterTemplates.", ".json")
    file.write TestHelpers.getObjectMapper().writeValueAsString(templates)

    then:
    println "A printer templates definition file has been generated at ${file.absolutePath}"
    notThrown Throwable
  }

  private PrinterTemplateVo createTemplate(Random rnd, String name) {
    Collections.shuffle(this.availablePrinters, rnd)
    def printersToCreate = this.availablePrinters.subList(0, 1 + rnd.nextInt(this.availablePrinters.size() - 1))


    def printerMunicipalityMappings = municipalitiesToCreate.entrySet().collect {
      new PrinterMunicipalityMapping(it.key, it.value, printersToCreate[rnd.nextInt(printersToCreate.size())].id)
    }

    def printerSwissAbroadMapping = new PrinterSwissAbroadMapping(printersToCreate[rnd.nextInt(printersToCreate.size())].id)

    def printerTemplateVO = new PrinterTemplateVo(
            name,
            printersToCreate as PrinterConfiguration[],
            printerMunicipalityMappings as PrinterMunicipalityMapping[],
            printersToCreate[rnd.nextInt(printersToCreate.size())].id,
            printerSwissAbroadMapping)
    printerTemplateVO
  }

}
