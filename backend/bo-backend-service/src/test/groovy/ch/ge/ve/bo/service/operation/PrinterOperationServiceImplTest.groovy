/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation

import static ch.ge.ve.bo.repository.dataset.OperationDataset.Options
import static ch.ge.ve.bo.repository.dataset.OperationDataset.electoralOperation

import ch.ge.ve.bo.repository.ConnectedUser
import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.UnsecuredOperationRepository
import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.DownloadToken
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.repository.exception.InvalidRealmTypeException
import ch.ge.ve.bo.service.model.PactConfiguration
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo
import ch.ge.ve.bo.service.operation.download.token.DownloadTokenService
import ch.ge.ve.bo.service.operation.model.ConfigurationStatusVo
import ch.ge.ve.bo.service.operation.model.OperationStatusVo
import ch.ge.ve.bo.service.operation.model.TallyArchiveStatusVo
import ch.ge.ve.bo.service.operation.model.VotingMaterialStatusVo
import ch.ge.ve.bo.service.operation.model.VotingPeriodStatusVo
import ch.ge.ve.bo.service.operation.status.OperationStatusService
import ch.ge.ve.bo.service.printer.PrinterTemplateService
import ch.ge.ve.chvote.pactback.contract.operation.status.VotingMaterialsStatusVo
import java.time.Duration
import java.time.LocalDateTime
import spock.lang.Specification

/**
 * Unit tests for the printer operation services ({@link ch.ge.ve.bo.service.operation.PrinterOperationService}).
 */
class PrinterOperationServiceImplTest extends Specification {
  File tempDir = File.createTempDir()
  def unsecuredOperationRepositoryMock = Mock(UnsecuredOperationRepository)
  def operationStatusServiceMock = Mock(OperationStatusService)
  def printerTemplateServiceMock = Mock(PrinterTemplateService)
  def downloadTokenService = Mock(DownloadTokenService)
  def pactConfiguration = Mock(PactConfiguration)
  def downloadToken = new DownloadToken()

  void setup() {
    downloadToken.setId("test")
  }

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def printerOperationService = new PrinterOperationServiceImpl(unsecuredOperationRepositoryMock,
          operationStatusServiceMock, printerTemplateServiceMock, downloadTokenService, pactConfiguration)

  def 'findByPrinterManagementEntity by non-printer user should throw an exception'() {
    given: 'a non-printer user'
    def managementEntity = "managementEntity"
    ConnectedUserTestUtils.connectUser("test", "GE", managementEntity)

    when: 'calling the findByPrinterManagementEntity service'
    printerOperationService.findByPrinterManagementEntity()

    then: 'an exception should be thrown'
    0 * unsecuredOperationRepositoryMock.findByPrinterTemplateIn(_)
    thrown(InvalidRealmTypeException)
  }

  def 'findByPrinterManagementEntity Should return operation for which there is voting material available and status is CREATED'() {
    given: 'a printer user'
    preconditionForFindByPrinterManagementEntity(VotingMaterialsStatusVo.State.CREATED)

    when: 'calling the findByPrinterManagementEntity service'
    def operations = printerOperationService.findByPrinterManagementEntity()

    then: 'unsecuredOperationRepository should be searched'
    [4l] == operations.id
    Duration.between(operations.printerFileGenerationDate[0], LocalDateTime.now()).toMinutes() < 1

    1 * downloadTokenService.getOrCreateDownloadToken(_ as Operation, tempDir.toString() + File.separator + "test2") >>
            downloadToken
    operations.printerFileDownloadToken[0].matches("test")

  }


  def 'findByPrinterManagementEntity Should return operation for which there is voting material available and status is VALIDATED'() {
    given: 'a printer user'
    preconditionForFindByPrinterManagementEntity(VotingMaterialsStatusVo.State.VALIDATED)

    when: 'calling the findByPrinterManagementEntity service'
    def operations = printerOperationService.findByPrinterManagementEntity()

    then: 'unsecuredOperationRepository should be searched'
    [4l] == operations.id
    Duration.between(operations.printerFileGenerationDate[0], LocalDateTime.now()).toMinutes() < 1


    1 * downloadTokenService.getOrCreateDownloadToken(_ as Operation, tempDir.toString() + File.separator + "test2") >>
            downloadToken
    operations.printerFileDownloadToken[0].matches("test")

  }

  private void preconditionForFindByPrinterManagementEntity(VotingMaterialsStatusVo.State state) {
    pactConfiguration.getVotingMaterialBaseLocation() >> tempDir.toString()
    ConnectedUserTestUtils.connectUser("test", ConnectedUser.REALM_PRINTER, "printer1")
    printerTemplateServiceMock.getPrinterTemplatesByPrinterId(_) >> [new PrinterTemplateVo("tmp1", null, null, null, null)]

    def operationWithoutVotingMaterialStatus = electoralOperation(new Options().with(Options.id, 1))
    def operationWithoutVotingMaterial = electoralOperation(new Options().with(Options.id, 2))
    def operationWithVotingMaterialStatusButNoCardToPrint = electoralOperation(new Options().with(Options.id, 3))
    def operationWithVotingMaterialStatusAndCardToPrint = electoralOperation(new Options().with(Options.id, 4))


    def confStatus = new ConfigurationStatusVo.Builder()
            .setState(ConfigurationStatusVo.State.INCOMPLETE, LocalDateTime.now(), "user")
            .setCompletedSections(["test": false])
            .build()

    VotingPeriodStatusVo votingPeriodStatusVo = new VotingPeriodStatusVo(VotingPeriodStatusVo.State.INCOMPLETE, [:], null, null, null, null)

    OperationStatusVo statusOpe1 = new OperationStatusVo(confStatus, null, null, TallyArchiveStatusVo.notRequested(), null, DeploymentTarget.NOT_DEFINED)

    OperationStatusVo statusOpe2 = new OperationStatusVo(confStatus,
            new VotingMaterialStatusVo(VotingMaterialStatusVo.State.AVAILABLE_FOR_CREATION, LocalDateTime.now(), null, null),
            votingPeriodStatusVo, TallyArchiveStatusVo.notRequested(), null, null)

    OperationStatusVo statusOpe3 = new OperationStatusVo(confStatus,
            createVotingMaterialStatusWithVotingMat(["printer2": "test"], state),
            votingPeriodStatusVo, TallyArchiveStatusVo.notRequested(), null, null)

    OperationStatusVo statusOpe4 = new OperationStatusVo(confStatus,
            createVotingMaterialStatusWithVotingMat(["printer2": "test", "printer1": "test2"], state),
            votingPeriodStatusVo, TallyArchiveStatusVo.notRequested(), null, null)

    tempDir.toPath().resolve("test").toFile().createNewFile()
    tempDir.toPath().resolve("test2").toFile().createNewFile()


    unsecuredOperationRepositoryMock.findByPrinterTemplateIn(["tmp1"]) >> [
            operationWithoutVotingMaterialStatus,
            operationWithoutVotingMaterial,
            operationWithVotingMaterialStatusButNoCardToPrint,
            operationWithVotingMaterialStatusAndCardToPrint
    ]


    operationStatusServiceMock.getStatus(operationWithoutVotingMaterialStatus.id) >> statusOpe1
    operationStatusServiceMock.getStatus(operationWithoutVotingMaterial.id) >> statusOpe2
    operationStatusServiceMock.getStatus(operationWithVotingMaterialStatusButNoCardToPrint.id) >> statusOpe3
    operationStatusServiceMock.getStatus(operationWithVotingMaterialStatusAndCardToPrint.id) >> statusOpe4
  }

  private VotingMaterialStatusVo createVotingMaterialStatusWithVotingMat(LinkedHashMap<String, String> printerFiles, VotingMaterialsStatusVo.State state) {
    VotingMaterialsStatusVo materialsStatusVo = new VotingMaterialsStatusVo(null, LocalDateTime.now(), state, null, null, null, printerFiles, null)
    new VotingMaterialStatusVo(materialsStatusVo, [:], "boh-vprt")
  }


}
