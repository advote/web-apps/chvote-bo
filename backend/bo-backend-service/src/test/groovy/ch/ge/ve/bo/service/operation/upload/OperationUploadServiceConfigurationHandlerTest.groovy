/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload

import static ch.ge.ve.bo.FileType.DOCUMENT_CERTIFICATE
import static ch.ge.ve.bo.FileType.DOCUMENT_FAQ
import static ch.ge.ve.bo.FileType.DOCUMENT_TERMS
import static ch.ge.ve.bo.FileType.DOMAIN_OF_INFLUENCE
import static ch.ge.ve.bo.FileType.ELECTION_REPOSITORY
import static ch.ge.ve.bo.FileType.VOTATION_REPOSITORY
import static ch.ge.ve.bo.repository.dataset.OperationDataset.electoralOperation
import static ch.ge.ve.bo.service.TestHelpers.getObjectMapper
import static ch.ge.ve.bo.service.TestHelpers.getResourceString
import static ch.ge.ve.bo.service.TestHelpers.parseMultiPartRequest
import static ch.ge.ve.bo.service.TestHelpers.readPublicKeyFromClasspath
import static ch.ge.ve.bo.service.dataset.DomainOfInfluenceFileVoDataset.validDomainOfInfluenceFileVo
import static ch.ge.ve.bo.service.dataset.VotationRepositoryFileVoDataset.votationRepositoryFileVo

import ch.ge.ve.bo.Language
import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.dataset.VoterTestingCardsLotDataset
import ch.ge.ve.bo.repository.entity.CardType
import ch.ge.ve.bo.repository.entity.DictionaryDataType
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot
import ch.ge.ve.bo.service.dictionnary.DictionaryService
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.model.ContentAndFileName
import ch.ge.ve.bo.service.model.PactConfiguration
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration
import ch.ge.ve.bo.service.operation.model.BallotDocumentationVo
import ch.ge.ve.bo.service.operation.model.ElectionPagePropertiesModelVo
import ch.ge.ve.bo.service.operation.model.HighlightedQuestionVo
import ch.ge.ve.bo.service.operation.model.LocalizedHighlightedQuestionVo
import ch.ge.ve.bo.service.operation.model.OperationStatusAndConsistencyVo
import ch.ge.ve.bo.service.operation.parameters.document.BallotDocumentationService
import ch.ge.ve.bo.service.operation.parameters.document.HighlightedQuestionService
import ch.ge.ve.bo.service.operation.parameters.election.ElectionPagePropertiesService
import ch.ge.ve.bo.service.operation.status.OperationStatusAndConsistencyService
import ch.ge.ve.bo.service.printer.PrinterTemplateService
import ch.ge.ve.bo.service.testing.card.TestingCardRegisterGeneratorService
import java.time.Clock
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import spock.lang.Specification

class OperationUploadServiceConfigurationHandlerTest extends Specification {
  public static final int OPERATION_ID = 100000
  HttpPost httpPost = null
  def operationRepositoryMock = Mock(SecuredOperationRepository)
  def fileServiceMock = Mock(FileService)
  def httpClient = Mock(HttpClient)
  def printerTemplateService = Mock(PrinterTemplateService)
  def testingCardRegisterGeneratorService = Mock(TestingCardRegisterGeneratorService)
  def operationStatusAndConsistencyService = Mock(OperationStatusAndConsistencyService)
  def consistentOperation = Mock(OperationStatusAndConsistencyVo)
  def inconsistentOperation = Mock(OperationStatusAndConsistencyVo)
  def ballotDocumentationService = Mock(BallotDocumentationService)
  def electionPagePropertiesService = Mock(ElectionPagePropertiesService)
  def highlightedQuestionService = Mock(HighlightedQuestionService)
  def dictionaryService = Mock(DictionaryService)

  static def pactConfig = new PactConfiguration()
  def handler = new OperationUploadServiceConfigurationHandler(
          pactConfig,
          operationRepositoryMock,
          fileServiceMock,
          printerTemplateService,
          objectMapper,
          httpClient,
          testingCardRegisterGeneratorService,
          operationStatusAndConsistencyService,
          Clock.fixed(Instant.EPOCH, ZoneOffset.UTC),
          ballotDocumentationService,
          electionPagePropertiesService,
          highlightedQuestionService,
          dictionaryService,
  )


  def cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def setup() {
    ConnectedUserTestUtils.connectUser()
    consistentOperation.isConfigurationCompleteAndConsistent() >> true
    inconsistentOperation.isConfigurationCompleteAndConsistent() >> false
  }


  def setupSpec() {
    pactConfig.setUsername("user")
    pactConfig.setPassword("pass")
    pactConfig.url.operationConfigurationUpload = "operationConfigurationUploadUrl/{0,number,#}"
  }

  def "uploadConfigurationToPact should throw exception if operation is not consistent or incomplete"() {
    given:
    operationStatusAndConsistencyService.getOperationStatusAndConsistency(OPERATION_ID) >> inconsistentOperation

    when: 'upload configuration to pact'
    handler.upload(OPERATION_ID)

    then:
    thrown(TryToUploadUncompleteOrInconsitentOperation)
  }

  def "uploadConfigurationToPact"() {
    given: 'An operation that can be uploaded'
    operationStatusAndConsistencyService.getOperationStatusAndConsistency(OPERATION_ID) >> consistentOperation
    def operation = electoralOperation(new OperationDataset.Options().with(OperationDataset.Options.id, OPERATION_ID))
    fileServiceMock.getFiles(OPERATION_ID, true, EnumSet.of(DOMAIN_OF_INFLUENCE, DOCUMENT_FAQ, VOTATION_REPOSITORY,
            ELECTION_REPOSITORY, DOCUMENT_CERTIFICATE, DOCUMENT_TERMS)) >>
            [votationRepositoryFileVo(), validDomainOfInfluenceFileVo(true)]
    operationRepositoryMock.findOne(OPERATION_ID, true) >> operation
    printerTemplateService.getVirtualPrinterForTestCard() >> new PrinterConfiguration("Printer1", "printer 1", readPublicKeyFromClasspath("public-key-pa1.pub"), 99999)

    VoterTestingCardsLot lot1 = VoterTestingCardsLotDataset.voterTestingCardsLot(CardType.TEST_SITE_TESTING_CARD)
    lot1.setLotName("lot 1")
    lot1.setShouldPrint(false)
    Map<VoterTestingCardsLot, Map<String, String>> testingCards = new HashMap<>()
    testingCards.put(lot1, ["boh printer": "<voterDelivery>whatever</voterDelivery>"])
    testingCardRegisterGeneratorService.generateForOperation(OPERATION_ID, true) >> testingCards

    ballotDocumentationService.findByOperation(OPERATION_ID) >>
            [new BallotDocumentationVo("ballot", "bd.pdf", "label", "me", 1, Language.DE, LocalDateTime.of(2010, 1, 1, 10, 20))]
    ballotDocumentationService.getFileContent(1) >> new ContentAndFileName("bd.pdf", "This is a doc".bytes)

    highlightedQuestionService.findByOperation(OPERATION_ID) >>
            [new HighlightedQuestionVo(1, [
                    new LocalizedHighlightedQuestionVo(2, "hq1.pdf", LocalDateTime.of(2010, 1, 1, 10, 20), "question FR ?", Language.FR),
                    new LocalizedHighlightedQuestionVo(3, "hq2.pdf", LocalDateTime.of(2010, 1, 1, 10, 50), "question DE ?", Language.DE)
            ])]
    highlightedQuestionService.getFileContent(2) >> new ContentAndFileName("hq1.pdf", "FR".bytes)
    highlightedQuestionService.getFileContent(3) >> new ContentAndFileName("hq2.pdf", "FR".bytes)

    dictionaryService.get(DictionaryDataType.VOTE_RECEIVER_DEFAULT_TRANSLATIONS, String.class, OPERATION_ID) >> ['{}']
    dictionaryService.getOne(DictionaryDataType.VOTE_RECEIVER_GROUP_VOTATION, Boolean.class, OPERATION_ID) >> true

    electionPagePropertiesService.getBallotToModelMapping(OPERATION_ID) >>
            ["ballot": new ElectionPagePropertiesModelVo(1, "model name", true, true, true, true, true, true, true,
                    "model", true, true, true, ["col1", "col2"] as String[], ["col3"] as String[])]

    when: 'upload configuration to pact'
    handler.upload(OPERATION_ID)

    then: 'operation is uploaded'
    1 * httpClient.execute({ httpPost = it }, _)
    httpPost.method == "POST"
    httpPost.getURI().toString() == "operationConfigurationUploadUrl/" + OPERATION_ID

    ByteArrayOutputStream stream = new ByteArrayOutputStream()
    httpPost.entity.writeTo(stream)
    def parts = parseMultiPartRequest(stream.toString("UTF-8"))
    parts.size() == 8
    parts[7].headers["Content-Disposition"] == 'form-data; name="operation"'
    parts[7].headers["Content-Type"] == 'application/json; charset=UTF-8'
    JSONAssert.assertEquals(getResourceString(getClass(), "configurationRequest.json"), parts[7].content, JSONCompareMode.NON_EXTENSIBLE)

    parts[0].headers["Content-Disposition"] == 'form-data; name="data"; filename="highlighted-questions-2.zip"'
    parts[1].headers["Content-Disposition"] == 'form-data; name="data"; filename="highlighted-questions-3.zip"'
    parts[2].headers["Content-Disposition"] == 'form-data; name="data"; filename="ballot-documentation-1.zip"'

    parts[3].headers["Content-Disposition"] == 'form-data; name="data"; filename="cards-test-not-printable_EL042050_boh-printer_lot-1_1970-01-01-00h00m00s.zip"'
    parts[4].headers["Content-Disposition"] == 'form-data; name="data"; filename="file-0.zip"'
    parts[5].headers["Content-Disposition"] == 'form-data; name="data"; filename="file-1.zip"'
    parts[6].headers["Content-Disposition"] == 'form-data; name="data"; filename="translations.zip"'

  }


}
