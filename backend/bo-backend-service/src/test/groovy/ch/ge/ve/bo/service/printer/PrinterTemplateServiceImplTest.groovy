/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.printer

import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.service.TestHelpers
import spock.lang.Specification

class PrinterTemplateServiceImplTest extends Specification {
  def service = new PrinterTemplateServiceImpl(TestHelpers.objectMapper)

  void setup() {
    ConnectedUserTestUtils.connectUserWithAllRoles()
  }

  void cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def "GetVirtualPrinterForTestCard"() {
    when:
    def result = service.getVirtualPrinterForTestCard()

    then:
    result.name == "BOH Virtual Printer"
  }

  def "GetPrinterTemplatesByPrinterId"() {
    when:
    def result = service.getPrinterTemplatesByPrinterId("boh-vprt")

    then:
    result.templateName == ["BOH Printer", "BOH Printer", "BOH Printer"]
  }

  def "GetPrinterTemplate"() {
    when:
    def result = service.getPrinterTemplate("GE", "BOH Printer")

    then:
    result.printerConfigurations.length == 1

  }
}
