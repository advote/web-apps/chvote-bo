/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.parameters.document

import ch.ge.ve.bo.Language
import ch.ge.ve.bo.repository.BallotDocumentationRepository
import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.BallotDocumentation
import ch.ge.ve.bo.repository.entity.Operation
import ch.ge.ve.bo.service.TestHelpers
import spock.lang.Specification

class BallotDocumentationServiceImplTest extends Specification {

    def securedOperationRepository = Mock(SecuredOperationRepository)
    def ballotDocumentationRepository = Mock(BallotDocumentationRepository.class)

    def service = new BallotDocumentationServiceImpl(
            TestHelpers.passThroughSecuredOperationHolderService(),
            securedOperationRepository,
            ballotDocumentationRepository)

    void setup() {
        ConnectedUserTestUtils.connectUser()
    }

    void cleanup() {
        ConnectedUserTestUtils.disconnectUser()
    }

    def "AddBallotDocumentation"() {
        given:
        Operation operation = OperationDataset.votingOperation()
        securedOperationRepository.findOne(operation.id, false) >> operation

        when:
        def result = service.addBallotDocumentation(operation.id, "ballotId", Language.DE, "label", "fileName", "content".bytes)

        then:
        1 * ballotDocumentationRepository.save(_) >> { args -> def q = args[0]; q.id = 1; return q }
        "ballotId" == result.ballotId
        1 == result.id
    }

    def "DeleteBallotDocumentation"() {
        given:
        def ballotDocumentation = new BallotDocumentation()
        ballotDocumentationRepository.findById(1) >> Optional.of(ballotDocumentation)

        when:
        service.deleteBallotDocumentation(1)

        then:
        1 * ballotDocumentationRepository.delete(ballotDocumentation)
    }

    def "FindByOperation"() {
        given:
        def ballotDocumentation = new BallotDocumentation()
        ballotDocumentation.fileName = "test"
        ballotDocumentationRepository.findByOperationId(1) >> [ballotDocumentation]

        when:
        def response = service.findByOperation(1)

        then:
        1 == response.size()
        "test" == response.get(0).fileName
    }

    def "GetFileContent"() {
        given:
        def ballotDocumentation = new BallotDocumentation()
        ballotDocumentation.fileName = "test"
        ballotDocumentation.fileContent = "content".bytes
        ballotDocumentationRepository.findById(1) >> Optional.of(ballotDocumentation)


        when:
        def content = service.getFileContent(1)

        then:
        "test" == content.fileName
        "content" == new String(content.getContent())
    }
}
