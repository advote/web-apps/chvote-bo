/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register.model

import com.fasterxml.jackson.databind.ObjectMapper
import org.skyscreamer.jsonassert.JSONAssert
import spock.lang.Specification

class DetailedReportTest extends Specification {


    def "serialisation/deserialisation"() {
        given:
        def objectMapper = new ObjectMapper()
        def detailReport = new DetailedReport()
        detailReport.addSwiss("Geneva")
        detailReport.addSwiss("Geneva")
        detailReport.addSwiss("Lancy")


        detailReport.addForeigner("Geneva")
        detailReport.addSwissAbroad("FR", 1)
        detailReport.addSwissAbroad("FR", 1)
        detailReport.addSwissAbroad("FR", 2)
        detailReport.addSwissAbroad("DE", 3)
        detailReport.addDoi("TEST1")
        detailReport.addDoi("TEST2")
        detailReport.addDoi(null)

        when:
        def json = objectMapper.writeValueAsString(detailReport)
        def detailReportRestored = objectMapper.readValue(json, DetailedReport)
        def json2 = objectMapper.writeValueAsString(detailReportRestored)


        then:
        JSONAssert.assertEquals('''
{"domainOfInfluence":"TEST1, TEST2",
"swiss":[
    {"municipality":"Geneva","count":2},
    {"municipality":"Lancy","count":1}],
"foreigner":[
    {"municipality":"Geneva","count":1}
],
"swissAbroad":[
    {"lang":"DE","postage-code":3,"count":1},
    {"lang":"FR","postage-code":1,"count":2},
    {"lang":"FR","postage-code":2,"count":1}
    ],
"municipalities": []
}
''', json,
                true)
        json == json2
    }

    def "Merge"() {
        given:
        def objectMapper = new ObjectMapper()
        def detailReport = new DetailedReport()
        detailReport.addSwiss("Geneva")
        detailReport.addSwiss("Geneva")
        detailReport.addSwiss("Lancy")
        detailReport.addMunicipality(1, "test")


        detailReport.addForeigner("Geneva")
        detailReport.addSwissAbroad("FR", 1)
        detailReport.addSwissAbroad("FR", 1)
        detailReport.addSwissAbroad("FR", 2)
        detailReport.addSwissAbroad("DE", 3)
        detailReport.addDoi("TEST1")
        detailReport.addDoi("TEST2")

        def detailReport2 = new DetailedReport()
        detailReport2.addSwiss("Geneva")
        detailReport2.addSwiss("Plan les ouates")


        detailReport2.addForeigner("Lancy")
        detailReport2.addSwissAbroad("FR", 4)
        detailReport2.addSwissAbroad("DE", 3)
        detailReport2.addDoi("TEST1")
        detailReport2.addDoi("TEST5")
        detailReport2.addMunicipality(1, "test")
        detailReport2.addMunicipality(2, "test2")
        when:
        def json = objectMapper.writeValueAsString(DetailedReport.merge(detailReport, detailReport2))

        then:
        JSONAssert.assertEquals('''
{"domainOfInfluence":"TEST1, TEST2, TEST5",
"swiss":[{"municipality":"Geneva","count":3},{"municipality":"Lancy","count":1},{"municipality":"Plan les ouates","count":1}],
"foreigner":[{"municipality":"Geneva","count":1},{"municipality":"Lancy","count":1}],
"swissAbroad":[
    {"lang":"DE","postage-code":3,"count":2},
    {"lang":"FR","postage-code":1,"count":2},
    {"lang":"FR","postage-code":2,"count":1},
    {"lang":"FR","postage-code":4,"count":1}],
"municipalities": [
    {
      "ofsId": 1,
      "name": "test"
    },
    {
      "ofsId": 2,
      "name": "test2"
    }
  ]
}''', json, true)
    }
}
