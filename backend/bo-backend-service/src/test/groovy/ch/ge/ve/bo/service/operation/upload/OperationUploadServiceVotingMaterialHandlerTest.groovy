/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.upload

import static ch.ge.ve.bo.repository.dataset.OperationDataset.electoralOperation
import static ch.ge.ve.bo.service.TestHelpers.getObjectMapper
import static ch.ge.ve.bo.service.TestHelpers.getResourceString
import static ch.ge.ve.bo.service.TestHelpers.parseMultiPartRequest
import static ch.ge.ve.bo.service.TestHelpers.readPublicKeyFromClasspath

import ch.ge.ve.bo.FileType
import ch.ge.ve.bo.repository.ConnectedUserTestUtils
import ch.ge.ve.bo.repository.SecuredOperationRepository
import ch.ge.ve.bo.repository.dataset.FileDataset
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.dataset.VoterTestingCardsLotDataset
import ch.ge.ve.bo.repository.entity.CardType
import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.VoterTestingCardsLot
import ch.ge.ve.bo.service.dataset.FileVoDataset
import ch.ge.ve.bo.service.file.FileService
import ch.ge.ve.bo.service.model.PactConfiguration
import ch.ge.ve.bo.service.model.printer.PrinterConfiguration
import ch.ge.ve.bo.service.model.printer.PrinterMunicipalityMapping
import ch.ge.ve.bo.service.model.printer.PrinterSwissAbroadMapping
import ch.ge.ve.bo.service.model.printer.PrinterTemplateVo
import ch.ge.ve.bo.service.operation.model.OperationStatusAndConsistencyVo
import ch.ge.ve.bo.service.operation.status.OperationStatusAndConsistencyService
import ch.ge.ve.bo.service.printer.PrinterTemplateService
import ch.ge.ve.bo.service.testing.card.TestingCardRegisterGeneratorService
import java.time.Clock
import java.time.Instant
import java.time.ZoneOffset
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpPost
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode
import spock.lang.Specification

class OperationUploadServiceVotingMaterialHandlerTest extends Specification {
  public static final int OPERATION_ID = 100000
  HttpPost httpPost = null
  def operationRepositoryMock = Mock(SecuredOperationRepository)
  def fileServiceMock = Mock(FileService)
  def httpClient = Mock(HttpClient)
  def printerTemplateService = Mock(PrinterTemplateService)
  def testingCardRegisterGeneratorService = Mock(TestingCardRegisterGeneratorService)
  def operationStatusAndConsistencyService = Mock(OperationStatusAndConsistencyService)
  def consistentOperation = Mock(OperationStatusAndConsistencyVo)
  def inconsistentOperation = Mock(OperationStatusAndConsistencyVo)


  static def pactConfig = new PactConfiguration()

  def cleanup() {
    ConnectedUserTestUtils.disconnectUser()
  }

  def setup() {
    ConnectedUserTestUtils.connectUser()
    consistentOperation.isVotingMaterialCompleteAndConsistent() >> true
    inconsistentOperation.isVotingMaterialCompleteAndConsistent() >> false
  }

  def votingMaterialHandler = new OperationUploadServiceVotingMaterialHandler(

          pactConfig,
          operationRepositoryMock,
          fileServiceMock,
          printerTemplateService,
          objectMapper,
          httpClient,
          testingCardRegisterGeneratorService,
          operationStatusAndConsistencyService,
          Clock.fixed(Instant.EPOCH, ZoneOffset.UTC)
  )

  def setupSpec() {
    pactConfig.setUsername("user")
    pactConfig.setPassword("pass")
    pactConfig.url.operationConfigurationUpload = "operationConfigurationUploadUrl/{0,number,#}"
    pactConfig.url.votingMaterialUpload = "votingMaterialUploadUrl/{0,number,#}"
  }

  def "uploadVotingMaterialToPact should throw exception if operation is not consistent or incomplete"() {
    given:
    operationStatusAndConsistencyService.getOperationStatusAndConsistency(OPERATION_ID) >> inconsistentOperation

    when: 'upload voting material to pact'
    votingMaterialHandler.upload(OPERATION_ID)

    then:
    thrown(TryToUploadUncompleteOrInconsitentOperation)
  }


  def "uploadVotingMaterialToPact"() {
    given:
    operationStatusAndConsistencyService.getOperationStatusAndConsistency(OPERATION_ID) >> consistentOperation

    def operation = electoralOperation(
            new OperationDataset.Options()
                    .with(OperationDataset.Options.id, OPERATION_ID)
                    .with(OperationDataset.Options.votingCardTitle, "voting card")
                    .with(OperationDataset.Options.printerTemplate, "template")
                    .with(OperationDataset.Options.deploymentTarget, DeploymentTarget.SIMULATION)
                    .with(OperationDataset.Options.simulationName, "Simulation name"),

    )
    fileServiceMock.getFiles(operation.getId(), true, EnumSet.of(FileType.REGISTER)) >>
            [
                    FileVoDataset.someFileVo(new FileDataset.Options()
                            .with(FileDataset.Options.fileType, FileType.REGISTER)
                            .with(FileDataset.Options.id, 1)),
                    FileVoDataset.someFileVo(new FileDataset.Options()
                            .with(FileDataset.Options.fileType, FileType.REGISTER)
                            .with(FileDataset.Options.id, 2)
                    )
            ]
    operationRepositoryMock.findOne(OPERATION_ID, true) >> operation

    printerTemplateService.getPrinterTemplate("GE","template") >> new PrinterTemplateVo(
            "template",
            [
                    new PrinterConfiguration("Printer1", "printer 1", readPublicKeyFromClasspath("public-key-pa1.pub"), 1234),
                    new PrinterConfiguration("Printer2", "printer 2", readPublicKeyFromClasspath("public-key-pa2.pub"), 5678),
            ] as PrinterConfiguration[],
            [
                    new PrinterMunicipalityMapping(1, "city 1", "Printer1")
            ] as PrinterMunicipalityMapping[], "Printer2",
            new PrinterSwissAbroadMapping("Printer2")
    )

    VoterTestingCardsLot lot1 = VoterTestingCardsLotDataset.voterTestingCardsLot(CardType.TEST_SITE_TESTING_CARD)
    lot1.setLotName("lot 1")
    lot1.setShouldPrint(false)
    Map<VoterTestingCardsLot, Map<String, String>> testingCards = new HashMap<>()
    testingCards.put(lot1, ["boh printer contrôleur": "<voterDelivery>whatever</voterDelivery>"])
    testingCardRegisterGeneratorService.generateForOperation(OPERATION_ID, false) >> testingCards

    printerTemplateService.getVirtualPrinterForTestCard() >> new PrinterConfiguration("vp", "vp", readPublicKeyFromClasspath("public-key-pa1.pub"), 9012)

    when: 'upload voting material to pact'
    votingMaterialHandler.upload(OPERATION_ID)

    then:
    1 * httpClient.execute({ httpPost = it }, _)
    httpPost.method == "POST"
    httpPost.getURI().toString() == "votingMaterialUploadUrl/100000"
    ByteArrayOutputStream stream = new ByteArrayOutputStream()
    httpPost.entity.writeTo(stream)
    def parts = parseMultiPartRequest(stream.toString("UTF-8"))
    parts.size() == 4
    parts[3].headers["Content-Disposition"] == 'form-data; name=\"configuration\"'
    parts[3].headers["Content-Type"] == 'application/json; charset=UTF-8'
    JSONAssert.assertEquals(getResourceString(getClass(), "votingMaterialrequestJson.json"), parts[3].content, JSONCompareMode.NON_EXTENSIBLE)
    parts[0].headers["Content-Disposition"] == 'form-data; name="data"; filename="REGISTER.1.zip"'
    parts[0].headers["Content-Type"] == 'application/octet-stream'
    parts[1].headers["Content-Disposition"] == 'form-data; name="data"; filename="REGISTER.2.zip"'
    parts[1].headers["Content-Type"] == 'application/octet-stream'
    parts[2].headers["Content-Disposition"] == 'form-data; name="data"; filename="cards-test-not-printable_EL042050_boh-printer-contrôleur_lot-1_1970-01-01-00h00m00s.zip"'
    parts[2].headers["Content-Type"] == 'application/octet-stream'
  }


}
