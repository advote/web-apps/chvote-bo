/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.status.complete

import ch.ge.ve.bo.repository.SimulationPeriodConfigurationRepository
import ch.ge.ve.bo.repository.dataset.OperationDataset
import ch.ge.ve.bo.repository.entity.DeploymentTarget
import ch.ge.ve.bo.repository.entity.SimulationPeriodConfiguration
import ch.ge.ve.bo.service.TestHelpers
import spock.lang.Specification

class PeriodConfigurationCompleteTest extends Specification {
  def repository = Mock(SimulationPeriodConfigurationRepository)
  def service = new PeriodConfigurationComplete(repository, TestHelpers.passThroughSecuredOperationHolderService())


  def "IsSectionComplete is always true for real operation"() {
    given:
    def operation = OperationDataset.electoralOperation()
    operation.setDeploymentTarget(DeploymentTarget.REAL)

    expect:
    service.isSectionComplete(operation)
  }


  def "IsSectionComplete is false when a simulation has no period defined"() {
    given:
    def operation = OperationDataset.electoralOperation()
    operation.setDeploymentTarget(DeploymentTarget.SIMULATION)
    repository.findByOperationId(operation.id) >> Optional.empty()

    expect:
    !service.isSectionComplete(operation)
  }

  def "IsSectionComplete is true when  a simulation has period defined"() {
    given:
    def operation = OperationDataset.electoralOperation()
    operation.setDeploymentTarget(DeploymentTarget.SIMULATION)
    repository.findByOperationId(operation.id) >> Optional.of(new SimulationPeriodConfiguration())

    expect:
    service.isSectionComplete(operation)
  }

  def "sectionName is period-configuration"() {
    expect:
    service.sectionName == "period-configuration"
  }
}
