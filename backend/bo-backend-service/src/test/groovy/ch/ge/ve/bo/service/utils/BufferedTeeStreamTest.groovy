/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.utils

import ch.ge.ve.bo.service.utils.BufferedTeeStream
import java.util.concurrent.CompletableFuture
import spock.lang.Specification

class BufferedTeeStreamTest extends Specification {


    def "CreateInputStream"() {

        given: "a buffered tee stream"
        def toStream = "1-2-3-4-5-6-7-8-9"
        BufferedTeeStream bufferedTeeStream = new BufferedTeeStream(
                new ByteArrayInputStream(toStream.getBytes()), 2)

        and: "different readers at different speed"
        def r1 = new TestReader(timeToReadOne: 1, inputStream: bufferedTeeStream.createInputStream())
        def r2 = new TestReader(timeToReadOne: 10, inputStream: bufferedTeeStream.createInputStream())
        def r3 = new TestReader(timeToReadOne: 15, chunkSize: 2, inputStream: bufferedTeeStream.createInputStream())

        when: "running readers"
        [r1, r2, r3].forEach { new Thread(it).start() }

        then: "it should all returns same values"
        r1.result.get() == toStream
        r2.result.get() == toStream
        r3.result.get() == toStream

    }

    class TestReader implements Runnable {
        long timeToReadOne
        InputStream inputStream
        int chunkSize = 1
        CompletableFuture<String> result = new CompletableFuture<>()

        @Override
        void run() {
            ByteArrayOutputStream bos = new ByteArrayOutputStream()
            int nRead
            byte[] data = new byte[chunkSize]
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                bos.write(data, 0, nRead)
                Thread.sleep(timeToReadOne)

            }
            result.complete(new String(bos.toByteArray()))
        }
    }
}
