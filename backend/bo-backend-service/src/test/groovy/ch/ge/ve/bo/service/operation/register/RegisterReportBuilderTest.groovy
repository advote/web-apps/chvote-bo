/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.bo.service.operation.register

import static ch.ge.ve.bo.service.TestHelpers.getResourceString
import static ch.ge.ve.bo.service.operation.register.RegisterReportBuilder.SupportedFormat.CSV
import static ch.ge.ve.bo.service.operation.register.RegisterReportBuilder.SupportedFormat.PDF

import java.nio.charset.Charset
import org.apache.commons.io.IOUtils
import spock.lang.Specification
import spock.lang.Unroll

class RegisterReportBuilderTest extends Specification {
    def builder = new RegisterReportBuilder()

    def "BuildReport in PDF"() {
        given: "a register report data"
        def report = getResourceString(RegisterReportBuilderTest, "register-report.json", true)

        when: "converted to pdf"
        def pdf = builder.buildReport(report, "operation under test", PDF)
        File file = File.createTempFile("test-report", ".pdf")
        println("Test report stored on " + file)
        IOUtils.write(pdf, new FileOutputStream(file))

        then: "a pdf is built without error"
        notThrown(Throwable)
    }

    @Unroll
    def "BuildReport with #file"() {
        given: "a register report data"
        def report = getResourceString(RegisterReportBuilderTest, file + ".json", true)

        when: "converted to csv"
        def csv = new String(builder.buildReport(report, "operation under test", CSV), Charset.forName("UTF-8"))

        then: "csv file is correct"
        csv.trim() == getResourceString(RegisterReportBuilderTest, file + ".csv", true).trim()

        where:
        _ | file
        _ | "register-report"
        _ | "register-report-no-foreigner"
        _ | "register-report-no-swiss"
        _ | "register-report-no-swiss-abroad"
        _ | "register-report-no-foreigner-no-swiss"

    }


}
