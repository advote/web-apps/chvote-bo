/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts
let path = require('path');
let prepareReporter = require("./e2e/pmsr/protractor-multi-screenshots-reporter").prepareReporter;
let completeReport = require("./e2e/pmsr/protractor-multi-screenshots-reporter").completeReport;

let chromeOptions = {
  'args': ['no-sandbox', 'headless', 'disable-gpu', "window-size=1400,1500"],
  //'args': ['no-sandbox', 'disable-gpu', "window-size=1400,1500"],
  // Set download path and avoid prompting for download
  prefs: {
    'download': {
      'prompt_for_download': false,
      'directory_upgrade': true,
      'default_directory': path.join(__dirname, '/test_reports/protractor')
    },
    'safebrowsing': {
      'enabled': true
    }
  }
};

exports.config = {
  allScriptsTimeout: 44000,
  suites: {
    'Admin/Electoral authority key': './e2e/00-admin/01-electoral-authority-key/*.e2e-spec.ts',
    'Home': './e2e/01-home/*.e2e-spec.ts',
    'Dashboard': './e2e/02-dashboard/*.e2e-spec.ts',
    'Operation/Parameters/Base': './e2e/03-operation/01-parameters/01-base/*.e2e-spec.ts',
    'Operation/Parameters/Milestone': './e2e/03-operation/01-parameters/02-milestone/*.e2e-spec.ts',
    'Operation/Parameters/Domain of influence': './e2e/03-operation/01-parameters/03-domain-influence/*.e2e-spec.ts',
    'Operation/Parameters/Repository': './e2e/03-operation/01-parameters/04-repository/*.e2e-spec.ts',
    'Operation/Parameters/Documentation': './e2e/03-operation/01-parameters/05-documentation/*.e2e-spec.ts',
    'Operation/Parameters/Testing cards': './e2e/03-operation/01-parameters/06-testing-cards/*.e2e-spec.ts',
    'Operation/Parameters/Management Entity': './e2e/03-operation/01-parameters/07-management-entity/*.e2e-spec.ts',
    'Operation/Parameters/Ballot Documentation': './e2e/03-operation/01-parameters/08-ballot-documentation/*.e2e-spec.ts',
    'Operation/Parameters/Election Page Properties': './e2e/03-operation/01-parameters/09-election-page-properties/*.e2e-spec.ts',
    'Operation/Parameters/Workflow': './e2e/03-operation/01-parameters/10-workflow/*.e2e-spec.ts',
    'Operation/Parameters/Workflow/modification': './e2e/03-operation/01-parameters/11-modification/*.e2e-spec.ts',
    'Operation/Parameters/Voting Site Configuration': './e2e/03-operation/01-parameters/12-voting-site-configuration/*.e2e-spec.ts',
    'Operation/Voting material/Register': './e2e/03-operation/02-voting-material/01-register/01-register.e2e-spec.ts',
    'Operation/Voting material/Register/Concurrency': './e2e/03-operation/02-voting-material/01-register/02-register-concurrency.e2e-spec.ts',
    'Operation/Voting material/Card Title': './e2e/03-operation/02-voting-material/02-card-title/*.e2e-spec.ts',
    'Operation/Voting material/Printer Template': './e2e/03-operation/02-voting-material/03-printer-template/*.e2e-spec.ts',
    'Operation/Voting material/Testing cards': './e2e/03-operation/02-voting-material/04-testing-cards/*.e2e-spec.ts',
    'Operation/Voting material/Workflow': './e2e/03-operation/02-voting-material/05-workflow/*.e2e-spec.ts',
    'Operation/Voting period/Electoral Authority Key': './e2e/03-operation/03-voting-period/01-electoral-authority-key/*.e2e-spec.ts',
    'Operation/Voting period/Voting Site Period': './e2e/03-operation/03-voting-period/02-voting-site-period/*.e2e-spec.ts',
    'Operation/Voting period/Workflow': './e2e/03-operation/03-voting-period/03-workflow/*.e2e-spec.ts',
    'Operation/tally': './e2e/03-operation/04-tally/*.e2e-spec.ts',
    'Right Management/Dashboard': './e2e/04-right-management/01-dashboard-restriction.e2e-spec.ts',
    'Right Management/Creation operation': './e2e/04-right-management/02-operation-creation-restriction.e2e-spec.ts',
    'Right Management/Base parameter': './e2e/04-right-management/03-base-parameter-restriction.e2e-spec.ts',
    'Right Management/Milestone': './e2e/04-right-management/04-milestone-restriction.e2e-spec.ts',
    'Right Management/Domain of Influence': './e2e/04-right-management/05-doi-restriction.e2e-spec.ts',
    'Right Management/Repository': './e2e/04-right-management/06-repository-restriction.e2e-spec.ts',
    'Right Management/Documentation': './e2e/04-right-management/07-document-restriction.e2e-spec.ts',
    'Right Management/Testing cards': './e2e/04-right-management/08-testing-card-lot-restriction.e2e-spec.ts',
    'Right Management/Register': './e2e/04-right-management/09-register-restriction.e2e-spec.ts',
    'Right Management/Voting card title': './e2e/04-right-management/10-voting-card-title-restriction.e2e-spec.ts',
    'Right Management/Printer template': './e2e/04-right-management/11-printer-restriction.e2e-spec.ts',
    'Right Management/Operation management': './e2e/04-right-management/12-operation-management-restriction.e2e-spec.ts'
  },
  exclude: ['**/*.po.js'], // exclude page objects

  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': chromeOptions
  },

  params: {
    mockServer: {
      hostname: "127.0.0.1",
      port: 9754
    }
  },
  directConnect: true,
  baseUrl: 'http://localhost:4300/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {
    }
  },
  beforeLaunch: function () {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
  },
  onPrepare() {
    prepareReporter();
  },

  onComplete: function () {
    browser.getCapabilities().then(function (caps) {
      let browserName = caps.get('browserName');
      let browserVersion = caps.get('version');
      completeReport(browserName, browserVersion);
    });
  }

};
