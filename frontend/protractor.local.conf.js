/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

//
// This is the protractor configuration file for local environment
// One should only change
// global.disableScreenshots : Define if screenshots are disabled hence reducing time taken to pass the suite
// specFilter : Define conditions to take a spec or not in the run (See below)
//

global.disableScreenshots = false;

// noinspection JSUnusedLocalSymbols
const ALL = (k) => true;
// noinspection JSUnusedLocalSymbols
const keyContains = (expectedContent) => (k) => k.indexOf(expectedContent) >= 0;
// noinspection JSUnusedLocalSymbols
const or = (f1, f2, f3, f4, f5, f6, f7) => (x) => f1(x) ||
  (f2 && f2(x)) ||
  (f3 && f3(x)) ||
  (f4 && f4(x)) ||
  (f5 && f5(x)) ||
  (f6 && f6(x)) ||
  (f7 && f7(x));


//
// specFilter permits to select which suite(s) you want to run
// To get all suites

const specFilter = ALL;


//
//
// to take only suites containing management
// const specFilter = keyContains("management")
//
// to take all suites containing management and all suites containing workflow
//const specFilter = or(keyContains("management"), keyContains("workflow"))
//
//
//const specFilter = or(keyContains("Operation/Parameters/Repository") );

// -------------------------------------------
// Should not change afterward
// -------------------------------------------

let filterConfig = (suites, keyFilter) => {
  let filteredSuites = {};
  Object.keys(suites)
    .filter(keyFilter)
    .forEach(k => filteredSuites[k] = suites[k]);
  return filteredSuites;
};

let config = require("./protractor.conf").config;
config.suites = filterConfig(config.suites, specFilter);
console.log("Will run suites " + Object.keys(config.suites).map(suite => "\n\t -" + suite).join());
console.log("Will take screenshot: " + !global.disableScreenshots);
exports.config = config;


