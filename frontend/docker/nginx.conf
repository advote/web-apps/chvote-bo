# based on http://brainspl.at/nginx.conf.txt

worker_processes  auto;

# error_log  "/log/error.log";

error_log /dev/stdout info;
pid        "/opt/bitnami/nginx/tmp/nginx.pid";

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;

    add_header X-Frame-Options SAMEORIGIN;
    client_body_temp_path  "/opt/bitnami/nginx/tmp/client_body" 1 2;
    proxy_temp_path "/opt/bitnami/nginx/tmp/proxy" 1 2;
    fastcgi_temp_path "/opt/bitnami/nginx/tmp/fastcgi" 1 2;
    scgi_temp_path "/opt/bitnami/nginx/tmp/scgi" 1 2;
    uwsgi_temp_path "/opt/bitnami/nginx/tmp/uwsgi" 1 2;

    log_format main '$remote_addr - $remote_user [$time_local] '
                    '"$request" $status  $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

    # access_log  "/log/access.log";
    access_log /dev/stdout;

    # no sendfile on OSX
    sendfile        on;

    tcp_nopush     on;
    tcp_nodelay       off;

    #keepalive_timeout  0;
    keepalive_timeout  65;
    gzip on;
    gzip_http_version 1.0;
    gzip_comp_level 2;
    gzip_proxied any;
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

  # HTTP Server
  server {
    # port to listen on. Can also be set to an IP:PORT
    listen 8080;
    server_name  localhost;

    location / {
      root   /app;
      index  index.html index.htm;
      try_files $uri $uri/ /index.html;
    }

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
      root   /app;
    }

    location /status {
      stub_status on;
      access_log   off;
      allow 127.0.0.1;
      deny all;
    }
  }
}
