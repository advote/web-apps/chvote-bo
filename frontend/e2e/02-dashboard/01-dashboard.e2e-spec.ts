/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { DashboardPage } from '../page-object/dashboard/dashboard.po';
import { OperationListSection } from '../page-object/dashboard/operation-list-section.po';
import { Main } from '../page-object/main.po';


describe('Dashboard', () => {

  beforeAll(() => {
    Main.login();
    Main.navigateTo('dashboard');
  });

  it('header title should be "Tableau de bord des opérations de vote"', () => {
    expect(DashboardPage.headerTitle).toBe('Tableau de bord des opérations de vote');
  });

  it('operation list section should be present', () => {
    expect(OperationListSection.element.displayed).toBeTruthy();
  });

  it('operation list grid should display 4 columns', () => {
    expect(OperationListSection.operationsGrid.headerValue('date')).toBe('Date');
    expect(OperationListSection.operationsGrid.headerValue('shortLabel')).toBe('Libellé court');
    expect(OperationListSection.operationsGrid.headerValue('longLabel')).toBe('Libellé long');
    expect(OperationListSection.operationsGrid.headerValue('management')).toBe('Pilotage');
  });
});
