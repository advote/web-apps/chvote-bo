/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../../page-object/main.po';
import { OperationOption, tally, votingPeriod } from '../../shared/mock-server';
import { DeploymentSummarySection, OperationManagementPage } from '../../page-object/operation/operation-management.po';
import { browser } from 'protractor';
import { VotingSitePeriodPage } from '../../page-object/operation/voting-period/voting-site-period.po';
import { TallyPage } from '../../page-object/operation/tally/tally.po';

describe('Operation/Tally/Workflow/simulation', () => {
  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-period",
      'test-voting-period-workflow',
      'TEST_e2e_voting_period_workflow',
      [
        OperationOption.ADD_ALL,
        OperationOption.FOR_SIMULATION,
        OperationOption.WITH_VOTING_MATERIAL_FINALIZED,
      ]
    );

    browser.controlFlow().execute(() => {
      votingPeriod.setVotingPeriodToInitialized(Main.operationId);
    });
    DeploymentSummarySection.waitForProductionStatusMessageToMatch(
      /Initialisation de la période de vote validée le.*/);
  });

  describe('Creation in progress', () => {

    it('Should allow user to close voting period', () => {
      VotingSitePeriodPage.closeVotingPeriodButton.click();
      DeploymentSummarySection
        .waitForProductionStatusMessageToMatch(/L'archive de dépouillement est en cours de création/)
    });

    it('Should navigate to tally section', () => {
      expect(OperationManagementPage.isNavigationTabDisplayed(3)).toBeTruthy();
    });

    it('Should display a message pointing out that the archive creation is in progress', () => {
      expect(TallyPage.tallyArchiveInProgressElement.isPresent()).toBeTruthy()
    })

  });


  describe('Creation failed', () => {

    it(
      'should have status "La création de l\'archive de dépouillement a échoué" when the tally archive creation is in progress',
      () => {
        tally.setTallyArchiveCreationToStatus(Main.operationId, 'CREATION_FAILED');
        DeploymentSummarySection
          .waitForProductionStatusMessageToMatch(/La création de l'archive de dépouillement a échoué/)
      });

    it('Should display a message pointing out that the archive creation has failed', () => {
      expect(TallyPage.tallyArchiveFailedElement.isPresent()).toBeTruthy()
    });

    it('Should allow user to relaunch archive creation', () => {
      TallyPage.tallyArchiveRetryButton.click();
      DeploymentSummarySection
        .waitForProductionStatusMessageToMatch(/L'archive de dépouillement est en cours de création/);
      expect(TallyPage.tallyArchiveInProgressElement.isPresent()).toBeTruthy();
      expect(TallyPage.tallyArchiveFailedElement.isPresent()).toBeFalsy()
    });

  });


  describe('Created', () => {

    it(
      'should have status "L\'archive de dépouillement est disponible" when the tally archive is created',
      () => {
        tally.setTallyArchiveCreationToStatus(Main.operationId, 'CREATED');
        DeploymentSummarySection
          .waitForProductionStatusMessageToMatch(/L'archive de dépouillement est disponible/)
      });

    it('Should display a message pointing out that the archive creation is available', () => {
      expect(TallyPage.tallyArchiveCreatedElement.isPresent()).toBeTruthy()
    });

    it('Should allow user to download archive', () => {
      expect(TallyPage.tallyArchiveDownloadButton.displayed).toBeTruthy()
    });

    it('Should deny access to user with wrong access right', ()=>{
      browser.getCurrentUrl().then(url=>{
        Main.login("ge4");
        return browser.get(url);
      });

      expect(TallyPage.tallyArchiveDownloadButton.displayed).toBeFalsy();
      expect(TallyPage.isDownloadArchiveDenied).toBeTruthy()
    })
  });


  describe('NotEnoughVotes', () => {

    it(
      'should have status "Aucune archive de dépouillement disponible" when not enough votes for creation of archive',
      () => {
        tally.setTallyArchiveCreationToStatus(Main.operationId, 'NOT_ENOUGH_VOTES_CAST');
        DeploymentSummarySection
          .waitForProductionStatusMessageToMatch(/Aucune archive de dépouillement disponible/)
      });

    it('Should display a message pointing out that not enough votes for creation of archive', () => {
      expect(TallyPage.tallyArchiveNotEnoughtVotes.isPresent()).toBeTruthy()
    });

  });


});
