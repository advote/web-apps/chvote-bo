/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from "../../../shared/mock-server";
import { Main } from '../../../page-object/main.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { VotingPeriodEditPage } from '../../../page-object/operation/voting-period/voting-period-edit.po';
import { VotingSitePeriodPage } from '../../../page-object/operation/voting-period/voting-site-period.po';
import { waitForVisibility } from '../../../shared/e2e.utils';


describe('Operation/Voting period/Voting Site Period', () => {
  describe('Real', () => {
    beforeAll(() => {
      Main.login();
      Main.createOperationAndNavigate(
        "operations/[OPERATION_ID]/voting-period",
        'test_voting_period_workflow',
        'TEST_e2e_voting_period_workflow',
        [
          OperationOption.ADD_MILESTONE,
          OperationOption.ADD_DOMAIN_INFLUENCE,
          OperationOption.ADD_REPOSITORY_VOTATION,
          OperationOption.ADD_REPOSITORY_ELECTION,
          OperationOption.ADD_DOCUMENT,
          OperationOption.ADD_TESTING_CARDS,
          OperationOption.ADD_REGISTER_SE,
          OperationOption.ADD_REGISTER_SR,
          OperationOption.ADD_CARD_TITLE,
          OperationOption.ADD_PRINTER_TEMPLATE,
          OperationOption.FOR_REAL,
          OperationOption.WITH_VOTING_MATERIAL_FINALIZED,
        ]
      );
    });


    it('Card should be in readonly', () => {
      Main.login('ge4');
      Main.navigateTo('operations/[OPERATION_ID]/voting-period');
      waitForVisibility(OperationManagementPage.votingSitePeriodCard.element);
      expect(OperationManagementPage.votingSitePeriodCard.readOnly).toBeTruthy();
    });

    it('Content of the card should be set with milestone data', () => {
      expect(OperationManagementPage.votingSitePeriodCard.getContentValue(0)).toEqual("05.06.2057 12:01");
      expect(OperationManagementPage.votingSitePeriodCard.getContentValue(1)).toEqual("06.06.2057 09:00");
      expect(OperationManagementPage.votingSitePeriodCard.getContentValue(2)).toEqual("9");
    });


    it('fields should be disabled', () => {
      OperationManagementPage.votingSitePeriodCard.button.click();
      expect(VotingSitePeriodPage.dateOpenField.enabled).toBeFalsy();
      expect(VotingSitePeriodPage.dateCloseField.enabled).toBeFalsy();
      expect(VotingSitePeriodPage.gracePeriodField.enabled).toBeFalsy();
    });

    it('Should be no save button', () => {
      expect(VotingSitePeriodPage.saveButton.displayed).toBeFalsy();
    });


  });


  describe('Simulation', () => {

    beforeAll(() => {
      Main.login();
      Main.createOperationAndNavigate(
        "operations/[OPERATION_ID]/voting-period",
        'test_voting_period_workflow',
        'TEST_e2e_voting_period_workflow',
        [
          OperationOption.ADD_MILESTONE,
          OperationOption.ADD_DOMAIN_INFLUENCE,
          OperationOption.ADD_REPOSITORY_VOTATION,
          OperationOption.ADD_REPOSITORY_ELECTION,
          OperationOption.ADD_DOCUMENT,
          OperationOption.ADD_TESTING_CARDS,
          OperationOption.ADD_REGISTER_SE,
          OperationOption.ADD_REGISTER_SR,
          OperationOption.ADD_CARD_TITLE,
          OperationOption.ADD_PRINTER_TEMPLATE,
          OperationOption.FOR_SIMULATION,
          OperationOption.WITH_VOTING_MATERIAL_FINALIZED,
        ]
      );
    });

    describe('Creation', () => {

      it('card should not be in readonly', () => {
        expect(OperationManagementPage.votingSitePeriodCard.readOnly).toBeFalsy();
      });

      it('card should not be completed', () => {
        expect(OperationManagementPage.votingSitePeriodCard.completeBadgeDisplayed).toBeFalsy();
      });

      it('[CONFIGURE] button should open edit page', () => {
        OperationManagementPage.votingSitePeriodCard.button.click();
        expect(VotingSitePeriodPage.root.displayed).toBeTruthy();
      });

      it('Should not allow user to save without filling the form', () => {
        VotingSitePeriodPage.saveButton.click();
        expect(VotingSitePeriodPage.dateCloseField.error).toBe("Champ obligatoire");
        expect(VotingSitePeriodPage.gracePeriodField.error).toBe("Champ obligatoire");
      });


      it('Should not allow user to save with a date in the past', () => {
        VotingSitePeriodPage.dateOpenField.value = '01.01.2000 11:00';
        VotingSitePeriodPage.dateCloseField.value = '01.01.2050 11:00';
        VotingSitePeriodPage.gracePeriodField.value = '10';
        VotingSitePeriodPage.saveButton.click();
        expect(VotingSitePeriodPage.dateOpenField.error).toBe("Sélectionner une date ultérieure");
      });

      it('Should not allow user to save with open date after close date', () => {
        VotingSitePeriodPage.dateOpenField.value = '01.01.2060 11:00';
        VotingSitePeriodPage.saveButton.click();
        expect(VotingSitePeriodPage.dateOpenField.error).toBe("Sélectionner une date antérieure");
        expect(VotingSitePeriodPage.dateCloseField.error).toBe("Sélectionner une date ultérieure");
      });

      it('Should allow user to save having selected correct dates', () => {
        VotingSitePeriodPage.dateOpenField.value = '01.01.2049 11:00';
        VotingSitePeriodPage.saveButton.click(true);
      });


      it('card should be completed', () => {
        expect(OperationManagementPage.votingSitePeriodCard.completeBadgeDisplayed).toBeTruthy();
      });

      it('Content of the card should be updated accordingly', () => {
        expect(OperationManagementPage.votingSitePeriodCard.getContentValue(0)).toEqual("01.01.2049 11:00");
        expect(OperationManagementPage.votingSitePeriodCard.getContentValue(1)).toEqual("01.01.2050 11:00");
        expect(OperationManagementPage.votingSitePeriodCard.getContentValue(2)).toEqual("10");
      });

    });

    describe('Modification', () => {

      it('Should allow user to save having selected a different period', () => {
        OperationManagementPage.votingSitePeriodCard.button.click();
        VotingSitePeriodPage.dateOpenField.value = '01.01.2048 11:00';
        VotingSitePeriodPage.saveButton.click(true);
      });

      it('Content of the card should be updated accordingly', () => {
        expect(OperationManagementPage.votingSitePeriodCard.getContentValue(0)).toEqual("01.01.2048 11:00");
      });
    });

    describe('form modification detection', () => {
      it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
        OperationManagementPage.votingSitePeriodCard.button.click();
        VotingPeriodEditPage.clickBackLink();
        expect(OperationManagementPage.votingSitePeriodCard.present).toBeTruthy();
      });

      it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
        OperationManagementPage.votingSitePeriodCard.button.click();
        VotingSitePeriodPage.dateOpenField.value = '01.01.2049 11:00';
        VotingPeriodEditPage.clickBackLink(false);
        expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
        Main.confirmDialog.cancel();
        expect(OperationManagementPage.votingSitePeriodCard.present).toBeFalsy();
      });

      it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
        VotingPeriodEditPage.clickBackLink(false);
        Main.confirmDialog.accept();
        expect(OperationManagementPage.votingSitePeriodCard.present).toBeTruthy();
      });
    });

    describe('Right management', () => {
      it('Card should be in readonly for user with no appropriate right', () => {
        Main.login('ge4');
        Main.navigateTo('operations/[OPERATION_ID]/voting-period');
        waitForVisibility(OperationManagementPage.votingSitePeriodCard.element)
        expect(OperationManagementPage.votingSitePeriodCard.readOnly).toBeTruthy();
      });

      it('fields should be disabled for user with no appropriate right', () => {
        OperationManagementPage.votingSitePeriodCard.button.click();

        expect(VotingSitePeriodPage.dateOpenField.enabled).toBeFalsy();
        expect(VotingSitePeriodPage.dateCloseField.enabled).toBeFalsy();
        expect(VotingSitePeriodPage.gracePeriodField.enabled).toBeFalsy();
      });

      it('Should be no save button', () => {
        expect(VotingSitePeriodPage.saveButton.displayed).toBeFalsy();
      });

    });


  });


});
