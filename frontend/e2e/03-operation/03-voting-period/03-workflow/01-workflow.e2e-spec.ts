/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption, votingPeriod } from "../../../shared/mock-server";
import { Main } from '../../../page-object/main.po';
import {
  DeploymentSummarySection, OperationManagementPage, VotingPeriodNotificationSection
} from '../../../page-object/operation/operation-management.po';
import { SelectedElectoralAuthorityKeyPage } from '../../../page-object/operation/voting-period/selected-electoral-authority-key.po';
import { browser } from 'protractor';
import { deepEqual } from '../../../shared/e2e.utils';
import { VotingSitePeriodPage } from '../../../page-object/operation/voting-period/voting-site-period.po';

describe('Operation/Voting period/Workflow', () => {
  const EXPECTED_JSON = {
    "user": "ge1",
    "electoralAuthorityKey": {
      "zipFileName": "key1",
      "attachmentType": "ELECTION_OFFICER_KEY",
      "importDateTime": "JSDate[02.06.2057 10:00:00.000]",
      "externalIdentifier": "___"
    },
    "target": "SIMULATION",
    "simulationName": "Test Simulation",
    "siteOpeningDate": "JSDate[04.06.2010 10:10:00.000]",
    "siteClosingDate": "JSDate[04.06.2057 11:20:00.000]",
    "gracePeriod": 10
  };

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-period",
      'test-voting-period-workflow',
      'TEST_e2e_voting_period_workflow',
      [
        OperationOption.ADD_ALL,
        OperationOption.FOR_SIMULATION,
        OperationOption.WITH_VOTING_MATERIAL_FINALIZED,
      ]
    );
  });

  function expectCardsAreInReadonly(isReadOnly: boolean): void {
    expect(OperationManagementPage.votingSitePeriodCard.readOnly).toBe(isReadOnly);
    expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.readOnly).toBe(isReadOnly);
  }

  describe('complete', () => {
    it('should display "Paramétrage de la période de vote défini le..."', () => {
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(
        /Paramétrage de la période de vote défini le.*/);
    });

    it('should not be in read-only mode', () => {
      expectCardsAreInReadonly(false);
    });

    it('should allow user to send voting material', () => {
      expect(VotingPeriodNotificationSection.deploymentButton.enabled).toBeTruthy();
    });
  });


  describe('send voting period configuration to PACT', () => {
    it('should have status "Paramétrage de la période de vote envoyé le..."', () => {
      VotingPeriodNotificationSection.deploymentButton.click();
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Paramétrage de la période de vote envoyé le .*/)
    });

    it("Should have an expected request sent to PACT", () => {
      let actualJson;
      votingPeriod.getLastRequestJson(json => {
        actualJson = JSON.parse(json as string);
      });

      browser.controlFlow().execute(() => deepEqual(EXPECTED_JSON, actualJson, ["electoralAuthorityKey.externalIdentifier"]));

    });


    it('should hide deployment button and show a link to PACT in order to request voting period initialisation', () => {
      expect(VotingPeriodNotificationSection.deploymentButton.present).toBeFalsy();
      expect(VotingPeriodNotificationSection.createRequestLink.isPresent()).toBeTruthy();
    });

    it('should switch to read-only mode', () => {
      expectCardsAreInReadonly(true);
    });
  });

  describe('user request voting period initialization in PACT', () => {
    it('should have status "Initialisation de la période de vote demandée..." when a user request a creation', () => {
      votingPeriod.requestVotingPeriodInitialization(Main.operationId);
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Initialisation de la période de vote demandée.*/)
    });

    it('should show a link to PACT in order to validate the voting period initialization', () => {
      expect(VotingPeriodNotificationSection.validateRequestLink.isDisplayed()).toBeTruthy();
    });

    it('should stay in read-only mode', () => {
      expectCardsAreInReadonly(true);
    });
  });

  describe('user refuse voting period initialization in PACT', () => {
    it(
      'should have status "Initialisation de la période de vote refusée" when a user in PACT has refused voting material deployment',
      () => {
        votingPeriod.rejectVotingPeriodInitialization(Main.operationId);
        DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Initialisation de la période de vote refusée.*/)
      });

    it('should switch to read-write mode', () => {
      expectCardsAreInReadonly(false);
    });

    it('cannot redeploy meanwhile no change has been done', () => {
      expect(VotingPeriodNotificationSection.deploymentButton.present).toBeFalsy();
    });

    it('After a change status should be "Paramétrage de la période de vote défini"', () => {
      OperationManagementPage.selectedElectoralAuthorityKeyCard.button.click();
      SelectedElectoralAuthorityKeyPage.keySelectField.select("key2");
      SelectedElectoralAuthorityKeyPage.saveButton.click();
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(
        /Paramétrage de la période de vote défini le.*/);
    });

    it('should allow user to redeploy', () => {
      VotingPeriodNotificationSection.deploymentButton.click(true);
    });

  });

  describe('voting period initialization failure', () => {
    it(
      'should have status "Échec de l\'initialisation de la période de vote" when a initialization of voting period has failed',
      () => {
        votingPeriod.setVotingPeriodInitializationToFailed(Main.operationId);
        browser.waitForAngular();
        DeploymentSummarySection.waitForProductionStatusMessageToMatch(
          /Échec de l'initialisation de la période de vote.*/);
      });
    it('should switch to read-write mode', () => {
      expectCardsAreInReadonly(false);
    });
    it('should allow redeployment', () => {
      VotingPeriodNotificationSection.deploymentButton.click();
    });
  });

  describe('voting period initialization in progress', () => {
    it(
      'should have status "Période de vote en cours d\'initialisation" when a voting period initialization is in progress',
      () => {
        votingPeriod.setVotingPeriodInitializationToInProgress(Main.operationId);
        DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Période de vote en cours d'initialisation*/);
        expectCardsAreInReadonly(true);
      });

    it('should be to read-only mode', () => {
      expectCardsAreInReadonly(true);
    });
  });

  describe('voting period initialized', () => {
    it(
      'should have status "Initialisation de la période de vote validée le " when the voting period has successfully initialized',
      () => {
        votingPeriod.setVotingPeriodToInitialized(Main.operationId);
        DeploymentSummarySection.waitForProductionStatusMessageToMatch(
          /Initialisation de la période de vote validée le.*/);
        expectCardsAreInReadonly(true);
      });

    it('should be to read-only mode', () => {
      expectCardsAreInReadonly(true);
    });

    it('Should display a button to close the voting site', () => {
      expect(VotingSitePeriodPage.closeVotingPeriodButton.displayed).toBeTruthy()
    })

  });

});
