/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from "../../../shared/mock-server";
import { Main } from '../../../page-object/main.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { SelectedElectoralAuthorityKeyPage } from '../../../page-object/operation/voting-period/selected-electoral-authority-key.po';
import { VotingPeriodEditPage } from '../../../page-object/operation/voting-period/voting-period-edit.po';

describe('Operation/Voting period/Electoral Authority Key', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-period",
      'test_voting_period_workflow',
      'TEST_e2e_voting_period_workflow',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_REGISTER_SE,
        OperationOption.ADD_REGISTER_SR,
        OperationOption.ADD_CARD_TITLE,
        OperationOption.ADD_PRINTER_TEMPLATE,
        OperationOption.FOR_SIMULATION,
        OperationOption.WITH_VOTING_MATERIAL_FINALIZED,
      ]
    );
  });

  describe('Creation', () => {


    it('card should not be in readonly', () => {
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.readOnly).toBeFalsy();
    });


    it('card should not be completed', () => {
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.completeBadgeDisplayed).toBeFalsy();
    });

    it('[CONFIGURE] button should open edit page', () => {
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.completeBadgeDisplayed).toBeFalsy();
    });

    it('There should be a message on the fact that there is no electoral authorithy key configured', () => {
      OperationManagementPage.selectedElectoralAuthorityKeyCard.button.click();
      expect(SelectedElectoralAuthorityKeyPage.hasNoElectoralAuthorityKeys).toBeTruthy();
    });

    it('After inserting keys message should disappear', () => {
      Main.addElectoralAuthorityKeys();
      Main.refresh();
      expect(SelectedElectoralAuthorityKeyPage.hasNoElectoralAuthorityKeys).toBeFalsy();
    });

    it('Should not allow user to save without selecting a key', () => {
      SelectedElectoralAuthorityKeyPage.saveButton.click();
      expect(SelectedElectoralAuthorityKeyPage.keySelectField.error).toBe("Champ obligatoire");
    });

    it('After selecting a key should display information about the selected key', () => {
      SelectedElectoralAuthorityKeyPage.keySelectField.select("key1");
      expect(SelectedElectoralAuthorityKeyPage.keyLabel).toBe("key1");
    });

    it('Should allow user to save having selected a key', () => {
      SelectedElectoralAuthorityKeyPage.saveButton.click(true);
    });


    it('card should be completed', () => {
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.completeBadgeDisplayed).toBeTruthy();
    });

    it('Content of the card should be updated accordingly', () => {
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.getContentValue(0)).toEqual("key1");
    });

  });

  describe('Modification', () => {

    it('Should allow user to save having selected a different key', () => {
      OperationManagementPage.selectedElectoralAuthorityKeyCard.button.click();
      SelectedElectoralAuthorityKeyPage.keySelectField.select("key2");
      SelectedElectoralAuthorityKeyPage.saveButton.click(true);
    });

    it('Content of the card should be updated accordingly', () => {
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.getContentValue(0)).toEqual("key2");
    });
  });

  describe('form modification detection', () => {
    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.selectedElectoralAuthorityKeyCard.button.click();
      VotingPeriodEditPage.clickBackLink();
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.selectedElectoralAuthorityKeyCard.button.click();
      SelectedElectoralAuthorityKeyPage.keySelectField.select("key1");
      VotingPeriodEditPage.clickBackLink(false);
      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      VotingPeriodEditPage.clickBackLink(false);
      Main.confirmDialog.accept();
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.present).toBeTruthy();
    });
  });

  describe('Right management', () => {
    it('Card should be in readonly for user with no appropriate right', () => {
      Main.login('ge4');
      Main.navigateTo('operations/[OPERATION_ID]/voting-period');
      expect(OperationManagementPage.selectedElectoralAuthorityKeyCard.readOnly).toBeTruthy();
    });

    it('Selector should be disabled for user with no appropriate right', () => {
      OperationManagementPage.selectedElectoralAuthorityKeyCard.button.click();
      expect(SelectedElectoralAuthorityKeyPage.keyLabel).toBe("key2");
      expect(SelectedElectoralAuthorityKeyPage.keyLogin).toBe("mock-server");
      expect(SelectedElectoralAuthorityKeyPage.keyImportDate).toContain("02.06.2057");
    });

    it('Should be no save button', () => {
      expect(SelectedElectoralAuthorityKeyPage.saveButton.displayed).toBeFalsy();
    });

  });

});
