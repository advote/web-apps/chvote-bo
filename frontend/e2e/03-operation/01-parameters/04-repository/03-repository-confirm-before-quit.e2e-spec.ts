/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { ParameterEditPage } from "../../../page-object/operation/parameters/parameter-edit.po";
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { OperationOption } from '../../../shared/mock-server';
import { RepositoryEdit } from '../../../page-object/operation/parameters/repository-edit.po';
import { Main } from '../../../page-object/main.po';

describe('Operation/Parameters/Repository/form modification detection', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_repository',
      'TEST_e2e_repository',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION
      ]
    );
  });

  it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
    OperationManagementPage.repositoryCard.button.click();
    ParameterEditPage.clickBackLink();
    expect(OperationManagementPage.repositoryCard.present).toBeTruthy();
  });

  it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
    OperationManagementPage.repositoryCard.button.click();
    RepositoryEdit.uploadInput.file = '../resources/parameters/valid-repository-election.xml';
    ParameterEditPage.clickBackLink(false);

    expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
    Main.confirmDialog.cancel();
    expect(OperationManagementPage.repositoryCard.present).toBeFalsy();
  });

  it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
    ParameterEditPage.clickBackLink(false);
    Main.confirmDialog.accept();
    expect(OperationManagementPage.repositoryCard.present).toBeTruthy();
  });

});
