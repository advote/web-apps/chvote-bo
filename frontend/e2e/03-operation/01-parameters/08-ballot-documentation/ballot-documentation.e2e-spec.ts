/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from "../../../shared/mock-server";
import { Main } from '../../../page-object/main.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { BallotDocumentationEdit } from '../../../page-object/operation/parameters/ballot-documentation-edit.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { browser } from 'protractor';
import { waitForVisibility } from '../../../shared/e2e.utils';

let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Ballot Documentation', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_ballot_documentation',
      'TEST_e2e_ballot_documentation',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS
      ]
    );
  });

  describe('ballot documentation card before edit', () => {
    it('ballot documentation card should be displayed in the dashboard', () => {
      expect(OperationManagementPage.ballotDocumentationCard.displayed).toBeTruthy();
    });

    it('card should indicate there is no ballot documentation', () => {
      expect(OperationManagementPage.ballotDocumentationCard.getContentValue(0)).toBe('Aucune documentation');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.ballotDocumentationCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('ballot documentation edit section', () => {
    it('should display the ballot documentation section when clicking on the [CONFIGURE] button', () => {
      OperationManagementPage.ballotDocumentationCard.button.click();
      expect(BallotDocumentationEdit.root.displayed).toBeTruthy();
    });

    it('card title should be "Documentation des scrutins"', () => {
      expect(BallotDocumentationEdit.root.title).toBe('Documentation des scrutins');
    });

    it('card subtitle should be "Liste des documents des scrutins"', () => {
      expect(BallotDocumentationEdit.root.subtitle).toContain('Liste des documents des scrutins');
    });

    it('ballot select should have placeholder "Scrutin"', () => {
      expect(BallotDocumentationEdit.ballotSelect.placeholder).toBe('Scrutin *');
      since('input should be required').expect(BallotDocumentationEdit.ballotSelect.required).toBeTruthy();
    });

    it('language select should have placeholder "Langue"', () => {
      expect(BallotDocumentationEdit.languageSelect.placeholder).toBe('Langue *');
      since('input should be required').expect(BallotDocumentationEdit.languageSelect.required).toBeTruthy();
    });

    it('localized label input should have placeholder "Intitulé"', () => {
      expect(BallotDocumentationEdit.localizedLabelInput.placeholder).toBe('Intitulé');
      since('input should be required').expect(BallotDocumentationEdit.localizedLabelInput.required).toBeTruthy();
    });

    it('file input should have placeholder "Sélectionner un fichier"', () => {
      expect(BallotDocumentationEdit.uploadInput.fileInput.placeholder).toBe('Sélectionner un fichier');
      since('input should be required').expect(BallotDocumentationEdit.uploadInput.fileInput.required).toBeTruthy();
    });

    it('[BROWSE] button should be present', () => {
      since('button should be displayed').expect(BallotDocumentationEdit.uploadInput.browseButton.displayed)
        .toBeTruthy();
      expect(BallotDocumentationEdit.uploadInput.browseButton.text).toBe('attach_file PARCOURIR');
    });

    it('[UPLOAD] button should be present and disabled', () => {
      since('button should be displayed').expect(BallotDocumentationEdit.uploadInput.uploadButton.displayed)
        .toBeTruthy();
      since('button should be disable').expect(BallotDocumentationEdit.uploadInput.uploadButton.enabled).toBeFalsy();
      expect(BallotDocumentationEdit.uploadInput.uploadButton.text).toBe('file_upload IMPORTER');
    });
  });

  describe('import ballot documentation', () => {
    it('should display an error if the ballot or language or localized label is not specified', () => {
      // given
      BallotDocumentationEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      // when
      BallotDocumentationEdit.uploadInput.uploadButton.click();
      // then
      since('ballot input should be in error').expect(BallotDocumentationEdit.ballotSelect.error)
        .toBe('Champ obligatoire');
      since('language input should be in error').expect(BallotDocumentationEdit.languageSelect.error)
        .toBe('Champ obligatoire');
      since('localized label input should be in error').expect(BallotDocumentationEdit.localizedLabelInput.error)
        .toBe('Champ obligatoire');
    });

    it('should display the result grid if the file is valid', () => {
      // given
      BallotDocumentationEdit.ballotSelect.select('20181030VP-CAN-GE');
      BallotDocumentationEdit.languageSelect.select('Français');
      BallotDocumentationEdit.localizedLabelInput.value = 'test';
      // when
      BallotDocumentationEdit.uploadInput.uploadButton.click();
      // then
      expect(BallotDocumentationEdit.table.displayed).toBeTruthy();
    });

    it('ballot document table should display 6 columns...', () => {
      expect(BallotDocumentationEdit.table.headerValue('managementEntity')).toBe('Entité de gestion');
      expect(BallotDocumentationEdit.table.headerValue('ballotId')).toBe('Scrutin');
      expect(BallotDocumentationEdit.table.headerValue('file')).toBe('Fichier');
      expect(BallotDocumentationEdit.table.headerValue('localizedLabel')).toBe('Intitulé du document');
      expect(BallotDocumentationEdit.table.headerValue('language')).toBe('Langue');
      expect(BallotDocumentationEdit.table.headerValue('delete')).toBe('Supprimer');
    });

    it('... And one row', () => {
      since('table should contain one row').expect(BallotDocumentationEdit.table.rowCount).toBe(1);
      expect(BallotDocumentationEdit.table.cellValue(0, 'managementEntity')).toBe('Canton de Genève');
      expect(BallotDocumentationEdit.table.cellValue(0, 'ballotId')).toBe('20181030VP-CAN-GE');
      expect(BallotDocumentationEdit.table.cellValue(0, 'file')).toBe('test-file-1.pdf');
      expect(BallotDocumentationEdit.table.cellValue(0, 'localizedLabel')).toBe('test');
      expect(BallotDocumentationEdit.table.cellValue(0, 'language')).toBe('Français');
      expect(BallotDocumentationEdit.table.cellValue(0, 'delete')).toBe('clear');
    });

    it('selecting same ballot should allow user to select same language for a new import', () => {
      BallotDocumentationEdit.ballotSelect.select('20181030VP-CAN-GE');
      expect(BallotDocumentationEdit.languageSelect.hasOption('Français')).toBeTruthy();
    });

    it('selecting same language should allow user to select same ballot for a new import', () => {
      BallotDocumentationEdit.ballotSelect.select('20181030VP-CAN-GE');
      BallotDocumentationEdit.languageSelect.select('Français');
      expect(BallotDocumentationEdit.ballotSelect.hasOption('20181030VP-CAN-GE')).toBeTruthy();
    });
  });

  describe('delete a ballot documentation', () => {
    it('should open a confirmation dialog when clicking on the [DELETE] button', () => {
      // when
      BallotDocumentationEdit.getDeleteButton(0).click();
      // then
      since('the confirmation dialog should be displayed').expect(Main.confirmDialog.content.isDisplayed())
        .toBeTruthy();
      expect(Main.confirmDialog.content.getText())
        .toBe('Souhaitez-vous supprimer la documentation en Français du scrutin 20181030VP-CAN-GE ?');
    });

    it('cancelling action should close the dialog', () => {
      // when
      Main.confirmDialog.cancel();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
    });

    it('accepting action should remove the ballot documentation file', () => {
      // when
      BallotDocumentationEdit.getDeleteButton(0).click();
      Main.confirmDialog.accept();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
      since('table should be empty').expect(BallotDocumentationEdit.table.rowCount).toBe(0);
    });
  });

  describe('documentation summary card', () => {
    it('title should be "Documentation scrutins"', () => {
      BallotDocumentationEdit.ballotSelect.select('20181030VP-CAN-GE');
      BallotDocumentationEdit.languageSelect.select('Français');
      BallotDocumentationEdit.localizedLabelInput.value = 'test';
      BallotDocumentationEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      BallotDocumentationEdit.uploadInput.uploadButton.click();
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.ballotDocumentationCard.title).toBe('Documentation scrutins');
    });

    it('subtitle should be empty', () => {
      expect(OperationManagementPage.ballotDocumentationCard.subtitle).toBe('');
    });

    it('content should display the number of documents', () => {
      expect(OperationManagementPage.ballotDocumentationCard.getContentValue(0)).toBe('1 documentation');
    });

    it('[CONFIGURE] button should be displayed', () => {
      expect(OperationManagementPage.ballotDocumentationCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.ballotDocumentationCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('invited management entity rights', () => {
    it('should be in read only if user has no right', () => {
      Main.login('ge4', true);
      Main.navigateTo(`operations/[OPERATION_ID]/parameters`);
      waitForVisibility(OperationManagementPage.ballotDocumentationCard.element);
      since('summary card should be in read-only mode').expect(OperationManagementPage.ballotDocumentationCard.readOnly)
        .toBeTruthy();

      OperationManagementPage.ballotDocumentationCard.button.click();
      since('delete button should be hidden').expect(BallotDocumentationEdit.getDeleteButton(0).present).toBeFalsy();
      since('input form should be hidden').expect(BallotDocumentationEdit.ballotSelect.present).toBeFalsy();
    });

    it('should allow an invited management entity to see ballot documentation', () => {
      Main.inviteManagementEntity('Gy');
      Main.login('ge3', true);
      Main.navigateTo('operations/[OPERATION_ID]/parameters');
      waitForVisibility(OperationManagementPage.ballotDocumentationCard.element);
      expect(OperationManagementPage.ballotDocumentationCard.readOnly).toBeFalsy();
    });

    it('should not allow guest management entity to delete other\'s ballot documentation', () => {
      OperationManagementPage.ballotDocumentationCard.button.click();
      expect(BallotDocumentationEdit.getDeleteButton(0).present).toBeFalsy();
    });

    it('should allow guest management entity to add new ballot documentation', () => {
      BallotDocumentationEdit.ballotSelect.select('20181030VP-CAN-GE');
      BallotDocumentationEdit.languageSelect.select('Allemand');
      BallotDocumentationEdit.localizedLabelInput.value = 'test';
      BallotDocumentationEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      BallotDocumentationEdit.uploadInput.uploadButton.click();
      browser.wait(BallotDocumentationEdit.table.rowCount.then(count => count == 2), 5000, "Row count should be 2");
    });

    it('should allow operation owner to delete both documentations', () => {
      Main.login('ge1', true);
      Main.navigateTo('/operations/[OPERATION_ID]/parameters/edit/ballot-documentation');
      waitForVisibility(BallotDocumentationEdit.root.element);
      since('first document should be deletable').expect(BallotDocumentationEdit.getDeleteButton(0).present)
        .toBeTruthy();
      since('second document should be deletable').expect(BallotDocumentationEdit.getDeleteButton(1).present)
        .toBeTruthy();

      // end suit, remove ballot documentation and switch back to the parameters page
      Main.updateOperation([OperationOption.REMOVE_BALLOT_DOCUMENTATION]);
      ParameterEditPage.clickBackLink();
    });
  });

});
