/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { configuration } from '../../../shared/mock-server';
import { browser } from 'protractor';
import {
  DeploymentSummarySection, OperationManagementPage, ParametersNotificationSection
} from '../../../page-object/operation/operation-management.po';
import { Main } from '../../../page-object/main.po';
import { BaseEdit } from '../../../page-object/operation/parameters/base-edit.po';
import { ConsistencyCheck } from '../../../page-object/consistency-check.po';
import { waitForVisibility } from '../../../shared/e2e.utils';

let since = require('jasmine2-custom-message');

const FLOW = browser.controlFlow();

describe('Operation/Parameters/Workflow', () => {

  describe('deploy configuration', () => {
    it('should wait consistency to get computed', () => {
      ConsistencyCheck.waitConsistencyCheck();
    });

    it('should have status "En cours de création" after uploading', () => {
      expect(DeploymentSummarySection.testCard.displayed).toBeTruthy();
      expect(DeploymentSummarySection.testStatusMessage).toBe('En cours de création');
    });

    it('should have parameters in read-only mode', () => {
      expect(OperationManagementPage.baseCard.readOnly).toBeTruthy();
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

  });

  describe('deployment failed', () => {
    it('should have status "Erreur lors de la création du site de test" after PACT failed during uploading', () => {
      FLOW.execute(() => configuration.putDeploymentInErrorInPact(Main.operationId));
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Erreur lors de la création du site de test/);
    });

    it('should not have parameters in read-only mode', () => {
      expect(OperationManagementPage.baseCard.readOnly).toBeFalsy();
    });

    it('should allow re-uploading', () => {
      ParametersNotificationSection.uploadConfigurationButton.click();
      expect(DeploymentSummarySection.testCard.displayed).toBeTruthy();
      expect(DeploymentSummarySection.testStatusMessage).toBe('En cours de création');
    });
  });

  describe('test site is available', () => {
    it('should have status "En cours de test" after PACT finished deployment', () => {
      FLOW.execute(() => configuration.finishTestDeploymentInPact(Main.operationId));
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Site en phase de test du paramétrage/);
    });

    it('should have parameters in read-only mode', () => {
      expect(OperationManagementPage.baseCard.readOnly).toBeTruthy();
    });

    it('should have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('disponible');
    });

    it('should have validation buttons visible', () => {
      since('[INVALIDATE] button should be displayed').expect(ParametersNotificationSection.invalidateParametersButton.displayed).toBeTruthy();
      since('[VALIDATE] button should be displayed').expect(ParametersNotificationSection.validateParametersButton.displayed).toBeTruthy();
    });
  });

  describe('test site invalidation', () => {
    it('should display a confirm dialog when invalidating the test site', () => {
      ParametersNotificationSection.invalidateParametersButton.click();
      Main.confirmDialog.waitDisplayed();

    });

    it('should have status "Paramétrage invalidé le..." after invalidation', () => {
      Main.confirmDialog.accept();
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Paramétrage invalidé le /);
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('should not have parameters in read-only mode', () => {
      expect(OperationManagementPage.baseCard.readOnly).toBeFalsy();
    });

    it('should have [TEST CONFIGURATION] button available after modification', () => {
      OperationManagementPage.baseCard.button.click();
      BaseEdit.longLabelInput.value = 'modified after invalidation';
      BaseEdit.saveButton.click(true);
      ConsistencyCheck.waitConsistencyCheck();
      expect(ParametersNotificationSection.uploadConfigurationButton.displayed).toBeTruthy();
    });

    it('should allow configuration testing', () => {
      ParametersNotificationSection.uploadConfigurationButton.click();
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/En cours de création/);

      FLOW.execute(() => configuration.finishTestDeploymentInPact(Main.operationId));
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Site en phase de test du paramétrage/);
    });

  });

  describe('test site validation', () => {
    it('should have status "Paramétrage validé le..." after validation', () => {
      ParametersNotificationSection.validateParametersButton.click();
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Paramétrage validé le /);
    });

    it('<deployment request> link should be available', () => {
      expect(ParametersNotificationSection.deploymentRequestLink.isDisplayed()).toBeTruthy();
    });

    it('should have parameters in read-only mode', () => {
      expect(OperationManagementPage.baseCard.readOnly).toBeTruthy();
    });

    it("Should have voting material available", () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('disponible');
    });
  });

  describe('deployment requested in PACT', () => {

    it('test deployment summary card should be available', () => {
      FLOW.execute(() => configuration.requestDeploymentInPact(Main.operationId));
      waitForVisibility(DeploymentSummarySection.testCard.element);
    });

    it('should have status "Déploiement demandé le..."', () => {
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Déploiement demandé le/);
    });

    it('<validate deployment> link should be available', () => {
      expect(ParametersNotificationSection.deploymentValidationLink.isDisplayed()).toBeTruthy();
    });

    it('production deployment summary card should be hidden', () => {
      expect(DeploymentSummarySection.productionCard.present).toBeFalsy();
    });

    it('should have parameters in read-only mode', () => {
      expect(OperationManagementPage.baseCard.readOnly).toBeTruthy();
    });

    it('should have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('disponible');
    });

  });

  describe('deployment refused in PACT', () => {

    it(
      'should have status "Déploiement refusé le... par mock-server : Mock server has been asked to refuse this deployment" after rejection',
      () => {
        FLOW.execute(() => configuration.refuseDeploymentRequestInPact(Main.operationId));
        DeploymentSummarySection.waitForTestStatusMessageToMatch(
          /Déploiement refusé le .* par mock-server : Mock server has been asked to refuse this deployment/
        );
      });

    it('should not have parameters in read-only mode', () => {
      expect(OperationManagementPage.baseCard.readOnly).toBeFalsy();
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('should have [TEST CONFIGURATION] button available after modification', () => {
      OperationManagementPage.baseCard.button.click();
      BaseEdit.longLabelInput.value = 'modified after invalidation';
      BaseEdit.saveButton.click(true);
      ConsistencyCheck.waitConsistencyCheck();
      expect(ParametersNotificationSection.uploadConfigurationButton.displayed).toBeTruthy();
    });

    it('should allow user to resubmit after modification', () => {
      ParametersNotificationSection.uploadConfigurationButton.click();
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/En cours de création/);

      FLOW.execute(() => configuration.finishTestDeploymentInPact(Main.operationId));
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Site en phase de test du paramétrage/);

      ParametersNotificationSection.validateParametersButton.click();
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Paramétrage validé le /);

      FLOW.execute(() => configuration.requestDeploymentInPact(Main.operationId));
      DeploymentSummarySection.waitForTestStatusMessageToMatch(/Déploiement demandé le/);
    });

  });

  describe('deployment validated in PACT', () => {
    it('should have status "Déploiement validé le..." after validation', () => {
      FLOW.execute(() => configuration.validateDeploymentRequestInPact(Main.operationId));
      DeploymentSummarySection.waitForDeploymentStatusMessageToMatch(/Déploiement validé le .*/);
    });

    it('should have parameters in read-only mode', () => {
      expect(OperationManagementPage.baseCard.readOnly).toBeTruthy();
    });

    it('should have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

  });

  describe('start operation in simulation target', () => {
    it('should allow creation of a simulation with a simulation name',
      () => {
        DeploymentSummarySection.waitForDeploymentStatusMessageToMatch(/Déploiement validé le .*/);
        DeploymentSummarySection.targetSimulationDeploymentButton.click();
        waitForVisibility(DeploymentSummarySection.simulationNameDialog);

        Main.screenshot('Simulation name popup is open');
        DeploymentSummarySection.simulationNameInput.value = 'test simulation';
        DeploymentSummarySection.simulationNameValidationButton.click();
        DeploymentSummarySection.waitForDeploymentStatusMessageToMatch(/Simulation "test simulation"/);
      });
  });

});
