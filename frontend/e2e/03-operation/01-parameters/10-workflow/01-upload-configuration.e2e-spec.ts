/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { configuration, OperationOption } from '../../../shared/mock-server';
import { browser } from 'protractor';
import { deepEqual } from '../../../shared/e2e.utils';
import {
  DeploymentSummarySection, OperationManagementPage, ParametersNotificationSection
} from '../../../page-object/operation/operation-management.po';
import { Main } from '../../../page-object/main.po';
import { ConsistencyCheck } from '../../../page-object/consistency-check.po';

let since = require('jasmine2-custom-message');

const EXPECTED_OPERATION = {
  "user": "ge1",
  "clientId": "2087242",
  "operationName": "TEST_e2e_workflow_parameters",
  "operationLabel": "test-workflow-parameters",
  "operationDate": "JSDate[12.06.2057 00:00:00.000]",
  "gracePeriod": 9,
  "canton": "GE",
  "groupVotation": true,
  "testSitePrinter": {
    "municipality": {"ofsId": 9999, "name": "Virtual municipality for printer boh-vprt"},
    "printer": {
      "id": "boh-vprt",
      "name": "BOH Virtual Printer",
      "publicKey": "CD9f8pbhRmPyKpgvtSSEPdhyvtRC7v8qZJYLnZ2ZXW3UevEH1Kw2iGskV2vnzswIwDO2xwZvCkR5xCO79tU5htVg64JbGJoE++cuLBzWWC0iWksVndp1z59yyQdj7+nFlXDr3CtgQiDGHlqHLJ2xaBxHgZTsHCFWvTTMxhWz9uk="
    }
  },
  "attachments": [
    {"zipFileName": "test-voting-cards-0.zip", "attachmentType": "REGISTER", "importDateTime": "--"},
    {
      "zipFileName": "file-0.zip", "attachmentType": "DOMAIN_OF_INFLUENCE", "importDateTime": "--",
      "externalIdentifier": "---"
    },
    {
      "zipFileName": "file-1.zip", "attachmentType": "REPOSITORY_VOTATION", "importDateTime": "--",
      "externalIdentifier": "---"
    },
    {
      "zipFileName": "file-2.zip", "attachmentType": "REPOSITORY_ELECTION", "importDateTime": "--",
      "externalIdentifier": "---"
    },
    {
      "zipFileName": "file-3.zip", "attachmentType": "DOCUMENT_FAQ", "importDateTime": "--", "language": "FR",
      "externalIdentifier": "---"
    },
    {
      "zipFileName": "file-4.zip", "attachmentType": "DOCUMENT_CERTIFICATE", "importDateTime": "--", "language": "FR",
      "externalIdentifier": "---"
    },
    {
      "zipFileName": "file-5.zip", "attachmentType": "DOCUMENT_TERMS", "importDateTime": "--", "language": "FR",
      "externalIdentifier": "---"
    },
    {"zipFileName": "translations.zip", "attachmentType": "TRANSLATIONS", "importDateTime": "--"}],
  "electionSiteConfigurations": [
    {
      "ballot": "Test majoritaire sans liste 2018", "displayCandidateSearchForm": false,
      "allowChangeOfElectoralList": false, "displayEmptyPosition": false,
      "displayCandidatePositionOnACompactBallotPaper": false, "displayCandidatePositionOnAModifiedBallotPaper": false,
      "displaySuffrageCount": false, "displayListVerificationCode": false, "allowOpenCandidature": false,
      "allowMultipleMandates": false, "displayVoidOnEmptyBallotPaper": false,
      "candidateInformationDisplayModel": "Grand Conseil",
      "displayedColumnsOnVerificationTable": ["candidate-identity", "verification-code"],
      "columnsOrderOnVerificationTable": []
    },
    {
      "ballot": "Grand Conseil 2018", "displayCandidateSearchForm": false, "allowChangeOfElectoralList": false,
      "displayEmptyPosition": false, "displayCandidatePositionOnACompactBallotPaper": false,
      "displayCandidatePositionOnAModifiedBallotPaper": false, "displaySuffrageCount": false,
      "displayListVerificationCode": false, "allowOpenCandidature": false, "allowMultipleMandates": false,
      "displayVoidOnEmptyBallotPaper": false, "candidateInformationDisplayModel": "Grand Conseil",
      "displayedColumnsOnVerificationTable": ["candidate-identity", "verification-code"],
      "columnsOrderOnVerificationTable": []
    },
    {
      "ballot": "Conseil d'Etat 2018", "displayCandidateSearchForm": false, "allowChangeOfElectoralList": false,
      "displayEmptyPosition": false, "displayCandidatePositionOnACompactBallotPaper": false,
      "displayCandidatePositionOnAModifiedBallotPaper": false, "displaySuffrageCount": false,
      "displayListVerificationCode": false, "allowOpenCandidature": false, "allowMultipleMandates": false,
      "displayVoidOnEmptyBallotPaper": false, "candidateInformationDisplayModel": "Grand Conseil",
      "displayedColumnsOnVerificationTable": ["candidate-identity", "verification-code"],
      "columnsOrderOnVerificationTable": []
    }
  ],
  "highlightedQuestions": [],
  "ballotDocumentations": [],
  "milestones": {
    "SITE_VALIDATION": "JSDate[01.06.2057 00:00:00.000]", "CERTIFICATION": "JSDate[02.06.2057 00:00:00.000]",
    "PRINTER_FILES": "JSDate[03.06.2057 00:00:00.000]", "BALLOT_BOX_INIT": "JSDate[04.06.2057 00:00:00.000]",
    "SITE_OPEN": "JSDate[05.06.2057 12:01:00.000]", "SITE_CLOSE": "JSDate[06.06.2057 09:00:00.000]",
    "BALLOT_BOX_DECRYPT": "JSDate[08.06.2057 00:00:00.000]", "RESULT_VALIDATION": "JSDate[01.07.2057 00:00:00.000]",
    "DATA_DESTRUCTION": "JSDate[02.07.2057 00:00:00.000]"
  }
};

const FLOW = browser.controlFlow();

describe('Operation/Parameters/Workflow', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test-workflow-parameters',
      'TEST_e2e_workflow_parameters',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_ELECTION_PAGE_PROPERTIES,
        OperationOption.ADD_VOTING_SITE_CONFIGURATION
      ]
    );
    FLOW.execute(() => configuration.resetPactConfiguration());
  });

  describe('upload configuration to PACT', () => {
    it('[TEST PARAMETERS] button should be displayed', () => {
      ConsistencyCheck.waitConsistencyCheck();
      expect(ParametersNotificationSection.uploadConfigurationButton.displayed).toBeTruthy();
    });

    it('should failed gracefully on PACT rejection', () => {
      // given
      FLOW.execute(() => configuration.nextConfigurationUploadIsInvalid());
      // when
      ParametersNotificationSection.uploadConfigurationButton.click();
      // then
      configuration.getLastOperationConfigurationUploadedToPact((content) => {
        expect(content).toBe('');
      });

      Main.screenshot("Bad request to pact");

      Main.businessErrorDialog.waitContentTextToBe('La requête envoyée au PACT n\'a pas été acceptée');

      Main.businessErrorDialog.close();
    });

    it('should upload configuration to PACT', () => {
      ParametersNotificationSection.uploadConfigurationButton.click();
      // Operation Data
      let actualOperation;
      configuration.getLastOperationConfigurationUploadedToPact(json => {
        actualOperation = JSON.parse(json as string);
      });

      FLOW.execute(() => deepEqual(EXPECTED_OPERATION, actualOperation,
        [
          "operationName",
          "operationDate",
          "clientId",
          "attachments.0.zipFileName",
          "attachments.0.importDateTime", "attachments.0.externalIdentifier",
          "attachments.1.importDateTime", "attachments.1.externalIdentifier",
          "attachments.2.importDateTime", "attachments.2.externalIdentifier",
          "attachments.3.importDateTime", "attachments.3.externalIdentifier",
          "attachments.4.importDateTime", "attachments.4.externalIdentifier",
          "attachments.5.importDateTime", "attachments.5.externalIdentifier",
          "attachments.6.importDateTime", "attachments.6.externalIdentifier",
          "attachments.7.importDateTime"
        ]));

    });

    it('test deployment summary card should be visible', () => {
      expect(DeploymentSummarySection.testCard.displayed).toBeTruthy();
    });

    it('test deployment status should be "En cours de création"', () => {
      expect(DeploymentSummarySection.testStatusMessage).toBe('En cours de création');
    });

    it('parameters should be in read-only mode', () => {
      since('base summary card should be in read-only').expect(OperationManagementPage.baseCard.readOnly).toBeTruthy();
      since('milestone summary card should be in read-only').expect(OperationManagementPage.milestoneCard.readOnly)
        .toBeTruthy();
      since('domain of influence summary card should be in read-only')
        .expect(OperationManagementPage.domainInfluenceCard.readOnly).toBeTruthy();
      since('repository summary card should be in read-only').expect(OperationManagementPage.repositoryCard.readOnly)
        .toBeTruthy();
      since('testing cards summary card should be in read-only')
        .expect(OperationManagementPage.testingCardsLotCard.readOnly).toBeTruthy();
      since('election page properties summary card should be in read-only')
        .expect(OperationManagementPage.electionPagePropertiesCard.readOnly).toBeTruthy();
      since('documentation summary card should be in read-only')
        .expect(OperationManagementPage.documentationCard.readOnly).toBeTruthy();
      since('management entity summary card should be in read-only')
        .expect(OperationManagementPage.managementEntityCard.readOnly).toBeTruthy();
      since('ballot documentation summary card should be in read-only')
        .expect(OperationManagementPage.ballotDocumentationCard.readOnly).toBeTruthy();
    });

  });

});
