/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../../../page-object/main.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { ManagementEntityEdit } from '../../../page-object/operation/parameters/management-entity/management-entity-edit.po';
import { ManagementEntitySelection } from '../../../page-object/operation/parameters/management-entity/management-entity-select.po';
import { OperationOption } from '../../../shared/mock-server';
import { RepositoryEdit } from '../../../page-object/operation/parameters/repository-edit.po';
import { OperationListSection } from '../../../page-object/dashboard/operation-list-section.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { waitForVisibility } from '../../../shared/e2e.utils';

let since = require('jasmine2-custom-message');


describe('Operation/Parameters/Management Entity', () => {
  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_testing_cards',
      'TEST_e2e_testing_cards',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_VOTING_SITE_CONFIGURATION
      ]
    );
  });

  describe('management entity summary card before edit', () => {
    it('management entity card should be displayed in the dashboard', () => {
      expect(OperationManagementPage.managementEntityCard.displayed).toBeTruthy();
    });

    it('title should be "Entités de gestion"', () => {
      expect(OperationManagementPage.managementEntityCard.title).toBe('Entités de gestion');
    });

    it('subtitle should be "Définir les entités de gestion invitées"', () => {
      expect(OperationManagementPage.managementEntityCard.subtitle).toBe('Définir les entités de gestion invitées');
    });

    it('guest management entity should be equals to 0/44 for the newly created management entity', () => {
      expect(OperationManagementPage.managementEntityCard.getContentLabel(0)).toBe('Entités de gestion invitées');
      expect(OperationManagementPage.managementEntityCard.getContentValue(0)).toBe('0 / 44');
    });

    it('[CONFIGURE] button should be displayed', () => {
      expect(OperationManagementPage.managementEntityCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.managementEntityCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('management entity edit section', () => {

    it('should display the management entity section when clicking on the [CONFIGURE] button', () => {
      OperationManagementPage.managementEntityCard.button.click();
    });

    it('card title should be "Entités de gestion"', () => {
      expect(ManagementEntityEdit.root.title).toBe('Entités de gestion');
    });

    it('card subtitle should be "Définir les entités de gestion invitées"', () => {
      expect(ManagementEntityEdit.root.subtitle).toContain('Définir les entités de gestion invitées');
    });

    it('should display that no management entity has been invited', () => {
      since('no management entity should be displayed').expect(ManagementEntityEdit.hasNoManagementEntity).toBeTruthy();
      since('all management entities should be hidden').expect(ManagementEntityEdit.hasAllManagementEntities)
        .toBeFalsy();
      since('invited management entities list should be #{expected} (was #{actual})')
        .expect(ManagementEntityEdit.invitedManagementEntities).toEqual([]);
    });

    it('[REVOKE] button should be hidden', () => {
      expect(ManagementEntityEdit.revokeButton.present).toBeFalsy();
    });

    it('[INVITE] button should be displayed', () => {
      expect(ManagementEntityEdit.inviteButton.displayed).toBeTruthy();
    });

  });

  describe('invite', () => {
    it('should display a select popup when clicking on [INVITE] button', () => {
      ManagementEntityEdit.inviteButton.click();
      expect(ManagementEntitySelection.root.isPresent).toBeTruthy();
    });

    it('popup title should be "Sélectionner les entités de gestion à inviter"', () => {
      expect(ManagementEntitySelection.title).toBe('Sélectionner les entités de gestion à inviter');
    });

    it('all management entities should be displayed', () => {
      expect(ManagementEntitySelection.availableManagementEntities.then(v => v.length)).toBe(44);
    });

    it('[INVITE] button should be disable', () => {
      expect(ManagementEntitySelection.acceptButton.enabled).toBeFalsy();
    });

    it('when filtering only matching management entity should be displayed', () => {
      ManagementEntitySelection.filterInput.value = 'vu';
      expect(ManagementEntitySelection.availableManagementEntities).toEqual(['Avully', 'Avusy', 'Bellevue']);
    });

    it('can toggle all management entity', () => {
      ManagementEntitySelection.toggleAll();
    });

    it('can toggle one management entity', () => {
      ManagementEntitySelection.toggle('Avully');
    });

    it('can invite selected management entities', () => {
      ManagementEntitySelection.acceptButton.click();
      ManagementEntitySelection.waitForInvisibility();
    });

    it('should update list page', () => {
      since('no management entity should be hidden').expect(ManagementEntityEdit.hasNoManagementEntity).toBeFalsy();
      since('all management entities should be hidden').expect(ManagementEntityEdit.hasAllManagementEntities)
        .toBeFalsy();
      since('invited management entities list should be #{expected} (was #{actual})')
        .expect(ManagementEntityEdit.invitedManagementEntities).toEqual(['Avusy', 'Bellevue']);
    });

    it('[REVOKE] button should be displayed', () => {
      expect(ManagementEntityEdit.revokeButton.displayed).toBeTruthy();
    });

    it('[INVITE] button should be displayed', () => {
      expect(ManagementEntityEdit.inviteButton.displayed).toBeTruthy();
    });

    it('should only propose not invited management entities when returning on selection', () => {
      ManagementEntityEdit.inviteButton.click();
      expect(ManagementEntitySelection.availableManagementEntities.then(v => v.length)).toBe(42);
    });

    it('should accept inviting all management entities', () => {
      ManagementEntitySelection.toggleAll();
      ManagementEntitySelection.acceptButton.click();
      ManagementEntitySelection.waitForInvisibility();
    });

    it('should update list page', () => {
      since('no management entity should be hidden').expect(ManagementEntityEdit.hasNoManagementEntity).toBeFalsy();
      since('all management entities should be displayed').expect(ManagementEntityEdit.hasAllManagementEntities)
        .toBeTruthy();
      since('invited management entities list should be #{expected} (was #{actual})')
        .expect(ManagementEntityEdit.invitedManagementEntities).toEqual([]);
    });

    it('[REVOKE] button should be displayed', () => {
      expect(ManagementEntityEdit.revokeButton.displayed).toBeTruthy();
    });

    it('[INVITE] button should be hidden', () => {
      expect(ManagementEntityEdit.inviteButton.present).toBeFalsy();
    });
  });

  describe('revoke', () => {
    it('should display a select popup when clicking on [REVOKE] button', () => {
      ManagementEntityEdit.revokeButton.click();
      expect(ManagementEntitySelection.root.isPresent).toBeTruthy();
    });

    it('popup title should be "Sélectionner les entités de gestion à révoquer"', () => {
      expect(ManagementEntitySelection.title).toBe('Sélectionner les entités de gestion à révoquer');
    });

    it('all management entities should be displayed', () => {
      expect(ManagementEntitySelection.availableManagementEntities.then(v => v.length)).toBe(44);
    });

    it('[REVOKE] button should be disable', () => {
      expect(ManagementEntitySelection.acceptButton.enabled).toBeFalsy();
    });

    it('when filtering only matching management entity should be displayed', () => {
      ManagementEntitySelection.filterInput.value = 'vu';
      expect(ManagementEntitySelection.availableManagementEntities).toEqual(['Avully', 'Avusy', 'Bellevue']);
    });

    it('can toggle all management entity', () => {
      ManagementEntitySelection.filterInput.value = '';
      ManagementEntitySelection.toggleAll();
    });

    it('can toggle one management entity', () => {
      ManagementEntitySelection.toggle('Gy');
    });

    it('can revoke selected management entities', () => {
      ManagementEntitySelection.acceptButton.click();
      ManagementEntitySelection.waitForInvisibility();
    });

    it('should update list page', () => {
      since('no management entity should be hidden').expect(ManagementEntityEdit.hasNoManagementEntity).toBeFalsy();
      since('all management entities should be hidden').expect(ManagementEntityEdit.hasAllManagementEntities)
        .toBeFalsy();
      since('invited management entities list should be #{expected} (was #{actual})')
        .expect(ManagementEntityEdit.invitedManagementEntities).toEqual(['Gy']);
    });

    it('[REVOKE] button should be displayed', () => {
      expect(ManagementEntityEdit.revokeButton.displayed).toBeTruthy();
    });

    it('[INVITE] button should be displayed', () => {
      expect(ManagementEntityEdit.inviteButton.displayed).toBeTruthy();
    });

    it('should only propose invited management entities when returning on selection', () => {
      ManagementEntityEdit.revokeButton.click();
      expect(ManagementEntitySelection.availableManagementEntities.then(v => v.length)).toBe(1);
    });

    it('should accept revoking all management entities', () => {
      ManagementEntitySelection.toggleAll();
      ManagementEntitySelection.acceptButton.click();
      ManagementEntitySelection.waitForInvisibility();
    });

    it('should update list page', () => {
      since('no management entity should be displayed').expect(ManagementEntityEdit.hasNoManagementEntity).toBeTruthy();
      since('all management entities should be hidden').expect(ManagementEntityEdit.hasAllManagementEntities)
        .toBeFalsy();
      since('invited management entities list should be #{expected} (was #{actual})')
        .expect(ManagementEntityEdit.invitedManagementEntities).toEqual([]);
    });

    it('[REVOKE] button should be hidden', () => {
      expect(ManagementEntityEdit.revokeButton.present).toBeFalsy();
    });

    it('[INVITE] button should be hidden', () => {
      expect(ManagementEntityEdit.inviteButton.displayed).toBeTruthy();
    });
  });

  describe('invited management entity rights', () => {

    it('invite allow other management entity to see the operation', () => {
      ManagementEntityEdit.inviteButton.click();
      ManagementEntitySelection.filterInput.value = 'G';
      ManagementEntitySelection.toggle('Gy');
      ManagementEntitySelection.acceptButton.click();
      ManagementEntitySelection.waitForInvisibility();
      expect(ManagementEntityEdit.invitedManagementEntities).toEqual(['Gy']);
      Main.screenshot('Gy got invited');
      Main.login('ge3', true);
      Main.screenshot('ge3 is logged');
      expect(OperationListSection.operationsGrid.rowCount).toBe(1);
    });

    it('cannot revoke a management entity that have imported files in the operation', () => {
      Main.navigateTo('operations/[OPERATION_ID]/parameters/edit/repository');
      waitForVisibility(RepositoryEdit.root.element);
      RepositoryEdit.uploadInput.file = '../resources/parameters/valid-repository-votation.xml';
      RepositoryEdit.uploadInput.uploadButton.click();
      Main.screenshot('ge3 has uploaded a repository');

      Main.login('ge1');
      Main.navigateTo('operations/[OPERATION_ID]/parameters/edit/management-entity');
      ManagementEntityEdit.revokeButton.click();
      waitForVisibility(ManagementEntitySelection.root);
      ManagementEntitySelection.toggleAll();


      ManagementEntitySelection.acceptButton.click();
      waitForVisibility(ManagementEntitySelection.revokeError)
      expect(ManagementEntitySelection.revokeErrorMessage)
        .toBe('L\'opération contient des élements importés par l\'entité de gestion "Gy"');

      // end test suite: remove the repository imported by invited management entity and revoke this entity
      Main.navigateTo('operations/[OPERATION_ID]/parameters/edit/repository');
      RepositoryEdit.getDeleteButton(2).click();
      Main.confirmDialog.accept();
      ParameterEditPage.clickBackLink();
      Main.invitedManagementEntities.push('Gy');

    }, 48000);
  });

})
;


