/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, protractor } from "protractor";
import { OperationOption } from "../../../shared/mock-server";
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { DomainInfluenceEdit } from '../../../page-object/operation/parameters/domain-influence-edit.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { Main } from '../../../page-object/main.po';
let since = require('jasmine2-custom-message');
import * as moment from "moment";
import { waitForVisibility } from '../../../shared/e2e.utils';

const invalidCsv = `Ligne;Colonne;Message
32;62;"cvc-enumeration-valid: Value 'XX' is not facet-valid with respect to enumeration '[CH, CT, BZ, MU, SC, KI, OG, KO, SK, AN]'. It must be a value from the enumeration."
32;62;"cvc-type.3.1.3: The value 'XX' of element 'ns3:domainOfInfluenceType' is not valid."`;

describe('Operation/Parameters/Domain of influence', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_doi',
      'TEST_e2e_domain_of_influence',
      [
        OperationOption.ADD_MILESTONE
      ]
    );
  });

  describe('domain of influence edit section', () => {

    it('should display the domain of influence section when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.domainInfluenceCard.button.click();
      // then
      expect(DomainInfluenceEdit.root.displayed).toBeTruthy();
    });

    it('card title should be "Domaines d\'influence"', () => {
      expect(DomainInfluenceEdit.root.title).toBe('Domaines d\'influence');
    });

    it('card subtitle should be "Liste des domaines d\'influence de l\'opération"', () => {
      expect(DomainInfluenceEdit.root.subtitle).toContain('Liste des domaines d\'influence de l\'opération');
    });

    it('file input should have placeholder "Sélectionner un fichier de domaines d\'influence"', () => {
      expect(DomainInfluenceEdit.uploadInput.fileInput.placeholder).toBe('Sélectionner un fichier de domaines d\'influence');
      since('input should be required').expect(DomainInfluenceEdit.uploadInput.fileInput.required).toBeTruthy();
    });

    it('[BROWSE] button should be present', () => {
      since('button should be displayed').expect(DomainInfluenceEdit.uploadInput.browseButton.displayed).toBeTruthy();
      expect(DomainInfluenceEdit.uploadInput.browseButton.text).toBe('attach_file PARCOURIR');
    });

    it('[UPLOAD] button should be present and disabled', () => {
      since('button should be displayed').expect(DomainInfluenceEdit.uploadInput.uploadButton.displayed).toBeTruthy();
      since('button should be disable').expect(DomainInfluenceEdit.uploadInput.uploadButton.enabled).toBeFalsy();
      expect(DomainInfluenceEdit.uploadInput.uploadButton.text).toBe('file_upload IMPORTER');
    });
  });

  describe('import a domain of influence', () => {

    it('should display an error if the file is not an XML file', () => {
      // given
      DomainInfluenceEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      // when
      DomainInfluenceEdit.uploadInput.uploadButton.click();
      // then
      expect(DomainInfluenceEdit.uploadInput.fileInput.error).toBe('Le fichier sélectionné n\'a pas été reconnu comme un fichier XML');
    });

    it('should display an error if the file is invalid', () => {
      // given
      DomainInfluenceEdit.uploadInput.file = '../resources/parameters/invalid-domain-influence.xml';
      // when
      DomainInfluenceEdit.uploadInput.uploadButton.click();
      // then
      expect(DomainInfluenceEdit.validationErrors.isDisplayed()).toBeTruthy();

      // TODO: to uncomment once the Chrome driver download issue is fixed
      // domainInfluenceSection.validationErrorsDownloadButton.click();
      // expect(lastDownloadedContent()).toEqual(invalidCsv)
    });

    it('should display the result grid if the file is valid', () => {
      // given
      DomainInfluenceEdit.uploadInput.file = '../resources/parameters/valid-domain-influence.xml';
      // when
      DomainInfluenceEdit.uploadInput.uploadButton.click();
      // then
      expect(DomainInfluenceEdit.fileTable.displayed).toBeTruthy();
    });

    it('the result grid should display 4 columns...', () => {
      expect(DomainInfluenceEdit.fileTable.headerValue('file')).toBe('Fichier');
      expect(DomainInfluenceEdit.fileTable.headerValue('author')).toBe('Auteur');
      expect(DomainInfluenceEdit.fileTable.headerValue('importedDate')).toBe('Importé le');
      expect(DomainInfluenceEdit.fileTable.headerValue('delete')).toBe('Supprimer');
    });

    it('... And one row', () => {
      since('table should contain #{expected} row (actual #{actual})').expect(DomainInfluenceEdit.fileTable.rowCount).toBe(1);
      expect(DomainInfluenceEdit.fileDetailPanel.headerLabel).toBe('valid-domain-influence.xml');
      expect(DomainInfluenceEdit.fileTable.cellValue(0, 'author')).toBe('ge1');
      expect(DomainInfluenceEdit.fileTable.cellValue(0, 'importedDate')).toBe(moment().format('D.MM.Y'));
      expect(DomainInfluenceEdit.fileTable.cellValue(0, 'delete')).toBe('clear');
    });


    it('should expand the panel when clicking on the detail panel', () => {
      // when
      DomainInfluenceEdit.fileDetailPanel.header.element(by.tagName('mat-icon')).click();
      // then
      expect(DomainInfluenceEdit.fileDetailPanel.content.isDisplayed()).toBeTruthy();
    });

    it('should display 4 columns...', () => {
      expect(DomainInfluenceEdit.detailTable.headerValue('type')).toBe('Type de domaine d\'influence');
      expect(DomainInfluenceEdit.detailTable.headerValue('id')).toBe('Identification locale');
      expect(DomainInfluenceEdit.detailTable.headerValue('name')).toBe('Nom du domaine d\'influence');
      expect(DomainInfluenceEdit.detailTable.headerValue('shortName')).toBe('Nom abrégé');
    });

    it('... With three rows', () => {
      since('table should contain #{expected} rows (actual #{actual})').expect(DomainInfluenceEdit.detailTable.rowCount).toBe(4);

      expect(DomainInfluenceEdit.detailTable.cellValue(0, 'type')).toBe('CH');
      expect(DomainInfluenceEdit.detailTable.cellValue(0, 'id')).toBe('1');
      expect(DomainInfluenceEdit.detailTable.cellValue(0, 'name')).toBe('Confédération');
      expect(DomainInfluenceEdit.detailTable.cellValue(0, 'shortName')).toBe('CH');

      expect(DomainInfluenceEdit.detailTable.cellValue(1, 'type')).toBe('CT');
      expect(DomainInfluenceEdit.detailTable.cellValue(1, 'id')).toBe('1');
      expect(DomainInfluenceEdit.detailTable.cellValue(1, 'name')).toBe('Canton de Zürich');
      expect(DomainInfluenceEdit.detailTable.cellValue(1, 'shortName')).toBe('ZH');

      expect(DomainInfluenceEdit.detailTable.cellValue(2, 'type')).toBe('MU');
      expect(DomainInfluenceEdit.detailTable.cellValue(2, 'id')).toBe('261');
      expect(DomainInfluenceEdit.detailTable.cellValue(2, 'name')).toBe('Ville de Zürich');
      expect(DomainInfluenceEdit.detailTable.cellValue(2, 'shortName')).toBe('Züri');

      expect(DomainInfluenceEdit.detailTable.cellValue(3, 'type')).toBe('MU');
      expect(DomainInfluenceEdit.detailTable.cellValue(3, 'id')).toBe('262');
      expect(DomainInfluenceEdit.detailTable.cellValue(3, 'name')).toBe('Ville de Genève');
      expect(DomainInfluenceEdit.detailTable.cellValue(3, 'shortName')).toBe('Genf');
    });
  });

  describe('delete a domain of influence', () => {

    it('should open a confirmation dialog when clicking on the [DELETE] button', () => {
      // when
      DomainInfluenceEdit.deleteButton.click();

      // then
      since('the confirmation dialog should be displayed').expect(Main.confirmDialog.content.isDisplayed()).toBeTruthy();
      expect(Main.confirmDialog.content.getText()).toBe('Souhaitez-vous supprimer le fichier valid-domain-influence.xml ?');
    });

    it('cancelling action should close the dialog', () => {
      // when
      Main.confirmDialog.cancel();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
    });

    it('accepting action should remove the domain of influence file', () => {
      // when
      DomainInfluenceEdit.deleteButton.click();
      Main.confirmDialog.accept();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
      since('file grid should be hidden').expect(DomainInfluenceEdit.fileTable.displayed).toBeFalsy();
    });
  });

  describe('domain of influence summary card', () => {

    it('title should be "Domaines d\'influence *"', () => {
      Main.updateOperation([OperationOption.ADD_DOMAIN_INFLUENCE]);
      ParameterEditPage.clickBackLink();
      Main.refresh();
      waitForVisibility(OperationManagementPage.domainInfluenceCard.element);
      expect(OperationManagementPage.domainInfluenceCard.title).toBe('Domaines d\'influence *');
    });

    it('subtitle should be empty', () => {
      expect(OperationManagementPage.domainInfluenceCard.subtitle).toBe('');
    });

    it('content should display the domains of influence', () => {
      since('first label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.domainInfluenceCard.getContentLabel(0)).toBe('CH');
      since('first value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.domainInfluenceCard.getContentValue(0)).toBe('1 domaine');
      since('second label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.domainInfluenceCard.getContentLabel(1)).toBe('CT');
      since('second value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.domainInfluenceCard.getContentValue(1)).toBe('1 domaine');
      since('third label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.domainInfluenceCard.getContentLabel(2)).toBe('MU');
      since('third value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.domainInfluenceCard.getContentValue(2)).toBe('3 domaines');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.domainInfluenceCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.domainInfluenceCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('form modification detection', () => {

    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.domainInfluenceCard.button.click();
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.domainInfluenceCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.domainInfluenceCard.button.click();
      DomainInfluenceEdit.uploadInput.file = '../resources/parameters/valid-domain-influence.xml';
      ParameterEditPage.clickBackLink(false);

      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.domainInfluenceCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      ParameterEditPage.clickBackLink(false);
      Main.confirmDialog.accept();
      expect(OperationManagementPage.domainInfluenceCard.present).toBeTruthy();
    });
  });

});
