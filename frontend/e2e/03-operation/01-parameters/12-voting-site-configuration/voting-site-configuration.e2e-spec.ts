/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { OperationOption } from '../../../shared/mock-server';
import { Main } from '../../../page-object/main.po';
import { VotingSiteConfigurationEdit } from '../../../page-object/operation/parameters/voting-site-configuration-edit.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';

describe('Operation/Parameters/Voting Site Configuration', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_vsc',
      'TEST_e2e_voting_site_config', [OperationOption.ADD_DOMAIN_INFLUENCE]
    );
  });

  describe('initial card status', () => {
    it('should not be completed', () => {
      expect(OperationManagementPage.votingSiteConfigurationCard.completeBadgeDisplayed).toBeFalsy();
    });
    it('should display no config', () => {
      expect(OperationManagementPage.votingSiteConfigurationCard.noConfigDisplayed).toBeTruthy();
    });
    it('should not be in error', () => {
      expect(OperationManagementPage.votingSiteConfigurationCard.inErrorBadgeDisplayed).toBeFalsy();
    });
    it('should not be in readonly', () => {
      expect(OperationManagementPage.votingSiteConfigurationCard.readOnly).toBeFalsy();
    });
  });

  describe('edit section', () => {

    it('should display the voting site configuration when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.votingSiteConfigurationCard.button.click(true);
      // then
      expect(VotingSiteConfigurationEdit.root.displayed).toBeTruthy();
    });

    it('should display default language', () => {
      expect(VotingSiteConfigurationEdit.defaultLanguage.value).toBe("Français");
    });

    it('default language should be checked and disabled', () => {
      expect(VotingSiteConfigurationEdit.frCheckbox.checked).toBeTruthy();
      expect(VotingSiteConfigurationEdit.frCheckbox.disabled).toBeTruthy();
    });

    it('Other language should not be checked but enabled', () => {
      expect(VotingSiteConfigurationEdit.deCheckbox.checked).toBeFalsy();
      expect(VotingSiteConfigurationEdit.rmCheckbox.checked).toBeFalsy();
      expect(VotingSiteConfigurationEdit.itCheckbox.checked).toBeFalsy();
      expect(VotingSiteConfigurationEdit.deCheckbox.disabled).toBeFalsy();
      expect(VotingSiteConfigurationEdit.rmCheckbox.disabled).toBeFalsy();
      expect(VotingSiteConfigurationEdit.itCheckbox.disabled).toBeFalsy();
    });


    it('should allow user to add a language', () => {
      VotingSiteConfigurationEdit.rmCheckbox.click();
      expect(VotingSiteConfigurationEdit.rmCheckbox.checked).toBeTruthy();
    });

    it('should allow user to change default language', () => {
      VotingSiteConfigurationEdit.defaultLanguage.select("Italien");
      expect(VotingSiteConfigurationEdit.defaultLanguage.value).toBe("Italien");
    });

    it('should set italian as site languages', () => {
      expect(VotingSiteConfigurationEdit.itCheckbox.checked).toBeTruthy();
    });

    it('should disable italian and enable french', () => {
      expect(VotingSiteConfigurationEdit.itCheckbox.disabled).toBeTruthy();
      expect(VotingSiteConfigurationEdit.frCheckbox.disabled).toBeFalsy();
    });

    it('should allow user to save', () => {
      VotingSiteConfigurationEdit.saveButton.click(true);
      expect(OperationManagementPage.votingSiteConfigurationCard.displayed).toBeTruthy()
    });

  });


  describe('after save card status', () => {
    it('should be completed', () => {
      expect(OperationManagementPage.votingSiteConfigurationCard.completeBadgeDisplayed).toBeTruthy();
    });
    it('should display chosen default language', () => {
      expect(OperationManagementPage.votingSiteConfigurationCard.getContentLabel(0)).toBe('Langue par défaut');
      expect(OperationManagementPage.votingSiteConfigurationCard.getContentValue(0)).toBe('Italien');
    });
  });

  describe('edit section after update', () => {

    it('should display the voting site configuration when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.votingSiteConfigurationCard.button.click();
      // then
      expect(VotingSiteConfigurationEdit.root.displayed).toBeTruthy();
    });

    it('should display default language', () => {
      expect(VotingSiteConfigurationEdit.defaultLanguage.value).toBe("Italien");
    });

    it('Should have selected', () => {
      expect(VotingSiteConfigurationEdit.frCheckbox.checked).toBeTruthy();
      expect(VotingSiteConfigurationEdit.deCheckbox.checked).toBeFalsy();
      expect(VotingSiteConfigurationEdit.itCheckbox.checked).toBeTruthy();
      expect(VotingSiteConfigurationEdit.rmCheckbox.checked).toBeTruthy();
    });

    it('Should have Italian disabled', () => {
      expect(VotingSiteConfigurationEdit.frCheckbox.disabled).toBeFalsy();
      expect(VotingSiteConfigurationEdit.deCheckbox.disabled).toBeFalsy();
      expect(VotingSiteConfigurationEdit.itCheckbox.disabled).toBeTruthy();
      expect(VotingSiteConfigurationEdit.rmCheckbox.disabled).toBeFalsy();
    });

    it('should allow user to save a change', () => {
      VotingSiteConfigurationEdit.deCheckbox.click();
      VotingSiteConfigurationEdit.saveButton.click(true);
      expect(OperationManagementPage.votingSiteConfigurationCard.displayed).toBeTruthy()
    });
  });


  describe('form modification detection', () => {

    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.votingSiteConfigurationCard.button.click();
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.votingSiteConfigurationCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.votingSiteConfigurationCard.button.click();
      VotingSiteConfigurationEdit.defaultLanguage.select("Romanche");
      ParameterEditPage.clickBackLink(false);

      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.votingSiteConfigurationCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      ParameterEditPage.clickBackLink(false);
      Main.confirmDialog.accept();
      expect(OperationManagementPage.votingSiteConfigurationCard.present).toBeTruthy();
    });
  });


});

