/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser } from "protractor";
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { MilestoneEdit } from '../../../page-object/operation/parameters/milestone-edit.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { OperationOption } from '../../../shared/mock-server';
import { Main } from '../../../page-object/main.po';

let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Milestones', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_milestone',
      'TEST_e2e_milestone', [OperationOption.ADD_DOMAIN_INFLUENCE]
    );
  });

  describe('milestone edit section', () => {

    it('should display the milestone section when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.milestoneCard.button.click();
      // then
      expect(MilestoneEdit.root.displayed).toBeTruthy();
    });

    it('should display the navigation list and the back link', () => {
      since('back link should be displayed').expect(ParameterEditPage.backLink.isPresent()).toBeTruthy();
      since('navigation list should be displayed').expect(ParameterEditPage.navigationList.present).toBeTruthy();
      since('navigation list should display #{expected} items (#{actual} were found)')
        .expect(ParameterEditPage.navigationList.itemCount).toBe(10);
      since('operation link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(1)).toBe('Opération *');
      since('milestones link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(2)).toBe('Jalons opérationnels *');
      since('domain of influence link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(3)).toBe('Domaines d\'influence *');
      since('repository link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(4)).toBe('Référentiel(s) *');
      since('testing cards link should be displayed and required (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(5)).toBe('Électeurs de test *');
      since('documentation link should be displayed (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(6)).toBe('Documentation');
      since('ballot documentation link should be displayed (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(7)).toBe('Documentation scrutins');
      since('management entities link should be displayed (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(8)).toBe('Entités de gestion');
      since('Voting site link should be displayed (expected "#{expected}", was "#{actual}")')
        .expect(ParameterEditPage.navigationList.itemText(9)).toBe('Site de vote');
    });

    it('card title should be "Jalons opérationnels"', () => {
      expect(MilestoneEdit.root.title).toBe('Jalons opérationnels');
    });

    it('card subtitle should be "Définir les jalons de l\'opération"', () => {
      expect(MilestoneEdit.root.subtitle).toContain('Définir les jalons de l\'opération');
    });

    it('site validation date input should have placeholder "Validation du site internet"', () => {
      expect(MilestoneEdit.milestoneInput('SITE_VALIDATION').placeholder).toBe('Validation du site internet');
    });

    it('certification date input should have placeholder "Certification de l\'opération"', () => {
      expect(MilestoneEdit.milestoneInput('CERTIFICATION').placeholder).toBe('Certification de l\'opération');
    });

    it('printer files date input should have placeholder "Génération des fichiers imprimeurs"', () => {
      expect(MilestoneEdit.milestoneInput('PRINTER_FILES').placeholder).toBe('Génération des fichiers imprimeurs');
    });

    it('ballot box init date input should have placeholder "Initialisation de l\'urne électronique"', () => {
      expect(MilestoneEdit.milestoneInput('BALLOT_BOX_INIT').placeholder)
        .toBe('Initialisation de l\'urne électronique');
    });

    it('site open date input should have placeholder "Ouverture du site de vote"', () => {
      expect(MilestoneEdit.milestoneInput('SITE_OPEN').placeholder).toContain('Ouverture du site de vote');
      since('input should be required').expect(MilestoneEdit.milestoneInput('SITE_OPEN').required).toBeTruthy();
    });

    it('site close date input should have placeholder "Fermeture du site de vote"', () => {
      expect(MilestoneEdit.milestoneInput('SITE_CLOSE').placeholder).toContain('Fermeture du site de vote');
      since('input should be required').expect(MilestoneEdit.milestoneInput('SITE_CLOSE').required).toBeTruthy();
    });

    it('grace period input should have placeholder "Délai de grâce (en minutes)"', () => {
      expect(MilestoneEdit.gracePeriodInput.placeholder).toContain('Délai de grâce (en minutes)');
      since('input should be required').expect(MilestoneEdit.gracePeriodInput.required).toBeTruthy();
    });

    it('ballot box decrypt date input should have placeholder "Extraction de l\'urne chiffrée"', () => {
      expect(MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').placeholder)
        .toContain('Extraction de l\'urne chiffrée');
      since('input should be required').expect(MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').required)
        .toBeTruthy();
    });

    it('result validation date input should have placeholder "Validation des résultats"', () => {
      expect(MilestoneEdit.milestoneInput('RESULT_VALIDATION').placeholder).toBe('Validation des résultats');
    });

    it('data destruction date input should have placeholder "Destruction des données"', () => {
      expect(MilestoneEdit.milestoneInput('DATA_DESTRUCTION').placeholder).toBe('Destruction des données');
    });

    it('[CLEAR] button should be present and disable', () => {
      since('button should be displayed').expect(MilestoneEdit.clearButton.displayed).toBeTruthy();
      since('button should be disable').expect(MilestoneEdit.clearButton.enabled).toBeFalsy();
      expect(MilestoneEdit.clearButton.text).toBe('clear EFFACER');
    });

    it('[SAVE] button should be present and disable', () => {
      since('button should be displayed').expect(MilestoneEdit.saveButton.displayed).toBeTruthy();
      since('button should be disable').expect(MilestoneEdit.saveButton.enabled).toBeFalsy();
      expect(MilestoneEdit.saveButton.text).toBe('ENREGISTRER');
    });
  });

  describe('input milestones', () => {

    it('should display error messages if no input', () => {
      // given
      MilestoneEdit.milestoneInput('SITE_VALIDATION').value = '12.06.2057';
      // when
      MilestoneEdit.saveButton.click();
      // then
      since('site open date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('SITE_OPEN').error).toBe('Champ obligatoire');
      since('site close date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('SITE_CLOSE').error).toBe('Champ obligatoire');
      since('grace period input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.gracePeriodInput.error).toBe('Champ obligatoire');
      since('ballot box date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').error).toBe('Champ obligatoire');
    });

    it('should display error message if incorrect milestones entered', () => {
      // given
      MilestoneEdit.milestoneInput('SITE_VALIDATION').value = '01.06.2017';
      MilestoneEdit.milestoneInput('CERTIFICATION').value = '01.06.2017';
      MilestoneEdit.milestoneInput('PRINTER_FILES').value = '01.06.2017';
      MilestoneEdit.milestoneInput('BALLOT_BOX_INIT').value = '01.06.2017';
      MilestoneEdit.milestoneInput('SITE_OPEN').value = '01.06.2017 09:00';
      MilestoneEdit.milestoneInput('SITE_CLOSE').value = '01.06.2017 09:00';
      MilestoneEdit.gracePeriodInput.value = '61';
      MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').value = '01.06.2017 09:00';
      MilestoneEdit.milestoneInput('RESULT_VALIDATION').value = '01.06.2017';
      MilestoneEdit.milestoneInput('DATA_DESTRUCTION').value = '01.06.2017';
      // when
      MilestoneEdit.saveButton.click();
      // then
      since('site validation date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('SITE_VALIDATION').error)
        .toBe('La date doit être postérieure à la date du jour.');
      since('certification date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('CERTIFICATION').error)
        .toBe('La date doit être postérieure à la date du jour.');
      since('printer files date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('PRINTER_FILES').error)
        .toBe('La date doit être postérieure à la date du jour.');
      since('ballot box init date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('BALLOT_BOX_INIT').error)
        .toBe('La date doit être postérieure à la date du jour.');
      since('site open date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('SITE_OPEN').error)
        .toBe('La date doit être postérieure à la date du jour.');
      since('site close date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('SITE_CLOSE').error)
        .toBe('La date doit être postérieure à la date du jour.');
      since('grace period input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.gracePeriodInput.error).toBe('La valeur doit être comprise entre 0 et 60');
      since('ballot box date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').error)
        .toBe('La date doit être postérieure à la date du jour.');
      since('result validation date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('RESULT_VALIDATION').error)
        .toBe('La date doit être postérieure à la date du jour.');
      since('data destruction date input should display error "#{expected}" (was "#{actual}")')
        .expect(MilestoneEdit.milestoneInput('DATA_DESTRUCTION').error)
        .toBe('La date doit être postérieure à la date du jour.');
    });

    it('should display error message if milestones entered are not compatible with operation date', () => {
      // given
      MilestoneEdit.milestoneInput('SITE_VALIDATION').value = '12.06.2057';
      MilestoneEdit.milestoneInput('CERTIFICATION').value = '12.06.2057';
      MilestoneEdit.milestoneInput('PRINTER_FILES').value = '12.06.2057';
      MilestoneEdit.milestoneInput('BALLOT_BOX_INIT').value = '12.06.2057';
      MilestoneEdit.milestoneInput('SITE_OPEN').value = '12.06.2057 09:00';
      MilestoneEdit.milestoneInput('SITE_CLOSE').value = '12.06.2057 09:00';
      MilestoneEdit.gracePeriodInput.value = '9';
      MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').value = '12.06.2057 09:00';
      MilestoneEdit.milestoneInput('RESULT_VALIDATION').value = '12.06.2057';
      MilestoneEdit.milestoneInput('DATA_DESTRUCTION').value = '12.06.2057';
      // when
      MilestoneEdit.saveButton.click();
      // then
      browser.controlFlow().execute(() => {
        since('site validation date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('SITE_VALIDATION').error)
          .toBe('Ce champ doit être antérieur à la date de l\'opération.');
        since('certification date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('CERTIFICATION').error)
          .toBe('Ce champ doit être antérieur à la date de l\'opération.');
        since('printer files date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('PRINTER_FILES').error)
          .toBe('Ce champ doit être antérieur à la date de l\'opération.');
        since('ballot box init date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('BALLOT_BOX_INIT').error)
          .toBe('Ce champ doit être antérieur à la date de l\'opération.');
        since('site open date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('SITE_OPEN').error)
          .toBe('Ce champ doit être antérieur à la date de l\'opération.');
        since('site close date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('SITE_CLOSE').error)
          .toBe('La date de fermeture du site de vote doit être postérieure à la date d\'ouverture du site de vote.');
        since('ballot box date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').error)
          .toBe('Ce champ doit être antérieur ou égal à la date de l\'opération.');
        since('result validation date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('RESULT_VALIDATION').error).toBe(
          'La date de la validation des résultats doit être postérieure ou égal à la date d\'extraction de l\'urne chiffrée.');
        since('data destruction date input should display error "#{expected}" (was "#{actual}")')
          .expect(MilestoneEdit.milestoneInput('DATA_DESTRUCTION').error)
          .toBe('Ce champ doit être postérieur à la date de l\'opération.');
      });
    });

    it('should update operation\'s milestones when they are correct after clicking on [SAVE] button', () => {
      // given
      MilestoneEdit.milestoneInput('SITE_VALIDATION').value = '01.06.2057';
      MilestoneEdit.milestoneInput('CERTIFICATION').value = '02.06.2057';
      MilestoneEdit.milestoneInput('PRINTER_FILES').value = '03.06.2057';
      MilestoneEdit.milestoneInput('BALLOT_BOX_INIT').value = '04.06.2057';
      MilestoneEdit.milestoneInput('SITE_OPEN').value = '05.06.2057 12:01';
      MilestoneEdit.milestoneInput('SITE_CLOSE').value = '06.06.2057 09:00';
      MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').value = '08.06.2057 10:00';
      MilestoneEdit.milestoneInput('RESULT_VALIDATION').value = '01.07.2057';
      MilestoneEdit.milestoneInput('DATA_DESTRUCTION').value = '02.07.2058';
      MilestoneEdit.gracePeriodInput.value = '9';
      // when
      MilestoneEdit.saveButton.click(true);
      OperationManagementPage.milestoneCard.button.click();
      // then
      expect(MilestoneEdit.clearButton.enabled).toBeTruthy();
    });
  });

  describe('milestone summary card', () => {

    it('title should be "Jalons opérationnels *"', () => {
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.milestoneCard.title).toBe('Jalons opérationnels *');
    });

    it('subtitle should be "Principaux jalons de l\'opération"', () => {
      expect(OperationManagementPage.milestoneCard.subtitle).toBe('Principaux jalons de l\'opération');
    });

    it('content should display the site opening date', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.milestoneCard.getContentLabel(0)).toBe('Ouverture du site de vote');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.milestoneCard.getContentValue(0)).toBe('05.06.2057 12:01');
    });

    it('content should display the site closing date', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.milestoneCard.getContentLabel(1)).toBe('Fermeture du site de vote');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.milestoneCard.getContentValue(1)).toBe('06.06.2057 09:00');
    });

    it('content should display the grace period', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.milestoneCard.getContentLabel(2)).toBe('Délai de grâce (en minutes)');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.milestoneCard.getContentValue(2)).toBe('9');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.milestoneCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.milestoneCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('clear milestones', () => {

    it('should open a confirmation dialog', () => {
      // when
      OperationManagementPage.milestoneCard.button.click();
      MilestoneEdit.clearButton.click();
      // then
      since('the confirmation dialog should be displayed').expect(Main.confirmDialog.content.isDisplayed())
        .toBeTruthy();
      expect(Main.confirmDialog.content.getText()).toBe('Souhaitez-vous supprimer tous les jalons ?');
    });

    it('cancelling action should close the dialog', () => {
      // when
      Main.confirmDialog.cancel();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
    });

    it('accepting action should delete the milestones', () => {
      // when
      MilestoneEdit.clearButton.click();
      Main.confirmDialog.accept();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
    });

    it('no configuration should be displayed', () => {
      since('no configuration should be displayed').expect(OperationManagementPage.milestoneCard.noConfigDisplayed)
        .toBeTruthy();
    });

    it('completed badge should not be present', () => {
      expect(OperationManagementPage.milestoneCard.completeBadgeDisplayed).toBeFalsy();
    });
  });

  describe('form modification detection', () => {

    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.milestoneCard.button.click();
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.milestoneCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.milestoneCard.button.click();
      MilestoneEdit.gracePeriodInput.value = "10";
      ParameterEditPage.clickBackLink(false);

      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.milestoneCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      ParameterEditPage.clickBackLink(false);
      Main.confirmDialog.accept();

      // last test, reset operation with milestones
      Main.updateOperation([OperationOption.ADD_MILESTONE]);
      expect(OperationManagementPage.milestoneCard.present).toBeTruthy();
    });
  });

});

