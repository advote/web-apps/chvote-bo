/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by } from "protractor";
import { OperationOption } from "../../../shared/mock-server";
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { DocumentationEdit } from '../../../page-object/operation/parameters/documentation-edit.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { Main } from '../../../page-object/main.po';

let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Documentation', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_documentation',
      'TEST_e2e_documentation',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION
      ]
    );
  });

  describe('documentation card before edit', () => {
    it('documentation card should be displayed', () => {
      expect(OperationManagementPage.documentationCard.button.present).toBeTruthy();
    });

    it('documentation card should have no document', () => {
      expect(OperationManagementPage.documentationCard.content.element(by.id('document-faq-count')).getText()).toBe('Absent');
      expect(OperationManagementPage.documentationCard.content.element(by.id('document-terms-count')).getText()).toBe('Absent');
      expect(OperationManagementPage.documentationCard.content.element(by.id('document-certificate-count')).getText()).toBe('Absent');
      expect(OperationManagementPage.documentationCard.content.element(by.id('question-count')).getText()).toBe('Absent');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.documentationCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('documentation edit section', () => {
    it('should display the documentation section when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.documentationCard.button.click();
      // then
      expect(DocumentationEdit.root.displayed).toBeTruthy();
    });

    it('card title should be "Documentation"', () => {
      expect(DocumentationEdit.root.title).toBe('Documentation');
    });

    it('card subtitle should be "Liste des documents de l\'opération"', () => {
      expect(DocumentationEdit.root.subtitle).toContain('Liste des documents de l\'opération');
    });

    it('type select should have placeholder "Type documentation"', () => {
      expect(DocumentationEdit.typeSelect.placeholder).toBe('Type documentation *');
      since('input should be required').expect(DocumentationEdit.typeSelect.required).toBeTruthy();
    });

    it('language select should have placeholder "Choix de la langue"', () => {
      expect(DocumentationEdit.languageSelect.placeholder).toBe('Choix de la langue *');
      since('input should be required').expect(DocumentationEdit.languageSelect.required).toBeTruthy();
    });

    it('file input should have placeholder "Sélectionner un document"', () => {
      expect(DocumentationEdit.uploadInput.fileInput.placeholder).toBe('Sélectionner un document');
      since('input should be required').expect(DocumentationEdit.uploadInput.fileInput.required).toBeTruthy();
    });

    it('[BROWSE] button should be present', () => {
      since('button should be displayed').expect(DocumentationEdit.uploadInput.browseButton.displayed).toBeTruthy();
      expect(DocumentationEdit.uploadInput.browseButton.text).toBe('attach_file PARCOURIR');
    });

    it('[UPLOAD] button should be present and disabled', () => {
      since('button should be displayed').expect(DocumentationEdit.uploadInput.uploadButton.displayed).toBeTruthy();
      since('button should be disable').expect(DocumentationEdit.uploadInput.uploadButton.enabled).toBeFalsy();
      expect(DocumentationEdit.uploadInput.uploadButton.text).toBe('file_upload IMPORTER');
    });
  });

  describe('import a documentation', () => {
    it('should display an error if the type or language is not specified', () => {
      // given
      DocumentationEdit.uploadInput.file = '../resources/parameters/valid-domain-influence.xml';
      // when
      DocumentationEdit.uploadInput.uploadButton.click();
      // then
      since('type input should be in error').expect(DocumentationEdit.typeSelect.error).toBe('Champ obligatoire');
      since('language input should be in error').expect(DocumentationEdit.languageSelect.error).toBe('Champ obligatoire');
    });

    it('should display an error if the file is not a PDF file', () => {
      // given
      DocumentationEdit.typeSelect.select('FAQ');
      DocumentationEdit.languageSelect.select('Français');
      DocumentationEdit.uploadInput.file = '../resources/parameters/valid-domain-influence.xml';
      // when
      DocumentationEdit.uploadInput.uploadButton.click();
      // then
      expect(DocumentationEdit.uploadInput.fileInput.error).toBe('Le fichier sélectionné n\'est pas un fichier PDF');
    });

    it('should display the result grid if the file is valid', () => {
      // given
      DocumentationEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      // when
      DocumentationEdit.uploadInput.uploadButton.click();
      // then
      expect(DocumentationEdit.table.displayed).toBeTruthy();
    });

    it('document table should display 4 columns...', () => {
      expect(DocumentationEdit.table.headerValue('type')).toBe('Type de document');
      expect(DocumentationEdit.table.headerValue('language')).toBe('Langue');
      expect(DocumentationEdit.table.headerValue('file')).toBe('Nom du fichier chargé');
      expect(DocumentationEdit.table.headerValue('delete')).toBe('Supprimer');
    });

    it('... And one row', () => {
      since('table should contain one row').expect(DocumentationEdit.table.rowCount).toBe(1);
      expect(DocumentationEdit.table.cellValue(0, 'type')).toBe('FAQ');
      expect(DocumentationEdit.table.cellValue(0, 'language')).toBe('Français');
      expect(DocumentationEdit.table.cellValue(0, 'file')).toBe('test-file-1.pdf');
      expect(DocumentationEdit.table.cellValue(0, 'delete')).toBe('clear');
    });

    it('should add a newly added document to the grid', () => {
      // given
      DocumentationEdit.typeSelect.select('Conditions d\'utilisation');
      DocumentationEdit.languageSelect.select('Allemand');
      DocumentationEdit.uploadInput.file = '../resources/parameters/test-file-1.pdf';
      // when
      DocumentationEdit.uploadInput.uploadButton.click();
      // then
      since('table should contain two row').expect(DocumentationEdit.table.rowCount).toBe(2);

      expect(DocumentationEdit.table.cellValue(0, 'type')).toBe('Conditions d\'utilisation');
      expect(DocumentationEdit.table.cellValue(0, 'language')).toBe('Allemand');
      expect(DocumentationEdit.table.cellValue(0, 'file')).toBe('test-file-1.pdf');
      expect(DocumentationEdit.table.cellValue(0, 'delete')).toBe('clear');

      expect(DocumentationEdit.table.cellValue(1, 'type')).toBe('FAQ');
      expect(DocumentationEdit.table.cellValue(1, 'language')).toBe('Français');
      expect(DocumentationEdit.table.cellValue(1, 'file')).toBe('test-file-1.pdf');
      expect(DocumentationEdit.table.cellValue(1, 'delete')).toBe('clear');
    });

    it('should be possible to import same-type document only if in different language', () => {
      // when
      DocumentationEdit.typeSelect.select('Conditions d\'utilisation');
      // then
      since('same language for same-type document should not be available').expect(DocumentationEdit.languageSelect.hasOption('Allemand')).toBeFalsy();
      since('other language for same-type document should be available').expect(DocumentationEdit.languageSelect.hasOption('Français')).toBeTruthy();
    });

    it('should be possible to import same-language document only if of different type', () => {
      // when
      DocumentationEdit.languageSelect.select('Français');
      // then
      since('same type for same-language document should not be available').expect(DocumentationEdit.typeSelect.hasOption('FAQ')).toBeFalsy();
      since('other type for same-language document should be available').expect(DocumentationEdit.typeSelect.hasOption('Document de certificat')).toBeTruthy();
    });
  });

  describe('delete a documentation', () => {

    it('should open a confirmation dialog when clicking on the [DELETE] button', () => {
      // when
      DocumentationEdit.getDeleteButton(0).click();
      // then
      since('the confirmation dialog should be displayed').expect(Main.confirmDialog.content.isDisplayed()).toBeTruthy();
      expect(Main.confirmDialog.content.getText()).toBe('Souhaitez-vous supprimer le document test-file-1.pdf ?');
    });

    it('cancelling action should close the dialog', () => {
      // when
      Main.confirmDialog.cancel();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
    });

    it('accepting action should remove the document file', () => {
      // when
      DocumentationEdit.getDeleteButton(0).click();
      Main.confirmDialog.accept();
      // then
      expect(Main.confirmDialog.content.isPresent()).toBeFalsy();
      since('table should contain one row').expect(DocumentationEdit.table.rowCount).toBe(1);
      expect(DocumentationEdit.table.cellValue(0, 'type')).toBe('FAQ');
      expect(DocumentationEdit.table.cellValue(0, 'language')).toBe('Français');
      expect(DocumentationEdit.table.cellValue(0, 'file')).toBe('test-file-1.pdf');
      expect(DocumentationEdit.table.cellValue(0, 'delete')).toBe('clear');
    });

    it('can create document until having a complete status', () => {
      DocumentationEdit.typeSelect.select('Conditions d\'utilisation');
      DocumentationEdit.languageSelect.select('Allemand');
      DocumentationEdit.uploadInput.file = '../resources/parameters/test-file-2.pdf';
      DocumentationEdit.uploadInput.uploadButton.click();

      DocumentationEdit.typeSelect.select('Document de certificat');
      DocumentationEdit.languageSelect.select('Allemand');
      DocumentationEdit.uploadInput.file = '../resources/parameters/test-file-3.pdf';
      DocumentationEdit.uploadInput.uploadButton.click();
      since('table should contain three rows').expect(DocumentationEdit.table.rowCount).toBe(3);
    });
  });

  describe('documentation summary card', () => {
    it('title should be "Documentation"', () => {
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.documentationCard.title).toBe('Documentation');
    });

    it('subtitle should be "Documentation de l\'opération et Questions à mettre en avant"', () => {
      expect(OperationManagementPage.documentationCard.subtitle).toBe('Documentation de l\'opération et Questions à mettre en avant');
    });

    it('content should display the number of documents by type', () => {
      since('first type label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.documentationCard.getContentLabel(0)).toBe('FAQ');
      since('first type value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.documentationCard.getContentValue(0)).toBe('1 langue');
      since('second type label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.documentationCard.getContentLabel(1)).toBe('Questions');
      since('second type value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.documentationCard.getContentValue(1)).toBe('Absent');
      since('third type label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.documentationCard.getContentLabel(2)).toBe('Conditions d\'utilisation');
      since('third type value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.documentationCard.getContentValue(2)).toBe('1 langue');
      since('fourth type label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.documentationCard.getContentLabel(3)).toBe('Document de certificat');
      since('fourth type value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.documentationCard.getContentValue(3)).toBe('1 langue');
    });

    it('[CONFIGURE] button should be displayed', () => {
      expect(OperationManagementPage.documentationCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.documentationCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('form modification detection', () => {

    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.documentationCard.button.click();
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.documentationCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.documentationCard.button.click();
      DocumentationEdit.uploadInput.file = '../resources/parameters/test-file-3.pdf';
      ParameterEditPage.clickBackLink(false);

      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.documentationCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      ParameterEditPage.clickBackLink(false);
      Main.confirmDialog.accept();
      expect(OperationManagementPage.documentationCard.present).toBeTruthy();
      OperationManagementPage.documentationCard.button.click();
    });
  })

});
