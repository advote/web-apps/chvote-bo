/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { TestingCardsList } from '../../../page-object/operation/testing-cards/testing-cards-list.po';
import { OperationOption } from '../../../shared/mock-server';
import { Main } from '../../../page-object/main.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';

describe('Operation/Parameters/Testing cards/delete', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters/edit/testing-card-lot',
      'test_testing_cards',
      'TEST_e2e_testing_cards',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_TESTING_CARDS
      ]
    );
  });

  it('should open a confirmation dialog when clicking on delete icon', () => {
    TestingCardsList.getDeleteButton(0).click();
    Main.confirmDialog.waitDisplayed();
  });

  it('should not delete a lot when clicking on [CANCEL]', () => {
    Main.confirmDialog.cancel();
    expect(TestingCardsList.table.rowCount).toBe(1);

  });

  it('should delete a lot when clicking on [OK]', () => {
    TestingCardsList.getDeleteButton(0).click();
    Main.confirmDialog.accept();
    expect(TestingCardsList.table.rowCount).toEqual(0);

    // reset testing cards deletion
    Main.updateOperation([OperationOption.ADD_TESTING_CARDS]);
    ParameterEditPage.clickBackLink();
  });

});
