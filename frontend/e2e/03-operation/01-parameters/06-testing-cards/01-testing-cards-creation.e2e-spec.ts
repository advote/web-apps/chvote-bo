/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { TestingCardsList } from '../../../page-object/operation/testing-cards/testing-cards-list.po';
import { TestingCardsEdit } from '../../../page-object/operation/testing-cards/testing-cards-edit.po';
import { OperationOption } from '../../../shared/mock-server';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { ParameterEditPage } from '../../../page-object/operation/parameters/parameter-edit.po';
import { RepositoryEdit } from '../../../page-object/operation/parameters/repository-edit.po';
import { ConsistencyCheck } from '../../../page-object/consistency-check.po';
import { Main } from '../../../page-object/main.po';
import { browser, protractor } from 'protractor';
let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Testing cards/creation', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters',
      'test_testing_cards',
      'TEST_e2e_testing_cards',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT
      ]
    );
  });

  describe('testing cards list section', () => {
    it('should display the testing cards section when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.testingCardsLotCard.button.click();
      // then
      expect(TestingCardsList.root.displayed).toBeTruthy();
    });

    it('card title should be "Lot de cartes de test et de contrôle"', () => {
      expect(TestingCardsList.root.title).toBe('Lot de cartes de test et de contrôle');
    });

    it('card subtitle should be "Liste des lots de cartes de test et de contrôle"', () => {
      expect(TestingCardsList.root.subtitle).toContain('Liste des lots de cartes de test et de contrôle');
    });

    it('[CREATE LOT] button should be present', () => {
      since('button should be displayed').expect(TestingCardsList.createButton.displayed).toBeTruthy();
      expect(TestingCardsList.createButton.text).toBe('add DÉFINIR UN LOT DE CARTES');
    });
  });

  describe('create a lot', () => {
    it('should open creation page when clicking on the [CREATE LOT] button', () => {
      TestingCardsList.createButton.click();
      expect(TestingCardsEdit.root.displayed).toBeTruthy();
    });

    it('should disable change of lot type and be set to "Cartes de test"', () => {
      since('the field should be disable').expect(TestingCardsEdit.cardType.enabled).toBeFalsy();
      expect(TestingCardsEdit.cardType.value).toBe('Cartes de test');
    });

    it('should disable change of printing mode and be not printable', () => {
      since('the field should be disable').expect(TestingCardsEdit.shouldPrint.disabled).toBeTruthy();
      since('the field should be checked').expect(TestingCardsEdit.shouldPrint.checked).toBeFalsy();
    });

    it('should display errors when not correctly filled', () => {
      TestingCardsEdit.saveButton.click();
      expect(TestingCardsEdit.lotName.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.count.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.signature.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.firstName.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.lastName.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.birthday.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.street.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.postalCode.error).toContain('Champ obligatoire');
      expect(TestingCardsEdit.city.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.country.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.doi.error).toBe('Champ obligatoire');
      expect(TestingCardsEdit.language.error).toBe('Champ obligatoire');
    });

    it('should be saved when correctly filled', () => {
      TestingCardsEdit.lotName.value = 'New lot';
      TestingCardsEdit.count.value = '1000';
      TestingCardsEdit.signature.select('M.');
      TestingCardsEdit.firstName.value = 'firstMame';
      TestingCardsEdit.lastName.value = 'lastName';
      TestingCardsEdit.birthday.value = '25.08.1980';
      TestingCardsEdit.address.value = 'address1';
      TestingCardsEdit.street.value = 'street';
      TestingCardsEdit.postalCode.value = '00000';
      TestingCardsEdit.city.value = 'city';
      TestingCardsEdit.country.value = 'country';
      TestingCardsEdit.doi.select('CH / Confédération', true);
      TestingCardsEdit.language.select('FR');
      TestingCardsEdit.saveButton.click(true);

      since('grid should contains 1 row').expect(TestingCardsList.table.rowCount).toBe(1);
      expect(TestingCardsList.table.cellValue(0, 'lotName')).toBe('New lot');
    });
  });

  describe('inconsistency error', () => {
    it('should prevent creation if there is no repository', () => {
      ParameterEditPage.clickBackLink();
      OperationManagementPage.repositoryCard.button.click();
      RepositoryEdit.getDeleteButton(1).click();
      Main.confirmDialog.accept();
      RepositoryEdit.getDeleteButton(0).click();
      Main.confirmDialog.accept();
      ParameterEditPage.clickBackLink();
      OperationManagementPage.testingCardsLotCard.button.click();
      since('[CREATE LOT] button should be hidden').expect(TestingCardsList.createButton.present).toBeFalsy();
      since('[EDIT] button should be hidden').expect(TestingCardsList.getEditButton(0).present).toBeFalsy()
    });

    it('should display an inconsistency error', () => {
      since('consistency error should be displayed').expect(ConsistencyCheck.hasError).toBeTruthy();
      expect(ConsistencyCheck.error(0)).toBe(
        'Le domaine d\'influence "CH" configuré pour le lot de cartes de test "New lot" n\'est pas présent dans le référentiel de l\'opération');
    });

    it('tab "parameters" should be in error', () => {
      expect(OperationManagementPage.getNavigationTabIcon(0)).toBe('warning');
    });

    it('"Domain of influence" and "Testing cards" menu should be in error', () => {
      since('"Domain of influence" should be in error').expect(ParameterEditPage.navigationList.inErrorBadgeDisplayed(3)).toBeTruthy();
      since('"Testing cards" should be in error').expect(ParameterEditPage.navigationList.inErrorBadgeDisplayed(5)).toBeTruthy();

      // reset repository deletion
      Main.updateOperation([OperationOption.ADD_REPOSITORY_VOTATION, OperationOption.ADD_REPOSITORY_ELECTION]);
      Main.refresh();
    });
  });

});
