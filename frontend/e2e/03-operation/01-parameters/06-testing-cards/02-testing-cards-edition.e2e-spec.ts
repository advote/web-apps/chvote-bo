/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { TestingCardsList } from "../../../page-object/operation/testing-cards/testing-cards-list.po";
import { TestingCardsEdit } from "../../../page-object/operation/testing-cards/testing-cards-edit.po";
import { ParameterEditPage } from "../../../page-object/operation/parameters/parameter-edit.po";
import { OperationOption } from '../../../shared/mock-server';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { Main } from '../../../page-object/main.po';

let since = require('jasmine2-custom-message');

describe('Operation/Parameters/Testing cards/edition', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]/parameters/edit/testing-card-lot',
      'test_testing_cards',
      'TEST_e2e_testing_cards',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_TESTING_CARDS
      ]
    );
  });

  describe('perform edition', () => {
    it('should open edition page when clicking on the [EDIT] button', () => {
      TestingCardsList.getEditButton(0).click();
      expect(TestingCardsEdit.root.displayed).toBeTruthy();
    });

    it('should disable change of lot type and be set to "Cartes de test"', () => {
      since('the field should be disable').expect(TestingCardsEdit.cardType.enabled).toBeFalsy();
      expect(TestingCardsEdit.cardType.value).toBe('Cartes de test');
    });

    it('should disable change of printing mode and be not printable', () => {
      since('the field should be disable').expect(TestingCardsEdit.shouldPrint.disabled).toBeTruthy();
      since('the field should be checked').expect(TestingCardsEdit.shouldPrint.checked).toBeFalsy();
    });

    it('should not allow special characters in lot name', () => {
      TestingCardsEdit.lotName.value = '#this should fail';
      TestingCardsEdit.saveButton.click();
      expect(TestingCardsEdit.lotName.error).toBe('Caractères spéciaux interdits');
    });

    it('should allow edition of lots', () => {
      TestingCardsEdit.lotName.value = 'Card for testing 2';
      TestingCardsEdit.saveButton.click(true);
      expect(TestingCardsList.table.cellValue(0, 'lotName')).toBe('Card for testing 2');
    });
  });

  describe('summary card', () => {

    it('title should be "Électeurs de test *"', () => {
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.testingCardsLotCard.title).toBe('Électeurs de test *');
    });

    it('subtitle should be "Définir les cartes de test"', () => {
      expect(OperationManagementPage.testingCardsLotCard.subtitle).toBe('Définir les cartes de test');
    });

    it('content should display the lot\'s count', () => {
      since('label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.testingCardsLotCard.getContentLabel(0)).toBe('Nombre de lot de carte');
      since('value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.testingCardsLotCard.getContentValue(0)).toBe('1');
    });

    it('content should display the voting material status', () => {
      since('label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.testingCardsLotCard.getContentLabel(1)).toBe('Matériel de test');
      since('value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.testingCardsLotCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.testingCardsLotCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('form modification detection', () => {

    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.testingCardsLotCard.button.click();
      TestingCardsList.getEditButton(0).click();
      ParameterEditPage.clickBackLink();
      expect(OperationManagementPage.testingCardsLotCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.testingCardsLotCard.button.click();
      TestingCardsList.getEditButton(0).click();
      TestingCardsEdit.lotName.value = 'Card for testing modified';
      ParameterEditPage.clickBackLink(false);

      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.testingCardsLotCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      ParameterEditPage.clickBackLink(false);
      Main.confirmDialog.accept();
      expect(OperationManagementPage.testingCardsLotCard.present).toBeTruthy();
      OperationManagementPage.testingCardsLotCard.button.click();
    });
  });

});
