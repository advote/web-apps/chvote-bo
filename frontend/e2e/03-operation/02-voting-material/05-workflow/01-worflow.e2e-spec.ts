/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser } from 'protractor';
import { OperationOption, votingMaterial } from '../../../shared/mock-server';
import {
  DeploymentSummarySection, OperationManagementPage, VotingMaterialNotificationSection
} from '../../../page-object/operation/operation-management.po';
import { DashboardPage } from '../../../page-object/dashboard/dashboard.po';
import { VotingMaterialCreationProgress } from '../../../page-object/operation/voting-material/voting-material-creation-progress.po';
import { Main } from '../../../page-object/main.po';
import { ConsistencyCheck } from '../../../page-object/consistency-check.po';
import { RegisterEdit } from '../../../page-object/operation/voting-material/register/register-edit.po';
import { RegisterSummary } from '../../../page-object/operation/voting-material/register/register-summary.po';
import { VotingMaterialEditPage } from '../../../page-object/operation/voting-material/voting-material-edit.po';
import { CardTitleEdit } from '../../../page-object/operation/voting-material/card-title-edit.po';
import { PrinterTemplateEdit } from '../../../page-object/operation/voting-material/printer-template-edit.po';
import { OperationListSection } from '../../../page-object/dashboard/operation-list-section.po';
import { waitForVisibility } from '../../../shared/e2e.utils';

let since = require('jasmine2-custom-message');

describe('Operation/Voting material/Workflow', () => {

  function expectCardsAreInReadonly(isReadOnly: boolean): void {
    expect(OperationManagementPage.cardTitleCard.readOnly).toBe(isReadOnly);
    expect(OperationManagementPage.registerCard.readOnly).toBe(isReadOnly);
    expect(OperationManagementPage.printerTemplateCard.readOnly).toBe(isReadOnly);
    expect(OperationManagementPage.testingCardsLotCard.readOnly).toBe(isReadOnly);
  }

  function doAChangeAndRedeploy(): void {
    expect(VotingMaterialNotificationSection.deploymentButton.present).toBeFalsy();
    OperationManagementPage.cardTitleCard.button.click();
    CardTitleEdit.titleInput.value = 'Change done';
    Main.screenshot('Changing title');
    CardTitleEdit.saveButton.click(true);
    ConsistencyCheck.waitConsistencyCheck();
    DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Configuration du matériel de vote définie le.*/);
    browser.refresh();
    VotingMaterialNotificationSection.deploymentButton.click();
    DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Configuration du matériel de vote envoyé.*/)
  }

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-material",
      'test-voting-material-workflow',
      'TEST_e2e_voting_material_workflow',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_REGISTER_SE,
        OperationOption.ADD_REGISTER_SR,
        OperationOption.ADD_CARD_TITLE,
        OperationOption.ADD_PRINTER_TEMPLATE,
        OperationOption.FOR_SIMULATION
      ]
    );
  });

  describe('complete', () => {
    it('should display "Configuration du matériel de vote définie le..."', () => {
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Configuration du matériel de vote définie le.*/);
    });

    it('should not be in read-only mode', () => {
      expectCardsAreInReadonly(false);
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('should wait consistency to get computed', () => {
      ConsistencyCheck.waitConsistencyCheck();
    });

    it('should allow user to send voting material', () => {
      expect(VotingMaterialNotificationSection.deploymentButton.enabled).toBeTruthy();
    });
  });

  describe('send voting material configuration to PACT', () => {

    it('should have status "Configuration du matériel de vote envoyé..."', () => {
      Main.refresh();
      VotingMaterialNotificationSection.deploymentButton.click();
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Configuration du matériel de vote envoyé.*/)
    });

    it('should hide deployment button and show a link to PACT in order to request voting material creation', () => {
      since('[DEPLOY] button should be hidden').expect(VotingMaterialNotificationSection.deploymentButton.present)
        .toBeFalsy();
      since('<create> link should be visible').expect(VotingMaterialNotificationSection.createLink.isPresent())
        .toBeTruthy();
    });

    it('should switch to read-only mode', () => {
      expectCardsAreInReadonly(true);
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('should set register in read-only mode', () => {
      OperationManagementPage.registerCard.button.click();
      Main.screenshot('inside edit component');
      since('[UPLOAD] button should be hidden').expect(RegisterEdit.uploadButton.present).toBeFalsy();
      since('[DELETE] button in table should be hidden').expect(RegisterSummary.deleteButton(0).present).toBeFalsy();
      VotingMaterialEditPage.clickBackLink();
    });

    it('should set voting card title in read-only mode', () => {
      OperationManagementPage.cardTitleCard.button.click();
      Main.screenshot('inside edit component');
      since('[SAVE] button should be hidden').expect(CardTitleEdit.saveButton.present).toBeFalsy();
      since('title input should be disable').expect(CardTitleEdit.titleInput.enabled).toBeFalsy();
      VotingMaterialEditPage.clickBackLink();
    });

    it('should set printer template select page in read-only mode', () => {
      OperationManagementPage.printerTemplateCard.button.click();
      Main.screenshot('inside edit component');
      since('[SAVE] button should be hidden').expect(PrinterTemplateEdit.saveButton.present).toBeFalsy();
      since('template select should be disable').expect(PrinterTemplateEdit.templateSelect.enabled).toBeFalsy();
      VotingMaterialEditPage.clickBackLink();
    });
  });

  describe('user request voting material creation in PACT', () => {
    it('should have status "Création de matériel de vote demandée" when a user request a creation', () => {
      votingMaterial.requestVotingMaterialCreation(Main.operationId);
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Création de matériel de vote demandée.*/)
    });

    it('should show a link to PACT in order to validate the voting material creation', () => {
      expect(VotingMaterialNotificationSection.validateCreationLink.isDisplayed()).toBeTruthy();
    });

    it('should stay in read-only mode', () => {
      expectCardsAreInReadonly(true);
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });
  });

  describe('user refuse voting material creation in PACT', () => {
    it(
      'should have status "Création du matériel de vote refusée" when a user in PACT has refused voting material deployment',
      () => {
        votingMaterial.rejectVotingMaterialCreation(Main.operationId);
        DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Création du matériel de vote refusée.*/)
      });

    it('should switch to read-write mode', () => {
      expectCardsAreInReadonly(false);
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('should allow redeployment after a change has been done', () => {
      doAChangeAndRedeploy();
    });
  });

  describe('voting material creation failure', () => {
    it('should have status "Échec de la création du matériel de vote" when a creation of voting material has failed',
      () => {
        votingMaterial.setVotingMaterialCreationToFailed(Main.operationId);
        DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Échec de la création du matériel de vote.*/)
      });

    it('should switch to read-write mode', () => {
      expectCardsAreInReadonly(false);
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('should allow redeployment directly', () => {
      expect(VotingMaterialNotificationSection.deploymentButton.enabled).toBeTruthy();
      VotingMaterialNotificationSection.deploymentButton.click();
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Configuration du matériel de vote envoyé.*/);
      expectCardsAreInReadonly(true);
    });
  });

  describe('voting material creation in progress', () => {
    it('should have status "Matériel de vote en cours de création" when a voting material creation is in progress',
      () => {
        votingMaterial.setVotingMaterialCreationToInProgress(Main.operationId, 'ONE_STEP_DONE');
        DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Matériel de vote en cours de création*/);
        expectCardsAreInReadonly(true);
      });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('should open progress dialog when user click on the status', () => {
      DeploymentSummarySection.productionStatusMessage.click();
      expect(VotingMaterialCreationProgress.root.isDisplayed()).toBeTruthy();
    });

    it('should have first step completed', () => {
      VotingMaterialCreationProgress.initializeParametersPanel.open();
      expect(VotingMaterialCreationProgress.initializeParametersPanel.progressInPercent).toBe('100%');
      expect(VotingMaterialCreationProgress.initializeParametersPanel.stepStucks).toBeFalsy();
      expect(VotingMaterialCreationProgress.initializeParametersPanel.getControlComponentProgress(-1)).toBe('100%');
      expect(VotingMaterialCreationProgress.initializeParametersPanel.isControlComponentStuck(-1)).toBeFalsy();
    });

    it('should have second step in progress', () => {
      VotingMaterialCreationProgress.sendVotersPanel.open();
      expect(VotingMaterialCreationProgress.sendVotersPanel.progressInPercent).toBe('30%');
      expect(VotingMaterialCreationProgress.sendVotersPanel.stepStucks).toBeFalsy();
      expect(VotingMaterialCreationProgress.sendVotersPanel.getControlComponentProgress(0)).toBe('50%');
      expect(VotingMaterialCreationProgress.sendVotersPanel.isControlComponentStuck(0)).toBeFalsy();
      expect(VotingMaterialCreationProgress.sendVotersPanel.getControlComponentProgress(1)).toBe('30%');
      expect(VotingMaterialCreationProgress.sendVotersPanel.isControlComponentStuck(1)).toBeFalsy();
    });

    it('should have third step not started', () => {
      expect(VotingMaterialCreationProgress.requestPrivateCredentialsPanel.progressInPercent).toBe('0%');
      expect(VotingMaterialCreationProgress.requestPrivateCredentialsPanel.disabled).toBeTruthy();
    });

    it('should be stuck after some time', () => {
      votingMaterial.setVotingMaterialCreationToInProgress(Main.operationId, 'STUCK');
      browser.refresh();
      waitForVisibility(DeploymentSummarySection.productionStatusMessage);
      DeploymentSummarySection.productionStatusMessage.click();

      VotingMaterialCreationProgress.sendVotersPanel.open();
      expect(VotingMaterialCreationProgress.sendVotersPanel.progressInPercent).toBe('10%');
      expect(VotingMaterialCreationProgress.sendVotersPanel.stepStucks).toBeTruthy();
      expect(VotingMaterialCreationProgress.sendVotersPanel.getControlComponentProgress(0)).toBe('60%');
      expect(VotingMaterialCreationProgress.sendVotersPanel.isControlComponentStuck(0)).toBeFalsy();
      expect(VotingMaterialCreationProgress.sendVotersPanel.getControlComponentProgress(1)).toBe('10%');
      expect(VotingMaterialCreationProgress.sendVotersPanel.isControlComponentStuck(1)).toBeTruthy();
    });
  });

  describe('voting material created', () => {
    it('should have status "Matériel de vote créé" when the voting material creation has successfully ended', () => {
      votingMaterial.setVotingMaterialToCreated(Main.operationId);
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Matériel de vote créé*/);
      expectCardsAreInReadonly(true);
    });

    it('should have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('disponible');
    });

    it('should allow an invited printer to download printer files', () => {
      Main.login('prn4');
      expect(DashboardPage.createButton.present).toBeFalsy();
      since('column "date" should be present').expect(OperationListSection.operationsGrid.header('date').isPresent())
        .toBeTruthy();
      since('column "shortLabel" should be present')
        .expect(OperationListSection.operationsGrid.header('shortLabel').isPresent()).toBeTruthy();
      since('column "longLabel" should be present')
        .expect(OperationListSection.operationsGrid.header('longLabel').isPresent()).toBeTruthy();
      since('column "printerArchiveInfo" should be present')
        .expect(OperationListSection.operationsGrid.header('printerArchiveInfo').isPresent()).toBeTruthy();
      since('column "printerArchiveDownload" should be present')
        .expect(OperationListSection.operationsGrid.header('printerArchiveDownload').isPresent()).toBeTruthy();
      since('column "management" should be present')
        .expect(OperationListSection.operationsGrid.header('management').isPresent()).toBeFalsy();
      expect(OperationListSection.operationsGrid.rowCount).toBe(1);
    });

    it('should not allow a not invited printer to download printer files', () => {
      Main.login('prn3');
      expect(DashboardPage.createButton.present).toBeFalsy();
      expect(OperationListSection.operationsGrid.rowCount).toBe(0);
    });

    it('should have [TEST VOTING CARDS] button available', () => {
      Main.login('ge1');
      Main.navigateTo('/operations/[OPERATION_ID]/voting-material');
      waitForVisibility(DeploymentSummarySection.testingVotingCardButton.root);
    });
  });

  describe('user request an invalidation in PACT', () => {
    it('should have status "Matériel de vote créé. Une demande d\'invalidation à été émise"', () => {
      votingMaterial.requestInvalidationOfVotingMaterial(Main.operationId);
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(
        /Matériel de vote créé. Une demande d'invalidation a été émise.*/);
      expectCardsAreInReadonly(true);
    });

    it('should have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('disponible');
    });
  });

  describe('user reject an invalidation in PACT', () => {
    it("should have status 'La demande d'invalidation a été refusée'", () => {
      votingMaterial.rejectInvalidationOfVotingMaterial(Main.operationId);
      browser.debugger();
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(
        /La demande d'invalidation a été refusée.*/);
      expectCardsAreInReadonly(true);
    });

    it('should have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('disponible');
    });
  });

  describe('user confirm an invalidation in PACT', () => {
    it('should have status "Matériel de vote invalidé"', () => {
      votingMaterial.invalidateVotingMaterial(Main.operationId);
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Matériel de vote invalidé le.*/)
    });

    it('should switch to read-write mode', () => {
      expectCardsAreInReadonly(false);
    });

    it('should not have voting material available', () => {
      expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('Should allow user to submit voting material', () => {
      expect(VotingMaterialNotificationSection.deploymentButton.displayed).toBeTruthy();
    })
  });

  describe('user validate voting material', () => {
    it('Voting period should not be open meanwhile voting material is not validated"', () => {
      votingMaterial.setVotingMaterialToCreated(Main.operationId);
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Matériel de vote créé.*/);
      expect(OperationManagementPage.isNavigationTabDisplayed(2)).toBeFalsy();
    });

    it('should have a confirm dialog when user want to validate"', () => {
      VotingMaterialNotificationSection.validateVotingMaterialButton.click();
      Main.confirmDialog.waitDisplayed();
    });

    it('When user cancel nothing happens and user can reask for validate"', () => {
      Main.confirmDialog.cancel();
      expect(VotingMaterialNotificationSection.validateVotingMaterialButton.displayed).toBeTruthy();
    });

    it("When user accept status should be 'Matériel de vote validé'", () => {
      VotingMaterialNotificationSection.validateVotingMaterialButton.click();
      Main.confirmDialog.accept();
      DeploymentSummarySection.waitForProductionStatusMessageToMatch(/Matériel de vote validé.*/);
    });

    it("Should open voting period", () => {
      expect(OperationManagementPage.isNavigationTabDisplayed(2)).toBeTruthy();
    });

  });


});
