/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from "../../../shared/mock-server";
import { Main } from '../../../page-object/main.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { CardTitleEdit } from '../../../page-object/operation/voting-material/card-title-edit.po';
import { VotingMaterialEditPage } from '../../../page-object/operation/voting-material/voting-material-edit.po';

let since = require('jasmine2-custom-message');

describe('Operation/Voting material/Card title', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-material",
      'test_card_title',
      'TEST_e2e_card_title',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_REGISTER_SE,
        OperationOption.ADD_REGISTER_SR,
        OperationOption.FOR_SIMULATION,
      ]
    );
  });

  describe('card title edit section', () => {

    it('should display the card title edit section when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.cardTitleCard.button.click();
      // then
      expect(CardTitleEdit.root.displayed).toBeTruthy();
    });

    it('card title should be "Libellé à imprimer sur la carte de vote"', () => {
      expect(CardTitleEdit.root.title).toBe('Libellé à imprimer sur la carte de vote');
    });

    it('title input should have placeholder "Libellé imprimé sur la carte de vote"', () => {
      expect(CardTitleEdit.titleInput.placeholder).toBe('Libellé imprimé sur la carte de vote');
      since('input should be required').expect(CardTitleEdit.titleInput.required).toBeTruthy();
    });

    it('[SAVE] button should be present', () => {
      since('button should be displayed').expect(CardTitleEdit.saveButton.displayed).toBeTruthy();
      expect(CardTitleEdit.saveButton.text).toBe('ENREGISTRER');
    });

    it('should display the navigation list and the back link', () => {
      since('back link should be displayed').expect(VotingMaterialEditPage.backLink.isDisplayed()).toBeTruthy();
      since('navigation list should be displayed').expect(VotingMaterialEditPage.navigationList.present).toBeTruthy();
      since('navigation list should display #{expected} items (#{actual} were found)').expect(VotingMaterialEditPage.navigationList.itemCount).toBe(5);
      since('register link should be displayed and required (expected "#{expected}", was "#{actual}")').expect(VotingMaterialEditPage.navigationList.itemText(1)).toBe('Registre électoral *');
      since('voting card link should be displayed and required (expected "#{expected}", was "#{actual}")').expect(VotingMaterialEditPage.navigationList.itemText(2)).toBe('Carte de vote *');
      since('printer configuration link should be displayed and required (expected "#{expected}", was "#{actual}")').expect(VotingMaterialEditPage.navigationList.itemText(3)).toBe('Configuration imprimeur *');
      since('testing material link should be displayed and required (expected "#{expected}", was "#{actual}")').expect(VotingMaterialEditPage.navigationList.itemText(4)).toBe('Matériel de test *');
    });
  });

  describe('save the card title', () => {
    it('should save the card title and switch to the operation management', () => {
      CardTitleEdit.titleInput.value = 'test title for card';
      CardTitleEdit.saveButton.click(true);
      expect(OperationManagementPage.cardTitleCard.displayed).toBeTruthy();
    });
  });

  describe('card title summary card', () => {

    it('title should be "Carte de vote *"', () => {
      expect(OperationManagementPage.cardTitleCard.title).toBe('Carte de vote *');
    });

    it('subtitle should be "Libellé à imprimer sur la carte de vote"', () => {
      expect(OperationManagementPage.cardTitleCard.subtitle).toBe('Libellé à imprimer sur la carte de vote');
    });

    it('content should display the card title', () => {
      since('label should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.cardTitleCard.getContentLabel(0)).toBe('Libellé');
      since('value should be "#{expected}" (was "#{actual}")').expect(OperationManagementPage.cardTitleCard.getContentValue(0)).toBe('test title for card');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.cardTitleCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.cardTitleCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('form modification detection', () => {

    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.cardTitleCard.button.click();
      VotingMaterialEditPage.clickBackLink();
      expect(OperationManagementPage.cardTitleCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.cardTitleCard.button.click();
      CardTitleEdit.titleInput.value = 'NEW test title for card';
      VotingMaterialEditPage.clickBackLink(false);

      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.cardTitleCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      VotingMaterialEditPage.clickBackLink(false);
      Main.confirmDialog.accept();
      expect(OperationManagementPage.cardTitleCard.present).toBeTruthy();
    });
  });


});
