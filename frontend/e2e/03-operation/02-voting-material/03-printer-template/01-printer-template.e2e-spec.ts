/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from "../../../shared/mock-server";
import { Main } from '../../../page-object/main.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { PrinterTemplateEdit } from '../../../page-object/operation/voting-material/printer-template-edit.po';
import { VotingMaterialEditPage } from '../../../page-object/operation/voting-material/voting-material-edit.po';
let since = require('jasmine2-custom-message');

describe('Operation/Voting material/Printer template', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-material",
      'test_printer_template',
      'TEST_e2e_printer_template',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_REGISTER_SE,
        OperationOption.ADD_REGISTER_SR,
        OperationOption.ADD_CARD_TITLE,
        OperationOption.FOR_SIMULATION,
      ]
    );
  });

  describe('printer template edit section', () => {
    it('should display the printer template edit section when clicking on the [CONFIGURE] button', () => {
      // when
      OperationManagementPage.printerTemplateCard.button.click();
      // then
      expect(PrinterTemplateEdit.root.displayed).toBeTruthy();
    });

    it('card title should be "Définir le modèle de configuration imprimeur"', () => {
      expect(PrinterTemplateEdit.root.title).toBe('Définir le modèle de configuration imprimeur');
    });

    it('template select input should have placeholder "Sélectionner un modèle"', () => {
      expect(PrinterTemplateEdit.templateSelect.placeholder).toBe('Sélectionner un modèle');
    });

    it('[SAVE] button should be present', () => {
      since('button should be displayed').expect(PrinterTemplateEdit.saveButton.displayed).toBeTruthy();
      expect(PrinterTemplateEdit.saveButton.text).toBe('ENREGISTRER');
    });
  });

  describe('save the printer template', () => {
    it('should save the printer template and switch to the operation management', () => {
      PrinterTemplateEdit.templateSelect.select('template 3');
      // TODO: test the display of template's information
      PrinterTemplateEdit.saveButton.click(true);
      expect(OperationManagementPage.printerTemplateCard.displayed).toBeTruthy();
    });
  });

  describe('printer template summary card', () => {
    it('title should be "Configuration imprimeur *"', () => {
      expect(OperationManagementPage.printerTemplateCard.title).toBe('Configuration imprimeur *');
    });

    it('subtitle should be "Modèle de configuration imprimeur"', () => {
      expect(OperationManagementPage.printerTemplateCard.subtitle).toBe('Modèle de configuration imprimeur');
    });

    it('content should display the printer template', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.printerTemplateCard.getContentLabel(0)).toBe('Modèle sélectionné');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.printerTemplateCard.getContentValue(0)).toBe('template 3');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.printerTemplateCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.printerTemplateCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('form modification detection', () => {
    it('should be able to enter edit mode and quit without any warning if no change has been made', () => {
      OperationManagementPage.printerTemplateCard.button.click();
      VotingMaterialEditPage.clickBackLink();
      expect(OperationManagementPage.printerTemplateCard.present).toBeTruthy();
    });

    it('should not quit the current screen when user try to quit with unsaved changes and doesn\'t confirm', () => {
      OperationManagementPage.printerTemplateCard.button.click();
      PrinterTemplateEdit.templateSelect.select('template 2');
      VotingMaterialEditPage.clickBackLink(false);

      expect(Main.confirmDialog.content.getText()).toBe('Voulez vous vraiment interrompre l\'action en cours ?');
      Main.confirmDialog.cancel();
      expect(OperationManagementPage.printerTemplateCard.present).toBeFalsy();
    });

    it('should quit the current screen when user try to quit with unsaved changes and confirm', () => {
      VotingMaterialEditPage.clickBackLink(false);
      Main.confirmDialog.accept();
      expect(OperationManagementPage.printerTemplateCard.present).toBeTruthy();
    });
  });
});
