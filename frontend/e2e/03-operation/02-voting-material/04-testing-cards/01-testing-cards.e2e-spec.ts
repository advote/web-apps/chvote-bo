/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { OperationOption } from '../../../shared/mock-server';
import { Main } from '../../../page-object/main.po';
import { OperationManagementPage } from '../../../page-object/operation/operation-management.po';
import { TestingCardsList } from '../../../page-object/operation/testing-cards/testing-cards-list.po';
import { TestingCardsEdit } from '../../../page-object/operation/testing-cards/testing-cards-edit.po';
import { VotingMaterialEditPage } from '../../../page-object/operation/voting-material/voting-material-edit.po';

let since = require('jasmine2-custom-message');

describe('Operation/Voting material/Testing cards', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-material",
      'test_testing_cards',
      'TEST_e2e_testing_cards',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_REGISTER_SE,
        OperationOption.ADD_REGISTER_SR,
        OperationOption.ADD_CARD_TITLE,
        OperationOption.ADD_PRINTER_TEMPLATE,
        OperationOption.FOR_SIMULATION
      ]
    );
  });

  describe('testing cards summary card before edit', () => {

    it('title should be "Matériel de test *"', () => {
      expect(OperationManagementPage.testingCardsLotCard.title).toBe('Matériel de test *');
    });

    it('subtitle should be "Définir le matériel de test"', () => {
      expect(OperationManagementPage.testingCardsLotCard.subtitle).toBe('Définir le matériel de test');
    });

    it('content should display the lot\'s count', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.testingCardsLotCard.getContentLabel(0)).toBe('Nombre de lot de carte');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.testingCardsLotCard.getContentValue(0)).toBe('1');
    });

    it('content should display the voting material status', () => {
      since('label should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.testingCardsLotCard.getContentLabel(1)).toBe('Matériel de test');
      since('value should be "#{expected}" (was "#{actual}")')
        .expect(OperationManagementPage.testingCardsLotCard.getContentValue(1)).toBe('indisponible');
    });

    it('[CONFIGURE] button should be present', () => {
      expect(OperationManagementPage.testingCardsLotCard.buttonLabel).toBe('CONFIGURER');
    });

    it('completed badge should be present', () => {
      expect(OperationManagementPage.testingCardsLotCard.completeBadgeDisplayed).toBeTruthy();
    });
  });

  describe('testing cards edit section', () => {
    it('should display the testing card edit section when clicking on the [CONFIGURE] button', () => {
      OperationManagementPage.testingCardsLotCard.button.click();
      expect(TestingCardsList.root.displayed).toBeTruthy();
    });

    it('card title should be "Lot de cartes de test et de contrôle"', () => {
      expect(TestingCardsList.root.title).toBe('Lot de cartes de test et de contrôle');
    });

    it('card subtitle should be "Liste des lots de cartes de test et de contrôle"', () => {
      expect(TestingCardsList.root.subtitle).toBe('Liste des lots de cartes de test et de contrôle');
    });

    it('[CREATE LOT] button should be present', () => {
      since('button should be displayed').expect(TestingCardsList.createButton.displayed).toBeTruthy();
      expect(TestingCardsList.createButton.text).toBe('add DÉFINIR UN LOT DE CARTES');
    });

    it('should have 1 row in the list of configured lot', () => {
      expect(TestingCardsList.table.rowCount).toBe(1);
    });
  });

  describe('testing cards edition', () => {
    it('should enable change of lot type', () => {
      TestingCardsList.getEditButton(0).click();
      expect(TestingCardsEdit.cardType.enabled).toBeTruthy();
    });

    it('should enable change of printing mode', () => {
      TestingCardsEdit.cardType.select('Cartes de test');
      expect(TestingCardsEdit.shouldPrint.disabled).toBeFalsy();
    });

    it('should display technical printer when test card are not to print', () => {
      TestingCardsEdit.shouldPrint.setValue(false);
      expect(TestingCardsEdit.printerName).toBe('Imprimeur virtuel');
    });

    it('should display selected printer in template when test card are to print', () => {
      TestingCardsEdit.shouldPrint.setValue(true);
      expect(TestingCardsEdit.printerName).toBe('Printer1');
    });

    it(
      'should disable change of printing mode if switch to "controller testing card" type and set printing mode to enabled',
      () => {
        TestingCardsEdit.cardType.select('Cartes de contrôleur');
        since('printing mode should be disable').expect(TestingCardsEdit.shouldPrint.disabled).toBeTruthy();
        since('printing mode should be true').expect(TestingCardsEdit.shouldPrint.checked).toBeTruthy();
      });

    it('should display selected printer in template in printer area', () => {
      expect(TestingCardsEdit.printerName).toBe('Printer1');
    });

    it(
      'should disable change of printing mode if switch to "printer testing card" type and set printing mode to enabled',
      () => {
        TestingCardsEdit.cardType.select('Cartes de test imprimeur');
        since('printing mode should be disable').expect(TestingCardsEdit.shouldPrint.disabled).toBeTruthy();
        since('printing mode should be true').expect(TestingCardsEdit.shouldPrint.checked).toBeTruthy();
      });

    it('should display "all printers" in printer area', () => {
      expect(TestingCardsEdit.printerName).toBe('Tous les imprimeurs');
    });

    it('should allow edition of lots', () => {
      TestingCardsEdit.lotName.value = 'Card modified';
      Main.screenshot('testing cards lot edition');
      TestingCardsEdit.saveButton.click(true);
    });

    it('lot name should have been modified', () => {
      expect(TestingCardsList.table.cellValue(0, 'lotName')).toBe('Card modified');
      VotingMaterialEditPage.clickBackLink();
    });
  });

});
