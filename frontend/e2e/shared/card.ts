/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, element, ElementFinder } from "protractor";
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate an angular card component.
 */
export class Card {

  card: ElementFinder;

  constructor(idOrTagName: string,
              byId: boolean = true,
              root: ElementFinder = null) {
    this.card = byId ? element(by.id(idOrTagName)) : root.element(by.tagName(idOrTagName));
  }

  /**
   * @returns {ElementFinder} the card's element itself
   */
  get element(): ElementFinder {
    return this.card;
  }

  /**
   * @returns true if the card is present in the DOM, false otherwise
   */
  get present(): promise.Promise<boolean> {
    return this.card.isPresent();
  }

  /**
   * @returns true if the card is displayed, false otherwise
   */
  get displayed(): promise.Promise<boolean> {
    return this.card.isDisplayed();
  }

  /**
   * @returns the card's title
   */
  get title(): promise.Promise<string> {
    return this.card.element(by.className('mat-card-title')).getText();
  }

  /**
   * @returns the card's subtitle
   */
  get subtitle(): promise.Promise<string> {
    return this.card.element(by.className('mat-card-subtitle')).getText();
  }

  /**
   * @returns the card's content element
   */
  get content(): ElementFinder {
    return this.card.element(by.className('mat-card-content'));
  }

  /**
   * @returns the card's actions element
   */
  get actions(): ElementFinder {
    return this.card.element(by.className('mat-card-actions'));
  }
}
