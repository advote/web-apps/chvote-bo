/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from "protractor";
import { InputField } from './input-field';
import * as PATH from "path";
import { Button } from './button';

/**
 * Object used to manipulate an upload input component.
 */
export class UploadInput {

  constructor(private root: ElementFinder) {
  }

  /**
   * @returns {InputField} the input field
   */
  get fileInput(): InputField {
    return new InputField(this.root, 'uploadFile');
  }

  /**
   * Sets the file to upload.
   *
   * @param path the path to the file to upload
   */
  set file(path: string) {
    this.root.element(by.css('input[type="file"]')).sendKeys(PATH.resolve(__dirname, path));
  }

  /**
   * @returns {Button} the [BROWSE] button
   */
  get browseButton(): Button {
    return new Button(this.root.element(by.id('browseButton')), this.root.browser_);
  }

  /**
   * @returns {Button} the [UPLOAD] button
   */
  get uploadButton(): Button {
    return new Button(this.root.element(by.id('uploadButton')), this.root.browser_);
  }
}
