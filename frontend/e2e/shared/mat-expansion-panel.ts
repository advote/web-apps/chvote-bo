/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from "protractor";

export class MatExpansionPanel {

  private root: ElementFinder;

  get disabled() {
    return this.root.element(by.tagName("mat-expansion-panel-header"))
      .getAttribute("aria-disabled")
      .then(value => value === "true");
  }

  constructor(root: ElementFinder) {
    this.root = root;
  }

  click(): void {
    this.root.click();
  }

  get title(): ElementFinder {
    return this.root.element(by.tagName('mat-panel-title'));
  }


  get description(): ElementFinder {
    return this.root.element(by.tagName('mat-panel-description'));
  }

  get content(): ElementFinder {
    return this.root.element(by.className('mat-expansion-panel-content'));
  }

}
