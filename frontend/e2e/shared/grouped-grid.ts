/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from "protractor";
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate a grouped grid.
 */
export class GroupedGrid {

  private grid: ElementFinder;

  /**
   * Create a grouped grid reference based on its parent.
   *
   * @param root the grid's parent
   */
  constructor(root: ElementFinder) {
    this.grid = root.element(by.className('grouped-grid'));
  }

  /**
   * Get header text of the given column.
   *
   * @param columnIndex index of the column
   * @returns the corresponding header's text
   */
  headerValue(columnIndex: number): promise.Promise<string> {
    return this.grid.all(by.tagName('th')).get(columnIndex).getText();
  };

  /**
   * @returns the number of groups
   */
  get groupCount(): promise.Promise<number> {
    return this.grid.all(by.tagName('tbody')).count();
  }

  /**
   * Get the group element at the given index.
   *
   * @param index the group index
   * @returns the corresponding group element
   */
  group(index: number): ElementFinder {
    return this.grid.all(by.tagName('tbody')).get(index);
  }

  /**
   * Get the group name at the given index.
   *
   * @param index the group index
   * @returns the corresponding group name
   */
  groupName(index: number): promise.Promise<string> {
    return this.group(index).element(by.className('grid-group')).element(by.tagName('span')).getText();
  }

  /**
   * Get the number of row in the given group.
   *
   * @param group the group's index
   * @returns the number of associated rows
   */
  rowCount(group: number): promise.Promise<number> {
    return this.group(group).all(by.tagName('tr')).count();
  }

  /**
   * Get the cell value at the given position
   *
   * @param group the group index
   * @param row the row index
   * @param column the column index
   * @returns the value of the corresponding cell
   */
  cellValue(group: number, row: number, column: number): promise.Promise<string> {
    return this.group(group).all(by.tagName('tr')).get(row).all(by.tagName('td')).get(column).getText();
  }
}
