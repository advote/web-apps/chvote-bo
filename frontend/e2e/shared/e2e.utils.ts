/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, ElementFinder, protractor, ProtractorBrowser } from 'protractor';

export function hasClass(element, cls) {
  return element.getAttribute('class').then(function (classes) {
    return classes.split(' ').indexOf(cls) !== -1;
  });
}

export function waitForInvisibility(element: ElementFinder, timeout?: number, b?: ProtractorBrowser) {
  (b || browser).wait(protractor.ExpectedConditions.invisibilityOf(element), timeout || 10000,
    "element not invisible " + element);
}


export function waitForVisibility(element: ElementFinder, timeout ?: number, b?: ProtractorBrowser) {
  (b || browser).wait(protractor.ExpectedConditions.visibilityOf(element), timeout || 10000,
    "element not visible " + element);
}

export function waitForClickable(element: ElementFinder, timeout ?: number, b?: ProtractorBrowser) {
  (b || browser).wait(protractor.ExpectedConditions.elementToBeClickable(element), timeout || 10000,
    "element not clickable " + element);
}


function _deepEqual(expected, actual, currentPath: string, excludePaths: string[]) {
  if (typeof expected == "object") {
    const pathOf = k => currentPath ? currentPath + "." + k : k;
    const notInExclude = k => excludePaths.indexOf(pathOf(k)) < 0;
    let expectedKeys = Object.keys(expected).filter(notInExclude);
    let actualKeys = Object.keys(actual).filter(notInExclude);

    expectedKeys.forEach(k => {
      if (actualKeys.indexOf(k) < 0) {
        throw new Error(pathOf(k) + " is not present in actual")
      }
    });

    actualKeys.forEach(k => {
      if (expectedKeys.indexOf(k) < 0) {
        throw new Error(pathOf(k) + " is not present in expected")
      } else {
        _deepEqual(expected[k], actual[k], pathOf(k), excludePaths);
      }
    });
  } else {
    expect(expected).toEqual(actual);
  }
}

export function deepEqual(expected, actual, excludePath: string[] = []) {
  try {
    _deepEqual(expected, actual, "", excludePath)
  }
  catch (e) {
    fail(`${e}
Expected : 
----------
${JSON.stringify(expected, null, 2)}
Actual : 
----------
${JSON.stringify(actual, null, 2)}\`)
`
    )
  }
}


