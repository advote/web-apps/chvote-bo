/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, protractor } from 'protractor';
import { RequestOptions } from 'http';
import { log } from 'util';

const http = require('http');

function request(path: string) {
  let deferred = protractor.promise.defer();
  let options: RequestOptions = {
    hostname: browser.params.mockServer.hostname,
    port: browser.params.mockServer.port,
    path: path,
    headers: {},
    method: 'GET',
  };

  let callback = function (res) {
    let data = "";
    res.on('data', (chunk) => {
      data += chunk;
    });
    res.on('end', () => {
      deferred.fulfill(data);
    });
  };

  http.request(options, callback).end();

  return deferred.promise;
}

function requestWithCallBack(path: string, onResultCallBack: (any) => any) {
  browser.wait(() => request(path)
    .then(data => {
      try {
        onResultCallBack(data);
      }
      catch (e) {
        return false;
      }
      return true;
    }), 30000);
}

export function resetDB() {
  log('resetting database');
  return request('/mock/database/reset');
}

export function addElectoralAuthorityKeys() {
  log('Add electoral authority keys');
  return request('/mock/database/add-electoral-authority-keys');
}

export enum OperationOption {
  ADD_ALL = "ADD_ALL",
  ADD_MILESTONE = "ADD_MILESTONE",
  ADD_DOMAIN_INFLUENCE = "ADD_DOMAIN_INFLUENCE",
  ADD_REPOSITORY_VOTATION = "ADD_REPOSITORY_VOTATION",
  ADD_REPOSITORY_ELECTION = "ADD_REPOSITORY_ELECTION",
  REMOVE_REPOSITORY_ELECTION = "REMOVE_REPOSITORY_ELECTION",
  ADD_DOCUMENT = "ADD_DOCUMENT",
  ADD_TESTING_CARDS = "ADD_TESTING_CARDS",
  ADD_ELECTION_PAGE_PROPERTIES = "ADD_ELECTION_PAGE_PROPERTIES",
  REMOVE_ELECTION_PAGE_PROPERTIES = "REMOVE_ELECTION_PAGE_PROPERTIES",
  ADD_VOTING_SITE_CONFIGURATION = "ADD_VOTING_SITE_CONFIGURATION",
  ADD_REGISTER_SE = "ADD_REGISTER_SE",
  ADD_REGISTER_SR = "ADD_REGISTER_SR",
  ADD_CARD_TITLE = "ADD_CARD_TITLE",
  ADD_PRINTER_TEMPLATE = "ADD_PRINTER_TEMPLATE",
  ADD_SIMULATION_SITE_PERIOD = "ADD_SIMULATION_SITE_PERIOD",
  REMOVE_BALLOT_DOCUMENTATION = "REMOVE_BALLOT_DOCUMENTATION",
  FOR_REAL = "FOR_REAL",
  FOR_SIMULATION = "FOR_SIMULATION",
  WITH_VOTING_MATERIAL_FINALIZED = "WITH_VOTING_MATERIAL_FINALIZED",
}

export function createOperation(shortName = "ope",
                                longName = "Operation%20under%20test",
                                options: OperationOption[] = []) {
  log(`creating operation "${longName}"`);
  let optionsAsString = options.map(option => "option=" + option).join("&");
  return request(`/mock/database/operation/create/${shortName}/${longName}?${optionsAsString}`);
}

export function updateOperation(operationId,
                                options: OperationOption[] = []) {
  let optionsAsString = options.map(option => "option=" + option).join("&");
  return request(`/mock/database/operation/update/${operationId}?${optionsAsString}`);
}

export const configuration = {
  finishTestDeploymentInPact(operationId) {
    return request(`/mock/pact/operation/${operationId}/configuration/status/finish-test-deployment-in-pact`);
  },
  requestDeploymentInPact(operationId) {
    return request(`/mock/pact/operation/${operationId}/configuration/status/request-deployment-in-pact`);
  },
  putDeploymentInErrorInPact(operationId) {
    return request(`/mock/pact/operation/${operationId}/configuration/status/put-deployment-in-error-in-pact`);
  },
  validateDeploymentRequestInPact(operationId) {
    return request(`/mock/pact/operation/${operationId}/configuration/status/validate-deployment-request-in-pact`);
  },
  refuseDeploymentRequestInPact(operationId) {
    return request(`/mock/pact/operation/${operationId}/configuration/status/refuse-deployment-request-in-pact`);
  },
  resetPactConfiguration() {
    return request('/mock/pact/upload/configuration/reset');
  },
  getLastOperationConfigurationUploadedToPact(onResultCallBack: (any) => any) {
    requestWithCallBack('/mock/pact/upload/configuration/last-sent-operation', onResultCallBack);
  },
  nextConfigurationUploadIsInvalid() {
    return request('/mock/pact/upload/configuration/next-upload-is-invalid');
  },
  getLastConfigurationZipUploadedToPact(onResultCallBack: (any) => any) {
    requestWithCallBack('/mock/pact/upload/configuration/last-sent-zip-list', onResultCallBack);
  },
  addGuestManagementEntity(operationId: number, managementEntity: string) {
    return request(
      `/mock/database/operation/${operationId}/add-guest-management-entity?managementEntity=${encodeURIComponent(
        managementEntity)}`)
  },
  deleteGuestManagementEntity(operationId: number, managementEntity: string) {
    return request(
      `/mock/database/operation/${operationId}/delete-guest-management-entity?managementEntity=${encodeURIComponent(
        managementEntity)}`)
  },
};


export const votingPeriod = {

  addToOperation(operationId) {
    return request(`/mock/database/operation/${operationId}/add-voting-material`)
  },

  invalidateVotingMaterial(operationId) {
    return request(`/mock/pact/operation/${operationId}/invalidate-voting-material`);
  },

  rejectVotingPeriodInitialization(operationId) {
    return request(`/mock/pact/operation/${operationId}/reject-voting-period-initialization`);
  },

  requestVotingPeriodInitialization(operationId) {
    return request(`/mock/pact/operation/${operationId}/request-voting-period-initialization`);
  },

  setVotingPeriodInitializationToFailed(operationId) {
    return request(`/mock/pact/operation/${operationId}/set-voting-period-initialization-to-failed`);
  },

  setVotingPeriodInitializationToInProgress(operationId) {
    return request(`/mock/pact/operation/${operationId}/set-voting-period-initialization-to-in-progress`);
  },

  setVotingPeriodToInitialized(operationId) {
    return request(`/mock/pact/operation/${operationId}/set-voting-period-to-initialized`);
  },

  getLastRequestJson(onResultCallBack: (any) => any) {
    requestWithCallBack('/mock/pact/upload/voting-period/last-sent-operation', onResultCallBack);
  },

  reset() {
    return request(`/mock/pact/upload/voting-period/reset`);
  },

};


export const votingMaterial = {

  addToOperation(operationId) {
    return request(`/mock/database/operation/${operationId}/add-voting-material`)
  },

  invalidateVotingMaterial(operationId) {
    return request(`/mock/pact/operation/${operationId}/invalidate-voting-material`);
  },

  rejectVotingMaterialCreation(operationId) {
    return request(`/mock/pact/operation/${operationId}/reject-voting-material-creation`);
  },

  requestInvalidationOfVotingMaterial(operationId) {
    return request(`/mock/pact/operation/${operationId}/request-invalidation-voting-material`);
  },

  rejectInvalidationOfVotingMaterial(operationId) {
    return request(`/mock/pact/operation/${operationId}/reject-invalidation-voting-material`);
  },

  requestVotingMaterialCreation(operationId) {
    return request(`/mock/pact/operation/${operationId}/request-voting-material-creation`);
  },

  setVotingMaterialCreationToFailed(operationId) {
    return request(`/mock/pact/operation/${operationId}/set-voting-material-creation-to-failed`);
  },

  setVotingMaterialCreationToInProgress(operationId, progressLevel) {
    return request(`/mock/pact/operation/${operationId}/set-voting-material-creation-to-in-progress/` + progressLevel);
  },

  setVotingMaterialToCreated(operationId) {
    return request(`/mock/pact/operation/${operationId}/set-voting-material-to-created`);
  },

  getLastRequestJson() {
    return request(`/mock/pact/upload/voting-materials/last-sent-operation`);
  },

  getLastZipsSent() {
    return request(`/mock/pact/upload/voting-materials/last-sent-zip-list`);
  },

  reset() {
    return request(`/mock/pact/upload/voting-materials/reset`);
  },

};

export const tally = {

  setTallyArchiveCreationToStatus(operationId,
                                  status: 'CREATION_FAILED' | 'NOT_REQUESTED' | 'CREATED' | 'CREATION_IN_PROGRESS' | 'NOT_ENOUGH_VOTES_CAST') {
    return request(`/mock/pact/operation/${operationId}/update-tally-archive-status?state=${status}`)
  },


};

