/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder, ProtractorBrowser } from "protractor";
import { Card } from './card';
import { promise } from 'selenium-webdriver';
import { hasClass } from './e2e.utils';
import { Button } from './button';

/**
 * Object used to manipulate a summary card component.
 */
export class SummaryCard extends Card {

  private parameterCard: ElementFinder;

  constructor(tagName: string, b: ProtractorBrowser = browser) {
    super('completable-card', false, b.element(by.tagName(tagName)));
    this.parameterCard = b.element(by.tagName(tagName));
  }

  /**
   * Get the value of the given content's label.
   *
   * @param index index number of the seek label
   * @return the value of this label
   */
  getContentLabel(index: number): promise.Promise<string> {
    return this.content.all(by.className('readonly-label')).get(index).getText();
  }

  /**
   * Get the value of the given content.
   *
   * @param index index number of the seek content
   * @return the value of this content
   */
  getContentValue(index: number): promise.Promise<string> {
    return this.content.all(by.className('readonly-value')).get(index).getText();
  }

  /**
   * @returns {promise.Promise<boolean>} the presence of the "no configuration" text
   */
  get noConfigDisplayed(): promise.Promise<boolean> {
    return this.content.element(by.className('no-config')).isPresent();
  }

  /**
   * @returns {Button} the edit button
   */
  get button(): Button {
    return new Button(this.actions.element(by.tagName('button')));
  }

  /**
   * @returns {promise.Promise<string>} the edit button's label
   */
  get buttonLabel(): promise.Promise<string> {
    return this.button.root.all(by.tagName('span')).get(0).getText();
  }

  /**
   * @returns {promise.Promise<boolean>} the presence of the "completed" badge
   */
  get completeBadgeDisplayed(): promise.Promise<boolean> {
    return this.content.element(by.className('task-complete')).isPresent();
  }

  /**
   * @returns {promise.Promise<boolean>} the presence of the "in error" badge
   */
  get inErrorBadgeDisplayed(): promise.Promise<boolean> {
    return this.content.element(by.className('task-error')).isPresent();
  }

  /**
   * @returns {promise.Promise<any>} true of the card is in read-only mode
   */
  get readOnly() {
    return hasClass(this.parameterCard.element(by.tagName('mat-card')), 'read-only');
  }
}
