/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder, protractor } from "protractor";
import { promise } from "selenium-webdriver";
import { waitForClickable, waitForVisibility, waitForInvisibility } from './e2e.utils';

/**
 * Object used to manipulate a select field.
 */
export class SelectField {

  private field: ElementFinder;

  constructor(root: ElementFinder, formControlName?: string) {
    if (formControlName) {
      this.field = root.element(by.css(`mat-select[formcontrolname="${formControlName}"]`));
    } else {
      this.field = root;
    }
  }

  /**
   * @returns true if the input field is present in the DOM, false otherwise
   */
  get present(): promise.Promise<boolean> {
    return this.field.isPresent();
  }

  /**
   * @returns {promise.Promise<string>} the input field's placeholder
   */
  get placeholder(): promise.Promise<string> {
    return this.field.element(by.xpath(`ancestor::mat-form-field//label[contains(@class,'mat-form-field-label')]`))
      .getText();
  }

  /**
   * @returns {promise.Promise<string>} the input field's error message
   */
  get error(): promise.Promise<string> {
    return this.field.element(by.xpath('ancestor::mat-form-field//mat-error')).getText();
  }

  /**
   * @returns {promise.Promise<string>} "true" if the field is required, false otherwise
   */
  get required(): promise.Promise<string> {
    return this.field.getAttribute('required');
  }

  /**
   * @returns {promise.Promise<boolean>} true if the field is enabled, false otherwise
   */
  get enabled(): promise.Promise<boolean> {
    return this.field.getAttribute("aria-disabled").then(value => value !== "true");
  }

  /**
   * @returns {promise.Promise<string>} the select field value
   */
  get value(): promise.Promise<String> {
    let value = this.field.element(by.className("mat-select-value-text"));
    return value.isPresent().then(present => present ? value.getText() : null);
  }

  /**
   * Select an option in the dropdown list
   *
   * @param {string} option the text option to select
   * @param {boolean} closeWithEscape if the dropdown should be close with the <ESC> key
   */
  select(option: string, closeWithEscape = false): void {
    waitForClickable(this.field);
    this.field.click();
    waitForVisibility(browser.element(by.css(`.mat-select-panel`)));
    let selectOption = browser.element.all(by.css(`mat-option`))
      .filter(element => element.getText().then(text => text.indexOf(option) >= 0));
    browser.wait(() => selectOption.count().then(count => count > 0), 5000,
      `no such element to select mat-option with text '${option}'`);
    selectOption.first().click();
    if (closeWithEscape) {
      selectOption.first().sendKeys(protractor.Key.ESCAPE);
    }
    waitForInvisibility(browser.element(by.css(`.mat-select-panel`)));
  }

  /**
   * @param {string} option the text option to select
   * @returns {promise.Promise<boolean>} whether the select field has the given option or not
   */
  hasOption(option: string): promise.Promise<boolean> {
    this.field.click();
    waitForVisibility(browser.element(by.css(`.mat-select-panel`)));
    let allOptions = browser.element.all(by.css(`mat-option`));
    let isPresent = allOptions.filter(element => element.getText().then(text => text.indexOf(option) >= 0)).isPresent();
    this.field.sendKeys(protractor.Key.ESCAPE);
    waitForInvisibility(browser.element(by.css(`.mat-select-panel`)));
    return isPresent;
  }

}
