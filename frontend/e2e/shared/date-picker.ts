/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from "protractor";
import { InputField } from './input-field';
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate a date picker field.
 */
export class DatePicker extends InputField {

  /**
   * Create a new date picker field reference
   *
   * @param {ElementFinder} container the root element containing the input field
   * @param {string} id the field's unique id
   * @param {boolean} byFormControlName whether the field is identified by its from control name or directly its ID
   */
  constructor(container: ElementFinder,
              id: string,
              byFormControlName: boolean = true) {
    super(container, id, byFormControlName);
  }

  /**
   * @returns the date picker field's placeholder
   */
  get placeholder(): promise.Promise<string> {
    return this.field.element(by.xpath(`ancestor::mat-form-field//label[contains(@class,'mat-form-field-label')]`)).getText();
  }
}
