/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from 'protractor';
import { hasClass } from './e2e.utils';
import { promise } from 'selenium-webdriver';

/**
 * Object used to manipulate a slide toggle field.
 */
export class SlideToggle {

  private field: ElementFinder;
  private selected: boolean = false;

  constructor(root: ElementFinder, formControlName: string) {
    this.field = root.element(by.css(`mat-slide-toggle[formcontrolname="${formControlName}"]`));
    this.checked.then(checked => {
      this.selected = checked;
    });
  }

  /**
   * @returns true if the field is disable, false otherwise
   */
  get disabled() {
    return hasClass(this.field, 'mat-disabled');
  }

  /**
   * @returns {promise.Promise<boolean>} whether the field is checked or not
   */
  get checked(): promise.Promise<boolean> {
    return this.field.element(by.tagName('input')).isSelected();
  }

  /**
   * Sets this field's value
   *
   * @param {boolean} value the new value
   */
  setValue(value: boolean) {
    if (value !== this.selected) {
      this.click();
      this.selected = value;
    }
  }

  /**
   * Performs a click on the field
   */
  click() {
    this.field.click();
  }
}
