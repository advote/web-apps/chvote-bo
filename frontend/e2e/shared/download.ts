/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser } from 'protractor';

function returnContent(callback) {
  let reader = new FileReader();
  reader.addEventListener("loadend", () => callback(reader.result));
  reader.readAsText((<any>window).lastDownloadedContent);
}

function returnFileName(callback) {
  return callback((<any>window).lastDownloadedFileName);
}

export function lastDownloadedContent() {
  browser.sleep(100);
  return browser.executeAsyncScript(returnContent);
}


export function lastDownloadedFileName() {
  return browser.executeAsyncScript(returnFileName);

}

