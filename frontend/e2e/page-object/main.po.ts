/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ProtractorBrowser } from 'protractor';
import { log } from 'util';
import {
  addElectoralAuthorityKeys, configuration, createOperation, OperationOption, resetDB, updateOperation
} from '../shared/mock-server';
import { ConfirmDialog } from '../shared/confirm-dialog';
import { DeploymentSummarySection } from './operation/operation-management.po';
import { BusinessErrorDialog } from '../shared/business-error-dialog';
import { promise } from 'selenium-webdriver';
import controlFlow = promise.controlFlow;

/**
 * Main page object containing utility functions used by all pages.
 */
export class Main {

  public static connectedUser: string = undefined;
  public static operationId: number = undefined;
  public static invitedManagementEntities: string[] = [];
  public static confirmDialog: ConfirmDialog = new ConfirmDialog();
  public static businessErrorDialog: BusinessErrorDialog = new BusinessErrorDialog();

  private static takeScreenshot = require('../pmsr/protractor-multi-screenshots-reporter.js').takeScreenshot;

  /**
   * Refresh the browser page
   */
  static refresh(): void {
    browser.refresh();

  }

  /**
   * Navigate to the given page
   *
   * @param path {string} the destination path
   * @param {ProtractorBrowser} b the browser instance (default to current Protractor Browser)
   * @return {promise.Promise<boolean>} is true if the url has been actually changed
   */
  static navigateTo(path: string, b: ProtractorBrowser = browser): void {
    browser.controlFlow().execute(() => {
      let dest = path.replace('[OPERATION_ID]', <any>this.operationId);
      log(`navigate to ${dest}`);
      b.get(dest);
    })
    browser.waitForAngular()
  }

  /**
   * Performs the login operation if necessary.
   *
   * @param {string} user the user to log (default to "ge1")
   * @param {boolean} showLoginInfo if the login popup should be displayed (default to true)
   * @param {ProtractorBrowser} b the browser instance (default to current Protractor Browser)
   */
  static login(user: string = 'ge1', showLoginInfo: boolean = false, b: ProtractorBrowser = browser): void {
    if (user !== this.connectedUser || b !== browser) {
      this.connectedUser = user;
      b.get('/login')
        .then(() => {
          log(`login as ${user}`);
          browser.executeScript('localStorage.setItem("_e2e", "true")');
          b.element(by.css('input[name="username"]')).sendKeys(user)
        })
        .then(() => b.element(by.css('input[name="password"]')).sendKeys('pass'))
        .then(() => b.element(by.id('loginForm')).submit())
        .then(() => {
          if (showLoginInfo) {
            b.element(by.id('loginMenuButton')).click();
          }
        });
    }
  }

  /**
   * create an operation and navigate to the given path
   *
   * @param {string} path the target path
   * @param {string} shortName the operation's short name
   * @param {string} longName the operation's long name
   * @param {OperationOption[]} options the operation's creation options
   */
  static createOperationAndNavigate(path: string,
                                    shortName = 'test_e2e',
                                    longName = 'TEST_e2e_operation_under_test',
                                    options: OperationOption[] = [
                                      OperationOption.ADD_MILESTONE,
                                      OperationOption.ADD_DOMAIN_INFLUENCE,
                                      OperationOption.ADD_REPOSITORY_VOTATION,
                                      OperationOption.ADD_REPOSITORY_ELECTION,
                                      OperationOption.ADD_DOCUMENT,
                                      OperationOption.ADD_TESTING_CARDS,
                                      OperationOption.ADD_ELECTION_PAGE_PROPERTIES,
                                      OperationOption.ADD_REGISTER_SE,
                                      OperationOption.ADD_REGISTER_SR,
                                      OperationOption.ADD_CARD_TITLE,
                                      OperationOption.ADD_PRINTER_TEMPLATE
                                    ]): void {
    this.invitedManagementEntities = [];
    browser.controlFlow().execute(() => resetDB())
      .then(() => createOperation(shortName, longName, options))
      .then(data => {
        this.operationId = <number> data;
        this.navigateTo(path);
      })
  }


  static inviteManagementEntity(managementEntity: string): void {
    if (this.invitedManagementEntities.indexOf(managementEntity) === -1) {
      browser.controlFlow().execute(() => {
        log(`invite management entity "${managementEntity}"`);
        configuration.addGuestManagementEntity(this.operationId, managementEntity);
      });
    }
  }

  static revokeManagementEntity(managementEntity: string): void {
    if (this.invitedManagementEntities.indexOf(managementEntity) !== -1) {
      browser.controlFlow().execute(() => {
        log(`revoke management entity "${managementEntity}"`);
        configuration.deleteGuestManagementEntity(this.operationId, managementEntity);
      });
    }
  }

  /**
   * Update an operation directly in the backend
   *
   * @param {OperationOption[]} options the operation update options
   */
  static updateOperation(options: OperationOption[] = []): void {
    browser.controlFlow().execute(() => {
      if (this.operationId !== undefined) {
        log(`Update operation ${this.operationId} : ${options.join(", ")}`);
        updateOperation(this.operationId, options)
      }
    });

  }

  /**
   * Take a screenshot
   *
   * @param {string} label the screenshot's label
   * @param {ProtractorBrowser} element the element or browser take we should take a screenshot on (default to current
   *     Protractor Browser)
   */
  static screenshot(label: string, element: { takeScreenshot: () => any } = browser): void {
    this.takeScreenshot(label, element);
  }

  /**
   * Deploy the current operation directly in simulation target
   *
   * @param {string} targetLogin the target login after deployment is done
   * @param {boolean} toReal should be deploy to real or simulation
   */
  static deployConfiguration(targetLogin: string = 'ge1', toReal = false): void {
    this.login(targetLogin);
    this.navigateTo('operations/[OPERATION_ID]/voting-material');
    DeploymentSummarySection.productionCard.present.then(present => {
      if (!present) {
        log('deploy configuration to simulation target');
        this.login('ge1');
        this.navigateTo('operations/[OPERATION_ID]/voting-material');
        browser.controlFlow().execute(() => configuration.validateDeploymentRequestInPact(this.operationId));
        DeploymentSummarySection.waitForDeploymentStatusMessageToMatch(/Déploiement validé le .*/);

        if (toReal) {
          DeploymentSummarySection.targetRealDeploymentButton.click();
          DeploymentSummarySection.waitForDeploymentStatusMessageToMatch(/Opération réelle/);
        } else {
          DeploymentSummarySection.targetSimulationDeploymentButton.click();
          DeploymentSummarySection.simulationNameInput.value = 'test simulation';
          DeploymentSummarySection.simulationNameValidationButton.click();
          DeploymentSummarySection.waitForDeploymentStatusMessageToMatch(/Simulation "test simulation"/);
        }
        this.login(targetLogin);
        this.navigateTo('operations/[OPERATION_ID]/voting-material');
      }
    });
  }

  static tryForceStatusRefresh() {
    controlFlow().execute(() => {
      log("Try to force status refresh");
      browser.executeScript("if (document._forceStatusRefresh) {document._forceStatusRefresh()}")
    })

  }

  static addElectoralAuthorityKeys() {
    browser.controlFlow().execute(() => addElectoralAuthorityKeys());
  }
}
