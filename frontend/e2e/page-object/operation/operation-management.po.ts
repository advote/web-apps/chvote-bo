/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, element, ElementFinder } from 'protractor';
import { promise } from 'selenium-webdriver';
import { SummaryCard } from '../../shared/completable-card';
import { Cache } from '../cache.po';
import { Card } from '../../shared/card';
import { falseIfMissing } from 'protractor/built/util';
import { Button } from '../../shared/button';
import { InputField } from '../../shared/input-field';
import { Main } from '../main.po';

/**
 * Page object for the operation management page.
 */
export class OperationManagementPage {

  /**
   * @returns {promise.Promise<string>} the operation management page title
   */
  static get title(): promise.Promise<string> {
    return element(by.id('operationManagementHeaderTitle')).getText();
  }

  /**
   * Check if the given navigation tab is displayed
   *
   * @param {number} index index of the targetted tab
   * @returns {promise.Promise<boolean>} whether the tab is displayed or not
   */
  static isNavigationTabDisplayed(index: number): promise.Promise<boolean> {
    return Cache.navigationTab(index).isPresent();
  }


  /**
   * Get the navigation tab
   *
   * @param {number} index index of the targetted tab
   */
  static getNavigationTab(index: number) {
    return Cache.navigationTab(index);
  }

  /**
   * Get the navigation tab icon
   *
   * @param {number} index index of the targetted tab
   * @returns {promise.Promise<string>} the text representation of the tab's icon
   */
  static getNavigationTabIcon(index: number): promise.Promise<string> {
    return Cache.navigationTab(index).element(by.tagName('mat-icon')).getText();
  }

  /**
   * Get the navigation tab label
   *
   * @param {number} index index of the targetted tab
   * @returns {promise.Promise<string>} the tab's label
   */
  static getNavigationTabName(index: number): promise.Promise<string> {
    return Cache.navigationTab(index).element(by.className('step-name')).getText();
  }

  /**
   * @returns {SummaryCard} the base parameters card object
   */
  static get baseCard(): SummaryCard {
    return Cache.summaryCard('base-card');
  }

  /**
   * @returns {SummaryCard} the milestones card object
   */
  static get milestoneCard(): SummaryCard {
    return Cache.summaryCard('milestone-card');
  }

  /**
   * @returns {SummaryCard} the voting site configuration card object
   */
  static get votingSiteConfigurationCard(): SummaryCard {
    return Cache.summaryCard('voting-site-configuration-card');
  }

  /**
   * @returns {SummaryCard} the domain of influence card object
   */
  static get domainInfluenceCard(): SummaryCard {
    return Cache.summaryCard('domain-influence-card');
  }

  /**
   * @returns {SummaryCard} the repository card object
   */
  static get repositoryCard(): SummaryCard {
    return Cache.summaryCard('repository-card');
  }

  /**
   * @returns {SummaryCard} the documentation card object
   */
  static get documentationCard(): SummaryCard {
    return Cache.summaryCard('document-card');
  }

  /**
   * @returns {SummaryCard} the test cards lot card object
   */
  static get testingCardsLotCard(): SummaryCard {
    return Cache.summaryCard('testing-cards-lot-card');
  }

  /**
   * @returns {SummaryCard} the management entity card object
   */
  static get managementEntityCard(): SummaryCard {
    return Cache.summaryCard('management-entity-card');
  }

  /**
   * @returns {SummaryCard} the ballot documentation card object
   */
  static get ballotDocumentationCard(): SummaryCard {
    return Cache.summaryCard('ballot-documentation-card');
  }

  /**
   * @returns {SummaryCard} the election page properties card object
   */
  static get electionPagePropertiesCard(): SummaryCard {
    return Cache.summaryCard('election-page-properties-card');
  }

  /**
   * @returns {SummaryCard} the register card object
   */
  static get registerCard(): SummaryCard {
    return Cache.summaryCard('register-card');
  }

  /**
   * @returns {SummaryCard} the card title card object
   */
  static get cardTitleCard(): SummaryCard {
    return Cache.summaryCard('card-title-card');
  }

  /**
   * @returns {SummaryCard} the printer card object
   */
  static get printerTemplateCard(): SummaryCard {
    return Cache.summaryCard('printer-card');
  }

  /**
   * @returns {SummaryCard} the selected electoral authority key card object
   */
  static get selectedElectoralAuthorityKeyCard(): SummaryCard {
    return Cache.summaryCard('selected-electoral-authority-key-card');
  }

  /**
   * @returns {SummaryCard} the  oting site period card object
   */
  static get votingSitePeriodCard(): SummaryCard {
    return Cache.summaryCard('voting-site-period-card');
  }
}

export class ParametersNotificationSection {

  static get isProductionSectionDisplayed(){
    return element(by.tagName("production-summary")).isPresent()
  }

  static get isTestSectionDisplayed(){
    return element(by.tagName("test-summary")).isPresent()
  }

  static get uploadConfigurationButton() {
    return Cache.button('uploadConfigurationButton');
  }

  static get invalidateParametersButton() {
    return Cache.button('invalidateParametersButton');
  }

  static get validateParametersButton() {
    return Cache.button('validateParametersButton');
  }

  static get deploymentRequestLink(): ElementFinder {
    return Cache.element('deploymentRequestLink');
  }

  static get deploymentValidationLink(): ElementFinder {
    return Cache.element('deploymentValidationLink');
  }
}

export class VotingMaterialNotificationSection {

  static get validateVotingMaterialButton() {
    return Cache.button('validateVotingMaterialButton');
  }

  static get deploymentButton() {
    return Cache.button('deploymentButton');
  }

  static get createLink(): ElementFinder {
    return Cache.element('createLink');
  }

  static get validateCreationLink(): ElementFinder {
    return Cache.element('validateCreationLink');
  }
}

export class VotingPeriodNotificationSection {

  static get deploymentButton() {
    return Cache.button('deploymentButton');
  }

  static get createRequestLink(): ElementFinder {
    return Cache.element('createRequestLink');
  }

  static get validateRequestLink(): ElementFinder {
    return Cache.element('validateRequestLink');
  }
}

export class DeploymentSummarySection {

  static get productionCard(): Card {
    return Cache.card('productionDeploymentSummaryCard');
  }

  static get testCard(): Card {
    return Cache.card('testDeploymentSummaryCard');
  }

  static get testStatusMessage(): promise.Promise<string> {
    return Cache.element('testStatusMessage').getText();
  }

  static get modifyButton(): Button{
    return Cache.button('modifyButton');
  }


  static waitForTestStatusMessageToMatch(regex: RegExp): void {
    Main.tryForceStatusRefresh();
    browser.wait(() => this.testStatusMessage.then(text => text.match(regex), falseIfMissing));
  }

  static get productionStatusMessage(): ElementFinder {
    return Cache.element('productionStatusMessage');
  }

  static get deploymentStatusMessage(): ElementFinder {
    return Cache.element('deploymentStatusMessage');
  }

  static waitForDeploymentStatusMessageToMatch(regex: RegExp): void {
    Main.tryForceStatusRefresh();
    browser.wait(() => this.deploymentStatusMessage.getText().then(text => text.match(regex), falseIfMissing))
  }


  static waitForProductionStatusMessageToMatch(regex: RegExp): void {
    Main.tryForceStatusRefresh();
    browser.wait(() => this.productionStatusMessage.getText().then(text => text.match(regex), falseIfMissing))
  }

  static get targetRealDeploymentButton(): Button {
    return Cache.button('targetRealDeploymentButton');
  }

  static get targetSimulationDeploymentButton(): Button {
    return Cache.button('targetSimulationDeploymentButton');
  }

  static get simulationNameDialog(): ElementFinder {
    return Cache.element('simulationNameDialog', by.tagName('td-prompt-dialog'));
  }

  static get simulationNameInput(): InputField {
    return Cache.inputField(this.simulationNameDialog, 'simulationNameDialog', 'value', false, true);
  }

  static get simulationNameValidationButton(): Button {
    return Cache.button('simulationNameValidationButton',
      this.simulationNameDialog.element(by.css('button[color="accent"]')));
  }

  static get testingVotingCardButton(): Button {
    return Cache.button('non-printable-test-cards');
  }

}

