/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, element, ElementFinder } from "protractor";
import { Card } from '../../../shared/card';
import { Button } from '../../../shared/button';
import { MatTable } from '../../../shared/table';
import { UploadInput } from '../../../shared/upload-input';
import { SlideDetails } from '../../../shared/slide-details';
import { GroupedGrid } from '../../../shared/grouped-grid';
import { Cache } from '../../cache.po';
import { waitForInvisibility, waitForVisibility } from '../../../shared/e2e.utils';

/**
 * Page object for the repository edit section.
 */
export class RepositoryEdit {

  /**
   * @returns {Card} the edit section element itself
   */
  static get root(): Card {
    return Cache.card('repositoryEdit');
  }

  /**
   * @returns {SlideDetails} the details section
   */
  static get slideDetails(): SlideDetails {
    return Cache.slideDetail('repositorySlideDetail', this.root.element);
  }

  /**
   * @returns {UploadInput} the upload file input
   */
  static get uploadInput(): UploadInput {
    return Cache.uploadInputField('repositoryUploadInputField', this.root.content);
  }

  /**
   * @returns {ElementFinder} the error section
   */
  static get validationErrors(): ElementFinder {
    return Cache.element('validationErrors');
  }

  /**
   * @returns {MatTable} the grid displaying the repository files
   */
  static get table(): MatTable {
    return Cache.table('repositoriesGrid');
  }

  /**
   * Get the [DETAIL] button in the grid at the given row
   *
   * @param {number} row the row index
   * @returns {Button} the corresponding [DETAIL] button
   */
  static getDetailButton(row: number): Button {
    return Cache.button(
      `repositoryTableDetailButton-${row}`,
      this.table.cell(row, 'actions').element(by.className('button-details'))
    );
  }

  /**
   * Get the [DELETE] button in the grid at the given row
   *
   * @param {number} row the row index
   * @returns {Button} the corresponding [DELETE] button
   */
  static getDeleteButton(row: number): Button {
    return Cache.button(
      `repositoryTableDeleteButton-${row}`,
      this.table.cell(row, 'actions').element(by.className('button-delete'))
    );
  }


  /**
   * Get the [REPLACE] button in the grid at the given row
   *
   * @param {number} row the row index
   * @returns {Button} the corresponding [REPLACE] button
   */
  static getReplaceButton(row: number): Button {
    return Cache.button(
      `repositoryTableReplaceButton-${row}`,
      this.table.cell(row, 'actions').element(by.className('button-replace'))
    );
  }

  static get replaceCancelButton() {
    return Cache.button("cancelReplaceButton");
  }

  static get replacementFormIsPresent() {
    return browser.element(by.className("replacement-form")).isPresent();
  }

}

export class ElectionDetails {

  /**
   * @returns {MatTable} the grid displaying the proportional ballots
   */
  static get proportionalGrid(): MatTable {
    return Cache.table('respositoryProportionalTable', element(by.css('#table-for-proportional mat-table')));
  }

  /**
   * @returns {MatTable} the grid displaying the majority ballots
   */
  static get majorityGrid(): MatTable {
    return Cache.table('respositoryMajorityTable', element(by.css('#table-for-majority mat-table')));
  }

  /**
   * @returns {MatTable} the grid displaying the majority ballots with lists
   */
  static get majorityWithListGrid(): MatTable {
    return Cache.table('respositoryMajorityWithListTable', element(by.css('#table-for-majority-with-list mat-table')));
  }

  /**
   * @returns {MatTable} the grid displaying the electoral rolls
   */
  static get electoralRolls(): MatTable {
    return Cache.table('electoral-lists');
  }

  /**
   * waits for the electoral rolls gird to be displayed
   */
  static waitForElectoralRollsDisplayed(): void {
    waitForVisibility(this.electoralRolls.table);
    waitForVisibility(this.electoralRolls.table)
  }

  /**
   * close the electoral rolls view
   */
  static closeElectoralRolls(): void {
    Cache.button('cancel').click();
    waitForInvisibility(this.electoralRolls.table);
  }
}

export class VotationDetails {

  /**
   * Get the grouped grid associated to the given repository panel.
   * @returns the associated grouped grid
   */
  static get groupedGrid(): GroupedGrid {
    return new GroupedGrid(element(by.tagName('votation-details')));
  }
}
