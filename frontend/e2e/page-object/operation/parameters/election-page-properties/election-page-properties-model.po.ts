/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from 'protractor';
import { Card } from '../../../../shared/card';
import { Cache } from '../../../cache.po';
import { InputField } from '../../../../shared/input-field';
import { SlideToggle } from '../../../../shared/slide-toggle';
import { SelectField } from '../../../../shared/select-field';
import { Button } from '../../../../shared/button';
import { promise } from 'selenium-webdriver';

export class ElectionPropertiesModelEdit {

  static get root(): Card {
    return Cache.card('electionPagePropertiesModelEdit');
  }

  static get modelName(): InputField {
    return Cache.inputField(this.root.content, 'electionPagePropertiesModelEdit', 'modelName');
  }

  static get displayCandidateSearchForm(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'displayCandidateSearchForm');
  }

  static get allowChangeOfElectoralRoll(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'allowChangeOfElectoralList');
  }

  static get displayEmptyPosition(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'displayEmptyPosition');
  }

  static get displayCandidatePositionOnACompactBallotPaper(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'displayCandidatePositionOnACompactBallotPaper');
  }

  static get displayCandidatePositionOnAModifiedBallotPaper(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'displayCandidatePositionOnAModifiedBallotPaper');
  }

  static get displaySuffrageCount(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'displaySuffrageCount');
  }

  static get displayListVerificationCode(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'displayListVerificationCode');
  }

  static get candidateInformationDisplayModel(): SelectField {
    return Cache.selectField(this.root.content, 'electionPagePropertiesModelEdit', 'candidateInformationDisplayModel');
  }

  static get allowOpenCandidature(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'allowOpenCandidature');
  }

  static get allowMultipleMandates(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'allowMultipleMandates');
  }

  static get displayVoidOnEmptyBallotPaper(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'electionPagePropertiesModelEdit', 'displayVoidOnEmptyBallotPaper');
  }

  static get saveButton(): Button {
    return Cache.button('saveButton');
  }
}

export class VerificationCodeTableEdit {

  static get root(): ElementFinder {
    return Cache.element('verificationCodeTable', by.tagName('verification-code-edit'));
  }

  static get columnsToDisplaySection(): ElementFinder {
    return Cache.element('columns-to-display');
  }

  static get columnsToSortSection(): ElementFinder {
    return Cache.element('columns-to-sort');
  }

  static get numberOfColumnToDisplay(): promise.Promise<number> {
    return this.columnsToDisplaySection.all(by.tagName('mat-select')).then(items => items.length);
  }

  static get numberOfColumnToSort(): promise.Promise<number> {
    return this.columnsToSortSection.all(by.tagName('mat-select')).then(items => items.length);
  }

  static getColumnToDisplay(index: number) {
    return Cache.selectField(this.columnsToDisplaySection.element(by.id(`column_to_display_${index}`)), `verificationCodeTable-display-${index}`);
  }

  static getColumnToSort(index: number) {
    return Cache.selectField(this.columnsToSortSection.element(by.id(`column_to_sort_${index}`)), `verificationCodeTable-sort-${index}`);
  }

  static get addAColumnToDisplayButton() {
    return new Button('add-column-to-display');
  }

  static get addAColumnToSortButton() {
    return new Button('add-column-to-sort');
  }
}
