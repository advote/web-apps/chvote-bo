/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder } from 'protractor';
import { Cache } from '../../../cache.po';
import { Card } from '../../../../shared/card';
import { promise } from 'selenium-webdriver';
import { Button } from '../../../../shared/button';
import { SelectField } from '../../../../shared/select-field';
import { InputField } from '../../../../shared/input-field';
import { waitForInvisibility } from '../../../../shared/e2e.utils';

/**
 * Page object for the election properties edit section.
 */
export class ElectionPropertiesEdit {

  /**
   * @returns {Card} the edit section element itself
   */
  static get root(): Card {
    return Cache.card('electionPagePropertiesEdit');
  }

  /**
   * @returns {ElementFinder} the element displaying the list of election's mapping
   */
  static get mappings(): ElementFinder {
    return Cache.element('electionPropertiesMappings', by.tagName('election-page-properties-mapping'));
  }

  /**
   * @returns {ElementFinder} the element displaying the list of models
   */
  static get models(): ElementFinder {
    return Cache.element('electionPropertiesModels', by.tagName('election-page-properties-model-list'));
  }

  /**
   * @returns {Button} the [CREATE MODEL] button
   */
  static get createModelButton(): Button {
    return Cache.button('addModel');
  }

  /**
   * @returns {Button} the [BULK UPDATE] button
   */
  static get bulkUpdateButton(): Button {
    return Cache.button('bulk-update-button');
  }

  /**
   * Get the model button identified by its name
   *
   * @param {string} name the model's name
   * @returns {ModelButton} the corresponding model button
   */
  static getModel(name: string) {
    return new ModelButton(this.models, name);
  }

  /**
   * Get he mapping for the given ballot
   *
   * @param {string} ballot the concerned ballot
   * @returns {ElectionMapping} the corresponding ballot mapping
   */
  static getMapping(ballot: string): ElectionMapping {
    return new ElectionMapping(this.mappings, ballot);
  }
}

/**
 * Object representing a ballot mapping
 */
export class ElectionMapping {
  private root: ElementFinder;
  private ballot: string;

  constructor(mappings: ElementFinder, ballot: string) {
    this.root = mappings.element(by.id(ballot));
    this.ballot = ballot;
  }

  get selectField() {
    return Cache.selectField(this.root.element(by.tagName('mat-select')), this.ballot);
  }

  get deleteButton() {
    return Cache.button(`electionMappingDeleteButton-${this.ballot}`, this.root.element(by.id('deleteButton')));
  }

  get present(): promise.Promise<boolean> {
    return this.root.isPresent();
  }
}

/**
 * Object representing a model button
 */
export class ModelButton {
  private root: ElementFinder;

  constructor(modelList: ElementFinder, modelName: string) {
    this.root = modelList.element(by.id(modelName));
  }

  get present() {
    return this.root.isPresent();
  }

  click() {
    return this.root.element(by.className('model-name')).click();
  }

  clickOnDelete() {
    return this.root.element(by.className('model-delete')).click();
  }

  get hasDelete() {
    return this.root.element(by.className('model-delete')).isPresent();
  }
}

/**
 * Page objet representing the bulk update dialog
 */
export class BulkUpdatePopup {

  static get root(): ElementFinder {
    return Cache.element('electionModelBulkMapping', by.tagName('election-page-bulk-update'));
  }

  static get acceptButton(): Button {
    return Cache.button('accept');
  }

  static get cancelButton(): Button {
    return Cache.button('cancel');
  }

  static get modelSelect(): SelectField {
    return Cache.selectField(this.root.element(by.id('model-select')), 'electionModelBulkMapping');
  }

  static get present(): promise.Promise<boolean> {
    return this.root.isPresent();
  }

  static toggleAll(): void {
    this.root.element(by.css('mat-header-row mat-checkbox')).click();
  }

  static toggle(ballot: string): void {
    this.root.element(by.id(`cb#${ballot}`)).click();
  }

  static get allBallots(): promise.Promise<string[]> {
    return this.root.all(by.className('ballot-name'))
      .map(elementFinder => elementFinder.getText());
  }

  static get filterInput(): InputField {
    return Cache.inputField(this.root, 'electionModelBulkMapping', 'search-input', false);
  }

  static waitForInvisibility() {
    waitForInvisibility(this.root);
  }
}

