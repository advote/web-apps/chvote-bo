/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from "protractor";
import { Card } from '../../../shared/card';
import { UploadInput } from '../../../shared/upload-input';
import { Button } from '../../../shared/button';
import { MatTable } from '../../../shared/table';
import { ExpansionPanel } from '../../../shared/expansion-panel';
import { Cache } from '../../cache.po';

/**
 * Page object for the domain of influence edit section.
 */
export class DomainInfluenceEdit {

  static get root(): Card {
    return Cache.card('domainInfluenceEdit');
  }

  static get uploadInput(): UploadInput {
    return Cache.uploadInputField('doiUploadInput', this.root.content);
  }

  static get validationErrors(): ElementFinder {
    return Cache.element('validationErrors');
  }

  /**
   * @returns {MatTable} the table displaying the list of domains of influence files
   */
  static get fileTable(): MatTable {
    return Cache.table('fileGrid');
  }

  /**
   * @return {ExpansionPanel} the domain of influence detail panel
   */
  static get fileDetailPanel(): ExpansionPanel {
    return Cache.expansionPanel(
      'doiDetailPanel',
      this.fileTable.cell(0, 'file').element(by.tagName('td-expansion-panel'))
    );
  }

  /**
   * @returns {MatTable} the table displaying the detail of domains of influence
   */
  static get detailTable(): MatTable {
    return Cache.table('detailGrid');
  }

  /**
   * @returns {Button} the file grid's delete button
   */
  static get deleteButton(): Button {
    return Cache.button('doiDeleteButton', this.fileTable.cell(0, 'delete').element(by.tagName('button')));
  }
}

