/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from 'protractor';
import { Button } from '../../../../shared/button';
import { Cache } from '../../../cache.po';
import { promise } from 'selenium-webdriver';
import { InputField } from '../../../../shared/input-field';
import { waitForClickable, waitForInvisibility } from '../../../../shared/e2e.utils';

/**
 * Page object for the management entity selection popup.
 */
export class ManagementEntitySelection {

  /**
   * @returns {ElementFinder} the page root element
   */
  static get root(): ElementFinder {
    return Cache.element('managementEntitySelection', by.tagName('management-entity-selector'));
  }

  /**
   * @returns {promise.Promise<string>} selection popup title
   */
  static get title(): promise.Promise<string> {
    return this.root.element(by.className('mat-dialog-title')).getText();
  }

  /**
   * @returns {promise.Promise<string[]>} the list of available management entities
   */
  static get availableManagementEntities(): promise.Promise<string[]> {
    return this.root
      .all(by.className('management-entity-name'))
      .map(elementFinder => elementFinder.getText());
  }

  /**
   * @returns {Button} the [ACCEPT] button
   */
  static get acceptButton(): Button {
    return Cache.button('accept');
  }

  /**
   * @returns {Button} the [CANCEL] button
   */
  static get cancelButton(): Button {
    return Cache.button('cancel');
  }

  /**
   * @returns {InputField} the filter input field
   */
  static get filterInput(): InputField {
    return Cache.inputField(this.root, 'managementEntitySelection', 'search-input', false);
  }

  /**
   * @returns {ElementFinder} the revoke error element
   */
  static get revokeError() {
    return this.root.element(by.id('revokeError'));
  }

  /**
   * @returns {promise.Promise<string>} the revoke error message
   */
  static get revokeErrorMessage(): promise.Promise<string> {
    return this.revokeError.all(by.tagName('span')).get(1).getText();
  }

  /**
   * Toggle the select all checkbox
   */
  static toggleAll(): void {
    let element = this.root.element(by.css('mat-header-row mat-checkbox'));
    waitForClickable(element);
    element.click();
  }

  /**
   * Toggle the given management entity check box
   *
   * @param {string} managementEntity the name of the management entity to toggle
   */
  static toggle(managementEntity: string): void {
    let element = this.root.element(by.id(`cb#${managementEntity}`));
    waitForClickable(element);
    element.click();
  }

  /**
   * Wait for the popup screen to disappear
   */
  static waitForInvisibility(): void {
    waitForInvisibility(this.root);
  }
}
