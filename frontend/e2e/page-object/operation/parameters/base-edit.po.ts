/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Card } from '../../../shared/card';
import { InputField } from '../../../shared/input-field';
import { DatePicker } from '../../../shared/date-picker';
import { Button } from '../../../shared/button';
import { Cache } from '../../cache.po';

/**
 * Page object for the base parameters edit section.
 */
export class BaseEdit {

  static get root(): Card {
    return Cache.card('baseEdit');
  }

  static get longLabelInput(): InputField {
    return Cache.inputField(this.root.content, 'baseEdit', 'longLabel');
  }

  static get shortLabelInput(): InputField {
    return Cache.inputField(this.root.content, 'baseEdit', 'shortLabel');
  }

  static get dateInput(): DatePicker {
    return Cache.datePickerField(this.root.content, 'baseEdit', 'date');
  }

  static get saveButton(): Button {
    return Cache.button('saveButton');
  }
}

