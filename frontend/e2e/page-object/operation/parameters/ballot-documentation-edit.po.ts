/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by } from 'protractor';
import { Button } from '../../../shared/button';
import { SelectField } from '../../../shared/select-field';
import { UploadInput } from '../../../shared/upload-input';
import { MatTable } from '../../../shared/table';
import { Cache } from '../../cache.po';
import { Card } from '../../../shared/card';
import { InputField } from '../../../shared/input-field';

/**
 * Page object for the ballot documentation edit section.
 */
export class BallotDocumentationEdit {

  /**
   * @returns {Card} the edit section element itself
   */
  static get root(): Card {
    return Cache.card('BallotDocumentationEdit');
  }

  /**
   * @returns {SelectField} the ballot select field
   */
  static get ballotSelect(): SelectField {
    return Cache.selectField(this.root.content, 'BallotDocumentationEdit', 'ballotId');
  }

  /**
   * @returns {SelectField} the document language select field
   */
  static get languageSelect(): SelectField {
    return Cache.selectField(this.root.content, 'BallotDocumentationEdit', 'language');
  }

  /**
   * @returns {InputField} the document localized label input field
   */
  static get localizedLabelInput(): InputField {
    return Cache.inputField(this.root.content, 'BallotDocumentationEdit', 'localizedLabel');
  }

  /**
   * @returns {UploadInput} the upload file input
   */
  static get uploadInput(): UploadInput {
    return Cache.uploadInputField('ballotDocumentationUploadInputField', this.root.content);
  }

  /**
   * @returns {MatTable} the table displaying the ballot document files
   */
  static get table(): MatTable {
    return Cache.table('ballotDocumentationList');
  }

  /**
   * Get the delete button on the grid at the given row
   *
   * @param row index of the given row
   * @returns {Button} the grid's delete button at the given row
   */
  static getDeleteButton(row: number): Button {
    return Cache.button(
      `ballotDocumentationTableDeleteButton-${row}`,
      this.table.cell(row, 'delete').element(by.tagName('button'))
    );
  }

}
