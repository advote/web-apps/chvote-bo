/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, element } from 'protractor';
import { Button } from '../../../shared/button';
import { SlideToggle } from '../../../shared/slide-toggle';
import { DatePicker } from '../../../shared/date-picker';
import { promise } from 'selenium-webdriver';
import { Card } from '../../../shared/card';
import { Cache } from '../../cache.po';
import { SelectField } from '../../../shared/select-field';
import { InputField } from '../../../shared/input-field';

/**
 * Page object for the testing cards lot edit section
 */
export class TestingCardsEdit {

  /**
   * @returns {Card} the edit section element itself
   */
  static get root(): Card {
    return Cache.card('testingCardsLotEdit');
  }

  /**
   * @returns {SelectField} the "card type" field
   */
  static get cardType(): SelectField {
    return Cache.selectField(this.root.content, 'testingCardsLotEdit', 'cardType');
  }

  /**
   * @returns {InputField} the "lot name" field
   */
  static get lotName(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'lotName');
  }

  /**
   * @returns {SlideToggle} the "should print" field
   */
  static get shouldPrint(): SlideToggle {
    return Cache.slideToggle(this.root.content, 'testingCardsLotEdit', 'shouldPrint');
  }

  /**
   * @returns {InputField} the "count" field
   */
  static get count(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'count');
  }

  /**
   * @returns {SelectField} the "signature" field
   */
  static get signature(): SelectField {
    return Cache.selectField(this.root.content, 'testingCardsLotEdit', 'signature');
  }

  /**
   * @returns {InputField} the "first name" field
   */
  static get firstName(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'firstName');
  }

  /**
   * @returns {InputField} the "last name" field
   */
  static get lastName(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'lastName');
  }

  /**
   * @returns {DatePicker} the "birthday field
   */
  static get birthday(): DatePicker {
    return Cache.datePickerField(this.root.content, 'testingCardsLotEdit', 'birthday');
  }

  /**
   * @returns {InputField} the first "address" field
   */
  static get address(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'address1');
  }

  /**
   * @returns {InputField} the first "address2" field
   */
  static get address2(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'address2');
  }

  /**
   * @returns {InputField} the "street" field
   */
  static get street(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'street');
  }

  /**
   * @returns {InputField} the "postal code" field
   */
  static get postalCode(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'postalCode');
  }

  /**
   * @returns {InputField} the "city" field
   */
  static get city(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'city');
  }

  /**
   * @returns {InputField} the "country" field
   */
  static get country(): InputField {
    return Cache.inputField(this.root.content, 'testingCardsLotEdit', 'country');
  }

  /**
   * @returns {SelectField} the "domain of influence" field
   */
  static get doi(): SelectField {
    return Cache.selectField(this.root.content, 'testingCardsLotEdit', 'doi');
  }

  /**
   * @returns {SelectField} the "language" field
   */
  static get language(): SelectField {
    return Cache.selectField(this.root.content, 'testingCardsLotEdit', 'language');
  }

  /**
   * @returns {Button} the [SAVE] button
   */
  static get saveButton(): Button {
    return Cache.button('saveButton');
  }

  /**
   * @returns {promise.Promise<string>} the printer's name
   */
  static get printerName(): promise.Promise<string> {
    return element(by.id('printer')).getText();
  }
}
