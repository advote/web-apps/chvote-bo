/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, ElementFinder } from 'protractor';
import { Button } from '../../../shared/button';
import { MatTable } from '../../../shared/table';
import { Card } from '../../../shared/card';
import { Cache } from '../../cache.po';

/**
 * Page object for the testing cards list section
 */
export class TestingCardsList {

  /**
   * @returns {Card} the edit section element itself
   */
  static get root(): Card {
    return Cache.card('testingCardsLotList');
  }

  /**
   * @returns {MatTable} the element displaying the list of testing cards lots
   */
  static get table(): MatTable {
    return Cache.table('testingCardsLotTable');
  }

  /**
   * @returns {Button} the [CREATE LOT] button
   */
  static get createButton(): Button {
    return Cache.button('createNewTestingCardsLotButton');
  }

  /**
   * Get the action cell of the given row
   *
   * @param {number} row the index of the concerned row
   * @returns {ElementFinder} the corresponding action cell
   */
  static getActionCell(row: number): ElementFinder {
    return this.table.cell(row, 'actions')
  }

  /**
   * Get the [DELETE] button of the given row
   *
   * @param {number} row the index of the concerned row
   * @returns {Button} the corresponding [DELETE] button
   */
  static getDeleteButton(row: number): Button {
    return Cache.button(
      `testingCardsList-deleteButton-${row}`,
      this.getActionCell(row).element(by.className('btn-delete'))
    );
  }

  /**
   * Get the [EDIT] button of the given row
   *
   * @param {number} row the index of the concerned row
   * @returns {Button} the corresponding [EDIT] button
   */
  static getEditButton(row: number): Button {
    return Cache.button(
      `testingCardsList-editButton-${row}`,
      this.getActionCell(row).element(by.className('btn-edit'))
    );
  }
}
