/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, element, ElementFinder } from 'protractor';
import { MatExpansionPanel } from '../../../shared/mat-expansion-panel';
import { promise } from 'selenium-webdriver';

export class VotingMaterialCreationProgress {

  static get root(): ElementFinder {
    return element(by.tagName('voting-material-creation-progress'));
  }

  static get initializeParametersPanel(): ProgressionPanel {
    return new ProgressionPanel(this.root, 'INITIALIZE_PARAMETERS');
  }

  static get sendVotersPanel(): ProgressionPanel {
    return new ProgressionPanel(this.root, 'SEND_VOTERS');
  }

  static get requestPrivateCredentialsPanel(): ProgressionPanel {
    return new ProgressionPanel(this.root, 'REQUEST_PRIVATE_CREDENTIALS');
  }
}

class ProgressionPanel {
  private expansionPanel: MatExpansionPanel;

  constructor(root: ElementFinder, id: string) {
    this.expansionPanel = new MatExpansionPanel(root.element(by.id(id)));
  }

  get stepStucks(): promise.Promise<boolean> {
    return this.expansionPanel.description.element(by.className('step_stuck')).isPresent();
  }

  get progressInPercent(): promise.Promise<string> {
    return this.expansionPanel.description.element(by.className('percent')).getText();
  }

  get disabled(): promise.Promise<boolean> {
    return this.expansionPanel.disabled;
  }

  isControlComponentStuck(ccIndex: number): promise.Promise<boolean> {
    return this.expansionPanel.content.element(by.id(`cc_progress_${ccIndex}`)).element(by.className('cc_stuck'))
      .isPresent();
  }

  getControlComponentProgress(ccIndex: number): promise.Promise<string> {
    return this.expansionPanel.content.element(by.id(`cc_progress_${ccIndex}`)).element(by.className('percent'))
      .getText();
  }

  open(): void {
    this.expansionPanel.click();
  }
}
