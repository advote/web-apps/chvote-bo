/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Cache } from '../../../cache.po';
import { Card } from '../../../../shared/card';
import { UploadInput } from '../../../../shared/upload-input';
import { MatTable } from '../../../../shared/table';

/**
 * Page object for the register upload section.
 */
export class RegisterUpload {

  static get root(): Card {
    return Cache.card('registerUpload');
  }

  static get uploadInput(): UploadInput {
    return Cache.uploadInputField('registerUploadInput', this.root.content);
  }

  static get businessErrors() {
    return Cache.element('business-error').getText();
  }

  static get validationErrors() {
    return Cache.element('validationErrors');
  }

  static get duplicateErrors() {
    return Cache.element('duplicateErrors');
  }

  static get undefinedDoiErrors() {
    return Cache.element('undefinedDoiErrors');
  }

  static get duplicateErrorsDownloadButton() {
    return Cache.button('duplicateErrorsDownloadButton');
  }

  static get validationErrorsDownloadButton() {
    return Cache.button('validationErrorsDownloadButton');
  }

  static get undefinedDoiErrorsDownloadButton() {
    return Cache.button('undefinedDoiErrorsDownloadButton');
  }

  static get cancelButton() {
    return Cache.button('cancelButton');
  }

  static get reportTable(): MatTable {
    return Cache.table('registerReportTable');
  }

  static get validateButton() {
    return Cache.button('validateImport');
  }
}
