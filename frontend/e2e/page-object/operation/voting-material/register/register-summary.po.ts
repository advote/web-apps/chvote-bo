/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { MatTable } from '../../../../shared/table';
import { Button } from '../../../../shared/button';
import { by } from 'protractor';
import { Cache } from '../../../cache.po';
import { promise } from 'selenium-webdriver';

/**
 * Page object for the register summary table
 */
export class RegisterSummary {

  static get root(): MatTable {
    return Cache.table('registerFilesTable');
  }

  static fileName(rowIndex: number): promise.Promise<string> {
    return this.root.cell(rowIndex, 'fileName').getText();
  }

  static uploader(rowIndex: number): promise.Promise<string> {
    return this.root.cell(rowIndex, 'uploadDetails').element(by.className('uploader')).getText();
  }

  static uploadDate(rowIndex: number): promise.Promise<string> {
    return this.root.cell(rowIndex, 'uploadDetails').element(by.className('upload-date')).getText();
  }

  static detailedReport(rowIndex: number): Button {
    return new Button(this.root.cell(rowIndex, 'detailedReport').$('button'));
  }

  static deleteButton(rowIndex: number): Button {
    return new Button(this.root.cell(rowIndex, 'delete').$('button'));
  }

  static emissionDate(rowIndex: number): promise.Promise<string> {
    return this.root.cell(rowIndex, 'uploadDetails').element(by.className('emission-date')).getText();
  }

  static emitter(rowIndex: number): promise.Promise<string> {
    return this.root.cell(rowIndex, 'uploadDetails').element(by.className('emitter')).getText();
  }

  static voterCount(rowIndex: number): promise.Promise<string> {
    return this.root.cell(rowIndex, 'statistics').element(by.className('voter-count')).getText();
  }

  static countingCircleCount(rowIndex: number): promise.Promise<string> {
    return this.root.cell(rowIndex, 'statistics').element(by.className('counting-circle-count')).getText();
  }

  static doiCount(rowIndex: number): promise.Promise<string> {
    return this.root.cell(rowIndex, 'statistics').element(by.className('doi-count')).getText();
  }
}
