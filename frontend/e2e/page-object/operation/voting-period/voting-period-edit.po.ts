/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { by, element, ElementFinder } from "protractor";
import { NavList } from '../../../shared/nav-list';
import { waitForClickable, waitForInvisibility } from '../../../shared/e2e.utils';

/**
 * Page object for the operation's voting period edition.
 */
export class VotingPeriodEditPage {

  /**
   * @returns the root element of this page
   */
  static get root(): ElementFinder {
    return element(by.tagName('edit-with-side-bar'));
  }

  /**
   * @returns {ElementFinder} the link element to go back to the parameters dashboard
   */
  static get backLink(): ElementFinder {
    return this.root.element(by.tagName('side-bar-back-button'));
  }

  static clickBackLink(shouldDisappear = true) {
    waitForClickable(VotingPeriodEditPage.backLink);
    this.backLink.click();
    if (shouldDisappear){
      waitForInvisibility(VotingPeriodEditPage.backLink);
    }
  }

  /**
   * @returns {NavList} the navigation list element
   */
  static get navigationList(): NavList {
    return new NavList('sidebar-navlist');
  }
}
