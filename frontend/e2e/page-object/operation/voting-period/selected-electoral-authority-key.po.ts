/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Button } from '../../../shared/button';
import { Card } from '../../../shared/card';
import { Cache } from '../../cache.po';
import { by } from 'protractor';
import { promise } from 'selenium-webdriver';

/**
 * Page object for the Selected Electoral Authority Key edit section.
 */
export class SelectedElectoralAuthorityKeyPage {

  static get root(): Card {
    return Cache.card('selectedElectoralAuthorityKeyEdit');
  }

  static get hasNoElectoralAuthorityKeys(): promise.Promise<boolean> {
    return this.root.element.element(by.id("no-electoral-authority-keys")).isPresent();
  }

  static get keySelectField() {
    return Cache.selectField(this.root.content, 'selectedElectoralAuthorityKeyEdit', 'keyId');
  };

  static get keyLabel() {
    return this.root.element.element(by.id("key_label")).getText();
  };

  static get keyHash() {
    return this.root.element.element(by.id("key_hash")).getText();
  };

  static get keyLogin() {
    return this.root.element.element(by.id("key_login")).getText();
  };

  static get keyImportDate() {
    return this.root.element.element(by.id("key_importDate")).getText();
  };

  static get saveButton(): Button {
    return Cache.button('saveButton');
  }
}
