/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, ElementFinder } from 'protractor';
import { Cache } from '../cache.po';
import { MatTable } from '../../shared/table';
import { InputField } from '../../shared/input-field';
import { Card } from '../../shared/card';
import { UploadInput } from '../../shared/upload-input';
import { Button } from '../../shared/button';
import { waitForInvisibility } from '../../shared/e2e.utils';

export class ElectoralAuthorityKeyEdit {

  static get root(): Card {
    return Cache.card('electoralAuthorityKeyEdit');
  }

  static get grid(): MatTable {
    return Cache.table('electoralAuthorityKeyList');
  }

  static get label(): InputField {
    return Cache.inputField(this.root.content, 'electoralAuthorityKeyEdit', 'label');
  }

  static get uploadInput(): UploadInput {
    return Cache.uploadInputField('keyUploadInputField', this.root.content);
  }
}

export class UpdateNamePopup {

  static get root(): ElementFinder {
    return Cache.element('ElectoralAuthorityKeyUpdateNamePopup', by.tagName('edit-label'));
  }

  static get label(): InputField {
    return Cache.inputField(this.root, 'ElectoralAuthorityKeyUpdateNamePopup', 'label');
  }

  static get acceptButton(): Button {
    return Cache.button('accept');
  }

  static get cancelButton(): Button {
    return Cache.button('cancel');
  }

  static get present() {
    return this.root.isPresent();
  }

  static waitForInvisibility() {
    waitForInvisibility(this.root)
  }
}
