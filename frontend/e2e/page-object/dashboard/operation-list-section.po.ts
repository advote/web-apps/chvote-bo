/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by } from "protractor";
import { MatTable } from "../../shared/table";
import { Card } from '../../shared/card';

/**
 * Page object for the operation list section.
 */
export class OperationListSection {

  static get element(): Card {
    return new Card('operationListSection');
  }

  /**
   * @returns {MatTable} the grid displaying the operations list
   */
  static get operationsGrid() {
    return new MatTable(browser.element(by.id("operationsGrid")));
  }
}

