/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser, by, element, ElementFinder } from "protractor";
import { promise } from 'selenium-webdriver';
import { waitForInvisibility } from '../shared/e2e.utils';

/**
 * Page object for the global consistency check messages
 */
export class ConsistencyCheck {

  private static _errors: ElementFinder = element(by.className('notification--consistency-errors'));

  /**
   * Wait for the consistency check to be done
   */
  static waitConsistencyCheck(): void {
    waitForInvisibility(browser.element(by.className('notification--consistency-in-progress')), 30000);
  }

  /**
   * @returns {promise.Promise<boolean>} whether there is consistency error or not
   */
  static get hasError(): promise.Promise<boolean> {
    return this.errors.isPresent();
  }

  /**
   * Get the error text at the given index
   *
   * @param {number} index the error index in the list of errors
   * @returns {promise.Promise<string>} the corresponding error text
   */
  static error(index: number): promise.Promise<string> {
    return this.errors.all(by.tagName('li')).get(index).getText();
  }

  private static get errors(): ElementFinder {
    if (this._errors === undefined) {
      this._errors = element(by.className('notification--consistency-errors'));
    }
    return this._errors;
  }
}
