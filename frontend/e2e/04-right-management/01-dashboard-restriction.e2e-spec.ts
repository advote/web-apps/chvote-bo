/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../page-object/main.po';
import { OperationListSection } from '../page-object/dashboard/operation-list-section.po';
let since = require('jasmine2-custom-message');

describe('Right Management/Dashboard Restriction', () => {

  beforeAll(() => {
    Main.login();
    Main.createOperationAndNavigate('dashboard');
    Main.refresh();
  });

  it(
    'should display the common dashboard with the created operation after login with a user having the same management entity',
    () => {
      expect(OperationListSection.operationsGrid.rowCount).toBe(1);
      since('[MANAGEMENT] button should be displayed')
        .expect(OperationListSection.operationsGrid.header('management').isPresent()).toBeTruthy();
      since('[PRINT ARCHIVE DOWNLOAD] button should be hidden')
        .expect(OperationListSection.operationsGrid.header('printerArchiveDownload').isPresent()).toBeFalsy();
    });

  it('should not display the operation if we switch to a user having a different management entity', () => {
    Main.revokeManagementEntity('Gy');
    Main.login('ge3', true);
    expect(OperationListSection.operationsGrid.rowCount).toBe(0);
  });

  it('should display the Printer dashboard if the user is a printer', () => {
    Main.login('prn1', true);
    since('[MANAGEMENT] button should be hidden')
      .expect(OperationListSection.operationsGrid.header('management').isPresent()).toBeFalsy();
    since('[PRINT ARCHIVE DOWNLOAD] button should be displayed')
      .expect(OperationListSection.operationsGrid.header('printerArchiveDownload').isPresent()).toBeTruthy();
  });

});
