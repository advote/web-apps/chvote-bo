/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../page-object/main.po';
import { RegisterSummary } from '../page-object/operation/voting-material/register/register-summary.po';
import { RegisterEdit } from '../page-object/operation/voting-material/register/register-edit.po';
import { OperationOption } from '../shared/mock-server';
import { VotingMaterialEditPage } from '../page-object/operation/voting-material/voting-material-edit.po';
import { OperationManagementPage } from '../page-object/operation/operation-management.po';

describe('Right Management/Register', () => {

  beforeAll(() => {
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-material",
      'test_right_management',
      'TEST_e2e_right_management',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_REGISTER_SE,
        OperationOption.ADD_REGISTER_SR,
        OperationOption.ADD_CARD_TITLE,
        OperationOption.ADD_PRINTER_TEMPLATE,
        OperationOption.FOR_SIMULATION
      ]
    );

    Main.login('ge4');
    Main.navigateTo("operations/[OPERATION_ID]/voting-material");

  });

  it('import of a register file is disabled if user doesn\'t have this role', () => {
    OperationManagementPage.registerCard.button.click();
    expect(RegisterEdit.uploadButton.present).toBeFalsy();
  }, 240000);

  it('getting consolidated report is disabled if user doesn\'t have this role', () => {
    expect(RegisterEdit.consolidatedReportButton.present).toBeFalsy();
  }, 240000);

  it('getting detailed report is disabled if user doesn\'t have this role', () => {
    expect(RegisterSummary.detailedReport(0).present).toBeFalsy();
  }, 240000);

  it('removing a register file is disabled if user doesn\'t have this role', () => {
    expect(RegisterSummary.deleteButton(0).present).toBeFalsy();
    VotingMaterialEditPage.clickBackLink();
  }, 240000);

});
