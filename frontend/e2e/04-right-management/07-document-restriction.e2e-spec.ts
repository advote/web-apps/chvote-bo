/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../page-object/main.po';
import { DocumentationEdit } from '../page-object/operation/parameters/documentation-edit.po';
import { OperationManagementPage } from '../page-object/operation/operation-management.po';
import { ParameterEditPage } from '../page-object/operation/parameters/parameter-edit.po';

describe('Right Management/Documentation', () => {

  beforeAll(() => {
    Main.login('ge4');
    Main.createOperationAndNavigate('operations/[OPERATION_ID]/parameters');
  });

  it('should not display [DELETE] button if user doesn\'t have this role', () => {
    OperationManagementPage.documentationCard.button.click();
    expect(DocumentationEdit.getDeleteButton(0).present).toBeFalsy();
  });

  it('should not display [IMPORT] button if user doesn\'t have this role', () => {
    expect(DocumentationEdit.uploadInput.uploadButton.present).toBeFalsy();
    ParameterEditPage.clickBackLink();
  });
});
