/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../page-object/main.po';
import { BaseEdit } from '../page-object/operation/parameters/base-edit.po';
import { OperationManagementPage } from '../page-object/operation/operation-management.po';
import { ParameterEditPage } from '../page-object/operation/parameters/parameter-edit.po';
let since = require('jasmine2-custom-message');

describe('Right Management/Base parameters', () => {

  beforeAll(() => {
    Main.login('ge4');
    Main.createOperationAndNavigate('operations/[OPERATION_ID]/parameters');
  });

  it('all user interactions should be disabled if user doesn\'t have this role', () => {
    OperationManagementPage.baseCard.button.click();
    since('date input should be disabled').expect(BaseEdit.dateInput.enabled).toBeFalsy();
    since('long label input should be disabled').expect(BaseEdit.longLabelInput.enabled).toBeFalsy();
    since('short label input should be disabled').expect(BaseEdit.shortLabelInput.enabled).toBeFalsy();
    since('[SAVE] button should be hiddend').expect(BaseEdit.saveButton.present).toBeFalsy();
    ParameterEditPage.clickBackLink();
  });

});
