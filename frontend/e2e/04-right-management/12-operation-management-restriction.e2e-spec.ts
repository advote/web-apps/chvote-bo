/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { browser } from 'protractor';
import { configuration, OperationOption, votingMaterial } from '../shared/mock-server';
import { Main } from '../page-object/main.po';
import { DeploymentSummarySection, ParametersNotificationSection, VotingMaterialNotificationSection } from '../page-object/operation/operation-management.po';
let since = require('jasmine2-custom-message');

const FLOW = browser.controlFlow();

describe('Right Management/Operation management', () => {

  beforeAll(() => {
    Main.login('ge4');
    Main.operationId = undefined;
    Main.createOperationAndNavigate(
      'operations/[OPERATION_ID]',
      'test_right_management',
      'TEST_e2e_right_management',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_ELECTION_PAGE_PROPERTIES
      ]
    );
  });

  it('configuration upload is disabled if user doesn\'t have this role', () => {
    expect(ParametersNotificationSection.uploadConfigurationButton.present).toBeFalsy();
  });

  it('target deployment is disabled if user doesn\'t have this role', () => {
    FLOW.execute(() => configuration.validateDeploymentRequestInPact(Main.operationId));
    DeploymentSummarySection.waitForDeploymentStatusMessageToMatch(/Déploiement validé le .*/);
    since('[SIMULATION] button should be hidden').expect(DeploymentSummarySection.targetSimulationDeploymentButton.present).toBeFalsy();
    since('[REAL] button should be hidden').expect(DeploymentSummarySection.targetRealDeploymentButton.present).toBeFalsy();
  });

  it('voting material upload is disabled if user doesn\'t have this role', () => {
    Main.login('ge1');
    Main.deployConfiguration();
    votingMaterial.addToOperation(Main.operationId);
    Main.login('ge4');
    Main.navigateTo('operations/[OPERATION_ID]/voting-material');
    expect(VotingMaterialNotificationSection.deploymentButton.present).toBeFalsy();
  });

});
