/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../page-object/main.po';
import { PrinterTemplateEdit } from '../page-object/operation/voting-material/printer-template-edit.po';
import { OperationOption } from '../shared/mock-server';
import { OperationManagementPage } from '../page-object/operation/operation-management.po';
import { VotingMaterialEditPage } from '../page-object/operation/voting-material/voting-material-edit.po';
let since = require('jasmine2-custom-message');

describe('Right Management/Printer', () => {

  beforeAll(() => {
    Main.createOperationAndNavigate(
      "operations/[OPERATION_ID]/voting-material",
      'test_right_management',
      'TEST_e2e_right_management',
      [
        OperationOption.ADD_MILESTONE,
        OperationOption.ADD_DOMAIN_INFLUENCE,
        OperationOption.ADD_REPOSITORY_VOTATION,
        OperationOption.ADD_REPOSITORY_ELECTION,
        OperationOption.ADD_DOCUMENT,
        OperationOption.ADD_TESTING_CARDS,
        OperationOption.ADD_REGISTER_SE,
        OperationOption.ADD_REGISTER_SR,
        OperationOption.ADD_CARD_TITLE,
        OperationOption.ADD_PRINTER_TEMPLATE,
        OperationOption.FOR_SIMULATION
      ]
    );

    Main.login('ge4');
    Main.navigateTo("operations/[OPERATION_ID]/voting-material");
  });

  it('all user interactions should be disabled if user doesn\'t have this role', () => {
    OperationManagementPage.printerTemplateCard.button.click();
    since('[SAVE] button should be hidden').expect(PrinterTemplateEdit.saveButton.present).toBeFalsy();
    since('template select should be disabled').expect(PrinterTemplateEdit.templateSelect.enabled).toBeFalsy();
    VotingMaterialEditPage.clickBackLink();
  });

});
