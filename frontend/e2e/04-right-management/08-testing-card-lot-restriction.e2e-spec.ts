/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../page-object/main.po';
import { TestingCardsList } from '../page-object/operation/testing-cards/testing-cards-list.po';
import { TestingCardsEdit } from '../page-object/operation/testing-cards/testing-cards-edit.po';
import { OperationManagementPage } from '../page-object/operation/operation-management.po';
import { ParameterEditPage } from '../page-object/operation/parameters/parameter-edit.po';
let since = require('jasmine2-custom-message');

describe('Right Management/Testing card', () => {

  beforeAll(() => {
    Main.login('ge4');
    Main.createOperationAndNavigate('operations/[OPERATION_ID]/parameters');
  });

  it('should not display [CREATE] button if user doesn\'t have this role', () => {
    OperationManagementPage.testingCardsLotCard.button.click();
    expect(TestingCardsList.createButton.present).toBeFalsy();
  });

  it('should not display [DELETE] button if user doesn\'t have this role', () => {
    expect(TestingCardsList.getDeleteButton(0).present).toBeFalsy();
  });

  it('should display [EDIT] button even if user doesn\'t have this role', () => {
    expect(TestingCardsList.getEditButton(0).present).toBeTruthy();
  });

  it('should open detail page in read-only mode if user click on [EDIT] button', () => {
    TestingCardsList.getEditButton(0).click();
    since('edit section should be displayed').expect(TestingCardsEdit.root.displayed).toBeTruthy();
    since('[SAVE] button should be hidden').expect(TestingCardsEdit.saveButton.present).toBeFalsy();

    // All fields are disabled
    since('lotName input should be disabled').expect(TestingCardsEdit.lotName.enabled).toBeFalsy();
    since('firstName input should be disabled').expect(TestingCardsEdit.firstName.enabled).toBeFalsy();
    since('lastName input should be disabled').expect(TestingCardsEdit.lastName.enabled).toBeFalsy();
    since('address input should be disabled').expect(TestingCardsEdit.address.enabled).toBeFalsy();
    since('address2 input should be disabled').expect(TestingCardsEdit.address2.enabled).toBeFalsy();
    since('street input should be disabled').expect(TestingCardsEdit.street.enabled).toBeFalsy();
    since('postalCode input should be disabled').expect(TestingCardsEdit.postalCode.enabled).toBeFalsy();
    since('city input should be disabled').expect(TestingCardsEdit.city.enabled).toBeFalsy();
    since('country input should be disabled').expect(TestingCardsEdit.country.enabled).toBeFalsy();
    since('cardType input should be disabled').expect(TestingCardsEdit.cardType.enabled).toBeFalsy();
    since('signature input should be disabled').expect(TestingCardsEdit.signature.enabled).toBeFalsy();
    since('language input should be disabled').expect(TestingCardsEdit.language.enabled).toBeFalsy();
    since('doi input should be disabled').expect(TestingCardsEdit.doi.enabled).toBeFalsy();
    since('count input should be disabled').expect(TestingCardsEdit.count.enabled).toBeFalsy();
    since('shouldPrint input should be disabled').expect(TestingCardsEdit.shouldPrint.disabled).toBeTruthy();
    since('birthday input should be disabled').expect(TestingCardsEdit.birthday.enabled).toBeFalsy();
    ParameterEditPage.clickBackLink();
  });

});
