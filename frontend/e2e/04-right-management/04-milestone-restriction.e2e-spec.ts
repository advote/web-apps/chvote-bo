/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Main } from '../page-object/main.po';
import { MilestoneEdit } from '../page-object/operation/parameters/milestone-edit.po';
import { OperationManagementPage } from '../page-object/operation/operation-management.po';
import { ParameterEditPage } from '../page-object/operation/parameters/parameter-edit.po';
let since = require('jasmine2-custom-message');

describe('Right Management/Milestone', () => {

  beforeAll(() => {
    Main.login('ge4');
    Main.createOperationAndNavigate('operations/[OPERATION_ID]/parameters');
  });

  it('all user interactions should be disabled if user doesn\'t have this role', () => {
    OperationManagementPage.milestoneCard.button.click();
    since('site validation input should be disabled').expect(MilestoneEdit.milestoneInput('SITE_VALIDATION').enabled).toBeFalsy();
    since('certification input should be disabled').expect(MilestoneEdit.milestoneInput('CERTIFICATION').enabled).toBeFalsy();
    since('printer files input should be disabled').expect(MilestoneEdit.milestoneInput('PRINTER_FILES').enabled).toBeFalsy();
    since('ballot box init input should be disabled').expect(MilestoneEdit.milestoneInput('BALLOT_BOX_INIT').enabled).toBeFalsy();
    since('site open input should be disabled').expect(MilestoneEdit.milestoneInput('SITE_OPEN').enabled).toBeFalsy();
    since('site close input should be disabled').expect(MilestoneEdit.milestoneInput('SITE_CLOSE').enabled).toBeFalsy();
    since('ballot box decrypt input should be disabled').expect(MilestoneEdit.milestoneInput('BALLOT_BOX_DECRYPT').enabled).toBeFalsy();
    since('result validation input should be disabled').expect(MilestoneEdit.milestoneInput('RESULT_VALIDATION').enabled).toBeFalsy();
    since('data desctruction input should be disabled').expect(MilestoneEdit.milestoneInput('DATA_DESTRUCTION').enabled).toBeFalsy();
    since('grace period input should be disabled').expect(MilestoneEdit.gracePeriodInput.enabled).toBeFalsy();
    ParameterEditPage.clickBackLink();
  });

});
