# CHVote-2.0 BackOffice Frontend project

## Continuous Integration Test Reports

https://chvote2.gitlab.io/chvote-bo/

## Must read

- [Coding style for FrontEnd development](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/development-directives/coding-style-FrontEnd.md)

## Prerequisites

* Install NodeJS and NPM - https://nodejs.org/en/download/
* Install Angular CLI plugin

`npm install -g @angular/cli`

## Install required node modules.


Inside project directory :

`npm install`


## UI Components to use

* All Angular Material components 

* DateTime Picker : Angular 2 Datetime Picker
  * [Site](https://github.com/ng2-ui/datetime-picker) 

## Global error handler

Unhandled JavaScript errors are catched by `GlobalErrorHandled` which :
* logs error to the browser console
* calls a REST endpoint of the server to centralize logs, in order to ease diagnostic. 

see `GlobalErrorHandler` in frontend project and `FrontEndLoggerController` in backend project

## Server error handling

HttpService must be used to call remote REST services, as it defines a global error handling strategy.

see `HttpService`

### Business errors

Business validation errors are received as HTTP responses with status code 400 and a ValidationErrorModel payload.

Calling component should declare an error handler to handle known business validation errors. To do so, one must set 'errorHandled' property to true.
```
  retrieveOperations(): void {
    this.operationService.findAll().subscribe(
      operations => {
        // on success handler
      },
      err => {
        let error = err as ValidationErrorModel;
        if (error.exceptionClassName.indexOf("ImportOperationRepositoryException")) {
          // do something with business error

          // mark the error as being handled
          error.errorHandled=true;
        }
      }
    );
  }
```

Error interceptor will check that property to determine if business error has been handled by the calling component. If not, a generic business error popup will be shown.

see `BusinessErrorDialogComponent`

### Technical errors

Technical errors (typically RuntimeException) are received as HTTP responses with status code 500 and a TechnicalErrorVo payload.

Error interceptor will first display a technical error dialog box and then call component's error handler.

see `TechnicalErrorDialogComponent`

## I18N

### Selected component
Angular native I18N is still in beta and requires to bundle one application per language. It does not allow to change language on the fly.
 
We use [ngx-translate](https://github.com/ngx-translate/core) instead.

### Usage

* pipe : `{{ 'footer.content' | translate:footerParam }}` 
* service : `translate.get('HELLO', {value: 'world'}).subscribe((res: string) => { console.log(res); });`
* directive : `<div [translate]="'HELLO'" [translateParams]="{value: 'world'}"></div>`

### Translation files
#### Location

Translation file are modularized by component. They are written in typescript and are merged in a global i18n.ts file

## Resources
### Angular Documentation

[Architecture overview](https://angular.io/docs/ts/latest/guide/architecture.html)

[Template syntax](https://angular.io/docs/ts/latest/guide/template-syntax.html)

[Forms](https://angular.io/docs/ts/latest/guide/forms.html)

[Form Validation](https://angular.io/docs/ts/latest/cookbook/form-validation.html)

[Reactive Forms](https://angular.io/docs/ts/latest/guide/reactive-forms.html)

[Testing](https://angular.io/docs/ts/latest/guide/testing.html)

[Component interaction](https://angular.io/docs/ts/latest/cookbook/component-communication.html)

[Dependency Injection](https://angular.io/docs/ts/latest/cookbook/dependency-injection.html)

[Security](https://angular.io/docs/ts/latest/guide/security.html)

[Routing and Navigation](https://angular.io/docs/ts/latest/guide/router.html)

[Modules](https://angular.io/docs/ts/latest/guide/ngmodule.html)

[HTTP Server communication](https://angular.io/docs/ts/latest/guide/server-communication.html)

[CheatSheet](https://angular.io/docs/ts/latest/guide/cheatsheet.html)

### Angular Material

[Components](https://material.angular.io/components)

[Feature status](https://github.com/angular/material2#feature-status)

### Terradata Covalent components

[Covalent home page](https://teradata.github.io/covalent/#/)

### Angular 2 Flex Layout

[Angular 2 Flex Layout demo](https://tburleson-layouts-demos.firebaseapp.com/#/docs)

[Angular 2 Flex Layout demo source code](https://github.com/angular/flex-layout/tree/master/src/demo-app/app)

[Angular 2 Flex Layout documentation](https://github.com/angular/flex-layout/wiki/API-Documentation)

## Flexbox Layout

[A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
