/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { DataSource } from "@angular/cdk/collections";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { MatSort } from "@angular/material";
import "rxjs/add/operator/map";
import "rxjs/add/observable/merge";
import { map, merge } from "rxjs/operators";
import { Operation } from "../../operation/model/operation";
import { OperationService } from "../../operation/service/operation.service";
import { sortOnProperty } from "../../core/util/table-utils";
import { AuthorizationService } from "../../core/service/http/authorization.service";
import { GeneratedVotingMaterialService } from "../../operation/service/generated-voting-material.service";
import { FileService } from "../../core/service/file-service/file-service";
import { HttpParameters } from '../../core/service/http/http.parameters.service';

@Component({
  selector: 'operation-list',
  templateUrl: './operation-list.component.html',
  styleUrls: ['./operation-list.component.scss']
})
export class OperationListComponent implements OnInit {
  allOperations = new BehaviorSubject<Operation[]>([]);
  @ViewChild(MatSort) sort: MatSort;
  dataSource;
  displayedColumns;

  constructor(private operationService: OperationService,
              private authorizationService: AuthorizationService,
              private fileService: FileService,
              private httpParams: HttpParameters,
              private generatedVotingMaterialService: GeneratedVotingMaterialService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.displayedColumns = this.authorizationService.hasAtLeastOneRole(["ROLE_PRINTER"]) ?
      ['date', 'shortLabel', 'longLabel', 'printerArchiveInfo', 'printerArchiveDownload']
      : ['date', 'shortLabel', 'longLabel', 'management'];

    this.refreshOperations();
    this.dataSource = new OperationsDataSource(this.allOperations, this.sort);
  }

  refreshOperations() {
    this.operationService.findAll().subscribe((operations => this.allOperations.next(operations)));
  }

  public management(id: number): void {
    this.router.navigate(["operations", id]);
  }

  public getDownloadLink(token: string) {

    return `${this.httpParams.apiBaseURL}/download-with-token/${token}`;
  }
}

class OperationsDataSource extends DataSource<any> {

  constructor(private operations: BehaviorSubject<Operation[]>, private sort: MatSort) {
    super();
  }

  connect(): Observable<Operation[]> {
    return this.operations.pipe(
      merge(this.sort.sortChange),
      map(() => sortOnProperty(this.operations.getValue(), this.sort.active, this.sort.direction))
    );

  }

  disconnect() {
  }
}
