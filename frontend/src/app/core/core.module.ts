/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { ErrorHandler, NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { BusinessErrorDialogComponent } from "./business-error-dialog/business-error-dialog.component";
import { TechnicalErrorDialogComponent } from "./technical-error-dialog/technical-error-dialog.component";
import { MatButtonModule, MatDialogModule, MatIconModule, MatProgressSpinnerModule } from "@angular/material";
import { MissingTranslationHandler, TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { BOMissingTranslationHandler } from "./bo-missing-translation-handler";
import { FlexLayoutModule } from "@angular/flex-layout";
import { RemoteLoggerService } from "./service/logger/remote.logger.service";
import { GlobalErrorHandler } from "./error-handler/global.error-handler";
import { BaseUrlService } from './service/base-url/base-url.service';
import { UploadService } from './service/upload/upload.service';
import { OperationResolver } from '../operation/service/operation-resolver';
import { OperationService } from '../operation/service/operation.service';
import { FileService } from './service/file-service/file-service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpParameters } from './service/http/http.parameters.service';
import { InProgressInterceptor, WaitingCover } from './service/http/InProgressInterceptor';
import { Observable } from 'rxjs/Observable';
import { i18n } from '../i18n/i18n';
import { AuthorizationGuard, AuthorizationService } from './service/http/authorization.service';
import { AuthenticationInterceptor } from './service/http/authentication.interceptor';
import { DateInterceptor } from './service/http/DateInterceptor';
import { ErrorInterceptor } from './service/http/ErrorInterceptor';
import { NoCacheInterceptor } from './service/http/NoCacheInterceptor';
import { of } from 'rxjs/observable/of';
import { ConfirmBeforeQuitGuard } from './confirm-before-quit';
import { DictionaryService } from './service/dictionary.service';


class ConstTranslateLoader extends TranslateLoader {
  constructor(private i18n: any) {
    super();
  }

  getTranslation(lang: string): Observable<any> {
    return of(this.i18n[lang]);
  }
}

@NgModule({
  imports: [
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    CommonModule,
    FlexLayoutModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useValue: new ConstTranslateLoader(i18n),
      },
      missingTranslationHandler: {provide: MissingTranslationHandler, useClass: BOMissingTranslationHandler}
    })
  ],
  declarations: [TechnicalErrorDialogComponent, BusinessErrorDialogComponent, WaitingCover],
  entryComponents: [TechnicalErrorDialogComponent, BusinessErrorDialogComponent, WaitingCover],
  providers: [
    AuthorizationService,
    DatePipe,
    HttpParameters,
    DictionaryService,
    BaseUrlService,
    RemoteLoggerService,
    {provide: ErrorHandler, useClass: GlobalErrorHandler},
    UploadService,
    FileService,
    OperationResolver,
    OperationService,
    AuthorizationGuard,
    ConfirmBeforeQuitGuard,
    {provide: HTTP_INTERCEPTORS, useClass: DateInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: NoCacheInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: InProgressInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true}
  ],

  exports: [HttpClientModule]
})
export class CoreModule {
}
