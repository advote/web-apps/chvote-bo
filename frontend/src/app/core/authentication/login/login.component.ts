/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { AuthorizationService } from '../../service/http/authorization.service';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  credentialError: boolean;

  constructor(private authenticationService: AuthenticationService,
              private authorizationService: AuthorizationService,
              private router: Router) {
  }

  login(credentials: Credentials): void {
    this.authenticationService.login(credentials.username, credentials.password)
      .subscribe(() => {
        this.router.navigate([this.authorizationService.lastVisited || "/"]);
      }, () => {
        this.authenticationService.logout();
        this.credentialError = true;
      })

  }
}


interface Credentials {
  username: string;
  password: string;
}
