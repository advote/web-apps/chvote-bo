/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { BaseUrlService } from '../base-url/base-url.service';
import { HttpResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { FieldValidationErrorModel } from './field.validation.error.model';
import { GlobalValidationErrorModel } from './global.validation.error.model';
import { padNumber } from '../../util/utils';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class HttpParameters {
  public apiBaseURL;
  public stopBackgroundRequest = new BehaviorSubject<boolean>(false);
  /**
   * Translate error message using translation service. Translated message is stored in error.message
   * @param validationErrorModel
   */
  public translateMessages = (validationErrorModel) => {
    validationErrorModel.globalErrors.forEach(globalError => {
      let paramMap = this.paramListToParamObject(globalError);
      this.translate.get(globalError.messageKey, paramMap).subscribe((translatedMessage: string) => {
        globalError.message = translatedMessage;
      });
    });
    validationErrorModel.fieldErrors.forEach(fieldError => {
      let paramMap = this.paramListToParamObject(fieldError);
      this.translate.get(fieldError.messageKey, paramMap).subscribe((translatedMessage: string) => {
        fieldError.message = translatedMessage;
      });
    });
  };
  /**
   * Convert a list of string into an object with properties named param<index>
   * @param error
   * @returns an object whose properties are named param<index>, one property for each message params of error
   */
  private paramListToParamObject = (error: GlobalValidationErrorModel | FieldValidationErrorModel) => {
    let paramMap = {};
    if (error.messageParams) {
      let i: number = 0;
      error.messageParams.forEach(param => {
        paramMap["param" + i] = param;
        i++
      });
    }
    return paramMap;
  };

  constructor(private baseUrlService: BaseUrlService, private translate: TranslateService) {
    this.apiBaseURL = baseUrlService.getBackendBaseUrl();
    Date.prototype.toJSON = function () {
      let day = padNumber(this.getDate(), 2);
      let month = padNumber(this.getMonth() + 1, 2);
      let hours = padNumber(this.getHours(), 2);
      let minutes = padNumber(this.getMinutes(), 2);
      let seconds = padNumber(this.getSeconds(), 2);
      let milliseconds = padNumber(this.getMilliseconds(), 3);
      return `JSDate[${day}.${month}.${this.getFullYear()} ${hours}:${minutes}:${seconds}.${milliseconds}]`;
    };
  }

  /**
   * Parse an HTTP response header to extract the file name.
   *
   * @param response the HTTP response
   * @returns {String} the filename extracted from the response
   */
  static getFileNameFromHeader(response: HttpResponse<any>): string {
    let header = response.headers.get('content-disposition');
    let fileName = header.split(";")[1].trim().split("=")[1];
    return fileName.replace(/"/g, '');
  }

}
