/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { MatDialog } from "@angular/material";
import { GlobalValidationErrorModel } from "./global.validation.error.model";
import { TechnicalErrorModel } from "./technical.error.model";
import { ErrorInterceptor } from './ErrorInterceptor';
import { HttpParameters } from './http.parameters.service';
import { HttpHandler, HttpRequest } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { _throw as throwObservable } from 'rxjs/observable/throw';


const shouldNotBeCalled = res => fail("On success should not be called !");
describe('ErrorInterceptor', () => {

  let dialogMock: MatDialog;
  let httpParametersMock: HttpParameters = <any>{
    translateMessages: jasmine.createSpy("translateMessages")
  };

  let errorInterceptor: ErrorInterceptor;

  beforeEach(() => {
    dialogMock = <any>{
      open: jasmine.createSpy("open").and.returnValue({
        afterClosed: () => new Subject()
      })
    };
    errorInterceptor = new ErrorInterceptor(httpParametersMock, dialogMock);
  });


  describe('businessError', () => {


    let nextIsBusinessError: HttpHandler = {
      handle: (req: HttpRequest<any>) => throwObservable({
        status: 400,
        error: JSON.stringify({
          exceptionClassName: "BusinessException",
          globalErrors: [new GlobalValidationErrorModel("business.error.key")]
        })
      })
    };


    it('should show a dialog when business error is not handled by initiator', () => {
      errorInterceptor.intercept(null, nextIsBusinessError)
        .subscribe(res => fail("On success should not be called !"), err => {
        });
      expect(dialogMock.open).toHaveBeenCalled();
    });


    it('should not show a dialog when business error is handled by initiator', () => {

      errorInterceptor.intercept(null, nextIsBusinessError)
        .subscribe(shouldNotBeCalled, err => {
          err
            .onValidationError("thisOneGetNotCalledException ", shouldNotBeCalled)
            .onValidationError("BusinessException", error => expect(error).not.toBeNull())
        });
      expect(dialogMock.open).not.toHaveBeenCalled();
    });

  });

  describe('technicalError', () => {
    let nextIsTechnicalError: HttpHandler = {
      handle: (req: HttpRequest<any>) => throwObservable({
        status: 500,
        error: JSON.stringify(new TechnicalErrorModel(500, "global.errors.unexpected"))
      })
    };

    it('should show a dialog when technical error is not handled by initiator', () => {
      errorInterceptor.intercept(null, nextIsTechnicalError)
        .subscribe(shouldNotBeCalled, err => {
        });

      expect(dialogMock.open).toHaveBeenCalled();
    });


    it('should not show a dialog when technical error is handled by initiator', () => {
      errorInterceptor.intercept(null, nextIsTechnicalError)
        .subscribe(res => fail("On success should not be called !"), err => {
          err.onTechnicalError("global.errors.unexpected", error => expect(error).not.toBeNull())
        });
      expect(dialogMock.open).not.toHaveBeenCalled();
    });
  });

});

