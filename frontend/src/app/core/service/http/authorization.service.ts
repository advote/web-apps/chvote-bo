/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';


import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpParameters } from './http.parameters.service';
import { User } from '../../model/user';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthorizationService {


  private _userSnapshot: User;
  public userChange = new BehaviorSubject<User>(null);
  private serviceUrl: string;
  lastVisited: string;

  constructor(private http: HttpClient, private httpParameters: HttpParameters) {
    this.serviceUrl = this.httpParameters.apiBaseURL + '/user';
  }

  updateUser(): Observable<User> {
    this.userSnapshot = null;

    return this.http
      .get<User>(`${this.serviceUrl}/me`)
      .pipe(tap(u => this.userSnapshot = u));
  }

  clear() {
    this.userSnapshot = null;
  }

  hasAtLeastOneRole(roles: string[]) {
    return !!roles.find(
      role => this.userSnapshot.roles.indexOf(role) >= 0 || this.userSnapshot.roles.indexOf("ROLE_" + role) >= 0)
  }

  set userSnapshot(value: User) {
    this.userChange.next(value);
    this._userSnapshot = value;
  }

  get userSnapshot(): User {
    return this._userSnapshot;
  }

}


@Injectable()
export class AuthorizationGuard implements CanActivate, CanActivateChild {

  constructor(private authService: AuthorizationService) {

  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.authService.userSnapshot) {
      return true;
    }
    return this.authService.updateUser().pipe(
      mapTo(true),
      catchError(() => {
        this.authService.lastVisited = state.url;
        return of(false)
      })
    );
  }

  canActivateChild(route: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
