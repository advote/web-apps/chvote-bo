/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpParameters } from './http/http.parameters.service';

@Injectable()
export class DictionaryService {
  public serviceUrl;

  constructor(private http: HttpClient, private params: HttpParameters) {
    this.serviceUrl = this.params.apiBaseURL;
  }

  getCandidateInformationDisplayModels(operationId): Observable<string[]> {
    return this.http.get<string[]>(`${this.serviceUrl}/${operationId}/candidate-information-display-models`);
  }

  getColumnsForVerificationsCodes(operationId): Observable<string[]> {
    return this.http.get<string[]>(`${this.serviceUrl}/${operationId}/columns-for-verification-codes`);
  }

  getDefaultLang(operationId): Observable<string> {
    return this.http.get<string>(`${this.serviceUrl}/${operationId}/default-lang`);
  }

}
