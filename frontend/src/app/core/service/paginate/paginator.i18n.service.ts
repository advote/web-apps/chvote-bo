/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatPaginatorIntl } from '@angular/material';

@Injectable()
export class PaginatorI18NService extends MatPaginatorIntl {

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length == 0 || pageSize == 0) {
      return `0 ${this.translate.instant('global.table.paginator.of')} ${length}`;
    }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;

    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;

    return `${startIndex + 1} - ${endIndex} ${this.translate.instant('global.table.paginator.of')} ${length}`;
  };

  constructor(private translate: TranslateService) {
    super();
    this.update();
    translate.onLangChange.subscribe(() => {
      this.update();
      this.changes.next()
    });

  }

  private update() {
    this.itemsPerPageLabel = this.translate.instant('global.table.paginator.itemsPerPageLabel');
    this.nextPageLabel = this.translate.instant('global.table.paginator.nextPageLabel');
    this.previousPageLabel = this.translate.instant('global.table.paginator.previousPageLabel');
  }

}
