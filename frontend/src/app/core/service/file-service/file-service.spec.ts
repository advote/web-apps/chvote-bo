/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { TranslateService } from '@ngx-translate/core';
import { FileService } from './file-service';
import { DatePipe } from '@angular/common';

describe('FileService', () => {

  const headersTranslated = {"test.field1": "field 1", "test.field2": "field 2"};


  let translationService: TranslateService = <any>{instant: key => headersTranslated[key]};
  let fileService = new FileService(translationService, new DatePipe("fr"));

  it("should generate valid CSV file", () => {
    let csv = fileService.generateCsvFromFlatObject([
      {
        field2: "test2",
      },
      {
        field1: 'tes"t',
        field2: 3,
      },
    ], "test");
    expect(csv).toBe('\uFEFFfield 2;field 1\n' +
                     '"test2";\n' +
                     '3;"tes""t"');
  });


});
