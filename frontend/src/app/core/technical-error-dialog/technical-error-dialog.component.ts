/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { HttpParameters } from '../service/http/http.parameters.service';

@Component({
  selector: 'app-technical-error-dialog',
  templateUrl: './technical-error-dialog.component.html',
  styleUrls: ['./technical-error-dialog.component.scss']
})
export class TechnicalErrorDialogComponent {


  constructor(@Inject(MAT_DIALOG_DATA) public data: any, httpParameters: HttpParameters,
              dialogRef: MatDialogRef<TechnicalErrorDialogComponent>) {
    dialogRef.afterOpen().subscribe(() => httpParameters.stopBackgroundRequest.next(true));
    dialogRef.afterClosed().subscribe(() => httpParameters.stopBackgroundRequest.next(false))
  }


  get message() {
    return this.data.message || 'global.technical-error-dialog.content';
  }

}
