/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material";

@Component({
  selector: 'app-business-error-dialog',
  templateUrl: './business-error-dialog.component.html'
})
export class BusinessErrorDialogComponent {

  public isFieldValidationError: boolean;
  public errorMessages: Array<string> = new Array(0);

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    if (data.fieldValidationError === true) {
      this.isFieldValidationError = true;
      data.fieldErrors.forEach(fieldError => {
        this.errorMessages.push(fieldError.message);
      });
    } else {
      this.isFieldValidationError = false;
      data.globalErrors.forEach(globalError => {
        this.errorMessages.push(globalError.message);
      });
    }
  }
}
