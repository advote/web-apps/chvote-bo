/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TdDialogService } from '@covalent/core';

export interface ConfirmBeforeQuit {
  isFormModified(): boolean;
}


@Injectable()
export class ConfirmBeforeQuitGuard implements CanDeactivate<ConfirmBeforeQuit> {


  constructor(private translateService: TranslateService, private dialogService: TdDialogService) {
  }

  canDeactivate(component: ConfirmBeforeQuit) {
    if (component && component.isFormModified()) {
      return this.dialogService.openConfirm({
        message: this.translateService.instant('global.actions.quit-form-confirm'),
        disableClose: true,
        cancelButton: this.translateService.instant('global.actions.no'),
        acceptButton: this.translateService.instant('global.actions.yes'),
      }).afterClosed()
    }

    return true;
  }


}
