/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { paginate, sortOnProperty } from "./table-utils";


describe('sortOnProperty', () => {
  const arrayUnderTest = [
    {id: 1, aString: "110a", aNumberAsString: "110", aDate: new Date(2010, 1), aNumber: 110},
    {id: 2, aString: "2a", aNumberAsString: "2", aDate: new Date(2010, 5), aNumber: 2},
    {id: 3, aString: "00a", aNumberAsString: "00", aDate: new Date(2010, 3), aNumber: 4},
  ];

  it("should support strings", () => {
    expect(sortOnProperty(arrayUnderTest, "aString", 'asc').map(v => v.id)).toEqual([3, 1, 2]);
    expect(sortOnProperty(arrayUnderTest, "aString", 'desc').map(v => v.id)).toEqual([2, 1, 3]);
    expect(sortOnProperty(arrayUnderTest, "aString", '').map(v => v.id)).toEqual([1, 2, 3]);
  });

  it("should support numbers that are typed as string", () => {
    expect(sortOnProperty(arrayUnderTest, "aNumberAsString", 'asc').map(v => v.id)).toEqual([3, 2, 1]);
    expect(sortOnProperty(arrayUnderTest, "aNumberAsString", 'desc').map(v => v.id)).toEqual([1, 2, 3]);
    expect(sortOnProperty(arrayUnderTest, "aNumberAsString", '').map(v => v.id)).toEqual([1, 2, 3]);
  });

  it("should support dates", () => {
    expect(sortOnProperty(arrayUnderTest, "aDate", 'asc').map(v => v.id)).toEqual([1, 3, 2]);
    expect(sortOnProperty(arrayUnderTest, "aDate", 'desc').map(v => v.id)).toEqual([2, 3, 1]);
    expect(sortOnProperty(arrayUnderTest, "aDate", '').map(v => v.id)).toEqual([1, 2, 3]);
  });

  it("should support numbers", () => {
    expect(sortOnProperty(arrayUnderTest, "aNumber", 'asc').map(v => v.id)).toEqual([2, 3, 1]);
    expect(sortOnProperty(arrayUnderTest, "aNumber", 'desc').map(v => v.id)).toEqual([1, 3, 2]);
    expect(sortOnProperty(arrayUnderTest, "aNumber", '').map(v => v.id)).toEqual([1, 2, 3]);
  });

  it("should support no sorting", () => {
    expect(sortOnProperty(arrayUnderTest, "", 'asc').map(v => v.id)).toEqual([1, 2, 3]);
    expect(sortOnProperty(arrayUnderTest, null, 'asc').map(v => v.id)).toEqual([1, 2, 3]);
    expect(sortOnProperty(arrayUnderTest, "", 'desc').map(v => v.id)).toEqual([1, 2, 3]);
    expect(sortOnProperty(arrayUnderTest, "", '').map(v => v.id)).toEqual([1, 2, 3]);
  });
});

describe('paginate', () => {
  it("paginate an array", () => {
    let array = [0, 1, 2, 3];
    expect(paginate(array, 0, 2)).toEqual([0, 1]);
    expect(paginate(array, 1, 2)).toEqual([2, 3]);


  });
});




