/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export function sortOnProperty(array: any[], property: string, direction: string) {
  if (!property || direction == '') {
    return array;
  }
  return array.slice().sort((a, b) => {
    let propertyA: Date | number | string = a[property];
    let propertyB: Date | number | string = b[property];
    let valueA = isNaN(+propertyA) ? propertyA : +propertyA;
    let valueB = isNaN(+propertyB) ? propertyB : +propertyB;
    return (valueA < valueB ? -1 : 1) * (direction == 'asc' ? 1 : -1);
  });
}


export function paginate(array: any[], pageIndex, pageSize) {
  const data = array.slice();
  const startIndex = pageIndex * pageSize;
  return data.splice(startIndex, pageSize);
}
