/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { VotingSiteConfigurationService } from '../voting-site-configuration.service';
import { ConfirmBeforeQuit } from '../../../../core/confirm-before-quit';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { enableFormComponent, enableFormComponents, isFormModifiedAndNotSaved } from '../../../../core/util/form-utils';
import { Operation } from '../../../model/operation';
import { flatMap, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { VotingSiteConfiguration } from '../voting-site-configuration';
import { DictionaryService } from '../../../../core/service/dictionary.service';
import { ReadOnlyService } from '../../../service/read-only.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'voting-site-configuration-edit',
  templateUrl: './voting-site-configuration.component.html',
  styleUrls: ['./voting-site-configuration.component.scss']
})
export class VotingSiteConfigurationComponent implements OnInit, ConfirmBeforeQuit, OnDestroy {

  form: FormGroup;
  readOnly: boolean;

  private saved = false;
  private operation: Operation;
  private config: VotingSiteConfiguration;
  private subscriptions: Subscription[] = [];


  constructor(private service: VotingSiteConfigurationService,
              private formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private dictionaryService: DictionaryService,
              private readOnlyService: ReadOnlyService,
              private operationManagementService: OperationManagementService) {

  }

  get ready() {
    return !!this.config;
  }

  get allPossibleLanguages() {
    return ['DE', 'FR', 'IT', 'RM'];
  }

  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.form, this.saved);
  }

  ngOnInit() {
    this.subscriptions.push(
      this.readOnlyService.isVotingSiteConfigurationInReadonly()
        .subscribe(readOnly => {
          this.readOnly = readOnly;
          this.updateForReadonly();
        }),
      this.operationManagementService.operation
        .pipe(
          tap(operation => this.operation = operation),
          flatMap(operation =>this.service.findForOperation(operation.id)),
          flatMap(config => {
            if (!config) {
              return this.dictionaryService.getDefaultLang(this.operation.id)
                .pipe(map(defaultLanguage => {
                  return {defaultLanguage: defaultLanguage, allLanguages: [defaultLanguage]}
                }))
            }
            return of(config)
          }),
          tap(config => this.config = config)
        )
        .subscribe(config => {
          let controlConfig = {
            defaultLanguage: [config.defaultLanguage, Validators.required],
          };
          this.allPossibleLanguages.forEach(lang => {
            controlConfig[lang] = [this.config.allLanguages.indexOf(lang) >= 0]
          });
          this.form = this.formBuilder.group(controlConfig);
          this.updateForReadonly();
        })
    )
  }

  save() {
    this.form.controls.defaultLanguage.markAsTouched();
    this.form.updateValueAndValidity();

    if (this.form.valid) {
      let values = this.form.getRawValue();
      let votingSiteConfiguration: VotingSiteConfiguration = {
        defaultLanguage: values.defaultLanguage,
        allLanguages: this.allPossibleLanguages.filter(l => values[l])
      };

      this.service.saveOrUpdate(this.operation.id, votingSiteConfiguration)
        .subscribe(() => {
          this.operationManagementService.shouldUpdateStatus();
          this.saved = true;
          this.router.navigate(['../..'], {relativeTo: this.route});
        });
    }
  }

  changeDefaultLang() {
    this.form.controls[this.form.controls.defaultLanguage.value].setValue(true);
    this.updateForReadonly();
  }

  private updateForReadonly() {
    if (this.form) {
      enableFormComponents(this.form.controls, !this.readOnly);
      enableFormComponent(this.form.controls[this.form.controls.defaultLanguage.value], false);
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
