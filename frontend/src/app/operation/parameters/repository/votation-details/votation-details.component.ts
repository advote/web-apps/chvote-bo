/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit } from '@angular/core';
import { VotationRepositoryFile } from '../model/repository-file';
import { DatePipe } from '@angular/common';
import { DomainOfInfluence } from '../../../../model/domain-of-Influence.model';


@Component({
  selector: 'votation-details',
  templateUrl: './votation-details.component.html',
  styleUrls: ['./votation-details.component.scss']
})
export class VotationDetailsComponent implements OnInit {

  @Input() file: VotationRepositoryFile;

  @Input() title: string;
  @Input() allDoi: DomainOfInfluence[];
  repositoryDetailColumns = [
    {
      label: 'parameters.repository.edit.detail-grid.header.subject-position',
      field: 'ballotPosition',
      width: '30%'
    },
    {
      label: 'parameters.repository.edit.detail-grid.header.domain-of-influence',
      field: 'domainOfInfluence',
      width: '20%'
    },
    {
      label: 'parameters.repository.edit.detail-grid.header.answer-type',
      field: 'acceptedAnswers',
      width: '50%',
      translate: true
    }
  ];

  constructor(private datePipe: DatePipe) {
  }

  get details() {
    return this.file.details.map(
      details => Object.assign({}, details, {domainOfInfluence: this.getDoiName(details.domainOfInfluence)}));
  }

  ngOnInit() {
  }

  getDoiName(doiId: string) {
    let doi = this.allDoi.find(doi => doi.id == doiId && doi.type !== "Unknown");
    return doi ? doi.name : doiId;
  }


}
