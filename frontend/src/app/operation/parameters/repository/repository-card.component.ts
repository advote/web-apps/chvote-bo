/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { RepositoryFile } from './model/repository-file';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';
import { OperationManagementService } from '../../service/operation-managment.service';
import { ReadOnlyService } from '../../service/read-only.service';

@Component({
  selector: 'repository-card',
  templateUrl: './repository-card.component.html'
})
export class RepositoryCardComponent implements OnInit, OnDestroy {
  @Input() files: RepositoryFile[];
  completed = false;
  inError = false;
  private _subscriptions: Subscription[] = [];

  constructor(private readOnlyService: ReadOnlyService,
              private translateService: TranslateService,
              private operationManagementService: OperationManagementService) {
  }

  private _readOnly: boolean;

  get readOnly() {
    return this._readOnly;
  }

  get votationRepositoryCount(): string {
    return this.getI18nCount(true);
  }

  get electionRepositoryCount(): string {
    return this.getI18nCount(false);
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnInit() {
    this._subscriptions.push(
      this.operationManagementService.status.subscribe(status => {
        this.completed = status.configurationStatus.completedSections["repository"];
        this.inError = status.configurationStatus.sectionsInError["repository"];
      }),
      this.readOnlyService.isRepositoryInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
    )
  }

  getI18nCount(forVotation: boolean) {
    let count = 0;
    if (this.files) {
      count = this.files.filter(f => forVotation == (f.type == "VOTATION_REPOSITORY")).length;
    }

    if (count === 0) {
      return this.translateService.instant('parameters.repository.card.no-repository');
    }
    if (count === 1) {
      return this.translateService.instant('parameters.repository.card.1-repository');
    }
    return this.translateService.instant('parameters.repository.card.n-repository',
      {count: this.files.length});
  }


}
