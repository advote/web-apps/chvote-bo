/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Base } from "../../../../core/model/base";
import { VoteInformation } from "./vote-information";

/**
 * Model representing an operation's repository file.
 */
export class RepositoryFile extends Base {
  managementEntity: string;
  contestIdentification: string;
  contestDate: Date;
  contestDescription: string;
  fileName: string;
  type: string;
}

export class VotationRepositoryFile extends RepositoryFile {
  details: VoteInformation[];
}


export class ElectionRepositoryFile extends RepositoryFile {
  electionInformationList: ElectionInformation[];
}


export class ElectionInformation {
  doiId: string;
  typeOfElection: string;
  ballot: string;
  numberOfMandates: number;
  mandate: string;
  listCount: number;
  candidatureCount: number;
  candidateCount: number;
  lists: ElectionInformationList[];
}

export class ElectionInformationList {
  indentureNumber: string;
  name: string;
  inAnUnion: boolean;
  candidateCount: number;
}
