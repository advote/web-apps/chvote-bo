/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatSort, MatTableDataSource } from '@angular/material';
import { ElectionInformation } from '../model/repository-file';
import { ElectionDetailsElectoralListComponent } from './election-details-electoral-lists.component';


@Component({
  selector: 'election-details-table',
  templateUrl: './election-details-table.component.html',
  styleUrls: ['./election-details-table.component.scss']
})
export class ElectionDetailsTableComponent implements OnInit {
  @Input()
  dataSource: MatTableDataSource<ElectionInformation>;
  @Input()
  type: string;

  displayedColumns = [];
  hasList = false;

  constructor(private dialog: MatDialog) {


  }

  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    if (this.type == "MAJORITY") {
      this.displayedColumns = ["ballot", "mandate", "numberOfMandates", "candidateCount"];
    } else {
      this.displayedColumns =
        ["ballot", "mandate", "numberOfMandates", "listCount", "candidatureCount"];
      this.hasList = true;
    }
  }


  showLists(element: ElectionInformation) {
    this.dialog.open(ElectionDetailsElectoralListComponent, {data: element.lists, width: "77rem"});

  }

}
