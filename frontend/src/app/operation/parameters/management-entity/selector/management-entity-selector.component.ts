/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { ManagementEntityService } from '../service/management-entity.service';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { Operation } from '../../../model/operation';
import { Exceptions } from '../../../../core/model/exceptions';

export enum Mode {
  invite = "invite",
  revoke = "revoke",
}

export interface ManagementEntitySelectorData {
  mode: Mode,
  possibleChoices: string[];
  operation: Operation;
}

@Component({
  selector: 'management-entity-selector',
  templateUrl: './management-entity-selector.component.html',
  styleUrls: ['./management-entity-selector.component.scss']
})
export class ManagementEntitySelectorComponent {

  revokeError: string = null;
  dataSource = new MatTableDataSource();
  selection = new SelectionModel<any>(true, []);

  constructor(@Inject(MAT_DIALOG_DATA) private data: ManagementEntitySelectorData,
              private dialogRef: MatDialogRef<ManagementEntitySelectorComponent>,
              private operationManagementService: OperationManagementService,
              private managementEntityService: ManagementEntityService) {
    this.search("");
  }

  get displayedColumns() {
    return ["select", "managementEntity"];
  }

  get selectedValues() {
    return this.data.possibleChoices
  }

  get mode() {
    return this.data.mode.toString()
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected == numRows;
  }

  search(q) {
    this.dataSource.data =
      this.data.possibleChoices.filter(managementEntity =>
        managementEntity.toLocaleLowerCase().indexOf(q.toLocaleLowerCase()) >= 0)
        .map(managementEntity => {
          return {managementEntity}
        });

    this.selection.clear();
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  validate() {
    let selectedManagementEntities: string[] = this.selection.selected.map(v => v.managementEntity);

    if (this.data.mode == Mode.invite) {
      this.managementEntityService.invite(this.data.operation.id, selectedManagementEntities)
        .subscribe(operation => {
          this.operationManagementService.updateOperation(operation);
          this.dialogRef.close(this.selection.selected);
        });
    } else {
      this.managementEntityService.revoke(this.data.operation.id, selectedManagementEntities)
        .subscribe(
          operation => {
            this.operationManagementService.updateOperation(operation);
            this.dialogRef.close(this.selection.selected);
          },
          errorProvider => errorProvider.onValidationError(Exceptions.CANNOT_REVOKE,
            error => {
              this.revokeError = error.globalErrors[0].message;
            }
          )
        );
    }
  }


}
