/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { Subscription } from 'rxjs/Subscription';
import { ManagementEntityService } from '../service/management-entity.service';
import { Operation } from '../../../model/operation';
import { MatDialog } from '@angular/material';
import {
  ManagementEntitySelectorComponent, ManagementEntitySelectorData, Mode
} from '../selector/management-entity-selector.component';
import { ReadOnlyService } from '../../../service/read-only.service';

@Component({
  selector: 'management-entity-edit',
  templateUrl: './management-entity-edit.component.html',
  styleUrls: ['./management-entity-edit.component.scss']
})
export class ManagementEntityEditComponent implements OnInit, OnDestroy {

  possibleManagementEntities: string[];
  ready = false;
  private _readOnly: boolean;
  private subscriptions: Subscription[] = [];
  private operation: Operation;

  constructor(private operationManagementService: OperationManagementService,
              private managementEntityService: ManagementEntityService,
              private readOnlyService: ReadOnlyService,
              private dialog: MatDialog) {
  }

  get cardReadOnly(): boolean {
    return this._readOnly;
  }

  get readOnly(): boolean {
    return this._readOnly;
  }

  get guestManagementEntities() {
    return this.operation.guestManagementEntities;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnInit() {
    this.subscriptions.push(
      this.readOnlyService.isManagementEntityInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
      this.operationManagementService.operation.subscribe(operation => this.operation = operation),
      this.managementEntityService.getAllManagementEntities().subscribe(managementEntities => {
        this.possibleManagementEntities = managementEntities.filter(me => me != this.operation.managementEntity);
        this.ready = true;
      })
    )
  }

  openRevokePopup() {
    let data: ManagementEntitySelectorData = {
      mode: Mode.revoke,
      possibleChoices: this.operation.guestManagementEntities,
      operation: this.operation
    };
    this.dialog.open(ManagementEntitySelectorComponent, {data, disableClose: true});
  }

  openInvitePopup() {
    let data: ManagementEntitySelectorData = {
      mode: Mode.invite,
      possibleChoices: this.possibleManagementEntities
        .filter(e => this.operation.guestManagementEntities.indexOf(e) < 0),
      operation: this.operation
    };
    this.dialog.open(ManagementEntitySelectorComponent, {data, disableClose: true});
  }

}
