/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_managementEntity = {
    fr: {
      "card": {
        "title": "Entités de gestion",
        "subtitle": "Définir les entités de gestion invitées",
        "numberOfGuestEntities": "Entités de gestion invitées"
      },
      "list": {
        "title": "Liste des entités de gestion invitées sur l'opération",
        "no-guest-management-entity": "Aucune entité de gestion n'a été invitée",
        "all-management-entities-invited": "Toutes les entités de gestion du canton ont été invitées",
        "count": "Nombre d'entités de gestion invitées : "
      },
      "title": "Entités de gestion",
      "subtitle": "Définir les entités de gestion invitées",
      "actions": {
        "invite": "Inviter des entités de gestion",
        "revoke": "Révoquer des entités de gestion"
      },
      "dialog": {
        "form": {
          "fields": {
            "filter": {
              "placeholder": "Filtre"
            }
          }
        },
        "table": {
          "header": {
            "management-entity": "Entité de gestion"
          }
        },
        "invite": {
          "title": "Sélectionner les entités de gestion à inviter",
          "validate": "Inviter"
        },
        "revoke": {
          "title": "Sélectionner les entités de gestion à révoquer",
          "validate": "Révoquer"
        },
        "search": "recherche",
        "error": {
          "cannot-revoke": {
            "title": "Revocation impossible sur les entités sélectionnées",
            "one": "L'opération contient des élements importés par l'entité de gestion \"{{param0}}\"",
            "several": "L'opération contient des élements importés par les entités de gestion suivantes: {{param0}}"
          }
        }
      }
    },
    de: {
      "card": {
        "title": "DE - Entités de gestion",
        "subtitle": "DE - Définir les entités de gestion invitées",
        "numberOfGuestEntities": "DE - Entités de gestion invitées"
      },
      "list": {
        "title": "DE - Liste des entités de gestion invitées sur l'opération",
        "no-guest-management-entity": "DE - Aucune entité de gestion n'a été invitée",
        "all-management-entities-invited": "DE - Toutes les entités de gestion du canton ont été invitées",
        "count": "DE - Nombre d'entités de gestion invitées : "
      },
      "title": "DE - Entités de gestion",
      "subtitle": "DE - Définir les entités de gestion invitées",
      "actions": {
        "invite": "DE - Inviter des entités de gestion",
        "revoke": "DE - Révoquer des entités de gestion"
      },
      "dialog": {
        "form": {
          "fields": {
            "filter": {
              "placeholder": "DE - Filtre"
            }
          }
        },
        "table": {
          "header": {
            "management-entity": "DE - Entité de gestion"
          }
        },
        "invite": {
          "title": "DE - Sélectionner les entités de gestion à inviter",
          "validate": "DE - Inviter"
        },
        "revoke": {
          "title": "DE - Sélectionner les entités de gestion à révoquer",
          "validate": "DE - Révoquer"
        },
        "search": "DE - recherche",
        "error": {
          "cannot-revoke": {
            "title": "DE - Revocation impossible sur les entités sélectionnées",
            "one": "DE - L'opération contient des élements importés par l'entité de gestion \"{{param0}}\"",
            "several": "DE - L'opération contient des élements importés par les entités de gestion suivantes: {{param0}}"
          }
        }
      }
    }
  }
;
