/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Operation } from '../../../model/operation';
import { ManagementEntityService } from '../service/management-entity.service';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../../service/read-only.service';

@Component({
  selector: 'management-entity-card',
  templateUrl: './management-entity-card.component.html',
  styleUrls: ['./management-entity-card.component.scss']
})
export class ManagementEntityCardComponent implements OnInit, OnDestroy {
  private _subscriptions: Subscription[] = [];
  private _readOnly = false;
  ready = false;
  possibleGuestsCount = 0;
  completed = false;
  inError = false;

  get guestsCount(): number {
    return this.operation ? this.operation.guestManagementEntities.length : 0;
  }

  @Input() operation: Operation;

  constructor(private readOnlyService: ReadOnlyService,
              private service: ManagementEntityService,
              private operationManagementService: OperationManagementService) {
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  get readOnly() {
    return this._readOnly;
  }

  ngOnInit() {
    this._subscriptions.push(
      this.operationManagementService.status.subscribe(status => {
        this.completed = status.configurationStatus.completedSections["management-entity"];
        this.inError = status.configurationStatus.sectionsInError["management-entity"];
      }),
      this.readOnlyService.isManagementEntityInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
      this.service.getAllManagementEntities().subscribe(allManagementEntities => {
        this.possibleGuestsCount = allManagementEntities.length - 1;
        this.ready = true;
      })
    )
  }


}
