/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit } from "@angular/core";
import { Operation } from "../../model/operation";
import { OperationParametersModel } from "../model/operation-parameters.model";
import { Subscription } from "rxjs/Subscription";
import { OperationStatus } from "../../model/operation-status";
import { ReadOnlyService } from "app/operation/service/read-only.service";
import { Observable } from "rxjs/Observable";
import { OperationManagementService } from "../../service/operation-managment.service";
import * as _ from "underscore";
import { ElectionPagePropertiesService } from '../election-page-properties/services/election-page-properties.service';

@Component({
  templateUrl: './parameters-edit.component.html',
  styleUrls: ['./parameters-edit.component.scss']
})
export class ParametersEditComponent implements OnInit, OnDestroy {

  operation: Operation;
  operationStatus: OperationStatus;
  hasElection: boolean;
  hasElectionPageModel: boolean;
  public baseParameterInReadOnly: Observable<boolean>;
  public milestoneInReadOnly: Observable<boolean>;
  public documentInReadOnly: Observable<boolean>;
  public doiInReadOnly: Observable<boolean>;
  public managementEntityInReadOnly: Observable<boolean>;
  public repositoryInReadOnly: Observable<boolean>;
  public testingCardInReadOnly: Observable<boolean>;
  public ballotDocumentationInReadOnly: Observable<boolean>;
  public electionPagePropertiesInReadOnly: Observable<boolean>;
  public votingSiteConfigurationInReadOnly: Observable<boolean>;
  public baseParameterComplete = false;
  public milestoneComplete = false;
  public documentComplete = false;
  public doiComplete = false;
  public managementEntityComplete = false;
  public repositoryComplete = false;
  public testingCardComplete = false;
  public ballotDocumentationComplete = false;
  public electionPagePropertiesComplete = false;
  public votingSiteConfigurationComplete = false;
  public baseParameterInError = false;
  public milestoneInError = false;
  public documentInError = false;
  public doiInError = false;
  public managementEntityInError = false;
  public repositoryInError = false;
  public testingCardInError = false;
  public ballotDocumentationInError = false;
  public electionPagePropertiesInError = false;
  public votingSiteConfigurationInError = false;
  private subscriptions: Subscription[] = [];


  constructor(private readOnlyService: ReadOnlyService,
              private operationManagementService: OperationManagementService,
              private parametersModel: OperationParametersModel,
              private electionPagePropertiesService: ElectionPagePropertiesService) {

    this.baseParameterInReadOnly = readOnlyService.isBaseParameterInReadOnly();
    this.milestoneInReadOnly = readOnlyService.isMilestoneInReadOnly();
    this.documentInReadOnly = readOnlyService.isDocumentInReadOnly();
    this.doiInReadOnly = readOnlyService.isDoiInReadOnly();
    this.managementEntityInReadOnly = readOnlyService.isManagementEntityInReadOnly();
    this.repositoryInReadOnly = readOnlyService.isRepositoryInReadOnly();
    this.testingCardInReadOnly = readOnlyService.isTestingCardInReadOnly(true);
    this.ballotDocumentationInReadOnly = readOnlyService.isBallotDocumentationInReadOnly();
    this.electionPagePropertiesInReadOnly = readOnlyService.isElectionPagePropertiesInReadOnly();
    this.votingSiteConfigurationInReadOnly = readOnlyService.isVotingSiteConfigurationInReadonly();
  }

  get baseRouterLink() {
    return `/operations/${this.operation.id}/parameters`;
  }

  ngOnInit() {
    this.subscriptions.push(
      this.parametersModel.currentOperation.subscribe(operation => this.operation = operation),
      this.operationManagementService.status.subscribe(operationStatus => {
        this.operationStatus = operationStatus;

        const confStatus = operationStatus.configurationStatus;
        this.baseParameterComplete = confStatus.completedSections["base-parameter"];
        this.milestoneComplete = confStatus.completedSections["milestone"];
        this.documentComplete = confStatus.completedSections["document"];
        this.doiComplete = confStatus.completedSections["domain-of-influence"];
        this.managementEntityComplete = confStatus.completedSections["management-entity"];
        this.repositoryComplete = confStatus.completedSections["repository"];
        this.testingCardComplete = confStatus.completedSections["testing-card-lot"];
        this.ballotDocumentationComplete = confStatus.completedSections["ballot-document"];
        this.electionPagePropertiesComplete = confStatus.completedSections["election-page-properties"];
        this.votingSiteConfigurationComplete = confStatus.completedSections["voting-site-configuration"];

        this.baseParameterInError = confStatus.sectionsInError["base-parameter"];
        this.milestoneInError = confStatus.sectionsInError["milestone"];
        this.documentInError = confStatus.sectionsInError["document"];
        this.doiInError = confStatus.sectionsInError["domain-of-influence"];
        this.managementEntityInError = confStatus.sectionsInError["management-entity"];
        this.repositoryInError = confStatus.sectionsInError["repository"];
        this.testingCardInError = confStatus.sectionsInError["testing-card-lot"];
        this.ballotDocumentationInError = confStatus.sectionsInError["ballot-document"];
        this.electionPagePropertiesInError = confStatus.sectionsInError["election-page-properties"];
        this.votingSiteConfigurationInError = confStatus.sectionsInError["voting-site-configuration"];
      }),
      this.parametersModel.currentRepositoryFiles.subscribe(files => {
        this.hasElection = _.find(files, function (file) {
          return file.type === 'ELECTION_REPOSITORY';
        }) !== undefined;
        this.electionPagePropertiesService.getModelsForOperation(this.operation.id)
          .subscribe(models => this.hasElectionPageModel = models.length > 0);
      }),
    )
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
