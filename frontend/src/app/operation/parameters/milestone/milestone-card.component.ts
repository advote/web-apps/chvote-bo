/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { Operation } from '../../model/operation';
import { MilestoneType } from './model/milestone-type';
import * as _ from "underscore";
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../service/read-only.service';
import { OperationManagementService } from '../../service/operation-managment.service';

@Component({
  selector: 'milestone-card',
  styleUrls: ["./milestone-card.component.scss"],
  templateUrl: './milestone-card.component.html'
})
export class MilestoneCardComponent implements OnChanges, OnInit, OnDestroy {

  @Input() operation: Operation;
  milestonesByType;
  completed = false;
  inError = false;
  private _subscriptions: Subscription[] = [];

  constructor(private readOnlyService: ReadOnlyService,
              private operationManagementService: OperationManagementService) {
  }

  private _readOnly: boolean;

  get readOnly() {
    return this._readOnly;
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnInit() {
    this._subscriptions.push(
      this.operationManagementService.status.subscribe(status => {
        this.completed = status.configurationStatus.completedSections["milestone"];
        this.inError = status.configurationStatus.sectionsInError["milestone"];
      }),
      this.readOnlyService.isMilestoneInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
    )
  }

  ngOnChanges(): void {
    if (this.operation.milestones && this.operation.milestones.length > 0) {
      this.milestonesByType = _.indexBy(this.operation.milestones, milestone => {
        return (typeof milestone.type == 'string') ? milestone.type : MilestoneType[milestone.type];
      });
    }
  }

  getMilestoneDate(milestoneType: string) {
    return (this.milestonesByType) ? this.milestonesByType[milestoneType].date : '';
  }
}
