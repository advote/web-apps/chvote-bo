/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_parameters_milestone = {
  fr: {
    "card": {
      "title": "Jalons opérationnels",
      "subtitle": "Principaux jalons de l'opération"
    },
    "edit": {
      "title": "Jalons opérationnels",
      "subtitle": "Définir les jalons de l'opération",
      "before-vote": "Avant le vote",
      "during-vote": "Opération",
      "after-vote": "Après le vote",
      "actions": {
        "clear": "effacer"
      },
      "form": {
        "fields": {
          "SITE_VALIDATION": {
            "placeholder": "Validation du site internet"
          },
          "CERTIFICATION": {
            "placeholder": "Certification de l'opération"
          },
          "PRINTER_FILES": {
            "placeholder": "Génération des fichiers imprimeurs"
          },
          "BALLOT_BOX_INIT": {
            "placeholder": "Initialisation de l'urne électronique"
          },
          "SITE_OPEN": {
            "placeholder": "Ouverture du site de vote"
          },
          "SITE_CLOSE": {
            "placeholder": "Fermeture du site de vote"
          },
          "BALLOT_BOX_DECRYPT": {
            "placeholder": "Extraction de l'urne chiffrée"
          },
          "RESULT_VALIDATION": {
            "placeholder": "Validation des résultats"
          },
          "DATA_DESTRUCTION": {
            "placeholder": "Destruction des données"
          },
          "grace-period": {
            "placeholder": "Délai de grâce (en minutes)",
            "errors": {
              "min-max-value": "La valeur doit être comprise entre 0 et 60"
            }
          }
        },
        "errors": {
          "past-date": "La date doit être postérieure à la date du jour.",
          "milestone-date-must-be-before-operation": "Ce champ doit être antérieur à la date de l'opération.",
          "milestone-date-must-be-before-or-equal-operation": "Ce champ doit être antérieur ou égal à la date de l'opération.",
          "milestone-date-must-be-after-operation": "Ce champ doit être postérieur à la date de l'opération.",
          "milestone-date-must-be-after-or-equal-operation": "Ce champ doit être postérieur ou égal à la date de l'opération.",
          "certification-date": "La date de la certification de l'opération doit être postérieure ou égale à la date de validation du site internet.",
          "printer-files-date": "La date de la génération des fichiers imprimeurs doit être postérieure ou égale à la date de la certification de l'opération.",
          "ballot-box-init-date": "La date de l'initialisation de l'urne électronique doit être postérieure à la date de la génération des fichiers imprimeurs.",
          "site-open-date": "La date d'ouverture du site de vote doit être postérieure à la date de l'initialisation de l'urne électronique.",
          "site-close-date": "La date de fermeture du site de vote doit être postérieure à la date d'ouverture du site de vote.",
          "ballot-box-decrypt-date": "La date d'extraction de l'urne chiffrée doit être postérieure à la date de fermeture du site de vote (incluant le délai de grâce).",
          "result-validation-date": "La date de la validation des résultats doit être postérieure ou égal à la date d'extraction de l'urne chiffrée.",
          "data-destruction-date": "La date de destruction des données doit être postérieure à la date de la validation des résultats."
        },
        "save-success": "Les jalons de l'opération \"{{operation}}\" ont été mis à jour"
      },
      "delete-dialog": {
        "message": "Souhaitez-vous supprimer tous les jalons\u00A0?"
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Jalons opérationnels",
      "subtitle": "DE - Principaux jalons de l'opération"
    },
    "edit": {
      "title": "DE - Jalons opérationnels",
      "subtitle": "DE - Définir les jalons de l'opération",
      "before-vote": "DE - Avant le vote",
      "during-vote": "DE - Opération",
      "after-vote": "DE - Après le vote",
      "actions": {
        "clear": "DE - effacer"
      },
      "form": {
        "fields": {
          "SITE_VALIDATION": {
            "placeholder": "DE - Validation du site internet"
          },
          "CERTIFICATION": {
            "placeholder": "DE - Certification de l'opération"
          },
          "PRINTER_FILES": {
            "placeholder": "DE - Génération des fichiers imprimeurs"
          },
          "BALLOT_BOX_INIT": {
            "placeholder": "DE - Initialisation de l'urne électronique"
          },
          "SITE_OPEN": {
            "placeholder": "DE - Ouverture du site de vote"
          },
          "SITE_CLOSE": {
            "placeholder": "DE - Fermeture du site de vote"
          },
          "BALLOT_BOX_DECRYPT": {
            "placeholder": "DE - Extraction de l'urne chiffrée"
          },
          "RESULT_VALIDATION": {
            "placeholder": "DE - Validation des résultats"
          },
          "DATA_DESTRUCTION": {
            "placeholder": "DE - Destruction des données"
          },
          "grace-period": {
            "placeholder": "DE - Délai de grâce (en minutes)",
            "errors": {
              "min-max-value": "DE - La valeur doit être comprise entre 0 et 60"
            }
          }
        },
        "errors": {
          "past-date": "DE - La date doit être postérieure à la date du jour.",
          "milestone-date-must-be-before-operation": "DE - Ce champ doit être antérieur à la date de l'opération.",
          "milestone-date-must-be-before-or-equal-operation": "DE - Ce champ doit être antérieur ou égal à la date de l'opération.",
          "milestone-date-must-be-after-operation": "DE - Ce champ doit être postérieur à la date de l'opération.",
          "milestone-date-must-be-after-or-equal-operation": "DE - Ce champ doit être postérieur ou égal à la date de l'opération.",
          "certification-date": "DE - La date de la certification de l'opération doit être postérieure ou égale à la date de validation du site internet.",
          "printer-files-date": "DE - La date de la génération des fichiers imprimeurs doit être postérieure ou égale à la date de la certification de l'opération.",
          "ballot-box-init-date": "DE - La date de l'initialisation de l'urne électronique doit être postérieure à la date de la génération des fichiers imprimeurs.",
          "site-open-date": "DE - La date d'ouverture du site de vote doit être postérieure à la date de l'initialisation de l'urne électronique.",
          "site-close-date": "DE - La date de fermeture du site de vote doit être postérieure à la date d'ouverture du site de vote.",
          "ballot-box-decrypt-date": "DE - La date d'extraction de l'urne chiffrée doit être postérieure à la date de fermeture du site de vote (incluant le délai de grâce).",
          "result-validation-date": "DE - La date de la validation des résultats doit être postérieure ou égal à la date d'extraction de l'urne chiffrée.",
          "data-destruction-date": "DE - La date de destruction des données doit être postérieure à la date de la validation des résultats."
        },
        "save-success": "DE - Les jalons de l'opération \"{{operation}}\" ont été mis à jour"
      },
      "delete-dialog": {
        "message": "DE - Souhaitez-vous supprimer tous les jalons\u00A0?"
      }
    }
  }
};


