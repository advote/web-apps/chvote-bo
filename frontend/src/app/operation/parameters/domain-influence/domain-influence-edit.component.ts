/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { OperationParametersModel } from '../model/operation-parameters.model';
import { DomainInfluenceFile } from './model/domain-influence-file';
import { MatSnackBar, MatSort } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Operation } from '../../model/operation';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { TdDialogService } from '@covalent/core';
import { DomainInfluenceService } from './services/domain-influence.service';
import { DomainInfluenceImportResult } from './model/domain-influence-import-result';
import { FileService } from '../../../core/service/file-service/file-service';
import { map } from 'rxjs/operators';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { ConfirmBeforeQuit } from '../../../core/confirm-before-quit';
import { UploadInputComponent } from '../../../shared/upload-input/upload-input.component';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../service/read-only.service';

@Component({
  templateUrl: './domain-influence-edit.component.html',
  styleUrls: ['../parameter-edit.scss', './domain-influence-edit.component.scss']
})
export class DomainInfluenceEditComponent implements OnInit, ConfirmBeforeQuit {
  private _subscriptions: Subscription[] = [];
  private _readOnly = false;
  operation: Operation;
  file: DomainInfluenceFile;


  uploadResult: DomainInfluenceImportResult = undefined;
  validationErrorCsvContent: string;

  fileDataSource: DomainInfluenceFileDataSource;

  @ViewChild(UploadInputComponent)
  uploadInputComponent: UploadInputComponent;


  isFormModified(): boolean {
    return this.uploadInputComponent && this.uploadInputComponent.isFormModified();
  }


  get fileDisplayedColumns() {
    let readableColumn = ['file', 'author', 'importedDate'];
    if (this.readOnly || !this.authorizationService.hasAtLeastOneRole(["DELETE_DOI"])) {
      return readableColumn;
    } else {
      return readableColumn.concat(['delete']);
    }
  }

  @ViewChild(MatSort) sort: MatSort;

  constructor(private parametersModel: OperationParametersModel,
              private domainInfluenceService: DomainInfluenceService,
              private fileService: FileService,
              private dialogService: TdDialogService,
              private translateService: TranslateService,
              private authorizationService: AuthorizationService,
              private readOnlyService: ReadOnlyService,
              private snackBar: MatSnackBar) {
  }


  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  get readOnly() {
    return this._readOnly;
  }

  ngOnInit() {
    this._subscriptions.push(
      this.readOnlyService.isDoiInReadOnly()
        .subscribe(
          readOnly => {
            this._readOnly = readOnly;
          }
        ),
      this.parametersModel.currentOperation.subscribe(operation => this.operation = operation),
      this.parametersModel.currentDomainInfluenceFile.subscribe(file => this.file = file),
    );
    this.fileDataSource = new DomainInfluenceFileDataSource(this.parametersModel);

  }


  get uploadUrl(): string {
    return `${this.domainInfluenceService.serviceUrl}/${this.operation.id}/upload`;
  }


  resetUploadResult() {
    this.uploadResult = undefined;
  }

  processUploadResult(result: DomainInfluenceImportResult) {
    this.uploadResult = result;
    if (this.uploadResult) {
      if (this.uploadResult.validationErrors.length > 0) {
        this.validationErrorCsvContent = this.fileService.generateCsvFromFlatObject(
          this.uploadResult.validationErrors, "global.validation.csv.header");
      } else {
        this.parametersModel.updateDomainInfluenceFile(result.domainOfInfluenceFile);
        // UI-feedback: notify user that import was successful
        this.snackBar.open(
          this.translateService.instant(
            'parameters.domain-influence.edit.form.import-success',
            {fileName: result.domainOfInfluenceFile.fileName}
          ),
          '',
          {
            duration: 5000
          }
        );
      }
    }
  }

  downloadValidationReport() {
    this.fileService.downloadFile('domain_of_influence_errors.csv', this.validationErrorCsvContent);
  }

  download(file: DomainInfluenceFile, event: Event) {
    this.domainInfluenceService.download(this.operation.id).subscribe(blob => {
      this.fileService.downloadBlob(file.fileName, blob);
    });
    event.preventDefault();  // prevent href link
    event.stopPropagation(); // prevent repository panel's expansion toggle
  }

  deleteFile() {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.domain-influence.edit.delete-dialog.message',
        {fileName: this.file.fileName}),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.domainInfluenceService.deleteFile(this.file.id).subscribe(() => {
          this.snackBar.open(
            this.translateService.instant(
              'parameters.domain-influence.edit.delete-success',
              {fileName: this.file.fileName}
            ),
            '',
            {duration: 5000}
          );
          this.parametersModel.updateDomainInfluenceFile(new DomainInfluenceFile());
        });
      }
    });
  }
}

class DomainInfluenceFileDataSource extends DataSource<DomainInfluenceFile> {
  constructor(private parametersModel: OperationParametersModel) {
    super();
  }

  connect(): Observable<DomainInfluenceFile[]> {
    return this.parametersModel.currentDomainInfluenceFile.pipe(map(file => [file]));
  }

  disconnect() {
  }
}
