/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_parameters_base = {
  fr: {
    "card": {
      "title": "Opération",
      "subtitle": "Information de base concernant l'opération"
    },
    "edit": {
      "title": "Opération",
      "subtitle": "Définir les paramètres de l'opération",
      "form": {
        "fields": {
          "long-label": {
            "placeholder": "Libellé de l'opération",
            "errors": {
              "max-value": "Taille maximum: 200 caractères"
            }
          },
          "short-label": {
            "placeholder": "Libellé court",
            "errors": {
              "max-value": "Taille maximum: 50 caractères"
            }
          },
          "date": {
            "placeholder": "Date de l'opération"
          }
        },
        "errors": {
          "date-locked": "La date d'opération ne peut pas être modifiée",
          "past-date": "La date doit être postérieure à la date du jour"
        },
        "save-success": "L'opération \"{{operation}}\" a été mise à jour"
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Opération",
      "subtitle": "DE - Information de base concernant l'opération"
    },
    "edit": {
      "title": "DE - Opération",
      "subtitle": "DE - Définir les paramètres de l'opération",
      "form": {
        "fields": {
          "long-label": {
            "placeholder": "DE - Libellé de l'opération",
            "errors": {
              "max-value": "DE - Taille maximum: 200 caractères"
            }
          },
          "short-label": {
            "placeholder": "DE - Libellé court",
            "errors": {
              "max-value": "DE - Taille maximum: 50 caractères"
            }
          },
          "date": {
            "placeholder": "DE - Date de l'opération"
          }
        },
        "errors": {
          "date-locked": "DE - La date d'opération ne peut pas être modifiée",
          "past-date": "DE - La date doit être postérieure à la date du jour"
        },
        "save-success": "DE - L'opération \"{{operation}}\" a été mise à jour"
      }
    }
  },
};
