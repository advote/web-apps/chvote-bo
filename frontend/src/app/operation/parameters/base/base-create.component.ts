/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Operation } from '../../model/operation';
import { OperationService } from '../../service/operation.service';
import { Router } from '@angular/router';
import { BaseSaveOrEditComponent } from './base-save-or-edit-component';
import { BaseConfiguration } from './base-configuration';

@Component({
  templateUrl: './base-edit.component.html',
  styleUrls: ['../parameter-edit.scss']
})
export class BaseCreateComponent extends BaseSaveOrEditComponent {
  constructor(formBuilder: FormBuilder,
              private operationService: OperationService,
              private router: Router) {
    super(formBuilder);
    this.operation = new Operation();
    this.resetForm();
  }

  get readOnly() {
    return false;
  }

  doSave(baseConfig: BaseConfiguration) {
    this.operationService.create(baseConfig).subscribe(
      savedOperationId => {
        this.router.navigate(['/operations', savedOperationId, 'parameters']);
      }
    );
  }

  public cancel(): void {
    this.router.navigate(["dashboard"]);
  }

  get createMode(): boolean {
    return true;
  }
}
