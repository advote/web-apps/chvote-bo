/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Operation } from '../../model/operation';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../service/read-only.service';
import { OperationManagementService } from '../../service/operation-managment.service';

@Component({
  selector: 'base-card',
  templateUrl: './base-card.component.html'
})
export class BaseCardComponent implements OnInit, OnDestroy {
  private _readOnly: boolean;
  private _subscriptions: Subscription[] = [];
  @Input() operation: Operation;
  completed = false;
  inError = false;

  constructor(private readOnlyService: ReadOnlyService, private operationManagementService : OperationManagementService) {

  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  get readOnly() {
    return this._readOnly;
  }

  ngOnInit() {
    this._subscriptions.push(
      this.readOnlyService.isBaseParameterInReadOnly().subscribe(readOnly => this._readOnly = readOnly),
      this.operationManagementService.status.subscribe(status => {
        this.completed = status.configurationStatus.completedSections["base-parameter"];
        this.inError = status.configurationStatus.sectionsInError["base-parameter"];
      })
    );
  }

}
