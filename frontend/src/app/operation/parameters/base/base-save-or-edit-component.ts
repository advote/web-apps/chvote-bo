/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { AfterViewInit, QueryList, ViewChildren } from '@angular/core';
import { MatInput } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Operation } from '../../model/operation';
import * as moment from 'moment';
import { focusOnError, isFormModifiedAndNotSaved } from '../../../core/util/form-utils';
import { ConfirmBeforeQuit } from '../../../core/confirm-before-quit';
import { BaseConfiguration } from './base-configuration';

export abstract class BaseSaveOrEditComponent implements AfterViewInit, ConfirmBeforeQuit {

  operation: Operation = new Operation();
  baseParametersForm: FormGroup;
  now: Date = moment().add(1, 'days').hours(0).minutes(0).seconds(0).toDate();
  saved = false;

  constructor(private formBuilder: FormBuilder) {
    this.createForm();
  }

  @ViewChildren(MatInput) inputs: QueryList<MatInput>;

  ngAfterViewInit(): void {
    // autofocus on first field
    setTimeout(() => this.inputs.first.focus());
  }

  resetForm() {
    this.baseParametersForm.reset({
      longLabel: this.operation.longLabel,
      shortLabel: this.operation.shortLabel,
      date: this.operation.date ? moment(this.operation.date) : null
    });

  }

  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.baseParametersForm, this.saved);
  }


  createForm() {
    this.baseParametersForm = this.formBuilder.group({
      longLabel: [
        {value: null},
        [
          Validators.required,
          Validators.maxLength(200)
        ]
      ],
      shortLabel: [
        {value: null},
        [
          Validators.required,
          Validators.maxLength(50)
        ]
      ],
      date: [
        {value: null},
        Validators.required
      ]
    });
  }

  save(): void {
    // validate form model
    this.baseParametersForm.controls.longLabel.markAsTouched();
    this.baseParametersForm.controls.shortLabel.markAsTouched();
    this.baseParametersForm.controls.date.markAsTouched();
    this.baseParametersForm.updateValueAndValidity();

    if (!this.baseParametersForm.valid) {
      focusOnError(this.baseParametersForm);
    } else {
      // form is valid, update operation's values from form model
      const formModel = this.baseParametersForm.getRawValue();
      let baseConfig: BaseConfiguration = {
        longLabel: formModel.longLabel,
        shortLabel: formModel.shortLabel,
        date: formModel.date.toDate()
      };

      this.doSave(baseConfig);
      this.saved = true;
    }
  }

  public abstract cancel();

  abstract doSave(baseConfig: BaseConfiguration);

  abstract get createMode(): boolean;
}
