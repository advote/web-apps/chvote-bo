/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_parameters_document = {
  fr: {
    "card": {
      "title": "Documentation",
      "subtitle": "Documentation de l'opération et Questions à mettre en avant",
      "no-document": "Absent",
      "1-document": "1 langue",
      "n-document": "{{count}} langues",
      "question_count": "Questions",
      "no-question": "Absent",
      "1-question": "1 question",
      "n-question": "{{count}} questions",

    },
    "edit": {
      "title": "Documentation",
      "subtitle": "Liste des documents de l'opération",
      "form": {
        "fields": {
          "type": {
            "placeholder": "Type documentation",
            "label-key": {
              "DOCUMENT_FAQ": "FAQ",
              "DOCUMENT_TERMS": "Conditions d'utilisation",
              "DOCUMENT_CERTIFICATE": "Document de certificat"
            }
          },
          "language": {
            "placeholder": "Choix de la langue",
            "label-key": {
              "FR": "Français",
              "DE": "Allemand",
              "IT": "Italien",
              "RM": "Romanche",
              "EN": "Anglais"
            }
          },
          "inputFile": {
            "placeholder": "Sélectionner un document"
          }
        },
        "import-success": "Le document \"{{fileName}}\" a été importé avec succès"
      },
      "delete-success": "Le document \"{{fileName}}\" a été supprimé avec succès",
      "delete-dialog": {
        "message": "Souhaitez-vous supprimer le document {{fileName}}\u00A0?"
      },
      "grid": {
        "header": {
          "type": "Type de document",
          "language": "Langue",
          "file": "Nom du fichier chargé",
          "delete": "Supprimer"
        }
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Documentation",
      "subtitle": "DE - Information générale concernant l'opération",
      "no-document": "DE - aucun document",
      "1-document": "DE - 1 document",
      "n-document": "DE - {{count}} documents"
    },
    "edit": {
      "title": "DE - Documentation",
      "subtitle": "DE - Liste des documents de l'opération",
      "form": {
        "fields": {
          "type": {
            "placeholder": "DE - Type documentation",
            "label-key": {
              "DOCUMENT_FAQ": "DE - FAQ",
              "DOCUMENT_TERMS": "DE - Conditions d'utilisation",
              "DOCUMENT_CERTIFICATE": "DE - Document de certificat"
            }
          },
          "language": {
            "placeholder": "DE - Choix de la langue",
            "label-key": {
              "FR": "DE - Français",
              "DE": "DE - Allemand",
              "IT": "DE - Italien",
              "RM": "DE - Romanche",
              "EN": "DE - Anglais"
            }
          },
          "inputFile": {
            "placeholder": "DE - Sélectionner un document"
          }
        },
        "import-success": "DE - Le document \"{{fileName}}\" a été importé avec succès"
      },
      "delete-success": "DE - Le document \"{{fileName}}\" a été supprimé avec succès",
      "delete-dialog": {
        "message": "DE - Souhaitez-vous supprimer le document {{fileName}}\u00A0?"
      },
      "grid": {
        "header": {
          "type": "DE - Type de document",
          "language": "DE - Langue",
          "file": "DE - Nom du fichier chargé",
          "delete": "DE - Supprimer"
        }
      }
    }
  }
}
