/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { HighlightedQuestion, LocalizedHighlightedQuestion } from '../model/highlighted-question';
import { DataSource } from '@angular/cdk/collections';
import { MatSort } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { merge } from 'rxjs/observable/merge';
import { of } from 'rxjs/observable/of';
import { map } from 'rxjs/operators';
import { sortOnProperty } from '../../../../../core/util/table-utils';
import { HighlightedQuestionService } from '../services/highlighted-question.service';
import { Operation } from '../../../../model/operation';
import { FileService } from '../../../../../core/service/file-service/file-service';

@Component({
  selector: 'localized-highlighted-question-list',
  templateUrl: './localized-highlighted-question-list.component.html',
  styleUrls: ['./localized-highlighted-question-list.component.scss']
})
export class LocalizedHighlightedQuestionListComponent {
  @Input() editMode = false;
  @Input() operation: Operation;
  @Output() onRemove = new EventEmitter<LocalizedHighlightedQuestion>();
  @ViewChild(MatSort) sort: MatSort;
  empty = true;
  dataSource;


  constructor(private translateService: TranslateService,
              private highlightedQuestionService: HighlightedQuestionService,
              private fileService: FileService) {
  }

  remove(element) {
    this.onRemove.emit(element);
  }

  download(e, element: LocalizedHighlightedQuestion) {
    this.highlightedQuestionService.download(this.operation.id, element.id).subscribe(blob => {
      this.fileService.downloadBlob(element.fileName, blob)
    });
    e.preventDefault();
  }


  get displayedColumns(): string[] {
    return ["localizedQuestion", "language", "fileName", "date"].concat(this.editMode ? ["actions"] : []);
  }

  @Input()
  set question(question: HighlightedQuestion) {
    this.empty = question.localizedQuestions.length == 0;
    this.dataSource =
      new LocalizedHighlightedQuestionDataSource(question.localizedQuestions, this.sort, this.translateService);
  }
}

class LocalizedHighlightedQuestionDataSource extends DataSource<any> {
  constructor(private data: LocalizedHighlightedQuestion[],
              private sort: MatSort,
              private translateService: TranslateService) {
    super();
  }

  connect(): Observable<any[]> {
    return merge(of(this.data), this.sort.sortChange)
      .pipe(
        map(() => this.data.map(
          question => Object.assign({}, question, {
            language: this.translateService.instant(
              `parameters.highlighted-question.form.fields.language.label-key.${question.language}`)
          })
        )),
        map(translated => sortOnProperty(translated, this.sort.active, this.sort.direction)));
  }

  disconnect() {
  }
}
