/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { HighlightedQuestion, LocalizedHighlightedQuestion } from '../model/highlighted-question';
import { HighlightedQuestionService } from '../services/highlighted-question.service';
import { Operation } from '../../../../model/operation';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { OperationManagementService } from '../../../../service/operation-managment.service';
import { TdDialogService } from '@covalent/core';
import { TranslateService } from '@ngx-translate/core';

const ALL_LANGUAGES = [
  {value: 'DE', labelKey: 'parameters.highlighted-question.form.fields.language.label-key.DE'},
  {value: 'FR', labelKey: 'parameters.highlighted-question.form.fields.language.label-key.FR'},
  {value: 'IT', labelKey: 'parameters.highlighted-question.form.fields.language.label-key.IT'},
  {value: 'RM', labelKey: 'parameters.highlighted-question.form.fields.language.label-key.RM'}
];


@Component({
  selector: 'highlighted-question-edit',
  templateUrl: './highlighted-question-edit.component.html',
  styleUrls: ['./highlighted-question-edit.component.scss', '../../../parameter-edit.scss']
})
export class HighlightedQuestionEditComponent {

  languages = ALL_LANGUAGES;
  question: HighlightedQuestion;


  @Input() operation: Operation;
  @Output() finishEditing = new EventEmitter<any>();

  questionForm: FormGroup;

  constructor(fb: FormBuilder,
              private operationManagementService: OperationManagementService,
              private translateService: TranslateService,
              private dialogService: TdDialogService,
              private highlightedQuestionService: HighlightedQuestionService) {
    this.questionForm = fb.group({
      localizedQuestion: [null, [Validators.required, Validators.maxLength(80)]],
      language: [null, [Validators.required,]],
    });
  }

  @Input()
  set originalQuestion(question: HighlightedQuestion) {
    this.updateQuestion(question);
  }

  get uploadUrl() {
    return this.highlightedQuestionService.serviceUrl(this.operation.id) + "/" +
           this.question.id + "?" +
           "localizedQuestion=" + encodeURIComponent(this.questionForm.value.localizedQuestion) + "&" +
           "language=" + encodeURIComponent(this.questionForm.value.language);
  }

  updateQuestion(value: HighlightedQuestion) {
    this.question = value;
    let questionLanguages = value.localizedQuestions.map(question => question.language);
    this.languages = ALL_LANGUAGES.filter(language => questionLanguages.indexOf(language.value) == -1);
  }

  checkBeforeUpload() {
    return () => {
      this.questionForm.controls.localizedQuestion.markAsTouched();
      this.questionForm.controls.language.markAsTouched();
      this.questionForm.updateValueAndValidity();

      if (!this.questionForm.valid) {
        return false;
      }

      if (this.question.id) {
        return true;
      }

      return this.highlightedQuestionService.addQuestion(this.operation.id).pipe(
        map(question => {
            this.question.id = question.id;
            return true;
          }
        )
      )
    }
  }

  processUploadResult(question) {
    this.updateQuestion(question);
    this.questionForm.reset({localizedQuestion: null, language: null});

    if (this.question.localizedQuestions.length == ALL_LANGUAGES.length) {
      this.close();
    }

    this.operationManagementService.shouldUpdateStatus(false);
  }

  close() {
    if (this.question.localizedQuestions.length == 0 && this.question.id) {
      this.highlightedQuestionService.deleteQuestion(this.operation.id, this.question.id)
        .subscribe(() => {
          this.finishEditing.emit(true);
        });
    } else {
      this.finishEditing.emit(true);
    }
  }


  removeLocalizedQuestion(localizedQuestion: LocalizedHighlightedQuestion) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.highlighted-question.delete-localized-question-dialog.message',
        {lang: localizedQuestion.language}),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
      width: "55rem",
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.highlightedQuestionService.deleteLocalizedQuestion(this.operation.id, this.question.id,
          localizedQuestion.id)
          .subscribe(question => {
            this.updateQuestion(question);
            if (this.question.localizedQuestions.length == 0) {
              this.close();
            }
          });
      }
    });
  }
}
