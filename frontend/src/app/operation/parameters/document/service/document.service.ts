/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { OperationFile } from '../../../../core/model/operation-file';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpParameters } from '../../../../core/service/http/http.parameters.service';

@Injectable()
export class DocumentService {
  public serviceUrl: string;

  constructor(private http: HttpClient, httpParams: HttpParameters) {
    this.serviceUrl = httpParams.apiBaseURL + '/operation/document';
  }

  /**
   * Retrieve the list of document files linked to a given operation
   *
   * @param operationId the concerned operation's unique ID
   * @returns an Observable on the array of document files
   */
  list(operationId: number): Observable<OperationFile[]> {
    return this.http.get<OperationFile[]>(`${this.serviceUrl}/${operationId}`);
  }

  /**
   * Download the document file identified by its ID.
   *
   * @param documentId the document's unique ID
   * @returns an Observable on the server's response
   */
  download(documentId: number): Observable<Blob> {
    return this.http.get(`${this.serviceUrl}/${documentId}/download`,
      {headers: new HttpHeaders({'Accept': 'application/pdf'}), responseType: 'blob'});
  }

  /**
   * Delete the given document file.
   *
   * @param fileId the document's unique ID
   * @returns an Observable on the server's response
   */
  deleteFile(fileId: number): Observable<any> {
    return this.http.delete(`${this.serviceUrl}/${fileId}`);
  }
}
