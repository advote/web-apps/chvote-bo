/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ParametersComponent } from "./parameters.component";
import { RepositoryEditComponent } from './repository/repository-edit.component';
import { DomainInfluenceEditComponent } from './domain-influence/domain-influence-edit.component';
import { DocumentEditComponent } from './document/document-edit.component';
import { TestingCardsLotDetailsComponent } from '../testing-cards-lot/testing-cards-lot-details/testing-cards-lot-details.component';
import { TestingCardsLotListComponent } from '../testing-cards-lot/testing-cards-lot-list/testing-cards-lot-list.component';
import { MilestoneEditComponent } from './milestone/milestone-edit.component';
import { ParametersEditComponent } from './parameter-edit/parameters-edit.component';
import { TestingCardsLotResolver } from '../testing-cards-lot/service/testing-cards-lot.resolver';
import { BaseEditComponent } from './base/base-edit.component';
import { AuthorizationGuard } from '../../core/service/http/authorization.service';
import { ManagementEntityEditComponent } from './management-entity/edit/management-entity-edit.component';
import { ConfirmBeforeQuitGuard } from '../../core/confirm-before-quit';
import { BallotDocumentationEditComponent } from './ballot-documentation/ballot-documentation-edit.component';
import { ElectionPagePropertiesEditComponent } from './election-page-properties/election-page-properties-edit.component';
import { ElectionPagePropertiesModelResolver } from './election-page-properties/services/election-page-properties.resolver';
import { ElectionPagePropertiesModelEditComponent } from './election-page-properties/election-page-properties-model-edit.component';
import { VotingSiteConfigurationComponent } from './voting-site-configuration/edit/voting-site-configuration.component';

const routes: Routes = [
  {
    path: 'edit',
    component: ParametersEditComponent,
    children: [
      {
        path: 'base',
        component: BaseEditComponent,
        canDeactivate: [ConfirmBeforeQuitGuard]
      },
      {
        path: 'milestone',
        component: MilestoneEditComponent,
        canDeactivate: [ConfirmBeforeQuitGuard]
      },
      {
        path: 'ballot-documentation',
        component: BallotDocumentationEditComponent,
        canDeactivate: [ConfirmBeforeQuitGuard]
      },
      {
        path: 'domain-influence',
        component: DomainInfluenceEditComponent,
        canDeactivate: [ConfirmBeforeQuitGuard]
      },
      {
        path: 'repository',
        component: RepositoryEditComponent,
        canDeactivate: [ConfirmBeforeQuitGuard]
      },
      {
        path: 'document',
        component: DocumentEditComponent,
        canDeactivate: [ConfirmBeforeQuitGuard]
      },
      {
        path: 'management-entity',
        component: ManagementEntityEditComponent
      },
      {
        path: 'voting-site-configuration',
        component: VotingSiteConfigurationComponent,
        canDeactivate: [ConfirmBeforeQuitGuard]
      },
      {
        path: 'election-page-properties',
        children: [
          {
            path: 'model/create',
            component: ElectionPagePropertiesModelEditComponent,
            canDeactivate: [ConfirmBeforeQuitGuard]
          },
          {
            path: 'model/:modelId',
            resolve: {
              model: ElectionPagePropertiesModelResolver
            },
            component: ElectionPagePropertiesModelEditComponent,
            canDeactivate: [ConfirmBeforeQuitGuard]
          },
          {
            path: '',
            component: ElectionPagePropertiesEditComponent
          }
        ]
      },
      {
        path: 'testing-card-lot',
        children: [
          {
            path: 'create',
            component: TestingCardsLotDetailsComponent,
            data: {
              forConfiguration: true
            },
            canDeactivate: [ConfirmBeforeQuitGuard]
          },
          {
            path: ':voterTestingCardsLotId',
            component: TestingCardsLotDetailsComponent,
            resolve: {
              voterTestingCardsLot: TestingCardsLotResolver,
            },
            data: {
              forConfiguration: true
            },
            canDeactivate: [ConfirmBeforeQuitGuard]
          },
          {
            path: '',
            component: TestingCardsLotListComponent,
            data: {
              forConfiguration: true
            }
          },
        ]
      },

    ],
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard]
  },
  {
    path: '',
    component: ParametersComponent,
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard]
  }
];

/**
 * Routing module for the parameters page.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametersRoutingModule {
}
