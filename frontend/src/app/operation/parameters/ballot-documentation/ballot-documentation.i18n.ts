/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_parameters_ballot_documentation = {
  fr: {
    "card": {
      "title": "Documentation scrutins",
      "subtitle": "",
      "no-documentation": "Aucune documentation",
      "1-documentation": "1 documentation",
      "n-documentation": "{{count}} documentations"
    },
    "edit": {
      "repository-file-required": "Au moins un référentiel d'opération est nécessaire afin de renseigner la documentation des scrutins",
      "title": "Documentation des scrutins",
      "subtitle": "Liste des documents des scrutins",
      "form": {
        "fields": {
          "input-file": {
            "placeholder": "Sélectionner un fichier"
          },
          "ballot": {
            "placeholder": "Scrutin"
          },
          "localized-label": {
            "placeholder": "Intitulé"
          },
          "language": {
            "placeholder": "Langue",
            "label-key": {
              "FR": "Français",
              "DE": "Allemand",
              "IT": "Italien",
              "RM": "Romanche",
            }
          }
        },
        "import-success": "La documentation du scrutin a été importé avec succès",
        "duplicate-localized-label": "Ce libellé est déjà attribué pour {{ballotId}} en {{language}}."
      },
      "delete-success": "Le documentation du scrutin a été supprimé avec succès",
      "delete-dialog": {
        "message": "Souhaitez-vous supprimer la documentation en {{language}} du scrutin {{ballotId}} ?"
      },
    },
    "table": {
      "header": {
        "management-entity": "Entité de gestion",
        "ballot": "Scrutin",
        "file": "Fichier",
        "localized-label": "Intitulé du document",
        "language": "Langue",
        "delete": "Supprimer"
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Documentation scrutins",
      "subtitle": "DE - ",
      "no-documentation": "DE - Aucune documentation",
      "1-documentation": "DE - 1 documentation",
      "n-documentation": "DE - {{count}} documentations"
    },
    "edit": {
      "repository-file-required": "DE - Au moins un référentiel d'opération est nécessaire afin de renseigner la documentation des scrutins",
      "title": "DE - Documentation des scrutins",
      "subtitle": "DE - Liste des documents des scrutins",
      "form": {
        "fields": {
          "input-file": {
            "placeholder": "DE - Sélectionner un fichier"
          },
          "ballot": {
            "placeholder": "DE - Scrutin"
          },
          "localized-label": {
            "placeholder": "DE - Intitulé"
          },
          "language": {
            "placeholder": "DE - Langue",
            "label-key": {
              "FR": "DE - Français",
              "DE": "DE - Allemand",
              "IT": "DE - Italien",
              "RM": "DE - Romanche",
            }
          }
        },
        "import-success": "DE - La documentation du scrutin a été importé avec succès",
        "duplicate-localized-label": "DE - Ce libellé est déjà attribué pour {{ballotId}} en {{language}}."
      },
      "delete-success": "DE - Le documentation du scrutin a été supprimé avec succès",
      "delete-dialog": {
        "message": "DE - Souhaitez-vous supprimer la documentation en {{language}} du scrutin {{ballotId}} ?"
      },
    },
    "table": {
      "header": {
        "management-entity": "DE - Entité de gestion",
        "ballot": "DE - Scrutin",
        "file": "DE - Fichier",
        "localized-label": "DE - Intitulé du document",
        "language": "DE - Langue",
        "delete": "DE - Supprimer"
      }
    }
  },
};
