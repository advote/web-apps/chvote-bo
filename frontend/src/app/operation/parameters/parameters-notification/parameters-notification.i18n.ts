/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_parameters_notification = {
  fr: {
    "info": {
      "ready-to-test": "Vous pouvez désormais créer un site de test et les fichiers imprimeurs de test.",
      "can-request-deployment": "Vous pouvez désormais demander le déploiement en vue de la création d'une instance du protocole",
      "can-validate-deployment": "Vous pouvez désormais valider ou invalider le déploiement en vue de la création d'une instance du protocole",
      "consistency": {
        "in-progress": "Le calcul de la cohérence du paramétrage est en cours",
        "errors": "Erreurs de cohérence détectées"
      }
    },
    "upload": {
      "button": "Tester le paramétrage",
      "success": "La demande a été prise en compte"
    },
    "request-deployment": {
      "title": "Demander le déploiement",
      "pact-link": "Ouvrir l'application PACT"
    },
    "validate-deployment": {
      "title": "Valider ou refuser le déploiement",
      "pact-link": "Ouvrir l'application PACT"
    },

    "validation": {
      "invalidate": {
        "button": "Invalider le paramétrage",
        "success": "Les paramètres ont été invalidés",
        "dialog": {
          "title": "Invalidation des paramètres",
          "message": "Le site de test ainsi que les cartes de votes seront détruits. Les paramètres seront à nouveau disponibles pour configuration."
        }
      },
      "validate": {
        "button": "Valider le paramétrage",
        "success": "Les paramètres ont été validés"
      }
    }
  },
  de: {
    "info": {
      "ready-to-test": "DE - Vous pouvez désormais créer un site de test et les fichiers imprimeurs de test."
    },
    "upload": {
      "button": "DE - Tester le paramétrage",
      "success": "DE - La demande a été prise en compte"
    },
    "validation": {
      "invalidate": {
        "button": "DE - Invalider le paramétrage",
        "success": "DE - Les paramètres ont été invalidés",
        "dialog": {
          "title": "DE - Invalidation des paramètres",
          "message": "DE - Le site de test ainsi que les cartes de votes seront détruits. Les paramètres seront à nouveau disponibles pour configuration."
        }
      },
      "validate": {
        "button": "DE - Valider le paramétrage",
        "success": "DE - Les paramètres ont été validés"
      }
    }
  },
};
