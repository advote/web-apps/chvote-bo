/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Mapping, Model } from './model/election-page-properties';
import { Operation } from '../../model/operation';
import { TdDialogService } from '@covalent/core';
import { TranslateService } from '@ngx-translate/core';
import { ElectionPagePropertiesService } from './services/election-page-properties.service';
import { OperationDataService } from '../../service/operation.data.service';
import { AuthorizationService } from '../../../core/service/http/authorization.service';

@Component({
  selector: 'election-page-properties-mapping',
  templateUrl: './election-page-properties-mapping.component.html',
  styleUrls: ['./election-page-properties-mapping.component.scss']
})
export class ElectionPagePropertiesMappingComponent implements OnInit {

  @Input()
  models: Model[];

  @Output()
  mappingChange = new EventEmitter<Model[]>();

  @Input()
  mapping: Mapping;

  @Input()
  operation: Operation;

  @Input()
  readOnly;

  allBallots: string[] = [];

  orphanMapping: string[];

  constructor(private dialogService: TdDialogService,
              private translateService: TranslateService,
              private operationDataService: OperationDataService,
              private service: ElectionPagePropertiesService,
              private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    this.operationDataService.getAllRelatedBallots(this.operation.id, true, false).subscribe(ballots => {
      this.orphanMapping = Object.keys(this.mapping).filter(ballot => ballots.indexOf(ballot) == -1);
      this.allBallots = Array.from(new Set(ballots.concat(Object.keys(this.mapping)))).sort()
    });
  }

  isOrphan(ballot) {
    return this.orphanMapping.indexOf(ballot) >= 0;
  }


  valueForBallot(ballot) {
    return this.mapping[ballot];
  }


  mappedModelNameForBallot(ballot) {
    const model = this.models.find(model => model.id == this.mapping[ballot]);
    return model ? model.modelName : null;
  }

  updateMapping(ballot: string, modelId) {
    if (modelId == -1) {
      this.service.removeMapping(this.operation.id, ballot).subscribe(() => {
        delete this.mapping[ballot];
      });
    } else {
      this.service.associateModelToBallots(this.operation.id, modelId, [ballot])
        .subscribe(() => {
          this.mapping[ballot] = modelId;
        });
    }

  }

  removeMapping(ballot) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.election-page-properties.edit.mapping.delete-dialog.message',
        {ballot}),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.service.removeMapping(this.operation.id, ballot).subscribe(() => {
          this.allBallots = this.allBallots.filter(b => b != ballot);
          delete this.mapping[ballot];
        });
      }
    });
  }

  userCanDoMapping() {
    return this.authorizationService.hasAtLeastOneRole(["MAP_ELECTION_PAGE_PROPERTIES_MODEL_TO_BALLOT"]);
  }
}
