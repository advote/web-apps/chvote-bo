/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Mapping, Model } from './model/election-page-properties';
import { ActivatedRoute, Router } from '@angular/router';
import { TdDialogService } from '@covalent/core';
import { TranslateService } from '@ngx-translate/core';
import { ElectionPagePropertiesService } from './services/election-page-properties.service';
import { Operation } from '../../model/operation';
import { AuthorizationService } from '../../../core/service/http/authorization.service';

@Component({
  selector: 'election-page-properties-model-list',
  templateUrl: './election-page-properties-model-list.component.html',
  styleUrls: ['./election-page-properties-model-list.component.scss']
})
export class ElectionPagePropertiesModelListComponent implements OnInit {

  @Input()
  models: Model[];

  @Output()
  modelsChange = new EventEmitter<Model[]>();


  @Input()
  mapping: Mapping;

  @Input()
  operation: Operation;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private dialogService: TdDialogService,
              private translateService: TranslateService,
              private service: ElectionPagePropertiesService,
              private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
  }


  remove(model: Model) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('parameters.election-page-properties.edit.models.delete-dialog.message',
        model),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.service.deleteModel(this.operation.id, model.id).subscribe(() => {
          this.modelsChange.emit(this.models.filter(m => m !== model));
        });
      }
    });
  }

  hasMapping(model: Model) {
    return this.mapping && Object.keys(this.mapping).find(k => this.mapping[k] == model.id)
  }

  editModel(model: Model) {
    this.router.navigate(["model", model.id], {relativeTo: this.route})
  }

  userCanDeleteMapping(model) {
    return !this.hasMapping(model) &&
           this.authorizationService.hasAtLeastOneRole(["DELETE_ELECTION_PAGE_PROPERTIES_MODEL"]);
  }
}
