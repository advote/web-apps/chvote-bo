/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Model } from '../model/election-page-properties';
import { Observable } from 'rxjs/Observable';
import { ElectionPagePropertiesService } from './election-page-properties.service';
import { map, take } from 'rxjs/operators';

@Injectable()
export class ElectionPagePropertiesModelResolver implements Resolve<Model> {


  constructor(private service: ElectionPagePropertiesService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Model> | Promise<Model> | Model {
    let modelId = +route.paramMap.get('modelId');
    let operationId = +route.parent.parent.parent.parent.paramMap.get("id");

    return this.service.find(operationId, modelId).pipe(
      take(1),
      map(value => {
        if (value) {
          return value;
        } else { // id not found
          this.router.navigate(['/']);
          return null;
        }
      })
    );

  }


}
