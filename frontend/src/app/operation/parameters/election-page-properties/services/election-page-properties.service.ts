/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParameters } from '../../../../core/service/http/http.parameters.service';
import { Observable } from 'rxjs/Observable';
import { Mapping, Model } from '../model/election-page-properties';

@Injectable()
export class ElectionPagePropertiesService {
  public serviceUrl;

  constructor(private http: HttpClient, private params: HttpParameters) {
    this.serviceUrl = (operationId) => `${this.params.apiBaseURL}/operation/${operationId}/election-page-properties`;
  }

  getModelsForOperation(operationId): Observable<Model[]> {
    return this.http.get<Model[]>(`${this.serviceUrl(operationId)}/model`);
  }

  saveOrUpdateModel(operationId, model): Observable<Model> {
    return this.http.post<Model>(`${this.serviceUrl(operationId)}/model`, model);
  }

  deleteModel(operationId, modelId) {
    return this.http.delete(`${this.serviceUrl(operationId)}/model/${modelId}`);

  }

  getBallotToModelMapping(operationId): Observable<Mapping> {
    return this.http.get<Mapping>(`${this.serviceUrl(operationId)}/mapping`);
  }

  associateModelToBallots(operationId, modelId, ballotIds: string[]) {
    return this.http.put(`${this.serviceUrl(operationId)}/mapping`, {modelId, ballotIds});
  }

  removeMapping(operationId, ballotId) {
    return this.http.delete(`${this.serviceUrl(operationId)}/mapping`, {params: {ballotId}});
  }

  find(operationId: number, modelId: number) {
    return this.http.get<Model>(`${this.serviceUrl(operationId)}/model/${modelId}`);
  }

}
