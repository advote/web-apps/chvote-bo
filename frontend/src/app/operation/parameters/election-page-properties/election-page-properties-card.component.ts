/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ReadOnlyService } from '../../service/read-only.service';
import { ElectionPagePropertiesService } from './services/election-page-properties.service';
import { Subscription } from 'rxjs/Subscription';

import { Operation } from '../../model/operation';
import { OperationManagementService } from '../../service/operation-managment.service';

@Component({
  selector: 'election-page-properties-card',
  templateUrl: './election-page-properties-card.component.html',
  styleUrls: ['./election-page-properties-card.component.scss']
})
export class ElectionPagePropertiesCardComponent implements OnInit, OnDestroy {
  @Input() operation: Operation;
  mappingCount: number;
  modelCount: number;
  readOnly: boolean;

  private subscriptions: Subscription[] = [];
  completed = false;
  inError = false;

  constructor(private readOnlyService: ReadOnlyService,
              private service: ElectionPagePropertiesService,
              operationManagementService: OperationManagementService) {
    this.subscriptions.push(
      operationManagementService.status.subscribe(status => {
        this.completed = status.configurationStatus.completedSections["election-page-properties"];
        this.inError = status.configurationStatus.sectionsInError["election-page-properties"];
      }),
      readOnlyService.isElectionPagePropertiesInReadOnly().subscribe(readonly => this.readOnly = readonly));
  }

  ngOnInit() {
    this.service.getModelsForOperation(this.operation.id).subscribe(models => this.modelCount = models.length);
    this.service.getBallotToModelMapping(this.operation.id).subscribe(mapping => this.mappingCount = Object.keys(mapping).length);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
