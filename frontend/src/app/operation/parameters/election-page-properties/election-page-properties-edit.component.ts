/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Operation } from '../../model/operation';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../service/read-only.service';
import { ElectionPagePropertiesService } from './services/election-page-properties.service';
import { Mapping, Model } from './model/election-page-properties';
import { OperationManagementService } from '../../service/operation-managment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OperationDataService } from '../../service/operation.data.service';
import { MatDialog } from '@angular/material';
import { ModelSelectorComponent, ModelSelectorData } from './selector/model-selector.component';

@Component({
  selector: 'election-page-properties-edit',
  templateUrl: './election-page-properties-edit.component.html',
  styleUrls: ['./election-page-properties-edit.component.scss']
})
export class ElectionPagePropertiesEditComponent implements OnInit, OnDestroy {
  operation: Operation;

  mapping: Mapping;
  models: Model[];
  readOnly: boolean;
  private subscriptions: Subscription[] = [];
  private ballots: string[];

  constructor(private readOnlyService: ReadOnlyService,
              private service: ElectionPagePropertiesService,
              private operationManagementService: OperationManagementService,
              private route: ActivatedRoute,
              private operationDataService: OperationDataService,
              private dialog: MatDialog,
              private router: Router) {
    this.subscriptions.push(
      readOnlyService.isElectionPagePropertiesInReadOnly().subscribe(readonly => this.readOnly = readonly),
      operationManagementService.operation.subscribe(operation => this.operation = operation)
    );
  }


  get canDoMapping() {
    return this.models && this.models.length > 0 &&
           (this.ballots && this.ballots.length > 0 ||
            this.mapping && Object.keys(this.mapping).length > 0);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }


  ngOnInit() {
    this.service.getModelsForOperation(this.operation.id).subscribe(models => this.models = models);
    this.refreshMapping();
    this.operationDataService.getAllRelatedBallots(this.operation.id, true, false)
      .subscribe(ballots => this.ballots = ballots);
  }


  private refreshMapping() {
    this.service.getBallotToModelMapping(this.operation.id).subscribe(mapping => this.mapping = mapping);
  }

  addModel() {
    this.router.navigate(["model/create"], {relativeTo: this.route})
  }

  bulkUpdate() {
    let data: ModelSelectorData = {
      operation: this.operation,
      possibleBallots: this.ballots,
      possibleModels: this.models
    };
    this.dialog.open(ModelSelectorComponent, {data, disableClose: true, width: "700px"})
      .afterClosed()
      .subscribe(() => this.refreshMapping());
  }

}
