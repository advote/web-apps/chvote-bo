/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_election_page_properties = {
  fr: {
    "side-bar-title": "Affichage Élection",
    "card": {
      "title": "Paramètres d'affichage d'une élection",
      "subtitle": "",
      "model-count": "Modèles de paramètres",
      "mapping-count": "Scrutins configurés",
    },
    "edit": {
      "title": "Paramètres d'affichage d'une élection",
      "subtitle": "Configurer l'affichage des scrutins type élection sur le site de vote",
      "mapping": {
        "title": "Paramètres d'affichage par scrutin",
        "model-name": "Modèle de paramètres",
        "action": {
          "delete": "Supprimer",
          "unmap": "Retirer l'association"
        },
        "delete-dialog": {
          "message": "Le scrutin {{ballot}} n'est plus associé à l'opération. Souhaitez-vous supprimer cette association ?"
        },
        "bulk-update": {
          "select-ballot": "1. Sélection des scrutins",
          "select-model": "2. Sélection du modèle",
          "filter": "Filtre",
          "validate": "Associer",
          "open": "Associer les scrutins en lot",
          "table": {
            "header": {
              "ballot": "Scrutin"
            }
          }
        }
      },


      "models": {
        "title": "Liste des modèles de paramètres d'affichage d'une élection",
        "table": {
          "header": {
            "model-name": "Modèle de paramètres",
            "action": "Modèle de paramètres",
          }
        },
        "action": {
          "edit": "Editer",
          "delete": "Supprimer",
          "create": "Créer un modèle"
        },
        "delete-dialog": {
          "message": "Voulez vous vraiement supprimer le modèle de paramètres '{{modelName}}' ?"
        },
        "details": {
          "title": {
            "edit": "Éditer un modèle de paramètres d'affichage d'une élection",
            "create": "Définir un modèle de paramètres d'affichage d'une élection",
          }
        },
        "verificationCode": {
          "title": {
            "edit": "Éditer la configuration des codes de vérification",
            "create": "Définir la configuration des codes de vérification",
          }
        },
        "form": {
          "verification-table": {
            "column-to-display": {
              "title": "Colonnes à afficher sur le tableau de vérification",
              "add-column": "Ajouter une colonne",
              "remove-column": "Ne pas afficher la colonne"
            },
            "column-to-sort": {
              "title": "Tri du tableau de vérification",
              "add-column": "Ajouter un tri",
              "remove-column": "Supprimer le tri",
              "first-sort": "Trier par",
              "further-sort": "Puis par",
            },
            "columns": {
              "candidate-number": "Numéro du candidat",
              "candidate-identity": "Identité",
              "candidate-position": "Position sur le bulletin",
              "verification-code": "Code de vérification"
            }
          },
          "sections": {
            "ballot-paper": "Bulletin de vote",
            "list-display": "Affichage des listes",
            "candidate-display": "Affichage des candidats",
            "summary-page": "Page récapitulative",
            "verification-page": "Page de vérification",
            "rules": "Règles"
          },
          "fields": {
            "model-name": "Nom du modèle",
            "display-candidate-search-form": "Affichage du formulaire de recherche de candidats",
            "set-verification-code-display": "Paramétrer l'affichage des codes",
            "columns-for-verification-codes": {
              "candidate-number": "Numéro du candidat",
              "candidate-identity": "Identité du candidat",
              "candidate-position": "Numéro de position du candidat dans la liste",
              "verification-code": "Code de vérification",
            },
            "allow-change-of-electoral-list": "Autoriser le changement de liste électorale",
            "display-empty-position": "Afficher la ou les positions vide(s) dans une liste incomplète",
            "display-candidate-position-on-ballot-paper": {
              "title": "Affichage du numéro de position du candidat dans la liste",
              "compact-ballot-paper": "Bulletin compact",
              "modified-ballot-paper": "Bulletin modifié (ajout de candidat)",
            },
            "display-suffrage-count": "Afficher le calcul des suffrages",
            "display-list-verification-code": "Afficher les codes de vérification des listes",
            "candidate-information-display-model": "Modèle d'affichage des informations complémentaires",
            "allow-open-candidature": "Autoriser les candidatures libres",
            "allow-multiple-mandates": "Autoriser le cumul",
            "display-void-on-empty-ballot-paper": "Afficher un message si un bulletin est nul suivant la règle \"Un bulletin sans candidat et avec un nom de parti est nul\"",
          },
        }
      },
    },
  },
  de: {
    "side-bar-title": "DE - Affichage Élection",
    "card": {
      "title": "DE - Paramètres d'affichage d'une élection",
      "subtitle": "DE - ",
      "model-count": "DE - Modèles de paramètres",
      "mapping-count": "DE - Scrutins configurés",
    },
    "edit": {
      "title": "DE - Paramètres d'affichage d'une élection",
      "subtitle": "DE - Configurer l'affichage des scrutins type élection sur le site de vote",
      "mapping": {
        "title": "DE - Paramètres d'affichage par scrutin",
        "model-name": "DE - Modèle de paramètres",
        "action": {
          "delete": "DE - Supprimer",
          "unmap": "DE - Retirer l'association"
        },
        "delete-dialog": {
          "message": "DE - Le scrutin {{ballot}} n'est plus associé à l'opération. Souhaitez-vous supprimer cette association ?"
        },
        "bulk-update": {
          "select-ballot": "DE - 1. Sélection des scrutins",
          "select-model": "DE - 2. Sélection du modèle",
          "filter": "DE - Filtre",
          "validate": "DE - Associer",
          "open": "DE - Associer les scrutins en lot",
          "table": {
            "header": {
              "ballot": "DE - Scrutin"
            }
          }
        }
      },


      "models": {
        "title": "DE - Liste des modèles de paramètres d'affichage d'une élection",
        "table": {
          "header": {
            "model-name": "DE - Modèle de paramètres",
            "action": "DE - Modèle de paramètres",
          }
        },
        "action": {
          "edit": "DE - Editer",
          "delete": "DE - Supprimer",
          "create": "DE - Créer un modèle"
        },
        "delete-dialog": {
          "message": "DE - Voulez vous vraiement supprimer le modèle de paramètres '{{modelName}}' ?"
        },
        "details": {
          "title": {
            "edit": "DE - Éditer un modèle de paramètres d'affichage d'une élection",
            "create": "DE - Définir un modèle de paramètres d'affichage d'une élection",
          }
        },
        "verificationCode": {
          "title": {
            "edit": "DE - Éditer la configuration des codes de vérification",
            "create": "DE - Définir la configuration des codes de vérification",
          }
        },
        "form": {
          "verification-table": {
            "column-to-display": {
              "title": "DE - Colonnes à afficher sur le tableau de vérification",
              "add-column": "DE - Ajouter une colonne",
              "remove-column": "DE - Ne pas afficher la colonne"
            },
            "column-to-sort": {
              "title": "DE - Tri du tableau de vérification",
              "add-column": "DE - Ajouter un tri",
              "remove-column": "DE - Supprimer le tri",
              "first-sort": "DE - Trier par",
              "further-sort": "DE - Puis par",
            },
            "columns": {
              "candidate-number": "DE - Numéro du candidat",
              "candidate-identity": "DE - Identité",
              "candidate-position": "DE - Position sur le bulletin",
              "verification-code": "DE - Code de vérification"
            }
          },
          "sections": {
            "ballot-paper": "DE - Bulletin de vote",
            "list-display": "DE - Affichage des listes",
            "candidate-display": "DE - Affichage des candidats",
            "summary-page": "DE - Page récapitulative",
            "verification-page": "DE - Page de vérification",
            "rules": "DE - Règles"
          },
          "fields": {
            "model-name": "DE - Nom du modèle",
            "display-candidate-search-form": "DE - Affichage du formulaire de recherche de candidats",
            "set-verification-code-display": "DE - Paramétrer l'affichage des codes",
            "columns-for-verification-codes": {
              "candidate-number": "DE - Numéro du candidat",
              "candidate-identity": "DE - Identité du candidat",
              "candidate-position": "DE - Numéro de position du candidat dans la liste",
              "verification-code": "DE - Code de vérification",
            },
            "allow-change-of-electoral-list": "DE - Autoriser le changement de liste électorale",
            "display-empty-position": "DE - Afficher la ou les positions vide(s) dans une liste incomplète",
            "display-candidate-position-on-ballot-paper": {
              "title": "DE - Affichage du numéro de position du candidat dans la liste",
              "compact-ballot-paper": "DE - Bulletin compact",
              "modified-ballot-paper": "DE - Bulletin modifié (ajout de candidat)",
            },
            "display-suffrage-count": "DE - Afficher le calcul des suffrages",
            "display-list-verification-code": "DE - Afficher les codes de vérification des listes",
            "candidate-information-display-model": "DE - Modèle d'affichage des informations complémentaires",
            "allow-open-candidature": "DE - Autoriser les candidatures libres",
            "allow-multiple-mandates": "DE - Autoriser le cumul",
            "display-void-on-empty-ballot-paper": "DE - Afficher un message si un bulletin est nul suivant la règle \"Un bulletin sans candidat et avec un nom de parti est nul\"",
          },
        }
      },
    },
  },
};
