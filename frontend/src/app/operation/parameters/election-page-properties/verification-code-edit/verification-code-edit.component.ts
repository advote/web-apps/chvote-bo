/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import { MatSelect, MatSelectChange } from '@angular/material';

@Component({
  selector: 'verification-code-edit',
  templateUrl: './verification-code-edit.component.html',
  styleUrls: ['./verification-code-edit.component.scss']
})
export class VerificationCodeEditComponent {

  @Input()
  readOnly = false;

  @Input()
  possibleColumns: string[];

  @Input()
  displayedColumns: string[];

  @Input()
  sortOrder: string[];

  @Output()
  displayedColumnsChange = new EventEmitter<string[]>();

  @Output()
  sortOrderChange = new EventEmitter<string[]>();


  constructor() {
  }


  @ViewChildren(MatSelect)
  matSelects: QueryList<MatSelect>;

  changeColumnToDisplay(index, change: MatSelectChange) {
    let oldValue = this.displayedColumns[index];
    let newDisplayedColumns = this.displayedColumns.slice();
    newDisplayedColumns[index] = change.value;
    newDisplayedColumns = newDisplayedColumns.filter(c => !!c);
    this.emitChange(newDisplayedColumns);

    // Due to a bug in angular material design component
    setTimeout(() => {
      newDisplayedColumns.forEach((value, i) => {
        if (change.value == value && i != index) {
          newDisplayedColumns[i] = oldValue
        }
      });
      this.emitChange(newDisplayedColumns);
    });

  }


  changeColumnToSort(index, change: MatSelectChange) {
    let newOrder = this.sortOrder.slice();
    newOrder[index] = change.value;
    this.emitChange(null, newOrder.filter(c => !!c));
  }

  sortPossibleColumns(index: number) {
    return this.possibleColumns.filter(
      column => this.sortOrder.indexOf(column) == index || this.sortOrder.indexOf(column) < 0)

  }


  addColumnToDisplay() {
    let newColumns = this.displayedColumns.slice();
    newColumns.push(
      this.possibleColumns.filter(column => this.displayedColumns.indexOf(column) < 0)[0]
    );
    this.emitChange(newColumns);
  }

  addColumnToSort() {
    let newOrder = this.sortOrder.slice();
    newOrder.push(
      this.possibleColumns.filter(column => this.sortOrder.indexOf(column) < 0)[0]
    );
    this.emitChange(null, newOrder);
  }


  private emitChange(displayedColumns?: string[], sortOrder?: string[]) {
    if (displayedColumns) {
      this.displayedColumnsChange.emit(displayedColumns);
    }

    if (sortOrder) {
      this.sortOrderChange.emit(sortOrder);
    }

  }

}
