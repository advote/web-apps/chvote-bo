/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Operation } from '../../model/operation';
import { OperationFile } from '../../../core/model/operation-file';
import { DomainInfluenceFile } from '../domain-influence/model/domain-influence-file';
import { RepositoryFile } from '../repository/model/repository-file';
import { Observable } from 'rxjs/Observable';
import { OperationManagementService } from '../../service/operation-managment.service';
import { DomainInfluenceService } from "app/operation/parameters/domain-influence/services/domain-influence.service";
import { RepositoryService } from '../repository/service/repository.service';
import { DocumentService } from '../document/service/document.service';


/**
 * Data model used to store operation's parameters elements to be shared between sibling components.
 */
@Injectable()
export class OperationParametersModel {

  private domainInfluenceFileSource = new BehaviorSubject<DomainInfluenceFile>(new DomainInfluenceFile());
  currentDomainInfluenceFile: Observable<DomainInfluenceFile> = this.domainInfluenceFileSource.asObservable();
  private repositoryFilesSource = new BehaviorSubject<RepositoryFile[]>([]);
  currentRepositoryFiles: Observable<RepositoryFile[]> = this.repositoryFilesSource.asObservable();
  private documentFilesSource = new BehaviorSubject<OperationFile[]>([]);
  currentDocumentFiles: Observable<OperationFile[]> = this.documentFilesSource.asObservable();
  private operationDateEditable = new BehaviorSubject<boolean>(true);
  isOperationDateEditable: Observable<boolean> = this.operationDateEditable.asObservable();

  public constructor(private operationManagementService: OperationManagementService,
                     private doiService: DomainInfluenceService,
                     private repositoryService: RepositoryService,
                     private documentService: DocumentService,) {
    this.operationManagementService.operation.subscribe((operation) => {
      if (operation) {
        this.doiService.getFile(operation.id).subscribe(
          file => this.setDomainInfluenceFile(file)
        );
        this.repositoryService.list(operation.id).subscribe(
          files => this.setRepositoryFiles(files)
        );
        this.documentService.list(operation.id).subscribe(
          files => this.setDocumentFiles(files)
        );
        this.updateOperationDateEditable();
      }
    });
  }

  get currentOperation() {
    return this.operationManagementService.operation.asObservable();
  }

  get currentOperationStatus() {
    return this.operationManagementService.status.asObservable();
  }


  updateOperation(operation: Operation) {
    this.operationManagementService.updateOperation(operation);
    this.updateOperationDateEditable();
  }


  public updateDomainInfluenceFile(domainInfluenceFile: DomainInfluenceFile) {
    this.domainInfluenceFileSource.next(domainInfluenceFile);
    this.operationManagementService.shouldUpdateStatus();
  }

  public setDomainInfluenceFile(domainInfluenceFile: DomainInfluenceFile) {
    this.domainInfluenceFileSource.next(domainInfluenceFile);
  }

  public updateRepositoryFiles(repositoryFiles: RepositoryFile[]) {
    this.setRepositoryFiles(repositoryFiles);
    this.operationManagementService.shouldUpdateStatus();
  }

  public setRepositoryFiles(repositoryFiles: RepositoryFile[]) {
    this.repositoryFilesSource.next(repositoryFiles);
    this.updateOperationDateEditable();
  }

  public updateDocumentFiles(documentFiles: OperationFile[]) {
    this.setDocumentFiles(documentFiles);
    this.operationManagementService.shouldUpdateStatus();
  }

  public setDocumentFiles(documentFiles: OperationFile[]) {
    this.documentFilesSource.next(documentFiles);
  }

  private updateOperationDateEditable(): void {
    let operation = this.operationManagementService.operation.getValue();
    if (operation) {
      this.operationDateEditable.next(
        (!operation.milestones || operation.milestones.length == 0) &&
        (!this.repositoryFilesSource.getValue() || this.repositoryFilesSource.getValue().length == 0)
      );
    }
  }
}
