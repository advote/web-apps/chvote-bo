/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from '@angular/core';
import { ParametersRoutingModule } from "./parameters-routing.module";
import { ParametersComponent } from './parameters.component';
import { SharedModule } from '../../shared/shared.module';
import { BaseCardComponent } from './base/base-card.component';
import { MilestoneCardComponent } from './milestone/milestone-card.component';
import { DomainInfluenceCardComponent } from './domain-influence/domain-influence-card.component';
import { RepositoryCardComponent } from './repository/repository-card.component';
import { DocumentCardComponent } from './document/document-card.component';
import { DomainInfluenceService } from './domain-influence/services/domain-influence.service';
import { RepositoryService } from './repository/service/repository.service';
import { DocumentService } from './document/service/document.service';
import { OperationParametersModel } from './model/operation-parameters.model';
import { ParametersNotificationComponent } from './parameters-notification/parameters-notification.component';
import { TestingCardsLotModule } from '../testing-cards-lot/testing-cards-lot.module';
import { CovalentFileModule } from '@covalent/core';
import { RepositoryEditComponent } from './repository/repository-edit.component';
import { DomainInfluenceEditComponent } from './domain-influence/domain-influence-edit.component';
import { DocumentEditComponent } from './document/document-edit.component';
import { DomainInfluenceGridComponent } from './domain-influence/domain-influence-grid/domain-influence-grid.component';
import { MilestoneEditComponent } from './milestone/milestone-edit.component';
import { ParametersEditComponent } from './parameter-edit/parameters-edit.component';
import { BaseEditComponent } from './base/base-edit.component';
import { ManagementEntityModule } from './management-entity/management-entity.module';
import { HighlightedQuestionListComponent } from './document/highlighted-question/components/highlighted-question-list.component';
import { HighlightedQuestionEditComponent } from './document/highlighted-question/components/highlighted-question-edit.component';
import { HighlightedQuestionService } from './document/highlighted-question/services/highlighted-question.service';
import { LocalizedHighlightedQuestionListComponent } from './document/highlighted-question/components/localized-highlighted-question-list.component';
import { BallotDocumentationService } from './ballot-documentation/service/ballot-documentation.service';
import { BallotDocumentationCardComponent } from './ballot-documentation/ballot-documentation-card.component';
import { BallotDocumentationEditComponent } from './ballot-documentation/ballot-documentation-edit.component';
import { ElectionPagePropertiesCardComponent } from './election-page-properties/election-page-properties-card.component';
import { ElectionPagePropertiesModelEditComponent } from './election-page-properties/election-page-properties-model-edit.component';
import { ElectionPagePropertiesModelListComponent } from './election-page-properties/election-page-properties-model-list.component';
import { ElectionPagePropertiesEditComponent } from './election-page-properties/election-page-properties-edit.component';
import { ElectionPagePropertiesMappingComponent } from './election-page-properties/election-page-properties-mapping.component';
import { ElectionPagePropertiesService } from './election-page-properties/services/election-page-properties.service';
import { ElectionPagePropertiesModelResolver } from './election-page-properties/services/election-page-properties.resolver';
import { VotationDetailsComponent } from './repository/votation-details/votation-details.component';
import { VerificationCodeEditComponent } from './election-page-properties/verification-code-edit/verification-code-edit.component';
import { ElectionDetailsComponent } from './repository/election-details/election-details.component';
import { ElectionDetailsTableComponent } from './repository/election-details/election-details-table.component';
import { ElectionDetailsElectoralListComponent } from './repository/election-details/election-details-electoral-lists.component';
import { ModelSelectorComponent } from './election-page-properties/selector/model-selector.component';
import { VotingSiteConfigurationComponent } from './voting-site-configuration/edit/voting-site-configuration.component';
import { VotingSiteConfigurationService } from './voting-site-configuration/voting-site-configuration.service';
import { VotingSiteConfigurationCardComponent } from './voting-site-configuration/card/voting-site-configuration-card.component';

@NgModule({
  imports: [
    SharedModule,
    ParametersRoutingModule,
    CovalentFileModule,
    TestingCardsLotModule,
    ManagementEntityModule,
  ],
  declarations: [
    ParametersComponent,
    ParametersNotificationComponent,
    BaseCardComponent,
    MilestoneCardComponent,
    DomainInfluenceCardComponent,
    RepositoryCardComponent,
    DocumentCardComponent,
    BallotDocumentationCardComponent,
    BallotDocumentationEditComponent,
    ParametersEditComponent,
    BaseEditComponent,
    MilestoneEditComponent,
    DomainInfluenceGridComponent,
    DomainInfluenceEditComponent,
    RepositoryEditComponent,
    DocumentEditComponent,
    HighlightedQuestionListComponent,
    HighlightedQuestionEditComponent,
    LocalizedHighlightedQuestionListComponent,
    ElectionPagePropertiesCardComponent,
    ElectionPagePropertiesModelEditComponent,
    ElectionPagePropertiesModelListComponent,
    ElectionPagePropertiesEditComponent,
    ElectionPagePropertiesMappingComponent,
    VotationDetailsComponent,
    ElectionDetailsComponent,
    ElectionDetailsTableComponent,
    ElectionDetailsElectoralListComponent,
    VerificationCodeEditComponent,
    ModelSelectorComponent,
    VotingSiteConfigurationComponent,
    VotingSiteConfigurationCardComponent
  ],
  providers: [
    DomainInfluenceService,
    RepositoryService,
    DocumentService,
    OperationParametersModel,
    HighlightedQuestionService,
    BallotDocumentationService,
    ElectionPagePropertiesService,
    VotingSiteConfigurationService,
    ElectionPagePropertiesModelResolver
  ],
  entryComponents: [
    ElectionDetailsElectoralListComponent,
    ModelSelectorComponent
  ]
})
export class ParametersModule {
}
