/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { OperationResolver } from './service/operation-resolver';
import { OperationManagementComponent } from './operation-management.component';
import { AuthorizationGuard } from '../core/service/http/authorization.service';
import { TallyComponent } from './tally/tally.component';

const routes: Routes = [
  {
    path: '',
    component: OperationManagementComponent,
    children: [
      {
        path: 'parameters',
        loadChildren: 'app/operation/parameters/parameters.module#ParametersModule'
      },
      {
        path: 'voting-material',
        loadChildren: 'app/operation/voting-material/voting-material.module#VotingMaterialModule',
      },
      {
        path: 'voting-period',
        loadChildren: 'app/operation/voting-period/voting-period.module#VotingPeriodModule',
      },
      {
        path: 'tally',
        component: TallyComponent,
      },
    ],
    resolve: {
      operation: OperationResolver,
    },
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard]
  }
];

/**
 * Routing module for the operation management page.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperationManagementRoutingModule {
}
