/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit } from '@angular/core';
import {
  AVAILABLE_FOR_CREATION, AVAILABLE_FOR_INITIALIZATION, COMPLETE, CREATED, CREATION_FAILED, CREATION_IN_PROGRESS,
  CREATION_REJECTED, CREATION_REQUESTED, IN_VALIDATION, INCOMPLETE, INITIALIZATION_FAILED, INITIALIZATION_IN_PROGRESS,
  INITIALIZATION_REJECTED, INITIALIZATION_REQUESTED, INITIALIZED, INVALIDATED, INVALIDATION_REJECTED,
  INVALIDATION_REQUESTED, MessageWithParameters, NOT_REQUESTED, OperationStatus, VALIDATED
} from '../../model/operation-status';
import { TranslateService } from '@ngx-translate/core';
import { Operation } from '../../model/operation';
import { FileService } from '../../../core/service/file-service/file-service';
import { GeneratedVotingMaterialService } from '../../service/generated-voting-material.service';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { OperationManagementService } from '../../service/operation-managment.service';
import { OperationService } from '../../service/operation.service';
import { Router } from '@angular/router';
import { TdDialogService } from '@covalent/core';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'production-summary',
  templateUrl: './production-summary.component.html',
  styleUrls: ['./../deployment-summary.component.scss']
})
export class ProductionSummaryComponent implements OnInit {
  @Input()
  operationStatus: OperationStatus;

  @Input()
  operation: Operation;

  showDetails = false;

  constructor(private operationManagementService: OperationManagementService,
              private operationService: OperationService,
              private router: Router,
              private dialogService: TdDialogService,
              private translateService: TranslateService,
              private fileService: FileService,
              private generatedVotingMaterialService: GeneratedVotingMaterialService,
              private snackBar: MatSnackBar,
              private authorizationService: AuthorizationService) {
  }

  get inDeploymentStatusMessage() {
    return this.translateMessageWithParameters(this.operationStatus.deploymentStatusMessage);
  }

  get hasInProductionStatusMessage() {
    return !!this.operationStatus.inProductionStatusMessage;
  }

  get goToVoteReceiver() {
    return window.open(this.operationStatus.configurationStatus.voteReceiverUrl, "vote_receiver")
  }

  get inProductionStatusMessage() {
    return this.translateMessageWithParameters(this.operationStatus.inProductionStatusMessage);
  }

  get userBelongsToOperationManagementEntity() {
    return this.operation.managementEntity === this.authorizationService.userSnapshot.managementEntity;
  }

  get canDownloadNonPrintableTestingCards() {
    return this.userBelongsToOperationManagementEntity &&
           this.operationStatus.votingMaterialStatus.nonPrintableCardsAvailable;
  }

  get deploymentStatusIcon() {
    switch (this.operationStatus.deploymentTarget) {
      case "SIMULATION" :
        return "assignment";
      case "REAL" :
        return "flight";
      default :
        return "check";
    }
  }

  get productionStatusIcon() {
    const stateToTakeIntoAccount = this.getStatusToTakeIntoAccount();
    switch (stateToTakeIntoAccount) {
      case INCOMPLETE :
      case AVAILABLE_FOR_CREATION:
      case CREATION_REQUESTED:
      case AVAILABLE_FOR_INITIALIZATION:
      case INITIALIZATION_REQUESTED:
        return "info_outline";
      case COMPLETE :
      case CREATED :
      case INITIALIZED:
      case INVALIDATION_REJECTED:
        return "check";
      case CREATION_REJECTED:
      case CREATION_FAILED:
      case INVALIDATED:
      case INITIALIZATION_REJECTED:
      case INITIALIZATION_FAILED:
        return "cancel";
      default :
        return null;
    }
  }

  get isProductionStatusInError() {
    const vmState = this.operationStatus.votingMaterialStatus.state;
    const vpState = this.operationStatus.votingPeriodStatus.state;
    const taState = this.operationStatus.tallyArchiveStatus.state;
    return vmState === CREATION_REJECTED ||
           vmState === CREATION_FAILED ||
           vmState === INVALIDATION_REQUESTED ||
           vmState === INVALIDATION_REJECTED ||
           vmState === INVALIDATED ||
           vpState === INITIALIZATION_REJECTED ||
           vpState === INITIALIZATION_FAILED ||
           taState === CREATION_FAILED;

  }

  get isInProgress() {
    const vmState = this.operationStatus.votingMaterialStatus.state;
    const vpState = this.operationStatus.votingPeriodStatus ? this.operationStatus.votingPeriodStatus.state : null;
    return vmState === CREATION_IN_PROGRESS || vpState === INITIALIZATION_IN_PROGRESS;
  }

  ngOnInit() {
  }

  get isProdSiteAccessible() {
    return (this.operationStatus.tallyArchiveStatus.state !== CREATED &&
            (this.operationStatus.votingPeriodStatus.state === INITIALIZED &&
            this.operation.date >= new Date(Date.now())));
  }

  /**
   * Check if block containing URL of VR Site and/or testing cards archive should be displayed
   */
  get displayLinks() {
    return this.operationStatus.configurationStatus.state === IN_VALIDATION ||
           this.operationStatus.configurationStatus.state === VALIDATED ||
           this.operationStatus.votingMaterialStatus.state === VALIDATED ||
           this.operationStatus.votingMaterialStatus.state === CREATED ||
           this.operationStatus.votingMaterialStatus.state === INVALIDATION_REJECTED;
  }

  hasDetails() {
    return this.operationStatus.votingMaterialStatus &&
           this.operationStatus.votingMaterialStatus.state === CREATION_IN_PROGRESS
  }

  downloadNonPrintableTestingCards() {
    let id = this.operation.id;
    this.generatedVotingMaterialService.downloadTestSiteNonPrintableVotingCards(id)
      .subscribe(response => {
        let fileName = response.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1];
        this.fileService.downloadBlob(fileName, response.body);
      });
  }

  toggleDetails() {
    this.showDetails = !this.showDetails;
  }

  targetDeploymentInReal() {
    this.operationService.targetReal(this.operation.id)
      .subscribe(() => {
        this.snackBar.open(
          this.translateService.instant('deployment-summary.production.target.real.success'), '',
          {duration: 5000}
        );
        this.operationManagementService
          .shouldUpdateStatus()
          .subscribe(() => {
            this.router.navigate(["operations", this.operation.id, "voting-material"])
          });

      });
  }

  targetDeploymentInSimulation() {
    this.dialogService.openPrompt({
      title: this.translateService.instant('deployment-summary.production.target.simulation.dialog.title'),
      message: this.translateService.instant('deployment-summary.production.target.simulation.dialog.message'),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.cancel').toUpperCase(),
      acceptButton: this.translateService.instant('global.actions.validate').toUpperCase()
    }).afterClosed().subscribe((simulationName: string) => {
      if (simulationName) {
        this.operationService.targetSimulation(this.operation.id, simulationName)
          .subscribe(() => {
            this.snackBar.open(
              this.translateService.instant('deployment-summary.production.target.simulation.success'), '',
              {duration: 5000}
            );
            this.operationManagementService
              .shouldUpdateStatus()
              .subscribe(() => {
                this.router.navigate(["operations", this.operation.id, "voting-material"])
              });
          });
      }
    });
  }

  private translateMessageWithParameters(message: MessageWithParameters) {
    return this.translateService.instant(message.message, message.parameters);
  }

  private getStatusToTakeIntoAccount() {
    const taState = this.operationStatus.tallyArchiveStatus.state;
    if (taState !== NOT_REQUESTED) {
      return taState
    }
    const vmState = this.operationStatus.votingMaterialStatus.state;
    if (vmState !== VALIDATED) {
      return vmState;
    }
    return this.operationStatus.votingPeriodStatus.state;
  }


}
