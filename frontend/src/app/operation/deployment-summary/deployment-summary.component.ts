/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input } from '@angular/core';
import { Operation } from '../model/operation';
import { DEPLOYED, OperationStatus } from '../model/operation-status';
import { OperationService } from '../service/operation.service';
import { OperationManagementService } from '../service/operation-managment.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TdDialogService } from '@covalent/core';
import { flatMap } from 'rxjs/operators';

@Component({
  selector: 'deployment-summary',
  templateUrl: './deployment-summary.component.html',
  styleUrls: ['./deployment-summary.component.scss']
})
export class DeploymentSummaryComponent {

  @Input()
  operation: Operation;

  @Input()
  operationStatus: OperationStatus;

  constructor(private operationService: OperationService,
              private operationManagementService: OperationManagementService,
              private router: Router,
              private dialogService: TdDialogService,
              private translateService: TranslateService) {
  }


  isTestSiteVisible() {
    return this.operationStatus.configurationStatus.state !== DEPLOYED || this.isInModification();
  }

  isProdSiteVisible() {
    return this.operationStatus.configurationStatus.state === DEPLOYED || this.isInModification();
  }

  get isModifiable() {
    return this.operationStatus.configurationStatus.modificationMode == "FULLY_MODIFIABLE" ||
           this.operationStatus.configurationStatus.modificationMode == "PARTIALLY_MODIFIABLE";
  }

  modifyConfiguration() {
    this.dialogService.openConfirm({
      message: this.translateService.instant(
        this.operationStatus.configurationStatus.modificationMode == 'IN_PARTIAL_MODIFICATION' ?
          'deployment-summary.actions.modify.confirm.partial-modification' :
          'deployment-summary.actions.modify.confirm.full-modification'
      ),

      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.operationService.modifyConfiguration(this.operation.id)
          .pipe(
            flatMap(() => this.operationManagementService.shouldUpdateStatus(false))
          )
          .subscribe(() => {
            this.router.navigate(["operations", this.operation.id, "parameters"]);
          });
      }
    });


  }

  private isInModification() {
    return this.operationStatus.configurationStatus.modificationMode == 'IN_PARTIAL_MODIFICATION' ||
           this.operationStatus.configurationStatus.modificationMode == 'IN_FULL_MODIFICATION';
  }
}
