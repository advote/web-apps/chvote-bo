/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_deployment_summary = {
  fr: {
    "test": {
      "name": "test",
      "link": {
        "site": "accès au site",
        "voting-cards": "cartes de vote de test"
      }
    },
    "production": {
      "name": "opération",
      "link": {
        "site": "accès au site",
        "voting-cards": "cartes de vote de test"
      },
      "target": {
        "simulation": {
          "button": "mode simulation",
          "success": "L'opération sera déployée en mode simulation",
          "dialog": {
            "title": "Donner un nom à votre simulation",
            "message": "Intitulé de la simulation"
          },
          "status": {
            "in-preparation": "Simulation \"{{simulationName}}\"",
            "started": "Simulation \"{{simulationName}}\" démarrée le {{date}} par {{user}}"
          }
        },
        "real": {
          "button": "mode réel",
          "success": "Les paramètres ont été validés",
          "status": {
            "in-preparation": "Opération réelle",
            "started": "Opération réelle démarrée le {{date}} par {{user}}"
          }
        }
      }
    },
    "configuration": {
      "status": {
        "incomplete": "Paramétrage en cours d'élaboration",
        "complete": "Paramétrage défini le {{date}} par {{user}}",
        "test_site_in_deployment": "En cours de création",
        "in_error": "Erreur lors de la création du site de test",
        "in_validation": "Site en phase de test du paramétrage",
        "invalidated": "Paramétrage invalidé le {{date}} par {{user}}",
        "validated": "Paramétrage validé le {{date}} par {{user}}",
        "deployment_requested": "Déploiement demandé le {{date}} par {{user}}",
        "deployment_refused": "Déploiement refusé le {{date}} par {{user}} : {{reason}}",
        "deployed": "Déploiement validé le {{date}} par {{user}}"
      }
    },
    "voting-material": {
      "progress": {
        "control-components": {
          "-1": "Tous les composants de contrôle",
          "0": "Premier composant de contrôle",
          "1": "Deuxième composant de contrôle",
          "2": "Troisième composant de contrôle",
          "3": "Quatrième composant de contrôle",
          "stuck": "Le composant de contrôle ne semble pas répondre",
        },
        "steps": {
          "INITIALIZE_PARAMETERS": "Envoi du paramétrage d'opération",
          "SEND_VOTERS": "Génération des codes des votants",
          "BUILD_PUBLIC_CREDENTIALS": "Calcul des codes publics",
          "REQUEST_PRIVATE_CREDENTIALS": "Agrégation de l'archive imprimeur",
          "stuck": "Au moins un des composants de contrôle semble ne pas répondre",
        }
      },
      "status": {
        "incomplete": "Configuration du matériel de vote en cours d'élaboration",
        "complete": "Configuration du matériel de vote définie le {{date}} par {{user}}",
        "available_for_creation": "Configuration du matériel de vote envoyé le {{date}} par {{user}} et disponible pour création",
        "creation_requested": "Création de matériel de vote demandée le {{date}} par {{user}}",
        "creation_rejected": "Création du matériel de vote refusée le {{date}} par {{user}} : {{comment}}",
        "creation_failed": "Échec de la création du matériel de vote le {{date}}",
        "creation_in_progress": "Matériel de vote en cours de création",
        "created": "Matériel de vote créé le {{date}} par {{user}}",
        "validated": "Matériel de vote validé le {{date}} par {{user}}",
        "invalidation_requested": "Matériel de vote créé. Une demande d'invalidation a été émise le {{date}} par {{user}}",
        "invalidation_rejected": "La demande d'invalidation a été refusée le {{date}} par {{user}}.<br> Le matériel de vote reste disponible pour validation.",
        "invalidated": "Matériel de vote invalidé le {{date}} par {{user}}"
      },
      "modification": {
        "errors": {
          "cannot-switch-to-modification-mode": "Impossible de passer en mode \"modification de paramétrage\""
        }
      }
    },
    "voting-period": {
      "status": {
        "incomplete": "Paramétrage de la période de vote en cours d'élaboration",
        "complete": "Paramétrage de la période de vote défini le {{date}} par {{user}}",
        "available_for_initialization": "Paramétrage de la période de vote envoyé le {{date}} par {{user}} et disponible pour initialisation",
        "initialization_requested": "Initialisation de la période de vote demandée le {{date}} par {{user}}",
        "initialization_rejected": "Initialisation de la période de vote refusée le {{date}} par {{user}} : {{comment}}",
        "initialization_failed": "Échec de l'initialisation de la période de vote le {{date}}",
        "initialization_in_progress": "Période de vote en cours d'initialisation",
        "initialized": "Initialisation de la période de vote validée le {{date}} par {{user}}",
      }
    },
    "tally-archive": {
      "status": {
        "creation_in_progress": "L'archive de dépouillement est en cours de création",
        "creation_failed": "La création de l'archive de dépouillement a échoué",
        "created": "L'archive de dépouillement est disponible",
        "not_enough_votes_cast": "Aucune archive de dépouillement disponible"
      }
    },
    "actions": {
      "modify": {
        "button": "Modifier le paramétrage",
        "confirm": {
          "full-modification": "Êtes-vous sûr de vouloir activer une modification du paramétrage de l'opération ? " +
                               "Le paramétrage courant sera définitivement perdu. " +
                               "Il faudra poursuivre le processus jusqu'au déploiement du paramétrage.",

          "partial-modification": "Êtes-vous sûr de vouloir activer une modification du paramétrage de l'opération ? " +
                                  "Le matériel de vote étant créé, vous ne pouvez modifier que certains éléments du paramétrage. " +
                                  "Toute modification du paramétrage est définitive."
        }
      }
    }
  },
  de: {
    "test": {
      "name": "DE - test",
      "link": {
        "site": "DE - accès au site",
        "voting-cards": "DE - cartes de vote de test"
      }
    },
    "production": {
      "name": "DE - opération",
      "link": {
        "site": "DE - accès au site",
        "voting-cards": "DE - cartes de vote de test"
      },
      "target": {
        "simulation": {
          "button": "DE - mode simulation",
          "success": "DE - L'opération sera déployée en mode simulation",
          "dialog": {
            "title": "DE - Donner un nom à votre simulation",
            "message": "DE - Intitulé de la simulation"
          },
          "status": {
            "in-preparation": "DE - Simulation \"{{simulationName}}\"",
            "started": "DE - Simulation \"{{simulationName}}\" démarrée le {{date}} par {{user}}"
          }
        },
        "real": {
          "button": "DE - mode réel",
          "success": "DE - Les paramètres ont été validés",
          "status": {
            "in-preparation": "DE - Opération réelle",
            "started": "DE - Opération réelle démarrée le {{date}} par {{user}}"
          }
        }
      }
    },
    "configuration": {
      "status": {
        "incomplete": "DE - Paramétrage en cours d'élaboration",
        "complete": "DE - Paramétrage défini le {{date}} par {{user}}",
        "test_site_in_deployment": "DE - En cours de création",
        "in_error": "DE - Erreur lors de la création du site de test",
        "in_validation": "DE - Site en phase de test du paramétrage",
        "invalidated": "DE - Paramétrage invalidé le {{date}} par {{user}}",
        "validated": "DE - Paramétrage validé le {{date}} par {{user}}",
        "deployment_requested": "DE - Déploiement demandé le {{date}} par {{user}}",
        "deployment_refused": "DE - Déploiement refusé le {{date}} par {{user}} : {{reason}}",
        "deployed": "DE - Déploiement validé le {{date}} par {{user}}"
      }
    },
    "voting-material": {
      "progress": {
        "control-components": {
          "-1": "DE - Tous les composants de contrôle",
          "0": "DE - Premier composant de contrôle",
          "1": "DE - Deuxième composant de contrôle",
          "2": "DE - Troisième composant de contrôle",
          "3": "DE - Quatrième composant de contrôle",
          "stuck": "DE - Le composant de contrôle ne semble pas répondre",
        },
        "steps": {
          "INITIALIZE_PARAMETERS": "DE - Envoi du paramétrage d'opération",
          "SEND_VOTERS": "DE - Génération des codes des votants",
          "BUILD_PUBLIC_CREDENTIALS": "DE - Calcul des codes publics",
          "REQUEST_PRIVATE_CREDENTIALS": "DE - Agrégation de l'archive imprimeur",
          "stuck": "DE - Au moins un des composants de contrôle semble ne pas répondre",
        }
      },
      "status": {
        "incomplete": "DE - Configuration du matériel de vote en cours d'élaboration",
        "complete": "DE - Configuration du matériel de vote définie le {{date}} par {{user}}",
        "available_for_creation": "DE - Configuration du matériel de vote envoyé le {{date}} par {{user}} et disponible pour création",
        "creation_requested": "DE - Création de matériel de vote demandée le {{date}} par {{user}}",
        "creation_rejected": "DE - Création du matériel de vote refusée le {{date}} par {{user}} : {{comment}}",
        "creation_failed": "DE - Échec de la création du matériel de vote le {{date}}",
        "creation_in_progress": "DE - Matériel de vote en cours de création",
        "created": "DE - Matériel de vote créé le {{date}} par {{user}}",
        "validated": "DE - Matériel de vote validé le {{date}} par {{user}}",
        "invalidation_requested": "DE - Matériel de vote créé. Une demande d'invalidation a été émise le {{date}} par {{user}}",
        "invalidation_rejected": "DE - La demande d'invalidation a été refusée le {{date}} par {{user}}.<br> Le matériel de vote reste disponible pour validation.",
        "invalidated": "DE - Matériel de vote invalidé le {{date}} par {{user}}"
      },
      "modification": {
        "errors": {
          "cannot-switch-to-modification-mode": "Impossible de passer en mode \"modification de paramétrage\""
        }
      }
    },
    "voting-period": {
      "status": {
        "incomplete": "DE - Paramétrage de la période de vote en cours d'élaboration",
        "complete": "DE - Paramétrage de la période de vote défini le {{date}} par {{user}}",
        "available_for_initialization": "DE - Paramétrage de la période de vote envoyé le {{date}} par {{user}} et disponible pour initialisation",
        "initialization_requested": "DE - Initialisation de la période de vote demandée le {{date}} par {{user}}",
        "initialization_rejected": "DE - Initialisation de la période de vote refusée le {{date}} par {{user}} : {{comment}}",
        "initialization_failed": "DE - Échec de l'initialisation de la période de vote le {{date}}",
        "initialization_in_progress": "DE - Période de vote en cours d'initialisation",
        "initialized": "DE - Initialisation de la période de vote validée le {{date}} par {{user}}",
      }
    },
    "tally-archive": {
      "status": {
        "creation_in_progress": "DE - L'archive de dépouillement est en cours de création",
        "creation_failed": "DE - La création de l'archive de dépouillement a échoué",
        "created": "DE - L'archive de dépouillement est disponible",
        "not_enough_votes_cast": "DE - Aucune archive de dépouillement disponible"
      }
    },
    "actions": {
      "modify": {
        "button": "DE - Modifier le paramétrage",
        "confirm": {
          "full-modification": "DE - Êtes-vous sûr de vouloir activer une modification du paramétrage de l'opération ?" +
                               "Le paramétrage courant sera définitivement perdu." +
                               "Il faudra poursuivre le processus jusqu'au déploiement du paramétrage.",
          "partial-modification": "DE - Êtes-vous sûr de vouloir activer une modification du paramétrage de l'opération ?" +
                                  "Le matériel de vote étant créé, vous ne pouvez modifier que certains éléments du paramétrage." +
                                  "Toute modification du paramétrage est définitive."
        }
      }
    }
  }
};
