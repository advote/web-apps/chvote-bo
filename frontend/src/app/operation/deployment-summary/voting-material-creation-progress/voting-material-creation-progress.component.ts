/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnInit } from '@angular/core';
import { VotingMaterialCreationProgress, VotingMaterialCreationProgressStep } from '../../model/operation-status';
import { OperationManagementService } from '../../service/operation-managment.service';

@Component({
  selector: 'voting-material-creation-progress',
  templateUrl: './voting-material-creation-progress.component.html',
  styleUrls: ['./voting-material-creation-progress.component.scss']
})
export class VotingMaterialCreationProgressComponent implements OnInit {

  progress: VotingMaterialCreationProgress;

  constructor(operationManagementService: OperationManagementService) {
    operationManagementService.status.subscribe(status => this.updateProgress(status.votingMaterialStatus.progress));


  }

  ngOnInit() {
  }

  private updateProgress(progress: VotingMaterialCreationProgress) {
    if (progress) {
      if (!this.progress) {
        this.progress = progress;
      } else {
        this.progress.stuck = progress.stuck;
        progress.steps.forEach((step, i) => this.updateStep(this.progress.steps[i], step));
      }
    }
  }

  private updateStep(oldStep: VotingMaterialCreationProgressStep,
                     newStep: VotingMaterialCreationProgressStep) {
    Object.assign(oldStep, newStep);
  }
}
