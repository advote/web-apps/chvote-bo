/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input } from '@angular/core';
import { FileService } from '../../core/service/file-service/file-service';
import { Operation } from '../model/operation';
import { GeneratedVotingMaterialService } from '../service/generated-voting-material.service';

@Component({
  selector: 'test-site-cards-download-button',
  template: `
    <button mat-button
            (click)="download()"
            *roles="['DOWNLOAD_GENERATED_CARDS_FOR_TEST_SITE']">
      <mat-icon>recent_actors</mat-icon>
      {{'deployment-summary.test.link.voting-cards' | translate | uppercase}}
    </button>
  `,
})
export class TestSiteCardsDownloadButtonComponent {

  @Input()
  operation: Operation;

  constructor(private fileService: FileService,
              private generatedVotingMaterialService: GeneratedVotingMaterialService) {
  }


  download() {
    this.generatedVotingMaterialService.downloadTestSiteVotingCards(this.operation.id)
      .subscribe(response => {
        let fileName = response.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1];
        this.fileService.downloadBlob(fileName, response.body);
      });
  }
}
