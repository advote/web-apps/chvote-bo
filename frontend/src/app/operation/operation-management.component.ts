/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Operation } from './model/operation';
import { NOT_REQUESTED, OperationStatus, VALIDATED } from './model/operation-status';
import { OperationManagementService } from './service/operation-managment.service';
import { Subscription } from "rxjs/Subscription";
import { interval } from 'rxjs/observable/interval';
import { HttpParameters } from '../core/service/http/http.parameters.service';

const CHECK_STATUS_INTERVAL = 5000;

@Component({
  templateUrl: './operation-management.component.html',
  styleUrls: ['./operation-management.component.scss']
})
export class OperationManagementComponent implements OnInit, OnDestroy {

  operation: Operation;
  operationStatus: OperationStatus;
  subscriptions: Subscription[] = [];

  constructor(private operationManagementService: OperationManagementService,
              private httpParameters: HttpParameters,
              private router: Router, private ngZone: NgZone) {
    (<any>document)._forceStatusRefresh = () => operationManagementService.shouldUpdateStatus(false);
  }

  get operationExists() {
    return this.operation && this.operationStatus;
  }

  get votingMaterialValidated() {
    return this.operationStatus.votingMaterialStatus.state == VALIDATED;
  }


  get isTallyStarted() {
    return this.operationStatus.tallyArchiveStatus.state != NOT_REQUESTED;
  }

  ngOnInit() {
    this.subscriptions.push(
      this.operationManagementService.operation.subscribe((operation) => {
        this.operation = operation;
      }),

      this.operationManagementService.status.subscribe((status) => {
        this.operationStatus = status;
        this.validateRoute();
      }),
    );


    // for e2e testing : Protractor wait no more update get done and therefore wait interval to finish which never
    // occurs ...
    this.ngZone.runOutsideAngular(() => {
      this.subscriptions.push(
        interval(CHECK_STATUS_INTERVAL)
          .subscribe(() => {
            if (!this.httpParameters.stopBackgroundRequest.getValue()) {
              this.ngZone.run(() => this.operationManagementService.shouldUpdateStatus(true));
            }
          })
      )
    });
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  private validateRoute() {
    if (this.operationExists) {
      let parametersRoute = ["operations", this.operation.id, "parameters"];
      let votingMaterialRoute = ["operations", this.operation.id, "voting-material"];
      let votingPeriodRoute = ["operations", this.operation.id, "voting-period"];
      let tallyRoute = ["operations", this.operation.id, "tally"];

      if (!this.router.isActive(this.router.createUrlTree(parametersRoute), false) &&
          !this.router.isActive(this.router.createUrlTree(votingMaterialRoute), false) &&
          !this.router.isActive(this.router.createUrlTree(votingPeriodRoute), false) &&
          !this.router.isActive(this.router.createUrlTree(tallyRoute), false)
      ) {
        if (this.operationStatus.tallyArchiveStatus.state != NOT_REQUESTED) {
          this.router.navigate(tallyRoute, {replaceUrl: true});
        } else if (this.operationStatus.votingMaterialStatus.state == VALIDATED) {
          this.router.navigate(votingPeriodRoute, {replaceUrl: true});
        } else if (this.operationStatus.deploymentTarget !== 'NOT_DEFINED') {
          this.router.navigate(votingMaterialRoute, {replaceUrl: true});
        } else {
          this.router.navigate(parametersRoute, {replaceUrl: true});
        }
      }
    }
  }
}
