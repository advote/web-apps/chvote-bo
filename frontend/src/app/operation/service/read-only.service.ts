/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { AuthorizationService } from '../../core/service/http/authorization.service';
import { OperationManagementService } from './operation-managment.service';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { OperationParameterType } from './operation-parameter-type';
import { ReadOnlyConfigurationSection } from '../model/operation-status';

@Injectable()
export class ReadOnlyService {

  public constructor(private operationManagementService: OperationManagementService,
                     private authService: AuthorizationService) {
  }

  isBaseParameterInReadOnly() {
    return this.isConfigurationSectionInReadOnly("base-parameter");
  }

  isMilestoneInReadOnly() {
    return this.isConfigurationSectionInReadOnly("milestone");
  }

  isDocumentInReadOnly() {
    return this.isConfigurationSectionInReadOnly("document");
  }

  isDoiInReadOnly() {
    return this.isConfigurationSectionInReadOnly("doi");
  }

  isManagementEntityInReadOnly() {
    return this.isConfigurationSectionInReadOnly("management-entity");
  }

  isRepositoryInReadOnly() {
    return this.isConfigurationSectionInReadOnly("repository");
  }

  isBallotDocumentationInReadOnly() {
    return this.isConfigurationSectionInReadOnly("ballot_documentation");
  }

  isElectionPagePropertiesInReadOnly() {
    return this.isConfigurationSectionInReadOnly("election_page_properties");
  }

  isTestingCardInReadOnly(forConfiguration: boolean) {
    if (forConfiguration) {
      return this.isConfigurationSectionInReadOnly('testing-card');
    }
    return this.isReadOnly(OperationParameterType.VOTING_MATERIAL,
      true, "EDIT_TEST_VOTING_CARD_DEFINITION", "DELETE_TEST_VOTING_CARD_DEFINITION",
      "CREATE_TEST_VOTING_CARD_DEFINITION");
  }

  isCardTitleInReadOnly() {
    return this.isReadOnly(OperationParameterType.VOTING_MATERIAL, true, "UPDATE_VOTING_CARD_TITLE");
  }

  isPrinterTemplateInReadOnly() {
    return this.isReadOnly(OperationParameterType.VOTING_MATERIAL, true, "SELECT_PRINTER_TEMPLATE");
  }

  isRegisterInReadOnly() {
    return this.isReadOnly(OperationParameterType.VOTING_MATERIAL, false, "DELETE_REGISTER_FILE",
      "UPLOAD_REGISTER_FILE");
  }

  isSelectedElectoralAuthorityKeyInReadonly() {
    return this.isReadOnly(OperationParameterType.VOTING_PERIOD, true, "SELECT_ELECTORAL_AUTHORITY_KEY");
  }

  isVotingSitePeriodInReadonly() {
    return this.isReadOnly(OperationParameterType.VOTING_PERIOD, true, "SELECT_SIMULATION_PERIOD").pipe(
      map(readOnly => {
        if (this.operationManagementService.status.getValue().deploymentTarget == "REAL") {
          return true;
        }
        return readOnly;
      })
    );
  }

  private isReadOnly(parameterType: OperationParameterType.VOTING_MATERIAL | OperationParameterType.VOTING_PERIOD,
                     restrictedToOperationManagementEntity: boolean,
                     ...roles: string[]): Observable<boolean> {


    let readonlyBySection = status =>
      (parameterType == OperationParameterType.VOTING_MATERIAL && status.votingMaterialStatus.readOnly) ||
      (parameterType == OperationParameterType.VOTING_PERIOD && status.votingPeriodStatus.readOnly);

    let readonlyByManagementId = () =>
      restrictedToOperationManagementEntity &&
      this.operationManagementService.operation.getValue().managementEntity !==
      this.authService.userSnapshot.managementEntity;

    return combineLatest(this.operationManagementService.operation, this.operationManagementService.status)
      .pipe(
        map(([ope, status]) => {
          return !ope || !status || readonlyBySection(status) ||
                 readonlyByManagementId() || !this.authService.hasAtLeastOneRole(roles);
        }));
  }

  private isConfigurationSectionInReadOnly(section: ReadOnlyConfigurationSection) {
    return this.operationManagementService.status.pipe(
      map(status => status.configurationStatus.readOnlySections.indexOf(section) >= 0)
    );
  }


  isVotingSiteConfigurationInReadonly() {
    return this.isConfigurationSectionInReadOnly("voting_site_configuration")
  }
}
