/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";

import { OperationStatus } from '../model/operation-status';
import { HttpParameters } from '../../core/service/http/http.parameters.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BACKGROUND_PROCESSING } from '../../core/service/http/InProgressInterceptor';

@Injectable()
export class OperationStatusService {


  private serviceUrl: string;

  constructor(private http: HttpClient, httpParams: HttpParameters) {
    this.serviceUrl = httpParams.apiBaseURL + '/operation';
  }

  getStatus(operationId, forPolling): Observable<OperationStatus> {

    return this.http.get<OperationStatus>(`${this.serviceUrl}/${operationId}/status`,
      {headers: forPolling ? new HttpHeaders().append(BACKGROUND_PROCESSING, "true") : new HttpHeaders()});
  }

}
