/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Operation } from '../../model/operation';
import { OperationManagementService } from '../../service/operation-managment.service';
import { TestingCardsLot } from '../model/testing-cards-lot';
import { TestingCardsLotService } from '../service/testing-cards-lot.service';
import { Subscription } from 'rxjs/Subscription';
import { ReadOnlyService } from '../../service/read-only.service';

@Component({
  selector: 'testing-cards-lot-card',
  templateUrl: './card.component.html'
})
export class TestingCardsLotCardComponent implements OnDestroy, OnInit {
  lots: TestingCardsLot[] = [];
  @Input() inConfiguration: boolean;
  @Input() operation: Operation;
  testingMaterialAvailability = false;
  completed = false;
  inError = false;
  private _subscriptions: Subscription[] = [];
  private _readOnly: boolean;

  constructor(private operationManagementService: OperationManagementService,
              private testingCardLotService: TestingCardsLotService,
              private readOnlyService: ReadOnlyService) {
  }

  get readOnly(): boolean {
    return this._readOnly;
  }

  get editSection() {
    return this.inConfiguration ? ['edit', 'testing-card-lot'] : ['testing-card-lot'];
  }

  get lotCount() {
    return this.lots.length;
  }

  get i18nSubSection() {
    return this.inConfiguration ? "for-configuration" : "for-voting-material";
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  ngOnInit(): void {
    this._subscriptions.push(
      this.readOnlyService.isTestingCardInReadOnly(this.inConfiguration)
        .subscribe(readOnly => this._readOnly = readOnly),

      this.testingCardLotService.findAll(this.operation.id, this.inConfiguration)
        .subscribe(lots => this.lots = lots),

      this.operationManagementService.status.subscribe(status => {
        this.testingMaterialAvailability =
          this.testingCardLotService.getTestingMaterialAvailability(status, this.inConfiguration);
        this.completed = (this.inConfiguration ? status.configurationStatus : status.votingMaterialStatus)
          .completedSections["testing-card-lot"];
        this.inError = (this.inConfiguration ? status.configurationStatus : status.votingMaterialStatus)
          .sectionsInError["testing-card-lot"];
      })
    )
  }

}
