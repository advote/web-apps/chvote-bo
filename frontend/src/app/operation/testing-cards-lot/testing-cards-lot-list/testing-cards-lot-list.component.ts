/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DataSource } from '@angular/cdk/collections';
import { MatSort } from "@angular/material/sort";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { TdDialogService } from '@covalent/core';
import { sortOnProperty } from '../../../core/util/table-utils';
import { OperationManagementService } from '../../service/operation-managment.service';
import { Subscription } from 'rxjs/Subscription';
import { map } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';
import { TestingCardsLot } from '../model/testing-cards-lot';
import { TestingCardsLotService } from '../service/testing-cards-lot.service';
import { ReadOnlyService } from '../../service/read-only.service';
import { OperationDataService } from '../../service/operation.data.service';
import { DomainOfInfluence } from '../../../model/domain-of-Influence.model';

@Component({
  selector: 'testing-cards-lot-list',
  templateUrl: './testing-cards-lot-list.component.html',
  styleUrls: ['./testing-cards-lot-list.component.scss']
})
export class TestingCardsLotListComponent implements OnInit, OnDestroy {

  testingCardsLotSubject = new BehaviorSubject<TestingCardsLot[]>([]);

  dataSource;

  subscriptions: Subscription[] = [];

  @ViewChild(MatSort) sort: MatSort;
  relatedDoi: DomainOfInfluence[];

  constructor(private route: ActivatedRoute,
              private translateService: TranslateService,
              private testingCardsLotService: TestingCardsLotService,
              private operationDataService: OperationDataService,
              private operationManagementService: OperationManagementService,
              private dialogService: TdDialogService,
              private readOnlyService: ReadOnlyService,
              private router: Router) {
  }

  private _readOnly: boolean;

  get readOnly() {
    return this._readOnly;
  }

  get displayedColumns(): string[] {
    return ["lotName", "cardType", "count", "voter", "language", "shouldPrint", "actions"];

  }

  get elementsCount() {
    return this.testingCardsLotSubject.value.length;
  }

  get forConfiguration() {
    return !!this.route.snapshot.data.forConfiguration;
  }

  ngOnInit() {
    this.dataSource =
      new TestingCardsLotDataSource(this.testingCardsLotSubject, this.sort, this.translateService);


    this.subscriptions.push(
      this.readOnlyService.isTestingCardInReadOnly(this.forConfiguration)
        .subscribe(readOnly => this._readOnly = readOnly),
      this.operationManagementService.operation.subscribe(operation => {
        this.testingCardsLotService.findAll(operation.id, this.forConfiguration)
          .subscribe(data => this.testingCardsLotSubject.next(data));
        this.operationDataService.getAllRelatedDois(operation.id).subscribe(
          dois => this.relatedDoi = dois.filter(doi => doi.type !== "Unknown"));


      })
    )
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  createNew() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }

  remove(element: TestingCardsLot) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('testing-cards-lot.dialogs.delete', element),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.testingCardsLotService.remove(element.id)
          .subscribe(() => this.testingCardsLotSubject.next(
            this.testingCardsLotSubject.getValue().filter((e) => e.id != element.id)));
      }
    });
  }

  edit(element: TestingCardsLot) {
    this.router.navigate([element.id], {relativeTo: this.route});
  }

}


class TestingCardsLotDataSource extends DataSource<any> {

  constructor(private data: BehaviorSubject<TestingCardsLot[]>, private sort: MatSort,
              private translateService: TranslateService) {
    super();
  }

  connect(): Observable<any[]> {
    return merge(this.data, this.sort.sortChange)
      .pipe(
        map(() => this.data.getValue().map(
          lot => Object.assign({}, lot, {
            cardType: this.translateService.instant(`dictionary.card-type.${lot.cardType}`),
            signature: this.translateService.instant(`dictionary.signature.${lot.signature}`),
            shouldPrint: this.translateService.instant(
              `testing-cards-lot.form.values.should-print.${lot.shouldPrint}`),
            address: [lot.address1, lot.address2, lot.street, `${lot.postalCode} ${lot.city} ${lot.country}`].filter(
              v => !!v)

          })
        )),
        map(translated => sortOnProperty(translated, this.sort.active, this.sort.direction)));
  }

  disconnect() {
  }
}
