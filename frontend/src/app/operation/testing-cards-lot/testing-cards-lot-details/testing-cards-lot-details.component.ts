/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { MatInput, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { DomainOfInfluence } from '../../../model/domain-of-Influence.model';
import { OperationDataService } from '../../service/operation.data.service';
import { OperationManagementService } from '../../service/operation-managment.service';
import { PrinterTemplateService } from '../../voting-material/printer/services/printer-template.service';
import {
  enableFormComponent, enableFormComponents, focusOnError, isFormModifiedAndNotSaved, REGEX_ALPHA_NUMERICAL_WITH_ACCENT,
  validateRegex
} from '../../../core/util/form-utils';
import { take, zip } from 'rxjs/operators';
import { TestingCardsLotService } from '../service/testing-cards-lot.service';
import * as moment from 'moment';
import { ConfirmBeforeQuit } from '../../../core/confirm-before-quit';
import { ReadOnlyService } from '../../service/read-only.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'testing-cards-lot-details',
  templateUrl: './testing-cards-lot-details.component.html',
  styleUrls: ['./testing-cards-lot-details.component.scss'],
  providers: [PrinterTemplateService]
})
export class TestingCardsLotDetailsComponent implements OnInit, ConfirmBeforeQuit, OnDestroy {
  form: FormGroup;

  mode = "create";
  @ViewChildren(MatInput) inputs: QueryList<MatInput>;
  readOnly: boolean;
  private subscriptions: Subscription[] = [];
  private allDoiForOperation: DomainOfInfluence[] = [];
  private operationId: number;
  private printerForTest: string;
  private saved = false;

  constructor(fb: FormBuilder,
              private router: Router,
              private operationManagementService: OperationManagementService,
              private operationDataService: OperationDataService,
              private testingCardsLotService: TestingCardsLotService,
              private translateService: TranslateService,
              private printerTemplateService: PrinterTemplateService,
              private snackBar: MatSnackBar,
              private readOnlyService: ReadOnlyService,
              private route: ActivatedRoute) {
    this.form = fb.group({
      id: 0,
      lotName: [null, [
        Validators.required,
        Validators.maxLength(20),
        validateRegex(REGEX_ALPHA_NUMERICAL_WITH_ACCENT, "onlyAlphaNumerical")
      ]],
      cardType: [this.forConfiguration ? {value: 'TEST_SITE_TESTING_CARD', disabled: true} : null,
        [Validators.required,]],
      count: [null, [Validators.required, Validators.min(1), Validators.max(1000)]],
      signature: [null, [Validators.required,]],
      firstName: [null, [Validators.required, Validators.maxLength(30)]],
      lastName: [null, [Validators.required, Validators.maxLength(30)]],
      birthday: [null, [Validators.required,]],
      address1: [null, [Validators.maxLength(200)]],
      address2: [null, [Validators.maxLength(200)]],
      street: [null, [Validators.required, Validators.maxLength(100)]],
      postalCode: [null, [Validators.required,
        Validators.maxLength(6),
        validateRegex(/^[0-9]*$/, "onlyNumerical")]],
      city: [null, [Validators.required, Validators.maxLength(50)]],
      country: [null, [Validators.required, Validators.maxLength(50)]],
      language: [null, [Validators.required,]],
      doi: [[], [Validators.required]],
      shouldPrint: [{value: false, disabled: this.forConfiguration}]
    });
  }

  get cardTypePossibleValues() {
    if (this.forConfiguration) {
      return ['TEST_SITE_TESTING_CARD'];
    }
    return ['PRODUCTION_TESTING_CARD', 'CONTROLLER_TESTING_CARD', 'PRINTER_TESTING_CARD'];
  }

  get signaturePossibleValues() {
    return ['M', 'Ms',];
  }

  get testingCardLanguagePossibleValues() {
    return ['FR', 'DE',];
  }

  get doiPossibleValue() {
    return this.allDoiForOperation;
  }

  get forConfiguration() {
    return !!this.route.snapshot.data.forConfiguration;
  }

  get printerName() {
    if (this.form.getRawValue().cardType == 'PRINTER_TESTING_CARD') {
      return this.translateService.instant('testing-cards-lot.form.values.all-printers');
    }

    if (this.form.get("shouldPrint").value) {
      return this.printerForTest ? this.printerForTest :
        this.translateService.instant('testing-cards-lot.form.values.printer-config-not-defined');
    }
    return this.translateService.instant('testing-cards-lot.form.values.technical-printer');
  }

  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.form, this.saved);
  }

  updateShouldPrint(forceChangeValue = false) {
    let currentType = this.form.getRawValue().cardType;
    let control = this.form.get("shouldPrint");
    if (this.readOnly === true) {
      control.disable();
      control.setValue(this.form.getRawValue().shouldPrint);
    } else if (currentType === 'TEST_SITE_TESTING_CARD') {
      control.disable();
      control.setValue(false);
    } else if (currentType === 'CONTROLLER_TESTING_CARD' || currentType === 'PRINTER_TESTING_CARD') {
      control.disable();
      control.setValue(true);
    } else if (currentType === "PRODUCTION_TESTING_CARD") {
      control.enable();
      if (forceChangeValue) {
        control.setValue(false);
      }
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe())
  }

  ngOnInit() {
    this.route.data.subscribe(data => {
        if (data.voterTestingCardsLot) {
          data.voterTestingCardsLot.birthday = moment(data.voterTestingCardsLot.birthday);
          this.form.reset(data.voterTestingCardsLot);
          this.mode = "edit";
          this.updateShouldPrint();
        }
      }
    );

    this.subscriptions.push(
      this.readOnlyService.isTestingCardInReadOnly(this.forConfiguration)
        .subscribe(ro => {
          this.readOnly = ro;
          this.updateDisableFormFields();
        })
    );

    this.operationManagementService.operation.pipe(
      take(1),
      zip(this.printerTemplateService.getAllTemplates())
    ).subscribe(value => {
      let operation = value[0];
      let templates = value[1];
      this.operationId = operation.id;
      this.operationDataService.getAllRelatedDois(this.operationId).subscribe(data => this.allDoiForOperation = data);
      let selectedTemplate = templates.find(t => t.templateName === operation.printerTemplate);
      if (selectedTemplate) {
        this.printerForTest = selectedTemplate.printerIdForPrintableTestingCard;
      }
    });
  }


  save(): void {
    Object.keys(this.form.controls).forEach(k => this.form.controls[k].markAsTouched());
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      let rawValue = this.form.getRawValue();
      rawValue.birthday = rawValue.birthday.toDate();
      if (this.mode === "create") {
        this.testingCardsLotService
          .create(this.operationId, Object.assign(rawValue, {id: 0}))
          .subscribe(() => this.afterUpdateOrSave('testing-cards-lot.dialogs.save-success'));
      } else {
        this.testingCardsLotService
          .update(rawValue)
          .subscribe(() => this.afterUpdateOrSave('testing-cards-lot.dialogs.update-success'));
      }
    } else {
      focusOnError(this.form)
    }
  }

  private afterUpdateOrSave(message: string) {
    this.snackBar.open(this.translateService.instant(message), '', {duration: 5000});
    this.saved = true;
    this.router.navigate(['..'], {relativeTo: this.route});
    this.operationManagementService.shouldUpdateStatus(false);

  }

  private updateDisableFormFields() {
    enableFormComponents(this.form.controls, !this.readOnly);
    enableFormComponent(this.form.controls.cardType, !this.readOnly && !this.forConfiguration);
    this.updateShouldPrint();
  }
}
