/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export class TestingCardsLot {
  constructor(public id: number, public lotName: string, public cardType: string, public count: number,
              public signature: string, public firstName: string, public lastName: string, public birthday: Date,
              public address1: string, public address2: string, public street: string, public postalCode: string,
              public city: string, public country: string, public language: string, public shouldPrint: boolean,
              public doi: string[]) {
  }
}
