/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpParameters } from '../../../core/service/http/http.parameters.service';
import { Subject } from 'rxjs/Subject';
import {
  CREATED, DEPLOYMENT_REQUESTED, IN_VALIDATION, INVALIDATION_REJECTED, INVALIDATION_REQUESTED, OperationStatus,
  VALIDATED
} from '../../model/operation-status';
import { tap } from 'rxjs/operators';
import { TestingCardsLot } from '../model/testing-cards-lot';

@Injectable()
export class TestingCardsLotService {
  gotUpdated = new Subject<any>();
  private serviceUrl: string;

  constructor(private http: HttpClient, private httpParams: HttpParameters) {
    this.serviceUrl = this.httpParams.apiBaseURL + '/voter-testing-cards-lot';
  }

  create(operationId: number, testingCardsLot: TestingCardsLot): Observable<TestingCardsLot> {
    return this.http.post<TestingCardsLot>(this.serviceUrl, testingCardsLot, {
      params: new HttpParams()
        .append("operationId", String(operationId))
    })
      .pipe(tap(() => this.gotUpdated.next(true)));
  }

  update(testingCardsLot: TestingCardsLot): Observable<TestingCardsLot> {
    return this.http.put<TestingCardsLot>(this.serviceUrl, testingCardsLot)
      .pipe(tap(() => this.gotUpdated.next(true)));
  }

  remove(id: number): Observable<boolean> {
    return this.http.delete<boolean>(this.serviceUrl + "/" + id)
      .pipe(tap(() => this.gotUpdated.next(true)));
  }


  find(id: number): Observable<TestingCardsLot> {
    return this.http.get<TestingCardsLot>(this.serviceUrl + "/" + id);
  }

  findAll(operationId: number, forConfiguration: boolean): Observable<TestingCardsLot[]> {
    return this.http.get<TestingCardsLot[]>(this.serviceUrl,
      {
        params: new HttpParams()
          .append("operationId", String(operationId))
          .append("forConfiguration", String(forConfiguration))
      }
    );
  }

  getTestingMaterialAvailability(status: OperationStatus, inConfiguration: boolean) {
    if (inConfiguration) {
      return [IN_VALIDATION, VALIDATED, DEPLOYMENT_REQUESTED].indexOf(status.configurationStatus.state) >= 0;
    }

    return status.votingMaterialStatus &&
           [CREATED, INVALIDATION_REQUESTED, INVALIDATION_REJECTED].indexOf(status.votingMaterialStatus.state) >= 0;

  }
}

