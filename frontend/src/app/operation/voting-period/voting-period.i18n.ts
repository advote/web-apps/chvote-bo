/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { i18n_voting_period_notification } from './voting-period-notification/voting-period-notification.i18n';
import { i18n_selected_electoral_authority_key } from './selected-electoral-authority-key/selected-electoral-authority-key.i18n';
import { i18n_voting_site_period } from './voting-site-period/voting-site-period.i18n';

export const i18n_voting_period = {
  fr: {
    "voting-site-period": i18n_voting_site_period.fr,
    "notification": i18n_voting_period_notification.fr,
    "selected-electoral-authority-key": i18n_selected_electoral_authority_key.fr
  },
  de: {
    "voting-site-period": i18n_voting_site_period.de,
    "notification": i18n_voting_period_notification.de,
    "selected-electoral-authority-key": i18n_selected_electoral_authority_key.de
  }
};
