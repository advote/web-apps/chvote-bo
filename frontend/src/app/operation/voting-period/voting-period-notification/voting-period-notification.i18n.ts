/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_voting_period_notification = {
  fr: {
    "info": {
      "consistency": {
        "in-progress": "Le calcul de la cohérence du matériel de vote est en cours",
        "errors": "Erreurs de cohérence détectées"
      }
    },
    "upload-voting-period": {
      "message": "Vous pouvez désormais envoyer la configuration de la période de vote",
      "button": "Finaliser la préparation de la période de vote",
      "success": "La configuration de la période de vote a été envoyée avec succès."
    },
    "create-voting-period-request": {
      "message": "Vous pouvez désormais demander l'initialisation de la période de vote",
      "title": "Demander l'initialisation de la période de vote",
      "pact-link": "Ouvrir l'application PACT"
    },
    "validate-voting-period-request": {
      "message": "Vous pouvez désormais valider ou refuser l'initialisation de la période de vote",
      "title": "Valider / refuser l'initialisation de la période de vote",
      "pact-link": "Ouvrir l'application PACT"
    },
    "close-voting-period": {
      "message": "Vous pouvez demander une fermeture anticipée du site de vote",
      "button": "Demander la fermeture anticipée",
      "success": "La demande de fermeture anticipée a été prise en compte"
    }
  },
  de: {
    "info": {
      "consistency": {
        "in-progress": "DE - Le calcul de la cohérence du matériel de vote est en cours",
        "errors": "DE - Erreurs de cohérence détectées"
      }
    },
    "upload-voting-period": {
      "message": "DE - Vous pouvez désormais envoyer la configuration de la période de vote",
      "button": "DE - Finaliser la préparation de la période de vote",
      "success": "DE - La configuration de la période de vote a été envoyée avec succès."
    },
    "create-voting-period-request": {
      "message": "DE - Vous pouvez désormais demander l'initialisation de la période de vote",
      "title": "DE - Demander l'initialisation de la période de vote",
      "pact-link": "DE - Ouvrir l'application PACT"
    },
    "validate-voting-period-request": {
      "message": "DE - Vous pouvez désormais valider ou refuser l'initialisation de la période de vote",
      "title": "DE - Valider / refuser l'initialisation de la période de vote",
      "pact-link": "DE - Ouvrir l'application PACT"
    },
    "close-voting-period": {
      "message": "DE - Vous pouvez demander une fermeture anticipée du site de vote",
      "button": "DE - Demander la fermeture anticipée",
      "success": "DE - La demande de fermeture anticipée a été prise en compte"
    }
  }
};
