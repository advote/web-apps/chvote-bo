/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input } from '@angular/core';
import { Operation } from '../../model/operation';
import {
  AVAILABLE_FOR_INITIALIZATION, COMPLETE, DEPLOYMENT_TARGET_SIMULATION, INITIALIZATION_FAILED, INITIALIZATION_REQUESTED,
  INITIALIZED, NOT_REQUESTED, OperationStatus
} from '../../model/operation-status';
import { OperationManagementService } from '../../service/operation-managment.service';
import { MatSnackBar } from '@angular/material';
import { UploadService } from '../../../core/service/upload/upload.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { OperationService } from '../../service/operation.service';
import { find } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'voting-period-notification',
  templateUrl: './voting-period-notification.component.html',
  styleUrls: ['./voting-period-notification.component.scss']
})
export class VotingPeriodNotificationComponent {

  @Input() operation: Operation;
  @Input() operationStatus: OperationStatus;


  isCloseVotingPeriodInProgress: boolean;

  constructor(private router: Router,
              private snackBar: MatSnackBar,
              private translateService: TranslateService,
              private operationManagementService: OperationManagementService,
              private authorizationService: AuthorizationService,
              private uploadService: UploadService,
              private operationService: OperationService) {
  }

  get userBelongsToOperationManagementEntity() {
    return this.operation.managementEntity == this.authorizationService.userSnapshot.managementEntity;
  }


  get isVotingPeriodClosable() {
    return this.operationStatus.votingPeriodStatus.state == INITIALIZED &&
           this.operationStatus.deploymentTarget == DEPLOYMENT_TARGET_SIMULATION;
  }

  get isDeploymentInfoDisplayed() {
    return this.isUploadButtonShown ||
           this.isRequestVotingPeriodCreationShown ||
           this.isRequestVotingPeriodValidationShown;
  }

  get isUploadButtonShown() {
    return (this.operationStatus.votingPeriodStatus.state == COMPLETE ||
            this.operationStatus.votingPeriodStatus.state == INITIALIZATION_FAILED) &&
           this.userBelongsToOperationManagementEntity &&
           this.authorizationService.hasAtLeastOneRole(['DEPLOY_VOTING_PERIOD']);
  }

  get pactLink() {
    return this.operationStatus.votingPeriodStatus.pactUrl;
  }

  get isRequestVotingPeriodCreationShown() {
    return this.userBelongsToOperationManagementEntity &&
           this.operationStatus.votingPeriodStatus.state == AVAILABLE_FOR_INITIALIZATION;
  }

  get isRequestVotingPeriodValidationShown() {
    return this.operationStatus.votingPeriodStatus.state == INITIALIZATION_REQUESTED &&
           this.userBelongsToOperationManagementEntity;
  }

  uploadVotingPeriodConfiguration() {
    this.uploadService.exportOperationVotingPeriodConfiguration(this.operation.id).subscribe(
      () => {
        this.snackBar.open(
          this.translateService.instant('voting-period.notification.upload-voting-period.success'), '', {
            duration: 5000
          });
        this.operationManagementService.shouldUpdateStatus();
      }
    );
  }

  closeVotingPeriod() {
    this.operationService.closeVotingPeriod(this.operation.id).subscribe(
      () => {
        this.operationManagementService.shouldUpdateStatus(false);
        this.isCloseVotingPeriodInProgress = true;
        this.operationManagementService.status.pipe(
          find(s => s.tallyArchiveStatus.state !== NOT_REQUESTED)
        ).subscribe(() => {
            this.isCloseVotingPeriodInProgress = false;
            this.router.navigate(["operations", this.operation.id, "tally"]);
            this.snackBar.open(
              this.translateService.instant('voting-period.notification.close-voting-period.success'), '', {
                duration: 5000
              });
          }
        )
      }
    );

  }
}
