/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OperationManagementService } from '../service/operation-managment.service';
import { Operation } from '../model/operation';
import { Subscription } from 'rxjs/Subscription';
import { OperationStatus } from '../model/operation-status';
import { ConfirmBeforeQuit } from '../../core/confirm-before-quit';
import { ReadOnlyService } from '../service/read-only.service';
import { ElectoralAuthorityKey } from '../../admin/electoral-authority-key/electoral-authority-key';
import { VotingSitePeriod } from './voting-site-period/voting-site-period';
import { VotingSitePeriodService } from './voting-site-period/voting-site-period.service';
import { SelectedElectoralAuthorityKeyService } from './selected-electoral-authority-key/selected-electoral-authority-key.service';

@Component({
  templateUrl: './voting-period.component.html'
})
export class VotingPeriodComponent implements OnInit, OnDestroy, ConfirmBeforeQuit {
  mode: "view";
  subscriptions: Subscription[] = [];

  operation: Operation;
  electoralAuthorityKey: ElectoralAuthorityKey;
  votingSitePeriod: VotingSitePeriod;
  votingSitePeriodCompleted = false;
  selectedElectoralAuthorityKeyCompleted = false;
  @ViewChild("formHolder") formHolder: ConfirmBeforeQuit;
  private operationStatus: OperationStatus;

  constructor(private route: ActivatedRoute,
              private readOnlyService: ReadOnlyService,
              private votingSitePeriodService: VotingSitePeriodService,
              private selectedElectoralAuthorityKeyService: SelectedElectoralAuthorityKeyService,
              private operationManagementService: OperationManagementService) {
  }

  get selectedElectoralAuthorityKeyInReadonly() {
    return this.readOnlyService.isSelectedElectoralAuthorityKeyInReadonly();
  }

  get votingSitePeriodInReadonly() {
    return this.readOnlyService.isVotingSitePeriodInReadonly();
  }

  get ready() {
    return this.operation && this.operationStatus;
  }

  get baseRouterLink() {
    return `/operations/${this.operation.id}/voting-period/`;
  }

  isFormModified(): boolean {
    return !this.formHolder || this.formHolder.isFormModified();
  }

  ngOnInit() {
    this.subscriptions.push(
      this.operationManagementService.operation.subscribe(operation => {
        this.operation = operation;
        this.votingSitePeriodService.findForOperation(operation.id)
          .subscribe(votingSitePeriod => this.votingSitePeriod = votingSitePeriod);
        this.selectedElectoralAuthorityKeyService.findForOperation(operation.id)
          .subscribe(electoralAuthorityKey => this.electoralAuthorityKey = electoralAuthorityKey);
      }),
      this.operationManagementService.status.subscribe(status => {
        this.operationStatus = status;
        this.votingSitePeriodCompleted = status.votingPeriodStatus.completedSections['period-configuration'];
        this.selectedElectoralAuthorityKeyCompleted =
          status.votingPeriodStatus.completedSections['electoral-authority-key-configuration'];
      }),
      this.route.data.subscribe((data) => this.mode = data.mode || "view")
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }


}
