/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ElectoralAuthorityKey } from '../../../admin/electoral-authority-key/electoral-authority-key';
import { HttpClient } from '@angular/common/http';
import { HttpParameters } from '../../../core/service/http/http.parameters.service';

@Injectable()
export class SelectedElectoralAuthorityKeyService {
  private serviceUrl;

  constructor(private http: HttpClient, private params: HttpParameters) {
    this.serviceUrl = (operation) => `${this.params.apiBaseURL}/operation/${operation}/electoral-authority-key`;
  }


  public saveOrUpdate(operationId: number, keyId: number): Observable<ElectoralAuthorityKey> {
    return this.http.post<ElectoralAuthorityKey>(this.serviceUrl(operationId), {keyId});
  }

  public findForOperation(operationId: number): Observable<ElectoralAuthorityKey> {
    return this.http.get<ElectoralAuthorityKey>(this.serviceUrl(operationId));
  }

}
