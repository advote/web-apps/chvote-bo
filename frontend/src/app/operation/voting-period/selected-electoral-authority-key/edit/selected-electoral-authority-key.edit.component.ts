/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnChanges, OnInit, Output } from '@angular/core';
import { ElectoralAuthorityKey } from '../../../../admin/electoral-authority-key/electoral-authority-key';
import { Subject } from 'rxjs/Subject';
import { SelectedElectoralAuthorityKeyService } from '../selected-electoral-authority-key.service';
import { ConfirmBeforeQuit } from '../../../../core/confirm-before-quit';
import { ElectoralAuthorityKeyService } from '../../../../admin/electoral-authority-key/electoral-authority-key.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { enableFormComponent, isFormModifiedAndNotSaved } from '../../../../core/util/form-utils';
import { Operation } from '../../../model/operation';

@Component({
  selector: 'selected-electoral-authority-key-edit',
  templateUrl: './selected-electoral-authority-key.edit.component.html',
  styleUrls: ['./selected-electoral-authority-key.edit.component.scss']
})
export class SelectedElectoralAuthorityKeyEditComponent implements OnInit, ConfirmBeforeQuit, OnChanges {

  form: FormGroup;
  @Input()
  readOnly: boolean;
  @Input()
  operation: Operation;
  @Input()
  key: ElectoralAuthorityKey;
  @Output()
  keyChange = new Subject<ElectoralAuthorityKey>();
  possibleKeys: ElectoralAuthorityKey[];
  private saved = false;

  constructor(private service: SelectedElectoralAuthorityKeyService,
              private keyService: ElectoralAuthorityKeyService,
              formBuilder: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private operationManagementService: OperationManagementService) {
    this.form = formBuilder.group({
      keyId: [null, Validators.required]
    });
  }

  get ready() {
    return !!this.possibleKeys;
  }

  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.form, this.saved);
  }

  ngOnInit() {
    this.keyService.findAll().subscribe(keys => this.possibleKeys = keys);
  }

  ngOnChanges(): void {
    this.form.reset({keyId: this.key ? this.key.id : null});
    this.updateForReadonly();
  }

  save() {
    this.form.controls.keyId.markAsTouched();
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      let keyId = this.form.getRawValue().keyId;
      this.service.saveOrUpdate(this.operation.id, keyId)
        .subscribe(() => {
          this.keyChange.next(this.keyById(keyId));
          this.operationManagementService.shouldUpdateStatus();
          this.saved = true;
          this.router.navigate(['..'], {relativeTo: this.route});
        });
    }
  }

  public keyById(keyId: any) {
    return this.possibleKeys.filter(key => key.id == keyId)[0];
  }

  private updateForReadonly() {
    enableFormComponent(this.form.controls.keyId, !this.readOnly);
  }
}
