/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_voting_site_period = {
  fr: {
    "card": {
      "title": "Ouverture du site de vote",
      "subtitle": "",
      "date-open": "Ouverture du site de vote",
      "date-close": "Fermeture du site de vote",
      "grace-period": "Délai de grâce (en minutes)",
    },
    "sidebar": {
      "title": "Ouverture Site Vote"
    },
    "edit": {
      "title": "Ouverture du site de vote",
      "subtitle": "",
      "form": {
        "fields": {
          "date-open": {
            "placeholder": "Date d'ouverture du site de vote"
          },
          "date-close": {
            "placeholder": "Date de fermeture du site de vote"
          },

          "grace-period": {
            "placeholder": "Délai de grâce (en minutes)"
          }
        }

      }
    }
  },
  de: {
    "card": {
      "title": "DE - Ouverture du site de vote",
      "subtitle": "DE - ",
      "date-open": "DE - Ouverture du site de vote",
      "date-close": "DE - Fermeture du site de vote",
      "grace-period": "DE - Délai de grâce (en minutes)",
    },
    "sidebar": {
      "title": "DE - Ouverture Site Vote"
    },
    "edit": {
      "title": "DE - Ouverture du site de vote",
      "subtitle": "DE - ",
      "form": {
        "fields": {
          "date-open": {
            "placeholder": "DE - Date d'ouverture du site de vote"
          },
          "date-close": {
            "placeholder": "DE - Date de fermeture du site de vote"
          },

          "grace-period": {
            "placeholder": "DE - Délai de grâce (en minutes)"
          }
        }

      }
    }
  }
};
