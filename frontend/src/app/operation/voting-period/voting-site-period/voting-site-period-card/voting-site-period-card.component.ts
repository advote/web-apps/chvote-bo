/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit } from '@angular/core';
import { VotingSitePeriod } from '../voting-site-period';

@Component({
  selector: 'voting-site-period-card',
  templateUrl: './voting-site-period-card.component.html',
  styleUrls: ['./voting-site-period-card.component.scss']
})
export class VotingSitePeriodCardComponent implements OnInit {


  @Input()
  period: VotingSitePeriod;

  @Input()
  readOnly: boolean;

  @Input()
  completed: boolean;


  constructor() {
  }

  ngOnInit() {
  }

}
