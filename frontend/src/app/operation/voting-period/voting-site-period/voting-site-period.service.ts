/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParameters } from '../../../core/service/http/http.parameters.service';
import { Observable } from 'rxjs/Observable';
import { VotingSitePeriod } from './voting-site-period';

@Injectable()
export class VotingSitePeriodService {
  private serviceUrl;

  constructor(private http: HttpClient, private params: HttpParameters) {
    this.serviceUrl = (operation) => `${this.params.apiBaseURL}/operation/${operation}/voting-site-period`;
  }

  public saveOrUpdate(operationId: number, period: VotingSitePeriod): Observable<VotingSitePeriod> {
    return this.http.post<VotingSitePeriod>(this.serviceUrl(operationId), period);
  }

  public findForOperation(operationId: number): Observable<VotingSitePeriod> {
    return this.http.get<VotingSitePeriod>(this.serviceUrl(operationId));
  }

}
