/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnInit } from '@angular/core';
import { OperationManagementService } from '../service/operation-managment.service';
import { CREATED, CREATION_FAILED, CREATION_IN_PROGRESS, NOT_ENOUGH_VOTES_CAST } from '../model/operation-status';
import { HttpParameters } from '../../core/service/http/http.parameters.service';
import { OperationService } from '../service/operation.service';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'tally',
  templateUrl: './tally.component.html',
  styleUrls: ['./tally.component.scss']
})
export class TallyComponent implements OnInit {

  constructor(private operationManagementService: OperationManagementService,
              private httpParams: HttpParameters,
              private snackBar: MatSnackBar,
              private operationService: OperationService,
              private translateService: TranslateService) {
  }

  get isTallyArchiveInProgress() {
    return this.getState() === CREATION_IN_PROGRESS;
  }

  get isTallyArchiveFailed() {
    return this.getState() === CREATION_FAILED;
  }

  get isTallyArchiveCreated() {
    return this.getState() === CREATED;
  }

  get isTallyNotEnoughVotesForCreatingArchive() {
    return this.getState() === NOT_ENOUGH_VOTES_CAST;
  }

  get isRetryInProgress() {
    //FIXME to be implemented
    return false;
  }

  get tallyArchiveDownloadToken() {
    return this.operationManagementService.status.getValue().tallyArchiveStatus.tallyArchiveDownloadToken;
  }

  ngOnInit() {
  }

  public getDownloadLink() {
    return `${this.httpParams.apiBaseURL}/download-with-token/${this.tallyArchiveDownloadToken}`;
  }

  retry() {
    this.operationService.closeVotingPeriod(this.operationManagementService.operation.getValue().id).subscribe(
      () => {
        this.operationManagementService.shouldUpdateStatus(false);
        this.snackBar.open(
          this.translateService.instant('tally.archive.notification.retry.sent'), '', {
            duration: 5000
          });
      }
    );

  }

  private getState() {
    return this.operationManagementService.status.getValue().tallyArchiveStatus.state;
  }


}
