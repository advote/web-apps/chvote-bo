/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_tally = {
  fr: {
    "archive": {
      "status": {
        "creation_in_progress": "L'archive de dépouillement est en cours de création.",
        "creation_failed": "La création de l'archive de dépouillement a échoué",
        "created": "L'archive de dépouillement est disponible",
        "not_enough_votes": "Le nombre de votes reçus est insuffisant pour effectuer le brassage. L'archive de dépouillement n'a pas pu être générée."
      },

      "access-denied": "Vous ne disposez pas des autorisations nécessaires afin d'accéder à l'archive de dépouillement",
      "actions": {
        "download": "Télécharger l'archive de dépouillement",
        "retry": "Relancer la géneration de l'archive de dépouillement"
      },
      "notification": {
        "retry": {
          "sent": "La demande de création de l'archive de dépouillement a été renvoyée"
        }
      },
      "errors": {
        "generation-already-requested": "La demande de géneration de l'archive de dépouillement a déjà été soumise",
        "generation-already-done": "L'archive de dépouillement est déja disponible",
        // in Practice impossible cases
        "operation-is-not-in-simulation": "L'operation n'est pas en simulation",
        "voting-period-not-initialized": "La période de vote n'a pas été initialisée.",
      }
    }
  },
  de: {
    "archive": {
      "status": {
        "creation_in_progress": "DE - L'archive de dépouillement est en cours de création.",
        "creation_failed": "DE - La création de l'archive de dépouillement a échoué",
        "created": "DE - L'archive de dépouillement est disponible",
        "not_enough_votes": "DE - Le nombre de votes reçus est insuffisant pour effectuer le brassage. L'archive de dépouillement n'a pas pu être générée."
      },

      "access-denied": "DE - Vous ne disposez pas des autorisations nécessaires afin d'accéder à l'archive de dépouillement",
      "actions": {
        "download": "DE - Télécharger l'archive de dépouillement",
        "retry": "DE - Relancer la géneration de l'archive de dépouillement"
      },
      "notification": {
        "retry": {
          "sent": "DE - La demande de création de l'archive de dépouillement a été renvoyée"
        }
      },
      "errors": {
        "generation-already-requested": "DE - La demande de géneration de l'archive de dépouillement a déjà été soumise",
        "generation-already-done": "DE - L'archive de dépouillement est déja disponible",
        // in Practice impossible cases
        "operation-is-not-in-simulation": "DE - L'operation n'est pas en simulation",
        "voting-period-not-initialized": "DE - La période de vote n'a pas été initialisée.",
      }
    }
  }
};
