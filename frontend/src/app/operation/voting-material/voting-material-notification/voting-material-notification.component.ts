/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input } from '@angular/core';
import { Operation } from '../../model/operation';
import {
  AVAILABLE_FOR_CREATION, COMPLETE, CREATED, CREATION_FAILED, CREATION_REQUESTED, INVALIDATED, INVALIDATION_REJECTED,
  INVALIDATION_REQUESTED, OperationStatus
} from '../../model/operation-status';
import { OperationManagementService } from '../../service/operation-managment.service';
import { MatSnackBar } from '@angular/material';
import { UploadService } from '../../../core/service/upload/upload.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { TdDialogService } from '@covalent/core';
import { OperationService } from '../../service/operation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'voting-material-notification',
  templateUrl: './voting-material-notification.component.html',
  styleUrls: ['./voting-material-notification.component.scss']
})
export class VotingMaterialNotificationComponent {

  @Input() operation: Operation;
  @Input() operationStatus: OperationStatus;


  constructor(private snackBar: MatSnackBar,
              private translateService: TranslateService,
              private operationManagementService: OperationManagementService,
              private router: Router,
              private authorizationService: AuthorizationService,
              private dialogService: TdDialogService,
              private operationService: OperationService,
              private uploadService: UploadService) {
  }

  get userBelongsToOperationManagementEntity() {
    return this.operation.managementEntity == this.authorizationService.userSnapshot.managementEntity;
  }


  get isDeploymentInfoDisplayed() {
    return this.isVotingMaterialCompleteAndConsistent() &&
           [COMPLETE, AVAILABLE_FOR_CREATION, CREATION_REQUESTED, CREATION_FAILED, INVALIDATED].indexOf(
             this.operationStatus.votingMaterialStatus.state) > -1;
  }

  get isValidationInfoDisplayed() {
    return this.isVotingMaterialCompleteAndConsistent &&
           [CREATED, INVALIDATION_REJECTED].indexOf(this.operationStatus.votingMaterialStatus.state) > -1;
  }

  get isInvalidationRequestedInfoDisplayed() {
    return this.isVotingMaterialCompleteAndConsistent &&
           [INVALIDATION_REQUESTED].indexOf(this.operationStatus.votingMaterialStatus.state) > -1;
  }


  get isUploadButtonShown() {
    return this.isVotingMaterialCompleteAndConsistent &&
           this.authorizationService.hasAtLeastOneRole(['DEPLOY_VOTING_MATERIAL']) &&
           [COMPLETE, CREATION_FAILED, INVALIDATED].indexOf(this.operationStatus.votingMaterialStatus.state) > -1;
  }

  get isRequestVotingMaterialCreationShown() {
    return [AVAILABLE_FOR_CREATION].indexOf(this.operationStatus.votingMaterialStatus.state) > -1;
  }

  get isValidateVotingMaterialCreationShown() {
    return [CREATION_REQUESTED].indexOf(this.operationStatus.votingMaterialStatus.state) > -1;
  }

  get inModification() {
    return ["IN_PARTIAL_MODIFICATION", "IN_FULL_MODIFICATION"]
             .indexOf(this.operationStatus.configurationStatus.modificationMode) >= 0;
  }

  get pactLink() {
    return this.operationStatus.votingMaterialStatus.pactUrl;
  }

  uploadVotingMaterialConfiguration() {
    this.uploadService.exportOperationVotingMaterialConfiguration(this.operation.id).subscribe(
      () => {
        this.snackBar.open(
          this.translateService.instant('voting-material.notification.upload-voting-material.success'), '', {
            duration: 5000
          });
        this.operationManagementService.shouldUpdateStatus();
      }
    );
  }

  validateVotingMaterial() {
    this.dialogService.openConfirm({
      message: this.translateService.instant('voting-material.notification.validate-voting-material.validate.dialog'),
      disableClose: true,
      width: "700px",
      cancelButton: this.translateService.instant('global.actions.cancel'),
      acceptButton: this.translateService.instant('global.actions.validate'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.operationService.validateVotingMaterial(this.operation.id)
          .subscribe(() => {
            this.snackBar.open(
              this.translateService.instant('voting-material.notification.validate-voting-material.validate.success'),
              '',
              {duration: 5000}
            );
            this.operationManagementService.shouldUpdateStatus();
            this.router.navigate(["operations", this.operation.id, "voting-period"]);
          });
      }
    });
  }

  private isVotingMaterialCompleteAndConsistent() {
    return this.operationStatus.votingMaterialCompleteAndConsistent;
  }

}
