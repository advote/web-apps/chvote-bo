/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_voting_material_notification = {
  fr: {
    "info": {
      "consistency": {
        "in-progress": "Le calcul de la cohérence du matériel de vote est en cours",
        "errors": "Erreurs de cohérence détectées"
      },
      "configuration-in-modification": "La configuration est en cours de modification."
    },
    "upload-voting-material": {
      "message": "Vous pouvez désormais envoyer la configuration du matériel de vote",
      "button": "Finaliser la préparation du matériel de vote",
      "success": "La configuration du matériel de vote a été envoyée avec succès."
    },
    "create-voting-material": {
      "message": "Vous pouvez désormais demander la création du matériel de vote",
      "title": "Demander la création du matériel de vote",
      "pact-link": "Ouvrir l'application PACT"
    },
    "validate-voting-material-creation": {
      "message": "Vous pouvez désormais valider ou invalider la création du matériel de vote ",
      "title": "Valider / refuser la création du matériel de vote",
      "pact-link": "Ouvrir l'application PACT"
    },

    "confirm-voting-material-invalidation": {
      "message": "Vous pouvez désormais confirmer ou refuser la demande d'invalidation du matériel de vote ",
      "title": "Valider / refuser la demande d'invalidation du matériel de vote",
      "pact-link": "Ouvrir l'application PACT"
    },

    "validate-voting-material": {
      "message": "Vous pouvez désormais valider ou refuser le matériel de vote créé",
      "invalidate": {
        "title": "Invalider le matériel de vote",
        "pact-link": "Ouvrir l'application PACT"
      },
      "validate": {
        "button": "Valider le matériel de vote",
        "dialog": "Cette opération acte que les imprimeurs ont envoyé les bulletins de vote. Il ne sera plus possible de modifier l'opération après validation.",
        "success": "Le matériel de vote a été validé avec succès"
      }

    }

  },
  de: {
    "info": {
      "consistency": {
        "in-progress": "DE - Le calcul de la cohérence du matériel de vote est en cours",
        "errors": "DE - Erreurs de cohérence détectées"
      },
      "configuration-in-modification": "DE - La configuration est en cours de modification."
    },
    "upload-voting-material": {
      "message": "DE - Vous pouvez désormais envoyer la configuration du matériel de vote",
      "button": "DE - Finaliser la préparation du matériel de vote",
      "success": "DE - La configuration du matériel de vote a été envoyée avec succès."
    },
    "create-voting-material": {
      "message": "DE - Vous pouvez désormais demander la création du matériel de vote",
      "title": "DE - Demander la création du matériel de vote",
      "pact-link": "DE - Ouvrir l'application PACT"
    },
    "validate-voting-material-creation": {
      "message": "DE - Vous pouvez désormais valider ou invalider la création du matériel de vote ",
      "title": "DE - Valider / refuser la création du matériel de vote",
      "pact-link": "DE - Ouvrir l'application PACT"
    },

    "confirm-voting-material-invalidation": {
      "message": "DE - Vous pouvez désormais confirmer ou refuser la demande d'invalidation du matériel de vote ",
      "title": "DE - Valider / refuser la demande d'invalidation du matériel de vote",
      "pact-link": "DE - Ouvrir l'application PACT"
    },

    "validate-voting-material": {
      "message": "DE - Vous pouvez désormais valider ou refuser le matériel de vote créé",
      "invalidate": {
        "title": "DE - Invalider le matériel de vote",
        "pact-link": "DE - Ouvrir l'application PACT"
      },
      "validate": {
        "button": "DE - Valider le matériel de vote",
        "dialog": "DE - Cette opération acte que les imprimeurs ont envoyé les bulletins de vote. Il ne sera plus possible de modifier l'opération après validation.",
        "success": "DE - Le matériel de vote a été validé avec succès"
      }

    }
  }
};
