/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_voting_material_register = {
  fr: {
    "card": {
      "title": "Registre électoral",
      "subtitle": "Informations concernant les registres électoraux",
      "register-file-count": "Nombre de fichiers de registre",
      "voter-count": "Nombre d'électeurs"
    },
    "title": "Registre électoral",
    "subtitle": "Liste des registres électoraux importés",
    "delete-dialog": {
      "message": "Souhaitez-vous supprimer le registre électoral {{fileName}}\u00A0?"
    },
    "actions": {
      "import": "importer un registre électoral",
      "download-global-report": "Télécharger le rapport détaillé global"
    },
    "grid": {
      "header": {
        "file": "Fichier",
        "uploadDetails": "Import",
        "uploader": "Importé par",
        "managementEntity": "Entité de gestion",
        "emissionDetails": "Émission",
        "statistics": "Statistiques",

        "upload-date": "Date d'import",

        "emitter": "Emetteur",
        "emission-date": "Date d'émission",
        "voter-count": "Électeurs",
        "doi-count": "Domaines d'influence",
        "counting-circle-count": "Circonscriptions de dépouillement",
        "detailed-report": "Rapport détaillé",
        "delete": "Supprimer"
      }
    },
    "upload": {
      "subtitle": "Importer un registre électoral pour l'opération : {{longLabel}}",
      "actions": {
        "preview": "pré-visualiser",
        "download-voters": "télécharger la liste des électeurs"
      },
      "upload-progress": "Pré-visualisation en cours",
      "upload-valid-title": "Rapport de synthèse du fichier électeur sélectionné",
      "upload-succeed": "Le fichier de registres électoraux a été correctement importé.",
      "errors": {
        "title": "Impossible d'importer le fichier électeur {{fileName}}.",
        "messageUniqueIdAlreadyImported": "Un registre avec le même messageId a déjà été importé pour cette opération",
        "invalid-number-of-voter": "Le nombre d'électeurs défini dans l'en-tête du fichier eCH-0045 ({{param1}}) ne correspond pas au nombre d'électeurs effectivement lu ({{param0}})",
        "no-domain-influence": "Impossible d'importer le registre, aucun domaine d'influence défini pour l'opération.",
        "concurrent-import": "Un autre utilisateur a activé la fonctionnalité d'import d'un registre électoral. L'analyse de détection des doublons doit être relancée.",
        "schema-validation": {
          "line1": "Des erreurs ont été détectées lors de l'analyse du fichier.",
          "line2": "Le fichier ne respecte pas la structure XSD définie par la norme eCH-0045 / Registre électoral.",
          "csv": {
            "header": {
              "lineNumber": "Ligne",
              "columnNumber": "Colonne",
              "message": "Message"
            }
          }
        },
        "duplicate": {
          "line1": "Des électeurs présents dans le fichier sont déjà définis dans les registres électoraux avec les mêmes attributs.",
          "line2": "Ceci induirait la génération de cartes de vote identique.",
          "line3": "Vérifier la liste des électeurs concernés afin d'effectuer les corrections.",
          "csv": {
            "header": {
              "fileName": "Nom du fichier",
              "importedBy": "Importé par",
              "importDate": "Date d'import",
              "emitter": "Emetteur",
              "emissionDate": "Date d'émission",
              "identifier": "Identifiant local",
              "title": "Formule d'appel",
              "lastName": "Nom(s)",
              "firstName": "Prénom(s)",
              "address": "Adresse",
              "municipality": "Commune",
              "dateOfBirth": "Date de naissance"
            }
          }
        },
        "undefined-doi": {
          "line1": "Des électeurs présents dans le fichier n'ont pas de DOI associé à au moins un scrutin de l'opération courante.",
          "line2": "Vérifier la liste des électeurs concernés afin d'effectuer les corrections.",
          "csv": {
            "header": {
              "fileName": "Nom du fichier",
              "emitter": "Emetteur",
              "emissionDate": "Date d'émission",
              "identifier": "Identifiant local",
              "title": "Formule d'appel",
              "lastName": "Nom(s)",
              "firstName": "Prénom(s)",
              "municipality": "Commune",
              "dois": "Liste des DOI"
            }
          }
        }
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Registre électoral",
      "subtitle": "DE - Informations concernant les registres électoraux",
      "register-file-count": "DE - Nombre de fichiers de registre",
      "voter-count": "DE - Nombre d'électeurs"
    },
    "title": "DE - Registre électoral",
    "subtitle": "DE - Liste des registres électoraux importés",
    "delete-dialog": {
      "message": "DE - Souhaitez-vous supprimer le registre électoral {{fileName}}\u00A0?"
    },
    "actions": {
      "import": "DE - importer un registre électoral",
      "download-global-report": "DE - Télécharger le rapport détaillé global"
    },
    "grid": {
      "header": {
        "file": "DE - Fichier",
        "uploader": "DE - Importé par",
        "upload-date": "DE - Date d'importation",
        "emitter": "DE - Emetteur",
        "emission-date": "DE - Date d'émission",
        "voter-count": "DE - Nombre d'électeurs",
        "doi-count": "DE - Nombre de domaines d'influence",
        "counting-circle-count": "DE - Nombre de circonscriptions de dépouillement",
        "detailed-report": "DE - Rapport détaillé",
        "delete": "DE - Supprimer"
      }
    },
    "upload": {
      "subtitle": "DE - Importer un registre électoral pour l'opération : {{longLabel}}",
      "actions": {
        "preview": "DE - pré-visualiser",
        "download-voters": "DE - télécharger la liste des électeurs"
      },
      "upload-progress": "DE - Pré-visualisation en cours",
      "upload-valid-title": "DE - Rapport de synthèse du fichier électeur sélectionné",
      "upload-succeed": "DE - Le fichier de registres électoraux a été correctement importé.",
      "errors": {
        "title": "DE - Impossible d'importer le fichier électeur {{fileName}}.",
        "messageUniqueIdAlreadyImported": "DE - Un registre avec le même messageId a déjà été importé pour cette opération",
        "invalid-number-of-voter": "DE - Le nombre d'électeurs défini dans l'en-tête du fichier eCH-0045 ({{param1}}) ne correspond pas au nombre d'électeurs effectivement lu ({{param0}})",
        "no-domain-influence": "DE - Impossible d'importer le registre, aucun domaine d'influence défini pour l'opération.",
        "concurrent-import": "DE - Un autre utilisateur a activé la fonctionnalité d'import d'un registre électoral. L'analyse de détection des doublons doit être relancée.",
        "schema-validation": {
          "line1": "DE - Des erreurs ont été détectées lors de l'analyse du fichier {{fileName}}.",
          "line2": "DE - Le fichier ne respecte pas la structure XSD définie par la norme eCH-0045 / Registre électoral.",
          "csv": {
            "header": {
              "lineNumber": "DE - Ligne",
              "columnNumber": "DE - Colonne",
              "message": "DE - Message"
            }
          }
        },
        "duplicate": {
          "line1": "DE - Des électeurs présents dans le fichier sont déjà définis dans les registres électoraux avec les mêmes attributs.",
          "line2": "DE - Ceci induirait la génération de cartes de vote identique.",
          "line3": "DE - Vérifier la liste des électeurs concernés afin d'effectuer les corrections.",
          "csv": {
            "header": {
              "fileName": "DE - Nom du fichier",
              "importedBy": "DE - Importé par",
              "importDate": "DE - Date d'import",
              "emitter": "DE - Emetteur",
              "emissionDate": "DE - Date d'émission",
              "identifier": "DE - Identifiant local",
              "title": "DE - Formule d'appel",
              "lastName": "DE - Nom(s)",
              "firstName": "DE - Prénom(s)",
              "address": "DE - Adresse",
              "municipality": "DE - Commune",
              "dateOfBirth": "DE - Date de naissance"
            }
          }
        },
        "undefined-doi": {
          "line1": "DE - Des électeurs présents dans le fichier n'ont pas de DOI associé à au moins un scrutin de l'opération courante.",
          "line2": "DE - Vérifier la liste des électeurs concernés afin d'effectuer les corrections.",
          "csv": {
            "header": {
              "fileName": "DE - Nom du fichier",
              "emitter": "DE - Emetteur",
              "emissionDate": "DE - Date d'émission",
              "identifier": "DE - Identifiant local",
              "title": "DE - Formule d'appel",
              "lastName": "DE - Nom(s)",
              "firstName": "DE - Prénom(s)",
              "municipality": "DE - Commune",
              "dois": "DE - Liste des DOI"
            }
          }
        }
      }
    }
  }

}
