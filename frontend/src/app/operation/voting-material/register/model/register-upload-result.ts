/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { SchemaValidationError } from '../../../../shared/model/schema-validation-error';
import { RegisterDuplicate } from './register-duplicate';
import { RegisterUndefinedDoi } from './register-undefined-doi';


export class RegisterUploadResult {

  constructor(public duplicates: RegisterDuplicate[],
              public undefinedDois: RegisterUndefinedDoi[],
              public validationErrors: SchemaValidationError[],
              public emitter: string,
              public emissionDate: string,
              public voterCount: string,
              public doiCount: string,
              public countingCircleCount: string,
              public messageUniqueId: string,
              public detailedReport: string,
              public fileName: string) {
  }
}
