/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ImportedRegisterFile } from '../model/imported-register-file';
import { Operation } from '../../../model/operation';
import { RegisterService } from '../services/register.service';
import { ReadOnlyService } from '../../../service/read-only.service';
import { Subscription } from 'rxjs/Subscription';
import { OperationManagementService } from '../../../service/operation-managment.service';


@Component({
  selector: 'register-card',
  templateUrl: './register-card.component.html',
  styleUrls: ['./register-card.component.scss']
})
export class RegisterCardComponent implements OnInit, OnDestroy {
  registers: ImportedRegisterFile[] = [];
  @Input()
  operation: Operation;
  completed = false;
  inError = false;
  private _subscriptions: Subscription[] = [];

  constructor(private service: RegisterService,
              private readOnlyService: ReadOnlyService,
              private operationManagementService: OperationManagementService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  private _readOnly = false;

  get readOnly(): boolean {
    return this._readOnly;
  }

  set readOnly(value: boolean) {
    this._readOnly = value;
  }

  get registerFileCount() {
    return this.registers.length
  }

  get voterCount() {
    return this.registers.reduce((acc, r) => acc + r.voterCount, 0);
  }

  ngOnInit() {
    this.service.getReports(this.operation.id).subscribe(files => this.registers = files);
    this._subscriptions.push(
      this.operationManagementService.status.subscribe(status => {
        this.completed = status.votingMaterialStatus.completedSections["register"];
        this.inError = status.votingMaterialStatus.sectionsInError["register"];
      }),
      this.readOnlyService.isRegisterInReadOnly().subscribe(readOnly => this._readOnly = readOnly));
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }

  goToRegisterList() {
    this.router.navigate(["register"], {relativeTo: this.route});
  }

}
