/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { FileService } from '../../../../core/service/file-service/file-service';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute, Router } from '@angular/router';
import { Operation } from '../../../model/operation';
import { RegisterUploadResult } from '../model/register-upload-result';
import { RegisterService } from '../services/register.service';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { of } from 'rxjs/observable/of';
import { ConfirmBeforeQuit } from '../../../../core/confirm-before-quit';
import { UploadInputComponent } from '../../../../shared/upload-input/upload-input.component';


@Component({
  selector: 'register-upload',
  templateUrl: './register-upload.component.html',
  styleUrls: ['./register-upload.component.scss']
})
export class RegisterUploadComponent implements ConfirmBeforeQuit {
  result: RegisterUploadResult;

  dataSource: DataSourceFromResult;
  displayedColumns = ["fileName", "emitter", "emissionDate", "voterCount", "doiCount", "countingCircleCount",
    "detailedReport"];
  @Input()
  operation: Operation;
  @ViewChild(UploadInputComponent)
  uploadInput: UploadInputComponent;
  private validated = false;

  constructor(private route: ActivatedRoute,
              private registerService: RegisterService,
              private snackBar: MatSnackBar,
              private translateService: TranslateService,
              private operationManagementService: OperationManagementService,
              private router: Router,
              private fileService: FileService) {
  }

  get uploadUrl(): string {
    return `${this.registerService.serviceUrl}/${this.operation.id}/upload`;
  }

  isFormModified(): boolean {
    return this.uploadInput.isFormModified() || this.isValid() && !this.validated;
  }

  resetResult() {
    this.result = undefined;
  }

  setResult(result: RegisterUploadResult) {
    this.result = result;
    this.dataSource = new DataSourceFromResult(this.result);
  }

  downloadDuplicates() {
    this.download("duplicate.csv",
      this.fileService.generateCsvFromFlatObject(this.result.duplicates,
        "voting-material.register.upload.errors.duplicate.csv.header"));
  }

  downloadUndefinedDois() {
    this.download("voters-without-doi.csv",
      this.fileService.generateCsvFromFlatObject(this.result.undefinedDois,
        "voting-material.register.upload.errors.undefined-doi.csv.header"));
  }

  downloadValidationReport() {
    this.download("validationError.csv",
      this.fileService.generateCsvFromFlatObject(this.result.validationErrors,
        "voting-material.register.upload.errors.schema-validation.csv.header"));
  }

  getReport() {
    this.fileService.downloadFile("report.pdf", this.result.detailedReport, true);
  }


  download(fileName: string, content: string) {
    this.fileService.downloadFile(fileName, content);
  }

  isValid() {
    return this.result &&
           this.result.validationErrors.length == 0 &&
           this.result.duplicates.length == 0 &&
           this.result.undefinedDois.length == 0;
  }

  importRegister() {
    this.registerService.validateUpload(this.result.messageUniqueId, this.operation.id)
      .subscribe(() => {
          this.snackBar.open(
            this.translateService.instant('voting-material.register.upload.upload-succeed'), '', {
              duration: 5000
            });
          this.operationManagementService.shouldUpdateStatus();
          this.validated = true
          this.router.navigate([".."], {relativeTo: this.route});
        }
      );

  }

  cancelImport() {
    this.router.navigate([".."], {relativeTo: this.route});
  }
}


class DataSourceFromResult extends DataSource<any> {

  constructor(private data: RegisterUploadResult) {
    super();
  }

  connect(): Observable<any[]> {
    return of([this.data]);
  }

  disconnect() {
  }
}
