/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from "@angular/core";
import { FileService } from '../../../../core/service/file-service/file-service';
import { HttpParameters } from '../../../../core/service/http/http.parameters.service';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { ImportedRegisterFile } from "app/operation/voting-material/register/model/imported-register-file";

@Injectable()
export class RegisterService {
  serviceUrl;

  // wrapped in a local variable for testing purposes

  constructor(private http: HttpClient, httpParams: HttpParameters, private fileService: FileService) {
    this.serviceUrl = httpParams.apiBaseURL + `/operation/register`;
  }

  validateUpload(messageUniqueId, operationId) {
    return this.http.get(`${this.serviceUrl}/${operationId}/validate-upload`,
      {params: new HttpParams().set("messageUniqueId", messageUniqueId)});
  }

  remove(operationId, messageUniqueId) {
    return this.http.delete(`${this.serviceUrl}/${operationId}`,
      {params: new HttpParams().set("messageUniqueId", messageUniqueId)});
  }

  getReports(operationId: number) {
    return this.http.get<ImportedRegisterFile[]>(`${this.serviceUrl}/${operationId}`);
  }

  downloadReport(messageUniqueId, operationId) {
    this.downloadPdf(`${this.serviceUrl}/${operationId}/detailedReport`,
      new HttpParams().set("messageUniqueId", messageUniqueId));
  }

  downloadConsolidatedReport(operationId) {
    this.downloadPdf(`${this.serviceUrl}/${operationId}/detailedReport/consolidatedDetailedReport`, new HttpParams());
  }

  private downloadPdf(url, params: HttpParams) {
    this.http.get(url, {
      observe: 'response',
      responseType: 'blob',
      params
    }).subscribe(
      (response: HttpResponse<Blob>) => {
        let blob = new Blob([response.body], {type: 'application/pdf'});
        this.fileService.downloadBlob(HttpParameters.getFileNameFromHeader(response), blob);
      }
    );
  }


}
