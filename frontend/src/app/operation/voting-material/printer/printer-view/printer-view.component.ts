/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnChanges } from '@angular/core';
import { PrinterTemplate } from '../model/PrinterTemplate';

@Component({
  selector: 'printer-view',
  templateUrl: './printer-view.component.html',
  styleUrls: ['./printer-view.component.scss']
})
export class PrinterViewComponent implements OnChanges {
  municipalitiesByPrinter;
  printerForSwissAbroad;
  printerForTestingCard;

  @Input()
  printerTemplate: PrinterTemplate;

  constructor() {
  }

  ngOnChanges() {
    this.setMunicipalitiesByPrinter();
    this.setPrinterForSwissAbroad();
    this.setPrinterForTestingCard();
  }


  setMunicipalitiesByPrinter() {


    let allPrinterIds = Array.from(
      new Set(this.printerTemplate.printerMunicipalityMappings.map(mapping => mapping.printerId)));
    this.municipalitiesByPrinter = allPrinterIds.map(printerId => ({
      printer: this.getPrinter(printerId).name,
      municipalities: this.printerTemplate.printerMunicipalityMappings
        .filter(mapping => mapping.printerId == printerId)
        .map(mapping => mapping.municipalityName).sort()
    }));
  }


  setPrinterForSwissAbroad() {
    this.printerForSwissAbroad = this.getPrinter(this.printerTemplate.printerSwissAbroadMapping.printerId).name;
  }

  setPrinterForTestingCard() {
    this.printerForTestingCard = this.getPrinter(this.printerTemplate.printerIdForPrintableTestingCard).name;
  }


  private getPrinter(printerId: string) {
    return this.printerTemplate.printerConfigurations.filter(p => p.id == printerId)[0];
  }
}
