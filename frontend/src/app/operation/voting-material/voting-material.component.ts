/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OperationManagementService } from '../service/operation-managment.service';
import { Operation } from '../model/operation';
import { Subscription } from 'rxjs/Subscription';
import { OperationStatus } from '../model/operation-status';
import { ConfirmBeforeQuit } from '../../core/confirm-before-quit';
import { ReadOnlyService } from '../service/read-only.service';
import { Observable } from 'rxjs/Observable';

@Component({
  templateUrl: './voting-material.component.html'
})
export class VotingMaterialComponent implements OnInit, OnDestroy, ConfirmBeforeQuit {

  subscriptions: Subscription[] = [];
  operation: Operation;

  mode: "view";
  registerInReadOnly: Observable<boolean>;
  cardTitleInReadOnly: Observable<boolean>;
  printerTemplateInReadOnly: Observable<boolean>;
  testingCardInReadOnly: Observable<boolean>;
  registerCompleted = false;
  votingCardTitleCompleted = false;
  printerConfigCompleted = false;
  registerInError = false;
  votingCardTitleInError = false;
  printerConfigInError = false;
  @ViewChild("formHolder") formHolder: ConfirmBeforeQuit;
  operationStatus: OperationStatus;

  constructor(private route: ActivatedRoute,
              private readOnlyService: ReadOnlyService,
              private operationManagementService: OperationManagementService) {
    this.registerInReadOnly = readOnlyService.isRegisterInReadOnly();
    this.cardTitleInReadOnly = readOnlyService.isCardTitleInReadOnly();
    this.printerTemplateInReadOnly = readOnlyService.isPrinterTemplateInReadOnly();
    this.testingCardInReadOnly = readOnlyService.isTestingCardInReadOnly(false);
  }

  get ready() {
    return this.operation && this.operationStatus;
  }

  get baseRouterLink() {
    return `/operations/${this.operation.id}/voting-material/`;
  }

  isFormModified(): boolean {
    return !this.formHolder || this.formHolder.isFormModified();
  }

  ngOnInit() {
    this.subscriptions.push(
      this.operationManagementService.operation.subscribe(operation => this.operation = operation),
      this.operationManagementService.status.subscribe(status => {
        this.operationStatus = status;

        this.registerCompleted = status.votingMaterialStatus.completedSections['register'];
        this.votingCardTitleCompleted = status.votingMaterialStatus.completedSections['voting-card-title'];
        this.printerConfigCompleted = status.votingMaterialStatus.completedSections['printer-template'];

        this.registerInError = status.votingMaterialStatus.sectionsInError['register'];
        this.votingCardTitleInError = status.votingMaterialStatus.sectionsInError['voting-card-title'];
        this.printerConfigInError = status.votingMaterialStatus.sectionsInError['printer-template'];

      }),
      this.route.data.subscribe((data) => this.mode = data.mode || "view")
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }


}
