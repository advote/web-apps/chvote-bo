/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Operation } from '../../../model/operation';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OperationService } from '../../../service/operation.service';
import { OperationManagementService } from '../../../service/operation-managment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorizationService } from '../../../../core/service/http/authorization.service';
import { ConfirmBeforeQuit } from '../../../../core/confirm-before-quit';
import { ReadOnlyService } from 'app/operation/service/read-only.service';
import { Subscription } from 'rxjs/Subscription';
import { enableFormComponent, isFormModifiedAndNotSaved } from '../../../../core/util/form-utils';

@Component({
  selector: 'card-title-edit',
  templateUrl: './card-title-edit.component.html',
  styleUrls: ['./card-title-edit.component.scss']
})
export class CardTitleEditComponent implements OnInit, OnDestroy, ConfirmBeforeQuit {
  private _subscriptions: Subscription[] = [];
  private _readOnly = false;

  @Input()
  operation: Operation;
  form: FormGroup;

  private saved = false;

  constructor(private formBuilder: FormBuilder,
              private operationService: OperationService,
              private router: Router,
              private route: ActivatedRoute,
              private authorizationService: AuthorizationService,
              private readOnlyService: ReadOnlyService,
              private operationManagementService: OperationManagementService) {
  }


  get readOnly(): boolean {
    return this._readOnly;
  }

  isFormModified(): boolean {
    return isFormModifiedAndNotSaved(this.form, this.saved);
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach(s => s.unsubscribe());
  }


  ngOnInit() {
    this.form = this.formBuilder.group({
      title: [this.operation.votingCardTitle, Validators.required]
    });

    this._subscriptions.push(
      this.readOnlyService.isCardTitleInReadOnly().subscribe(readOnly => {
        this._readOnly = readOnly;
        this.updateForReadonly();
      })
    );
  }


  private updateForReadonly() {
    if (this.form) {
      enableFormComponent(this.form.controls.title,
        !this.readOnly && this.authorizationService.hasAtLeastOneRole(['UPDATE_VOTING_CARD_TITLE']));
    }
  }

  save() {
    this.form.updateValueAndValidity();
    if (this.form.valid) {
      let newTitle = this.form.getRawValue().title;
      this.operationService.updateVotingCardTitle(this.operation, newTitle)
        .subscribe(() => {
          this.operation.votingCardTitle = newTitle;
          this.operationManagementService.updateOperation(this.operation);
          this.saved = true;
          this.router.navigate(['..'], {relativeTo: this.route});
        });
    }
  }

}
