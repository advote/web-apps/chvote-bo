/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_voting_material_card_title = {
  fr: {
    "card": {
      "title": "Carte de vote",
      "subtitle": "Libellé à imprimer sur la carte de vote",
      "voting-card-title": "Libellé"
    },
    "edit": {
      "title": "Libellé à imprimer sur la carte de vote",
      "form": {
        "fields": {
          "title": {
            "placeholder": "Libellé imprimé sur la carte de vote",
            "error": {
              "max-length": "Le libellé de la carte de vote doit etre inférieur à 40 charactères"
            }
          }
        }
      }
    }
  },
  de: {
    "card": {
      "title": "DE - Carte de vote",
      "subtitle": "DE - Libellé à imprimer sur la carte de vote",
      "voting-card-title": "DE - Libellé"
    },
    "edit": {
      "title": "DE - Libellé à imprimer sur la carte de vote",
      "form": {
        "fields": {
          "title": {
            "placeholder": "DE - Libellé imprimé sur la carte de vote",
            "error": {
              "max-length": "DE - Le libellé de la carte de vote doit etre inférieur à 40 charactères"
            }
          }
        }
      }
    }
  },

}
