/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { i18n_voting_material_card_title } from './card-title/card-title.i18n';
import { i18n_voting_material_printer } from './printer/printer.18n';
import { i18n_voting_material_register } from './register/register.i18n';
import { i18n_voting_material_notification } from './voting-material-notification/voting-material-notification.i18n';

export const i18n_voting_material = {
  fr: {
    "card-title": i18n_voting_material_card_title.fr,
    "printer": i18n_voting_material_printer.fr,
    "register": i18n_voting_material_register.fr,
    "notification": i18n_voting_material_notification.fr
  },
  de: {
    "card-title": i18n_voting_material_card_title.de,
    "printer": i18n_voting_material_printer.de,
    "register": i18n_voting_material_register.de,
    "notification": i18n_voting_material_notification.de
  }
};
