/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'default-errors',
  templateUrl: './default-errors.component.html'
})
export class DefaultErrorsComponent {

  @Input()
  form: FormGroup;

  @Input()
  fieldName: string;

  constructor(private translateService: TranslateService) {
  }

  get shouldDisplay() {
    let control = this.form.get(this.fieldName);
    return control.invalid && (control.dirty || control.touched);
  }

  get errorLabel() {
    let field = this.form.get(this.fieldName);


    if (field.hasError("required")) {
      return this.translateService.instant("global.forms.errors.required");
    }

    let firstErrorKey = Object.keys(field.errors)[0];
    return this.translateService.instant("global.forms.errors." + firstErrorKey, field.getError(firstErrorKey))
  }

}
