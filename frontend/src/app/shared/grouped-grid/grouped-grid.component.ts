/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Input, OnInit } from '@angular/core';
import * as _ from "underscore";

@Component({
  selector: 'grouped-grid',
  templateUrl: './grouped-grid.component.html',
  styleUrls: ['./grouped-grid.component.scss']
})
export class GroupedGridComponent implements OnInit {

  @Input() data: any;
  @Input() columns: any;
  @Input() groupBy: string;
  groupDisplay = {};

  constructor() {
  }

  get groups() {
    return _.uniq(_.pluck(this.data, this.groupBy), true);
  }

  ngOnInit(): void {
    this.groups.forEach(group => {
      this.groupDisplay[group] = false;
    })
  }

  getRowsForGroup(group: string) {
    let groupBy = this.groupBy;
    return _.filter(this.data, function (data) {
      return data[groupBy] === group;
    });
  }

  toggleGroupDisplay(group: string) {
    this.groupDisplay[group] = !this.groupDisplay[group];
  }
}
