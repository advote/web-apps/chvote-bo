/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { GroupedGridComponent } from './grouped-grid.component';

describe('GroupedGridComponent', () => {
  let groupedGridComponent: GroupedGridComponent;

  beforeEach(() => {
    groupedGridComponent = new GroupedGridComponent();
    groupedGridComponent.groupBy = 'family';
    groupedGridComponent.data = [
      {family: 'Stark', name: 'Sansa'},
      {family: 'Stark', name: 'Arya'},
      {family: 'Targaryen', name: 'Daenerys'},
      {family: 'Lannister', name: 'Jaime'},
      {family: 'Lannister', name: 'Cersei'},
      {family: 'Lannister', name: 'Tyrion'},
    ]
  });

  describe('get groups()', () => {
    it('should return the unique values of the groupBy field', () => {
      expect(JSON.stringify(groupedGridComponent.groups)).toBe(JSON.stringify(['Stark', 'Targaryen', 'Lannister']));
    });
  });

  describe('getRowsForGroup(group)', () => {
    it('should return the data specifed by the given group', () => {
      expect(JSON.stringify(groupedGridComponent.getRowsForGroup('Lannister'))).toBe(JSON.stringify([
        {family: 'Lannister', name: 'Jaime'},
        {family: 'Lannister', name: 'Cersei'},
        {family: 'Lannister', name: 'Tyrion'},
      ]));
    });
  });

  describe('toggleGroupDisplay(group)', () => {
    it('should toggle the display of the given group', () => {
      // given
      groupedGridComponent.ngOnInit();
      groupedGridComponent.groupDisplay['Targaryen'] = true;

      // when
      groupedGridComponent.toggleGroupDisplay('Stark');
      groupedGridComponent.toggleGroupDisplay('Targaryen');

      // then
      expect(groupedGridComponent.groupDisplay['Stark']).toBeTruthy();
      expect(groupedGridComponent.groupDisplay['Targaryen']).toBeFalsy();
    });
  });

});
