/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'slide-details',
  templateUrl: './slide-details.component.html',
  styleUrls: ['./slide-details.component.scss']
})
export class SlideDetailsComponent implements OnInit {
  @Output()
  close = new Subject();


  _showDetails = false;

  constructor() {
  }

  showDetails() {
    this._showDetails = true;
  }

  closeDetails() {
    this._showDetails = false;
    this.close.next();
  }

  ngOnInit() {
  }
}

@Component({
  selector: 'slide-details-content',
  template: '<ng-content></ng-content>',
})
export class SlideDetailsContentComponent {
}

@Component({
  selector: 'slide-details-details',
  template: '<div style="width: 100%"><ng-content></ng-content></div>',

})
export class SlideDetailsDetailsComponent {
}
