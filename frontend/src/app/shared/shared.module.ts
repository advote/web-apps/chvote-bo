/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { ModuleWithProviders, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
import {
  DateAdapter, MAT_DATE_LOCALE, MatButtonModule, MatCardModule, MatCheckboxModule,
  MatDatepickerModule, MatDialogModule,
  MatExpansionModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatPaginatorIntl, MatPaginatorModule,
  MatProgressBarModule, MatProgressSpinnerModule, MatRippleModule, MatSelectModule, MatSlideToggleModule,
  MatSnackBarModule, MatSortModule, MatTableModule, MatTooltipModule
} from "@angular/material";
import {
  CovalentDialogsModule, CovalentExpansionPanelModule, CovalentLayoutModule, CovalentMessageModule
} from "@covalent/core";
import { TranslateModule } from "@ngx-translate/core";
import { GroupedGridComponent } from "./grouped-grid/grouped-grid.component";
import { CdkTableModule } from "@angular/cdk/table";
import { PaginatorI18NService } from '../core/service/paginate/paginator.i18n.service';
import { NgxUploaderModule } from "ngx-uploader";
import { UploadInputComponent } from './upload-input/upload-input.component';
import { CompletableCardComponent } from './completable-card/completable-card.component';
import {
  EditContentComponent, EditWithSideBarComponent, SideBarBackButtonComponent, SideBarItemComponent
} from './edit-with-side-bar/edit-with-side-bar.component';
import { DefaultErrorsComponent } from './default-errors/default-errors.component';
import { HeaderComponent } from './header/header.component';
import { ActionCardComponent } from './action-card/action-card.component';
import { CheckRolesDirective } from './check-roles.directive';
import { DatetimeAdapter, MAT_DATETIME_FORMATS, MatDatetimepickerModule } from '@mat-datetimepicker/core';
import { MomentDatetimeAdapter } from '@mat-datetimepicker/moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  SlideDetailsComponent, SlideDetailsContentComponent, SlideDetailsDetailsComponent
} from './slide-details/slide-details.component';
import { SmallProgressSpinnerComponent } from './small-progress-spinner/small-progress-spinner.component';
import { TextWithEditComponent } from './text-with-edit/text-with-edit.component';

export const DATETIME_FORMATS = {
  parse: {
    datetimeInput: "DD.MM.YYYY HH:mm",
    dateInput: "DD.MM.YYYY"
  },
  display: {
    dateInput: "DD.MM.YYYY",
    monthInput: "MMMM",
    datetimeInput: "DD.MM.YYYY HH:mm",
    timeInput: "HH:mm",
    monthYearLabel: "MMM YYYY",
    dateA11yLabel: "LL",
    monthYearA11yLabel: "MMMM YYYY",
    popupHeaderDateLabel: "ddd, MMM DD"
  }
};

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
    MatTooltipModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatCardModule,
    MatListModule,
    MatCheckboxModule,
    CdkTableModule,
    NgxUploaderModule,
    CovalentDialogsModule,
  ],
  declarations: [
    HeaderComponent,
    GroupedGridComponent,
    UploadInputComponent,
    ActionCardComponent,
    CompletableCardComponent,
    EditWithSideBarComponent,
    SideBarItemComponent,
    EditContentComponent,
    SideBarBackButtonComponent,
    DefaultErrorsComponent,
    CheckRolesDirective,
    SlideDetailsComponent,
    SlideDetailsContentComponent,
    SlideDetailsDetailsComponent,
    SmallProgressSpinnerComponent,
    TextWithEditComponent
  ],
  exports: [
    // angular modules
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // angular material modules
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatIconModule,
    MatSnackBarModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatInputModule,
    MatTooltipModule,
    MatTableModule,
    MatSortModule,
    MatListModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatMenuModule,
    CdkTableModule,
    MatDatepickerModule,
    MatDatetimepickerModule,
    // Covalent modules
    CovalentLayoutModule,
    CovalentMessageModule,
    CovalentExpansionPanelModule,
    // other modules
    FlexLayoutModule,
    TranslateModule,
    NgxUploaderModule,
    // custom components
    HeaderComponent,
    GroupedGridComponent,
    UploadInputComponent,
    ActionCardComponent,
    ActionCardComponent,
    CompletableCardComponent,
    EditWithSideBarComponent,
    SideBarItemComponent,
    EditContentComponent,
    SideBarBackButtonComponent,
    DefaultErrorsComponent,
    CheckRolesDirective,
    SlideDetailsComponent,
    SlideDetailsContentComponent,
    SlideDetailsDetailsComponent,
    MatProgressSpinnerModule,
    SmallProgressSpinnerComponent,
    TextWithEditComponent,
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        PaginatorI18NService,
        {provide: MatPaginatorIntl, useExisting: PaginatorI18NService},
        {provide: MAT_DATETIME_FORMATS, useValue: DATETIME_FORMATS},
        {provide: MAT_DATE_LOCALE, useValue: 'fr-CH'},
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: DatetimeAdapter, useClass: MomentDatetimeAdapter, deps: [MAT_DATE_LOCALE, DateAdapter]}
      ]
    };
  }
}
