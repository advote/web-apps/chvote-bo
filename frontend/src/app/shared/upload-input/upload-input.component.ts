/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { UploaderOptions, UploadFile, UploadInput, UploadOutput, UploadStatus } from 'ngx-uploader';
import { MatDialog } from '@angular/material';
import { TechnicalErrorDialogComponent } from '../../core/technical-error-dialog/technical-error-dialog.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from "moment";
import { TranslateService } from '@ngx-translate/core';
import { ValidationErrorModel } from '../../core/service/http/validation.error.model';
import { DateInterceptor } from '../../core/service/http/DateInterceptor';
import { HttpParameters } from '../../core/service/http/http.parameters.service';
import { AUTHORIZATION_KEY } from '../../core/authentication/authentication.service';
import { ConfirmBeforeQuit } from '../../core/confirm-before-quit';

@Component({
  selector: 'upload-input',
  templateUrl: './upload-input.component.html',
  styleUrls: ['./upload-input.component.scss']
})
export class UploadInputComponent implements ConfirmBeforeQuit {

  options: UploaderOptions;
  uploadInput: EventEmitter<UploadInput> = new EventEmitter<UploadInput>();
  file: UploadFile;
  uploadInProgress = false;
  uploadForm: FormGroup;


  uploadProgress = {
    percent: 0,
    eta: 0
  };

  @ViewChild('fileInput') fileInput;

  @Input() placeholder: string;
  @Input() uploadUrl: string;
  @Input() uploadIcon: string;
  @Input() uploadText: string;
  @Input() progressText: string;
  @Input() fileAccept: string;
  @Input() checkBeforeUpload;
  @Output() uploadDone = new EventEmitter<any>();
  @Output() uploadStarted = new EventEmitter<boolean>();

  constructor(private formBuilder: FormBuilder,
              private httpService: HttpParameters,
              private translateService: TranslateService,
              private dialog: MatDialog) {
    this.createForm();
  }


  isFormModified(): boolean {
    return !!this.file;
  }

  createForm() {
    this.uploadForm = this.formBuilder.group({
      uploadFile: [
        {value: null},
        Validators.required
      ]
    });
  }

  onUploadOutput(output: UploadOutput): void {
    if (output.file) {
      this.file = output.file;
      this.uploadForm.controls.uploadFile.setValue(this.file.nativeFile.name);
    }

    this.updateUpload();
  }


  get timeLeft() {
    let minutes = Math.floor(this.uploadProgress.eta / 60);
    let seconds = this.uploadProgress.eta - minutes * 60;
    if (minutes > 5) {
      return this.translateService.instant("global.time.endAt") +
             moment().add(this.uploadProgress.eta, "seconds").format("HH:mm:ss");
    }
    if (minutes > 0) {
      return this.translateService.instant("global.time.timeLeft") +
             `${minutes}m ${seconds}s`;
    }
    return this.translateService.instant("global.time.timeLeft") +
           `${seconds}s`;
  }

  private updateUpload() {
    if (this.uploadInProgress) {
      if (this.file.progress.status == UploadStatus.Done) {
        this.uploadForm.controls.uploadFile.enable();
        if (this.file.responseStatus == 200) {
          let result = JSON.parse(JSON.stringify(this.file.response), DateInterceptor.parseDateFields);
          this.uploadDone.next(result);
          this.file = null;
          this.fileInput.nativeElement.value = ''; // reset the HTML file input to avoid graphical glitch
          this.uploadForm.controls.uploadFile.markAsPristine();

        } else {
          this.uploadDone.next(null);

          if (this.file.responseStatus == 400) {
            let businessError: ValidationErrorModel = this.file.response;
            this.httpService.translateMessages(businessError);
            this.uploadForm.controls.uploadFile.setErrors(
              {'invalidFile': {'message': businessError.globalErrors[0]['message']}});

          } else if (this.file.responseStatus == 500) {
            this.dialog.open(TechnicalErrorDialogComponent, {data: this.file.response.message});
          }
        }
        this.uploadProgress.percent = 0;
        this.uploadInProgress = false;
      } else {
        this.uploadProgress.percent = this.file.progress.data.percentage;
        this.uploadProgress.eta = this.file.progress.data.eta;
      }
    }
  }

  startUpload(): void {
    this.uploadInProgress = true;
    this.uploadForm.controls.uploadFile.disable();
    if (this.uploadStarted) {
      this.uploadStarted.next(true);
    }
    this.uploadProgress.percent = 0;
    if (this.file) {
      this.file.progress.status = UploadStatus.Queue;
    }

    const event: UploadInput = {
      type: 'uploadAll',
      url: this.uploadUrl,
      method: 'POST'
    };

    this.addAuthenticationIfAvailable(event);

    this.uploadInput.emit(event);
  }

  preview() {
    this.uploadForm.controls.uploadFile.markAsTouched();
    if (this.checkBeforeUpload) {
      const checkResult = this.checkBeforeUpload();

      if (checkResult.subscribe) {
        checkResult.subscribe(result => {
          if (result) {
            setTimeout(() => this.startUpload());
          }
        })
      } else if (checkResult) {
        this.startUpload();
      }
    } else {
      this.startUpload();
    }
  }

  private addAuthenticationIfAvailable(event: UploadInput) {
    const authorization = localStorage.getItem(AUTHORIZATION_KEY);
    if (authorization) {
      event.withCredentials = false;
      event.headers = {Authorization: "Basic " + authorization};
    }
  }
}
