/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthorizationGuard } from './core/service/http/authorization.service';
import { LoginComponent } from './core/authentication/login/login.component';
import { BaseCreateComponent } from './operation/parameters/base/base-create.component';
import { ConfirmBeforeQuitGuard } from './core/confirm-before-quit';

export const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {
    path: 'operations/create',
    component: BaseCreateComponent,
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
    canDeactivate: [ConfirmBeforeQuitGuard]
  },
  {
    path: 'operations/:id',
    loadChildren: 'app/operation/operation-management.module#OperationManagementModule',
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
  },
  {
    path: "admin",
    loadChildren: 'app/admin/admin.module#AdminModule',
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
  },
  {
    path: 'dashboard',
    loadChildren: 'app/dashboard/dashboard.module#DashboardModule',
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard],
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
    canActivate: [AuthorizationGuard],
    canActivateChild: [AuthorizationGuard]
  },
];

/**
 * Main routing module.
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
