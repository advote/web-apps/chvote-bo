/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators/tap';
import { ElectoralAuthorityKey } from './electoral-authority-key';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpParameters } from '../../core/service/http/http.parameters.service';

@Injectable()
export class ElectoralAuthorityKeyService {
  public completed = new BehaviorSubject<boolean>(false);
  private serviceUrl: string;

  constructor(private http: HttpClient, private params: HttpParameters) {
    this.serviceUrl = `${this.params.apiBaseURL}/electoral-authority-key`;
  }


  findAll(): Observable<ElectoralAuthorityKey[]> {


    return this.http.get<ElectoralAuthorityKey[]>(this.serviceUrl)
      .pipe(
        tap(keys => {
          this.completed.next(keys.length > 0);
        })
      );
  }


  updateLabel(id: number, newLabel: string): Observable<ElectoralAuthorityKey> {
    return this.http.put<ElectoralAuthorityKey>(this.serviceUrl, {id, label: newLabel});
  }


  uploadUrl(label: string) {
    return this.serviceUrl + "?label=" + encodeURIComponent(label);
  }

}

