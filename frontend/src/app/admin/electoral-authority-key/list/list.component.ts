/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataSource } from '@angular/cdk/collections';
import { ElectoralAuthorityKey } from '../electoral-authority-key';
import { ElectoralAuthorityKeyService } from '../electoral-authority-key.service';
import { MatDialog, MatSort } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { map, merge } from 'rxjs/operators';
import { sortOnProperty } from '../../../core/util/table-utils';
import { AuthorizationService } from '../../../core/service/http/authorization.service';
import { EditLabelComponent } from '../edit-label/edit-label.component';
import { labelNotAlreadyUsed } from '../../../core/util/form-utils';

@Component({
  selector: 'electoral-authority-key-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ElectoralAuthorityKeyListComponent implements OnInit {
  importForm: FormGroup;
  dataSource: ElectoralAuthorityKeyDataSource;
  displayedColumns = ["label", "keyHash", "importDate", "login",];
  @ViewChild(MatSort) sort: MatSort;
  keys: ElectoralAuthorityKey[] = [];

  constructor(fb: FormBuilder,
              private service: ElectoralAuthorityKeyService,
              public dialog: MatDialog,
              private authService: AuthorizationService) {
    this.importForm = fb.group({
      label: [null,
        [Validators.required, Validators.maxLength(200), labelNotAlreadyUsed(() => this.keys.map(k => k.label))]],
    });

  }

  get readOnly() {
    return !this.authService.hasAtLeastOneRole(["ADD_ELECTORAL_AUTHORITY_KEY"]);
  }

  get uploadUrl() {
    return this.service.uploadUrl(this.importForm.value.label);
  }


  checkBeforeUpload() {
    return () => {
      this.importForm.controls.label.markAsTouched();
      return this.importForm.valid;
    }
  }

  editLabel(element: ElectoralAuthorityKey) {
    let dialogRef = this.dialog.open(EditLabelComponent, {
      width: '650px',
      data: {label: element.label, otherLabels: this.keys.map(k => k.label).filter(l => l !== element.label)}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.updateLabel(element.id, result).subscribe(() => {
          element.label = result;
        });
      }
    });
  }

  ngOnInit() {
    this.refreshKeys();
  }

  private refreshKeys() {
    this.service.findAll().subscribe(keys => {
      this.keys = keys;
      this.dataSource = new ElectoralAuthorityKeyDataSource(keys, this.sort)
    })
  }

  processUploadResult() {
    this.refreshKeys();
    this.importForm.reset();
  }


}


class ElectoralAuthorityKeyDataSource extends DataSource<ElectoralAuthorityKey> {

  constructor(private keys: ElectoralAuthorityKey[], private sort: MatSort) {
    super();
  }

  connect(): Observable<ElectoralAuthorityKey[]> {
    return of(this.keys).pipe(
      merge(this.sort.sortChange),
      map((data) => sortOnProperty(this.keys, this.sort.active, this.sort.direction)));
  }

  disconnect() {
  }
}

