/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { labelNotAlreadyUsed } from '../../../core/util/form-utils';


@Component({
  selector: 'edit-label',
  templateUrl: './edit-label.component.html',
  styleUrls: ['./edit-label.component.scss']
})
export class EditLabelComponent implements OnInit {
  public form: FormGroup;

  constructor(fb: FormBuilder,
              public dialogRef: MatDialogRef<EditLabelComponent>,
              @Inject(MAT_DIALOG_DATA) public data: { label: string, otherLabels: string[] }) {
    this.form = fb.group({
      label: [data.label,
        [Validators.required, Validators.maxLength(200), labelNotAlreadyUsed(() => data.otherLabels)]],
    });
  }


  accept() {
    this.form.controls.label.markAsTouched();
    if (this.form.valid) {
      this.dialogRef.close(this.form.controls.label.value);
    }
  }


  cancel(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
  }

}
