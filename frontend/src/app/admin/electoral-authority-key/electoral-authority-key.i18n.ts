/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_electoralAuthorityKey = {
  fr: {
    "card": {
      "title": "Clés de l'autorité électorale",
      "subtitle": "Clés publiques utilisées pour le chiffrement de l'urne électronique ",
      "key-count": "Nombre de clés"
    },
    "label-edit-dialog": {
      "title": "Editer l'intitulé de la clé de l'autorité électorale"
    },
    "sidebar": {
      "title": "Clés Autorité Électorale"
    },
    "edit": {
      "title": "Clés de l'autorité électorale",
      "subtitle": "Définir les clés publiques utilisées pour le chiffrement de l'urne électronique",
      "table": {
        "header": {
          "label": "Intitulé de la clé",
          "login": "Importée par",
          "importDate": "Importée le",
          "keyHash": "Empreinte"
        }
      },
      "form": {
        "fields": {
          "label": {
            "placeholder": "Intitulé de la clé"
          },
          "input-file": {
            "placeholder": "Clé à importer"
          }
        }
      }
    },
    "error": {
      "invalid-format": "Le format de la clé n'est pas reconnu. Vérifier le niveau de sécurité de la clé."
    }
  },
  de: {
    "card": {
      "title": "DE - Clés de l'autorité électorale",
      "subtitle": "DE - Clés publiques utilisées pour le chiffrement de l'urne électronique ",
      "key-count": "DE - Nombre de clés"
    },
    "label-edit-dialog": {
      "title": "DE - Editer l'intitulé de la clé de l'autorité électorale"
    },
    "sidebar": {
      "title": "DE - Clés Autorité Électorale"
    },
    "edit": {
      "title": "DE - Clés de l'autorité électorale",
      "subtitle": "DE - Définir les clés publiques utilisées pour le chiffrement de l'urne électronique",
      "table": {
        "header": {
          "label": "DE - Intitulé de la clé",
          "login": "DE - Importée par",
          "importDate": "DE - Importée le",
          "keyHash": "DE - Empreinte"
        }
      },
      "form": {
        "fields": {
          "label": {
            "placeholder": "DE - Intitulé de la clé"
          },
          "input-file": {
            "placeholder": "DE - Clé à importer"
          }
        }
      }
    },
    "error": {
      "invalid-format": "DE - Le format de la clé n'est pas reconnu. Vérifier le niveau de sécurité de la clé."
    }
  },
};
