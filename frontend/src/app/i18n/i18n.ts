/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import { i18n_global } from './global.i18n';
import { i18n_dictionary } from './dictionary.i18n';
import { i18n_parameters_base } from '../operation/parameters/base/base.i18n';
import { i18n_parameters_document } from '../operation/parameters/document/document.i18n';
import { i18n_parameters_repository } from '../operation/parameters/repository/repository.i18n';
import { i18n_parameters_doi } from '../operation/parameters/domain-influence/domain-influence.i18n';
import { i18n_parameters_milestone } from '../operation/parameters/milestone/milestone.i18n';
import { i18n_operation_management } from '../operation/operation-management.i18n';
import { i18n_dashboard } from '../dashboard/dashboard.i18n';
import { i18n_completable_card } from '../shared/completable-card/completable-card.i18n';
import { i18n_parameters_notification } from '../operation/parameters/parameters-notification/parameters-notification.i18n';
import { i18n_deployment_summary } from '../operation/deployment-summary/deployment-summary.i18n';
import { i18n_voting_material } from '../operation/voting-material/voting-material.i18n';
import { i18n_testing_cards_lot } from '../operation/testing-cards-lot/testing-cards-lot.i18n';
import { i18n_managementEntity } from '../operation/parameters/management-entity/management-entity.i18n';
import { i18n_consistency } from './consistency.i18n';
import { i18n_highlighted_question } from '../operation/parameters/document/highlighted-question/highlighted-question.i18n';
import { i18n_parameters_ballot_documentation } from '../operation/parameters/ballot-documentation/ballot-documentation.i18n';
import { i18n_election_page_properties } from '../operation/parameters/election-page-properties/election-page-properties.i18n';
import { i18n_admin } from '../admin/admin.i18n';
import { i18n_voting_period } from '../operation/voting-period/voting-period.i18n';
import { i18n_tally } from '../operation/tally/tally.i18n';
import { i18n_voting_site_configuration } from '../operation/parameters/voting-site-configuration/voting-site-configuration.i18n';

export const i18n = {
  fr: {
    "dictionary": i18n_dictionary.fr,
    "global": i18n_global.fr,
    "dashboard": i18n_dashboard.fr,
    "operation-management": i18n_operation_management.fr,
    "deployment-summary": i18n_deployment_summary.fr,
    "admin": i18n_admin.fr,
    "parameters": {
      "base": i18n_parameters_base.fr,
      "milestone": i18n_parameters_milestone.fr,
      "domain-influence": i18n_parameters_doi.fr,
      "repository": i18n_parameters_repository.fr,
      "document": i18n_parameters_document.fr,
      "highlighted-question": i18n_highlighted_question.fr,
      "ballot-documentation": i18n_parameters_ballot_documentation.fr,
      "election-page-properties": i18n_election_page_properties.fr,
      "management-entity": i18n_managementEntity.fr,
      "notification": i18n_parameters_notification.fr,
      "voting-site-configuration": i18n_voting_site_configuration.fr
    },

    "consistency": i18n_consistency.fr,
    "voting-material": i18n_voting_material.fr,
    "voting-period": i18n_voting_period.fr,
    "testing-cards-lot": i18n_testing_cards_lot.fr,
    "completable-card": i18n_completable_card.fr,
    "tally": i18n_tally.fr
  },
  de: {
    "dictionary": i18n_dictionary.de,
    "global": i18n_global.de,
    "dashboard": i18n_dashboard.de,
    "operation-management": i18n_operation_management.de,
    "deployment-summary": i18n_deployment_summary.de,
    "parameters": {
      "base": i18n_parameters_base.de,
      "milestone": i18n_parameters_milestone.de,
      "domain-influence": i18n_parameters_doi.de,
      "repository": i18n_parameters_repository.de,
      "document": i18n_parameters_document.de,
      "highlighted-question": i18n_highlighted_question.de,
      "management-entity": i18n_managementEntity.de,
      "notification": i18n_parameters_notification.de,
      "voting-site-configuration": i18n_voting_site_configuration.de
    },
    "voting-material": i18n_voting_material.de,
    "testing-cards-lot": i18n_testing_cards_lot.de,
    "completable-card": i18n_completable_card.de,
    "tally": i18n_tally.de
  }
};

