/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_consistency = {
  fr: {
    "configuration": {
      "error": {
        "unknownDoiForTestingCardLot": "Le domaine d'influence \"{{ unknownDoi }}\" configuré pour le lot de cartes de test \"{{ lotName }}\" n'est pas présent dans le référentiel de l'opération",
        "unknownOperationRepositoryDoi": "Le domaine d'influence \"{{ unknownDoi }}\" est inconnu du système",
        "ballotInConfigurationNotInOperationRepository": "La documentation des scrutins de l'opération fait référence au scrutin \"{{ballot}}\" n'appartenant pas au référentiel de l'opération",
        "ballotInElectionPagePropertiesNotInOperationRepository": "La configuration des paramètres d'affichage d'une élection fait référence au scrutin \"{{ballot}}\" n'appartenant pas au référentiel de l'opération",
      }
    },
    "votingMaterial": {
      "error": {
        "noPrinterAssociatedToMunicipality": "La commune {{municipality}} de numéro OFS {{ofsId}} présente dans le fichier de registre {{filename}} n'a pas d'imprimeur associé",
        "unknownDoiForTestingCardLot": "Le domaine d'influence \"{{ unknownDoi }}\" configuré pour le lot de cartes de test \"{{ lotName }}\" n'est pas présent dans le référentiel de l'opération",
      }
    }
  },
  de: {
    "configuration": {
      "error": {
        "unknownDoiForTestingCardLot": "DE - Le domaine d'influence \"{{ unknownDoi }}\" configuré pour le lot de cartes de test \"{{ lotName }}\" n'est pas présent dans le référentiel de l'opération",
        "unknownOperationRepositoryDoi": "DE - Le domaine d'influence \"{{ unknownDoi }}\" est inconnu du système",
        "ballotInConfigurationNotInOperationRepository": "DE - La documentation des scrutins de l'opération fait référence au scrutin \"{{ballot}}\" n'appartenant pas au référentiel de l'opération",
        "ballotInElectionPagePropertiesNotInOperationRepository": "DE - La configuration des paramètres d'affichage d'une élection fait référence au scrutin \"{{ballot}}\" n'appartenant pas au référentiel de l'opération",
      }
    },
    "votingMaterial": {
      "error": {
        "noPrinterAssociatedToMunicipality": "DE - La commune {{municipality}} de numéro OFS {{ofsId}} présente dans le fichier de registre {{filename}} n'a pas d'imprimeur associé",
        "unknownDoiForTestingCardLot": "DE - Le domaine d'influence \"{{ unknownDoi }}\" configuré pour le lot de cartes de test \"{{ lotName }}\" n'est pas présent dans le référentiel de l'opération",
      }
    }
  }
};
