/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

const actions = {
  fr: {
    "cancel": "abandonner",
    "validate": "valider",
    "save": "enregistrer",
    "close": "fermer",
    "browse": "parcourir",
    "import": "importer",
    "download": "télécharger",
    "delete": "supprimer",
    "yes": "oui",
    "no": "non",
    "error-report-download": "télécharger le rapport d'erreur",
    "logout": "Déconnecter",
    "quit-form-confirm": "Voulez vous vraiment interrompre l'action en cours ?"
  },
  de: {
    "cancel": "DE - abandonner",
    "validate": "DE - valider",
    "save": "DE - enregistrer",
    "close": "DE - fermer",
    "browse": "DE - parcourir",
    "import": "DE - importer",
    "download": "DE - télécharger",
    "delete": "DE - supprimer",
    "yes": "DE - oui",
    "no": "DE - non",
    "error-report-download": "DE - télécharger le rapport d'erreur",
    "logout": "DE - Déconnecter",
    "quit-form-confirm": "DE - Voulez vous vraiment interrompre l'action en cours ?"
  }
};

const login = {
  fr: {
    "user": "Utilisateur",
    "canton": "Canton",
    "management-entity": "Entité de gestion"
  },
  de: {
    "user": "DE - Utilisateur",
    "canton": "DE - Canton",
    "management-entity": "DE - Entité de gestion"
  }
};

const errors = {
  fr: {
    "invalid-contest-id": "Le champ \"contestIdentification\" est absent ou invalide",
    "invalid-sender-id": "Le champ \"senderId\" est absent ou invalide",
    "invalid-message-id": "Le champ \"messageId\" est absent ou invalide",
    "file-already-imported": "Ce fichier a déjà été importé le {{param0}} par {{param1}} pour l'opération \"{{param2}}\"",
    "not-xml-file": "Le fichier sélectionné n'a pas été reconnu comme un fichier XML",
    "not-expected-xml-file": "Le fichier XML sélectionné n'est pas un fichier de type {{param0}}",
    "not-pdf-file": "Le fichier sélectionné n'est pas un fichier PDF",
    "validation-error": "Le fichier fourni est invalide",
    "read-only": "L'état de l'opération ne permet pas d'opérer ce changement",
    "unknown": "Une erreur inattendue s'est produite",
    "forbidden": "Vous n'avez pas accès à la resource demandée",
    "not-allowed": "Vous n'êtes pas autorisé à utiliser cette fonctionalité",
    "unexpected": "Une erreur non prévue est survenue.",
    "string-invalid": "La valeur fournie contient des caractères invalides.",
    "business": "Une erreur métier est survenue : {{param0}}",
    "pact": {
      "request": {
        "bad_request": "La requête envoyée au PACT n'a pas été acceptée",
        "not_found": "Le serveur PACT n'est pas disponible à l'adresse {{param0}}",
        "unknown": "L'appel au server PACT à l'adresse {{param0}} a renvoyé une erreur : {{param1}} {{param2}}"
      }
    },
  },
  de: {
    "invalid-contest-id": "DE - Le champ \"contestIdentification\" est absent ou invalide",
    "invalid-sender-id": "DE - Le champ \"senderId\" est absent ou invalide",
    "invalid-message-id": "DE - Le champ \"messageId\" est absent ou invalide",
    "file-already-imported": "DE - Ce fichier a déjà été importé le {{param0}} par {{param1}} pour l'opération \"{{param2}}\"",
    "not-xml-file": "DE - Le fichier sélectionné n'a pas été reconnu comme un fichier XML",
    "not-pdf-file": "DE - Le fichier sélectionné n'est pas un fichier PDF",
    "validation-error": "DE - Le fichier fourni est invalide",
    "read-only": "DE - L'état de l'opération ne permet pas d'opérer ce changement",
    "unknown": "DE - Une erreur inattendue s'est produite",
    "forbidden": "DE - Vous n'avez pas accès à la resource demandée",
    "unexpected": "DE - Une erreur non prévue est survenue.",
    "string-invalid": "DE - La valeur fournie contient des caractères invalides.",
    "business": "DE - Une erreur métier est survenue : {{param0}}",
    "pact": {
      "request": {
        "bad_request": "DE - La requête envoyée au PACT n'a pas été acceptée",
        "not_found": "DE - Le serveur PACT n'est pas disponible à l'adresse {{param0}}",
        "unknown": "DE - L'appel au server PACT à l'adresse {{param0}} a renvoyé une erreur : {{param1}} {{param2}}"
      }
    },
  }
};


const validation = {
  fr: {
    "csv": {
      "header": {
        "lineNumber": "Ligne",
        "columnNumber": "Colonne",
        "message": "Message"
      }
    }
  },
  de: {
    "csv": {
      "header": {
        "lineNumber": "DE - Ligne",
        "columnNumber": "DE - Colonne",
        "message": "DE - Message"
      }
    }
  }

};


const forms = {
  fr: {
    "fields": {
      "required": "* Champs obligatoires"
    },
    "errors": {
      "required": "Champ obligatoire",
      "maxlength": "La taille maximale est {{requiredLength}}",
      "minlength": "La taille minimale est {{requiredLength}}",
      "max": "La valeur maximale est {{max}}",
      "min": "La valeur minimale est {{min}}",
      "onlyAlphaNumerical": "Caractères spéciaux interdits",
      "onlyNumerical": "Seuls les chiffres sont autorisés",
      "label-already-used": "Ce libellé est déjà utilisé",
      "matDatepickerMin": "Sélectionner une date ultérieure",
      "matDatepickerMax": "Sélectionner une date antérieure"
    }
  },
  de: {
    "fields": {
      "required": "* DE - Champs obligatoires"
    },
    "errors": {
      "required": "DE - Champ obligatoire",
      "maxlength": "DE - La taille maximale est {{requiredLength}}",
      "minlength": "DE - La taille minimale est {{requiredLength}}",
      "max": "DE - La valeur maximale est {{max}}",
      "min": "DE - La valeur minimale est {{min}}",
      "onlyAlphaNumerical": "DE - Caractères spéciaux interdits",
      "onlyNumerical": "DE - Seuls les chiffres sont autorisés",
      "label-already-used": "DE - Ce libellé est déjà utilisé",
      "matDatepickerMin": "DE - Sélectionner une date ultérieure",
      "matDatepickerMax": "DE - Sélectionner une date antérieure"
    }
  },
};


const businessErrorDialog = {
  fr: {
    "title": "Erreur fonctionnelle"
  },
  de: {
    "title": "DE - Erreur fonctionnelle"
  }
};


const technicalErrorDialog = {
  fr: {
    "title": "Erreur technique",
    "content": "Une erreur technique s'est produite."
  },
  de: {
    "title": "DE - Erreur technique",
    "content": "DE - Une erreur technique s'est produite."
  }
};
const time = {
  fr: {
    "timeLeft": "Temps restant : ",
    "endAt": "Termine à : "
  },
  de: {
    "timeLeft": "DE - Temps restant : ",
    "endAt": "DE - Termine à : "
  }
};
const table = {
  fr: {
    "paginator": {
      "of": "de",
      "itemsPerPageLabel": "Lignes par page : ",
      "nextPageLabel": "Page suivante",
      "previousPageLabel": "Page précédente"
    }
  },
  de: {
    "paginator": {
      "of": "DE - de",
      "itemsPerPageLabel": "DE - Lignes par page : ",
      "nextPageLabel": "DE - Page suivante",
      "previousPageLabel": "DE - Page précédente"
    }
  }
};
const date = {
  fr: {
    "month": {
      "january": {
        "long": "Janvier",
        "short": "Jan",
        "narrow": "J"
      },
      "february": {
        "long": "Février",
        "short": "Fév",
        "narrow": "F"
      },
      "march": {
        "long": "Mars",
        "short": "Mar",
        "narrow": "M"
      },
      "april": {
        "long": "Avril",
        "short": "Avr",
        "narrow": "A"
      },
      "may": {
        "long": "Mai",
        "short": "Mai",
        "narrow": "M"
      },
      "june": {
        "long": "Juin",
        "short": "Jui",
        "narrow": "J"
      },
      "july": {
        "long": "Juillet",
        "short": "Jui",
        "narrow": "J"
      },
      "august": {
        "long": "Août",
        "short": "Aoû",
        "narrow": "A"
      },
      "september": {
        "long": "Septembre",
        "short": "Sep",
        "narrow": "S"
      },
      "october": {
        "long": "Octobre",
        "short": "Oct",
        "narrow": "O"
      },
      "november": {
        "long": "Novembre",
        "short": "Nov",
        "narrow": "N"
      },
      "december": {
        "long": "Décembre",
        "short": "Déc",
        "narrow": "D"
      }
    },
    "day": {
      "monday": {
        "long": "Lundi",
        "short": "Lun",
        "narrow": "L"
      },
      "tuesday": {
        "long": "Mardi",
        "short": "Mar",
        "narrow": "M"
      },
      "wednesday": {
        "long": "Mercredi",
        "short": "Mer",
        "narrow": "M"
      },
      "thursday": {
        "long": "Jeudi",
        "short": "Jeu",
        "narrow": "J"
      },
      "friday": {
        "long": "Vendredi",
        "short": "Ven",
        "narrow": "V"
      },
      "saturday": {
        "long": "Samedi",
        "short": "Sam",
        "narrow": "S"
      },
      "sunday": {
        "long": "Dimanche",
        "short": "Dim",
        "narrow": "D"
      }
    }
  },
  de: {
    "month": {
      "january": {
        "long": "DE - Janvier",
        "short": "DE - Jan",
        "narrow": "DE - J"
      },
      "february": {
        "long": "DE - Février",
        "short": "DE - Fév",
        "narrow": "DE - F"
      },
      "march": {
        "long": "DE - Mars",
        "short": "DE - Mar",
        "narrow": "DE - M"
      },
      "april": {
        "long": "DE - Avril",
        "short": "DE - Avr",
        "narrow": "DE - A"
      },
      "may": {
        "long": "DE - Mai",
        "short": "DE - Mai",
        "narrow": "DE - M"
      },
      "june": {
        "long": "DE - Juin",
        "short": "DE - Jui",
        "narrow": "DE - J"
      },
      "july": {
        "long": "DE - Juillet",
        "short": "DE - Jui",
        "narrow": "DE - J"
      },
      "august": {
        "long": "DE - Août",
        "short": "DE - Aoû",
        "narrow": "DE - A"
      },
      "september": {
        "long": "DE - Septembre",
        "short": "DE - Sep",
        "narrow": "DE - S"
      },
      "october": {
        "long": "DE - Octobre",
        "short": "DE - Oct",
        "narrow": "DE - O"
      },
      "november": {
        "long": "DE - Novembre",
        "short": "DE - Nov",
        "narrow": "DE - N"
      },
      "december": {
        "long": "DE - Décembre",
        "short": "DE - Déc",
        "narrow": "DE - D"
      }
    },
    "day": {
      "monday": {
        "long": "DE - Lundi",
        "short": "DE - Lun",
        "narrow": "DE - L"
      },
      "tuesday": {
        "long": "DE - Mardi",
        "short": "DE - Mar",
        "narrow": "DE - M"
      },
      "wednesday": {
        "long": "DE - Mercredi",
        "short": "DE - Mer",
        "narrow": "DE - M"
      },
      "thursday": {
        "long": "DE - Jeudi",
        "short": "DE - Jeu",
        "narrow": "DE - J"
      },
      "friday": {
        "long": "DE - Vendredi",
        "short": "DE - Ven",
        "narrow": "DE - V"
      },
      "saturday": {
        "long": "DE - Samedi",
        "short": "DE - Sam",
        "narrow": "DE - S"
      },
      "sunday": {
        "long": "DE - Dimanche",
        "short": "DE - Dim",
        "narrow": "DE - D"
      }
    }
  }
};

export const i18n_global = {
  fr: {
    "title": "CHVote Back Office",
    "upload-progress": "Import en cours",
    "actions": actions.fr,
    "login": login.fr,
    "errors": errors.fr,
    "validation": validation.fr,
    "forms": forms.fr,
    "business-error-dialog": businessErrorDialog.fr,
    "technical-error-dialog": technicalErrorDialog.fr,
    "time": time.fr,
    "table": table.fr,
    "date": date.fr
  },
  de: {
    "title": "DE - CHVote Back Office",
    "upload-progress": "DE - Import en cours",
    "actions": actions.de,
    "login": login.de,
    "errors": errors.de,
    "validation": validation.de,
    "forms": forms.de,
    "business-error-dialog": businessErrorDialog.de,
    "technical-error-dialog": technicalErrorDialog.de,
    "time": time.de,
    "table": table.de,
    "date": date.de
  }
};
