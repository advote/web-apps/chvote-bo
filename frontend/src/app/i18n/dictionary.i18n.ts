/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

export const i18n_dictionary = {
  fr: {
    "card-type": {
      "TEST_SITE_TESTING_CARD": "Cartes de test",
      "PRODUCTION_TESTING_CARD": "Cartes de test",
      "CONTROLLER_TESTING_CARD": "Cartes de contrôleur",
      "PRINTER_TESTING_CARD": "Cartes de test imprimeur"
    },
    "signature": {
      "M": "M.",
      "Ms": "Mme"
    },
    "testing-card-language": {
      "FR": "FR",
      "DE": "DE"
    }
  },
  de: {
    "card-type": {
      "TEST_SITE_TESTING_CARD": "DE - Cartes de test",
      "PRODUCTION_TESTING_CARD": "DE - Cartes de test",
      "CONTROLLER_TESTING_CARD": "DE - Cartes de contrôleur",
      "PRINTER_TESTING_CARD": "DE - Cartes de test imprimeur"
    },
    "signature": {
      "M": "DE - M.",
      "Ms": "DE - Mme"
    },
    "testing-card-language": {
      "FR": "DE - FR",
      "DE": "DE - DE"
    }
  },
};
