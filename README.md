# CHVote-2.0 BackOffice
[![pipeline status](https://gitlab.com/chvote2/web-apps/chvote-bo/badges/master/pipeline.svg)](https://gitlab.com/chvote2/web-apps/chvote-bo/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chvote-bo&metric=alert_status)](https://sonarcloud.io/dashboard?id=chvote-bo)

The BackOffice application allows you to perform all the following actions:

- Create an operation
- Set up the operation
- Test the settings 
- Request deployment in the PACT application
- Validate the voting documents
- Finalize the preparation of voting materials
- Request the creation of voting materials
- Validate the voting documents
- Enter the closing date of the voting site
- Finalize the preparation of the voting period
- Request the initialization of the voting period
- Validate the initialization of the voting period
- Request early closure of the voting site


## Application role in CHVote system
The BackOffice application is used to create and configure an electoral operation 

# Documentation

[Design documentation](https://gitlab.com/chvote2/web-apps/chvote-bo/builds/artifacts/master/raw/bo-design-docs/target/generated-docs/pdf/back-office-application-design.pdf?job=artifact%3Adocs)

# Components

Technicaly the project is composed of the following projects:

| Project name        | Type           | Description  |
| ------------- |-------------| -----|
| **Backend** | a multi-modules maven project | a java application bootstrapped by spring-boot framework. It exposes a REST API for the frontend application |
| **Deploy** | standard directory | All yaml files describing deployment instructions used to deploy to Kubernetes  |
| **Deploy-openshift** | standard directory | All yaml files describing deployment instructions used to deploy to Openshift  |
| **Frontend** | Angular app| The Back Office UI, based on Angular and Material design     |
| **Bo-docker** | standard directory | It consists of a dockerfile for the nginx image, the nginx config file and a proxy config file. All used to configure Back Office reverse-proxy component|
| **System-tests** | standard directory | Contains docker-compose definition file used in End to End testing of the Back Office|
| **Bo-design-docs** | Maven project | It contains the overall project documentation: an *asciidoc* file and *plantuml* diagrams.|

Please refer to the backend and frontend respectives READMEs for further information:
* [Backend README](backend/README.md)
* [Deploy-openshift README](deploy-openshift/README.md)
* [Frontend README](frontend/README.md)

# Building

## Pre-requisites
In order to build the project please follow instructions on the relevant README file (backend & frontend).

The default `JAVA_HOME` variable must refer to a valid `JDK 11` installation.

## Build steps
Here are the commands to use in order to build the project components:

##### 1. REST Backend

```bash
cd backend/bo-backend-rest
mvn clean install
```
##### 2.  Mock server

```bash
cd backend/bo-backend-mock-server
mvn clean install
```
##### 3.  Frontend

```bash
cd frontend
npm install
npm run build

```
This will install all node modules used by the Angular application and there dependencies.

# Running
Those are ordered steps to be followed in order to have the project running as a local application:

##### 1. Backend REST

The backend server can be started using this command:

```bash
cd backend/bo-backend-rest
SPRING_PROFILES_ACTIVE=development mvn spring-boot:run
```

##### 2. Mock server

The mock server can be started using this command:

```bash
cd backend/bo-backend-mock-server
SPRING_PROFILES_ACTIVE=development mvn spring-boot:run
```
An interactive Swagger API documentation page will be available at http://localhost:9754/mock/swagger-ui.html

In order to create an operation with mock-server choose : http://localhost:9754/mock/swagger-ui.html#!/database45controller/createOperationUsingGET


##### 3. Application frontend

The frontend application can be served using this command:

```bash
cd frontend
npm start
```

The frontend application will be available at http://localhost:4300/

# Contributing
See [CONTRIBUTING.md](https://gitlab.com/chvote2/documentation/chvote-docs/blob/master/CONTRIBUTING.md)

# License
This application is Open Source software released under the [Affero General Public License 3.0](LICENSE) 
license.
