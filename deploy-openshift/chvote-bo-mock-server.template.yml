apiVersion: v1
kind: Template
labels:
  app: BO
  role: mock-server
metadata:
  name: chvote-bo-mockserver-template
  annotations:
    description: BO Mock Server deployment template
    iconClass: icon-nodejs
    openshift.io/display-name: BO Mock Server template
    openshift.io/long-description: BO Mck Server deployment template
    tags: BO
    template.openshift.io/bindable: "false"
parameters:
  - displayName: BO mock server image name
    name: BO_MOCKSERVER_IMAGE
    required: true
    value: chvote-bo-mockserver
  - displayName: BO mock server docker image tag
    name: BO_MOCKSERVER_IMAGE_TAG
    required: true
    value: latest
  - displayName: PACT - BO shared volume claim
    name: PACT_SHARE_PVC_NAME
    required: true
    value: pact-share
  - displayName: BO volume claim for storing H2 database file and sharing with mock
      server. TEMPORARY
    name: BO_DATABASE_PVC_NAME
    required: true
    value: bo-database
  - description: Minimum amount of memory the pod can use.
    displayName: Minimum Memory allocation
    name: BO_MOCKSERVER_MEMORY_REQUEST
    required: true
    value: 500M
  - description: Maximum amount of memory the pod can use.
    displayName: Memory Limit
    name: BO_MOCKSERVER_MEMORY_LIMIT
    required: true
    value: 700M
  - description: Minimum amount of CPU the pod can use.
    displayName: CPU Request
    name: BO_MOCKSERVER_CPU_REQUEST
    required: true
    value: 250m
  - description: Maximum amount of CPU the pod can use.
    displayName: CPU Limit
    name: BO_MOCKSERVER_CPU_LIMIT
    required: true
    value: 1000m
objects:
  - apiVersion: v1
    kind: Service
    metadata:
      name: bo-mockserver-api
      annotations:
        description: Exposes the BO Mock Server api
    spec:
      ports:
        - name: api
          port: 9754
          targetPort: 9754
      selector:
        app: BO
        role: mock-server
  - apiVersion: v1
    kind: Service
    metadata:
      name: bo-mockserver-management
      annotations:
        description: Exposes the BO Mock Server management endpoints
    spec:
      ports:
        - name: management
          port: 9756
          targetPort: 9756
      selector:
        app: BO
        role: mock-server
  - apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: bo-mockserver-sa
      labels:
        app: BO
        role: mock-server
  - apiVersion: v1
    kind: ImageStream
    metadata:
      name: ${BO_MOCKSERVER_IMAGE}-is
      labels:
        app: BO
        role: mock-server
    spec:
      lookupPolicy:
        local: false
      tags:
        - name: ${BO_MOCKSERVER_IMAGE_TAG}
          from:
            kind: ImageStreamTag
            name: ${BO_MOCKSERVER_IMAGE}:${BO_MOCKSERVER_IMAGE_TAG}
            namespace: chvote-images
  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      name: bo-mockserver-dc
      annotations:
        description: Defines how to deploy the BO Mock Server application
        template.alpha.openshift.io/wait-for-ready: "true"
      labels:
        app: BO
        role: mock-server
    spec:
      replicas: 1
      selector:
        name: bo-mockserver-container
      strategy:
        type: Recreate
      triggers:
        - imageChangeParams:
            automatic: true
            containerNames:
              - bo-mockserver-container
            from:
              kind: ImageStreamTag
              name: ${BO_MOCKSERVER_IMAGE}-is:${BO_MOCKSERVER_IMAGE_TAG}
          type: ImageChange
        - type: ConfigChange
      template:
        metadata:
          name: bo-mockserver-container-template
          labels:
            app: BO
            name: bo-mockserver-container
            role: mock-server
        spec:
          containers:
            - envFrom:
                - configMapRef:
                    name: chvote-bo-mockserver-env-config
              image: ${BO_MOCKSERVER_IMAGE}-is:${BO_MOCKSERVER_IMAGE_TAG}
              name: bo-mockserver-container
              ports:
                - containerPort: 9754
                - containerPort: 9756
              resources:
                limits:
                  cpu: ${BO_MOCKSERVER_CPU_LIMIT}
                  memory: ${BO_MOCKSERVER_MEMORY_LIMIT}
                requests:
                  cpu: ${BO_MOCKSERVER_CPU_REQUEST}
                  memory: ${BO_MOCKSERVER_MEMORY_REQUEST}
              volumeMounts:
                - mountPath: /log
                  name: logs
                - mountPath: /data
                  name: data
                - mountPath: /usr/share/pact
                  name: pact
              livenessProbe:
                httpGet:
                  path: /manage/health
                  port: 9756
                failureThreshold: 3
                initialDelaySeconds: 60
                periodSeconds: 60
                successThreshold: 1
                timeoutSeconds: 10
              readinessProbe:
                httpGet:
                  path: /manage/health
                  port: 9756
                failureThreshold: 4
                initialDelaySeconds: 60
                periodSeconds: 30
                successThreshold: 1
                timeoutSeconds: 10
          serviceAccount: bo-mockserver-sa
          volumes:
            - emptyDir: {}
              name: data
            - emptyDir: {}
              name: logs
            - emptyDir: {}
              name: pact