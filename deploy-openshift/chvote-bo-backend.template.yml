apiVersion: v1
kind: Template
labels:
  app: BO
  role: backend
metadata:
  annotations:
    description: BO deployment template
    iconClass: icon-nodejs
    openshift.io/display-name: BO backend template
    openshift.io/long-description: BO backend deployment template
    tags: BO
    template.openshift.io/bindable: "false"
  labels:
    app: BO
    role: backend
  name: chvote-bo-backend-template
parameters:
  - displayName: BO backend image name
    name: BO_BACKEND_IMAGE
    required: true
    value: chvote-bo-backend
  - displayName: BO backend docker image tag
    name: BO_BACKEND_IMAGE_TAG
    required: true
    value: latest
  - displayName: PACT - BO shared volume claim
    name: PACT_SHARE_PVC_NAME
    required: true
    value: pact-share
  - displayName: BO volume claim for storing H2 database file and sharing with mock
      server. TEMPORARY
    name: BO_DATABASE_PVC_NAME
    required: true
    value: bo-database
  - description: Minimum amount of memory the backend pod can use.
    displayName: Minimum Memory allocation
    name: BO_BACKEND_MEMORY_REQUEST
    required: true
    value: 1G
  - description: Maximum amount of memory the backend pod can use.
    displayName: Memory Limit
    name: BO_BACKEND_MEMORY_LIMIT
    required: true
    value: 1.5G
  - description: Minimum amount of CPU the backend pod can use.
    displayName: CPU Request
    name: BO_BACKEND_CPU_REQUEST
    required: true
    value: 1000m
  - description: Maximum amount of CPU the backend pod can use.
    displayName: CPU Limit
    name: BO_BACKEND_CPU_LIMIT
    required: true
    value: 2000m
objects:
  - apiVersion: v1
    kind: Service
    metadata:
      name: bo-backend-api
      annotations:
        description: Exposes the BO backend api
      labels:
        app: BO
        role: backend
    spec:
      ports:
        - name: api
          port: 9753
          targetPort: 9753
      selector:
        app: BO
        role: backend
  - apiVersion: v1
    kind: Service
    metadata:
      name: bo-backend-management
      annotations:
        description: Exposes the BO backend management endpoints
      labels:
        app: BO
        role: backend
    spec:
      ports:
        - name: management
          port: 9755
          targetPort: 9755
      selector:
        app: BO
        role: backend
  - apiVersion: v1
    kind: Service
    metadata:
      name: bo-backend-db
      annotations:
        description: Exposes the BO H2 databse
      labels:
        app: BO
        role: backend
    spec:
      ports:
        - name: db
          port: 9757
          targetPort: 9757
      selector:
        app: BO
        role: backend
  - apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: bo-backend-sa
      labels:
        app: BO
        role: backend
  - apiVersion: v1
    kind: ImageStream
    metadata:
      name: ${BO_BACKEND_IMAGE}-is
      labels:
        app: BO
        role: backend
    spec:
      lookupPolicy:
        local: false
      tags:
        - name: ${BO_BACKEND_IMAGE_TAG}
          from:
            kind: ImageStreamTag
            name: ${BO_BACKEND_IMAGE}:${BO_BACKEND_IMAGE_TAG}
            namespace: chvote-images
  - apiVersion: v1
    kind: DeploymentConfig
    metadata:
      name: bo-backend-dc
      annotations:
        description: Defines how to deploy the BO backend application
        template.alpha.openshift.io/wait-for-ready: "true"
      labels:
        app: BO
        role: backend
    spec:
      replicas: 1
      selector:
        name: bo-backend-container
      strategy:
        type: Recreate
      triggers:
        - imageChangeParams:
            automatic: true
            containerNames:
              - bo-backend-container
            from:
              kind: ImageStreamTag
              name: ${BO_BACKEND_IMAGE}-is:${BO_BACKEND_IMAGE_TAG}
          type: ImageChange
        - type: ConfigChange
      template:
        metadata:
          name: bo-backend-container-template
          labels:
            app: BO
            name: bo-backend-container
            role: backend
        spec:
          containers:
            - name: bo-backend-container
              image: ${BO_BACKEND_IMAGE}-is:${BO_BACKEND_IMAGE_TAG}
              ports:
                - containerPort: 9753
                - containerPort: 9755
              envFrom:
                - configMapRef:
                    name: chvote-bo-backend-env-config
              resources:
                limits:
                  cpu: ${BO_BACKEND_CPU_LIMIT}
                  memory: ${BO_BACKEND_MEMORY_LIMIT}
                requests:
                  cpu: ${BO_BACKEND_CPU_REQUEST}
                  memory: ${BO_BACKEND_MEMORY_REQUEST}
              volumeMounts:
                - mountPath: /log
                  name: logs
                - mountPath: /data
                  name: data
                - mountPath: /usr/share/pact
                  name: pact
              livenessProbe:
                httpGet:
                  path: /manage/health
                  port: 9755
                failureThreshold: 3
                initialDelaySeconds: 60
                periodSeconds: 60
                successThreshold: 1
                timeoutSeconds: 10
              readinessProbe:
                httpGet:
                  path: /manage/health
                  port: 9755
                failureThreshold: 4
                initialDelaySeconds: 60
                periodSeconds: 30
                successThreshold: 1
                timeoutSeconds: 10
          serviceAccount: bo-backend-sa
          volumes:
            - emptyDir: {}
              name: data
            - emptyDir: {}
              name: logs
            - emptyDir: {}
              name: pact