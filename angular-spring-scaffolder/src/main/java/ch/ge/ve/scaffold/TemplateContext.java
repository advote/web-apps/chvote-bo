/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.scaffold;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TemplateContext {
  private final String objectName;
  private final String humanName;
  private final String className;
  private final String kebabCaseClassName;
  private final String restApiBaseName;
  private final String restControllerPackage;
  private final String rawClassName;
  private final String servicePackage;
  private final String modelFullClass;
  private       String i18nPrefix;

  private final List<EntityField> fields;
  private Map<String, Collection<String>> enums = new HashMap<>();

  public TemplateContext(Class<?> clazz, String restControllerPackage, String javaServicePackage) {
    modelFullClass = clazz.getName();
    rawClassName = clazz.getSimpleName();
    className = Utils.removeBoSufix(rawClassName);
    kebabCaseClassName = Utils.toTsFileName(className);
    restApiBaseName = Utils.toRestApiBaseName(className);
    objectName = Utils.toObjectName(className);
    humanName = Utils.toHumanName(className);
    fields = Stream.of(clazz.getDeclaredFields()).map((Field field) -> new EntityField(field, this))
                   .collect(Collectors.toList());
    this.servicePackage = javaServicePackage + "." + objectName;
    this.restControllerPackage = restControllerPackage + "." + objectName;
  }

  public String getKebabCaseClassName() {
    return kebabCaseClassName;
  }

  public String getClassName() {
    return className;
  }

  public String getObjectName() {
    return objectName;
  }

  public List<EntityField> getFields() {
    return fields;
  }

  public String getRestApiBaseName() {
    return restApiBaseName;
  }

  public String getHumanName() {
    return humanName;
  }

  public TemplateContext asFr() {
    i18nPrefix = "";
    return this;
  }

  public TemplateContext asDe() {
    i18nPrefix = "DE - ";
    return this;
  }

  public String getI18nPrefix() {
    return i18nPrefix;
  }

  public String getRestControllerPackage() {
    return restControllerPackage;
  }

  public String getRawClassName() {
    return rawClassName;
  }

  public String getServicePackage() {
    return servicePackage;
  }

  public String getModelFullClass() {
    return modelFullClass;
  }

  public void addEnum(String enumType, List<String> possibleValues) {
    enums.put(enumType, possibleValues);
  }

  public Map<String, Collection<String>> getEnums() {
    return enums;
  }


  public EntityField getField(String name) {
    return fields.stream().filter(f -> f.getName().equals(name)).findFirst()
                 .orElseThrow(() -> new RuntimeException("cannot find field with name " + name));
  }
}
