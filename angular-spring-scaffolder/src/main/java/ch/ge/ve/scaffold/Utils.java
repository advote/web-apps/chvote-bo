/*
 * #%L
 * chvote-bo
 * %%
 * Copyright (C) 2016 - 2019 République et Canton de Genève
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ch.ge.ve.scaffold;

public class Utils {
  public static String toTsFileName(String className) {
    return toSnakeCase(className);
  }

  public static String toRestApiBaseName(String className) {
    return toSnakeCase(className);
  }

  private static String toSnakeCase(String className) {
    return className.replaceAll("(.)(\\p{Upper})", "$1-$2").toLowerCase();
  }

  public static String toObjectName(String simpleName) {
    return simpleName.substring(0, 1).toLowerCase() + simpleName.substring(1);
  }

  public static String toHumanName(String className) {
    return className.replaceAll("(.)(\\p{Upper})", "$1 $2");
  }

  public static String removeBoSufix(String simpleName) {
    return simpleName.replaceAll("V[Oo]$", "");
  }
}
