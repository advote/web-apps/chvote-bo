<#-- @formatter:off -->
<form [formGroup]="form" novalidate>
<mat-card>
  <mat-card-header>
    <mat-card-title>{{ "${objectName}.title" | translate }}</mat-card-title>
    <mat-card-subtitle>{{ "${objectName}.subtitle." + mode | translate }}</mat-card-subtitle>
  </mat-card-header>

  <mat-card-content fxLayout="column" class="no-padding">
      <div fxLayout="row wrap" fxLayoutGap="2.5rem">
        <#list fields as field>
        <#if field.name != "id" >

        <#if field.tsType == "boolean">
        <mat-slide-toggle name="${field.name}" #${field.name} formControlName="${field.name}" >{{'${objectName}.form.placeholder.${field.name}' | translate }}</mat-slide-toggle>

        <#elseif field.tsType == "Date">
          <mat-form-field fxFlex="25 0 15rem">
              <mat-placeholder>{{'${objectName}.form.placeholder.${field.name}' | translate }}</mat-placeholder>
              <mat-datetimepicker-toggle [for]="${field.name}Datepicker" matSuffix></mat-datetimepicker-toggle>
              <mat-datetimepicker #${field.name}Datepicker type="<#if field.dateWithTime>datetime<#else>date</#if>"></mat-datetimepicker>
              <input matInput
                     formControlName="${field.name}"
                     [matDatetimepicker]="${field.name}Datepicker"
                     required
                     autocomplete="false">
              <mat-error *ngIf="form.get('${field.name}').hasError('required')">
                  <span>{{'global.forms.errors.required' | translate}}</span>
              </mat-error>
              <mat-error *ngIf="form.get('${field.name}').hasError('invalidDate')">
                  <span>{{'global.forms.errors.invalid-date' | translate}}</span>
              </mat-error>
          </mat-form-field>

        <#else>
        <mat-form-field fxFlex="50 0 20rem">
          <#if field.enum>
          <mat-select name="${field.name}" formControlName="${field.name}"  placeholder="{{'${objectName}.form.placeholder.${field.name}' | translate  }}" #${field.name}>
            <mat-option *ngFor="let possibleValue of ${field.enumType}PossibleValues" [value]="possibleValue">
            {{"dictionary.${field.enumType}." + possibleValue | translate}}
            </mat-option>
          </mat-select>
          <#else>


          <input matInput
            name="${field.name}"
            <#if field.tsType == "number">type="number"</#if>
            formControlName="${field.name}"
            #${field.name}
            placeholder="{{'${objectName}.form.placeholder.${field.name}' | translate  }}">
          </#if>


          <mat-error *ngIf="<#if field.enum><#-- TODO : this is a workarroud--> ${field.name}.errorState &&</#if> form.get('${field.name}').hasError('required')">
            <span>{{'global.forms.errors.required' | translate}}</span>
          </mat-error>

          <mat-error *ngIf="form.get('${field.name}').hasError('maxlength')">
            <span>{{'global.forms.errors.maxLength' | translate:form.get('${field.name}').getError('maxlength')}}</span>
          </mat-error>

          <mat-error *ngIf="form.get('${field.name}').hasError('minlength')">
            <span>{{'global.forms.errors.minLength' | translate:form.get('${field.name}').getError('minlength')}}</span>
          </mat-error>
        </mat-form-field>
        </#if>
        </#if>
        </#list>
        </div>
  </mat-card-content>
  <mat-card-actions>
    <div fxLayout fxLayoutAlign="end">
      <button mat-raised-button
              id="saveButton"
              color="primary"
              type="submit"
              (click)="save()">{{'global.actions.save' | translate | uppercase}}
      </button>
    </div>
  </mat-card-actions>
</mat-card>
</form>

<#-- @formatter:on -->