<#-- @formatter:off -->
<mat-card>
  <mat-card-header>
    <mat-card-title>{{ "${objectName}.title" | translate }}</mat-card-title>
    <mat-card-subtitle>{{ "${objectName}.subtitle.list" | translate }}</mat-card-subtitle>
  </mat-card-header>

  <mat-card-content fxLayout="column" class="no-padding">
    <div class="mat-card-button-toolbar" fxLayout fxLayoutAlign="end" fxLayoutGap="2.5rem">
      <button mat-raised-button (click)="createNew()" color="primary" id="createNew${className}Button">
        <mat-icon>add</mat-icon>
        {{"${objectName}.actions.create" | translate | uppercase}}
      </button>
    </div>


    <div [hidden]="elementsCount === 0">

      <mat-table #table [dataSource]="dataSource" matSort id="${objectName}Table">
        <#list fields as field>

        <!-- Column definition for field ${field.name} -->
        <ng-container matColumnDef="${field.name}">
          <mat-header-cell *matHeaderCellDef mat-sort-header>
            {{"${objectName}.grid.header.${field.name}" | translate}}
          </mat-header-cell>
          <mat-cell *matCellDef="let element">
          <#if field.tsType == "Date">
          {{element.${field.name} | date:'<#if field.dateWithTime>dd.MM.yyyy hh:mm<#else>dd.MM.yyyy</#if>'}}
          <#elseif field.isEnum()>
          {{"dictionary.${field.enumType}." + element.${field.name} | translate}}
          <#else>
          {{element.${field.name}}}
          </#if>
          </mat-cell>
        </ng-container>
        </#list>

        <!-- Column definition for actions -->
        <ng-container matColumnDef="actions">
          <mat-header-cell *matHeaderCellDef> {{"${objectName}.grid.header.actions" | translate}}
          </mat-header-cell>
          <mat-cell *matCellDef="let element">
            <button mat-icon-button color="accent" (click)="remove(element)"><mat-icon>clear</mat-icon></button>
            <button mat-icon-button (click)="edit(element)"><mat-icon>edit</mat-icon></button>
          </mat-cell>
        </ng-container>

        <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
        <mat-row *matRowDef="let row; columns: displayedColumns;"></mat-row>
      </mat-table>
    </div>

  </mat-card-content>
</mat-card>
<#-- @formatter:on -->