<#-- @formatter:off -->
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ${className}ListComponent } from './${kebabCaseClassName}-list/${kebabCaseClassName}-list.component';
import { ${className}DetailsComponent } from './${kebabCaseClassName}-details/${kebabCaseClassName}-details.component';
import { ${className}Resolver } from './service/${kebabCaseClassName}.resolver';

const routes: Routes = [
  {
    path: 'create',
    component: ${className}DetailsComponent
  },
  {
    path: ':${objectName}Id',
    component: ${className}DetailsComponent,
    resolve: {
      ${objectName}: ${className}Resolver,
    }
  },
  {
    path: '',
    component: ${className}ListComponent
  },
];

/**
 * Routing module for operations list page.
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ${className}RoutingModule {
}
<#-- @formatter:on -->