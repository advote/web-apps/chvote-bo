<#-- @formatter:off -->
package ${servicePackage}.impl;

import ${modelFullClass};
import ${servicePackage}.${className}Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ${className}ServiceMockImpl implements ${className}Service {
  private long nextId;
  Map<Long, ${rawClassName}> elements = new HashMap<>();

  @Override
  public List<${rawClassName}> findAll() {
    return new ArrayList<>(elements.values());
  }

  @Override
  public ${rawClassName} create(${rawClassName} element) {
    VoterTestingCardsLotDef newElement = new VoterTestingCardsLotDef(
        nextId++,<#list fields as field><#if !(field.name == "id")>element.${field.getter}<#if !field?is_last>,</#if></#if></#list>
        );
    elements.put(newElement.getId(), newElement);
    return newElement;
  }

  @Override
  public ${rawClassName} edit(${rawClassName} element) {
    elements.put(element.getId(), element);
    return element;
  }

  @Override
  public ${rawClassName} findOne(long id) {
    return elements.get(id);
  }

  @Override
  public boolean delete(long id) {
    return elements.remove(id) != null;
  }
}
<#-- @formatter:on -->