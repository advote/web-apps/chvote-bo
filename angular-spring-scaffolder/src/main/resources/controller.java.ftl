<#-- @formatter:off -->
package ${restControllerPackage};
import java.util.List;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import ${servicePackage}.${className}Service;
import ${modelFullClass};

import org.springframework.web.bind.annotation.*;

/**
 * Controller for ${humanName} related request
 */
@RestController
@RequestMapping("/${kebabCaseClassName}")
public class ${className}Controller {

  private final ${className}Service ${objectName}Service;

  /**
   * Main constructor
   *
   * @param ${objectName}Service Business service
   */
  public ${className}Controller(${className}Service ${objectName}Service) {
    this.${objectName}Service = ${objectName}Service;
  }

  @RequestMapping(value = "", method = GET)
  public List<${rawClassName}> findAll() {
    return ${objectName}Service.findAll();
  }

  @RequestMapping(method = POST)
  public ${rawClassName} create(@RequestBody ${rawClassName} ${objectName}) {
    return ${objectName}Service.create(${objectName});
  }

  @RequestMapping(method = PUT)
  public ${rawClassName} edit(@RequestBody ${rawClassName}  ${objectName})  {
    return  ${objectName}Service.edit(${objectName});
  }

  @RequestMapping(value = "/{id}", method = GET)
  public ${rawClassName} findOne(@PathVariable long id) {
    return  ${objectName}Service.findOne(id);
  }

  @RequestMapping(value = "/{id}", method = DELETE)
  public boolean delete(@PathVariable long id) {
    return  ${objectName}Service.delete(id);
  }

}
<#-- @formatter:on -->