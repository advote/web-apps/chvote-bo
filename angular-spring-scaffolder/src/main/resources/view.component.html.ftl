<#-- @formatter:off -->
<div id="${objectName}View" *ngIf="${objectName}">
    <#list fields as field>
    <section class="mat-typography">
       <h4>${field.name}</h4>
       ${field.name}
    </section>
    <#if field.tsType == "Date">
       {{${objectName}.${field.name} | date:'<#if field.dateWithTime>dd.MM.yyyy hh:mm<#else>dd.MM.yyyy</#if>'}}
    <#elseif field.isEnum()>
        {{"dictionary.${field.enumType}." + element.${field.name} | translate}}
    <#else>
       {{${objectName}.${field.name}}}
    </#if>
    </#list>
</div>
<#-- @formatter:on -->