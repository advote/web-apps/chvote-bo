<#-- @formatter:off -->
import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: '${kebabCaseClassName}-view',
  templateUrl: './${kebabCaseClassName}-view.component.html',
  styleUrls: ['./${kebabCaseClassName}-view.component.scss']
})
export class ${className}ViewComponent {
  @Input()
  ${objectName};

  constructor(private route: ActivatedRoute) {
  }


}
<#-- @formatter:on -->
