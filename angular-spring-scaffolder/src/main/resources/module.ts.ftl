<#-- @formatter:off -->
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ${className}ListComponent } from './${kebabCaseClassName}-list/${kebabCaseClassName}-list.component';
import { ${className}DetailsComponent } from './${kebabCaseClassName}-details/${kebabCaseClassName}-details.component';
import { ${className}ViewComponent } from './${kebabCaseClassName}-view/${kebabCaseClassName}-view.component';
import { ${className}Resolver } from './service/${kebabCaseClassName}.resolver';
import { ${className}RoutingModule } from './${kebabCaseClassName}-routing.module';
import { ${className}Service } from './service/${kebabCaseClassName}.service';
import { CovalentDialogsModule } from '@covalent/core';

@NgModule({
  imports: [
    SharedModule,
    CovalentDialogsModule,
    ${className}RoutingModule,
  ],
  declarations: [${className}DetailsComponent, ${className}ListComponent, ${className}ViewComponent],
  providers: [
    ${className}Service,
    ${className}Resolver
  ]
})
export class ${className}Module {
}
<#-- @formatter:on -->