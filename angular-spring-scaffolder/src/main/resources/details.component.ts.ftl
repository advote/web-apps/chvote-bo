<#-- @formatter:off -->
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ${className}Service } from '../service/${kebabCaseClassName}.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: '${kebabCaseClassName}-details',
  templateUrl: './${kebabCaseClassName}-details.component.html',
  styleUrls: ['./${kebabCaseClassName}-details.component.scss']
})
export class ${className}DetailsComponent implements OnInit {

  form : FormGroup
  mode = "create";

  constructor(fb: FormBuilder,
    private router:Router,
    private ${objectName}Service : ${className}Service,
    private translateService: TranslateService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute) {
    this.form = fb.group({
        id:0,
       <#list fields as field>
       <#if field.name != "id" >
       ${field.name}: [null, [<#if field.required> Validators.required,</#if>  <#if field.minLength gt 0> Validators.minLength(${field.minLength}),</#if> <#if field.maxLength gt 0> Validators.maxLength(${field.maxLength})</#if>]],
       </#if>
       </#list>
    });
  }

  <#list enums?keys as enum >
  get ${enum}PossibleValues(){
       return [<#list enums[enum] as enumValue >'${enumValue}',</#list> ];
  }
  </#list>

  ngOnInit() {
    this.route.data.subscribe(data => {
        if (data.${objectName}){
            this.form.reset(data.${objectName});
            this.mode = "edit";
        }
      }
    );
  }
  save(): void {
    Object.keys(this.form.controls).forEach(k => this.form.controls[k].markAsTouched());
    this.form.updateValueAndValidity();

    if (this.form.valid) {
      if (this.mode === "create") {
        this.${objectName}Service
            .create(Object.assign(this.form.getRawValue(), {id:0}))
            .subscribe(() => {
                this.snackBar.open(this.translateService.instant('${objectName}.dialogs.save-success',{duration: 2000}));
                this.router.navigate(['..'], { relativeTo: this.route });
            });
      } else {
        this.${objectName}Service
            .update(this.form.getRawValue())
            .subscribe(() => {
                this.snackBar.open(this.translateService.instant('${objectName}.dialogs.update-success',{duration: 2000}));
                this.router.navigate(['..'], { relativeTo: this.route });
            });
      }
    }
  }
}
<#-- @formatter:on -->