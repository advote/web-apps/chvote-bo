<#-- @formatter:off -->
export class ${className} {
    constructor(<#list fields as field>public ${field.name} : ${field.tsType}<#if !field?is_last>, </#if></#list>){}
}
<#-- @formatter:on -->