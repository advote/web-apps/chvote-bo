<#-- @formatter:off -->
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppModel } from '../../model/app.model';
import { DataSource } from '@angular/cdk/collections';
import { MatSort } from "@angular/material/sort";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { sortOnProperty } from '../../core/util/table-utils';
import {  ${className} } from '../model/${kebabCaseClassName}';
import {  ${className}Service } from '../service/${kebabCaseClassName}.service';
import { TdDialogService } from '@covalent/core';

@Component({
  selector: '${kebabCaseClassName}-list',
  templateUrl: './${kebabCaseClassName}-list.component.html',
  styleUrls: ['./${kebabCaseClassName}-list.component.scss']
})
export class ${className}ListComponent implements OnInit {
  <#assign subject = objectName + "Subject">
  <#assign service =  objectName + "Service">

  ${subject} = new BehaviorSubject<${className}[]>([]);

  dataSource;

  @ViewChild(MatSort) sort: MatSort;

  displayedColumns = [
    <#list fields as field>
          "${field.name}",
    </#list>
    "actions"];

  get elementsCount() {
    return this.${subject}.value.length;
  }

  constructor(private appModel: AppModel,
              private route: ActivatedRoute,
              private translateService: TranslateService,
              private ${service}: ${className}Service,
              private dialogService : TdDialogService,
              private router: Router) {
  }

  ngOnInit() {
    this.appModel.setTitleSection(this.translateService.instant('${objectName}.toolbarSection'));
    this.${service}.findAll().subscribe(data => this.${subject}.next(data))
    this.dataSource = new ${className}DataSource(this.${subject}, this.sort)
  }

  createNew() {
    this.router.navigate(['create'], {relativeTo: this.route});
  }

  remove(element: ${className}) {
    this.dialogService.openConfirm({
      message: this.translateService.instant('${objectName}.dialogs.delete', element),
      disableClose: true,
      cancelButton: this.translateService.instant('global.actions.no'),
      acceptButton: this.translateService.instant('global.actions.yes'),
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.${service}.remove(element.id)
          .subscribe(() => this.${subject}.next(this.${subject}.getValue().filter((e) => e.id != element.id)));
      }
    });
  }

  edit(element: ${className}) {
    this.router.navigate([element.id], {relativeTo: this.route});
  }

}


class ${className}DataSource extends DataSource<any> {

  constructor(private data: BehaviorSubject<${className}[]>, private sort: MatSort) {
    super();
  }

  connect(): Observable<any[]> {
    return Observable.merge(this.data, this.sort.sortChange)
      .map(() => sortOnProperty(this.data.getValue(), this.sort.active, this.sort.direction));
  }

  disconnect() {
  }
}
<#-- @formatter:on -->