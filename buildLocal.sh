function buildFrontEnd {
    echo "build frontend"
    cd frontend
    npm run build-prod
    npm run testOnce
    cd ..
}

function buildFrontEndImage {
    echo "build frontend image"
    cd frontend
    docker build -t chvote/bo-frontend .
    cd ..
}

function buildBackend {
    echo "build backend"
    cd backend
    mvn clean install
    cd ..
}

function buildBackendImage {
    echo "build backend image"
    cd backend/bo-backend-rest
    export BACKEND_ARTIFACT=`basename $(ls target/*.jar)`
    docker build -f Dockerfile.local --build-arg finalName=$BACKEND_ARTIFACT -t chvote/bo-backend-rest .
    cd ../..
}

function buildMockServerImage(){
    echo "build mock server image"
    cd backend/bo-backend-mock-server
    docker build -f Dockerfile.local -t chvote/bo-backend-mock-server .
    cd ../..
}

function buildReverseProxyImage(){
    echo "reverse proxy image"
    cd bo-reverse-proxy/src/main/docker
    docker build -t chvote/bo-reverse-proxy .
    cd ../../../..
}

function runDocker(){
    echo "run docker compose"
    cd system-tests
    docker-compose kill
    docker-compose rm -f
    docker-compose up -d
    cd ..
}


function runTest(){
    echo "run test"
    cd system-tests
    node wait-for-services.js
    cd ../frontend
    npm run e2eCI
    cd ..
}


function showLogs(){
    cd system-tests
    docker-compose logs -f --tail=1000
}

function navigateLogs(){
    cd system-tests
    docker-compose logs | less
}

function buildAll(){
    buildFrontEnd
    buildFrontEndImage
    buildBackend
    buildBackendImage
    buildMockServerImage
    buildReverseProxyImage
}

if [ $# -eq 0 ]
then
echo "./buildLocal.sh [steps]*"
echo "1) build FrontEnd"
echo "2) build FrontEnd Image"
echo "3) build Backend"
echo "4) build Backend Image"
echo "5) build MockServer Image"
echo "6) build ReverseProxy Image"
echo "0) build All"
echo "------------"
echo "d) run Docker"
echo "t) run Test"
echo "l) show Logs"
echo "n) navigate Logs "
fi


for param in "$@"
do
    case $param in
       1) buildFrontEnd ;;
       2) buildFrontEndImage ;;
       3) buildBackend ;;
       4) buildBackendImage ;;
       5) buildMockServerImage ;;
       6) buildReverseProxyImage ;;
       0) buildAll ;;
       d) runDocker ;;
       t) runTest ;;
       l) showLogs ;;
       n) navigateLogs ;;
       *) echo "INVALID CHOICE!" ;;
    esac
done
